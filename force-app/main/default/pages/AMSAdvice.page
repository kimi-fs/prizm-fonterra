<apex:page applyBodyTag="false" applyHtmlTag="false" showHeader="false" sidebar="false" docType="html-5.0" standardStylesheets="false"
    controller="AMSAdviceController" extensions="AMSSearchController">
    <apex:composition template="AMSCommunityTemplate">

        <apex:define name="page">
            <apex:variable var="page" value="AMSAdvice"/>
        </apex:define>

        <apex:define name="body">
            <apex:form >
                <div class="container">
                    <div class="grid-box">
                        <apex:outputPanel layout="block" id="adviceNav" styleClass="col col--sm-6">
                            <aside class="c-side-nav c-block">
                                <ul class="list-unstyled">
                                    <li class="c-side-nav__item c-side-nav__item--top">
                                        <span class="c-side-nav__link c-side-nav__link--static">
                                            Advice &amp; Support
                                            <svg width="11" height="17" viewBox="0 0 11 17" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M.313 15.273l1.374 1.454 8.77-8.285L1.808.272.434 1.728l7.11 6.715" />
                                            </svg>
                                        </span>
                                    </li>
                                    <apex:repeat value="{!dataCategoryWrappers}" var="topLevelCategory">
                                        <li class="c-side-nav__item">
                                            <apex:outputPanel rendered="{!topLevelCategory.adviceArticle.Id != null}">
                                                <apex:commandLink action="{!selectArticleWrapper}" value="{!topLevelCategory.label}" oncomplete="clearFAQSearch();window.scrollTo(0,0);updateUrl();injectExpandingMarkup();" rerender="adviceContent,adviceNav,updateUrlScript" styleClass="c-side-nav__link {! IF(topLevelCategory.currentArticle, 'c-side-nav__link--current', '')}" >
                                                    <apex:param name="articleCategory" value="{!topLevelCategory.label}" assignTo="{!currentArticleCategoryName}" />
                                                </apex:commandLink>
                                            </apex:outputPanel>
                                            <apex:outputPanel rendered="{!topLevelCategory.adviceArticle.Id == null}">
                                                <span class="c-side-nav__link {! IF(topLevelCategory.currentArticle, 'c-side-nav__link--current', '')}">
                                                    {!topLevelCategory.label}
                                                </span>
                                            </apex:outputPanel>
                                        </li>
                                        <ul class="list-unstyled">
                                            <apex:repeat value="{!topLevelCategory.childDataCategoryWrappers}" var="subCategory">
                                                <apex:outputPanel rendered="{! OR(topLevelCategory.childSelected, topLevelCategory.currentArticle)}">
                                                    <li class="c-side-nav__item c-side-nav__item--sub ">
                                                        <apex:outputPanel rendered="{!subCategory.adviceArticle.Id != null}">
                                                            <apex:commandLink action="{!selectArticleWrapper}" value="{!subCategory.label}" oncomplete="clearFAQSearch();window.scrollTo(0,0);updateUrl();injectExpandingMarkup();" rerender="adviceContent,adviceNav,updateUrlScript" styleClass="c-side-nav__link {! IF(subCategory.currentArticle, 'c-side-nav__link--current', '')}">
                                                                <apex:param name="articleCategory" value="{!subCategory.label}" assignTo="{!currentArticleCategoryName}" />
                                                            </apex:commandLink>
                                                        </apex:outputPanel>
                                                        <apex:outputPanel rendered="{!subCategory.adviceArticle.Id == null}">
                                                            <span class="c-side-nav__link {! IF(subCategory.currentArticle, 'c-side-nav__link--current', '')}">
                                                                {!subCategory.label}
                                                            </span>
                                                        </apex:outputPanel>
                                                    </li>
                                                </apex:outputPanel>
                                            </apex:repeat>
                                        </ul>
                                    </apex:repeat>
                                    <li class="c-side-nav__item">
                                        <apex:commandLink action="{!faqsSelected}" value="Frequently Asked Questions" oncomplete="clearFAQSearch();window.scrollTo(0,0);updateUrl();" rerender="adviceContent,adviceNav,updateUrlScript" styleClass="c-side-nav__link {!IF(showFAQs, 'c-side-nav__link--current', '')}">
                                            <apex:param name="faqs" value="true" assignTo="{!showFAQs}" />
                                        </apex:commandLink>
                                    </li>
                                </ul>
                                <apex:actionFunction name="clearFAQSearch" action="{!clearSearch}" oncomplete="window.bindAdviceEvents();" />
                            </aside>
                        </apex:outputPanel>
                        <apex:outputPanel layout="block" id="adviceContent" styleClass="col col--sm-18">
                            <div class="l-article-list">
                                <apex:outputPanel layout="none" rendered="{!NOT(showFAQs)}">
                                    <div class="c-advice c-block">
                                        <h2 class="c-advice__title">{!currentWrappedAdviceArticle.adviceArticle.Title}</h2>
                                        <apex:outputPanel layout="none" rendered="{!LEN(currentWrappedAdviceArticle.adviceArticle.Highlighted_Content__c) > 0}">
                                            <div class="c-highlight c-wysiwyg">
                                                <apex:outputText escape="false" value="{!currentWrappedAdviceArticle.adviceArticle.Highlighted_Content__c}" />
                                            </div>
                                        </apex:outputPanel>
                                        <div class="c-wysiwyg">
                                            <apex:outputText escape="false" value="{!currentWrappedAdviceArticle.adviceArticle.Content__c}" />
                                        </div>
                                    </div>

                                    <apex:repeat value="{!currentWrappedAdviceArticle.childDataCategoryWrappers}" var="childArticle">
                                        <div class="c-advice c-block">
                                            <apex:outputPanel layout="none" rendered="{!childArticle.adviceArticle != null && childArticle.adviceArticle.Display_Type__c == 'Expand'}">
                                                <h1 class="c-advice__title">
                                                    {!childArticle.adviceArticle.Title}
                                                </h1>
                                                <div class="c-wysiwyg">
                                                    <apex:outputText escape="none" value="{!childArticle.adviceArticle.Summary_Content__c}" />
                                                    <apex:outputPanel layout="none" rendered="{! LEN(childArticle.adviceArticle.Content__c) > 0}">
                                                        <div id="{!childArticle.adviceArticle.UrlName}" class="c-advice__collapsed">
                                                            <apex:outputText escape="false" value="{!childArticle.adviceArticle.Content__c}" />
                                                        </div>
                                                        <div class="c-advice__show-more">
                                                            <a href="#{!childArticle.adviceArticle.UrlName}" class="js-show-more-trigger">
                                                                <svg width="10" height="11" viewBox="0 0 10 11" xmlns="http://www.w3.org/2000/svg">
                                                                  <g fill-rule="nonzero" fill="#009FE3">
                                                                    <path d="M5.293 3.68L1.79.207.734 1.27l4.56 4.524L9.79 1.33 8.734.267"/>
                                                                    <path d="M1.79 5.206L.734 6.27l4.56 4.524L9.79 6.33 8.734 5.267 5.294 8.68"/>
                                                                  </g>
                                                                </svg>
                                                                <span class="js-trigger-text">Show more</span>
                                                            </a>
                                                        </div>
                                                    </apex:outputPanel>
                                                </div>
                                            </apex:outputPanel>
                                            <apex:outputPanel layout="none" rendered="{!childArticle.adviceArticle != null && childArticle.adviceArticle.Display_Type__c != 'Expand'}">
                                                <h3 class="c-advice__title">
                                                    <a href="{!URLFOR($Page.AMSAdvice)}?{!articleParameterName}={!childArticle.adviceArticle.UrlName}">{!childArticle.adviceArticle.Title}</a>
                                                </h3>
                                                <apex:outputText escape="none" value="{!childArticle.adviceArticle.Summary_Content__c}" />
                                                <a href="{!URLFOR($Page.AMSAdvice)}?{!articleParameterName}={!childArticle.adviceArticle.UrlName}" class="read-more">
                                                    Read more
                                                    <svg width="22" height="18" viewBox="0 0 22 18" xmlns="http://www.w3.org/2000/svg">
                                                        <g fill-rule="nonzero">
                                                            <path d="M0 9.866h20.154v-1.5H0"/>
                                                            <path d="M19.342 9.01l-6.06 6.06v2.12l8.18-8.18-8.18-8.182v2.12"/>
                                                        </g>
                                                    </svg>
                                                </a>
                                            </apex:outputPanel>
                                        </div>
                                    </apex:repeat>
                                </apex:outputPanel>
                                <apex:outputPanel layout="none" rendered="{!showFAQs}">
                                    <div class="c-advice c-block">
                                        <!-- for debugging
                                        Current FAQ Article: {!currentFAQArticle}<br/>
                                        Show FAQs : {!showFAQs}<br/>
                                        Number of Articles: {!numberOfArticles}<br/>
                                        -->
                                        <apex:outputPanel layout="none" id="searchHeader">
                                            <h1 class="c-advice__title c-advice__title--center">Frequently Asked Questions</h1>
                                        </apex:outputPanel>
                                        <apex:outputPanel layout="none">
                                            <div class="c-faq-search">
                                                <h5><apex:outputText value="I'm looking for..." /></h5>
                                                <apex:inputText id="seachInput" value="{!searchString}" html-placeholder="Search..." styleClass="l-form__input" />
                                                <apex:commandButton action="{!performSearch}" value="Find" styleClass="btn btn--primary" oncomplete="window.bindAdviceEvents();" rerender="adviceContent">
                                                </apex:commandButton>
                                                <!-- Don't have the clear for now - can ressurect later if we like
                                                <apex:commandButton action="{!clearSearch}" value="Clear" styleClass="btn btn--default" oncomplete="window.bindAdviceEvents();" rerender="adviceContent">
                                                </apex:commandButton>
                                                -->
                                            </div>
                                        </apex:outputPanel>
                                        <apex:outputPanel layout="none" id="searchResults" rendered="{!ISNULL(currentFAQArticle)}">
                                            <apex:outputPanel layout="none" rendered="{!AND(numberOfArticles = 0, OR(searchString = null, searchString = ''))}">
                                                <!-- Popular Articles -->
                                                <h5 class="text-center"><apex:outputText value="Popular Knowledge Articles" /></h5>
                                                <div class="l-faq-list">
                                                    <apex:repeat var="faq" value="{!topFAQs}">
                                                        <div class="c-faq">
                                                            <div class="c-faq__summary">
                                                                <h3 class="c-faq__title">
                                                                    <apex:commandLink action="{!selectFAQ}" oncomplete="window.scrollTo(0,0);window.bindAdviceEvents();" rerender="adviceContent">
                                                                        <apex:param name="article" value="{!faq.UrlName}" assignTo="{!currentArticleUrlName}" />
                                                                        <apex:outputText escape="false" value="{!faq.Title}"/>
                                                                    </apex:commandLink>
                                                                </h3>
                                                                <p>
                                                                    {!faq.Summary}
                                                                </p>
                                                            </div>
                                                            <apex:commandLink action="{!selectFAQ}" value="Read More" oncomplete="window.scrollTo(0,0);window.bindAdviceEvents();" rerender="adviceContent" styleClass="read-more">
                                                                <apex:param name="article" value="{!faq.UrlName}" assignTo="{!currentArticleUrlName}" />
                                                                <svg width="22" height="18" viewBox="0 0 22 18" xmlns="http://www.w3.org/2000/svg">
                                                                  <g fill-rule="nonzero">
                                                                    <path d="M0 9.866h20.154v-1.5H0"/>
                                                                    <path d="M19.342 9.01l-6.06 6.06v2.12l8.18-8.18-8.18-8.182v2.12"/>
                                                                  </g>
                                                                </svg>
                                                            </apex:commandLink>
                                                        </div>
                                                    </apex:repeat>
                                                </div>
                                            </apex:outputPanel>
                                            <apex:outputPanel layout="none" rendered="{!AND(numberOfArticles > 0, AND(searchString != null, searchString != ''))}">
                                                <!-- Search Results -->
                                                <div class="l-faq-list">
                                                    <apex:repeat var="result" value="{!pageResults}">
                                                        <div class="c-faq">
                                                            <div class="c-faq__summary">
                                                                <h3 class="c-faq__title">
                                                                    <apex:commandLink action="{!selectFAQ}" oncomplete="window.scrollTo(0,0);window.bindAdviceEvents();" rerender="adviceContent">
                                                                        <apex:param name="article" value="{!result.matchingURL}" assignTo="{!currentArticleUrlName}" />
                                                                        <apex:outputText escape="false" value="{!result.matchingTitle}"/>
                                                                    </apex:commandLink>
                                                                </h3>
                                                                <apex:outputText escape="false" value="{!result.matchingContent}" />
                                                            </div>
                                                            <apex:commandLink action="{!selectFAQ}" value="Read More" oncomplete="window.scrollTo(0,0);window.bindAdviceEvents();" rerender="adviceContent"  styleClass="read-more">
                                                                <apex:param name="article" value="{!result.matchingURL}" assignTo="{!currentArticleUrlName}" />
                                                                <svg width="22" height="18" viewBox="0 0 22 18" xmlns="http://www.w3.org/2000/svg">
                                                                  <g fill-rule="nonzero">
                                                                    <path d="M0 9.866h20.154v-1.5H0"/>
                                                                    <path d="M19.342 9.01l-6.06 6.06v2.12l8.18-8.18-8.18-8.182v2.12"/>
                                                                  </g>
                                                                </svg>
                                                            </apex:commandLink>
                                                        </div>
                                                    </apex:repeat>
                                                </div>
                                            </apex:outputPanel>
                                            <apex:outputPanel layout="none" rendered="{!AND(numberOfArticles = 0, AND(searchString != null, searchString != ''))}">
                                                <c:AMSWebText title="AMSAdvice: No Search Results" />
                                            </apex:outputPanel>

                                            <apex:outputPanel layout="none" rendered="{!numberOfPages > 1}">
                                                <!-- Pagination for search results -->
                                                <div class="c-pagination c-block">
                                                    <ul class="list-unstyled">
                                                        <apex:repeat value="{!paginationLinks}" var="p">
                                                            <apex:outputPanel layout="none"  rendered="{!p.showAsLink}">
                                                                <li class="c-pagination__item">
                                                                    <apex:commandLink action="{!jumpToPage}" oncomplete="window.scrollTo(0,0);window.bindAdviceEvents();" rerender="adviceContent" styleClass="c-pagination__link">
                                                                        <apex:param name="page" value="{!p.pageNumber}" assignTo="{!pageNumber}" />
                                                                        <apex:outputText escape="false" value="{!p.label}" />
                                                                    </apex:commandLink>
                                                                </li>
                                                            </apex:outputPanel>
                                                            <apex:outputPanel layout="none"  rendered="{!NOT(p.showAsLink)}">
                                                                <li class="c-pagination__item c-pagination__item--static">
                                                                    <span class="c-pagination__link">{!p.label}</span>
                                                                </li>
                                                            </apex:outputPanel>
                                                        </apex:repeat>
                                                    </ul>
                                                </div>
                                            </apex:outputPanel>
                                        </apex:outputPanel>
                                    </div>
                                    <apex:outputPanel layout="none" id="showArticle" rendered="{!NOT(ISNULL(currentFAQArticle))}">
                                        <!-- Selected FAQ article -->
                                        <div class="c-advice c-block">
                                            <h2 class="c-advice__title">{!currentFAQArticle.Title}</h2>
                                            <div class="c-wysiwyg">
                                                <apex:outputText escape="false" value="{!currentFAQArticle.Content__c}" />
                                            </div>
                                        </div>
                                    </apex:outputPanel>
                                </apex:outputPanel>
                            </div>
                        </apex:outputPanel>
                    </div>
                </div>
            </apex:form>
        </apex:define>
        <apex:define name="bottom">
            <script type="text/javascript">
                injectExpandingMarkup();
                function injectExpandingMarkup() {
                    var expandingDivs = document.querySelectorAll('.expandableContent');
                    for(var i = 0; i < expandingDivs.length; i++){
                        var elementId = 'expandingContent'+i;
                        var element = expandingDivs[i];
                        element.classList.add("c-advice__collapsed");
                        element.setAttribute("id", elementId);
                        element.insertAdjacentHTML("afterend","<div class='c-advice__show-more'> <a href='#"+elementId+"' class='js-show-more-trigger'> <svg width='10' height='11' viewBox='0 0 10 11' xmlns='http://www.w3.org/2000/svg'> <g fill-rule='nonzero' fill='#009FE3'> <path d='M5.293 3.68L1.79.207.734 1.27l4.56 4.524L9.79 1.33 8.734.267'/> <path d='M1.79 5.206L.734 6.27l4.56 4.524L9.79 6.33 8.734 5.267 5.294 8.68'/> </g> </svg> <span class='js-trigger-text'>Show more</span> </a> </div>");
                    }
                }
            </script>
            <apex:outputPanel id="updateUrlScript">
                <script>
                    function updateUrl() {
                        var pageUrl;
                        if({!showFAQs}){
                            pageUrl = '?'+'{!FAQParameterName}'+'=true';
                        }else{
                            pageUrl = '?'+'{!articleCategoryParameterName}'+'='+'{!URLENCODE(currentArticleCategoryName)}';
                        }
                        window.history.pushState('', '', pageUrl);
                        // because we don't refresh the whole page when updating we need to send google analytics virtual pageview
                        window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
                        ga('create', '{!$Label.AMS_Google_analytics_tracking_ID}', 'auto');
                        ga('send', { 'hitType': 'pageview', 'page': window.location.pathname+pageUrl });

                    }
                </script>
            </apex:outputPanel>
        </apex:define>
    </apex:composition>
</apex:page>