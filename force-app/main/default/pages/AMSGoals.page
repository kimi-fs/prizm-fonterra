<apex:page applyBodyTag="false" applyHtmlTag="false" showHeader="false" sidebar="false" docType="html-5.0" standardStylesheets="false" controller="AMSGoalsController">
    <apex:composition template="AMSCommunityTemplate">

        <apex:define name="page">
            <apex:variable var="page" value="AMSGoals"/>
        </apex:define>

        <apex:define name="body">
            <apex:form >
                <div class="l-dashboard">
                    <div class="container">
                        <h1 class="l-strategic-goal__title">
                            Strategic Goals
                        </h1>

                        <p class="description">
                            Listed below are your goals, both past and present. Those goals which have been completed are listed in the Archive section at the bottom while the goals you’re currently working on are listed at the top. You can access the actions, attachments and discussion for both active and archived goals by simply clicking the goal name in the list.<br/>
                            Should you wish to discuss any aspect of your goals, please contact your Farm Source Professional.
                        </p>

                        <div class="l-strategic-goal__list">
                            <h2 class="title">Active Goals</h2>

                            <apex:repeat value="{!activeGoals}" var="goal">
                                <a href="{!URLFOR($Page.AMSGoalDetails)}?goalId={!goal.Id}" class="c-list-card">
                                    <h3 class="c-list-card__heading">
                                        {!goal.Name}
                                    </h3>
                                    <div class="c-list-card__meta">
                                        <span class="c-list-card__meta-item">
                                            Target date:
                                            <apex:outputText value="{0, date, dd MMMMM yyyy}">
                                                <apex:param value="{!goal.Goal_Date__c}"/>
                                            </apex:outputText>
                                        </span>
                                        <span class="c-list-card__meta-item">
                                            {!goal.Actions__r.size} pending action(s)
                                        </span>
                                        <span class="c-list-card__meta-item">
                                            {!goal.Category__c}
                                        </span>
                                    </div>
                                </a>
                            </apex:repeat>
                        </div>

                        <div class="l-strategic-goal__list">
                            <div class="grid-box grid-box--v-center grid-box--no-gutter grid-box--single-row title-bar">
                                <div class="col col--sm-18">
                                    <h2 class="title">Archived Goals</h2>
                                </div>
                                <div class="col col--sm-6 input-select">
                                    <select class="l-form__input l-form__input--select">
                                        <option value="finished">Completed goals</option>
                                    </select>
                                </div>
                            </div>

                            <apex:repeat value="{!archivedGoals}" var="goal">
                                <a href="{!URLFOR($Page.AMSGoalDetails)}?goalId={!goal.Id}" class="c-list-card">
                                    <h3 class="c-list-card__heading">
                                        {!goal.Name}
                                    </h3>
                                    <div class="c-list-card__meta">
                                        <span class="c-list-card__meta-item">
                                            Target date:
                                            <apex:outputText value="{0, date, dd MMMMM yyyy}">
                                                <apex:param value="{!goal.Goal_Date__c}"/>
                                            </apex:outputText>
                                        </span>
                                        <span class="c-list-card__meta-item">
                                            {!goal.Actions__r.size} pending action(s)
                                        </span>
                                        <span class="c-list-card__meta-item">
                                            {!goal.Category__c}
                                        </span>
                                    </div>
                                </a>
                            </apex:repeat>
                        </div>
                    </div>
                </div>
            </apex:form>
        </apex:define>
    </apex:composition>
</apex:page>