<apex:page applyBodyTag="false" applyHtmlTag="false" showHeader="false" sidebar="false" docType="html-5.0" 
           standardStylesheets="false" controller="AMSContactController" action="{!initialiseEnquiry}">

    <apex:composition template="AMSCommunityTemplate">

        <apex:define name="body">
            <div class="container container--smaller">
                <div class="l-enquiry">
                    <h1 class="l-enquiry__heading">
                        Send Enquiry or Feedback
                    </h1>
                    <div class="l-supplier-forms">
                        <apex:form >
                            <apex:outputPanel layout="block">
                                <apex:outputPanel layout="block" rendered="{!submitted}">
                                    <div class="c-alert c-alert--good">
                                        <svg width="27" height="27" viewBox="0 0 27 27" xmlns="http://www.w3.org/2000/svg">
                                            <g fill="#8AC949" fill-rule="evenodd">
                                                <path d="M12.05 17.07c0-.82.1-1.472.3-1.96.198-.485.563-.964 1.093-1.436.53-.47.883-.855 1.06-1.15.175-.297.263-.61.263-.937 0-.99-.457-1.485-1.37-1.485-.435 0-.782.133-1.043.4-.26.266-.397.634-.41 1.102H9.397c.01-1.12.373-1.995 1.085-2.627.713-.633 1.684-.95 2.915-.95 1.242 0 2.206.3 2.89.9.687.602 1.03 1.45 1.03 2.546 0 .498-.112.968-.335 1.41-.222.443-.612.933-1.168 1.472l-.712.677c-.446.428-.7.93-.765 1.503l-.035.536h-2.25zm-.255 2.7c0-.393.133-.717.4-.972.266-.255.608-.382 1.024-.382.415 0 .756.127 1.023.382.266.255.4.58.4.972 0 .386-.13.706-.392.958-.26.25-.604.377-1.03.377-.43 0-.773-.126-1.034-.377-.26-.252-.39-.572-.39-.958z"/>
                                                <path d="M13.5 27C6.044 27 0 20.956 0 13.5S6.044 0 13.5 0 27 6.044 27 13.5 20.956 27 13.5 27zm0-2C19.85 25 25 19.85 25 13.5S19.85 2 13.5 2 2 7.15 2 13.5 7.15 25 13.5 25z" fill-rule="nonzero"/>
                                            </g>
                                        </svg>
                                        <c:AMSWebText title="AMSEnquiry: submitted" />
                                    </div>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!LEN(message) > 0}">
                                    <div class="c-alert c-alert--bad">
                                        <svg width="22" height="22" viewBox="0 0 22 22" xmlns="http://www.w3.org/2000/svg">
                                        <g fill="#CC0E00" fill-rule="evenodd">
                                            <path d="M10.297 5l.133 7.53h1.297L11.86 5h-1.563zm.79 11.422c.317 0 .576-.102.776-.305.2-.203.3-.463.3-.78 0-.324-.1-.587-.3-.79-.2-.203-.46-.305-.777-.305-.318 0-.578.102-.78.305-.204.203-.306.466-.306.79 0 .317.102.577.305.78.203.203.463.305.78.305z"/>
                                            <path d="M10.75 21.5C4.813 21.5 0 16.687 0 10.75S4.813 0 10.75 0 21.5 4.813 21.5 10.75 16.687 21.5 10.75 21.5zm0-1.5c5.11 0 9.25-4.14 9.25-9.25S15.86 1.5 10.75 1.5 1.5 5.64 1.5 10.75 5.64 20 10.75 20z" fill-rule="nonzero"/>
                                        </g>
                                        </svg>
                                        <apex:outputText value="{!message}" escape="false" />
                                    </div>
                                </apex:outputPanel>

                                <h2>
                                    Enquiry
                                </h2>
                                <p>
                                    <c:AMSWebText title="AMSEnquiry: topBlurb" />
                                </p>
                                <div class="l-form">
                                    <div class="l-form__field l-form__field--fw">
                                        <apex:outputLabel for="type" value="Type (required)" styleClass="l-form__label"/>
                                        <apex:selectList id="type" value="{!feedbackType}" multiselect="false" size="1" required="true" styleClass="l-form__input l-form__input--select">
                                            <apex:selectOptions value="{!feedbackTypeOptions}" />
                                        </apex:selectList>
                                    </div>
                                    <div class="l-form__field l-form__field--fw">
                                        <apex:outputLabel for="farm" value="Farm (required)" styleClass="l-form__label"/>
                                        <apex:selectList id="farm" value="{!farm}" multiselect="false" size="1" required="true" styleClass="l-form__input l-form__input--select">
                                            <apex:selectOptions value="{!farmOptions}" />
                                        </apex:selectList>
                                    </div>
                                    <div class="l-form__field l-form__field--fw">
                                        <apex:outputLabel for="enquiry" value="Message (required, max 500 chars)" styleClass="l-form__label"/>
                                        <apex:inputTextArea id="enquiry" value="{!enquiryText}" html-maxlength="500" styleClass="l-form__input l-form__input--textarea" />
                                    </div>
                                    <div class="l-form__actions l-form__actions--center">
                                        <apex:actionRegion >
                                            <apex:commandButton value="Cancel" action="{!cancel}" styleClass="btn btn--default-invert"/>
                                        </apex:actionRegion>
                                        <apex:commandButton value="Send" action="{!save}" styleClass="btn btn--secondary"/>
                                    </div>
                                </div>
                            </apex:outputPanel>
                        </apex:form>
                    </div>
                </div>
            </div>
        </apex:define>
    </apex:composition>
</apex:page>