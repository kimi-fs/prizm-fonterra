<apex:page applyBodyTag="false" applyHtmlTag="false" showHeader="false" sidebar="false" docType="html-5.0" standardStylesheets="false" controller="AMSNewsDetailController">
    <apex:composition template="{!IF(isLoggedIn, 'AMSCommunityTemplate', 'AMSGuestTemplate')}">

        <apex:define name="page">
            <apex:variable var="page" value="AMSNews"/>
        </apex:define>

        <apex:define name="body">
            <div class="container">
                <div class="grid-box">
                    <div class="col col--sm-6">
                        <aside class="c-side-nav c-block">
                            <ul class="list-unstyled">
                                <li class="c-side-nav__item c-side-nav__item--top">
                                    <apex:outputLink value="{!$Page.AMSNews}" styleClass="c-side-nav__link">
                                        News
                                        <svg width="11" height="17" viewBox="0 0 11 17" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M.313 15.273l1.374 1.454 8.77-8.285L1.808.272.434 1.728l7.11 6.715" />
                                        </svg>
                                        <apex:param name="{!pageNumberParameterName}" value="{!previousPageNumber}"/>
                                    </apex:outputLink>
                                </li>
                                <apex:repeat value="{!dataCategoryWrappers}" var="topLevelCategory">
                                    <li class="c-side-nav__item">
                                        <apex:outputLink value="{!URLFOR($Page.AMSNews)}?{!articleCategoryParameterName}={!topLevelCategory.name}" styleClass="c-side-nav__link {!IF(topLevelCategory.selected, 'c-side-nav__link--current', '')}">
                                            {!topLevelCategory.label}
                                        </apex:outputLink>
                                    </li>
                                    <ul class="list-unstyled">
                                        <apex:outputPanel layout="none" rendered="{!OR(topLevelCategory.subCategorySelected, topLevelCategory.selected)}">
                                            <apex:repeat value="{!topLevelCategory.subCategories}" var="subCategory">
                                                <li class="c-side-nav__item c-side-nav__item--sub ">
                                                    <apex:outputLink value="{!URLFOR($Page.AMSNews)}?{!articleCategoryParameterName}={!subCategory.name}" styleClass="c-side-nav__link {!IF(subCategory.selected, 'c-side-nav__link--current', '')}">
                                                        {!subCategory.label}
                                                    </apex:outputLink>
                                                </li>
                                            </apex:repeat>
                                        </apex:outputPanel>
                                    </ul>
                                </apex:repeat>
                            </ul>
                        </aside>
                    </div>
                    <div class="col col--sm-18">
                        <div class="l-article-list">
                            <apex:outputPanel rendered="{!previousPageNumber != null}">
                                <div class="c-back-link">
                                    <apex:outputLink value="{!$Page.AMSNews}" >
                                        <svg width="11" height="17" viewBox="0 0 11 17" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M10.456 1.727L9.083.273.313 8.558l8.648 8.17 1.374-1.455-7.108-6.715" />
                                        </svg>
                                        Back
                                        <apex:param name="{!pageNumberParameterName}" value="{!previousPageNumber}"/>
                                        <apex:param name="{!articleCategoryParameterName}" value="{!currentArticleCategoryName}"/>
                                    </apex:outputLink>
                                </div>
                            </apex:outputPanel>
                            <apex:outputPanel rendered="{!NOT(newsArticlePresent)}">
                                <h3 class="l-article-list__no-items">
                                    {!messages['no-article']}
                                </h3>
                            </apex:outputPanel>
                            <apex:outputPanel rendered="{!newsArticlePresent}">
                                <div class="c-article-summary c-article-summary--detail c-block">
                                    <div class="c-article-summary__image">
                                        <apex:outputField value="{!newsArticle.Headline_Image__c}"/>
                                    </div>
                                    <div class="c-article-summary__summary">
                                        <h1 class="c-article-summary__title">
                                            {!newsArticle.Title}
                                        </h1>
                                        <small>
                                            <apex:outputText >
                                                {!newsArticle.Author__c}. {!newsArticleFirstPublishedDate}
                                            </apex:outputText>
                                        </small>
                                        <div class="c-wysiwyg">
                                            <apex:outputText escape="false" value="{!newsArticle.Content__c}"/>
                                        </div>
                                    </div>
                                </div>
                            </apex:outputPanel>
                        </div>
                    </div>
                </div>
            </div>
        </apex:define>
    </apex:composition>
</apex:page>