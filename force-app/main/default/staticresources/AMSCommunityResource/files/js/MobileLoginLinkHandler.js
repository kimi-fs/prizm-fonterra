/**
 * The Mobile App requires OAuth2 context (key, callback, etc)
 * to be preserved - using the vanilla "Login" link doesn't
 * give the app the OAuth2 callback it requires. As such, when
 * in an app webview, the login links are overriden to provide
 * this context.
 *
 * The exact timing of the injection of the vars into the
 * webview can vary depending on platform, so this is handled
 * using a click listener, so it can be checked at the last
 * possible moment.
 */
function overrideLoginLinksForApp() {
    var loginLinks = document.querySelectorAll('.login-return-link');
    [].forEach.call(loginLinks, function(loginLink) {
        loginLink.addEventListener('click', function(e) {
            if (window.isInAppWebView && window.oauthStartUrl) {
                window.location.href = window.oauthStartUrl
                e.preventDefault();
                return false;
            }
        })
    });
}
document.addEventListener('DOMContentLoaded', function init() {
    overrideLoginLinksForApp();
});
