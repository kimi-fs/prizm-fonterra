Highcharts.setOptions({
  lang: {
    numericSymbols: null,
    thousandsSep: ','
  },
  chart: {
    style: {
      fontFamily: '"Droid Sans", sans-serif'
    }
  },
  credits: {
    enabled: false
  }
});

window.HighchartsCustomTooltip = {
  tooltipFormatter: function(highchartsRef, dateFormat, label, precision, isPercentage) {
    //ripped from https://stackoverflow.com/questions/7557533/display-tooltip-for-invisible-series-in-highcharts
    var s = '<div style="text-align: center;">'+ Highcharts.dateFormat(dateFormat, highchartsRef.x) +' : ' + label + '</div>';
    s += '<table class="c-graph__tooltip">'
    var chart = highchartsRef.points[0].series.chart; //get the chart object
    var categories = chart.xAxis[0].categories; //get the categories array
    var index = 0;
    while (highchartsRef.x !== categories[index]) {index++;} //compute the index of corr y value in each data arrays
    chart.series.forEach(function(series, i) { //loop through series array
      if (series.data[index] && series.data[index].y !== null) {
        s += '<tr>' + HighchartsCustomTooltip.formatSeriesLine(series, index, precision, isPercentage) + '</tr>';
      }
    });
    s += '</table>'
    return s;
  },
  formatValue: function(seriesName, value, precision, isPercentage) {
    var formatted;
    if (seriesName == 'Variance') {
      formatted = Highcharts.numberFormat(Math.abs(value), 2);
      formatted += '%';
    } else {
      formatted = Highcharts.numberFormat(value, precision);
      if (isPercentage) {
        formatted += '%';
      }
    }
    return formatted;
  },
  formatSeriesLine: function(series, index, precision, isPercentage) {
    var seriesLineMarkup = '<td class="tooltip-name">' + series.name + '</td> ';
    if (series.name == 'Variance') {
      seriesLineMarkup += '<td class="tooltip-icon" style="color:' + series.color + '"><span class="' + (series.data[index].y > 0.0 ? 'c-table__rise c-table__rise--tooltip' : 'c-table__drop c-table__drop--tooltip') + '"></span></td>';
    } else {
      seriesLineMarkup += '<td class="tooltip-icon" style="color:' + series.color + '"><div class="season-indicator" style="background-color:' + series.color + '"></div></td>';
    }

    seriesLineMarkup += '<td class="tooltip-value"><b>' + HighchartsCustomTooltip.formatValue(series.name, series.data[index].y, precision, isPercentage) + '</b></td>';     //use index to get the y value

    return seriesLineMarkup;
  }
}
