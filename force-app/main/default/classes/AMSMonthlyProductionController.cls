/**
 * Created by Nick on 07/08/17.
 *
 * AMSProdQualSummaryController
 *
 * Controller for ProdQualSummary page
 *
 * Author: NL (Trineo)
 * Date: 2017-08-07
 **/
public without sharing class AMSMonthlyProductionController extends AMSProductionReport {

    public AMSProductionReport baseController { get { return this; } private set; }

    private static final String DEFAULT_REPORT_TYPE = 'Monthly Production';

    public AMSCollectionService.CollectionPeriodForm collectionPeriodForm { get; set; }
    public AMSCollectionGraph collectionGraph { get; set; }

    public AMSMonthlyProductionController() {
        selectedReportTypeName = DEFAULT_REPORT_TYPE;
    }

    //Call recalculate for the forms
    public override void recalculateForm() {
        collectionPeriodForm = new AMSCollectionService.CollectionPeriodForm(fromDate,
            toDate,
            fromDatePriorSeason,
            toDatePriorSeason,
            priorFarmSeason,
            selectedFarmSeason,
            selectedFarmId,
            'Month');
        collectionGraph = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.GRAPH_SELECT_OPTIONS_TO_UNIT_OF_MEASUREMENT_MAP.get(selectedGraphOption), false);
    }

    public override void defaultFromToDate() {
        fromDate = selectedFarmSeason.Reference_Period__r.Start_Date__c;

        if (selectedFarmSeason.Reference_Period__r.Status__c == 'Past') {
            toDate = selectedFarmSeason.Reference_Period__r.End_Date__c;
            toDatePriorSeason = toDate.addYears(-1);
        } else {
            toDate = TODAY; //today for current season, but the prior season should remain -1 year on the date of the latest valid collection for comparison purposes
            toDatePriorSeason = getDateOfLatestCollection().addYears(-1);
        }

        fromDatePriorSeason = fromDate.addYears(-1);
        populateDateStringFromDates();
        populateMonthList();
    }

    private Date getDateOfLatestCollection() {

        Map<Id, Date> farmsLatestCollectionDate = AMSCollectionService.getFarmsLatestCollectionDate(new List<Id>{selectedFarmId});
        if (farmsLatestCollectionDate.containsKey(selectedFarmId) && farmsLatestCollectionDate.get(selectedFarmId) >= selectedFarmSeason.Reference_Period__r.Start_Date__c) {
            return farmsLatestCollectionDate.get(selectedFarmId);
        }

        return TODAY;
    }
}