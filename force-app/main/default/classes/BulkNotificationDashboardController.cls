public with sharing class BulkNotificationDashboardController {

    public final static String lightningReportLinkPrefix = '/lightning/r/Report/';

    public static Set<String> SMS_STATUSES = new Set<String> {
        MarketingCloudUtil.MCSTATUSPENDING,
        MarketingCloudUtil.MCSTATUSSENT,
        MarketingCloudUtil.MCSTATUSDELIVERED,
        MarketingCloudUtil.MCSTATUSDELAYED,
        MarketingCloudUtil.MCSTATUSFAILEDSEND,
        MarketingCloudUtil.MCSTATUSFAILEDDELIVERY
    };

    public static Id casesCreatedReportId {
        get {
            if (casesCreatedReportId == null && !Test.isRunningTest()) {
                casesCreatedReportId = [Select Id From Report Where DeveloperName = 'Bulk_SMS_Case_List'].Id;
            }

            return casesCreatedReportId;
        }
        set;
    }
    //Bulk_SMS_Case_List

    public static Id tasksCreatedReportId {
        get {
            if (tasksCreatedReportId == null && !Test.isRunningTest()) {
                tasksCreatedReportId = [Select Id From Report Where DeveloperName = 'Bulk_SMS_Task_List'].Id;
            }

            return tasksCreatedReportId;
        }
        set;
    }
    //Bulk_SMS_Task_List


    public List<BulkNotificationStatistics> bulkSentNotifications {get; set;}

    public class BulkNotificationStatistics {
        public String bulkIdKey {get; set;}
        public String username {get; set;}
        public String localDate {get; set;}

        public Integer numberOfCases {get; set;}
        public Integer numberOfFarms {get; set;}

        public Integer allSMS {get; set;}
        public Integer numberOfPendingSMS {get; set;}
        public Integer numberOfSentSMS {get; set;}
        public Integer numberOfDeliveredSMS {get; set;}
        public Integer numberOfDelayedSMS {get; set;}
        public Integer numberOfFailedSMS {get; set;}

        //classic links
        public String allTextLink {get; set;}
        public String casesCreatedLink {get; set;}
        public String textPendingLink {get; set;}
        public String textSentLink {get; set;}
        public String textDeliveredLink {get; set;}
        public String textDelayedLink {get; set;}
        public String textFailedLink {get; set;}

        //lightning links
        public String allTextLinkLightning {get; set;}
        public String casesCreatedLinkLightning {get; set;}
        public String textPendingLinkLightning {get; set;}
        public String textSentLinkLightning {get; set;}
        public String textDeliveredLinkLightning {get; set;}
        public String textDelayedLinkLightning {get; set;}
        public String textFailedLinkLightning {get; set;}

        public BulkNotificationStatistics(List<Case> cases, List<Task> tasks, Map<Id, User> mapUserById, String bulkSendId) {
            Set<Id> entities = new Set<Id>();
            bulkIdKey = bulkSendId;

            allTextLink = '/' + tasksCreatedReportId + '?pv0=' + bulkIdKey + '&pv1=PENDING,SENT,DELIVERED,DELAYED,FAILEDSEND,FAILEDDELIVERY';
            casesCreatedLink = '/' + casesCreatedReportId + '?pv0=' + bulkIdKey;
            textPendingLink = '/' + tasksCreatedReportId + '?pv0=' + bulkIdKey + '&pv1=PENDING';
            textSentLink = '/' + tasksCreatedReportId + '?pv0=' + bulkIdKey + '&pv1=SENT';
            textDeliveredLink = '/' + tasksCreatedReportId + '?pv0=' + bulkIdKey + '&pv1=DELIVERED';
            textDelayedLink = '/' + tasksCreatedReportId + '?pv0=' + bulkIdKey + '&pv1=DELAYED';
            textFailedLink = '/' + tasksCreatedReportId + '?pv0=' + bulkIdKey + '&pv1=FAILEDSEND&pv2=FAILEDDELIVERY';

            allTextLinkLightning = lightningReportLinkPrefix + tasksCreatedReportId + '/view?fv0=' + bulkIdKey + '&fv1=PENDING,SENT,DELIVERED,DELAYED,FAILEDSEND,FAILEDDELIVERY';
            casesCreatedLinkLightning = lightningReportLinkPrefix + casesCreatedReportId + '/view?fv0=' + bulkIdKey;
            textPendingLinkLightning = lightningReportLinkPrefix + tasksCreatedReportId + '/view?fv0=' + bulkIdKey + '&fv1=PENDING';
            textSentLinkLightning = lightningReportLinkPrefix + tasksCreatedReportId + '/view?fv0=' + bulkIdKey + '&fv1=SENT';
            textDeliveredLinkLightning = lightningReportLinkPrefix + tasksCreatedReportId + '/view?fv0=' + bulkIdKey + '&fv1=DELIVERED';
            textDelayedLink = lightningReportLinkPrefix + tasksCreatedReportId + '/view?fv0=' + bulkIdKey + '&fv1=DELAYED';
            textFailedLinkLightning = lightningReportLinkPrefix + tasksCreatedReportId + '/view?fv0=' + bulkIdKey + '&fv1=FAILEDSEND&fv2=FAILEDDELIVERY';

            if (cases != NULL) {
                for (Case c : cases) {
                    entities.add(c.AccountId);
                }

                numberOfCases = cases.size();
            } else {
                numberOfCases = 0;
            }
            if (tasks != NULL) {
                allSMS = tasks.size();
            } else {
                allSMS = 0;
            }

            numberOfPendingSMS = 0;
            numberOfSentSMS = 0;
            numberOfDeliveredSMS = 0;
            numberOfDelayedSMS = 0;
            numberOfFailedSMS = 0;

            if (tasks != NULL) {
                for (Task t : tasks) {
                    entities.add(t.WhatId);

                    if (t.SMS_Status_Reason__c == MarketingCloudUtil.MCSTATUSPENDING) {
                        numberOfPendingSMS += 1;
                    }

                    if (t.SMS_Status_Reason__c == MarketingCloudUtil.MCSTATUSSENT) {
                        numberOfSentSMS += 1;
                    }

                    if (t.SMS_Status_Reason__c == MarketingCloudUtil.MCSTATUSDELIVERED) {
                        numberOfDeliveredSMS += 1;
                    }

                    if (t.SMS_Status_Reason__c == MarketingCloudUtil.MCSTATUSDELAYED) {
                        numberOfDelayedSMS += 1;
                    }

                    if (t.SMS_Status_Reason__c == MarketingCloudUtil.MCSTATUSFAILEDSEND || t.SMS_Status_Reason__c == MarketingCloudUtil.MCSTATUSFAILEDDELIVERY) {
                        numberOfFailedSMS += 1;
                    }
                }
            }

            numberOfFarms = entities.size();

            Datetime dt = NULL;

            if (tasks != NULL) {
                username = mapUserById.get(tasks[0].CreatedById).Name;
                dt = tasks[0].CreatedDate;
            } else if (tasks == NULL && cases != NULL) {
                username = mapUserById.get(cases[0].CreatedById).Name;
                dt = cases[0].CreatedDate;
            } else {
                username = '';
            }

            if (dt != NULL) {
                localDate = dt.format('dd/MM/yyyy hh:mm a');
            } else {
                localDate = NULL;
            }
        }
    }

    public class BulkNotificationInformation {
        public Map<String, List<Case>> mapCaseListByBatchKey {get; set;}
        public Map<String, List<Task>> mapTaskListByBatchKey {get; set;}
        public Set<String> bulkSendIds {get; set;}

        public List<BulkNotificationStatistics> statistics {get; set;}

        public BulkNotificationInformation(String timespan) {

            mapCaseListByBatchKey = new Map<String, List<Case>>();
            mapTaskListByBatchKey = new Map<String, List<Task>>();
            statistics          = new List<BulkNotificationStatistics>();


            bulkSendIds = new Set<String>();
            Set<Id> caseRecordTypeIds = getCaseRecordTypeIds();

            Datetime dt = SYSTEM.now().addHours(- (Integer.valueOf(timespan)));

            List<Case> cases = [SELECT Id, CreatedById, CreatedDate, Description, AccountId FROM Case
                                WHERE RecordTypeId IN: caseRecordTypeIds
                                AND CreatedDate >=: dt];

            List<Task> tasks = [SELECT Id, CreatedById, CreatedDate, Description, SMS_Message_ID__c, WhatId, SMS_Status_Reason__c FROM Task
                                WHERE RecordTypeId =: GlobalUtility.activityTaskSystemGeneratedTaskRecordTypeId
                                AND SMS_Status_Reason__c IN: SMS_STATUSES
                                AND CreatedDate >=: dt];

            Set<Id> userIds = new Set<Id>();

            for (Task t : tasks) {
                String batchIdKey = t.SMS_Message_ID__c;

                if (String.isBlank(batchIdKey)) {
                    continue;
                }

                bulkSendIds.add(batchIdKey);
                userIds.add(t.CreatedById);

                if (!mapTaskListByBatchKey.containsKey(batchIdKey)) {
                    mapTaskListByBatchKey.put(batchIdKey, new List<Task>());
                }

                List<Task> tasksWithBatchIdKey = mapTaskListByBatchKey.get(batchIdKey);
                tasksWithBatchIdKey.add(t);

                mapTaskListByBatchKey.put(batchIdKey, tasksWithBatchIdKey);
            }

            for (Case c : cases) {
                String batchIdKey = getBatchIdKey(c.Description);

                if (String.isBlank(batchIdKey)) {
                    continue;
                }

                bulkSendIds.add(batchIdKey);
                userIds.add(c.CreatedById);

                if (!mapCaseListByBatchKey.containsKey(batchIdKey)) {
                    mapCaseListByBatchKey.put(batchIdKey, new List<Case>());
                }

                List<Case> casesWithBatchIdKey = mapCaseListByBatchKey.get(batchIdKey);
                casesWithBatchIdKey.add(c);

                mapCaseListByBatchKey.put(batchIdKey, casesWithBatchIdKey);
            }

            Map<Id, User> mapUserById = new Map<Id, User>([SELECT Id, Name FROM User WHERE Id IN: userIds]);

            List<String> orderedBatchKeys = new List<String>();

            for (String batchIdKey : bulkSendIds) {
                orderedBatchKeys.add(batchIdKey);
            }

            orderedBatchKeys.sort();

            for (Integer i = (orderedBatchKeys.size() - 1); i >= 0; i--) {
                statistics.add(new BulkNotificationStatistics(mapCaseListByBatchKey.get(orderedBatchKeys[i]), mapTaskListByBatchKey.get(orderedBatchKeys[i]), mapUserById, orderedBatchKeys[i]));
            }
        }

        private Set<Id> getCaseRecordTypeIds() {
            Id digitalServicesRecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND Name = 'Communications AU'].Id; //FSCRM-2327 'Digital Services' -> 'Communications'

            return new Set<Id>{digitalServicesRecordTypeId};
        }

        private String getBatchIdKey(String description) {
            if (String.isNotBlank(description)) {
                for (String newLine : description.split('\r\n')) {
                    if (newLine.contains(MarketingCloudUtil.BATCHKEYSTART) && newLine.contains(MarketingCloudUtil.BATCHKEYEND)) {
                        return newLine.split(MarketingCloudUtil.BATCHKEYSTART)[1].split(MarketingCloudUtil.BATCHKEYEND)[0];
                    }
                }
            }

            return null;
        }
    }

    public BulkNotificationDashboardController() {
        String timespan = ApexPages.currentPage().getParameters().get('timespan');

        if (timespan == null) {
            timespan = '24';
        }

        BulkNotificationInformation bni = new BulkNotificationInformation(timespan);
        bulkSentNotifications = bni.statistics;
    }
}