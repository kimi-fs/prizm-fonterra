/**
 * AMSSimpleCommentsController.cls
 * Description: Controller for the AMSSimpleComments component
 *
 * @author: John Au
 * @date: December 2018
 */
public class AMSSimpleCommentsController {

    public List<GoalService.FeedItemWrapper> feedItemWrappers { get; set; }
    public GoalService.FeedItemWrapper newFeedItemWrapper { get; set; }
    public Id parentIdParam {
        get;
        set {
            //one time initialization
            if (this.parentIdParam == null) {
                this.parentIdParam = value;
                refreshFeedItems();
            }
        }
    }

    public AMSSimpleCommentsController() {

    }

    public void insertNewFeedItem() {
        newFeedItemWrapper.save();

        refreshFeedItems();
    }

    private void refreshFeedItems() {
        this.newFeedItemWrapper = new GoalService.FeedItemWrapper(parentIdParam);
        this.feedItemWrappers = GoalService.getFeedItemsForObject(parentIdParam);
    }

}