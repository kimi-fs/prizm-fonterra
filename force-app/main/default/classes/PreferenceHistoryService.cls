public class PreferenceHistoryService implements Comparable {
    public DateTime dt {get; set;}

    public User user {get; set;}
    public Account account {get; set;}

    public String oldValue {get; set;}
    public String newValue {get; set;}
    public String field {get; set;}
    public String setting {get; set;}

    public static List<PreferenceHistoryService> getIndividualHistory(Contact individual) {
        Map<String, String> mapFieldLabelByDeveloperName = new Map<String, String>{
            'Preferred_Store__c' => 'Preferred Store',
            'Farm_Source_Copies__c' => 'Farm Source Copies',
            'Community_Programmes__c' => 'Community Programmes',
            'Events__c' => 'Events',
            'Hosting__c' => 'Hosting',
            'Tours__c' => 'Tours'
        };

        // Where field is in a set of fields? - only interested in the stop the post field?
        Map<Id, ContactHistory> individualHistory = new Map<Id, ContactHistory>([SELECT Id, ContactId, OldValue, NewValue, Field, CreatedById, CreatedDate FROM ContactHistory WHERE ContactId = :individual.Id AND Field IN :mapFieldLabelByDeveloperName.keySet() ORDER BY CreatedDate DESC ]);

        Set<Id> userIdSet = new Set<Id>();
        for (ContactHistory history : individualHistory.values()) {
            userIdSet.add(history.CreatedById);
        }
        Map<Id, User> mapUserById = new Map<Id, User>([SELECT Id, FirstName, LastName FROM User WHERE Id IN: userIdSet]);

        List<PreferenceHistoryService> individualFieldHistory = new List<PreferenceHistoryService>();

        for (Id historyId : individualHistory.keySet()) {
            ContactHistory history = individualHistory.get(historyId);

            PreferenceHistoryService fh = new PreferenceHistoryService();
            fh.user = mapUserById.get(history.CreatedById);
            fh.dt = populateTimeValue(history.CreatedDate);
            fh.field = populateFieldValue(history.Field, mapFieldLabelByDeveloperName);
            fh.oldValue = String.valueOf(history.OldValue);
            fh.newValue = String.valueOf(history.NewValue);

            individualFieldHistory.add(fh);

        }

        return individualFieldHistory;
    }

    public static List<PreferenceHistoryService> getMyPreferenceHistory(List<Channel_Preference__c> channelPreferences) {
        List<PreferenceHistoryService> preferenceHistoryList = new List<PreferenceHistoryService>();

        Map<String, String> mapFieldLabelByDeveloperName = new Map<String, String>{
            'created' => 'Created',
            'Active__c' => 'Active',
            'NotifyBySMS__c' => 'SMS',
            'DeliveryByEmail__c' => 'Email',
            'Fax__c' => 'Fax',
            'Phone__c' => 'Phone',
            'DeliveryMail__c' => 'Mail',
            'Location_Reference__c' => 'Location Reference'
        };

        Map<Id, Channel_Preference__c> channelPreferenceMap = new Map<Id, Channel_Preference__c>();
        channelPreferenceMap.putAll(channelPreferences);


        Set<Id> userIdSet = new Set<Id>();

        List<Channel_Preference__History> channelPreferenceHistory = new List<Channel_Preference__History>([SELECT Id, ParentId, OldValue, NewValue, Field, CreatedById, CreatedDate
                                                                                                            FROM Channel_Preference__History Channel_Preference__c
                                                                                                            WHERE ParentId IN: channelPreferenceMap.keySet()
                                                                                                            AND Field IN: mapFieldLabelByDeveloperName.keySet()
                                                                                                            ORDER BY CreatedDate DESC ]);

        for (Channel_Preference__History history : channelPreferenceHistory) {
            userIdSet.add(history.CreatedById);
        }

        Map<Id, User> mapUserById = new Map<Id, User>([SELECT Id, FirstName, LastName FROM User WHERE Id IN: userIdSet]);

        for (Channel_Preference__History history : channelPreferenceHistory) {
            Channel_Preference__c cp = channelPreferenceMap.get(history.ParentId);

            PreferenceHistoryService fh = new PreferenceHistoryService();
            fh.user = mapUserById.get(history.CreatedById);
            fh.dt = populateTimeValue(history.CreatedDate);
            fh.field = populateFieldValue(history.Field, mapFieldLabelByDeveloperName);
            fh.oldValue = String.valueOf(history.OldValue);
            fh.newValue = String.valueOf(history.NewValue);
            fh.setting = cp.Channel_Preference_Setting__r.Name;

            preferenceHistoryList.add(fh);

        }

        return preferenceHistoryList;

    }

    public static List<PreferenceHistoryService> getChannelPreferenceHistory(List<PreferenceService.IndividualPreferenceWrapper> individualPreferenceWrappers) {
        List<PreferenceHistoryService> channelPreferenceHistory = new List<PreferenceHistoryService>();

        Map<String, String> mapFieldLabelByDeveloperName = new Map<String, String>{
            'created' => 'Created',
            'Active__c' => 'Active',
            'NotifyBySMS__c' => 'SMS',
            'DeliveryByEmail__c' => 'Email',
            'Fax__c' => 'Fax',
            'Phone__c' => 'Phone',
            'DeliveryMail__c' => 'Mail',
            'Location_Reference__c' => 'Location Reference'
        };

        Map<Id, Channel_Preference__c> mapChannelPreferenceById = new Map<Id, Channel_Preference__c>();

        if (individualPreferenceWrappers != null) {
            for (PreferenceService.IndividualPreferenceWrapper ipw : individualPreferenceWrappers) {
                mapChannelPreferenceById.put(ipw.channelPreference.Id, ipw.channelPreference);
            }
        }

        Set<Id> userIdSet = new Set<Id>();

        Map<Id, Channel_Preference__History> mapChannelPreferenceHistoryById = new Map<Id, Channel_Preference__History>();

        for (Channel_Preference__History history : [SELECT Id, ParentId, OldValue, NewValue, Field, CreatedById, CreatedDate FROM Channel_Preference__History WHERE ParentId IN: mapChannelPreferenceById.keySet() AND Field IN :mapFieldLabelByDeveloperName.keySet() ORDER BY CreatedDate DESC ]) {
            userIdSet.add(history.CreatedById);
            mapChannelPreferenceHistoryById.put(history.Id, history);
        }

        Map<Id, User> mapUserById = new Map<Id, User>([SELECT Id, FirstName, LastName FROM User WHERE Id IN: userIdSet]);

        for (Id historyId : mapChannelPreferenceHistoryById.keySet()) {
            Channel_Preference__History history = mapChannelPreferenceHistoryById.get(historyId);
            Channel_Preference__c cp = mapChannelPreferenceById.get(history.ParentId);

            PreferenceHistoryService fh = new PreferenceHistoryService();
            fh.user = mapUserById.get(history.CreatedById);
            fh.dt = populateTimeValue(history.CreatedDate);
            fh.field = populateFieldValue(history.Field, mapFieldLabelByDeveloperName);
            fh.oldValue = String.valueOf(history.OldValue);
            fh.newValue = String.valueOf(history.NewValue);
            fh.setting = cp.Channel_Preference_Setting__r.Name;

            channelPreferenceHistory.add(fh);

        }

        return channelPreferenceHistory;
    }

    public static String populateFieldValue(String historyValue, Map<String, String> mapFieldLabelByDeveloperName) {
        if (String.valueOf(historyValue) == 'created') {
            return 'Created';
        } else {
            return mapFieldLabelByDeveloperName.get(String.valueOf(historyValue));
        }
    }

    public static Datetime populateTimeValue(Datetime createdDate) {
        TimeZone tz = UserInfo.getTimeZone();
        DateTime localTime = createdDate.AddSeconds(tz.getOffset(createdDate)/1000);
        return localTime;
    }

    public Integer compareTo (Object compareTo) {
        PreferenceHistoryService compareToObj = (PreferenceHistoryService) compareTo;
        Integer returnValue = 0;
        if(dt < compareToObj.dt) {
            returnValue = 1;
        } else if (dt > compareToObj.dt) {
            returnValue = -1;
        }
        return returnValue;
    }

}