public with sharing class ApprovalHistoryController {
    

    @AuraEnabled(cacheable=true)
    public static list<ProcessInstance> retriveProcessInstance(Id recordId) {

        String sObjName = recordId.getSObjectType().getDescribe().getName();
        List<Id> processInstanceIds = new List<Id>();
        // Get the ProcessInstance Id from the Non_Standard_Agreement__c
        if( sObjName.equals('Non_Standard_Agreement__c') ) {

            for (Non_Standard_Agreement__c a : [SELECT Id,(SELECT ID FROM ProcessInstances  ORDER BY CreatedDate DESC) 
                                                FROM Non_Standard_Agreement__c WHERE ID  =:recordId])
            {
                for(ProcessInstance pi :a.ProcessInstances)
                    processInstanceIds.add(pi.Id);
            }
        }else if(sObjName.equals('Case')){
            for (Case a : [SELECT Id,(SELECT ID FROM ProcessInstances  ORDER BY CreatedDate DESC) 
                                                FROM Case WHERE ID  = :recordId])
            {
                for(ProcessInstance pi :a.ProcessInstances)
                    processInstanceIds.add(pi.Id);
            }

        }
        
        // Now that we have the most recent process instances, we can check
        // the most recent process steps for history 
        List<ProcessInstance> processInstanceList = [SELECT TargetObjectId,CompletedDate, Status, LastActorId,LastActor.Name,
                    (SELECT Id, Actor.Name, OriginalActor.Name, Actor.UserRole.Name, StepStatus, CreatedDate, Comments, ProcessInstanceId 
                    FROM StepsAndWorkitems  
                    ORDER BY ID DESC), 
                    (SELECT Id, StepStatus, Comments FROM Steps ORDER BY CreatedDate DESC LIMIT 1 )
            FROM ProcessInstance 
            WHERE Id IN :processInstanceIds ORDER BY CreatedDate DESC] ;

        //This is for debugging purpose only
        for ( ProcessInstance pi : processInstanceList ) {
            if (pi.StepsAndWorkitems.size() > 0){
                for(ProcessInstanceHistory pih :pi.StepsAndWorkitems)
                    System.debug('Approver: ' + pih.OriginalActor.Name + ' Comment: ' + pih.Comments);
            }
        }
        return processInstanceList;
    }
}