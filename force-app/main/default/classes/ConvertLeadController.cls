public class ConvertLeadController {

    ApexPages.StandardController controller;
    public Lead myLead {get; set;}
    public Account myAccount {get; set;}

    public ConvertLeadController(ApexPages.StandardController stdController) {
        if (!Test.isRunningTest()) {
            this.controller = stdController;
            List<String> fields = new List<String>{'isConverted', 'Linked_Farm__r', 'Linked_Individual__r', 'Salutation', 'FirstName', 'MiddleName', 'LastName',
                                                    'Phone', 'MobilePhone', 'Email', 'Preferred_Contact_Method__c', 'Preferred_Contact_Time__c', 'Company',
                                                    'Supply_Region__c', 'Prospective_Farm_Id__c', 'Farm_Type__c', 'Description', 'Farm_Role__c',
                                                    'RecordType.Name', 'Current_Supplier__c', 'Competitor_Supply_Number__c', 'Specialty_Milk__c',
                                                    'Specialty_Milk_Notes__c', 'Specialty_Status__c', 'Specialty_Sub_Status__c'};
            this.controller.addFields(fields);
            this.myLead = (Lead)this.controller.getRecord();
        }
    }

    public PageReference convert(){
        PageReference returnPage = ApexPages.currentPage();
        Database.DMLOptions dmlParam = new Database.DMLOptions();
        dmlParam.DuplicateRuleHeader.allowSave = true;

        if (myLead.isConverted == false) {
            Boolean isAUFarm = myLead.RecordType.Name == 'Fonterra Lead AU';

            if (myLead.FirstName == null || myLead.Phone == null) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please ensure that the Individual First Name and Phone is set.'));
                return null;
            }

            if (myLead.Linked_Farm__c == null) {
                myAccount = createFarm(isAUFarm);
            } else {
                myAccount = myLead.Linked_Farm__r;
            }

            Database.LeadConvert lc = new Database.LeadConvert();

            lc.setLeadId(myLead.Id);
            lc.setAccountId(myAccount.Id);
            if (myLead.Linked_Individual__c != null) {
                updateContact(myLead.Linked_Individual__c, myAccount.Id);
                lc.setContactId(myLead.Linked_Individual__c);
            }
            lc.setOpportunityName(myLead.Company + '-' + myLead.FirstName + ' ' + myLead.LastName);
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);

            Database.LeadConvertResult lcr = Database.convertLead(lc, dmlParam);

            System.assert(lcr.isSuccess());

            updateOpp(lcr.getOpportunityId(), myLead.RecordType.Name, lcr.getContactId(), myLead);
            attachAddToOpp(lcr.getLeadId(), lcr.getOpportunityId());
            if (myLead.Linked_Individual__c != null) {
                updateContact(myLead.Linked_Individual__c, myAccount.Id);
            }
            updateOppContactRole(lcr.getContactId(), lcr.getOpportunityId());

            returnPage = new PageReference('/' + lcr.getOpportunityId());
            returnPage.setRedirect(true);
        }
        return returnPage;
    }

    public Account createFarm(Boolean isAUFarm) {
        Account newAccount = new Account();

        newAccount.Name = myLead.Prospective_Farm_Id__c;
        newAccount.Farm_Name__c = myLead.Company;
        newAccount.Farm_in_Milk_Supply__c = true;

        if (isAUFarm) {
            newAccount.Farm_Region__c = myLead.Supply_Region__c;
            newAccount.RecordTypeId = GlobalUtility.auAccountFarmRecordTypeId;
            newAccount.Specialty_Milk__c = myLead.Specialty_Milk__c;
            newAccount.Specialty_Milk_Notes__c = myLead.Specialty_Milk_Notes__c;
            newAccount.Specialty_Status__c = myLead.Specialty_Status__c;
            newAccount.Specialty_Sub_Status__c = myLead.Specialty_Sub_Status__c;
        }
        insert newAccount;
        return newAccount;
    }

    public void updateContact (Id contactId, Id accountId) {
        Contact newContact = [SELECT Id, AccountId From Contact WHERE Id = :contactId];

        newContact.AccountId = accountId;

        update newContact;
    }

    public void updateOpp(Id oppId, String ldRecordTypeName, Id contactId, Lead lead) {
        Opportunity newOpp = [SELECT RecordTypeId, Type, Description FROM Opportunity WHERE Id = :oppId];

        if (ldRecordTypeName == 'Fonterra Lead AU'){
            newOpp.RecordTypeId = GlobalUtility.oppFonterraAURecordTypeId;
            newOpp.Current_Processor__c = lead.Current_Supplier__c;
            newOpp.Current_Processor_s_Supplier_Number__c = lead.Competitor_Supply_Number__c;
            newOpp.Specialty_Milk__c = myLead.Specialty_Milk__c;
            newOpp.Specialty_Milk_Notes__c = myLead.Specialty_Milk_Notes__c;
            newOpp.Specialty_Status__c = myLead.Specialty_Status__c;
            newOpp.Specialty_Sub_Status__c = myLead.Specialty_Sub_Status__c;

        }

        newOpp.Type = myLead.Farm_Type__c;
        newOpp.Description = myLead.Description;
        newOpp.Individual__c = contactId;
        update newOpp;
    }

    public void attachAddToOpp(Id leadId, Id opportunityId) {

        List<Address__c> addressList    = [SELECT Id FROM Address__c WHERE Lead__c = :leadId];
        List<Address__c> updateList  = new List<Address__c>();

        for (Address__c addr : addressList) {
            addr.Lead__c = null;
            addr.Opportunity__c = opportunityId;
            updateList.add(addr);
        }
        update updateList;
    }

    public void updateOppContactRole(Id contactId, Id opportunityId) {
        OpportunityContactRole newOCR = [SELECT Role FROM OpportunityContactRole
                                            WHERE OpportunityId = :opportunityId AND ContactId = :contactId AND IsPrimary = true];

        newOCR.Role = myLead.Farm_Role__c;

        update newOCR;
    }
}