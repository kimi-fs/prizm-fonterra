/**
 * AMSSearchControllerTest
 *
 * Controller for testing the AMSSearchController
 *
 * Author: Ed (Trineo)
 * Date: July 2017
 */
@isTest
private class AMSSearchControllerTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup
    static void testSetup() {
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
    }

    @isTest static void testMenuBarSearch() {

        System.runAs(getCommunityUser()) {
            PageReference pageref = AMSSearchController.search('Something');
            System.assert(pageref != null);
            System.assertEquals('Something', pageref.getParameters().get(AMSSearchController.getSearchParameterName()));
        }
    }

    @isTest static void testSearchResults() {
        List<Knowledge__kav> newsArticles = TestClassDataUtil.createAMSNewsArticle(20);
        List<Knowledge__kav> adviceArticles = TestClassDataUtil.createAMSAdviceArticle(20);
        List<Id> newsIds = new List<Id>(new Map<Id, Knowledge__kav>(newsArticles).keySet());
        List<Id> adviceIds = new List<Id>(new Map<Id, Knowledge__kav>(adviceArticles).keySet());
        List<Id> allIds = new List<Id>();
        allIds.addAll(newsIds);
        allIds.addAll(adviceIds);

        System.runAs(getCommunityUser()) {
            PageReference pageref = AMSSearchController.search('Summary');
            System.Test.setCurrentPage(pageRef);

            Test.startTest();

            Test.setFixedSearchResults(allIds);

            AMSSearchController controller = new AMSSearchController();

            controller.performSearch();

            // Check how many results we have
            System.assertEquals(40 / AMSSearchController.PAGE_SIZE, controller.numberOfPages);

            // We should be on page 1
            System.assertEquals(1, controller.pageNumber);

            // Should have a full page of results
            System.assertEquals(AMSSearchController.PAGE_SIZE, controller.pageResults.size());

            List<AMSSearchController.PaginationLink> links = controller.getPaginationLinks();

            System.assertNotEquals(null, links);

            System.assertEquals(1, controller.getCurrentPageNumber());

            Test.stopTest();
        }
    }

    @isTest static void testFAQSearch() {
        TestClassDataUtil.createAMSFAQArticles(1);

        System.runAs(getCommunityUser()) {
            Knowledge__kav faqArticle = [
                                          SELECT Id,
                                          Title,
                                          UrlName,
                                          Summary
                                          FROM Knowledge__kav
                                          WHERE PublishStatus = 'Online'
                                            AND RecordTypeId = :GlobalUtility.amsFAQRecordTypeId
                                            AND Language = 'en_US'
                                      ];

            Test.startTest();
            // We do a SOSL search - so hard code the return values
            Test.setFixedSearchResults(new List<Id> {faqArticle.Id});

            PageReference pageRef = Page.AMSAdvice;
            System.Test.setCurrentPage(pageRef);

            AMSAdviceController adviceController = new AMSAdviceController();
            AMSSearchController searchController = new AMSSearchController(adviceController);

            System.assertEquals(null, searchController.pageResults);

            searchController.searchString = 'Test';

            searchController.performSearch();

            System.assertNotEquals(null, searchController.pageResults);

            System.assertEquals(1, searchController.pageResults.size());

            searchController.clearSearch();

            System.assertEquals(null, searchController.pageResults);

            Test.stopTest();
        }

    }

    private static User getCommunityUser() {
        User theUser = [
                           SELECT Id
                           FROM User
                           WHERE Email = :USER_EMAIL
                       ];

        return theUser;
    }

}