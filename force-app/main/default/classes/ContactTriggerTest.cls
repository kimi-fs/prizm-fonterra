/**
* Description: Test class for ContactTrigger
* @author: Salman Zafar (Davanti Consulting)
* @date: 14 May 2015
*/
@isTest
private class ContactTriggerTest {

    private static Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();

    private static Id derivedRelationshipRecordTypeId = GlobalUtility.derivedRelationshipRecordTypeId;

    @isTest static void test_method_one() {

        Test.startTest();
            Contact newContact = TestClassDataUtil.createIndividualAU('Bruce', 'Wayne', false);
            newContact.AccountId = TestClassDataUtil.createSingleAccountFarm().Id;
            newContact.Individual_MSCRM__c = 'test123';

            system.runAs(TestClassDataUtil.createIntegrationUser()){
                insert newContact;
                newContact.Individual_MSCRM__c = null;
                update newContact;

                newContact.Individual_MSCRM__c = '123123';
                update newContact;
            }
        Test.stopTest();
    }

    @isTest static void copyCommunityContactInfoToUserTest() {

        Contact theContact = TestClassDataUtil.createIndividualAU(false);
        theContact.AccountId = TestClassDataUtil.createSingleAccountFarm().Id;
        theContact.MobilePhone = '+61406111000';
        insert theContact;

        TestClassDataUtil.createAMSCommunityUserAndIndividual(theContact);

        User theUser = [SELECT Id, FirstName, MobilePhone, Contact.MobilePhone FROM User WHERE ContactId = :theContact.Id];
        System.assertEquals('+61406111000', theUser.MobilePhone);
        System.assertEquals('+61406111000', theUser.Contact.MobilePhone);

        System.runAs(TestClassDataUtil.createIntegrationUser()){
            Test.startTest();
                theContact.MobilePhone = '+61406222000';
                update theContact;
            Test.stopTest();
        }

        User theUser2 = [SELECT Id, FirstName, MobilePhone, Contact.MobilePhone FROM User WHERE ContactId = :theContact.Id];
        System.assertEquals('+61406222000', theUser2.MobilePhone);
        System.assertEquals('+61406222000', theUser2.Contact.MobilePhone);
    }

    @isTest static void contactMergeChannelPreferencesWithSettingsTest(){

        Account f1 = TestClassDataUtil.createSingleAccountFarm();
        Contact c1 = TestClassDataUtil.createIndividualAU('Bruce', 'Wayne', true);
        Contact c2 = TestClassDataUtil.createIndividualAU('Bruce', 'Wayne', true);

        Individual_Relationship__c ir = new Individual_Relationship__c(Individual__c = c1.Id, Farm__c = f1.Id, RecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Farm').getRecordTypeId());
        insert ir;

        Channel_Preference_Setting__c cycleChangePreferenceSetting = new Channel_Preference_Setting__c(
            Name = 'Cycle Change',
            Active__c = true,
            CME_Message_Type_Id__c = 'xxxx',
            DeliveryMail__c = 'N/A',
            DeliveryByEmail__c = 'Enabled',
            Type__c = 'Personal'
        );

        Channel_Preference_Setting__c farmEventsPreferenceSetting = new Channel_Preference_Setting__c(
            Name = 'Farm Events',
            Active__c = true,
            CME_Message_Type_Id__c = 'yyyy',
            DeliveryMail__c = 'N/A',
            DeliveryByEmail__c = 'Enabled',
            Type__c = 'Personal'
        );

        insert cycleChangePreferenceSetting;
        insert farmEventsPreferenceSetting;

        //create channel preferences for each contact, each contact should have one that needs to be kept
        Channel_Preference__c cp1WithSetting = new Channel_Preference__c(Contact_ID__c = c1.Id, DeliveryByEmail__c = true, Channel_Preference_Setting__c = cycleChangePreferenceSetting.Id);
        Channel_Preference__c cp1WithOutSetting = new Channel_Preference__c(Contact_ID__c = c2.Id, DeliveryByEmail__c = false, Channel_Preference_Setting__c = cycleChangePreferenceSetting.Id);
        insert cp1WithSetting;
        insert cp1WithOutSetting;

        Channel_Preference__c cp2WithSetting = new Channel_Preference__c(Contact_ID__c = c2.Id, DeliveryByEmail__c = true, Channel_Preference_Setting__c = farmEventsPreferenceSetting.Id);
        Channel_Preference__c cp2WithOutSetting = new Channel_Preference__c(Contact_ID__c = c1.Id, DeliveryByEmail__c = false, Channel_Preference_Setting__c = farmEventsPreferenceSetting.Id);
        insert cp2WithSetting;
        insert cp2WithOutSetting;

        //check we start with 4 channel preferences
        List<Channel_Preference__c> cps = [SELECT Id, Contact_ID__c, DeliveryByEmail__c FROM Channel_Preference__c];
        System.assertEquals(4, cps.size());

        //merge the contacts
        Test.startTest();
            Database.MergeResult results = Database.merge(c1, c2);
        Test.stopTest();

        //check we only have 1 contact left
        List<Contact> contacts = [SELECT Id FROM Contact];
        System.assertEquals(1, contacts.size());
        System.assertEquals(c1.Id, contacts[0].Id);

        //check 2 of the channel preferences were deleted
        List<Channel_Preference__c> cps2 = [SELECT Id, Contact_ID__c, DeliveryByEmail__c FROM Channel_Preference__c];
        System.assertEquals(2, cps2.size());
        System.assertEquals(c1.Id, cps2[0].Contact_ID__c);
        System.assertEquals(c1.Id, cps2[1].Contact_ID__c);

        //check the remaining channel preferences are the ones that had settings
        Set<Id> remainingCPId = new Set<Id>{cp1WithSetting.Id, cp2WithSetting.Id};
        System.assertEquals(true, remainingCPId.contains(cps2[0].Id));
        System.assertEquals(true, remainingCPId.contains(cps2[1].Id));
    }

    @isTest static void contactMergeOldestChannelPreferencesTest(){

        Account f1 = TestClassDataUtil.createSingleAccountFarm();
        Contact c1 = TestClassDataUtil.createIndividualAU('Bruce', 'Wayne', true);
        Contact c2 = TestClassDataUtil.createIndividualAU('Bruce', 'Wayne', true);
        Contact c3 = TestClassDataUtil.createIndividualAU('Bruce', 'Wayne', true);

        Individual_Relationship__c ir = new Individual_Relationship__c(Individual__c = c1.Id, Farm__c = f1.Id, RecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Farm').getRecordTypeId());
        insert ir;

        Channel_Preference_Setting__c cycleChangePreferenceSetting = new Channel_Preference_Setting__c(
            Name = 'Cycle Change',
            Active__c = true,
            CME_Message_Type_Id__c = 'xxxx',
            DeliveryMail__c = 'N/A',
            DeliveryByEmail__c = 'Enabled',
            Type__c = 'Personal'
        );

        Channel_Preference_Setting__c farmEventsPreferenceSetting = new Channel_Preference_Setting__c(
            Name = 'Farm Events',
            Active__c = true,
            CME_Message_Type_Id__c = 'yyyy',
            DeliveryMail__c = 'N/A',
            DeliveryByEmail__c = 'Enabled',
            Type__c = 'Personal'
        );

        insert cycleChangePreferenceSetting;
        insert farmEventsPreferenceSetting;

        //create channel preferences for each contact, each contact should have one that needs to be kept
        Channel_Preference__c cp1OldestSetting = new Channel_Preference__c(CreatedDate = System.now().addDays(-1), Contact_ID__c = c1.Id, Channel_Preference_Setting__c = cycleChangePreferenceSetting.Id, NotifyBySMS__c = true);
        Channel_Preference__c cp1NewestSetting = new Channel_Preference__c(Contact_ID__c = c2.Id, Channel_Preference_Setting__c = cycleChangePreferenceSetting.Id);
        Channel_Preference__c cp1NewestSetting2 = new Channel_Preference__c(Contact_ID__c = c3.Id, Channel_Preference_Setting__c = cycleChangePreferenceSetting.Id);
        insert cp1OldestSetting;
        insert cp1NewestSetting;
        insert cp1NewestSetting2;

        Channel_Preference__c cp2OldestSetting = new Channel_Preference__c(CreatedDate = System.now().addDays(-1), Contact_ID__c = c2.Id, Channel_Preference_Setting__c = farmEventsPreferenceSetting.Id);
        Channel_Preference__c cp2NewestSetting = new Channel_Preference__c(Contact_ID__c = c1.Id, Channel_Preference_Setting__c = farmEventsPreferenceSetting.Id);
        insert cp2OldestSetting;
        insert cp2NewestSetting;

        //check we start with 4 channel preferences
        List<Channel_Preference__c> cps = [SELECT Id, Contact_ID__c, DeliveryByEmail__c FROM Channel_Preference__c];
        System.assertEquals(5, cps.size());

        //merge the contacts
        Test.startTest();
            List<Database.MergeResult> results = Database.merge(c1, new List<Contact>{c2, c3});
        Test.stopTest();

        //check we only have 1 contact left
        List<Contact> contacts = [SELECT Id FROM Contact];
        System.assertEquals(1, contacts.size());
        System.assertEquals(c1.Id, contacts[0].Id);

        //check 2 of the channel preferences were deleted
        List<Channel_Preference__c> cps2 = [SELECT Id, Contact_ID__c, DeliveryByEmail__c FROM Channel_Preference__c];
        System.assertEquals(2, cps2.size());
        System.assertEquals(c1.Id, cps2[0].Contact_ID__c);
        System.assertEquals(c1.Id, cps2[1].Contact_ID__c);

        //check the remaining channel preferences are the oldest ones
        Set<Id> remainingCPId = new Set<Id>{cp1OldestSetting.Id, cp2OldestSetting.Id};
        System.assertEquals(true, remainingCPId.contains(cps2[0].Id));
        System.assertEquals(true, remainingCPId.contains(cps2[1].Id));
    }

}