public class AMSCommunityUtil {

    public static final String APPLE_STORE_DEVELOPERNAME = 'Apple_Store';
    public static final String GOOGLE_STORE_DEVELOPERNAME = 'Google_Store';
    public static final String WOOLIES_REWARDS_TNC_DEVELOPERNAME = 'Woolies_TnC';
    public static final String RITCHIES_REWARDS_TNC_DEVELOPERNAME = 'Ritchies_TnC';

    public static final String APP_LINKS_PAGE = 'AMSLanding';
    public static final String DETAILS_LINKS_PAGE = 'AMSMyDetails';

    public static final String LOGIN_AS_COOKIE = 'AMSCommunityIndividual';

    // Sets the current user if part of community based on logged in user
    public static void setCurrentUserAndContact(User currentUser, Contact individual) {
        if (System.Url.getSalesforceBaseUrl().toExternalForm().contains('visual.force')) {
            currentUser = null;
            individual = null;
        } else {
            currentUser = [
                SELECT Id,
                    ContactId,
                    Name,
                    Profile.Name
                FROM User
                WHERE Id = :UserInfo.getUserId()
            ];
            individual = [
                SELECT Id, MS_Individual_ID__c
                FROM Contact
                WHERE Id = :currentUser.ContactId
                LIMIT 1
            ];
        }
    }

    public static Map<String, String> initialisePageMessages(String page) {
        Map<String, String> messages = ExceptionService.getPageMessages(page);
        if (messages == null) {
            messages = new Map<String, String>();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'No messages configured'));
        }
        return messages;
    }

    public static SelectOption createFirstSelectOption(String value) {
        return new SelectOption(value, 'Please Select');
    }

    public static SelectOption createFirstSelectOption() {
        return createFirstSelectOption('');
    }

    public static String pageReferenceToCommunityFriendlyString(PageReference pageReference) {
        return pageReference.getUrl().removeStart('/apex');
    }

    public static String getPageLink(String pageName, String linkName) {
        String link;

        Map<String, CommunityPageLinks__c> pageLinks = CommunityPageLinks__c.getAll();
        if (pageLinks.containsKey(linkName) && pageLinks.get(linkName).Page__c == pageName) {
            link = pageLinks.get(linkName).Url__c;
        }
        return link;
    }

    public static String getAppleStoreLink() {
        return getPageLink(APP_LINKS_PAGE, APPLE_STORE_DEVELOPERNAME);
    }

    public static String getGoogleStoreLink() {
        return getPageLink(APP_LINKS_PAGE, GOOGLE_STORE_DEVELOPERNAME);
    }

    public static String getWooliesRewardsTermsAndConditionsLink() {
        return getPageLink(DETAILS_LINKS_PAGE, WOOLIES_REWARDS_TNC_DEVELOPERNAME);
    }

    public static String getRitchiesRewardsTermsAndConditionsLink() {
        return getPageLink(DETAILS_LINKS_PAGE, RITCHIES_REWARDS_TNC_DEVELOPERNAME);
    }

    public static String getArticleParameterName() {
        return 'UrlName';
    }

    public static String getArticleCategoryParameterName() {
        return 'category';
    }

    public static String getFAQParameterName() {
        return 'Faq';
    }

    public static String getPageNumberParameterName() {
        return 'PageNumber';
    }

    public static String getUserIdParameterName() {
        return 'u';
    }

    public static String getSearchParameterName() {
        return 'search';
    }

    public static String getStartURLParameterName() {
        return 'startURL';
    }

    public static String getFarmIdParameterName() {
        return 'farmId';
    }

    public static String getIndividualIdParameterName() {
        return 'individual';
    }

    public static String getGoalIdParameterName() {
        return 'goalId';
    }

    public static String getActionIdParameterName() {
        return 'actionId';
    }

    public enum FieldToSort {
        Label, Value
    }

    //https://github.com/abhinavguptas/Apex-Select-Option-Sorting/blob/master/src/classes/SelectOptionSorter.cls
    public static void sortSelectOptionList(List<SelectOption> opts, FieldToSort sortField) {
        Map<String, SelectOption> mapping = new Map<String, SelectOption>();
        // Suffix to avoid duplicate values like same labels or values are in inbound list
        Integer suffix = 1;

        for (SelectOption opt : opts) {
            if (sortField == FieldToSort.Label) {
                mapping.put(    // Done this cryptic to save scriptlines, if this loop executes 10000 times
                                // it would every script statement would add 1, so 3 would lead to 30000.
                             (opt.getLabel() + suffix++), // Key using Label + Suffix Counter
                             opt);
            } else {
                mapping.put(
                             (opt.getValue() + suffix++), // Key using Label + Suffix Counter
                             opt);
            }
        }

        List<String> sortKeys = new List<String>();
        sortKeys.addAll(mapping.keySet());
        sortKeys.sort();
        // clear the original collection to rebuilt it
        opts.clear();

        for (String key : sortKeys) {
            opts.add(mapping.get(key));
        }
    }

    private static final String encodeKeyString = 'a264c914116a40e1';

    public static String encryptString(String value) {
        Blob encodeKey = Blob.valueOf(encodeKeyString);
        Blob cipherText = Crypto.encryptWithManagedIV('AES128', encodeKey, Blob.valueOf(value));
        String encodedValue = EncodingUtil.base64Encode(cipherText);
        return encodedValue;
    }
}