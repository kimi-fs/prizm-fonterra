/*------------------------------------------------------------
Author:			Sean Soriano
Company:		Davanti Consulting
Description:	Scheduler Class for BatchSetReferencePeriodStatus Batch Class
History
12/01/2015		Sean Soriano	Created
------------------------------------------------------------*/
global class ScheduleBatchSetReferencePeriodStatus implements Schedulable, Database.Batchable<SObject> {

    global static void schedule() {
        String expression = ScheduleService.getCRONExpression('ScheduleBatchSetReferencePeriodStatus');
        String jobName = ScheduleService.getCRONJobName('ScheduleBatchSetReferencePeriodStatus');
        if (String.isNotBlank(expression)) {
            final ScheduleBatchSetReferencePeriodStatus job = new ScheduleBatchSetReferencePeriodStatus();
            System.schedule(jobName, expression, job);
        }
    }

	global void execute(SchedulableContext sc) {
		ScheduleBatchSetReferencePeriodStatus bsrp = new ScheduleBatchSetReferencePeriodStatus();
		database.executebatch(bsrp);
    }


    String query = 'SELECT Id, Status__c, Start_Date__c,End_Date__c FROM Reference_Period__c';

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Date dtToday = system.today();

        List<Reference_Period__c> lstRefPeriodsForUpdate = new List<Reference_Period__c>();

        for(sObject so : scope) {
            Reference_Period__c rp = (Reference_Period__c)so;

            //Check dates for current status
            if(dtToday >= rp.Start_Date__c && (dtToday <= rp.End_Date__c || rp.End_Date__c == null)) {
                if(rp.Status__c != 'Current'){
                    rp.Status__c = 'Current';
                    lstRefPeriodsForUpdate.add(rp);
                }
            //Check dates for past status
            } else if(dtToday > rp.Start_Date__c && dtToday > rp.End_Date__c && rp.End_Date__c != null) {
                if(rp.Status__c != 'Past'){
                    rp.Status__c = 'Past';
                    lstRefPeriodsForUpdate.add(rp);
                }
            //Check dates for future status
            } else if(dtToday < rp.Start_Date__c && (dtToday < rp.End_Date__c || rp.End_Date__c == null)) {
                if(rp.Status__c != 'Future'){
                    rp.Status__c = 'Future';
                    lstRefPeriodsForUpdate.add(rp);
                }
            }

        }

        if(!lstRefPeriodsForUpdate.isEmpty()) {
            system.debug('##Number of Reference Periods for Update: ' + lstRefPeriodsForUpdate.size());


            Database.SaveResult[] srList = Database.update(lstRefPeriodsForUpdate);

            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('##Successfully updated Reference. Reference ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('##The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('##Reference fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }

    global void finish(Database.BatchableContext BC) {

    }
}