/**
 * Description: Test class for PreferenceHistoryController
 * @author: Jayson (Trineo)
 * @date: August 2018
 */
@isTest
public with sharing class PreferenceHistoryControllerTest {
    public static final String LASTNAME = 'PreferenceHistoryControllerTest';

    @testSetup static void preferenceHistoryControllerTestData() {

        TestClassDataUtil.individualDefaultAccountAU();

        Contact individual = TestClassDataUtil.createIndividualAU('Bob', LASTNAME, true);
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(individual);

        List<Channel_Preference_Setting__c> channelPreferenceSettings = TestClassDataUtil.createAuChannelPreferenceSettings();

        insert TestClassDataUtil.createChannelPreference(channelPreferenceSettings, individual.Id);
    }

    @isTest static void preferenceHistoryControllerPersonal() {
        User u = [SELECT Id, ContactId FROM User WHERE LastName = :LASTNAME LIMIT 1];
        Contact individual = [SELECT Id FROM Contact WHERE Id = :u.ContactId LIMIT 1];

        PageReference page = Page.PreferenceHistory;
        page.getParameters().put('cid', u.ContactId);
        page.getParameters().put('scope', 'personal');
        page.getParameters().put('sort', 'user');
        test.setCurrentpage(page);
        ApexPages.StandardController scon = new ApexPages.StandardController(individual);
        PreferenceHistoryController controller = new PreferenceHistoryController(scon);
        //no available history in test classes
        System.assert(controller.myPreferencesChannelPreferenceHistory.isEmpty());
    }

    @isTest static void preferenceHistoryControllerFarm() {
        User u = [SELECT Id, ContactId FROM User WHERE LastName = :LASTNAME LIMIT 1];
        Contact individual = [SELECT Id FROM Contact WHERE Id = :u.ContactId LIMIT 1];

        PageReference page = Page.PreferenceHistory;
        page.getParameters().put('cid', u.ContactId);
        page.getParameters().put('scope', 'farm');
        page.getParameters().put('sort', 'cps');

        test.setCurrentpage(page);
        ApexPages.StandardController scon = new ApexPages.StandardController(individual);
        PreferenceHistoryController controller = new PreferenceHistoryController(scon);
        //no available history in test classes
        System.assert(controller.myFarmPreferenceHistory.isEmpty());

    }

}