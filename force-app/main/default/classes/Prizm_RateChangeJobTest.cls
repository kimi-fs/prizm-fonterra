@isTest
public class Prizm_RateChangeJobTest {
    
    @testSetup
    public static void createTestData(){
        
        try{
            //create permissions
            fsCore__User_Permission__c userPrm = fsCore.TestHelperSystem.getTestUserPermission();
            insert userPrm;
            System.assert(userPrm.Id != null);
            
            //Lending Contract and setup
            fsServ.TestHelperGlobal.createSetupData();
        
            fscore__Company_Setup__c testCompany = 
                [Select Id
                 , Name 
                 from fscore__Company_Setup__c Limit 1];
            
            fsCore__Branch_Setup__c testBranch = fsCore.TestHelperCompany.getTestBranchSetup('Test Branch', 'EST', testCompany.Id, null, null);
            insert testBranch;
            System.assert(testBranch.Id != null, 'Test branch created assert');
            
            
            fsServ.TestHelperGlobal.createLendingContracts(false, false);
            fsServ__Lending_Contract__c contract =
                [Select Id
                 , name
                 , fsServ__Is_Active__c
                 , fsServ__Branch_Code__c
                 , fsServ__Contract_Status__c 
                 , fsServ__Contract_Template_Name__c
                 from fsServ__Lending_Contract__c 
                 where Name = 'AT1701000002'];  
            
            
            fsCore__Contract_Template_Setup__c testContractTemplate = 
                [Select Id
                 , Name
                 , fsCore__Index_Name__c 
                 from fsCore__Contract_Template_Setup__c
                 where Id =: contract.fsServ__Contract_Template_Name__c];
            
            System.debug(loggingLevel.ERROR, 'Contract template : '+testContractTemplate);
            System.debug(loggingLevel.ERROR, 'Contract template Index : '+testContractTemplate.fsCore__Index_Name__c);
            

           fsCore__Index_Setup__c testIndexSetUp = fsCore.TestHelperProduct.getTestIndexSetup(
           'Test Index Set Up'
           );
           testIndexSetUp.fsCore__Is_Active__c = false;
           testIndexSetUp.fsCore__Index_Code__c = 'TIS';
           insert testIndexSetUp;
            
            Date bizDate = System.Today();
            
            fsCore__Index_Rate_Setup__c testIndexRateSetUp = fsCore.TestHelperProduct.getTestIndexRateSetup(
                testIndexSetUp.id
                , 4.45
                , bizDate);
            testIndexRateSetUp.Is_Already_Picked__c = true;
            testIndexRateSetUp.Job_Id__c = 'Picked';
            testIndexRateSetUp.fsCore__Start_Date__c = Date.newInstance(2021, 6, 1);
            testIndexRateSetUp.fsCore__End_Date__c = Date.newInstance(2021, 7, 31);
            insert testIndexRateSetUp;
            
            fsCore__Index_Rate_Setup__c testIndexRateSetUpOne = fsCore.TestHelperProduct.getTestIndexRateSetup(
                testIndexSetUp.id
                , 3.00
                , bizDate);
            testIndexRateSetUpOne.Is_Already_Picked__c = false;
            testIndexRateSetUpOne.Job_Id__c = null;
            testIndexRateSetUpOne.fsCore__Start_Date__c = Date.newInstance(2021, 8, 1);
            insert testIndexRateSetUpOne;
            
            
            testIndexSetUp.fsCore__Is_Active__c = true;
            update testIndexSetUp;
            
            testContractTemplate.fsCore__Index_Name__c = testIndexSetUp.id;
            update testContractTemplate;
            
            System.debug(loggingLevel.ERROR, 'contract template index after application : '+ testContractTemplate.fsCore__Index_Name__c );
            
            System.debug(loggingLevel.ERROR,'Contract : '+contract);
            
            testBranch =  
                [Select Id
                 , Name
                 , fsCore__Business_Date__c  
                 from fsCore__Branch_Setup__c
                 where fsCore__Branch_Code__c =: contract.fsServ__Branch_Code__c];
            
            testBranch.fsCore__Business_Date__c = Date.today();
            update testBranch;
            
        }catch(Exception e){
            System.debug(e.getMessage());
        }
        
    }
    
    @isTest
    private static void testRateChangeJob(){
        fsServ__Lending_Contract__c contract =
            [Select Id
             , name
             , fsServ__Is_Active__c
             , fsServ__Branch_Code__c
             , fsServ__Contract_Status__c
             , Index_Rate_Setup__c
             from fsServ__Lending_Contract__c 
             where Name = 'AT1701000002']; 
         System.debug(loggingLevel.ERROR, 'CONTRACT : '+contract.Index_Rate_Setup__c);
        
        Set<Id> contractToBeActivated = new Set<Id>();
        contractToBeActivated.add(contract.Id);        
        fsCore.ActionInput actionIP = new fsCore.ActionInput();
        actionIP.addRecords(contractToBeActivated);
        fsServ.ContractActivator activateAction = new fsServ.ContractActivator();
        activateAction.setInput(actionIP);
        activateAction.process();
   
        fsServ__Lending_Contract__c activeContract  =
            [Select Id
             , name
             , fsServ__Is_Active__c 
             , fsServ__Current_Payment_Cycle__c
             , fsServ__Contract_Payment_Cycle__c
             from fsServ__Lending_Contract__c 
             where Id =: contract.Id];
        
        System.assertEquals(fsCore.Constants.CYCLE_MONTHLY,activeContract.fsServ__Current_Payment_Cycle__c, 'Current payment cycle');
        System.assertEquals(fsCore.Constants.CYCLE_MONTHLY,activeContract.fsServ__Contract_Payment_Cycle__c, 'Contract payment cycle');
        System.debug(loggingLevel.ERROR, 'Active Contracts status : '+activeContract.fsServ__Is_Active__c);
        
        Test.startTest();
        Id jobId = Prizm_IndexRateChangeController.executeRateChangeJob();
        Test.stopTest();
        
        System.assert(jobId != null, 'Job not executed');
        
         fsServ__Lending_Contract__c updatedContract =
            [Select Id
             , name
             , Index_Rate_Setup__c
             from fsServ__Lending_Contract__c 
             where Name = 'AT1701000002']; 

        fsCore__Index_Rate_Setup__c testIndexUpdated = 
            [Select Id
             , Name
             , Is_Already_Picked__c
             , Job_Id__c
             , fsCore__Is_Active__c
             , fsCore__Start_Date__c
             from fsCore__Index_Rate_Setup__c
             where Id =: updatedContract.Index_Rate_Setup__c];        
        
        System.assertEquals(true,testIndexUpdated.Is_Already_Picked__c,'Test Index Picked for new rate queued transaction assert');
        System.assert(testIndexUpdated.Job_Id__c!=null);
        
        fsServ__Transaction_Processing_Queue__c testQueuedTransaction =
            [Select Id
             , Name
             , fsServ__Transaction_Code__c
             , fsServ__Transaction_Date__c
             , fsServ__Processing_Status__c
             from fsServ__Transaction_Processing_Queue__c
             where fsServ__Lending_Contract_Number__c =: contract.Id];
        
        System.debug(loggingLevel.ERROR, 'Transaction Queue : '+testQueuedTransaction);
        System.assert(testQueuedTransaction.Id!=null);
        System.assertEquals('RATE_CHANGED', testQueuedTransaction.fsServ__Transaction_Code__c, 'Rate Change Transaction Created assert');
        System.assertEquals('Ready', testQueuedTransaction.fsServ__Processing_Status__c, 'Rate Change Transaction status created assert');
        System.assertEquals(testIndexUpdated.fsCore__Start_Date__c, testQueuedTransaction.fsServ__Transaction_Date__c, 'Queued Transaction created on index rate start date assert');

   
        Prizm_VoidQueueTransactionController.voidQueuedTransaction(updatedContract.Index_Rate_Setup__c);
        
        
        fsServ__Transaction_Processing_Queue__c updatedTestQueuedTransaction =
            [Select Id
             , Name
             , fsServ__Transaction_Code__c
             , fsServ__Transaction_Date__c
             , fsServ__Processing_Status__c
             from fsServ__Transaction_Processing_Queue__c
             where fsServ__Lending_Contract_Number__c =: contract.Id];
        
         fsServ__Lending_Contract__c updatedRateOnContract =
            [Select Id
             , name
             , Index_Rate_Setup__c
             , fsServ__Contract_Date__c
             from fsServ__Lending_Contract__c 
             where Name = 'AT1701000002']; 
        
        fsCore__Index_Rate_Setup__c updatedIndexRate = 
            [Select Id
             , Name
             , Is_Already_Picked__c
             , Job_Id__c
             from fsCore__Index_Rate_Setup__c
             where Id =: updatedContract.Index_Rate_Setup__c];   
        
        
        System.debug(loggingLevel.ERROR, 'Transaction Queue : '+updatedTestQueuedTransaction);
        System.assert(updatedTestQueuedTransaction.Id!=null);
        System.assertEquals('Void', updatedTestQueuedTransaction.fsServ__Processing_Status__c, 'Rate Change Transaction status created assert');
        System.assertEquals(null, updatedRateOnContract.Index_Rate_Setup__c, 'Index Rate field Cleared on contract after voiding the transaction assert');
        System.assertEquals(false, updatedIndexRate.Is_Already_Picked__c, 'Is Already picked field cleared on index rate after voiding the transaction assert');

        
    }
    
    
    
     @isTest
    private static void testRateChangeBatchJobError(){
        fsServ__Lending_Contract__c contract =
            [Select Id
             , name
             , fsServ__Is_Active__c 
             from fsServ__Lending_Contract__c Limit 1];
        
        Exception e = new DMLException();
		e.setMessage('Error while updating record');
		Test.startTest();
        
		List<fsCore.ErrorObject> errorObjectList = new List<fsCore.ErrorObject>();
		Prizm_RateChangeJob job = new Prizm_RateChangeJob();
		errorObjectList.add(Prizm_RateChangeUtil.getErrorObject(e, null));
		job.insertErrorLog(errorObjectList);
        
		Test.stopTest();
        
		List<fscore__Diagnostic_Log__c> diagnosticLogs = [
			SELECT id, fsCore__Related_Record_Id__c, fsCore__Message_Text__c, CreatedDate
			FROM fscore__Diagnostic_Log__c
		];
        
		System.assertEquals(
			'Error while updating record',
			diagnosticLogs[0].fsCore__Message_Text__c,
			'Error log not correctly captured'
		);        
    }
    
    
    
     @isTest
    private static void testRateChangeJobOne(){
        fsServ__Lending_Contract__c contract =
            [Select Id
             , name
             , fsServ__Is_Active__c
             , fsServ__Branch_Code__c
             , fsServ__Contract_Status__c
             , Index_Rate_Setup__c
             from fsServ__Lending_Contract__c 
             where Name = 'AT1701000002']; 
        
        contract.fsServ__Contract_Date__c = System.today();
        update contract;
    
        
        Set<Id> contractToBeActivated = new Set<Id>();
        contractToBeActivated.add(contract.Id);        
        fsCore.ActionInput actionIP = new fsCore.ActionInput();
        actionIP.addRecords(contractToBeActivated);
        fsServ.ContractActivator activateAction = new fsServ.ContractActivator();
        activateAction.setInput(actionIP);
        activateAction.process();
   
        fsServ__Lending_Contract__c activeContract  =
            [Select Id
             , name
             , fsServ__Is_Active__c 
             , fsServ__Current_Payment_Cycle__c
             , fsServ__Contract_Payment_Cycle__c
             , fsServ__Current_Payment_Due_Day__c
             , fsServ__Current_Payment_Due_Day_2__c 
             from fsServ__Lending_Contract__c 
             where Id =: contract.Id];
        
        System.assertEquals(fsCore.Constants.CYCLE_MONTHLY,activeContract.fsServ__Current_Payment_Cycle__c, 'Current payment cycle');
        System.assertEquals(fsCore.Constants.CYCLE_MONTHLY,activeContract.fsServ__Contract_Payment_Cycle__c, 'Contract payment cycle');
        System.debug(loggingLevel.ERROR, 'Active Contracts status : '+activeContract.fsServ__Is_Active__c);
        
       

        Test.startTest();
        Id jobId = Prizm_IndexRateChangeController.executeRateChangeJob();
       // Prizm_VoidQueueTransactionController.voidQueuedTransaction(contract.Index_Rate_Setup__c);
        Test.stopTest();
       
         System.assert(jobId != null, 'Job not executed');

         fsServ__Lending_Contract__c updatedContract =
            [Select Id
             , name
             , Index_Rate_Setup__c
             , fsServ__Contract_Date__c
             from fsServ__Lending_Contract__c 
             where Name = 'AT1701000002']; 
       
        fsCore__Index_Rate_Setup__c testIndexUpdated = 
            [Select Id
             , Name
             , Is_Already_Picked__c
             , Job_Id__c
             , fsCore__Is_Active__c
             from fsCore__Index_Rate_Setup__c
             where Id =: updatedContract.Index_Rate_Setup__c];        
        
        System.assertEquals(true,testIndexUpdated.Is_Already_Picked__c,'Test Index Picked for new rate queued transaction assert');
        System.assert(testIndexUpdated.Job_Id__c!=null);
        
        fsServ__Transaction_Processing_Queue__c testQueuedTransaction =
            [Select Id
             , Name
             , fsServ__Transaction_Code__c
             , fsServ__Transaction_Date__c
             , fsServ__Processing_Status__c
             from fsServ__Transaction_Processing_Queue__c
             where fsServ__Lending_Contract_Number__c =: contract.Id];
        
        System.debug(loggingLevel.ERROR, 'Transaction Queue : '+testQueuedTransaction);
        System.assert(testQueuedTransaction.Id!=null);
        System.assertEquals('RATE_CHANGED', testQueuedTransaction.fsServ__Transaction_Code__c, 'Rate Change Transaction Created assert');
        System.assertEquals('Ready', testQueuedTransaction.fsServ__Processing_Status__c, 'Rate Change Transaction status created assert');
        System.assertEquals(updatedContract.fsServ__Contract_Date__c, testQueuedTransaction.fsServ__Transaction_Date__c, 'Queued Transaction created on contract date assert');
   
        
        Prizm_VoidQueueTransactionController.voidQueuedTransaction(updatedContract.Index_Rate_Setup__c);
        
        fsServ__Transaction_Processing_Queue__c updatedTestQueuedTransaction =
            [Select Id
             , Name
             , fsServ__Transaction_Code__c
             , fsServ__Transaction_Date__c
             , fsServ__Processing_Status__c
             from fsServ__Transaction_Processing_Queue__c
             where fsServ__Lending_Contract_Number__c =: contract.Id];
        
        fsServ__Lending_Contract__c updatedRateOnContract =
            [Select Id
             , name
             , Index_Rate_Setup__c
             , fsServ__Contract_Date__c
             from fsServ__Lending_Contract__c 
             where Name = 'AT1701000002']; 
        
        fsCore__Index_Rate_Setup__c updatedIndexRate = 
            [Select Id
             , Name
             , Is_Already_Picked__c
             , Job_Id__c
             from fsCore__Index_Rate_Setup__c
             where Id =: updatedContract.Index_Rate_Setup__c];   
        
        
        
        System.debug(loggingLevel.ERROR, 'Transaction Queue : '+updatedTestQueuedTransaction);
        System.assert(updatedTestQueuedTransaction.Id!=null);
        System.assertEquals('Void', updatedTestQueuedTransaction.fsServ__Processing_Status__c, 'Rate Change Transaction status created assert');
        System.assertEquals(null, updatedRateOnContract.Index_Rate_Setup__c, 'Index Rate field Cleared on contract after voiding the transaction assert');
        System.assertEquals(false, updatedIndexRate.Is_Already_Picked__c, 'Is Already picked field cleared on index rate after voiding the transaction assert');

        
    }

}