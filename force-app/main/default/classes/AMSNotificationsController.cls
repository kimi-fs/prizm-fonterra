/**
 * AMSNotificationsController.cls
 * Description: Controller for the AMSNotifications component
 * @author: Ricardo Hurtado
 * @date: December 2018
 */
public class AMSNotificationsController {

    private Id currentUserContactId { get; set; }
    public User currentUser { get; set; }

    public Boolean hasNewNotifications { get; set; }

    public List<GoalService.FeedItemWrapper> feedItemWrappers { get; set; }

    public AMSNotificationsController() {
        this.currentUserContactId = AMSContactService.determineContactId();

        if (this.currentUserContactId != null) {
            this.currentUser = AMSUserService.getUserFromContactId(currentUserContactId);
        } else {
            // Sometimes fetching the ContactId fails, fall back to using the
            // current user.
            this.currentUser = AMSUserService.getUser();
            this.currentUserContactId = this.currentUser.ContactId;
        }

        getNotifications();

        if (currentUser.AMS_Notifications_Last_Clicked__c == null && feedItemWrappers.size() > 0) {
            this.hasNewNotifications = true;
        } else if (feedItemWrappers.size() > 0) {
            this.hasNewNotifications = feedItemWrappers[0].FeedItem.CreatedDate > this.currentUser.AMS_Notifications_Last_Clicked__c;
        }
    }

    private void getNotifications() {
        List<Goal__c> goals = GoalService.getGoalsForIndividual(currentUserContactId);
        List<Action__c> actions = GoalService.getActionsForGoals(goals);

        Map<Id, Goal__c> goalsByIdMap = new Map<Id, Goal__c>(goals);
        Map<Id, Action__c> actionsByIdMap = new Map<Id, Action__c>(actions);

        Set<Id> goalAndActionIds = new Set<Id>();

        goalAndActionIds.addAll(goalsByIdMap.keySet());
        goalAndActionIds.addAll(actionsByIdMap.keySet());

        this.feedItemWrappers = GoalService.getFeedItemsForObjects(goalAndActionIds, 'DESC', 10, this.currentUser.Id);

        for (GoalService.FeedItemWrapper wrapper : feedItemWrappers) {
            Id parentId = wrapper.parentId;

            if (goalsByIdMap.containsKey(parentId)) {
                wrapper.parentName = goalsByIdMap.get(parentId).Name;
            } else if (actionsByIdMap.containsKey(parentId)) {
                wrapper.parentName = actionsByIdMap.get(parentId).Action_Name__c;
            }
        }
    }

    public void notificationBellClicked() {
        currentUser.AMS_Notifications_Last_Clicked__c = DateTime.now();

        AMSUserService.updateUser(currentUser, new List<String> { 'AMS_Notifications_Last_Clicked__c' });
    }

}