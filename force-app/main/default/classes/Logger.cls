/*------------------------------------------------------------
Author:         Dan Fowlie
Company:        Trineo
Description:    Utility Class for Logging events
Test Class:     Test_Logger.cls
------------------------------------------------------------*/

public with sharing class Logger {

    public static List<Log__c> logs {get; set;}

    public enum LogType {INFO, DEBUG, ERROR}


    public static void log(String summary, String details, LogType type){
        if (Logger.logs == null) Logger.logs = new List<Log__c>();

        Log__c log = new Log__c(
            Summary__c = summary,
            Details__c = details,
            Type__c = type.name()
        );
        Logger.logs.add(log);
    }

    public static void saveLog(){
        if (Logger.logs != null){
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.allowFieldTruncation = true;
            dml.optAllOrNone = false;

            Database.SaveResult[] logResults = Database.insert(Logger.logs, dml);
        }
    }

    public static void clearLogs(){
        Logger.logs = new List<Log__c>();
    }
}