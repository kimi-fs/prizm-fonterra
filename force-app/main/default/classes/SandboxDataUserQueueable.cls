/**
* Description:
* @author: Eric Hu (trineo)
* @date: Aug 2019
* @test: DFDSandboxDataTest
*/
public class SandboxDataUserQueueable implements Queueable {

    private String newOrgName { get; set; }
    private Boolean setPasswords { get; set; }

    public Map<String, Id> profileIdsByName {
        get{
            if (profileIdsByName == null){
                profileIdsByName = new Map<String, Id>();
                for(Profile p : [SELECT Id, Name FROM Profile]) {
                    profileIdsByName.put(p.Name, p.Id);
                }
            }
            return profileIdsByName;
        }set;
    }

    public Map<String, Id> roleIdsByName {
        get{
            if (roleIdsByName == null){
                roleIdsByName = new Map<String, Id>();
                for(UserRole r : [SELECT Id, Name FROM UserRole]) {
                    roleIdsByName.put(r.Name, r.Id);
                }
            }
            return roleIdsByName;
        }set;
    }

    private static Boolean runningInASandbox {
        get {
            if (runningInASandbox == null){
                runningInASandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
            }
            return runningInASandbox;
        } private set;
    }

    private static String baseUrlOrgName {
        get {
            if (baseUrlOrgName == null){
                baseUrlOrgName = URL.getSalesforceBaseUrl().getHost().split('--')[1].split('\\.')[0].toLowerCase();
            }
            return baseUrlOrgName;
        } private set;
    }

    public SandboxDataUserQueueable(Boolean setPasswordsNext) {
        newOrgName = baseUrlOrgName;
        setPasswords = setPasswordsNext;
    }

    public SandboxDataUserQueueable(String orgName, Boolean setPasswordsNext) {
        newOrgName = orgName;
        setPasswords = setPasswordsNext;
    }

    public void execute(QueueableContext context) {

        if(validate()) {
            List<User> users = createTestUsers(newOrgName);
            createPermissionSets();

            if(setPasswords) {
                System.debug('Resetting passwords');
                System.enqueueJob(new SandboxDataCleanUpQueueable(users));
            }
        }
    }

    public List<User> createTestUsers (String newOrgName) {
        List<User> users = new List<User>();

        // Create the list of users that we want to create, which will also let us know how many users we need to disable
        // to accomodate licenses for them.
        if (!Test.isRunningTest()) {
            users = createInternalUser(users, 'int',        'farmsourceoneautomation+integration-'+newOrgName+'@gmail.com', 'tu', 'Integration', 'frmauautomation.integration@fonterra.com.'+newOrgName, 'Integration User', 'Fonterra AU', FALSE);
            users = createInternalUser(users, 'sysad2',     'farmsourceoneautomation+system_admin2-'+newOrgName+'@gmail.com', 'tu', 'System Admin', 'frmauautomation.system_admin2@fonterra.com.'+newOrgName, 'System Administrator', 'Farm Source', FALSE);
            users = createInternalUser(users, 'adauss',     'farmsourceoneautomation+administrator_au_supplier_services-'+newOrgName+'@gmail.com', 'tu', 'Administrator AU - Supplier Services', 'frmauautomation.administrator_au_supplier_services@fonterra.com.'+newOrgName, 'Administrator AU - Supplier Services', 'Fonterra AU', FALSE);
            users = createInternalUser(users, 'ftauam',     'farmsourceoneautomation+field_team_au_area_manager-'+newOrgName+'@gmail.com', 'tu', 'Field Team AU - Area Manager', 'frmauautomation.field_team_au_area_manager@fonterra.com.'+newOrgName, 'Field Team AU - Area Manager', 'Fonterra AU', FALSE);
            users = createInternalUser(users, 'ftaubdm',    'farmsourceoneautomation+field_team_au_business_development_manager-'+newOrgName+'@gmail.com', 'tu', 'Field Team AU - Business Development Manager', 'frmauautomation.field_team_au_business_development_manager@fonterra.com.'+newOrgName, 'Field Team AU - Business Development Manager', 'Fonterra AU', FALSE);
            users = createInternalUser(users, 'muau',       'farmsourceoneautomation+marketing_user_au-'+newOrgName+'@gmail.com', 'tu', 'Marketing User AU', 'frmauautomation.marketing_user_au@fonterra.com.'+newOrgName, 'Marketing User AU', 'Fonterra AU', FALSE);
            users = createInternalUser(users, 'sscaua',     'farmsourceoneautomation+supplier_service_centre_au_agent-'+newOrgName+'@gmail.com', 'tu', 'Supplier Service Centre AU - Agent', 'frmauautomation.supplier_service_centre_au_agent@fonterra.com.'+newOrgName, 'Supplier Service Centre AU - Agent', 'Fonterra AU', FALSE);
            users = createInternalUser(users, 'tlaut',      'farmsourceoneautomation+transport_and_logistics_au_transport-'+newOrgName+'@gmail.com', 'tu', 'Transport & Logistics AU - Transport', 'frmauautomation.transport_and_logistics_au_transport@fonterra.com.'+newOrgName, 'Transport & Logistics AU - Transport', 'Fonterra AU', FALSE);

        // testing this class in Prod can take a long time, reduce the number of records to be inserted in a test
        } else {
            users = createInternalUser(new List<User>(), 'int', 'farmsourceoneautomation+integration-'+newOrgName+'@gmail.com', 'TheTestUser', 'Integration', 'frmautomation.integration@fonterra.com.'+newOrgName, 'Integration User', 'Fonterra AU', FALSE);
        }

        // Check it there is a spare license available for users
        // if not deactivate users who have the oldest LastLoginDate
        List<User> usersToDeactivate = new List<User>();
        Set<String> profilesToFreeUpLicenses = new Set<String>{
            'Supplier Service Centre AU - Agent'
        };

        Map<String, UserLicense> licenseTypesByName = new Map<String, UserLicense>();
        for(UserLicense license : [SELECT Id, Name, UsedLicenses, TotalLicenses FROM UserLicense]) {
            licenseTypesByName.put(license.Name, license);
        }

        // Free up the number of licenses that we need plus 4 extra for additional use
        Integer numberOfInternalUsersToDeactivate = Test.isRunningTest() ? 1 : users.size() + 4;
        if(licenseTypesByName.get('Salesforce').TotalLicenses - licenseTypesByName.get('Salesforce').UsedLicenses <= numberOfInternalUsersToDeactivate) {
            for(User u : [  SELECT Id, Alias, Email, FirstName, LastName, LastLoginDate, IsActive, SenderName, Username
                            FROM User
                            WHERE   Profile.Name IN :profilesToFreeUpLicenses AND
                                    IsActive = true
                            ORDER BY LastLoginDate ASC NULLS Last
                            LIMIT :numberOfInternalUsersToDeactivate]) {
                u.IsActive = false;
                usersToDeactivate.add(u);
            }
        }

        if(usersToDeactivate.size() > 0) {
            try {
                Database.SaveResult[] sr = Database.update(usersToDeactivate, false);
                Integer failures = 0;
                for(Database.SaveResult result : sr) {
                    if (!result.isSuccess()) {
                        failures++;
                    }
                }
                System.debug('Deactivating user failures: ' + failures);

            } catch (Exception e) {
                Logger.log('SandboxDataUserQueueable clear admin license error', e.getMessage() + ' - ' + e.getStackTraceString(), Logger.LogType.ERROR);
            }
        }


        for(UserLicense license : [SELECT Id, Name, UsedLicenses, TotalLicenses FROM UserLicense]) {
            System.debug('*** license = '+license.Name + ' - ' + (license.TotalLicenses - license.UsedLicenses));
        }



        try {
            insert users;
            return users;
        } catch (Exception e) {
            System.debug('*** e = '+e.getMessage() + ' - ' + e.getStackTraceString());
            Logger.log('SandboxDataUserQueueable insert users error', e.getMessage() + ' - ' + e.getStackTraceString(), Logger.LogType.ERROR);
            return null;
        }
    }

    public List<User> createInternalUser(List<User> userList, String alias, String email, String firstname, String lastname, String username, String profile, String role, Boolean giveKnowledge) {

        if(profileIdsByName.containsKey(profile)){
            userList.add( new User(
                    Alias = alias,
                    Email = email,
                    EmailEncodingKey='UTF-8',
                    FirstName = firstname,
                    Lastname = lastname,
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_NZ',
                    Profileid = profileIdsByName.get(profile),
                    UserRoleId = roleIdsByName.containsKey(role) ? roleIdsByName.get(role) : null,
                    Country = 'New Zealand',
                    IsActive = true,
                    TimezonesIdKey = 'Pacific/Auckland',
                    Username = username,
                    SenderName = Test.isRunningTest() ? 'SandboxDataTest user' : null,
                    UserPermissionsKnowledgeUser = false
                )
            );
        }
        return userList;
    }

    public static void createPermissionSets() {

        // Create a permission set so the automation admin doesn't need to reset their password
        PermissionSet pr = new PermissionSet(Label = 'Sandbox Don\'t expire password', Name = 'Sandbox_Dont_expire_password');
        pr.PermissionsPasswordNeverExpires = true;
        pr.Label = Test.isRunningTest() ? 'Sandbox Don\'t expire password test' : 'Sandbox Don\'t expire password';
        pr.Name = Test.isRunningTest() ? 'Sandbox_Dont_expire_password_test' : 'Sandbox_Dont_expire_password';

        try {
            insert pr;
        } catch (Exception e) {
            Logger.log('SandboxDataUserQueueable insert permission set error', e.getMessage() + ' - ' + e.getStackTraceString(), Logger.LogType.ERROR);
        }
    }

    public static void manualSteps() {

        manualSteps(baseUrlOrgName);
    }

    public static void manualSteps(String sandboxName) {

        whitelistTestIpAddresses();
    }

    public static void whitelistTestIpAddresses() {

        if(validate()) {
            MetadataService.MetadataPort service = new MetadataService.MetadataPort();
            service.SessionHeader = new MetadataService.SessionHeader_element();
            service.SessionHeader.sessionId = UserInfo.getSessionId();

            // updating Network Access deletes current entries
            // get current entries and add to a list to be re added
            List<String> fields = new List<String>{'fullName', 'networkAccess'};

            List<MetadataService.SecuritySettings> mdInfo = new List<MetadataService.SecuritySettings>();
            if(!Test.isRunningTest()) {
                mdInfo = (List<MetadataService.SecuritySettings>) service.readMetadata('SecuritySettings', fields).getRecords();
            }

            Set<String> currentEntries = new Set<String>();
            for(MetadataService.SecuritySettings md : mdInfo) {
                for(MetadataService.IpRange ip : md.networkAccess.ipRanges) {
                    currentEntries.add(ip.description+','+ip.start+','+ip.end_x);
                }
            }

            // add in the new entries
            currentEntries.add('Bamboo 1,13.236.48.140,13.236.48.140');
            currentEntries.add('Bamboo 2,13.236.237.91,13.236.237.91');
            currentEntries.add('Bamboo 3,54.66.186.185,54.66.186.185');
            currentEntries.add('Automation 1,202.175.128.71,202.175.128.71');
            currentEntries.add('Automation 2,202.175.128.74,202.175.128.74');
            currentEntries.add('Automation 3,165.225.114.98,165.225.114.98');
            currentEntries.add('Automation 4,165.225.114.131,165.225.114.131');
            currentEntries.add('Automation 5,165.225.98.60,165.225.98.60');
            currentEntries.add('Automation 6,202.175.128.61,202.175.128.61');
            currentEntries.add('Automation 7,202.175.128.62,202.175.128.62');
            currentEntries.add('Automation 8,165.225.114.95,165.225.114.95');
            currentEntries.add('Automation 9,165.225.114.125,165.225.114.125');
            currentEntries.add('Automation 10,202.175.128.77,202.175.128.77');
            currentEntries.add('Automation 11,165.225.114.106,165.225.114.106');
            currentEntries.add('Automation 12,165.225.98.61,165.225.98.61');
            currentEntries.add('Automation 13,165.225.114.120,165.225.114.120');
            currentEntries.add('Automation 14,165.225.114.119,165.225.114.119');
            currentEntries.add('Automation 15,165.225.114.105,165.225.114.105');

            // insert the current and new network access records
            MetadataService.SecuritySettings securitySettings = new MetadataService.SecuritySettings();
            securitySettings.networkAccess = new MetadataService.NetworkAccess();
            securitySettings.networkAccess.ipRanges = new MetadataService.IpRange[] {};

            for(String s : currentEntries) {
                List<String> parts = s.split(',');
                MetadataService.IpRange ip = new MetadataService.IpRange();
                ip.description = parts[0] == 'null' ? null : parts[0];
                ip.start = parts[1];
                ip.end_x = parts[2];
                securitySettings.networkAccess.ipRanges.add(ip);
            }

            if(!Test.isRunningTest()) {
                try {
                    List<MetadataService.UpsertResult> ipResults = service.upsertMetadata(new MetadataService.Metadata[] { securitySettings });

                } catch (exception e) {
                    Logger.log('SandboxDataCleanUpQueueable ip whitelisting error', e.getMessage() + ' - ' + e.getStackTraceString(), Logger.LogType.ERROR);
                }
            }
        }
    }

    public static Boolean validate() {

        // check this is not being run in Production
        if(runningInASandbox == false && Test.isRunningTest() == false) {
            System.assert(false, '** this should not be run in a production environment outside of a test **');
        }

        return true;
    }

    @future
    private static void saveLogs(String logString) {
        List<Log__c> logs = (List<Log__c>) JSON.deserialize(logString, List<Log__c>.class);
        insert logs;
    }
}