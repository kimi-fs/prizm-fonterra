@IsTest
public class EmailUnsubscribeControllerTest {
    @TestSetup static void setup() {
        TestClassDataUtil.createAuChannelPreferenceSettings();

        Contact individualAU = TestClassDataUtil.createIndividualAU(true);
    }

    @IsTest static void testAUPage() {
        Channel_Preference__c cp = [SELECT Id, Unsubscribe_Key__c FROM Channel_Preference__c WHERE Contact_Id__r.RecordType.Name = 'Individual AU'][0];

        PageReference pr = Page.EmailUnsubscribe;
        pr.getParameters().put('id', cp.Unsubscribe_Key__c);

        System.Test.setCurrentPage(pr);

        EmailUnsubscribeController beu = new EmailUnsubscribeController();
        beu.confirmUnsubscribe();
        beu.cancel();

        Channel_Preference__c updatedcp = [SELECT Id, DeliveryByEmail__c, Unsubscribe_Key__c FROM Channel_Preference__c WHERE Id =:cp.Id][0];

        System.assert(!updatedcp.DeliveryByEmail__c, 'Delivery By email should be turned off.');
    }
}