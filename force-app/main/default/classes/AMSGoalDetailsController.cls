/**
 * AMSGoalDetailsController.cls
 * Description: Controller for AMSGoalDetails.page
 * @author: John Au
 * @date: December 2018
 */
public with sharing class AMSGoalDetailsController {

    private Id goalId;
    public Goal__c goal { get; set; }
    public List<Action__c> actions { get; set; }

    public AMSGoalDetailsController() {
        this.goalId = ApexPages.currentPage().getParameters().get(AMSCommunityUtil.getGoalIdParameterName());
        this.goal = GoalService.getGoal(goalId);
        this.actions = GoalService.getActionsForGoals(new List<Goal__c>{ goal });
    }

}