/**
 * AMSProdQualSummaryController_Test
 * Test class for AMSProdQualSummaryController
 *
 * Author: NL (Trineo)
 * Date: 2017-08-08
 **/
@isTest
public with sharing class AMSProdQualSummaryControllerTest {

    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup static void testSetup() {
        //Create a integration user
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.createIntegrationUser();
    }
    public static void setupFarmDetails() {
        //Create Reference Periods for current period and last
        //Behavior is influenced if we are after or before the standard reference period start date
        User integrationUser = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name = 'Integration User' Limit 1];
        System.runAs(integrationUser) {
            Date today = System.today();
            Date currentSeasonStartDate;
            Date currentSeasonEndDate;
            if (today < Date.newInstance(today.year(), 5, 1)) {
                currentSeasonStartDate = Date.newInstance(today.year() - 1, 5, 1);
            } else {
                currentSeasonStartDate = Date.newInstance(today.year(), 5, 1);
            }
            if (today > Date.newInstance(today.year(), 4, 30)) {
                currentSeasonEndDate = Date.newInstance(today.year() + 1, 4, 30);
            } else {
                currentSeasonEndDate = Date.newInstance(today.year(), 4, 30);
            }
            //Inser reference period based on the calculated reference periods
            Reference_Period__c currentReferencePeriod = TestClassDataUtil.createReferencePeriod('Milking Season AU', 'current', false);
            currentReferencePeriod.Start_Date__c = currentSeasonStartDate;
            currentReferencePeriod.End_Date__c = currentSeasonEndDate;
            currentReferencePeriod.Status__c = 'Current';
            insert currentReferencePeriod;
            Reference_Period__c priorReferencePeriod = TestClassDataUtil.createReferencePeriod('Milking Season AU', 'prior', false);
            priorReferencePeriod.Start_Date__c = currentSeasonStartDate.addYears(-1);
            priorReferencePeriod.End_Date__c = currentSeasonEndDate.addYears(-1);
            priorReferencePeriod.Status__c = 'Past';
            insert priorReferencePeriod;
            //Create community user and associated farms
            User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
            communityUser.Email = USER_EMAIL;
            update communityUser;
            List<Account> farms = [
                    SELECT Id
                    FROM Account
                    WHERE Id IN (
                            SELECT Farm__c
                            FROM Individual_Relationship__c
                            WHERE Individual__c = :communityUser.ContactId
                            AND Active__c = true
                    )
            ];
            //create farm seasons for associated farms
            Farm_Season__c currentFarmSeason = TestClassDataUtil.createFarmSeasons(new List<Account>{
                    farms[0]
            }, new List<Reference_Period__c>{
                    currentReferencePeriod
            }, false)[0];
            currentFarmSeason.Peak_Cows__c = 10;
            currentFarmSeason.Dairy_Hectares__c = 10;
            insert currentFarmSeason;
            Farm_Season__c priorFarmSeason = TestClassDataUtil.createFarmSeasons(new List<Account>{
                    farms[0]
            }, new List<Reference_Period__c>{
                    priorReferencePeriod
            }, false)[0];
            priorFarmSeason.Peak_Cows__c = 10;
            priorFarmSeason.Dairy_Hectares__c = 10;
            insert priorFarmSeason;
        }
    }

    @isTest
    static void testPageWithNoFarmsAssigned() {
        setupFarmDetails();
        User communityUser = [SELECT Id, ContactId FROM User WHERE Email = :USER_EMAIL];
        Test.startTest();
        TestClassDataUtil.deactivateDerivedRelationshipsForIndividual(communityUser.ContactId);
        System.runAs(communityUser) {
            AMSProdQualSummaryController controller = new AMSProdQualSummaryController();
            System.assert(controller.individualHasFarms == false, 'Individual does not have farms flag is not working');
        }
        Test.stopTest();
    }

    @isTest
    static void testRecalculateForms() {
        setupFarmDetails();
        User communityUser = [SELECT Id, ContactId FROM User WHERE Email = :USER_EMAIL];
        //Get farms created during user set up
        System.runAs(communityUser) {
            Test.startTest();
            AMSProdQualSummaryController controller = new AMSProdQualSummaryController();
            System.assert(controller.collectionPeriodFormForMonth.collectionPeriodWrapperList.size() == 1, 'Month to date form doesnt have data');
            System.assert(controller.collectionPeriodFormForSeason.collectionPeriodWrapperList.size() == 1, 'Season to date form doesnt have data');
            Test.stopTest();
        }
    }

    @isTest
    static void restReportTypePopulatedAndForward() {
        setupFarmDetails();
        User communityUser = [SELECT Id, ContactId FROM User WHERE Email = :USER_EMAIL];
        //Get farms created during user set up
        System.runAs(communityUser) {
            Test.startTest();
            AMSProdQualSummaryController controller = new AMSProdQualSummaryController();
            System.assert(controller.reportTypeOptionList != null, 'Report type list has not populated');
            PageReference pageRef = controller.forwardToReport();
            System.assert(pageRef.getUrl() == Page.AMSProdQualSummary.getUrl(), 'Report forward does not return result');
            Test.stopTest();
        }
    }

}