public class Prizm_ContractCustomTriggerHandler {
    public static void createAdvanceRecs(Map<Id,fsServ__Lending_Contract__c> pOldContractMap){
        Set<Id> contractIds = pOldContractMap.keySet();
         fsCore.ActionInput actionIP = new fsCore.ActionInput();
            actionIP.addRecords(contractIds);
            System.debug('actionIP'+actionIP);
            Prizm_AdvanceProcessor actionObj = new Prizm_AdvanceProcessor();
            actionObj.setInput(actionIP);
            actionObj.process();
    }
    
    public static void updateAdvanceAppWithContract(Map<Id,fsServ__Lending_Contract__c> pNewContractMap){
        
        Map<Id, Id> advanceAppIdToContractIdMap = new Map<Id, Id>();
        
        system.debug(loggingLevel.ERROR, 'inside advance app update');
        
        List<fsServ__Lending_Contract__c> contractList = Prizm_LendingContractUtil.getContracts(pNewContractMap.keyset());
        
        for(fsServ__Lending_Contract__c contract : contractList){
            advanceAppIdToContractIdMap.put(contract.fsServ__Lending_Application_Number__r.Advance_Application__c, contract.Id);
        }
        
        system.debug(loggingLevel.ERROR, advanceAppIdToContractIdMap);
        
        List<Advance_Application__c> advances = [Select Id, Lending_Contract__c 
                                                 FROM Advance_Application__c 
                                                 WHERE Id IN :advanceAppIdToContractIdMap.keyset()];
        
        for(Advance_Application__c advance : advances){
            advance.Lending_Contract__c = advanceAppIdToContractIdMap.get(advance.Id);
        }
        
        system.debug(loggingLevel.ERROR, advances);
        update advances;
    }
}