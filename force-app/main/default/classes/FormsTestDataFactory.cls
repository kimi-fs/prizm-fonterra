/*
 * DESCRIPTION:
 * This class acts as a central test data generator for Fonterra forms
 *
 * VERSION:
 * 1.0
 *
 * CREATED: BY Mustafa Parekh ON 9-12-2020 FOR MPR-162
 *
 *
 */
@isTest
public with sharing class FormsTestDataFactory {
    public FormsTestDataFactory() {

    }

    // Creates Test Account data
    @isTest
    public static Account createAccountData()
    {
        Account act = new Account(RecordTypeId=GlobalUtility.auAccountFarmRecordTypeId,Name='0001');
        insert act;
        return act;
    }


    @isTest
    public static String createMilkCoolingPurchaseIncentiveData()
    {
        Milk_Cooling_Incentive__c mcid = new Milk_Cooling_Incentive__c(
            Action_Code__c = '',
            Cooling_Equipment_Complies_AS1187_S__c = true,
            Installed_Cooling_Equipment_Price__c = 2,
            Capacity_Increased_To_50__c = false,
            Incentive_Percentage_Rate__c = 0,
            Significantly_Improves_Cooling_Capacity__c = false,
            Total_Incentive_Payment__c = 0,
            Vat_Graduated_2500_Liters__c = false,
            Term_Incentive_No_Months__c = 0,
            Number_Vats_Not_Exceed_Two__c = false,
            Monthly_Payment__c = 0,
            Vat_Has_75mm_Outlet__c = false,
            Commencement_Date__c = Date.valueOf('2020-12-09'),
            Completion_Date__c = null
        );

        return JSON.serialize(mcid);
    }

    @isTest
    public static String createABNUpdateData()
    {
        ABN_Update__c abnUpdate = new ABN_Update__c(
            Australian_Business_Number__c = '12345678901',
            GST_Registered__c = 'Y'
        );

        return JSON.serialize(abnUpdate);
    }


    @isTest
    public static String createSupplierInformationDetailData()
    {
        Supplier_Information__c supplierInformationDetail = new Supplier_Information__c(
            Action_Code__c = '',
            Street_Number__c = '',
            Street_1__c = '',
            Street_2__c = '',
            Suburb__c = '',
            State__c = '',
            City__c = '',
            Postcode__c = '0000',
            Phone_Number__c = '+610200000000',
            Fax__c = '+610200000000',
            Mobile_Number__c = '+610400000000',
            Email__c = 'abc@xyz.com',
            UDV_Member__c = 'N',
            Farm_Access_Approved_by_Transport__c = 'N',
            Dairy_License_Number__c = '1234',
            FSP_Audit_Status_Verified__c = 'N',
            Last_Audit_Date__c = null
        );

        return JSON.serialize(supplierInformationDetail);
    }



    @isTest
    public static String createBankingInformationData()
    {
        Banking_Information__c bankingInformation = new Banking_Information__c(
            Effective_Date__c = date.newInstance(2020, 12, 12),
            Payment_Instruction_Type__c = 'New payment instructions',
            Bank__c = 'ABC Bank',
            Branch__c = 'XYZ Branch',
            Branch_Number__c = '123',
            Account_Number__c = '00000000'
        );

        return JSON.serialize(bankingInformation);
    }

}