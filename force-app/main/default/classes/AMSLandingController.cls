/**
 * Description: Landing page for AMS Community displays images from documents and AMS News knowledge articles
 * @author: Amelia (Trineo)
 * @date: July 2017
 */
public with sharing class AMSLandingController {

    public Boolean userIsLoggedIn {get; private set;}

    public static final Integer NEWS_ARTICLES_LIMIT = 3;

    public AMSLandingController() {
        this.userIsLoggedIn = UserInfo.getUserType() != 'Guest';
    }

    //gets the news articles
    public List<Knowledge__kav> getNewsArticles(){
        List<Knowledge__kav> newsArticles = new List<Knowledge__kav>();

        //get 15 articles but only display up to the limit- this is so dairy code articles
        //can be filtered out without preventing the other articles from being displayed
        //kinda hacky but meh
        for (Knowledge__kav newsArticle : AMSKnowledgeService.newsArticlesPaginated(0, 15)) {
            if (!AMSKnowledgeService.isDairyCodeArticle(newsArticle)) {
                newsArticles.add(newsArticle);
            }
            if (newsArticles.size() == NEWS_ARTICLES_LIMIT) {
                break;
            }
        }

        return newsArticles;
    }

    public static String getArticleParameterName() {
        return AMSCommunityUtil.getArticleParameterName();
    }

    public static String getAppStoreLink(){
        return AMSCommunityUtil.getAppleStoreLink();
    }

    public static String getGoogleStoreLink(){
        return AMSCommunityUtil.getGoogleStoreLink();
    }
}