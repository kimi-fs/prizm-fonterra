public abstract class Prizm_AdvanceTrnxProcessorBase implements fsCore.ActionProcessor{
	public fsCore.ActionInput mActionInput;
    public fsCore.ActionOutput mActionOutput;
    public List<Prizm_AdvanceTransactionObject> mAdvanceTrnxObjList;
	public List<Advance_Transaction__c> mAdvanceTrnxList;
	public List<String> mErrorList;
   
    public Prizm_AdvanceTrnxProcessorBase(){
		init();
		}
    public void init(){
	mActionInput = new fsCore.ActionInput();
    mActionOutput = new fsCore.ActionOutput();
	mAdvanceTrnxObjList = new List<Prizm_AdvanceTransactionObject>();
	mAdvanceTrnxList = new List<Advance_Transaction__c>();
	}
	
	public void setInput(fsCore.ActionInput pInput) {
		 mActionInput = pInput;
    }
	public void process() {
      createAdvanceTrnxObj();
	  doProcess();
    }
    public fsCore.ActionOutput getOutput() {
        return mActionOutput;
    }
	
    public abstract void doProcess();
	public abstract void createAdvanceTrnxObj();
    public abstract String getClassName(); 
  
    public virtual void checkTransactionType(List<Prizm_AdvanceTransactionObject> pAdvTrnxObjList){
	List<Prizm_AdvanceTransactionObject> postAdvTrxns = new List<Prizm_AdvanceTransactionObject>();
	List<Prizm_AdvanceTransactionObject> reverseAdvTrxns = new List<Prizm_AdvanceTransactionObject>();
	for(Prizm_AdvanceTransactionObject advTrnxObj : pAdvTrnxObjList){
		if(advTrnxObj.operation_type == 'Post'){
		  postAdvTrxns.add(advTrnxObj);
		}
		if(advTrnxObj.operation_type == 'Reverse'){
		  reverseAdvTrxns.add(advTrnxObj);
		}
		}
		
		if(!postAdvTrxns.isEmpty()){
		 postAdvanceTransaction(postAdvTrxns);
		}
		
		if(!reverseAdvTrxns.isEmpty()){
		 reverseAdvanceTransaction(reverseAdvTrxns);
		}
		
   }
    public virtual void validateAdvanceTransaction(Prizm_AdvanceTransactionObject pAdvTrnxObj) {
        mErrorList = new List<String>();
        if(pAdvTrnxObj.amount == null){
            mErrorList.add('Amount is not populated');
        }
        if(pAdvTrnxObj.date_field == null){
            mErrorList.add('Transaction date is missing');
        }
        if(pAdvTrnxObj.record_type_name == null){
            mErrorList.add('Record type is missing');
        } 
    }
	public virtual List<Advance_Transaction__c> postAdvanceTransaction(List<Prizm_AdvanceTransactionObject> pAdvTrnxObjList){
	  for(Prizm_AdvanceTransactionObject advTrnxObj : pAdvTrnxObjList){
	    validateAdvanceTransaction(advTrnxObj);
          System.debug(loggingLevel.Error,'mErrorList--->'+mErrorList);
		if(mErrorList.isEmpty()){
		   Advance_Transaction__c advanceTrnx = getAdvanceTransaction(advTrnxObj);
	       mAdvanceTrnxList.add(advanceTrnx);
		}
		
	  }
   return mAdvanceTrnxList;
   }
    public virtual List<Advance_Transaction__c> reverseAdvanceTransaction(List<Prizm_AdvanceTransactionObject> pAdvTrnxObjList){
	for(Prizm_AdvanceTransactionObject advTrnxObj : pAdvTrnxObjList){
	    validateAdvanceTransaction(advTrnxObj);
		
		if(mErrorList.isEmpty()){
			Advance_Transaction__c advanceTrnx = getAdvanceTransaction(advTrnxObj);
			mAdvanceTrnxList.add(advanceTrnx);
		}
	  }
        return mAdvanceTrnxList;
   }
    
   public Advance_Transaction__c getAdvanceTransaction(Prizm_AdvanceTransactionObject pAdvTrnxObj){
			Advance_Transaction__c advanceTrnx = new Advance_Transaction__c();
            advanceTrnx.Advance_Number__c = pAdvTrnxObj.advance_number;
            advanceTrnx.Amount__c = pAdvTrnxObj.amount;
            advanceTrnx.Date__c = pAdvTrnxObj.date_field;
            advanceTrnx.Description__c = pAdvTrnxObj.description;
            advanceTrnx.RecordTypeId = Schema.SObjectType.Advance_Transaction__c.getRecordTypeInfosByDeveloperName().get(pAdvTrnxObj.record_type_name).getRecordTypeId();
            //advanceTrnx.Source_ID__c 
            advanceTrnx.Status__c = pAdvTrnxObj.status;
            advanceTrnx.Transaction_Type__c =  pAdvTrnxObj.transaction_type;
			advanceTrnx.Lending_Contract_Number__c = pAdvTrnxObj.contract_number;
			advanceTrnx.Contract_Transaction_Number__c = pAdvTrnxObj.contract_transaction_number; 
			advanceTrnx.Is_Active__c = pAdvTrnxObj.is_active;
		return 	advanceTrnx;
   }
    
    public Prizm_AdvanceTransactionObject getDisbursementAdvanceTrnxObj(){
        Prizm_AdvanceTransactionObject advTrnxObj = new Prizm_AdvanceTransactionObject();
        
        advTrnxObj.operation_type ='Post';
        advTrnxObj.is_active = true;
        // advTrnxObj.contract_transaction_number {get; set;}
        advTrnxObj.date_field = date.today();
        advTrnxObj.description = 'Advance Transaction of type disbursement.';
        advTrnxObj.record_type_name = 'Disbursement';
        advTrnxObj.status = 'Ready';
      //  advTrnxObj.transaction_type = '01';
        return advTrnxObj;
    }
   
}