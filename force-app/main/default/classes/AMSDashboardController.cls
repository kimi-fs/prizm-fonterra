/**
 * AMSDashboardController.cls
 * Description: Controller for AMSDashboard.page
 * @author: John (Trineo)
 * @date: September 2017
 */
public without sharing class AMSDashboardController {

    public static final Integer DEFAULT_COLLECTION_LIMIT = 5;
    public static Integer FARMS_PER_PAGE = 5;

    private Id currentUserContactId;

    public List<Account> allFarmsForIndividual;         // all the farms the individual has access to
    public List<Account> farmsForIndividual;            // the farms to display on the page depending on the pagination

    public List<FarmCollectionWrapper> farmCollectionWrappers { get; set; }
    public Integer collectionLimit { get; set; }

    public List<SelectOption> lastColumnSelectOptionList { get; set; }

    private AMSNewsController newsController;
    public Preferences userPreferences { get; set ;}

    public String getArticleParameterName() {
        return AMSCommunityUtil.getArticleParameterName();
    }

    public List<AMSKnowledgeList.NewsArticleWrapper> wrappedArticles { get; set; }

    public Integer numberOFarms {get; private set;}

    public AMSDashboardController() {
        this.currentUserContactId = AMSContactService.determineContactId();

        this.collectionLimit = AMSDashboardController.DEFAULT_COLLECTION_LIMIT;
        this.lastColumnSelectOptionList = AMSCollectionService.DASHBOARD_SELECT_OPTIONS;

        this.allFarmsForIndividual = RelationshipAccessService.accessToFarms(this.currentUserContactId, RelationshipAccessService.Access.ACCESS_PRODUCTION_AND_QUALITY);
        this.numberOFarms = this.allFarmsForIndividual.size();
        this.farmsForIndividual = getFarmsToDisplayOnPage();

        this.userPreferences = getUserPreferencesFromContact();

        getForms();
        initializeNewsController();
        initialiseGoalsAndActions();
    }

    private void initialiseGoalsAndActions() {
        goalsForIndividual = GoalService.getGoalsForIndividual(currentUserContactId);
        actionsForIndividual = GoalService.getActionsForGoals(goalsForIndividual);
    }

    public List<Goal__c> goalsForIndividual { get; set; }
    public List<Action__c> actionsForIndividual { get; set; }

    /**
    * Getting the records as a subset from the total list so we dont add yet another query
    * and because soql offset wont necessarily work as the service checks the access level after the query
    */
    private List<Account> getFarmsToDisplayOnPage() {
        List<Account> farmsForPage = new List<Account>();
        farmsForPage = (List<Account>)PaginationController.getRecordsToDisplayOnPage(FARMS_PER_PAGE, this.allFarmsForIndividual);
        return farmsForPage;
    }

    private void initializeNewsController() {
        newsController = new AMSNewsController();
        AMSNewsController.ARTICLES_PER_PAGE = 5;
        wrappedArticles = newsController.getWrappedArticles();
    }

    public class FarmCollectionWrapper {
        public Account farm { get; set; }
        public AMSCollectionService.CollectionPeriodForm collectionPeriodForm { get; set; }
        public AMSCollectionService.CollectionPeriodForm monthToDateCollectionPeriodForm { get; set; }
        public AMSCollectionService.CollectionPeriodForm seasonToDateCollectionPeriodForm { get; set; }

        public String lastColumnType { get; set; }

        private AMSDashboardController controller;

        public FarmCollectionWrapper(AMSDashboardController controller, Account farm) {
            this.controller = controller;
            this.farm = farm;

            String lastColumnTypePreference = this.controller.userPreferences.DashboardDefaultColumnForFarmIdMap.get(this.farm.Id);
            lastColumnType = lastColumnTypePreference == null ? AMSCollectionService.DASHBOARD_SELECT_OPTIONS[0].getValue() : lastColumnTypePreference;
        }

        public void saveDefaultColumn() {
            this.controller.saveDashboardDefaultColumnInUserPreferences(farm.Id, this.lastColumnType);
        }

        public AMSCollectionService.UnitOfMeasurement getLastColumnUnitOfMeasurement() {
            return AMSCollectionService.GRAPH_SELECT_OPTIONS_TO_UNIT_OF_MEASUREMENT_MAP.get(lastColumnType);
        }
    }

    public void getForms() {
        farmCollectionWrappers = new List<FarmCollectionWrapper>();

        List<Id> farmIds = new List<Id>();

        for (Account farm : farmsForIndividual) {
            farmIds.add(farm.Id);
        }

        Map<Id, List<Farm_Season__c>> seasonsByFarmId = AMSCollectionService.queryRelatedFarmSeasonsForFarms(farmIds);
        List<Reference_Period__c> existingReferencePeriods = AMSReferencePeriodService.getPastReferencePeriods();
        Map<Id, Account> farmsWithLatest5Collections = AMSCollectionService.queryFarmsAndRelatedCollections(farmIds, existingReferencePeriods[0].Start_Date__c, existingReferencePeriods[0].End_Date__c, 5);
        Map<Id, Date> farmsLatestCollectionDate = AMSCollectionService.getFarmsLatestCollectionDate(farmIds);

        for (Account farm : farmsForIndividual) {
            FarmCollectionWrapper wrapper = new FarmCollectionWrapper(this, farm);

            List<Farm_Season__c> paddedSeasonList = AMSCollectionService.populateFarmSeasonListInDescendingOrder(seasonsByFarmId.get(farm.Id), existingReferencePeriods);

            Farm_Season__c currentSeason = paddedSeasonList[0];
            Farm_Season__c priorSeason = null;

            if (paddedSeasonList.size() > 1) {
                priorSeason = paddedSeasonList[1];
            }

            Date last5CollectionsDate = getDateForLast5CollectionsForFarm(farmsWithLatest5Collections.get(farm.Id));

            Date farmLastestCollectDate = farmsLatestCollectionDate.get(farm.Id);

            wrapper.collectionPeriodForm = new AMSCollectionService.CollectionPeriodForm(
                last5CollectionsDate,
                Date.today().addDays(1),
                priorSeason != null ? last5CollectionsDate.addYears(-1) : null,
                priorSeason != null ? Date.today().addDays(1).addYears(-1) : null,
                priorSeason,
                currentSeason,
                farm.Id,
                AMSCollectionService.DAY
            );

            wrapper.collectionPeriodForm.convertDateMapToListDescending(true);

            wrapper.monthToDateCollectionPeriodform = new AMSCollectionService.CollectionPeriodForm(
                Date.today().toStartOfMonth(),
                Date.today(),
                priorSeason != null ? Date.today().addYears(-1).toStartOfMonth() : null,
                priorSeason != null && farmLastestCollectDate != null ? farmLastestCollectDate.addYears(-1) : null,
                priorSeason,
                currentSeason,
                farm.Id,
                AMSCollectionService.MONTH
            );

            wrapper.seasonToDateCollectionPeriodForm = new AMSCollectionService.CollectionPeriodForm(
                currentSeason.Reference_Period__r.Start_Date__c,
                Date.today(),
                priorSeason != null ? priorSeason.Reference_Period__r.Start_Date__c : null,
                priorSeason != null && farmLastestCollectDate != null ? farmLastestCollectDate.addYears(-1) : null,
                priorSeason,
                currentSeason,
                farm.Id,
                AMSCollectionService.SEASON
            );

            farmCollectionWrappers.add(wrapper);
        }
    }

    private Date getDateForLast5CollectionsForFarm(Account farm) {
        if (!farm.Collections__r.isEmpty()) {
            return farm.Collections__r[farm.Collections__r.size() - 1].Pick_Up_Date__c;
        }

        return Date.today();
    }

    public class Preferences {
        public Map<String, String> DashboardDefaultColumnForFarmIdMap;
        public Boolean DashboardGoalsExpanded {
            get {
                if (this.DashboardGoalsExpanded == null) {
                    this.DashboardGoalsExpanded = true;
                }

                return this.DashboardGoalsExpanded;
            }
            set;
        }
        public Boolean DashboardActionsExpanded {
            get {
                if (this.DashboardActionsExpanded == null) {
                    this.DashboardActionsExpanded = true;
                }

                return this.DashboardActionsExpanded;
            }
            set;
        }

        public Preferences() {
            DashboardDefaultColumnForFarmIdMap = new Map<String, String>();
        }
    }

    private Preferences getUserPreferencesFromContact() {
        Contact currentContact = AMSContactService.getContact(this.currentUserContactId);
        Preferences preferences = null;

        if (currentContact != null && !String.isBlank(currentContact.AMS_User_Preferences__c)) {
            preferences = (Preferences) JSON.deserialize(currentContact.AMS_User_Preferences__c, Preferences.class);
        } else {
            preferences = new Preferences();
        }

        return preferences;
    }

    public void saveDashboardDefaultColumnInUserPreferences(String farmId, String dashboardDefaultColumn) {
        this.userPreferences = getUserPreferencesFromContact();
        this.userPreferences.DashboardDefaultColumnForFarmIdMap.put(farmId, dashboardDefaultColumn);

        saveUserPreferences();
    }

    public void toggleDashboardGoalsExpanded() {
        this.userPreferences = getUserPreferencesFromContact();
        this.userPreferences.DashboardGoalsExpanded = !this.userPreferences.DashboardGoalsExpanded;

        saveUserPreferences();
    }

    public void toggleDashboardActionsExpanded() {
        this.userPreferences = getUserPreferencesFromContact();
        this.userPreferences.DashboardActionsExpanded = !this.userPreferences.DashboardActionsExpanded;

        saveUserPreferences();
    }

    private void saveUserPreferences() {
        Contact currentContact = AMSContactService.getContact(this.currentUserContactId);
        currentContact.AMS_User_Preferences__c = JSON.serialize(this.userPreferences);

        AMSContactService.updateContact(currentContact, new List<String> { 'AMS_User_Preferences__c' });
    }

    public String getFarmIdParameterName() {
        return AMSCommunityUtil.getFarmIdParameterName();
    }

    public static String getFAQParameterName() {
        return AMSCommunityUtil.getFAQParameterName();
    }

    public static String getAppStoreLink() {
        return AMSCommunityUtil.getAppleStoreLink();
    }

    public static String getGoogleStoreLink() {
        return AMSCommunityUtil.getGoogleStoreLink();
    }

    public static Integer getFarmsPerPage() {
        return FARMS_PER_PAGE;
    }

    public AMSCollectionService.UnitOfMeasurement getKgMS() {
        return AMSCollectionService.KgMS;
    }

    public AMSCollectionService.UnitOfMeasurement getLitres() {
        return AMSCollectionService.Litres;
    }

    public AMSCollectionService.UnitOfMeasurement getBMCC() {
        return AMSCollectionService.BMCC;
    }

    public AMSCollectionService.UnitOfMeasurement getOtherTestResult() {
        return AMSCollectionService.OtherTestResult;
    }
}