global class Prizm_AdvanceTransactionTriggerWrapper extends fsCore.TriggerWrapperBase {
    private static final String CLASS_NAME = 'Prizm_AdvanceTransactionTriggerWrapper';
    private static final String TRIGGER_NAME = 'Prizm_AdvanceTransactionTrigger';
    private Prizm_AdvanceTransactionTriggerWrapper() {
        super.setTriggerDetails(TRIGGER_NAME, fsCore.Constants.CUSTOM);
    }
    private static Prizm_AdvanceTransactionTriggerWrapper mInstance = null;
    /* Method to get the only instance available */
    global static Prizm_AdvanceTransactionTriggerWrapper getInstance() {
        if (mInstance == null)
            mInstance = new Prizm_AdvanceTransactionTriggerWrapper();
        return mInstance;
    }
    global override void beforeInsert(List<sObject> pNewRecList) {
    }
    global override void beforeUpdate(
        List<sObject> pNewRecList,
        List<sObject> pOldRecList,
        Map<Id, sObject> pNewRecMap,
        Map<Id, sObject> pOldRecMap
    ) {
    }
    global override void beforeDelete(List<sObject> pOldRecList, Map<Id, sObject> pOldRecMap) {
    }
    global override void afterInsert(List<sObject> pNewRecList, Map<Id, sObject> pNewRecMap) {
    }
    global override void afterUpdate(
        List<sObject> pNewRecList,
        List<sObject> pOldRecList,
        Map<Id, sObject> pNewRecMap,
        Map<Id, sObject> pOldRecMap
    ) {
    }
    global override void afterDelete(List<sObject> pOldRecList, Map<Id, sObject> pOldRecMap) {
    }
    global override void afterUndelete(List<sObject> pNewRecList, Map<Id, sObject> pNewRecMap) {
    }
}