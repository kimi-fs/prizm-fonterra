/**
 * Description: Test for AMSEmailVerificationController
 * @author: Amelia (Trineo)
 * @date: July 2017
 */
@isTest
private class AMSEmailVerificationControllerTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';

    private static final Map<String, Community_Page_Messages__c> PAGE_MESSAGES = new Map<String, Community_Page_Messages__c>();
    static {
        String PAGE_NAME = 'AMSEmailVerification';
        PAGE_MESSAGES.put('email-sent', new Community_Page_Messages__c(Name = PAGE_NAME + '1', Page__c = PAGE_NAME, Reason__c = 'email-sent', Message__c = 'email-sent'));
        PAGE_MESSAGES.put('unknown-error', new Community_Page_Messages__c(Name = PAGE_NAME + '2', Page__c = PAGE_NAME, Reason__c = 'unknown-error', Message__c = 'unknown-error'));
    }

    @testSetup static void testSetup() {
        upsert PAGE_MESSAGES.values();
        TestClassDataUtil.individualDefaultAccountAU();
    }

    @isTest static void testGetUserEmailDomain() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id = : communityUser.ContactId LIMIT 1].Encrypted_Username__c;

        PageReference currentPage = Page.AMSEmailVerification;
        currentPage.getParameters().put(AMSCommunityUtil.getUserIdParameterName(), encryptedUsername);
        System.Test.setCurrentPage(currentPage);

        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            String emailSuffix = controller.getUserEmailDomain();
            System.assertEquals('@thisTest.com'.toLowerCase(), emailSuffix.toLowerCase(), 'Email suffix not correct');
        }
    }

    @isTest static void testCheckLogin_WithLoggedInUser() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            System.assertEquals(String.valueOf(Page.AMSLanding), String.valueOf(controller.checkLogin()), 'User should be logged in');
        }
    }
    @isTest static void testCheckLogin_WithNotLoggedInUser() {
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            System.assertEquals(null, controller.checkLogin(), 'User should not be logged in');
        }
    }

    @isTest static void testRedirectIfUserHasAccess_EmailVerified() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id = : communityUser.ContactId LIMIT 1].Encrypted_Username__c;
        System.runAs([SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1][0]) {
            communityUser.Email_Verified__c = Datetime.now();
            update communityUser;
        }

        PageReference currentPage = Page.AMSEmailVerification;
        currentPage.getParameters().put(AMSCommunityUtil.getUserIdParameterName(), encryptedUsername);
        System.Test.setCurrentPage(currentPage);

        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(communityUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            System.assertEquals(String.valueOf(Page.AMSFarmDetails), String.valueOf(controller.redirectIfUserHasAccessAndLoggedIn()), 'Page did not redirect');
        }
    }

    @isTest static void testRedirectIfUserHasAccess_NotEmailVerified() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id = : communityUser.ContactId LIMIT 1].Encrypted_Username__c;

        PageReference currentPage = Page.AMSEmailVerification;
        currentPage.getParameters().put(AMSCommunityUtil.getUserIdParameterName(), encryptedUsername);
        System.Test.setCurrentPage(currentPage);

        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            System.assertEquals(null, controller.redirectIfUserHasAccessAndLoggedIn(), 'Page did a redirect');
        }
    }

    @isTest static void testSetVerifiedEmail_LoggedInUser_EmailVerified() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id = : communityUser.ContactId LIMIT 1].Encrypted_Username__c;
        System.runAs([SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1][0]) {
            communityUser.Email_Verified__c = Datetime.now();
            update communityUser;
        }

        System.runAs(communityUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            PageReference pr = controller.setVerifiedEmail();
            System.assertEquals(String.valueOf(Page.AMSFarmDetails), String.valueOf(pr), 'Page did not redirect');
        }
    }

    @isTest static void testSetVerifiedEmail_NotLoggedInUser_EmailVerified() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id = : communityUser.ContactId LIMIT 1].Encrypted_Username__c;
        System.runAs([SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1][0]) {
            communityUser.Email_Verified__c = Datetime.now();
            update communityUser;
        }

        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            PageReference pr = controller.setVerifiedEmail();
            System.assertEquals(null, String.valueOf(pr), 'Page did a redirect');
        }
    }

    @isTest static void testSetVerifiedEmail_NotLoggedInUser_EmailNotVerified() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id = : communityUser.ContactId LIMIT 1].Encrypted_Username__c;

        PageReference currentPage = Page.AMSEmailVerification;
        currentPage.getParameters().put(AMSCommunityUtil.getUserIdParameterName(), encryptedUsername);
        System.Test.setCurrentPage(currentPage);

        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            PageReference pr = controller.setVerifiedEmail();
            System.assert(String.valueOf(pr).toLowerCase().contains('login'), 'Page did not redirect');
        }
        System.assert([SELECT Email_Verified__c FROM User WHERE Id =: communityUser.Id].Email_Verified__c != null, 'Email verified was not updated');
    }

    @isTest static void testSetVerifiedEmail_InvalidEncryptedUsername() {
        PageReference currentPage = Page.AMSEmailVerification;
        currentPage.getParameters().put(AMSCommunityUtil.getUserIdParameterName(), 'invalidEncryptedUsername');
        System.Test.setCurrentPage(currentPage);

        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            PageReference pr = controller.setVerifiedEmail();
            System.assertEquals(null, String.valueOf(pr), 'Page did a redirect');
        }
    }

    @isTest static void testSetVerifiedEmail_LoggedInUser_EmailNotVerified() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id = : communityUser.ContactId LIMIT 1].Encrypted_Username__c;

        System.runAs(communityUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            PageReference pr = controller.setVerifiedEmail();
            System.assertEquals(null, controller.setVerifiedEmail(), 'Page did redirect when user not verified');
        }
    }

    @isTest static void testGetUser_FromLoggedInUser() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            Test.startTest();
            User u = controller.getUser();
            Test.stopTest();
            System.assertEquals(communityUser.Id, u.Id, 'Logged in user id not correct');
        }
    }

    @isTest static void testGetUser_FromEncryptedUsername() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id = : communityUser.ContactId LIMIT 1].Encrypted_Username__c;

        PageReference currentPage = Page.AMSEmailVerification;
        currentPage.getParameters().put(AMSCommunityUtil.getUserIdParameterName(), encryptedUsername);
        System.Test.setCurrentPage(currentPage);

        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            Test.startTest();
            User u = controller.getUser();
            Test.stopTest();
            System.assertEquals(communityUser.Id, u.Id, 'User id not correct');
        }
    }

    @isTest static void testResendEmailVerification_ValidUser() {
        List<EmailTemplate> templates = [SELECT Id FROM EmailTemplate WHERE DeveloperName = : AMSEmailVerificationController.AMS_COMMUNITY_NEW_MEMEBER_EMAIL_TEMPLATE_DEVELOPERNAME];

        if (templates.size() == 0) {
            EmailTemplate e = new EmailTemplate(
                DeveloperName = AMSEmailVerificationController.AMS_COMMUNITY_NEW_MEMEBER_EMAIL_TEMPLATE_DEVELOPERNAME,
                TemplateType = 'Text',
                IsActive = true,
                FolderId = UserInfo.getOrganizationId(),
                Name = 'New member email'
            );
            insert e;
        }

        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            Test.startTest();
            controller.resendEmailVerification();
            Test.stopTest();
            System.assert((controller.message == PAGE_MESSAGES.get('email-sent').Message__c) || (controller.message.contains('NO_MASS_MAIL_PERMISSION')), 'Wrong page message ' + controller.message);
        }
    }

    @isTest static void testResendEmailVerification_InvalidUser() {
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser) {
            AMSEmailVerificationController controller = new AMSEmailVerificationController();
            Test.startTest();
            controller.resendEmailVerification();
            Test.stopTest();
            System.assert(controller.message.contains(PAGE_MESSAGES.get('unknown-error').Message__c), 'Did not show error message');
        }
    }
}