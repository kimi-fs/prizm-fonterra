global with sharing class Prizm_ContractCustomTriggerManager implements fsCore.TriggerManager{
    
    global void beforeInsert(List<sObject> pNewRecList) { }
    global void beforeUpdate(
        List<sObject> pNewRecList,
        List<sObject> pOldRecList,
        Map<Id, sObject> pNewRecMap,
        Map<Id, sObject> pOldRecMap
    ) {
        
    }
    global void beforeDelete(List<sObject> pOldRecList, Map<Id, sObject> pOldRecMap) {
    }
    global void afterInsert(List<sObject> pNewRecList, Map<Id, sObject> pNewRecMap){
        Prizm_ContractCustomTriggerHandler.createAdvanceRecs((Map<Id, fsServ__Lending_Contract__c>) pNewRecMap);
        Prizm_ContractCustomTriggerHandler.updateAdvanceAppWithContract((Map<Id, fsServ__Lending_Contract__c>) pNewRecMap);
    }
    global void afterUpdate(
        List<sObject> pNewRecList,
        List<sObject> pOldRecList,
        Map<Id, sObject> pNewRecMap,
        Map<Id, sObject> pOldRecMap ) {
            
        }
    global void afterDelete(List<sObject> pOldRecList, Map<Id, sObject> pOldRecMap) {
    }
    global void afterUndelete(List<sObject> pNewRecList, Map<Id, sObject> pNewRecMap) {
    }
}