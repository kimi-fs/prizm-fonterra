/*------------------------------------------------------------
 *  Author:         Sean Soriano
 *  Company:        Davanti Consulting
 *  Description:    Handler class for AddressTrigger
 *  Test Class:     AddressTrigger_Test
 *  ------------------------------------------------------------*/
public without sharing class AddressTriggerHandler extends TriggerHandler {

    private static final Set<Id> VALID_ENTITY_RECORD_TYPES = new Set<Id> {
        GlobalUtility.auAccountFarmRecordTypeId,
        GlobalUtility.auAccountPartyRecordTypeId,
        GlobalUtility.auAccountOrganisationRecordTypeId
    };

    public override void beforeInsert() {
        addressUpdateActive(Trigger.new);
    }
    public override void beforeUpdate() {
        addressUpdateActive(Trigger.new);
    }
    public override void afterDelete() {
        Map<Id, Address__c> oldAddressMap = (Map<Id, Address__c>) Trigger.oldMap;
        rollUpAddresses(Trigger.old, oldAddressMap);
    }
    public override void afterInsert() {
        Map<Id, Address__c> oldAddressMap = (Map<Id, Address__c>) Trigger.oldMap;
        rollUpAddresses(Trigger.new, oldAddressMap);
    }
    public override void afterUpdate() {
        Map<Id, Address__c> oldAddressMap = (Map<Id, Address__c>) Trigger.oldMap;
        rollUpAddresses(Trigger.new, oldAddressMap);
    }

    public static void rollUpAddresses(List<Address__c> triggerNew, Map<Id, Address__c> oldMap) {
        List<Id> accountIds = new List<Id>();
        List<Id> contactIds = new List<Id>();
        List<Id> opportunityIds = new List<Id>();

        for (Address__c address : triggerNew) {
            // If we're creating or deleting an inactive one it won't be rolled up
            if (Trigger.isInsert) {
                if (address.Active__c == false) {
                    continue;
                }
            }
            if (Trigger.isDelete) {
                if (address.Active__c == false) {
                    continue;
                }
            }

            // If the lookup values have changed we need to roll up the other records also
            if (Trigger.isUpdate) {
                if (oldMap.containsKey(address.Id)) {
                    Address__c oldAddress = oldMap.get(address.Id);
                    if (oldAddress.Entity__c != address.Entity__c) {
                        accountIds.add(oldAddress.Entity__c);
                    }
                    if (oldAddress.Individual__c != address.Individual__c) {
                        contactIds.add(oldAddress.Individual__c);
                    }
                    if (oldAddress.Opportunity__c != address.Opportunity__c) {
                        opportunityIds.add(oldAddress.Opportunity__c);
                    }
                }
            }

            accountIds.add(address.Entity__c);
            contactIds.add(address.Individual__c);
            opportunityIds.add(address.Opportunity__c);
        }

        List<sObject> recordsToSave = new List<sObject>();
        recordsToSave.addAll((List<sObject>)rollUpAddressToAccount(accountIds));
        recordsToSave.addAll((List<sObject>)rollUpAddressToContact(contactIds));
        recordsToSave.addAll((List<sObject>)rollUpAddressToOpportunity(opportunityIds));

        Database.DMLOptions dmlRecords = new Database.DMLOptions();
        dmlRecords.allowFieldTruncation = true;
        dmlRecords.optAllOrNone = false;

        List<Database.SaveResult> results = Database.update(recordsToSave, dmlRecords);
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                System.debug('Successfully updated: ' + sr.getId());
            } else {
                for (Database.Error err : sr.getErrors()) {
                    System.debug('Error updating record: ' + err.getStatusCode() + ' - ' + err.getMessage() + 'Fields: ' + err.getFields());
                }
            }
        }
    }

    private static List<Account> rollUpAddressToAccount(List<Id> accountIds) {
        List<Account> accounts = AddressService.getRelatedAddressesForAccounts(accountIds, VALID_ENTITY_RECORD_TYPES);
        List<Account> recordsToUpdate = new List<Account>();

        for (Account account : accounts) {
            Map<Id, List<Address__c>> addressRecordTypeMap = createAddressRecordTypeMap(account.Addresses__r);

            Address__c rollUpPostalAddress = determineAddressToRollUp(addressRecordTypeMap, GlobalUtility.postalAddressRecordTypeId);
            Account recordToUpdate = new Account(Id = account.Id);
            recordToUpdate.ShippingState = AddressService.getState(rollUpPostalAddress);
            recordToUpdate.ShippingStreet = AddressService.getStreet(rollUpPostalAddress);
            recordToUpdate.ShippingCity = AddressService.getCity(rollUpPostalAddress);
            recordToUpdate.ShippingCountry = AddressService.getCountry(rollUpPostalAddress);
            recordToUpdate.ShippingPostalCode = AddressService.getPostCode(rollUpPostalAddress);
            recordToUpdate.Postal_Address_ID__c = AddressService.getAddressID(rollUpPostalAddress);
            recordToUpdate.Postal_Address_Latest_End_Date__c = AddressService.getEndDate(rollUpPostalAddress);

            Address__c rollUpPhysicalAddress = determineAddressToRollUp(addressRecordTypeMap, GlobalUtility.physicalAddressRecordTypeId);
            recordToUpdate.BillingState = AddressService.getState(rollUpPhysicalAddress);
            recordToUpdate.BillingStreet = AddressService.getStreet(rollUpPhysicalAddress);
            recordToUpdate.BillingCity = AddressService.getCity(rollUpPhysicalAddress);
            recordToUpdate.BillingCountry = AddressService.getCountry(rollUpPhysicalAddress);
            recordToUpdate.BillingPostalCode = AddressService.getPostCode(rollUpPhysicalAddress);
            recordToUpdate.Physical_Address_ID__c = AddressService.getAddressID(rollUpPhysicalAddress);
            recordToUpdate.Physical_Address_Latest_End_Date__c = AddressService.getEndDate(rollUpPhysicalAddress);

            recordsToUpdate.add(recordToUpdate);
        }

        return recordsToUpdate;
    }

    private static List<Contact> rollUpAddressToContact(List<Id> contactIds) {
        List<Contact> records = AddressService.getRelatedAddressesForContacts(contactIds);
        List<Contact> recordsToUpdate = new List<Contact>();

        for (Contact contact : records) {
            Map<Id, List<Address__c>> addressRecordTypeMap = createAddressRecordTypeMap(contact.Addresses__r);

            Address__c rollUpPostalAddress = determineAddressToRollUp(addressRecordTypeMap, GlobalUtility.postalAddressRecordTypeId);
            Contact recordToUpdate = new Contact(Id = contact.Id);
            recordToUpdate.MailingState = AddressService.getState(rollUpPostalAddress);
            recordToUpdate.MailingStreet = AddressService.getStreet(rollUpPostalAddress);
            recordToUpdate.MailingCity = AddressService.getCity(rollUpPostalAddress);
            recordToUpdate.MailingCountry = AddressService.getCountry(rollUpPostalAddress);
            recordToUpdate.MailingPostalCode = AddressService.getPostCode(rollUpPostalAddress);
            recordToUpdate.Postal_Address_ID__c = AddressService.getAddressID(rollUpPostalAddress);
            recordToUpdate.Postal_Address_Latest_End_Date__c = AddressService.getEndDate(rollUpPostalAddress);

            if (rollUpPostalAddress != null) {
                recordToUpdate.Verified_No_Postal_Address__c = false;
            }

            recordsToUpdate.add(recordToUpdate);
        }

        return recordsToUpdate;
    }

    private static List<Opportunity> rollUpAddressToOpportunity(List<Id> opportunityIds) {
        List<Opportunity> records = AddressService.getRelatedAddressesForOpportunities(opportunityIds);
        List<Opportunity> recordsToUpdate = new List<Opportunity>();

        for (Opportunity opportunity : records) {
            Map<Id, List<Address__c>> addressRecordTypeMap = createAddressRecordTypeMap(opportunity.Addresses__r);

            Address__c rollUpPhysicalAddress = determineAddressToRollUp(addressRecordTypeMap, GlobalUtility.physicalAddressRecordTypeId);
            Opportunity recordToUpdate = new Opportunity(Id = opportunity.Id);
            recordToUpdate.State__c = AddressService.getState(rollUpPhysicalAddress);
            recordToUpdate.Street__c = AddressService.getStreet(rollUpPhysicalAddress);
            recordToUpdate.City__c = AddressService.getCity(rollUpPhysicalAddress);
            recordToUpdate.Postcode__c = AddressService.getPostCode(rollUpPhysicalAddress);
            recordToUpdate.Physical_Address_ID__c = AddressService.getAddressID(rollUpPhysicalAddress);
            recordToUpdate.Physical_Address_Latest_End_Date__c = AddressService.getEndDate(rollUpPhysicalAddress);

            recordsToUpdate.add(recordToUpdate);
        }

        return recordsToUpdate;
    }

    /**
     * Creates a map of RecordTypeId to list of Address__c
     */
    private static Map<Id, List<Address__c>> createAddressRecordTypeMap(List<Address__c> addressList) {
        Map<Id, List<Address__c>> addressRecordTypeMap = new Map<Id, List<Address__c>>();
        for (Address__c address : addressList) {
            if (addressRecordTypeMap.containsKey(address.RecordTypeId)) {
                addressRecordTypeMap.get(address.RecordTypeId).add(address);
            } else {
                addressRecordTypeMap.put(address.RecordTypeId, new List<Address__c> { address });
            }
        }

        return addressRecordTypeMap;
    }

    /**
     * Gets the list of the addresses for the relevant record type and gets the address to roll up
     */
    private static Address__c determineAddressToRollUp(Map<Id, List<Address__c>> addressRecordTypeMap, Id recordTypeId) {
        Address__c rollUpAddress;
        if (addressRecordTypeMap.containsKey(recordTypeId)) {
            rollUpAddress = determineAddressToRollUp(addressRecordTypeMap.get(recordTypeId));
        }

        return rollUpAddress;
    }

    /**
     * Creates a list of wrappers, sorts them and returns the one at the start of the list
     */
    private static Address__c determineAddressToRollUp(List<Address__c> addresses) {
        List<AddressWrapper> addressWrappersToSort = new List<AddressWrapper>();
        for (Address__c address : addresses) {
            addressWrappersToSort.add(new AddressWrapper(address));
        }

        if (addressWrappersToSort.isEmpty()) {
            return null;
        } else {
            addressWrappersToSort.sort();

            return addressWrappersToSort[0].address;
        }
    }

    public class AddressWrapper implements Comparable {
        public Address__c address { get; set; }

        public AddressWrapper(Address__c address) {
            this.address = address;
        }

        /*
         * ADDRESS COPY RULES:
         * If Number of Active Postal/Physical Address Records is 0: All Fields Blank - NOTE:Applies if NEVER more than 0 addresses, or if change from 1 active address to 0.
         * If Number of Active Postal/Physical Address Records is 1:Same as the active postal/physical address record.
         * Note: Active means where the start date is equal to or earlier than today, AND (the end date is in the future, or today. OR there is no end date).
         * If Number of Active Postal/Physical Address Records is More than 1: Same as the most-recent active postal/physical address record. Where most-recent is defined as the active record with the most-future end date, or no end date.
         * Note: If more than one active record with no end date exists, use the most-recently-created active record with no end date.
         */
        public Integer compareTo(Object compareTo) {
            AddressWrapper compareToAddress = (AddressWrapper)compareTo;
            if (this.address.End_Date__c == compareToAddress.address.End_Date__c) {
                return compareDates(this.address.CreatedDate, compareToAddress.address.CreatedDate);
            } else {
                return compareDates(this.address.End_Date__c, compareToAddress.address.End_Date__c);
            }
        }

        // If a date is null it is further in the future than a non null date
        private Integer compareDates(Datetime thisDate, Datetime otherDate) {
            if (thisDate == otherDate) {
                return 0;
            }
            if (thisDate != null && otherDate == null) {
                return 1;
            }
            if (thisDate == null && otherDate != null) {
                return -1;
            }
            if (thisDate < otherDate) {
                return 1;
            }

            return -1;
        }
    }

    // SET ADDRESS TO ACTIVE OR INACTIVE for Postal and Physical Addresses
    public static void addressUpdateActive(List<Address__c> lstAddresses) {
        Date dtToday = system.today();

        for (Address__c adr : lstAddresses) {
            if (adr.RecordTypeID == GlobalUtility.physicalAddressRecordTypeId || adr.RecordTypeID == GlobalUtility.postalAddressRecordTypeId) {
                system.debug('##TODAY:' + dtToday);
                system.debug('##START:' + adr.Start_Date__c);
                system.debug('##END:' + adr.End_Date__c);
                if (dtToday >= adr.Start_Date__c && (dtToday <= adr.End_Date__c || adr.End_Date__c == null)) {
                    adr.Active__c = true;
                } else if (dtToday < adr.Start_Date__c || dtToday > adr.End_Date__c) {
                    adr.Active__c = false;
                }
                system.debug('##ACTIVE ADDRESS: ' + adr);
            }
        }
    }

}