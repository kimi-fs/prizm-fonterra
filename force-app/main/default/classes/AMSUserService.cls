/**
 * Service for accessing user fields
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately.
 *
 * Author: Amelia (Trineo)
 * Date: July 2017
 */
public without sharing class AMSUserService {

    public static final List<String> USER_FIELDS_TO_QUERY = new List<String> {  'Alias',
                                                                                'AMS_Notifications_Last_Clicked__c',
                                                                                'BannerPhotoUrl',
                                                                                'CommunityNickname',
                                                                                'Connected_App_Access__c',
                                                                                'Contact.AMS_Landing_Page__c',
                                                                                'Contact.FarmSourceONE_Setup_Complete__c',
                                                                                'Contact.FirstName',
                                                                                'Contact.Preferred_Name__c',
                                                                                'Contact.Name',
                                                                                'Contact_Farm_Source_Id__c',
                                                                                'ContactId',
                                                                                'CreatedDate',
                                                                                'Email',
                                                                                'Email_Verified__c',
                                                                                'First_Password_Change__c',
                                                                                'First_Time_Setup_Complete__c',
                                                                                'First_Time_Setup_Complete_DateTime__c',
                                                                                'FirstName',
                                                                                'FullPhotoUrl',
                                                                                'Id',
                                                                                'IsActive',
                                                                                'IsProfilePhotoActive',
                                                                                'LastLoginDate',
                                                                                'LastModifiedDate',
                                                                                'LastName',
                                                                                'MiddleName',
                                                                                'MobilePhone',
                                                                                'Name',
                                                                                'Phone',
                                                                                'SmallPhotoUrl',
                                                                                'UserName',
                                                                                'UserPreferencesShowProfilePicToGuestUsers'};

    public static User queryUser(String queryCondition) {
        String query = 'SELECT ' + String.join(USER_FIELDS_TO_QUERY, ',') + ' FROM User ';
        query += queryCondition;
        query += ' AND Contact.RecordTypeId = \'' + GlobalUtility.individualAURecordTypeId + '\'';
        query += ' LIMIT 1 ';
        List<User> u = Database.query(query);
        return !u.isEmpty() ? u[0] : null;
    }

    public static User getUserFromEmailAddress(String email) {
        email = (email != null) ? String.escapeSingleQuotes(email) : null;
        return queryUser(' WHERE Email = \'' + email + '\'');
    }

    public static User getUser() {
        return queryUser(' WHERE Id = \'' + UserInfo.getUserId() + '\'');
    }

    public static User getUser(String encryptedUsername) {
        encryptedUsername = (encryptedUsername != null) ? String.escapeSingleQuotes(encryptedUsername) : null;
        return queryUser(' WHERE Contact.Encrypted_Username__c = \'' + encryptedUsername + '\'');
    }

    public static User getUserFromContactId(String contactId) {
        contactId = (contactId != null) ? String.escapeSingleQuotes(contactId) : null;
        return queryUser(' WHERE ContactId = \'' + contactId + '\'');
    }

    public static Boolean updateEmailVerified(String encryptedUsername) {
        List<User> u = new List<User>([SELECT Id, Username, ContactId, Email_Verified__c FROM User WHERE Contact.Encrypted_Username__c = : encryptedUsername LIMIT 1]);
        if (!u.isEmpty()) {
            updateEmailVerified(u[0]);
            return true;
        } else {
            return false;
        }
    }

    public static void updateEmailVerified(User u) {
        u.Email_Verified__c = Datetime.now();
        update u;

    }

    public static Boolean checkVerified(String encryptedUsername) {
        Boolean result = false;
        if (String.isNotBlank(encryptedUsername)) {
            List<User> u = [SELECT Id, Email_Verified__c FROM User WHERE Contact.Encrypted_Username__c = : encryptedUsername LIMIT 1];
            result = !u.isEmpty() ? u[0].Email_Verified__c != null : false;
        }
        return result;
    }

    public static Boolean checkVerified() {
        List<User> u = [SELECT Id, Email_Verified__c, Profile.UserLicense.LicenseDefinitionKey FROM User WHERE Id = : UserInfo.getUserId() LIMIT 1];
        Boolean result = !u.isEmpty() ? (u[0].Profile.UserLicense.LicenseDefinitionKey == 'SFDC' || u[0].Email_Verified__c != null) : false;
        return result;
    }

    public static String getAMSLandingPage(String userId) {
        List<User> u = [SELECT Id, Contact.AMS_Landing_Page__c FROM User WHERE Id = :userId LIMIT 1];
        return !u.isEmpty() ? u[0].Contact.AMS_Landing_Page__c : null;
    }

    public static String getUserEmailAddress(String encryptedUsername) {
        List<User> u = [SELECT Id, Email FROM User WHERE Contact.Encrypted_Username__c = : encryptedUsername LIMIT 1];
        return !u.isEmpty() ? u[0].Email : null;
    }

    public static String getEncryptedUsernameFromId(String userId) {
        List<User> u = [SELECT Id, Contact.Encrypted_Username__c FROM User WHERE Id = : userId LIMIT 1];
        return !u.isEmpty() ? u[0].Contact.Encrypted_Username__c : null;
    }

    public static Boolean getUserPasswordLocked(String userId) {
        List<UserLogin> ul = [SELECT IsPasswordLocked FROM UserLogin WHERE UserId = :userId];
        return !ul.isEmpty() ? ul[0].IsPasswordLocked : false;
    }

    public static Boolean checkUserLoggedIn() {
        return UserInfo.getUserType() != 'Guest';
    }

    public static Contact getContactFromEmail(String email) {
        List<Contact> c = [SELECT Id, Email FROM Contact WHERE Email = : email LIMIT 1];
        return !c.isEmpty() ? c[0] : null;
    }

    public static String getEmailDomain(String email) {
        if (email.contains('@')) {
            return String.isNotBlank(email) ? '@' + email.split('@')[1] : '';
        } else {
            return email;
        }
    }

    public static void updateUser(User user) {
        update user;
    }

    /**
     * Update a user, but only update the fields you specifically want to update
     *
     * Grabs the values from the user and puts it on a new user with only the id populated, then updates the user
     */
    public static void updateUser(User user, List<String> fieldsToUpdate) {
        User userToUpdate = new User(Id = user.Id);

        for (String fieldToUpdate : fieldsToUpdate) {
            userToUpdate.put(fieldToUpdate, user.get(fieldToUpdate));
        }

        update userToUpdate;
    }

    public static User createUserFromContact(Contact c) {
        User u = new User();
        u.Username = getUserName();
        u.Email = c.Email;
        u.FirstName = c.FirstName;
        u.LastName = c.LastName;
        u.Phone = c.Phone;
        u.CommunityNickname = createCommunityNickname(c.firstname, c.lastname);
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.ContactId = c.Id;

        u.EmailEncodingKey = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_AU';

        String alias = c.FirstName != null ? c.FirstName.subString(0, 1) + c.LastName : c.LastName;
        u.Alias = alias.left(8);

        Profile amsCommunityUserProfile = [ SELECT Id FROM Profile WHERE Name = : 'AMS Community User' ];
        u.ProfileId = amsCommunityUserProfile.Id;

        insert u;
        return u;
    }

    public static Boolean anotherUserHasEmail(String email) {
        List<User> u = [SELECT Id, Email, Email_Verified__c, ContactId FROM User WHERE (Email = : email OR Contact.Email = : email) AND Profile.Name = 'AMS Community User'];
        if (u.isEmpty()) {
            return false;
        } else {
            if (u.size() == 1) {
                return u[0].Id == UserInfo.getUserId() ? false : true;
            } else {
                return true;
            }
        }
    }

    public static String getWelcomeName(User user) {
        return String.isNotBlank(user.Contact.Preferred_Name__c) ? user.Contact.Preferred_Name__c : user.Contact.FirstName;
    }

    private static String createUserName() {
        return Label.Default_AMS_Community_Username_Prefix + getUniqueNumberForUsername() + Label.Default_AMS_Community_Username_Domain;
    }

    // Creates a record that has an autonumber field on it to get a unique number to be used in the username
    private static String getUniqueNumberForUsername() {
        Username_Unique_Number__c uniqueNumberRecord = new Username_Unique_Number__c();
        insert uniqueNumberRecord;
        String uniqueNumber = [SELECT Name FROM Username_Unique_Number__c WHERE Id = : uniqueNumberRecord.Id LIMIT 1].Name;
        delete uniqueNumberRecord;
        return uniqueNumber;
    }

    private static Boolean checkIfUniqueUserName(String username) {
        return [SELECT count() FROM User WHERE Username = : username] == 0;
    }

    public static String getUserName() {
        Boolean unique = false;
        String username;
        Integer countWatchdog = 0;
        // checks that the username created is unique, will only not be unique if a user with our format has been created manually so 99% of the time should only happen once
        while (!unique && countWatchdog <= 5) {
            username = createUserName();
            unique = checkIfUniqueUserName(username);
            countWatchdog ++;
        }
        if (!unique) {
            return null;
        }

        return username;
    }

    public static String createCommunityNickname(String firstname, String lastName) {
        String fullname = firstname + '.' + lastname;
        fullname = fullname.replaceAll( '\\s+', '');         // No whitespace
        fullname = fullname.replaceAll( '^[A-Za-z]', '');    // No special characters
        fullname = fullname.left(20);                        // Only the first 20
        String nickname = fullname + '.' + Datetime.now().getTime(); // Append time to make unique
        return nickname.left(40); // Ensure that it's no more than 40, SF limit
    }

    public static Boolean isValidPassword(String password) {
        Boolean valid = false;
        if (String.isNotBlank(password)) {
            // Checks for a minimum of 8 chars, 1 capital, 1 lower, 1 number, no spacees
            Pattern passwordPattern = Pattern.compile('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?!.*\\s)..{7,}$');
            Matcher passwordMatcher = passwordPattern.matcher(password);
            if (passwordMatcher.matches()) {
                valid = true;
            }
        }
        return valid;
    }

    //this method from FonterraCommunityUserService is totally bloody bonkers- the exception message thrown has nothing to do with the actual error
    //and the AMS usage catches the error and rewords it. Bonkers.
    public static void validateEmail(String newEmail) {
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,8}$'; // source: http://www.regular-expressions.info/email.html
        Pattern emailPattern = Pattern.compile(emailRegex);
        Matcher emailMatcher = emailPattern.matcher(newEmail);

        if (!emailMatcher.matches()){
            throw new ExceptionService.CustomException('Invalid e-mail format');
        }
    }

    public static Boolean passwordMatches(String password, String confirmPassword) {
        return String.isNotBlank(password) && (password == confirmPassword);
    }

    /**
    * Gets the page to redirect the user to for first time registration
    * If they user has primary access to any farms they go to AMSMyDetails
    * If not they go to AMSFarmDetails
    */
    public static PageReference firstTimeRegistrationNextPage(User user) {
        PageReference nextPage;

        Boolean userHasPrimaryAccess = checkIfUserHasPrimaryAccess(user);
        if (userHasPrimaryAccess) {
            nextPage = Page.AMSMyDetails;
        } else {
            nextPage = Page.AMSFarmDetails;
        }

        return nextPage;
    }

    private static Boolean checkIfUserHasPrimaryAccess(User user) {
        Boolean userHasPrimaryAccess = false;

        if (user != null && user.ContactId != null) {
            List<Individual_Relationship__c> primaryAccessFarms = RelationshipAccessService.derivedRelationshipsForOwnedFarms(user.ContactId);
            userHasPrimaryAccess = primaryAccessFarms.isEmpty() ? false : true;
        }

        return userHasPrimaryAccess;
    }

    /**
    * Checks to see if the running user has View_AMS_Community custom permission
    * Used in AMSContactService to only allow users with this permission to view the community as another individual
    */
    public static Boolean checkIfUserCanViewCommunity() {
        return GlobalUtility.checkUserHasCustomPermission('View_AMS_Community');
    }

}