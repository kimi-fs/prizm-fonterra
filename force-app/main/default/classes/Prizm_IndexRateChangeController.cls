public class Prizm_IndexRateChangeController {
   
	@AuraEnabled
    public static Id executeRateChangeJob(){
        Id batchJobId = Database.executeBatch(new Prizm_RateChangeJob(), 1);
        return batchJobId;
    
    }
    
    @AuraEnabled
    public static boolean isApprovalDone(Id pIndexRateId)
    {
        fsCore__Index_Rate_Setup__c indexRate = [SELECT Id,Is_Approved__c FROM fsCore__Index_Rate_Setup__c WHERE Id = :pIndexRateId];
        boolean isApproved = indexRate.Is_Approved__c;
        return isApproved;
    }
    

}