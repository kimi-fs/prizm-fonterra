public class GlobalUtility {


    // Account Record Type Names
    public static final String auAccountFarmRecordTypeName = 'Farm AU';
    public static final String auAccountPartyRecordTypeName = 'Party AU';
    public static final String auOrganisationAccountRecordTypeName = 'Organisation AU';
    public static final String entityRecordTypeName = 'Entity';

    //Account Record Type Ids
    public static Id entityRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(entityRecordTypeName).getRecordTypeId();
    public static Id auAccountFarmRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(auAccountFarmRecordTypeName).getRecordTypeId();
    public static Id auAccountPartyRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(auAccountPartyRecordTypeName).getRecordTypeId();
    public static Id auAccountOrganisationRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(auOrganisationAccountRecordTypeName).getRecordTypeId();

    //Case Record Type Ids
    public static Id caseDigitalServicesAURecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Digital Services AU').getRecordTypeId();
    public static Id caseFormATSApplicationRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('ATS Application').getRecordTypeId();
    public static Id caseCentralSchedulingAURecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Central Scheduling AU').getRecordTypeId();
    public static Id caseManageSupplyAURecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Manage Supply AU').getRecordTypeId();
    public static Id caseFormAdvanceApplicationRecordTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Advance Application').getRecordTypeId();
    public static Id caseFormMilkCoolingPurchaseIncentiveRecordTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Form - Milk Cooling Purchase Incentive').getRecordTypeId();
    public static Id caseFormHerdTestingIncentiveRecordTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Form - Herd Testing Incentive').getRecordTypeId();
    public static Id caseFormABNUpdateRecordTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Form - ABN Update').getRecordTypeId();
    public static Id caseFormCeaseOrTransferSupplyRecordTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Form - Notification to Cease or Transfer').getRecordTypeId();
    public static Id caseFormSupplierInformationRecordTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Form - Supplier Information').getRecordTypeId();
    public static Id caseFormStructuredDeductionRecordTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Form - Structured Deduction').getRecordTypeId();
    public static Id caseFormSharefarmingArrangementRecordTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Form - Sharefarming Arrangement').getRecordTypeId();
    public static Id caseFormBankingInformationRecordTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Form - Banking Information').getRecordTypeId();
    public static Id caseFormNewSupplierRecordTypeId =  Schema.SObjectType.Case.getRecordTypeInfosByName().get('Form - New Supplier').getRecordTypeId();


    //Individual Relationship Record Type Ids
    public static Id derivedRelationshipRecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Derived Relationship').getRecordTypeId();
    public static Id individualPartyRecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Party').getRecordTypeId();
    public static Id individualFarmRecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Farm').getRecordTypeId();
    public static Id individualRestrictedFarmRecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Restricted-Farm').getRecordTypeId();
    public static Id individualOrganisationRecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Organisation').getRecordTypeId();

    //Statment Entry RecordType Ids
    public static Id paymentStatementEntryRecordTypeId = Schema.SObjectType.Statement_Entry__c.getRecordTypeInfosByName().get('Payment').getRecordTypeId();
    public static Id chargeStatementEntryRecordTypeId = Schema.SObjectType.Statement_Entry__c.getRecordTypeInfosByName().get('Charge').getRecordTypeId();
    public static Id deductionsStatementEntryRecordTypeId = Schema.SObjectType.Statement_Entry__c.getRecordTypeInfosByName().get('Deductions').getRecordTypeId();
    public static Id totalsStatementEntryRecordTypeId = Schema.SObjectType.Statement_Entry__c.getRecordTypeInfosByName().get('Totals').getRecordTypeId();

    // Collection Summary RecordType Ids
    public static Id monthlyCollectionSummaryRecordTypeId = Schema.SObjectType.Collection_Summary__c.getRecordTypeInfosByName().get('Monthly').getRecordTypeId();

    // Agreement RecordType Ids
    public static Id agreementStandardRecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Standard Agreement').getRecordTypeId();
    public static Id agreementContractSupplyAURecordTypeId = Schema.SObjectType.Agreement__c.getRecordTypeInfosByName().get('Contract Supply AU').getRecordTypeId();

    //Individual Relationship Record Type Developer Name
    public static String individualPartyRecordName = 'Individual_Party';

    //Contact RecordType ids
    public static Id individualAURecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Individual_AU').getRecordTypeId();

    // Entity Relationship Record Type Developer Names
    public static final String partyFarmRecordTypeDeveloperName = 'Party_Farm';

    // Entity Relationship Record Type Ids
    public static Id partyFarmRecordTypeId = Schema.SObjectType.Entity_Relationship__c.getRecordTypeInfosByDeveloperName().get(partyFarmRecordTypeDeveloperName).getRecordTypeId();

    // Entity Relationship Record Type Developer Name
    public static String partyFarmRecordName = 'Party_Farm';

    // Opportunity Record Type Ids
    public static Id oppFonterraAURecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Fonterra Opportunity AU').getRecordTypeId();

    // Address Record Type Ids
    public static Id postalAddressRecordTypeId = Schema.SObjectType.Address__c.getRecordTypeInfosByName().get('Postal').getRecordTypeId();
    public static Id physicalAddressRecordTypeId = Schema.SObjectType.Address__c.getRecordTypeInfosByName().get('Physical').getRecordTypeId();
    public static Id deliveryAddressRecordTypeId = Schema.SObjectType.Address__c.getRecordTypeInfosByName().get('Delivery').getRecordTypeId();

    // Channel Preference Setting Record Type Ids
    public static Id australiaChannelPreferenceSettingRecordTypeId = Schema.SObjectType.Channel_Preference_Setting__c.getRecordTypeInfosByName().get('Australia').getRecordTypeId();

    // Activity Event record type ids
    public static Id activityEventAppointmentRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Appointment').getRecordTypeId();

     // Activity Task record type ids
    public static Id activityTaskSystemGeneratedTaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('System Generated Task').getRecordTypeId();

    // Goal__c recordType Ids
    public static Id goalStrategicGoalRecordTypeId = Schema.SObjectType.Goal__c.getRecordTypeInfosByName().get('Strategic Goal').getRecordTypeId();

    // Season Record Types
    public static Id seasonContractAURecordTypeId = Schema.SObjectType.Season__c.getRecordTypeInfosByDeveloperName().get('Contract_AU').getRecordTypeId();

    // Knowledge Article Record Types
    public static Id amsNewsRecordTypeId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByDeveloperName().get('AMS_News').getRecordTypeId();
    public static Id amsFAQRecordTypeId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByDeveloperName().get('AMS_FAQ').getRecordTypeId();
    public static Id amsWebTextRecordTypeId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByDeveloperName().get('AMS_Web_Text').getRecordTypeId();
    public static Id amsAdviceRecordTypeId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByDeveloperName().get('AMS_Advice').getRecordTypeId();

    //Account Field Values
    public static final String INDUSTRY_SECTOR_PICKLIST_FONTERRA_SUPPLIER = '9000';
    public static final String CUSTOMER_CLASS_CUSTOMER_CLASSIFICATION = '04';
    public static final String ACCOUNT_ACTIVE_STATUS = 'ACTI';
    public static final String TERMS_OF_PAYMENT_20TH_OF_MONTH_FOLLOWING = '0001';
    public static final String CUSTOMER_GROUP_PICKLIST_MY_MILK = '10';
    public static final String CUSTOMER_GROUP_PICKLIST_FONTERRA_DAIRY = '01';
    public static final String CUSTOMER_GROUP_PICKLIST_DRY = '03';

    //Case Field Values
    public static final String CASE_RAISED_BY_INTERNAL= 'Internal';
    public static final String CASE_RAISED_BY_EXTERNAL= 'External';
    public static final String CASE_SENTIMENT_NA = 'NA';
    public static final String CASE_STATUS_IN_PROGRESS = 'In progress';
    public static final String CASE_STATUS_CLOSED = 'Closed';
    public static final String CASE_SUB_STATUS_INVESTIGATION_REQUIRED = 'Investigation Required';
    public static final String CASE_SUB_STATUS_COMPLETE = 'Complete';
    public static final String CASE_SUB_TYPE_CEASES = 'Ceases';
    public static final String CASE_SUB_TYPE_SUPPLIER_CHANGE = 'Supplier Change';
    public static final String CASE_SUB_TYPE_PASSWORD_AND_USERNAME = 'Password & Username';
    public static final String CASE_TYPE_ACCOUNT_CHANGE = 'Account Change';
    public static final String CASE_TYPE_ONBOARDING_OFFBOARDING = 'Onboarding/Offboarding';
    public static final String CASE_TYPE_OUTBOUND_COMMS = 'Outbound Comms';
    public static final String CASE_TYPE_WEBSITE = 'Website';
    public static final String CASE_ORIGIN_ASPIRE_RULES_ENGINE = 'Aspire Rules Engine';
    public static final String CASE_ORIGIN_PHONE = 'Phone';

    //Collection Field Values
    public static final String COLLECTION_PERIOD_ALERT_TEXT = 'Alert';
    public static final String COLLECTION_PERIOD_BMCC_TEST_NAME = 'Cell Count';
    public static final String COLLECTION_PERIOD_BACTOSCAN_TEST_NAME = 'Bactoscan';
    public static final String COLLECTION_PERIOD_THERMODURICS_TEST_NAME = 'Thermodurics';
    public static final String COLLECTION_PERIOD_TEMPERATURE_TEST_NAME = 'Temperature';

    // Email Regex
    public static String emailRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';

    // Mobile number Regex
    public static String mobileRegex = '^\\+([0-9]{1,3} [0-9]+ [0-9]+)$';

    public static String integrationProfileName() {
        String strIntegrationProfileName  = Field_Team_Tools_Settings__c.getValues('IntegrationUserProfile').Value__c;
        return strIntegrationProfileName;
    }

    public static String strPulsarNameSpace = 'LuminixPulsar';

    public static String strCongaNameSpace = 'APXTConga4';

    public static Id defaultAccountIdAU {
        get { return getFieldTeamToolsValue('IndividualDefaultAccountAU'); }
    }

    private static Id getFieldTeamToolsValue(String settingName) {
        Field_Team_Tools_Settings__c setting = Field_Team_Tools_Settings__c.getValues(settingName);
        return setting != null ? setting.Value__c : null;
    }

    public static Profile integrationProfile;

    // Get ids of users with integration user profile
    public static Set<Id> getIntegrationUserIds() {

        Set<Id> idsToReturn = new Set<Id>();
        String integrationUserProfileName = integrationProfileName();

        for(User usr : [SELECT Id FROM User WHERE Profile.Name = :integrationProfileName()]) {
            idsToReturn.add(usr.Id);
        }

        return idsToReturn;

    }

    // Get id of the integration user
    private static Id integrationUserId;
    public static Id getIntegrationUserId() {
        if (integrationUserId == null) {
            List<User> integrationUser = [SELECT Id FROM User WHERE Name = 'integration' LIMIT 1];
            if (!integrationUser.isEmpty()) {
                integrationUserId = integrationUser[0].Id;
            }

        }
        return integrationUserId;
    }

    // Get record type name from id
    public static String getRecordTypeNameById(String objectName, Id recordTypeId) {
        return Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosById().get(recordTypeId).getName();
    }

    public static String IR_PF_ROLE_OWNER = 'Owner';

    // For replacing text from email templates
    public static String TEXT_TO_BE_REPLACED = 'DO NOT MODIFY TEXT - REFERRED IN APEX CLASS';

    public static String ER_ROLE_SHAREMILKER = 'Share Milker';
    public static String ER_ROLE_SHAREMILKER_PREVIOUS = 'Share Milker (Previous)';
    public static String ER_ROLE_CONTRACTMILKER = 'Contract Milker';
    public static String ER_ROLE_CONTRACTMILKER_PREVIOUS = 'Contract Milker (Previous)';
    public static String ER_ROLE_SHAREMILKER_WITHOUT_SPACE = 'Sharemilker';
    public static String ER_ROLE_SHAREMILKER_WITHOUT_SPACE_PREVIOUS = 'Sharemilker (Previous)';
    public static String ER_ROLE_CONTRACTMILKER_WITHOUT_SPACE = 'Contractmilker';
    public static String ER_ROLE_CONTRACTMILKER_WITHOUT_SPACE_PREVIOUS = 'Contractmilker (Previous)';
    public static String ER_ROLE_CONTRACTMILKER_OWNER = 'Owner';
    public static String ER_ROLE_CONTRACTMILKER_OWNER_PREVIOUS = 'Owner (Previous)';
    public static String ER_ROLE_PAYER = 'Payer';

    public static String ER_ROLE_SHARE_FARMER = 'Share Farmer';
    public static String ER_ROLE_SHARE_FARMER_PREVIOUS = 'Share Farmer (Previous)';

    public static String IR_ROLE_DIRECTOR = 'Director';
    public static String IR_ROLE_PARTNER = 'Partner';
    public static String IR_ROLE_TRUSTEE = 'Trustee';
    public static String IR_ROLE_SOLE_TRADER = 'Sole Trader';
    public static String IR_ROLE_EXECUTOR = 'Executor';
    public static String IR_ROLE_POWER_OF_ATTORNEY = 'Power of Attorney';
    public static String IR_ROLE_SHAREHOLDER = 'Shareholder';
    public static String IR_ROLE_MEMBER = 'Member';

    public static final String FONTERRA_FIELD_TEAM = 'Field Team - Area Manager';
    public static final GlobalUtility.Stringify Stringify = new GlobalUtility.Stringify();

    // Org wide email display names
    public static String ORG_WIDE_EMAIL_FARM_SOURCE = 'Farm Source';
    public static String ORG_WIDE_EMAIL_MILK_SUPPLY_SERVICE_CENTRE = 'Milk Supply Service Centre';

    public static OrgWideEmailAddress ORG_WIDE_EMAIL_ADDRESS_FONTERRA_AU;
    static {
        Organization_Wide_Email_Address_Setting__mdt fonterraAUOrgWideEmailAddressName = [SELECT Value__c FROM Organization_Wide_Email_Address_Setting__mdt WHERE DeveloperName = 'Fonterra_Australia'];
        List<OrgWideEmailAddress> orgWideAddresses = [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress WHERE DisplayName = :fonterraAUOrgWideEmailAddressName.Value__c];
        ORG_WIDE_EMAIL_ADDRESS_FONTERRA_AU = (!orgWideAddresses.isEmpty()) ? orgWideAddresses[0] : null;
    }

    public static OrgWideEmailAddress ORG_WIDE_EMAIL_ADDRESS_FARM_SOURCE_AU;
    static {
        Organization_Wide_Email_Address_Setting__mdt auOrgWideEmailAddressName = [SELECT Value__c FROM Organization_Wide_Email_Address_Setting__mdt WHERE DeveloperName = 'Farm_Source_Australia'];
        List<OrgWideEmailAddress> orgWideAddresses =  [SELECT Id, Address, DisplayName FROM OrgWideEmailAddress WHERE DisplayName = :auOrgWideEmailAddressName.Value__c];
        ORG_WIDE_EMAIL_ADDRESS_FARM_SOURCE_AU = (!orgWideAddresses.isEmpty()) ? orgWideAddresses[0] : null;
    }

    // Reference Period Types
    public static String REFERENCE_PERIOD_FINANCIAL_YEAR = 'Financial Year';
    public static String REFERENCE_PERIOD_MILKING_SEASON = 'Milking Season';
    public static String REFERENCE_PERIOD_WINTER_SEASON = 'Winter Season';
    public static String REFERENCE_PERIOD_MILKING_SEASON_AU = 'Milking Season AU';

    // Reference Period Status
    public static String REFERENCE_PERIOD_STATUS_CURRENT = 'Current';
    public static String REFERENCE_PERIOD_STATUS_PAST = 'Past';
    public static String REFERENCE_PERIOD_STATUS_FUTURE = 'Future';

    // Milk Quality test names
    public static String TEMPERATURE_TEST_NAME = 'Temperature';
    public static String CELL_COUNT_TEST_NAME = 'Cell Count';
    public static String BACTOSCAN_TEST_NAME = 'Bactoscan';
    public static String THERMODURICS_TEST_NAME = 'Thermodurics';
    public static String SEDIMENT_TEST_NAME = 'Sediment';
    public static String ANTIBIOTICS_TEST_NAME = 'Antibiotics';

    //Payment split Type
    public static String PAYMENT_SPLIT_TYPE_BALANCE = 'Balance';
    public static String PAYMENT_SPLIT_TYPE_FIXED = 'Fixed';

    //Payment split payee Type
    public static String PAYMENT_SPLIT_PAYEE_TYPE_OWNER = 'Owner';
    public static String PAYMENT_SPLIT_PAYEE_TYPE_SHARE_MILKER = 'Share Milker';
    public static String PAYMENT_SPLIT_PAYEE_TYPE_CONTRACT_MILKER = 'Contract Milker';

    //set for getting ER Roles for Derived relationship logic in ER and IR triggers
    public static set<String> getRolesER(){
        set<String> setRolesER = new set<String>{ER_ROLE_SHAREMILKER, ER_ROLE_SHAREMILKER_PREVIOUS,
                                                 ER_ROLE_SHARE_FARMER, ER_ROLE_SHARE_FARMER_PREVIOUS,
                                                 ER_ROLE_CONTRACTMILKER, ER_ROLE_CONTRACTMILKER_PREVIOUS,
                                                 ER_ROLE_CONTRACTMILKER_OWNER, ER_ROLE_CONTRACTMILKER_OWNER_PREVIOUS};
        return setRolesER;
    }

    //set for getting IR Roles for Derived relationship logic in ER and IR triggers
    public static set<String> getRolesIR(){
        set<String> setRolesIR = new set<String> {IR_ROLE_DIRECTOR, IR_ROLE_PARTNER, IR_ROLE_TRUSTEE, IR_ROLE_SOLE_TRADER,IR_ROLE_EXECUTOR,
                                                  IR_ROLE_POWER_OF_ATTORNEY, IR_ROLE_SHAREHOLDER, IR_ROLE_MEMBER};
        return setRolesIR;
    }

    //set for getting IR Roles for sending to aspire for MOF (FNB-1233)
    public static Set<String> getRolesIRForMOF(){
        Set<String> setRolesIR = new Set<String> {IR_ROLE_DIRECTOR, IR_ROLE_PARTNER, IR_ROLE_TRUSTEE, IR_ROLE_SOLE_TRADER,IR_ROLE_EXECUTOR,
                                                  IR_ROLE_POWER_OF_ATTORNEY};
        return setRolesIR;
    }

    public static Set<String> OPEN_CASE_STATUSES = new Set<String>{'New', 'Open', 'In Progress ','Escalated'};

    public static Profile getIntegrationProfile() {
        if (integrationProfile == NULL && integrationProfileName() != NULL) {
            integrationProfile = [select Id from Profile where Name = :integrationProfileName() limit 1];
            return integrationProfile;
        } else return integrationProfile;
    }

    private static Map<String, Id> queueNameIdMap = new Map<String, Id>();
    public static Id getQueueIdFromDeveloperName(String developerName) {
        if (!queueNameIdMap.containsKey(developerName)) {
            List<Group> queue = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName = :developerName LIMIT 1];
            queueNameIdMap.put(developerName, queue[0].Id);
        }
        return queueNameIdMap.get(developerName);
    }

    public static Boolean checkUserHasCustomPermission(String permissionName) {
        return FeatureManagement.checkPermission(permissionName);
    }

    /**
     *  Description : Get the Layout Theme color of an Object given the Object Plural Label, the Tab Label, Theme, and Color Context
     */
    public static String getThemeColor(String objectLabel, String tabLabel, String theme, String colorContext) {
        // Loop through the Tabs, Object and Colors to search for the specific Theme Color
        List<Schema.DescribeTabSetResult> tabSetDesc = Schema.DescribeTabs();
        for (Schema.DescribeTabSetResult tsr : tabSetDesc) {

            if (tsr.getLabel().equals(tabLabel)) {
                for (Schema.DescribeTabResult res : tsr.getTabs()) {
                    if (res.getLabel().equals(objectLabel)) {
                        List<Schema.DescribeColorResult> colors = res.getColors();
                        for (Schema.DescribeColorResult color : colors) {
                            if (color.getTheme().equals(theme) && color.getContext().equals(colorContext)) {
                                return color.getColor();
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        return null;
    }

    /**
    * Converts a dd MMMM YYYY string to a Date
    */
    public static Date convertStringToDate(String dateString) {
        Date convertedDate;

        dateString = dateString.trim();
        List<String> dateSplit = dateString.split(' ');

        if (dateSplit.size() == 3 && dateSplit[0].isNumeric() && dateSplit[2].isNumeric()) {
            Map<String, Integer> months = new Map<String, Integer> {'January' => 1,
                                                                    'February' => 2,
                                                                    'March' => 3,
                                                                    'April' => 4,
                                                                    'May' => 5,
                                                                    'June' => 6,
                                                                    'July' => 7,
                                                                    'August' => 8,
                                                                    'September' => 9,
                                                                    'October' => 10,
                                                                    'November' => 11,
                                                                    'December' => 12 };

            Integer day = Integer.valueOf(dateSplit[0]);
            Integer month = months.get(dateSplit[1]);
            Integer year = Integer.valueOf(dateSplit[2]);

            convertedDate = Date.newInstance(year, month, day);
        }
        return convertedDate;
    }

    public static Boolean validateEmail(String email){
        Pattern emailPattern = Pattern.compile(emailRegex);
        Matcher emailMatcher = emailPattern.matcher(email);
        return emailMatcher != null && emailMatcher.matches();
    }

    public static Map<String, String> getFieldIdsForSObject(String nameObj) {
        String objectTypeForQuery = nameObj.replace('__c', '');
        List<FieldDefinition> fieldDefinitions;
        List<EntityDefinition> entityDefinitions = [SELECT Id, DurableId from EntityDefinition WHERE DeveloperName = :objectTypeForQuery];
        fieldDefinitions = [SELECT Id, DeveloperName, NamespacePrefix, DurableId FROM FieldDefinition WHERE EntityDefinitionId = :entityDefinitions[0].DurableId];

        Map<String, String> developerNameIdMap = new Map<String, String>();
        for (FieldDefinition fd : fieldDefinitions) {
            String fieldId = fd.DurableId.split('\\.')[1];
            developerNameIdMap.put(fd.DeveloperName, fieldId);
        }
        return developerNameIdMap;
    }

    public static Map<String, String> getPicklistLabelMap(String objectName, String fieldName) {
        Map<String, String> customerGroupLabelMap = new Map<String, String>();
        if (Schema.getGlobalDescribe().containsKey(objectName)) {

            SObjectType sObjectType = Schema.getGlobalDescribe().get(objectName);
            Map<String, Schema.SObjectField> fieldMap = sObjectType.getDescribe().fields.getMap();

            if (fieldMap.containsKey(fieldName)) {
                Schema.DescribeFieldResult fieldResult = fieldMap.get(fieldName).getDescribe();
                List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry v : values) {
                    customerGroupLabelMap.put(v.getValue(), v.getLabel());
                }
            } else {
                System.debug('Invalid Field API Name');
            }
        } else {
            System.debug('Invalid Object API Name');
        }
        return customerGroupLabelMap;
    }

    public class Stringify {
        public Stringify() {

        }

        public String listToString(List<String> stringList) {
            String ret = '';

            for (String s : stringList) {
                ret += s + '\n\n';
            }

            return ret;
        }

        public String convert(Boolean b) {
            return String.valueOf(b);
        }
        public String convert(Decimal d) {
            return String.valueOf(d);
        }

        public String convert(Integer i) {
            return String.valueOf(i);
        }
        public String convert(Date d) {
            return formatDate(d);
        }
        public String convertSlashed(Date d) {
            return formatDate(d, 'YYYY/MM/dd');
        }
        public String convert(Datetime dt) {
            return formatDatetime(dt);
        }

        public String convert(Date d, String format) {
            return formatDate(d, format);
        }

        private String formatDate(Date d) {
            if (d == NULL) {
                return NULL;
            }

            Datetime dt = Datetime.newInstance(d.year(), d.month(), d.day());
            return formatDate(dt);
        }

        private String formatDate(Datetime dt) {
            if (dt == NULL) {
                return NULL;
            }

            return dt.format('YYYY-MM-dd');
        }

        private String formatDate(Date d, String format) {
            if (d == NULL) {
                return NULL;
            }

            Datetime dt = Datetime.newInstance(d.year(), d.month(), d.day());
            return formatDate(dt, format);
        }

        private String formatDate(Datetime dt, String format) {
            if (dt == NULL) {
                return NULL;
            }

            return dt.format(format);
        }

        private String formatDatetime(Datetime dt) {
            if (dt == NULL) {
                return NULL;
            }

            return dt.format('YYYY-MM-dd\'T\'HH:mm:ss.SSSXXX');
        }

        public String formatDatetime(Datetime dt, String format) {
            if (dt == NULL) {
                return NULL;
            }

            return dt.format(format);
        }

        /*
        *   Author: John Au
        *   Company: Trineo
        *   Description: Method to turn exceptions into JSON Strings
        */

        public String jsonifyException(Exception e) {
            Map<String, Object> exceptionDetails = new Map<String, Object>();

            exceptionDetails.put('LineNumber', e.getLineNumber());
            exceptionDetails.put('Cause', e.getCause());
            exceptionDetails.put('Message', e.getMessage());
            exceptionDetails.put('StacktraceString', e.getStacktraceString());
            exceptionDetails.put('TypeName', e.getTypeName());

            return JSON.serializePretty(exceptionDetails);
        }
    }

    /**
     * Populate Record Type based on the value of recordTypeFieldAPIName
     * This must be run first in the before trigger because it assigns the correct record type for any new record
     * records must be one SObject not a mixture
    **/
    public static void setRecordTypeIdFromField(Schema.SObjectField recordTypeFieldAPIName, List<SObject> records) {

        if (!records.isEmpty()) {

            Map<String, Schema.RecordTypeInfo> recordTypesDeveloperNames = records[0].getsObjectType().getDescribe().getRecordTypeInfosByDeveloperName();
            Map<String, Schema.RecordTypeInfo> recordTypesNames = records[0].getsObjectType().getDescribe().getRecordTypeInfosByName();

            for (SObject newRecord : records) {

                String recordTypeName = String.valueOf(newRecord.get(recordTypeFieldAPIName));
                if (String.isNotBlank(recordTypeName)){

                    //Try to match record type on Developer name, fall back to Record to label if there is no match
                    if (recordTypesDeveloperNames.containsKey(recordTypeName)) {
                        newRecord.put('RecordTypeId', recordTypesDeveloperNames.get(recordTypeName).getRecordTypeId());
                    } else if (recordTypesNames.containsKey(recordTypeName)) {
                        newRecord.put('RecordTypeId', recordTypesNames.get(recordTypeName).getRecordTypeId());
                    }
                }
            }
        }
    }

    /**
     * Checks to a value has changed on a record.
     * Returns true if oldMap is null as this is designed to be used in triggers.
     */
    public static Boolean isChanged(Schema.SObjectField field, SObject newRecord, Map<Id, SObject> oldMap) {
        if (oldMap == null) {
            return true;
        }

        if (oldMap.containsKey(newRecord.Id)) {
            SObject oldRecord = oldMap.get(newRecord.Id);
            return newRecord.get(field) != oldRecord.get(field);
        }

        return false;
    }

    // // Get Multipurpose Internet Mail Extensions (MIME) type
    public static Map<String, String> getMIMETypeMap() {
        //The ContentVersion and ContentDocument objects don't store MIME types. The objects store filetypes which can be mapped to MIME types.
        Map<String, String> mimeTypeMap = new Map<String, String>();

        List<MIME_Type__mdt> mimeTypes = [SELECT DeveloperName, MIME_Type__c FROM MIME_Type__mdt];
        for (MIME_Type__mdt mimeType : mimeTypes) {
            mimeTypeMap.put(mimeType.DeveloperName, mimeType.MIME_Type__c);
        }

        return mimeTypeMap;
    }

    // email template details
    public static EmailTemplate getEmailTemplate(String developerName) {

        EmailTemplate emailTemplate = [SELECT
                                             Id,
                                             Subject,
                                             Description,
                                             HtmlValue,
                                             DeveloperName,
                                             Body
                                       FROM
                                             EmailTemplate
                                       WHERE DeveloperName = :developerName];

        return emailTemplate;
    }

    //The method used to compare two SObjects and look to see if there has been a change based on the passed List<Schema.SObjectField>
    public static Boolean objectFieldsMatch(SObject left, SObject right, List<Schema.SObjectField> fields) {
        for (Schema.SObjectField field : fields) {
            if (left.get(field) != right.get(field)) {
                return false;
            }
        }
        return true;
    }

}