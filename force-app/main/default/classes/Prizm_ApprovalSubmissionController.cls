/* 
* Class Name   - Prizm_ApprovalSubmissionController
*
* Description  - Action Class for Prizm_ApprovalRequestSubmission component
*
* Developer(s) - Financial Spectra
*/

public class Prizm_ApprovalSubmissionController {
    
    @AuraEnabled
    public static void submitForApproval(Id pIndexRateId){    
        
        List<Id> approverIds = new List<Id>();
       // List<User> usersList = [SELECT Id,Name FROM User where UserRole.Name= 'Milk Supply Services Manager'];
        List<User> usersList = [SELECT Id,Name FROM User where Name= 'Sourabh Mishra'];
        System.debug(loggingLevel.ERROR,'Approvers : '+ usersList);
        
        if(usersList.size()>0)
        {
        for(User approver: usersList)
        {
            approverIds.add(approver.id);  
        }
        }
        
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();  
        req.setComments('Submitting approval request using Request Approval Button');  
        req.setObjectId(pIndexRateId);
        req.setProcessDefinitionNameOrId('Rate_Approval');
        req.setNextApproverIds(approverIds);
     
        Approval.ProcessResult result = Approval.process(req); 
        
        System.debug(loggingLevel.ERROR, 'result : '+ result);
    }
    
    
    @AuraEnabled
    public static List<ProcessInstance> isRequestAlreadySubmitted(Id pIndexRateId){    
        List<ProcessInstance> submittedRequestsList = [SELECT Id FROM ProcessInstance WHERE TARGETOBJECTID =: pIndexRateId];
        return submittedRequestsList;
      }
    
}