/**
* Description:
* Sets the Record Type ID and creates data category selections for 'Industry News' articles coming from SAP PO
*
* @author: John Au (Trineo)
* @date: December 2020
* @test: KnowledgeTriggerHandlerTest
*/
public without sharing class KnowledgeTriggerHandler extends TriggerHandler {

    private static final String AMS_NEWS_DATA_CATEGORY_GROUP_NAME = 'AMS_News';

    public override void beforeInsert() {
        if (GlobalUtility.getIntegrationUserIds().contains(UserInfo.getUserId())) {
            setRecordTypeId(Trigger.new);
        }
    }

    public override void afterInsert() {
        if (GlobalUtility.getIntegrationUserIds().contains(UserInfo.getUserId())) {
            setDataCategories(Trigger.new);
        }
    }

    public static void setDataCategories(List<Knowledge__kav> amsNewsArticles) {
        List<Knowledge__DataCategorySelection> dataCategorySelections = new List<Knowledge__DataCategorySelection>();

        for (Knowledge__kav amsNewsArticle : amsNewsArticles) {
            if (!String.isBlank(amsNewsArticle.Data_Category__c)) {
                Knowledge__DataCategorySelection dataCategorySelection = new Knowledge__DataCategorySelection();

                dataCategorySelection.ParentId = amsNewsArticle.Id;
                dataCategorySelection.DataCategoryName = amsNewsArticle.Data_Category__c;
                dataCategorySelection.DataCategoryGroupName = AMS_NEWS_DATA_CATEGORY_GROUP_NAME;

                dataCategorySelections.add(dataCategorySelection);
            }
        }

        setDataCategoryFuture(JSON.serialize(dataCategorySelections));
    }

    /**
     * Populate Record Type based on the value of recordTypeFieldAPIName
     * This must be run first in the before trigger because it assigns the correct record type for any new record
     **/
    public static void setRecordTypeId(List<SObject> newNewsArticles) {
        GlobalUtility.setRecordTypeIdFromField(Knowledge__kav.Record_Type_Developer_Name__c, newNewsArticles);
    }

    //needs to be done as a future, SF might be overriding the data category types on save
    @future
    public static void setDataCategoryFuture(String dataCategoriesToSet) {
        List<Knowledge__DataCategorySelection> dataCategorySelections = (List<Knowledge__DataCategorySelection>) JSON.deserialize(dataCategoriesToSet, List<Knowledge__DataCategorySelection>.class);
        List<Knowledge__kav> amsNewsArticlesToUpdate = new List<Knowledge__kav>();

        for (Knowledge__DataCategorySelection dataCategorySelection : dataCategorySelections) {
            amsNewsArticlesToUpdate.add(new Knowledge__kav(
                Id = dataCategorySelection.ParentId,
                IsVisibleInCsp = true,
                IsVisibleInPkb = true
            ));
        }

        update (amsNewsArticlesToUpdate);
        insert (dataCategorySelections);
    }
}