/**
 * Description: Controller to display herd size and diary hectares
 * @author: Amelia (Trineo)
 * @date: Septemeber 2017
 */
public with sharing class AMSHerdAndHectaresController {

    public static final Integer FARM_SEASON_LIMIT = 2;

    public SeasonWrapper selectedSeason { get; set; }
    public String successMessage {get; set;}
    public String errorMessage {get; set;}

    public Id selectedFarmId { get; set; }
    public List<Farm_Season__c> seasons { get; set; }
    public Id selectedFarmSeason { get; set; }

    public Boolean setCowsByMonth { get; set; }

    private static Map<String, String> messages;
    static {
        messages = AMSCommunityUtil.initialisePageMessages('AMSHerdAndHectares');
    }

    public AMSHerdAndHectaresController() {
        this.successMessage = '';
        this.errorMessage = '';
        this.setCowsByMonth = false;
    }

    public List<SelectOption> getFarmSelectOptions() {
        List<SelectOption> farmSelectOptions = new List<SelectOption>();
        farmSelectOptions.add(AMSCommunityUtil.createFirstSelectOption());

        for (Account farm : RelationshipAccessService.accessToFarms(AMSContactService.determineContactId(), RelationshipAccessService.Access.ACCESS_PRIMARY)) {
            farmSelectOptions.add(new SelectOption(farm.Id, farm.Name));
        }
        return farmSelectOptions;
    }

    public List<SelectOption> getSeasonSelectOptions() {
        List<SelectOption> seasonSelectOptions = new List<SelectOption>();
        seasonSelectOptions.add(AMSCommunityUtil.createFirstSelectOption());

        if (selectedFarmId != null) {
            this.seasons = AMSFarmSeasonService.getCurrentFarmSeasonForwards(selectedFarmId, FARM_SEASON_LIMIT);

            for (Farm_Season__c season : seasons) {
                seasonSelectOptions.add(new SelectOption(season.Id, season.Name));
            }
        }
        return seasonSelectOptions;
    }

    public void queryFarmSeason() {
        for (Farm_Season__c season : seasons) {
            if (season.Id == selectedFarmSeason) {
                selectedSeason = new SeasonWrapper(season);
                break;
            }
        }
    }

    public PageReference saveFarmSeason() {
        if (selectedSeason != null) {
            //check the user has entered a number and it is greater than 0
            this.errorMessage = '';
            if (!setCowsByMonth && (selectedSeason.cows == null || selectedSeason.cows == 0)) {
                this.errorMessage = messages.get('cows-zero');
            }
            if (selectedSeason.hectares == null || selectedSeason.hectares == 0) {
                this.errorMessage = this.errorMessage == '' ? messages.get('hectars-zero') : this.errorMessage + ', ' + messages.get('hectars-zero');
            }

            if (String.isBlank(this.errorMessage)) {
                try {
                    setCowsOnSeason();
                    selectedSeason.season.Dairy_Hectares__c = selectedSeason.hectares;
                    AMSFarmSeasonService.updateFarmSeason(selectedSeason.season);
                    this.successMessage = messages.get('successful-update');
                    this.errorMessage = '';
                    reloadAfterSave();
                } catch (Exception e) {
                    this.successMessage = '';
                    this.errorMessage = messages.get('unknown-error');
                }
            }
        }
        return null;
    }

    private void reloadAfterSave() {
        this.seasons = AMSFarmSeasonService.getCurrentFarmSeasonForwards(selectedFarmId, FARM_SEASON_LIMIT);
        queryFarmSeason();
        setCowsBySeason();
    }

    public void setCowsByMonth() {
        this.setCowsByMonth = true;
    }

    public void setCowsBySeason() {
        this.setCowsByMonth = false;
    }

    private void setCowsOnSeason() {
        if (!setCowsByMonth) {
            selectedSeason.season.Peak_Cows__c = selectedSeason.cows;
            selectedSeason.season.Cows_January__c = null;
            selectedSeason.season.Cows_February__c = null;
            selectedSeason.season.Cows_March__c = null;
            selectedSeason.season.Cows_April__c = null;
            selectedSeason.season.Cows_May__c = null;
            selectedSeason.season.Cows_June__c = null;
            selectedSeason.season.Cows_July__c = null;
            selectedSeason.season.Cows_August__c = null;
            selectedSeason.season.Cows_September__c = null;
            selectedSeason.season.Cows_October__c = null;
            selectedSeason.season.Cows_November__c = null;
            selectedSeason.season.Cows_December__c = null;
        } else {
            selectedSeason.season.Peak_Cows__c = null;
            selectedSeason.season.Cows_January__c = selectedSeason.cowsJanuary;
            selectedSeason.season.Cows_February__c = selectedSeason.cowsFebruary;
            selectedSeason.season.Cows_March__c = selectedSeason.cowsMarch;
            selectedSeason.season.Cows_April__c = selectedSeason.cowsApril;
            selectedSeason.season.Cows_May__c = selectedSeason.cowsMay;
            selectedSeason.season.Cows_June__c = selectedSeason.cowsJune;
            selectedSeason.season.Cows_July__c = selectedSeason.cowsJuly;
            selectedSeason.season.Cows_August__c = selectedSeason.cowsAugust;
            selectedSeason.season.Cows_September__c = selectedSeason.cowsSeptember;
            selectedSeason.season.Cows_October__c = selectedSeason.cowsOctober;
            selectedSeason.season.Cows_November__c = selectedSeason.cowsNovember;
            selectedSeason.season.Cows_December__c = selectedSeason.cowsDecember;
        }
    }

    public class SeasonWrapper {
        //use a wrapper so we can use string methods to test if the user has entered a number or not and be able to throw a custom error message
        public Farm_Season__c season { get; set; }
        public Decimal cows { get; set; }
        public Decimal hectares { get; set; }

        public Decimal cowsJanuary { get; set; }
        public Decimal cowsFebruary { get; set; }
        public Decimal cowsMarch { get; set; }
        public Decimal cowsApril { get; set; }
        public Decimal cowsMay { get; set; }
        public Decimal cowsJune { get; set; }
        public Decimal cowsJuly { get; set; }
        public Decimal cowsAugust { get; set; }
        public Decimal cowsSeptember { get; set; }
        public Decimal cowsOctober { get; set; }
        public Decimal cowsNovember { get; set; }
        public Decimal cowsDecember { get; set; }

        public Boolean setByMonth { get; set; }

        public SeasonWrapper(Farm_Season__c s) {
            this.season = s;
            this.cows = s.Peak_Cows__c;
            this.hectares = s.Dairy_Hectares__c;
            this.cowsJanuary = s.Cows_January__c;
            this.cowsFebruary = s.Cows_February__c;
            this.cowsMarch = s.Cows_March__c;
            this.cowsApril = s.Cows_April__c;
            this.cowsMay = s.Cows_May__c;
            this.cowsJune = s.Cows_June__c;
            this.cowsJuly = s.Cows_July__c;
            this.cowsAugust = s.Cows_August__c;
            this.cowsSeptember = s.Cows_September__c;
            this.cowsOctober = s.Cows_October__c;
            this.cowsNovember = s.Cows_November__c;
            this.cowsDecember = s.Cows_December__c;
            this.setByMonth = AMSFarmSeasonService.isHerdSizeSetByMonth(s);
        }
    }
}