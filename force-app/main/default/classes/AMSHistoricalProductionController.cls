/**
 * AMSHistoricalProductionController
 *
 * Controller for AMSHistoricalProduction page
 *
 * Author: NL (Trineo)
 * Date: 2017-08-30
 */
public without sharing class AMSHistoricalProductionController extends AMSProdQualReport {

    public AMSProdQualReport baseController { get { return this; } private set; }

    private static final String DEFAULT_REPORT_TYPE = 'Historical Production';
    private static final Integer MAX_SEASONS_TO_QUERY = 3;

    public String selectedGraphOption { get; set; }

    public List<String> dateString { get; set; }

    public HistoricalCollectionPeriodForm historicalCollectionPeriodForm { get; set; }
    public transient List<AMSCollectionGraph> collectionGraphs { get; set; }

    public class HistoricalCollectionPeriodForm {
        List<AMSCollectionService.CollectionPeriodForm> collectionPeriodForms;

        public List<Row> rows { get; set; }
        public List<AMSCollectionService.CollectionPeriodWrapper> totalWrappers { get; set; }
        public List<AMSCollectionService.CollectionPeriodWrapper> averageWrappers { get; set; }

        public HistoricalCollectionPeriodForm(List<AMSCollectionService.CollectionPeriodForm> collectionPeriodForms) {
            this.collectionPeriodForms = collectionPeriodForms;

            Integer numberOfPeriods = 0;

            if (!collectionPeriodForms.isEmpty()) {
                numberOfPeriods = collectionPeriodForms[0].collectionItemDateOrder.size();
            }

            rows = new List<Row>();
            totalWrappers = new List<AMSCollectionService.CollectionPeriodWrapper>();
            averageWrappers = new List<AMSCollectionService.CollectionPeriodWrapper>();

            for (Integer i = 0; i < numberOfPeriods; i++) {
                Row row = new Row();
                row.collectionItemDateOrder = collectionPeriodForms[0].collectionItemDateOrder[i];
                row.isCurrentMonth = collectionPeriodForms[0].collectionPeriodWrapperList[i].isCurrentMonth;
                row.collectionPeriodWrappers = new List<AMSCollectionService.CollectionPeriodWrapper>();

                for (AMSCollectionService.CollectionPeriodForm form : collectionPeriodForms) {
                    if (i == 0) { //first run, add all the totals and averages
                        totalWrappers.add(form.totalCollectionWrapper);
                        averageWrappers.add(form.averageCollectionWrapper);
                    }
                    row.collectionPeriodWrappers.add(form.collectionPeriodWrapperList[i]);
                }
                rows.add(row);
            }
        }
    }

    public class Row {
        public String collectionItemDateOrder { get; set; }
        public Boolean isCurrentMonth { get; set; }
        public List<AMSCollectionService.CollectionPeriodWrapper> collectionPeriodWrappers { get; set; }
    }

    public AMSHistoricalProductionController() {
        selectedReportTypeName = DEFAULT_REPORT_TYPE;
    }

    public override void initProperties() {
        selectedGraphOption = AMSCollectionService.graphSelectOptions[0].getValue();
    }

    //Call recalculate for the forms
    public override void recalculateForm() {
        while (paddedSeasonList.size() > MAX_SEASONS_TO_QUERY) {
            paddedSeasonList.remove(paddedSeasonList.size() - 1);
        }

        List<AMSCollectionService.CollectionPeriodForm> collectionPeriodForms = new List<AMSCollectionService.CollectionPeriodForm>();
        collectionGraphs = new List<AMSCollectionGraph>();
        Integer seasonCount = 0;
        populateScopedDates();
        for (Farm_Season__c farmSeason : paddedSeasonList) {
            AMSCollectionService.CollectionPeriodForm collectionPeriodForm = new AMSCollectionService.CollectionPeriodForm(
                scopedCurrentSeasonsFromDate.addYears(-seasonCount),
                scopedCurrentSeasonsToDate.addYears(-seasonCount),
                null,
                null,
                null,
                farmSeason,
                selectedFarmId,
                selectedPlotOption
            );

            collectionPeriodForm.convertDateMapToListAscending(false);
            collectionPeriodForms.add(collectionPeriodForm);
            seasonCount++;
        }

        historicalCollectionPeriodForm = new HistoricalCollectionPeriodForm(collectionPeriodForms);

        redrawGraphs();
    }

    public void redrawGraphs() {
        collectionGraphs = new List<AMSCollectionGraph>();

        Integer seasonCount = 0;
        for (Farm_Season__c farmSeason : paddedSeasonList) {
            AMSCollectionService.CollectionPeriodForm collectionPeriodForm = new AMSCollectionService.CollectionPeriodForm(
                fromDate.addYears(-seasonCount),
                toDate.addYears(-seasonCount),
                null,
                null,
                null,
                farmSeason,
                selectedFarmId,
                selectedPlotOption
            );

            collectionPeriodForm.convertDateMapToListDescending(false);
            collectionGraphs.add(new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.GRAPH_SELECT_OPTIONS_TO_UNIT_OF_MEASUREMENT_MAP.get(selectedGraphOption)));
            seasonCount++;
        }
    }

    private void populateScopedDates() {
        if (monthList.isEmpty() || selectedPlotOption == AMSCollectionService.MONTH) {
            scopedCurrentSeasonsFromDate = fromDate;
            scopedCurrentSeasonsToDate = toDate;
        } else {
            Integer currentMonth = TODAY.month();
            Integer indexOfSelectedMonthInMonthList = indexOf(monthList, selectedMonth);

            //if the first month is selected, don't default the from date- cap the to date to the end of the month
            if (indexOfSelectedMonthInMonthList == 0) {
                scopedCurrentSeasonsFromDate = fromDate;
                scopedCurrentSeasonsToDate = fromDate.addMonths(indexOfSelectedMonthInMonthList + 1).toStartOfMonth().addDays(-1);
            //if not the last month, default the from date and to date to start and end of month
            } else if (indexOfSelectedMonthInMonthList != monthList.size() - 1) {
                scopedCurrentSeasonsFromDate = fromDate.addMonths(indexOfSelectedMonthInMonthList).toStartOfMonth();
                scopedCurrentSeasonsToDate = fromDate.addMonths(indexOfSelectedMonthInMonthList + 1).toStartOfMonth().addDays(-1);
            //if the last month, default the from date, don't default the end date
            } else {
                scopedCurrentSeasonsFromDate = fromDate.addMonths(indexOfSelectedMonthInMonthList).toStartOfMonth();
                scopedCurrentSeasonsToDate = toDate;
            }
        }
    }

    public override void recalculateFromFarm() {
        super.recalculateFromFarm();
    }

    public AMSCollectionService.UnitOfMeasurement getSelectedGraphOptionUnitOfMeasurement() {
        return AMSCollectionService.GRAPH_SELECT_OPTIONS_TO_UNIT_OF_MEASUREMENT_MAP.get(selectedGraphOption);
    }
}