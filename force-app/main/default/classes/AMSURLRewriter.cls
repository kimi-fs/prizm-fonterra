/*
* URL Rewriter implementation for the AMS community
*
* @author: John Au (Trineo)
* @date: 02/04/2020
* @test: AMSURLRewriterTest
*/
public without sharing class AMSURLRewriter implements Site.UrlRewriter {

    public static final PageReference PRICING_PAGE_FRIENDLY = new PageReference('/pricing');
    public static final PageReference PRICING_PAGE = new PageReference('/AMSPricing');

    // Going from friendly to internal pages
    public PageReference mapRequestUrl(PageReference requestedPage) {
        if (requestedPage.getUrl().startsWith(PRICING_PAGE_FRIENDLY.getUrl())) {
            return PRICING_PAGE;
        }

        return null;
    }

    // Going from internal to friendly, note that we are stripping parameters if
    // we rewrite
    public List<PageReference> generateUrlFor(List<PageReference> regularPages) {
        List<PageReference> allPageReferences = new List<PageReference>();

        for (PageReference regularPage : regularPages) {
            if (regularPage.getUrl().startsWith(PRICING_PAGE.getUrl())) {
                allPageReferences.add(PRICING_PAGE_FRIENDLY);
            } else {
                allPageReferences.add(regularPage);
            }
        }

        return allPageReferences;
    }

}