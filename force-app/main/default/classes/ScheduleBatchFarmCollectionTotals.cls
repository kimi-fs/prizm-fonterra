/**
* Description:  Updates Collection Totals into the Farm
* @author Clayde Bael (Trineo)
* @date  11/07/2018
* @test ScheduleBatchFarmCollectionTotalsTest
**/

global class ScheduleBatchFarmCollectionTotals implements Schedulable, Database.Batchable<Account>, Database.Stateful  {

    private static final String SCHED_JOB_NAME = ScheduleService.getCRONJobName('ScheduleBatchFarmCollectionTotals');
    private static final Id farmAuRecordType = GlobalUtility.auAccountFarmRecordTypeId;
    private Map<Id,String> errorMap = new Map<Id, String>();

    global static void schedule() {
        String expression = ScheduleService.getCRONExpression('ScheduleBatchFarmCollectionTotals');
        if (String.isNotBlank(expression)) {
            final ScheduleBatchFarmCollectionTotals job = new ScheduleBatchFarmCollectionTotals();
            System.schedule(SCHED_JOB_NAME, expression, job);
        }
    }

    global static void abort() {
        ScheduleService.terminate(SCHED_JOB_NAME);
    }


    global void execute(SchedulableContext schedulableContext) {
        Database.executeBatch(new ScheduleBatchFarmCollectionTotals(), 1);
    }

    global List<Account> start(Database.BatchableContext bc) {
        return [SELECT  Id
                FROM    Account
                WHERE   RecordTypeId = :farmAuRecordType];
    }

    global void execute(Database.BatchableContext BC, List<Account> farms) {

        List<AggregateResult> collectionsTotals = [SELECT  Farm_ID__c,
                                                            SUM(Volume_Ltrs__c) Total_Litres,
                                                            SUM(Total_KgMS__c) Total_KgMS
                                                    FROM    Collection__c
                                                    WHERE   Pick_Up_Date__c = LAST_N_MONTHS:12
                                                    AND     Farm_ID__c IN :farms
                                                    GROUP BY Farm_ID__c];

        List<Account> accountsToUpdate = new List<Account>();
        for (AggregateResult result : collectionsTotals) {
            Decimal totalLitres = (Decimal) result.get('Total_Litres');
            Decimal totalKgMS   = (Decimal) result.get('Total_KgMS');

            if (totalLitres != null || totalKgMS != null) {
                Account acct = new Account(Id = (Id) result.get('Farm_ID__c'),
                    Total_Volume_12_Months__c = totalLitres,
                    Total_Solids_12_Months__c = totalKgMS);
                accountsToUpdate.add(acct);
            }
        }

        if (!accountsToUpdate.isEmpty()) {
            List<Database.SaveResult> dmlResult = Database.update(accountsToUpdate, false);
            errorMap.putAll(databaseSaveResultErrors(dmlResult, accountsToUpdate));
        }
    }

    //terribly written code pulled from BatchUtility.cls in NZ that I'm not taking over.
    private static Map<Id, String> databaseSaveResultErrors(List<Database.SaveResult> listResult, List<SObject> listsObject){
        Map<Id, String> mapRecId2ErrorMessage = new Map<Id, String>();
        Integer i = 0;

        for (Database.SaveResult res : listResult) {
            if (res.getErrors().size() > 0) {
                mapRecId2ErrorMessage.put((Id)listsObject[i].get('Id'),res.getErrors()[0].getMessage());
            }
            i++;
        }
        return mapRecId2ErrorMessage;
    }

    global void finish(Database.BatchableContext BC) {
        if (!errorMap.isEmpty()) {
            System.debug('## ScheduleBatchFarmCollectionTotals Errors ' + errorMap );
            Logger.log('ScheduleBatchFarmCollectionTotals', 'Error ' + errorMap, Logger.LogType.ERROR);
            Logger.saveLog();
        }
    }

}