@isTest
private class AMSFarmTargetsControllerTest {

    @testSetup static void setup() {

        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.integrationUserProfile();

        Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();

        Id individualAURecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual AU').getRecordTypeId();

        Map<String, Contact> individuals = new Map<String, Contact>();
        individuals.put('Tony Montana', new Contact(FirstName = 'Tony', LastName = 'Montana', Email = 'tony@email.com', Phone = '+64 9 1111111', RecordTypeId = individualAURecordTypeId, Type__c = 'Personal'));
        individuals.put('John Rambo', new Contact(FirstName = 'John', LastName = 'Rambo', Email = 'john@email.com', Phone = '+64 9 1111112', RecordTypeId = individualAURecordTypeId, Type__c = 'Personal'));
        insert individuals.values();

        Map<String, Account> farms = new Map<String, Account>();
        farms.put('Farm1', new Account( Name = 'Farm1',
                                        RecordTypeId = GlobalUtility.auAccountFarmRecordTypeId,
                                        Primary_Business_Contact__c = individuals.get('Tony Montana').Id,
                                        Primary_On_Farm_Contact__c = individuals.get('Tony Montana').Id));
        farms.put('Farm2', new Account( Name = 'Farm2',
                                        RecordTypeId = GlobalUtility.auAccountFarmRecordTypeId,
                                        Primary_Business_Contact__c = individuals.get('John Rambo').Id,
                                        Primary_On_Farm_Contact__c = individuals.get('John Rambo').Id));
        insert farms.values();

        List<Reference_Period__c> referencePeriods = new List<Reference_Period__c>();
        referencePeriods.add(new Reference_Period__c(Name = 'Milking Season 2017/2018', Type__c = GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU, Start_Date__c = Date.parse('1/06/2017'), End_Date__c = Date.parse('31/05/2018'), Reference_Period__c = '2017/2018'));
        referencePeriods.add(new Reference_Period__c(Name = 'Milking Season 2018/2019', Type__c = GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU, Start_Date__c = Date.parse('1/06/2018'), End_Date__c = Date.parse('31/05/2019'), Reference_Period__c = '2018/2019'));
        referencePeriods.add(new Reference_Period__c(Name = 'Milking Season 2019/2020', Type__c = GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU, Start_Date__c = Date.parse('1/06/2019'), End_Date__c = Date.parse('31/05/2020'), Reference_Period__c = '2019/2020'));
        referencePeriods.add(new Reference_Period__c(Name = 'Milking Season 2020/2021', Type__c = GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU, Start_Date__c = Date.parse('1/06/2020'), End_Date__c = Date.parse('31/05/2021'), Reference_Period__c = '2020/2021'));
        referencePeriods.add(new Reference_Period__c(Name = 'Milking Season 2021/2022', Type__c = GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU, Start_Date__c = Date.parse('1/06/2021'), End_Date__c = Date.parse('31/05/2022'), Reference_Period__c = '2021/2022'));
        referencePeriods.add(new Reference_Period__c(Name = 'Milking Season 2022/2023', Type__c = GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU, Start_Date__c = Date.parse('1/06/2022'), End_Date__c = Date.parse('31/05/2023'), Reference_Period__c = '2022/2023'));
        referencePeriods.add(new Reference_Period__c(Name = 'Milking Season 2023/2024', Type__c = GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU, Start_Date__c = Date.parse('1/06/2023'), End_Date__c = Date.parse('31/05/2024'), Reference_Period__c = '2023/2024'));
        referencePeriods.add(new Reference_Period__c(Name = 'Milking Season 2024/2025', Type__c = GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU, Start_Date__c = Date.parse('1/06/2024'), End_Date__c = Date.parse('31/05/2025'), Reference_Period__c = '2024/2025'));
        referencePeriods.add(new Reference_Period__c(Name = 'Milking Season 2025/2026', Type__c = GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU, Start_Date__c = Date.parse('1/06/2025'), End_Date__c = Date.parse('31/05/2026'), Reference_Period__c = '2025/2026'));
        for(Reference_Period__c rp :referencePeriods){
            if(System.today() < rp.Start_Date__c){
                rp.Status__c = 'Future';
            }
            if(System.today() > rp.End_Date__c){
                rp.Status__c = 'Past';
            }
            if(System.today() > rp.Start_Date__c && System.today() > rp.Start_Date__c ){
                rp.Status__c = 'Current';
            }
        }
        insert referencePeriods;

        List<Farm_Season__c> farmSeasons = new List<Farm_Season__c>();
        for(Account farm : farms.values()){
            for(Reference_Period__c rp :referencePeriods){
                farmSeasons.add(new Farm_Season__c( Name = rp.Reference_Period__c,
                                                    Farm__c = farm.Id,
                                                    Reference_Period__c = rp.Id,
                                                    Farm_Season_ID__c = farm.Name+'-'+rp.Reference_Period__c));
            }
        }
        insert farmSeasons;

        List<Collection__c> collections = new List<Collection__c>();
        Date firstDate = Date.newinstance(System.today().addYears(-1).year(), 7, 1);
        Date lastDate = Date.newinstance(System.today().addYears(1).year(), 6, 30);
        Integer numberOfDays = firstDate.daysBetween(lastDate);

        for(Integer i = 0; i < numberOfDays; i++){
            Date day = firstDate.addDays(i);

            Collection__c record = new Collection__c();
            record.Name = day.year()+day.month()+day.day()+'_1_1';
            record.Farm_ID__c = farms.get('Farm1').Id;
            record.Pick_Up_Date__c = day;
            record.Time__c = Datetime.newInstanceGmt(day.year(), day.month(), day.day(), System.now().hour(), System.now().minute(), System.now().second());
            record.Run_No__c = String.valueOf(Decimal.valueOf(Math.random() * 100).setscale(3));
            record.Volume_Ltrs__c = Decimal.valueOf(Math.random() * 10000).setscale(0);
            record.Temperature__c = 5;
            record.Fat_Kgs__c = record.Volume_Ltrs__c * 0.04;
            record.Prot_Kgs__c = record.Fat_Kgs__c * 0.85;
            record.Fat_Percent__c = 5.00;
            record.Prot_Percent__c = 3.4;
            collections.add(record);
        }
        insert collections;

        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        List<Entity_Relationship__c> farmsParty = new List<Entity_Relationship__c>();

        farmsParty.add(TestClassDataUtil.createPartyFarmEntityRelationship(party, farms.get('Farm1'), false));
        farmsParty.add(TestClassDataUtil.createPartyFarmEntityRelationship(party, farms.get('Farm2'), false));

        insert farmsParty;

        List<Individual_Relationship__c> individualRelationships = new List<Individual_Relationship__c>();
        individualRelationships.add(new Individual_Relationship__c( Active__c = true,
                                                                    Individual__c = individuals.get('Tony Montana').Id,
                                                                    Role__c = 'Director',
                                                                    Party__c = party.Id,
                                                                    RecordTypeId = GlobalUtility.individualPartyRecordTypeId));
        insert individualRelationships;

        // We will now have derived relationships with both farms
    }

    @isTest static void saveSelectedFarmAndSaveAllFarmsTest() {

        User communityUser;
        List<Account> farms;

        User integrationUser = TestClassDataUtil.createIntegrationUser();

        System.runAs(integrationUser) {
            Contact tonyContact = [SELECT Id, Email, FirstName, LastName, MobilePhone FROM Contact WHERE Phone = '+64 9 1111111'];

            communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(tonyContact);
        }

        System.runAs(communityUser) {
            PageReference pageRef = Page.AMSFarmTargets;
            System.Test.setCurrentPage(pageRef);
            AMSFarmTargetsController controller = new AMSFarmTargetsController();

            //check there are no existing target records
            List<Target__c> targets = [SELECT Id, Target_KgMS__c FROM Target__c];
            System.assertEquals(0, targets.size());

            System.assertNotEquals(null, controller.farmSeasons, 'No seasons - worse null!');
            System.assert(controller.farmSeasons.size() > 0, 'No farm seasons');

            //set a target on farm 1 and then save just this farm
            controller.farmSeasons[0].targets[0].record.Target_KgMS__c = 1000;
            controller.saveSelectedFarm();
            List<Target__c> targets2 = [SELECT Id, Target_KgMS__c FROM Target__c ORDER BY Target_KgMS__c DESC];
            System.assertEquals(24, targets2.size());
            System.assertEquals(1000, targets2[0].Target_KgMS__c);
        }

    }

}