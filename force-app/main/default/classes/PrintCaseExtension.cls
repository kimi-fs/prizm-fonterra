/**
 * PrintCaseExtension.cls
 * Description: Retrieves the Case Detail and Chatter Posts and Comments so they
 *              can be displayed in a VisualForce Page.
 * @author: Sarah Hay (Trineo)
 * @date: 26 January 2016
 */
public with sharing class PrintCaseExtension {

  // public Case c { get; set; }
  // public List<CaseFeed> feedItem { get; set; }

  // @testVisible
  public PrintCaseExtension(ApexPages.StandardController stdController) {
    // this.c = (Case)stdController.getRecord();
    // Id feedItemId = this.c.Id;
    // loadFeedItems(feedItemId);
  }

  // @testVisible
  // public List<CaseFeed> getFeedItems(){
  //   return feedItem;
  // }

  // @testVisible
  // private void loadFeedItems(String feedItemId) {
  //   this.feedItem = [SELECT CreatedDate, Id, Type, CreatedBy.FirstName, CreatedBy.LastName,
  //         ParentId, Body, Title,
  //           (SELECT Id, CommentBody, CreatedDate, CreatedBy.FirstName, CreatedBy.LastName
  //             FROM FeedComments ORDER BY CreatedDate)
  //         FROM CaseFeed
  //         WHERE ParentID = :feedItemId
  //         ORDER BY CreatedDate DESC, Id DESC];
  // }
}