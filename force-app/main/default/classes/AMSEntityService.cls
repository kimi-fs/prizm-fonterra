/**
 * AMSEntityService
 *
 * Service for retrieving entities, also see the RelationshipAccessService for
 * entities based on access.
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately.
 *
 * Author: Ed (Trineo)
 * Date: Aug 2017
 */
public without sharing class AMSEntityService {
    // Keep the fields in these lists so that we are consistent with which fields are returned.
    private static List<String> farmFields = new List<String>{  'Id',
                                                                'Name',
                                                                'Start_AM__c',
                                                                'Finish_AM__c',
                                                                'Start_PM__c',
                                                                'Finish_PM__c',
                                                                'Milking_Frequency_List__c',
                                                                'Farm_Region_Name__c'};

    private static List<String> partyFields = new List<String>{ };

    /**
     * Flush out the fields on the provided list of Farms, we only need the Ids
     * to be filled out.
     */
    public static List<Account> getFarms(List<Account> farms) {
        List<Id> farmIds = new List<Id>();
        for (Account farm : farms) {
            farmIds.add(farm.Id);
        }
        return getFarms(farmIds);
    }

    public static List<Account> getFarms(List<Id> farmIds) {
        String query =  'SELECT ' + String.join(farmFields, ',') +

                        ' FROM Account ' +
                        ' WHERE Id IN :farmIds';
        List<Account> farms = Database.query(query);
        return farms;
    }

    public static List<Account> getFarms(List<String> farmNames) {
        String query =  'SELECT ' + String.join(farmFields, ',') +
                        ' FROM Account ' +
                        ' WHERE RecordTypeID = \'' + GlobalUtility.auAccountFarmRecordTypeId + '\'' +
                        ' AND Name IN :farmNames ';

        List<Account> farms = Database.query(query);
        return farms;
    }

    public static List<Account> updateEntities(List<Account> entities) {
        update entities;
        return entities;
    }
}