/**
 * CreateCaseController.cls
 * Description: Takes in Individual Relationship or Farm and/or Individual IDs as parameters and provides
 *              the ability to create a case quickly against the relevant farm and individual after specifying
 *              case record type, type, and sub-type. Also shows a list of the last 20 cases created against that
 *              Farm/Individual.
 * @author: Sarah Hay (Trineo)
 * @date: February 2016
 * @test: CreateCaseControllerTest.cls
 */
public with sharing class CreateCaseController {

    public Contact individual {get; set;}
    public Account farm {get; set;}
    public Account party {get; set;}
    public Account fssAccount {get; set;}
    public Account organisation {get; set;}

    public List<SelectOption> recordTypeSelectOptions {get; set;}
    public Id selectedRecordType {get; set;}

    public Individual_Relationship__c IR {get; set;}
    public Case c {set; get;}

    public Boolean typeFields {get; set;}

    public Id irID {get; set;}
    public Id farmID {get; set;}
    public Id individualID {get; set;}

    public List<Case> relevantCases { get; private set; }

    public CreateCaseController() {
        this.c = new Case();
        this.typeFields = false;

        try {
            this.irID = ApexPages.currentPage().getParameters().get('irId');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Invalid Individual Relationship Id'));
        }

        try {
            this.farmID = ApexPages.currentPage().getParameters().get('farmId');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Invalid Farm Id'));
        }

        try {
            this.individualID = ApexPages.currentPage().getParameters().get('individualId');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'Invalid Individual Id'));
        }

        if (this.irID != null ) {
            this.IR = [SELECT Id, Farm__c, Individual__c, Organisation__c, Party__c, Party__r.Name FROM Individual_Relationship__c WHERE Id = :this.irID];
            if (this.IR.Party__c != null) {
                this.party = queryAccount(this.IR.Party__c);
                c.AccountId = this.IR.Party__c;
                c.Party__c = this.IR.Party__c;
                c.Party__r = this.party;
            }
            if (this.IR.Farm__c != null) {
                this.farm = queryAccount(this.IR.Farm__c);
                c.AccountId = this.IR.Farm__c;
            }
            if (this.IR.Organisation__c != null) {
                this.organisation = queryAccount(this.IR.Organisation__c);
                c.AccountId = this.IR.Organisation__c;
            }
            this.individual = queryContact(this.IR.Individual__c);
            c.ContactId = this.IR.Individual__c;
        }

        if (this.farmID != null) {
            this.farm = queryAccount(this.farmID);
            c.AccountId = this.farmID;
        }

        if (this.individualID != null) {
            this.individual = queryContact(this.individualID);
            c.ContactId = this.individualID;
        }

        if (individual == null && farm == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'No Individual or Farm Id Provided'));
        } else if (individual == null ) {
            this.relevantCases = queryCases(null, farm.Id);
        } else if (farm == null) {
            this.relevantCases = queryCases(individual.Id, null);
        } else if (farm != null && individual != null) {
            this.relevantCases = queryCases(individual.Id, farm.Id);
        } else if (fssAccount != null && individual != null) {
            this.relevantCases = queryCases(individual.Id, fssAccount.Id);
        } else if (organisation != null && individual != null) {
            this.relevantCases = queryCases(individual.Id, organisation.Id);
        }

        recordTypeSelectOptions = generateRecordTypeSelectOptions();
        selectedRecordType = recordTypeSelectOptions.isEmpty() ? null : recordTypeSelectOptions[0].getValue();
        setRT();
    }

    private List<Case> queryCases(Id contactId, Id accountId) {
        String query = 'SELECT *, Contact.Name, Account.Name, RecordType.Name FROM Case WHERE ';
        query += String.isNotBlank(contactId) ? ' ContactId = \'' + contactId + '\'' : '';
        query += String.isNotBlank(contactId) && String.isNotBlank(accountId) ? ' AND ' : '';
        query += String.isNotBlank(accountId) ? ' AccountId = \'' + accountId + '\'' : '';
        query += ' ORDER BY isClosed, Sorting_Date__c DESC LIMIT 20';
        return QueryUtil.query(query);
    }

    private Account queryAccount(Id accountId) {
        List<Account> account = QueryUtil.query('SELECT *, RecordType.DeveloperName FROM Account WHERE Id = \'' + accountId + '\'');
        return account.isEmpty() ? null : account[0];
    }

    private Contact queryContact(Id contactId) {
        List<Contact> contact = QueryUtil.query('SELECT *, RecordType.DeveloperName FROM Contact WHERE Id = \'' + contactId + '\'');
        return contact.isEmpty() ? null : contact[0];
    }

    public List<SelectOption> generateRecordTypeSelectOptions() {
        List<SelectOption> selectOptions = new List<SelectOption>();
        List<RecordType> caseRecordTypes = [SELECT Id, Name, DeveloperName FROM RecordType WHERE sObjectType = 'Case' AND IsActive = true];
        if (individual != null) {
            for (RecordType rt : caseRecordTypes) {
                if (rt.DeveloperName.endsWithIgnoreCase('_AU')) {
                    selectOptions.add(new SelectOption(rt.Id, rt.Name));
                }
            }
        }
        return selectOptions;

    }

    public void setRT() {
        this.c.RecordTypeId = selectedRecordType;
        this.typeFields = true;
        this.c.Type = null;
        this.c.Sub_Type__c = null;
        this.c.Issue_Type__c = null;
    }

}