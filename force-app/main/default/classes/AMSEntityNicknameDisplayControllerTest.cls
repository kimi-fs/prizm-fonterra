/**
 * Description: Test for AMSEntityNicknamesDisplayController
 *
 * @author: John Au (Trineo)
 * @date: October 2018
 */
@IsTest
private class AMSEntityNicknameDisplayControllerTest {

    private static final String testUserEmail = 'bubbasuperduperfakemail@fonterra.com';

    @TestSetup
    private static void setup() {
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.integrationUserProfile();

        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(testUserEmail);

        Map<String, Account> farms = new Map<String, Account>();
        farms.put('Farm1', new Account( Name = 'Farm1',
                                        RecordTypeId = GlobalUtility.auAccountFarmRecordTypeId,
                                        Primary_Business_Contact__c = communityUser.ContactId,
                                        Primary_On_Farm_Contact__c = communityUser.ContactId));
        farms.put('Farm2', new Account( Name = 'Farm2',
                                        RecordTypeId = GlobalUtility.auAccountFarmRecordTypeId,
                                        Primary_Business_Contact__c = communityUser.ContactId,
                                        Primary_On_Farm_Contact__c = communityUser.ContactId));
        insert farms.values();

        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        List<Entity_Relationship__c> farmsParty = new List<Entity_Relationship__c>();

        farmsParty.add(TestClassDataUtil.createPartyFarmEntityRelationship(party, farms.get('Farm1'), false));
        farmsParty.add(TestClassDataUtil.createPartyFarmEntityRelationship(party, farms.get('Farm2'), false));

        insert farmsParty;

        List<Individual_Relationship__c> individualRelationships = new List<Individual_Relationship__c>();
        individualRelationships.add(new Individual_Relationship__c( Active__c = true,
                                                                    Individual__c = communityUser.ContactId,
                                                                    Role__c = 'Director',
                                                                    Party__c = party.Id,
                                                                    RecordTypeId = GlobalUtility.individualPartyRecordTypeId));
        insert individualRelationships;

        // We will now have derived relationships with both farms
    }

    @IsTest
    private static void testDefaultFarmNameRetrieval() {
        User communityUser = [SELECT Id, ContactId FROM User WHERE Email = :testUserEmail];
        Account farm = [SELECT Id, Name FROM Account WHERE Name = 'Farm1' AND RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId LIMIT 1];

        System.runAs(communityUser) {
            AMSEntityNicknameDisplayController entityNicknameDisplayController = new AMSEntityNicknameDisplayController();

            entityNicknameDisplayController.entityIdParam = farm.Id;

            System.assertEquals('', entityNicknameDisplayController.entityNickname);
        }
    }

    @IsTest
    private static void testFarmNicknameRetrieval() {
        User communityUser = [SELECT Id, ContactId FROM User WHERE Email = :testUserEmail];
        Account farm = [SELECT Id, Name FROM Account WHERE Name = 'Farm1' AND RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId LIMIT 1];
        Individual_Relationship__c derivedRelationship = [SELECT Id, Entity_Nickname__c, Farm__c FROM Individual_Relationship__c WHERE RecordTypeId = :GlobalUtility.derivedRelationshipRecordTypeId And Farm__c = :farm.Id];

        derivedRelationship.Entity_Nickname__c = 'Bob';

        update (derivedRelationship);

        System.runAs(communityUser) {
            AMSEntityNicknameDisplayController entityNicknameDisplayController = new AMSEntityNicknameDisplayController();

            entityNicknameDisplayController.entityIdParam = farm.Id;

            System.assertEquals('Bob', entityNicknameDisplayController.entityNickname);
        }
    }
}