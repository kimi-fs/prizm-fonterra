/**
 * Description: PreferenceHistoryController
 * @author: Rory Figgins (Trineo)
 * @date: October 2018
 * @test: PreferenceHistoryControllerTest
 */
public with sharing class PreferenceHistoryController {

    public Contact individual {get; set;}
    public String scope {get; set;}
    public String sorting {get; set;}
    /* MY PREFERENCES */
    public List<Channel_Preference__c> myPreferencesChannelPreferences {get;set;}
    public List<PreferenceHistoryService> myPreferencesChannelPreferenceHistory {get; set;}

    /* MY FARM PREFERENCES */
    public List<PreferenceHistoryService> myFarmPreferenceHistory {get; set;}

    public PreferenceHistoryController(ApexPages.StandardController standardController) {
        this.individual = [SELECT Id FROM Contact WHERE Id = :ApexPages.currentPage().getParameters().get('cid') LIMIT 1];
        this.scope = ApexPages.currentPage().getParameters().get('scope');
        this.sorting = ApexPages.currentPage().getParameters().get('sort');

        if (this.scope == 'personal') {
            List<Channel_Preference__c> myPreferencesChannelPreferences = PreferenceHistoryController.getMyPreferencesChannelPreferences(this.individual.Id);

            this.myPreferencesChannelPreferenceHistory = new List<PreferenceHistoryService>();
            this.myPreferencesChannelPreferenceHistory = PreferenceHistoryService.getMyPreferenceHistory(myPreferencesChannelPreferences);
            this.myPreferencesChannelPreferenceHistory.addAll(PreferenceHistoryService.getIndividualHistory(this.individual));
            this.myPreferencesChannelPreferenceHistory.sort();

            if (this.sorting != null) {
                this.myPreferencesChannelPreferenceHistory = sortPreferenceHistory(this.myPreferencesChannelPreferenceHistory, this.sorting);
            }
        } else if (this.scope == 'farm') {
            List<PreferenceService.IndividualPreferenceWrapper> individualPreferenceWrappers = generateIndividualPreferenceWrappers(individual.Id);

            this.myFarmPreferenceHistory = PreferenceHistoryService.getChannelPreferenceHistory(individualPreferenceWrappers);
            this.myFarmPreferenceHistory.sort();

            if (this.sorting != null) {
                this.myFarmPreferenceHistory = sortPreferenceHistory(this.myFarmPreferenceHistory, this.sorting);
            }
        }
    }

    public static List<PreferenceHistoryService> sortPreferenceHistory(List<PreferenceHistoryService> histories, String sorting) {
        if (sorting == 'user') {
            Map<String, List<PreferenceHistoryService>> mapPreferenceHistoriesByUsername = new Map<String, List<PreferenceHistoryService>>();

            for (PreferenceHistoryService history : histories) {
                String username = history.user.FirstName + ' ' + history.user.LastName;

                if (!mapPreferenceHistoriesByUsername.containsKey(username)) {
                    mapPreferenceHistoriesByUsername.put(username, new List<PreferenceHistoryService>());
                }

                mapPreferenceHistoriesByUsername.get(username).add(history);
            }

            List<String> usernames = new List<String>();
            usernames.addAll(mapPreferenceHistoriesByUsername.keySet());
            usernames.sort();

            List<PreferenceHistoryService> sortedHistories = new List<PreferenceHistoryService>();

            for (String username : usernames) {
                for (PreferenceHistoryService history : mapPreferenceHistoriesByUsername.get(username)) {
                    sortedHistories.add(history);
                }
            }

            return sortedHistories;
        } else if (sorting == 'cps') {
            Map<String, List<PreferenceHistoryService>> mapPreferenceHistoriesByChannelPreferenceSettingName = new Map<String, List<PreferenceHistoryService>>();

            for (PreferenceHistoryService history : histories) {
                String settingName = history.setting;

                if (!mapPreferenceHistoriesByChannelPreferenceSettingName.containsKey(settingName)) {
                    mapPreferenceHistoriesByChannelPreferenceSettingName.put(settingName, new List<PreferenceHistoryService>());
                }

                mapPreferenceHistoriesByChannelPreferenceSettingName.get(settingName).add(history);
            }

            List<String> settingNames = new List<String>();
            settingNames.addAll(mapPreferenceHistoriesByChannelPreferenceSettingName.keySet());
            settingNames.sort();

            List<PreferenceHistoryService> sortedHistories = new List<PreferenceHistoryService>();

            for (String settingName : settingNames) {
                for (PreferenceHistoryService history : mapPreferenceHistoriesByChannelPreferenceSettingName.get(settingName)) {
                    sortedHistories.add(history);
                }
            }

            return sortedHistories;
        } else if (sorting == 'entity') {
            Map<String, List<PreferenceHistoryService>> mapPreferenceHistoriesByAccountName = new Map<String, List<PreferenceHistoryService>>();

            for (PreferenceHistoryService history : histories) {
                String accountName = history.account == null ? '' : history.account.Name;

                if (!mapPreferenceHistoriesByAccountName.containsKey(accountName)) {
                    mapPreferenceHistoriesByAccountName.put(accountName, new List<PreferenceHistoryService>());
                }

                mapPreferenceHistoriesByAccountName.get(accountName).add(history);
            }

            List<String> accountNames = new List<String>();
            accountNames.addAll(mapPreferenceHistoriesByAccountName.keySet());
            accountNames.sort();

            List<PreferenceHistoryService> sortedHistories = new List<PreferenceHistoryService>();

            for (String accountName : accountNames) {
                for (PreferenceHistoryService history : mapPreferenceHistoriesByAccountName.get(accountName)) {
                    sortedHistories.add(history);
                }
            }

            return sortedHistories;
        }


        return histories;
    }

    public static List<Channel_Preference__c> getMyPreferencesChannelPreferences(Id contactId){
        Map<Id, Channel_Preference__c> mapChannelPreferenceById = PreferenceService.getChannelPreferenceMap(contactId, true, CommunicationPreferencesController.PERSONAL_CHANNEL_PREFERENCE_SETTING_TYPE);

        return mapChannelPreferenceById.values();
    }

    private static List<PreferenceService.IndividualPreferenceWrapper> generateIndividualPreferenceWrappers(Id contactId) {
        Map<Id, Channel_Preference_Setting__c> mapChannelPreferenceSettingById = PreferenceService.getChannelPreferenceSettingMap(CommunicationPreferencesController.FARM_CHANNEL_PREFERENCE_SETTING_TYPE);
        Map<Id, Channel_Preference__c> mapChannelPreferenceById = PreferenceService.getChannelPreferenceMap(contactId, true, CommunicationPreferencesController.FARM_CHANNEL_PREFERENCE_SETTING_TYPE);

        if (mapChannelPreferenceById.keySet().size() == 0) {
            return null;
        }

        List<PreferenceService.IndividualPreferenceWrapper> individualPreferenceWrappers = new List<PreferenceService.IndividualPreferenceWrapper>();

        for (Channel_Preference__c cp : mapChannelPreferenceById.values()) {
            individualPreferenceWrappers.add(new PreferenceService.IndividualPreferenceWrapper(cp, mapChannelPreferenceSettingById.get(cp.Channel_Preference_Setting__c)));
        }

        return individualPreferenceWrappers;
    }

    public PageReference returnToPreferences() {
        return new PageReference('/apex/CommunicationPreferences?scontrolCaching=1&id=' + individual.Id);
    }
}