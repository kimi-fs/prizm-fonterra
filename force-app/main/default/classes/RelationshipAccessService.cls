/**
 * RelationshipAccessService
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately.
 *
 * Author: Ed (Trineo), Amelia (Trineo)
 * Date: July 2017
 */
public without sharing class RelationshipAccessService {
    public class RAException extends Exception { }

    /*
     * When adding a new access level -
     * 1. add the checkbox on IR,
     * 2. create an enum value to Access below, in the fields to query and a logic statement in hasSelectedAccess.
     * 3. To display on AMSThirdParty add to the Relationship_Access field set
     */

    // PRIMARY is a special case - it's not described by an access field but by the type of relationship being derived.
    public enum Access {
        ACCESS_PRIMARY,
        ACCESS_PRODUCTION_AND_QUALITY,
        ACCESS_TARGETS,
        ACCESS_MILKING_TIMES,
        ACCESS_STATEMENTS
    }

    public static final List<String> IR_FIELDS_TO_QUERY = new List<String> {
        'Access__c',
        'Access_Begin__c',
        'Access_End__c',
        'Access_Sponsoring__c',
        'Access_Valid__c',
        'Active__c',

        'Access_Production_Quality__c',
        'Access_Statements__c',
        'Access_Targets__c',
        'Access_Milking_Times_Collection__c',

        'Creation_Date__c',
        'Deactivation_Date__c',

        'Entity_Nickname__c',

        'Farm__c',
        'Farm__r.Name',
        'Farm__r.Farm_Name__c',
        'Party__c',
        'Party__r.Name',
        'Party__r.Party_ID__c',
        'Farm__r.Rep_Area_Manager__c',
        'Farm__r.Rep_Area_Manager__r.Name',
        'Farm__r.Rep_Area_Manager__r.Email',
        'Farm__r.Rep_Area_Manager__r.Phone',
        'Farm__r.Rep_Area_Manager__r.MobilePhone',
        'Farm__r.Quality_Specialist__c',
        'Farm__r.Quality_Specialist__r.Name',
        'Farm__r.Quality_Specialist__r.Email',
        'Farm__r.Quality_Specialist__r.Phone',
        'Farm__r.Quality_Specialist__r.MobilePhone',
        'Farm__r.Rep_Inbound_Service_Specialist__c',
        'Farm__r.Rep_Inbound_Service_Specialist__r.Name',
        'Farm__r.Rep_Inbound_Service_Specialist__r.Email',
        'Farm__r.Rep_Inbound_Service_Specialist__r.Phone',
        'Farm__r.Rep_Inbound_Service_Specialist__r.MobilePhone',

        'Id',
        'Individual__c',
        'Individual__r.Email',
        'Individual__r.Name',
        'Individual__r.MobilePhone',
        'Individual__r.Phone',
        'PF_Role__c',
        'RecordTypeId',
        'Role__c'
    };

    public static final List<String> ER_FIELDS_TO_QUERY = new List<String> {
        'Id',
        'Farm__c',
        'Farm__r.Name',
        'Farm__r.Farm_Name__c',
        'Farm__r.Farm_Region__r.Name',
        'Party__c',
        'Party__r.Name',
        'Active__c'
    };

    public static final List<String> OWNERSHIP_ROLES = new List<String> {
        'Director',
        'Partner',
        'Trustee',
        'Sole Trader',
        'Executor',
        'Power of Attorney',
        'Shareholder',
        'Member'
    };

    public static final String ER_RECORDTYPE_PARTYFARMID = GlobalUtility.partyFarmRecordTypeId;

    public static final Set<String> ER_ACTIVE_ACCESS_ROLES = new Set<String> {
        GlobalUtility.ER_ROLE_CONTRACTMILKER_OWNER,
        GlobalUtility.ER_ROLE_SHAREMILKER,
        GlobalUtility.ER_ROLE_CONTRACTMILKER
    };

    private static List<Individual_Relationship__c> getRelationships(String queryCondition, String order) {
        String query = 'SELECT ' + String.join(IR_FIELDS_TO_QUERY, ',');
        query += ' FROM Individual_Relationship__c ';
        query += queryCondition;
        query += ' ' + order;
        System.debug(query);
        List<Individual_Relationship__c> relationships = Database.query(query);

        return relationships;
    }

    private static List<Individual_Relationship__c> getRelationships(String queryCondition) {
        return getRelationships(queryCondition, ' ');
    }

    /**************************
    * Access functions
    **************************/

    /**
     * Is this entity visible (at any level) for this individual.
     */
    static public Boolean isAccessVisible(Id individualId, Id entityId) {
        Boolean access = false;

        String queryCondition = 'WHERE Active__c = true AND ' +
            'Individual__c = \'' + individualId + '\' AND ' +
            '(Farm__r.Id = \'' + entityId + '\' OR ' +
            ' Party__r.Id = \'' + entityId + '\')';

        List<Individual_Relationship__c> relationships = getRelationships(queryCondition);

        if (!relationships.isEmpty()) {
            access = true;
        }

        return access;
    }

    /**
     * Does this relationship provide *valid* access to the farm
     */
    static public Boolean isAccessVisible(Individual_Relationship__c relationship) {
        Boolean accessGranted = false;
        Id restrictedFarmRecordType = GlobalUtility.individualRestrictedFarmRecordTypeId;

        if (relationship != null) {
            if (relationship.Active__c == true) {
                if (relationship.RecordTypeId == restrictedFarmRecordType) {
                    accessGranted = relationship.Access_Valid__c;
                } else {
                    accessGranted = true;
                }
            }
        }

        return accessGranted;
    }

    static public Boolean isAccessGranted(Individual_Relationship__c relationship, Access requiredAccess) {
        return isAccessGranted(relationship, requiredAccess, true);
    }

    /**
     * Does this relationship provide the requested level of access.
     */
    static public Boolean isAccessGranted(Individual_Relationship__c relationship, Access requiredAccess, Boolean allowIndividualFarmRecordTypes) {
        Boolean accessGranted = false;

        Id derivedRelationshipRecordTypeId = GlobalUtility.derivedRelationshipRecordTypeId;
        Id restrictedFarmRecordType = GlobalUtility.individualRestrictedFarmRecordTypeId;
        Id individualFarmRelationshipRecordTypeId = GlobalUtility.individualFarmRecordTypeId;

        if (relationship != null) {
            if (relationship.Active__c == true) {
                if (relationship.RecordTypeId == restrictedFarmRecordType) {
                    if (relationship.Access_Valid__c == true &&
                        hasSelectedAccess(requiredAccess, relationship))
                    {
                        accessGranted = true;
                    }
                } else if (relationship.RecordTypeId == individualFarmRelationshipRecordTypeId && allowIndividualFarmRecordTypes) {
                    if (hasSelectedAccess(requiredAccess, relationship)) {
                        accessGranted = true;
                    }
                } else if (relationship.RecordTypeId == derivedRelationshipRecordTypeId) {
                    accessGranted = true;
                }
            }
        } else {
            throw new RAException('Required field Relationship_Access_Count__c not present');
        }

        return accessGranted;
    }

    /**
     * Check for the type of access - only applicable to Restricted Record Type, so not checking
     * for PRIMARY.
     */
    private static Boolean hasSelectedAccess(Access accessLevel, Individual_Relationship__c relationship) {
        Boolean hasAccess = false;
        if (accessLevel == Access.ACCESS_PRODUCTION_AND_QUALITY) {
            hasAccess = relationship.Access_Production_Quality__c;
        } else if (accessLevel == Access.ACCESS_TARGETS) {
            hasAccess = relationship.Access_Targets__c;
        } else if (accessLevel == Access.ACCESS_STATEMENTS) {
            hasAccess = relationship.Access_Statements__c;
        } else if (accessLevel == Access.ACCESS_MILKING_TIMES) {
            hasAccess = relationship.Access_Milking_Times_Collection__c;
        }

        return hasAccess;
    }

    /**
     * Does this relationship have no third party access flags selected
     */
    public static Boolean hasNoAccess(Individual_Relationship__c relationship) {
        Boolean hasNoAccess = true;
        for (Access a : Access.values()) {
            if (hasSelectedAccess(a, relationship)) {
                hasNoAccess = false;
                break;
            }
        }

        return hasNoAccess;
    }

    static public Boolean isAccessGranted(Id individualId, Id entityId, Access accessRequested) {
        return isAccessGranted(individualId, entityId, accessRequested, true);
    }

    /*******************
     * Query whether a relationship is valid and allowed. This function requires a query, and so should not
     * be invoked within a loop - if you need it within a loop then write a new access function here with
     * the appropriate parameters.
     */
    static public Boolean isAccessGranted(Id individualId, Id entityId, Access accessRequested, Boolean allowIndividualFarmRecordTypes) {
        Boolean accessGranted = false;

        Id restrictedFarmRecordType = GlobalUtility.individualRestrictedFarmRecordTypeId;

        /*
         * Perform the query from the point of view of the IR for the relationship we are interested in,
         * that way we are ensuring that the relationship is present and valid
         */

        String queryCondition = 'WHERE Access_Valid__c = true AND ' +
            'Individual__c = \'' + individualId + '\' AND ' +
            '(Farm__r.Id = \'' + entityId + '\' OR ' +
            ' Party__r.Id = \'' + entityId + '\')';

        List<Individual_Relationship__c> relationships = getRelationships(queryCondition);
        System.debug(individualId);
        System.debug(entityId);
        System.debug('Relationships: ' + relationships);
        for (Individual_Relationship__c relationship : relationships) {
            accessGranted = isAccessGranted(relationship, accessRequested, allowIndividualFarmRecordTypes);
            if (accessGranted) {
                // One valid access is enough.
                break;
            }
        }

        return accessGranted;
    }

    /**
     * Return a list of farms that the user has access to in any form whatsoever, whether it
     * be restricted or full access.
     */
    public static List<Account> accessToFarms(Id individualId) {
        List<Account> farms = new List<Account>();
        Set<Id> farmIds = new Set<Id>();

        String queryCondition = 'WHERE Access_Valid__c = true AND ' +
            'Individual__c = \'' + individualId + '\' AND ' +
            'Farm__c != null';

        List<Individual_Relationship__c> relationships = getRelationships(queryCondition);

        for (Individual_Relationship__c relationship : relationships) {
            if (isAccessVisible(relationship)) {
                if (!farmIds.contains(relationship.Farm__c)) {
                    farms.add(
                        new Account(
                            Id = relationship.Farm__c,
                            Name = relationship.Farm__r.Name,
                            Farm_Name__c = relationship.Farm__r.Farm_Name__c
                        )
                    );
                    farmIds.add(relationship.Farm__c);
                }
            }
        }

        return farms;
    }

    public static List<Account> accessToFarms(Id individualId, Access accessRequested) {
        return accessToFarms(individualId, accessRequested, true);
    }

    /**
     * Return a list of farms that have the requested level of access
     */
    public static List<Account> accessToFarms(Id individualId, Access accessRequested, Boolean allowIndividualFarmRecordTypes) {
        List<Account> farms = new List<Account>();
        Set<Id> farmIds = new Set<Id>();
        String queryCondition = 'WHERE Access_Valid__c = true AND ' +
            'Individual__c = \'' + individualId + '\' AND ' +
            'Farm__c != null ';
        String orderCondition = 'ORDER BY Farm__r.Name ';

        List<Individual_Relationship__c> relationships = getRelationships(queryCondition, orderCondition);

        for (Individual_Relationship__c relationship : relationships) {
            Boolean validAccess = false;
            validAccess = isAccessGranted(relationship, accessRequested, allowIndividualFarmRecordTypes);
            if (validAccess) {
                if (!farmIds.contains(relationship.Farm__c)) {
                    farms.add(
                        new Account(
                            Id = relationship.Farm__c,
                            Name = relationship.Farm__r.Name,
                            Farm_Name__c = relationship.Farm__r.Farm_Name__c
                        )
                    );
                    farmIds.add(relationship.Farm__c);
                }
            }
        }

        return farms;
    }

    /**
     * All of the farms that we are Owner/share farmer for
     */
    public static List<Individual_Relationship__c> derivedRelationshipsForOwnedFarms(Id individualId) {
        Id derivedRelationshipRecordTypeId = GlobalUtility.derivedRelationshipRecordTypeId;

        List<Individual_Relationship__c> ownedFarms = [SELECT Id,
            Active__c,
            Farm__c,
            Farm__r.Name,
            Party__c,
            Party__r.Name,
            Access_Valid__c,
            Access__c,
            Access_Statements__c,
            Access_Sponsoring__c,
            Access_Targets__c
            FROM Individual_Relationship__c
            WHERE Individual__c = : individualId AND
            Role__c IN : OWNERSHIP_ROLES AND
            Farm__c != null AND
            Active__c = true AND
            RecordTypeId = : derivedRelationshipRecordTypeId];

        return ownedFarms;
    }

    /**
     * All of the farms that we are Owner for (AU)
     */
    public static List<Individual_Relationship__c> getDerivedRelationshipsForOwnedFarmsAU(Id individualId) {
        List<Individual_Relationship__c> ownedFarms = [SELECT
            Id,
            Individual__c,
            Active__c,
            Farm__c,
            Party__c,
            Farm__r.Name,
            Access_Valid__c,
            Farm__r.Agreement_Type__c,
            Farm__r.Rep_Area_Manager__r.Name,
            Farm__r.Rep_Area_Manager__r.MobilePhone
            FROM
            Individual_Relationship__c
            WHERE
            Individual__c = : individualId AND
            PF_Role__c = : GlobalUtility.IR_PF_ROLE_OWNER AND
            Farm__c != null AND
            Active__c = true AND
            Access_Valid__c = true AND
            RecordTypeId = : GlobalUtility.derivedRelationshipRecordTypeId];

        return ownedFarms;
    }

    /*
     * Checks if a user has a derived relationship with pf role of Owner on a specified farm or a restricted relationship with the correct access
     */
    public static Boolean hasIndividualRelationshipAccessToFarm(Id individualId, Id farmId, Access requiredAccess) {
        Id individualRestrictedFarmRecordTypeId = GlobalUtility.individualRestrictedFarmRecordTypeId;
        Id derivedRelationshipRecordTypeId = GlobalUtility.derivedRelationshipRecordTypeId;

        String queryCondition = 'WHERE Individual__c = \'' + individualId + '\'' +
            'AND Farm__c = \'' + farmId + '\'' +
            'AND Access_Valid__c = true ' +
            'AND (RecordTypeId = \'' + individualRestrictedFarmRecordTypeId + '\'' +
            '   OR (RecordTypeId = \'' + derivedRelationshipRecordTypeId + '\'' +
            '   AND PF_Role__c = \'' + GlobalUtility.IR_PF_ROLE_OWNER + '\')) ';

        List<Individual_Relationship__c> relationships = getRelationships(queryCondition);

        // For all of the IRs - do they have the level of access that we are requesting.
        for (Individual_Relationship__c relationship : relationships) {
            if (isAccessGranted(relationship, requiredAccess)) {
                return true;
            }
        }

        return false;
    }

    /*
     * Checks if a user has a derived relationship with pf role of Owner on a specified farm or a restricted relationship with the correct access
     */
    public static List<Individual_Relationship__c> individualRelationshipsToFarm(Id individualId, Access requiredAccess) {
        Id individualRestrictedFarmRecordTypeId = GlobalUtility.individualRestrictedFarmRecordTypeId;
        Id derivedRelationshipRecordTypeId = GlobalUtility.derivedRelationshipRecordTypeId;
        List<Individual_Relationship__c> relationshipsWithAccess = new List<Individual_Relationship__c>();

        String queryCondition = 'WHERE Individual__c = \'' + individualId + '\'' +
            'AND Access_Valid__c = true ' +
            'AND (RecordTypeId = \'' + individualRestrictedFarmRecordTypeId + '\'' +
            '   OR (RecordTypeId = \'' + derivedRelationshipRecordTypeId + '\'' +
            '   AND PF_Role__c = \'' + GlobalUtility.IR_PF_ROLE_OWNER + '\')) ';

        for (Individual_Relationship__c relationship : getRelationships(queryCondition)) {
            Boolean validAccess = false;
            validAccess = isAccessGranted(relationship, requiredAccess, false);
            if (validAccess) {
                relationshipsWithAccess.add(relationship);
            }
        }

        return relationshipsWithAccess;
    }

    /*
     * To check the relationship records which has owner access to a party
     */
    public static List<Individual_Relationship__c> individualRelationshipsForOwnedParties(Id individualId) {
        String roles = '\'' + String.join(OWNERSHIP_ROLES, '\',\'') + '\'';
        String condition = 'WHERE Individual__c = \'' + individualId + '\' AND ' +
            'Role__c IN (' + roles + ') AND ' +
            'Active__c = true AND ' +
            'RecordTypeId = \'' + GlobalUtility.individualPartyRecordTypeId + '\'';
        String order = 'ORDER BY Party__r.Name ASC';

        return getRelationships(condition, order);
    }

    /**
     * Gets all current and future individual party relationships related to a party
     */
    public static List<Individual_Relationship__c> allCurrentAndFutureIndividialPartyRelationshipsForParty(Id partyId) {
        String condition = ' WHERE Party__c = \'' + partyId + '\' AND ' +
            ' RecordTypeId = \'' + GlobalUtility.individualPartyRecordTypeId + '\' AND' +
            ' Active__c = true';

        return getRelationships(condition);
    }

    /**
     * For all of the farms that the individualId user has the role owner or share farmer with return all of the
     * relationships with that farm and their access to that farm.
     */
    public static List<Individual_Relationship__c> relationshipsForOwnedFarms(Id individualId) {
        Id individualFarmRecordTypeId = GlobalUtility.individualFarmRecordTypeId;
        Id individualRestrictedFarmRecordTypeId = GlobalUtility.individualRestrictedFarmRecordTypeId;
        Id derivedRelationshipRecordTypeId = GlobalUtility.derivedRelationshipRecordTypeId;

        // Two stages to this - first all of the farms that we are Owner/Shared Farmer for, and then
        // get all of the relationships for those farms.

        // Note that we are only getting derived relationships - since those are the ones that we have the ability to sponsor
        // for.
        List<Individual_Relationship__c> ownedFarms = derivedRelationshipsForOwnedFarms(individualId);

        List<Id> ownedFarmIds = new List<Id>();

        for (Individual_Relationship__c relationship : ownedFarms) {
            ownedFarmIds.add(relationship.Farm__c);
        }

        System.debug('Owned Farm Ids: ' + ownedFarmIds);

        // Now for the farms that we own get all the Active relationships for that farm, note that we
        // are using Access_Valid here, which is checking that any sponsored relationships are still
        // valid. So we will *not* be returning any expired relationships.
        String query =  'SELECT ' + String.join(IR_FIELDS_TO_QUERY, ',') +
            ' FROM Individual_Relationship__c ' +
            'WHERE Farm__c IN :ownedFarmIds AND ' +
            'Active__c = true AND ' +
            'Access_Valid__c = true AND ' +
            '(RecordTypeId = :individualFarmRecordTypeId OR ' +
            ' RecordTypeId = :individualRestrictedFarmRecordTypeId OR ' +
            ' RecordTypeId = :derivedRelationshipRecordTypeId)' +
            ' ORDER BY Farm__r.Name ASC ';

        List<Individual_Relationship__c> relationships = Database.query(query);

        System.debug('Relationships: ' + relationships);

        return relationships;
    }

    public static List<Individual_Relationship__c> upsertIndividualRelationships(List<Individual_Relationship__c> relationships) {
        upsert relationships;

        return relationships;
    }

    /**
     * Update, but only the fields you specifically want to update
     *
     * Grabs the values from the object and puts it on a new object with only the id populated, then updates the object
     */
    public static void updateIndividualRelationships(List<Individual_Relationship__c> individualRelationships, List<String> fieldsToUpdate) {
        List<Individual_Relationship__c> individualRelationshipsToUpdate = new List<Individual_Relationship__c>();

        for (Individual_Relationship__c individualRelationship : individualRelationships) {
            Individual_Relationship__c individualRelationshipToUpdate = new Individual_Relationship__c(Id = individualRelationship.Id);

            for (String fieldToUpdate : fieldsToUpdate) {
                individualRelationshipToUpdate.put(fieldToUpdate, individualRelationship.get(fieldToUpdate));
            }

            individualRelationshipsToUpdate.add(individualRelationshipToUpdate);
        }

        update individualRelationshipsToUpdate;
    }

    /**
     * Given a list of Farm Ids and Party Ids return the list of ERs that map between those farms and partys,
     */
    public static List<Entity_Relationship__c> entityRelationshipsFromFarmParty(List<Id> farmIds, List<Id> partyIds) {
        String query =  'SELECT ' + String.join(ER_FIELDS_TO_QUERY, ',') +
            ' FROM Entity_Relationship__c ' +
            ' WHERE (Party__c IN :partyIds AND Farm__c IN :farmIds) ' +
            ' AND Active__c = true';

        List<Entity_Relationship__c> relationships = Database.query(query);

        System.debug('Relationships: ' + relationships);

        return relationships;
    }

    /*
     * Get the list of active ERs from a given Party Id where the party has an active access (Role = Owner/Share Milker/Contract Milker)
     */
    public static List<Entity_Relationship__c> activeEntityRelationshipsFromParty(Id partyId) {
        String query =  'SELECT ' + String.join(ER_FIELDS_TO_QUERY, ',') +
            ' FROM Entity_Relationship__c ' +
            ' WHERE Party__c = :partyId ' +
            '     AND RecordTypeId = :ER_RECORDTYPE_PARTYFARMID' +
            '     AND Role__c IN :ER_ACTIVE_ACCESS_ROLES' +
            '     AND Active__c = true';

        List<Entity_Relationship__c> relationships = Database.query(query);

        return relationships;
    }

    /**
     * Return all of the Farm-Party Entity Relationships that this Individual has access to.
     *
     * 1. This will consist of the Sponsoring ERs for TPA relationships
     * 2. All of the Party-Farm ERs outwards from a party that we have a Individual-Party relationship with
     */
    public static Map<Id, Entity_Relationship__c> accessToEntityRelationships(Id individualId, Access requiredAccess) {
        Id individualFarmRecordTypeId = GlobalUtility.individualFarmRecordTypeId;
        Id individualRestrictedFarmRecordTypeId = GlobalUtility.individualRestrictedFarmRecordTypeId;
        Id derivedRelationshipRecordTypeId = GlobalUtility.derivedRelationshipRecordTypeId;

        // Get all of the Active IRs for this individual
        List<Individual_Relationship__c> relationships = [SELECT Id,
            Active__c,
            Farm__c,
            Farm__r.Name,
            Farm__r.Farm_Name__c,
            Party__c,
            Party__r.Name,
            RecordTypeId,
            Access_Valid__c,
            Access__c,
            Access_Statements__c,
            Access_Sponsoring__c,
            Access_Targets__c
            FROM Individual_Relationship__c
            WHERE Individual__c = : individualId
            AND Active__c = true
            AND Access_Valid__c = true
            AND(
                RecordTypeId = : individualFarmRecordTypeId OR
                RecordTypeId = : individualRestrictedFarmRecordTypeId OR
                RecordTypeId = : derivedRelationshipRecordTypeId
            )];

        // Now filter these out based on the access and type of IR

        Set<Id> partyIds = new Set<Id>();
        Set<Id> erIds = new Set<Id>();

        for (Individual_Relationship__c relationship : relationships) {
            // TPA so we want the Access_Sponsoring__c ER
            if (isAccessGranted(relationship, requiredAccess, true)) {
                erIds.add(relationship.Access_Sponsoring__c);
                partyIds.add(relationship.Party__c);
            }
            // Ignore Individual-Farm relationships, they don't involve any ERs.
        }

        // Given a set of parties that we control, and a set of ERs that are sponsoring us
        // at the desired access level, return the ERs out from those partys and the sponsoring ERs
        String entityQuery =    'SELECT ' + String.join(ER_FIELDS_TO_QUERY, ',') +
            ' FROM Entity_Relationship__c ' +
            ' WHERE (Party__c IN :partyIds OR Id IN :erIds)' +
            ' AND Active__c = true' +
            ' ORDER BY Name ASC';

        Map<Id, Entity_Relationship__c> entityRelationships = new Map<Id, Entity_Relationship__c>(
            (List<Entity_Relationship__c>)Database.query(entityQuery)
        );

        return entityRelationships;
    }

    /**
     * Return a list of IRs to farms that the individual has the required access to.
     *
     * This includes:
     * 1. Restricted IRs with the requested level of access
     * 2. Derived IRs, which will all be full access
     */
    public static List<Individual_Relationship__c> accessToIndividualRelationships(Id individualId, Access requiredAccess) {
        Id individualFarmRecordTypeId = GlobalUtility.individualFarmRecordTypeId;
        Id individualRestrictedFarmRecordTypeId = GlobalUtility.individualRestrictedFarmRecordTypeId;
        Id derivedRelationshipRecordTypeId = GlobalUtility.derivedRelationshipRecordTypeId;

        String queryCondition = 'WHERE Active__c = true AND ' +
            'Individual__c = \'' + individualId + '\'' +
            'AND Farm__c != null ' +
            'AND Active__c = true ' +
            'AND ' +
            '   (RecordTypeId = \'' + derivedRelationshipRecordTypeId + '\'' +
            '    OR ' +
            '     (RecordTypeId = \'' + individualRestrictedFarmRecordTypeId + '\'' +
            '     AND ' +
            '     Access_Valid__c = true ' +
            '    ) ' +
            '   ) ';
        String orderCondition = 'ORDER BY CreatedDate ASC';

        List<Individual_Relationship__c> relationships = getRelationships(queryCondition, orderCondition);

        List<Individual_Relationship__c> relationshipsWithAccess = new List<Individual_Relationship__c>();

        // For all of the IRs - do they have the level of access that we are requesting.
        for (Individual_Relationship__c relationship : relationships) {
            if (isAccessGranted(relationship, requiredAccess, true)) {
                relationshipsWithAccess.add(relationship);
            }
        }

        return relationshipsWithAccess;
    }

    /**
     * Return all individuals associated with the farm (derived and individual-farm) with access
     */
    public static List<Individual_Relationship__c> relationshipsForFarms(List<Id> farmIds) {
        Id individualFarmRecordTypeId = GlobalUtility.individualFarmRecordTypeId;
        Id derivedRelationshipRecordTypeId = GlobalUtility.derivedRelationshipRecordTypeId;

        String queryCondition = 'WHERE Farm__c IN (\'' + String.join(farmIds, '\',\'') + '\') AND ' +
            ' Active__c = true AND ' +
            ' Access_Valid__c = true AND ' +
            ' Individual__r.RecordType.Name = \'Individual AU\' AND ' +
            ' (RecordTypeId = \'' + derivedRelationshipRecordTypeId + '\' OR ' +
            ' RecordTypeId = \'' + individualFarmRecordTypeId + '\')';

        List<Individual_Relationship__c> individualsOnFarm = getRelationships(queryCondition);

        return individualsOnFarm;
    }

    /**
     * Return all TPA users with access to production and quality
     */
    public static List<Individual_Relationship__c> restrictedIndividualRelationshipsWithProductionAndQualityAccess(List<Id> farmIds) {
        Id individualRestrictedFarmRecordTypeId = GlobalUtility.individualRestrictedFarmRecordTypeId;

        String queryCondition = 'WHERE Active__c = true AND ' +
            ' Access_Valid__c = true AND ' +
            ' Access_Production_Quality__c = true AND ' +
            ' RecordTypeId = \'' + individualRestrictedFarmRecordTypeId + '\' AND ' +
            ' Farm__r.Id IN (\'' + String.join(farmIds, '\',\'') + '\')';

        List<Individual_Relationship__c> restrictedIndividualRelationshipsWithProductionAndQualityAccess = getRelationships(queryCondition);

        return restrictedIndividualRelationshipsWithProductionAndQualityAccess;
    }

    /**
     * Return all TPA users with access to statements and income estimator
     */
    public static List<Individual_Relationship__c> restrictedIndividualRelationshipsWithStatementsAccess(List<Id> farmIds) {
        Id individualRestrictedFarmRecordTypeId = GlobalUtility.individualRestrictedFarmRecordTypeId;

        String queryCondition = 'WHERE Active__c = true AND ' +
            ' Access_Valid__c = true AND ' +
            ' Access_Statements__c = true AND ' +
            ' RecordTypeId = \'' + individualRestrictedFarmRecordTypeId + '\' AND ' +
            ' Farm__r.Id IN (\'' + String.join(farmIds, '\',\'') + '\')';

        List<Individual_Relationship__c> restrictedIndividualRelationshipsWithProductionAndQualityAccess = getRelationships(queryCondition);

        return restrictedIndividualRelationshipsWithProductionAndQualityAccess;
    }

    /**
     * Return all individuals that are eligible to receive notifications
     *
     * Only return one individual relationship per individual per farm
     */
    public static List<Individual_Relationship__c> individualRelationshipsWithProductionAndQualityNotifications(List<Id> farmIds) {
        List<Individual_Relationship__c> individualRelationshipsWithNotifications = new List<Individual_Relationship__c>();

        individualRelationshipsWithNotifications.addAll(relationshipsForFarms(farmIds));
        individualRelationshipsWithNotifications.addAll(restrictedIndividualRelationshipsWithProductionAndQualityAccess(farmIds));

        Map<String, Individual_Relationship__c> farmAndContactIdToIndividualRelationshipMap = new Map<String, Individual_Relationship__c>();

        for (Individual_Relationship__c individualRelationship : individualRelationshipsWithNotifications) {
            String farmAndContactIdKey = individualRelationship.Farm__c + '-' + individualRelationship.Individual__c;
            farmAndContactIdToIndividualRelationshipMap.put(farmAndContactIdKey, individualRelationship);
        }

        return farmAndContactIdToIndividualRelationshipMap.values();
    }

    /**
     * Return all individuals that are eligible to receive notifications
     *
     * Only return one individual relationship per individual per farm
     */
    public static List<Individual_Relationship__c> individualRelationshipsWithStatementNotifications(List<Id> farmIds) {
        List<Individual_Relationship__c> individualRelationshipsWithNotifications = new List<Individual_Relationship__c>();

        individualRelationshipsWithNotifications.addAll(relationshipsForFarms(farmIds));
        individualRelationshipsWithNotifications.addAll(restrictedIndividualRelationshipsWithStatementsAccess(farmIds));

        Map<String, Individual_Relationship__c> farmAndContactIdToIndividualRelationshipMap = new Map<String, Individual_Relationship__c>();

        for (Individual_Relationship__c individualRelationship : individualRelationshipsWithNotifications) {
            String farmAndContactIdKey = individualRelationship.Farm__c + '-' + individualRelationship.Individual__c;
            farmAndContactIdToIndividualRelationshipMap.put(farmAndContactIdKey, individualRelationship);
        }

        return farmAndContactIdToIndividualRelationshipMap.values();
    }

    /**
     * All IRs for an Individual
     */
    public static List<Individual_Relationship__c> allIndividualRelationshipsForIndividual(Id contactId) {
        String queryCondition = 'WHERE Active__c = true AND ' +
            ' Access_Valid__c = true AND ' +
            ' Individual__c = \'' + contactId + '\'';

        List<Individual_Relationship__c> allIndividualRelationshipsForIndividual = getRelationships(queryCondition);

        return allIndividualRelationshipsForIndividual;
    }

    /** 
     * Get the pricing option from the agreement the farmer has agreed to
     */
    public static String getActiveAgreementPriceOption(Id farmId) {
        Agreement__c agreement = [SELECT Pricing_Option__c FROM Agreement__c WHERE Farm__c = :farmId ORDER BY Start_Date__c DESC LIMIT 1];
        return agreement.Pricing_Option__c;
    }
}