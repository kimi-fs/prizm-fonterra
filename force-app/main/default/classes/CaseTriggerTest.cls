/**
 * Logic class controlling behaviour of the Case trigger
 * @author Sarah Hay
 * @date March 2016
 */
@isTest
private class CaseTriggerTest {

    //Note seeAllData is only used to facilitate seeing FeedItem records that get created
    @isTest(seeAllData = true) static void testFeedItemInsert() {
        Test.startTest();
        Service_Centre_Settings__c scs = Service_Centre_Settings__c.getOrgDefaults();
        scs.Post_Description_to_Chatter__c = true;
        Case c = new Case(Description = 'My Test Description');
        upsert scs;
        insert c;
        Test.stopTest();

        List<FeedItem> query = [SELECT Id, Body FROM FeedItem WHERE ParentId = :c.Id];
        List<Case> cases = [Select Id FROM Case WHERE Id = :c.Id];
        System.assertEquals(1, cases.size());
        System.assertEquals(1, query.size());
        System.assertEquals('Description: My Test Description', query.get(0).Body);

    }

    @isTest(seeAllData = true) static void testBulkFeedItemInsert() {
        Test.startTest();
        Service_Centre_Settings__c scs = Service_Centre_Settings__c.getOrgDefaults();
        scs.Post_Description_to_Chatter__c = true;
        List<Case> cases = new List<Case>();
        cases.add(new Case(Description = 'test 1'));
        cases.add(new Case(Description = 'test 2'));
        upsert scs;
        insert cases;
        Test.stopTest();

        List<FeedItem> query = [SELECT Id, Body, Type FROM FeedItem WHERE ParentId = :cases];
        System.assertEquals(2, query.size());
        System.assertEquals('Description: test 1', query.get(0).Body);
        System.assertEquals('Description: test 2', query.get(1).Body);
        System.assertEquals('TextPost', query.get(1).Type);

    }

}