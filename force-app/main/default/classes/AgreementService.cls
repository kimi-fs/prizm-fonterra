/**
 * Description: Service for the Agreement
 * @author: Clayde Bael (Trineo)
 * @date: May 2019
 * @test: AMSIncomeEstimatorController_Test, AgreementServiceTest
 */

public without sharing class AgreementService {
    public class AgreementException extends Exception {}

    public static final String AGREEMENT_STATUS_PENDING = 'Pending';
    public static final String AGREEMENT_STATUS_SUBMITTED = 'Submitted';
    public static final String AGREEMENT_STATUS_EXECUTED = 'Executed';
    public static final String AGREEMENT_STATUS_CANCELLED = 'Cancelled';
    public static final String AGREEMENT_TYPE_STANDARD = 'Standard Pricing Exclusive';

    public static final String SIGNING_METHOD_ONLINE = 'Online';

    private static final String SEND_EMAIL_CREATE_CASE = 'SEND_EMAIL_CREATE_CASE';
    private static Integer countMethodRun = 0;

    /**
     * Retrieves the Latest Agreement for the Party-Farm within the Start and End Date
     * Add fields as necessary
     */
    public static Agreement__c getLatestAgreement(Id farmId, Id partyId, Id recordTypeId, Date startDate, Date endDate) {
        String query = ' SELECT  Specialty_Milk_Price__c, '
                     + '         (SELECT ' + QueryUtil.getFieldsFromObjectForQuery('Season__c')
                     + '          FROM Agreement_Seasons__r '
                     + '          LIMIT 1 ) '
                     + ' FROM Agreement__c '
                     + ' WHERE '
                     + '        Farm__c = :farmId '
                     + '        AND Party__c = :partyId '
                     + '        AND RecordTypeId = :recordTypeId '
                     + '        AND Active__c = true '
                     +          (startDate != null ? ' AND Start_Date__c >= :startDate ' : '')
                     +          (endDate != null ? ' AND (Start_Date__c < :endDate )' : '')
                     + ' ORDER BY '
                     + '        Start_Date__c DESC, '
                     + '        End_Date__c DESC NULLS FIRST '
                     + ' LIMIT 1 ';

        List<Agreement__c> agreements = Database.query(query);

        if (!agreements.isEmpty()) {
            return agreements[0];
        }
        return null;
    }

    public static List<Agreement__c> getCurrentContactAgreements(List<Individual_Relationship__c> ownerIrels) {
        List<String> farmIds = new List<String>();

        for (Individual_Relationship__c irel : ownerIrels) {
            farmIds.add(irel.Farm__c);
        }

        List<Agreement__c> agreements = [SELECT
                                               Id,
                                               Farm__r.Name,
                                               Farm__r.Agreement_Type__c,
                                               Agreement_Status__c,
                                               Start_Date__c,
                                               Signing_Method__c,
                                               Release_Agreement_to_Farmer__c
                                         FROM
                                               Agreement__c
                                         WHERE
                                               Farm__c IN :farmIds
                                               AND
                                                   Agreement_Status__c != :AGREEMENT_STATUS_CANCELLED];
        return agreements;
    }

    public static List<Case> createCaseForSignedAgreements(List<Agreement__c> listNewAgreement, List<String> emailErrorMessages, String exceptionErrorMessage, Boolean isSignedOnline) {
        List<Case> caseToCreate = new List<Case>();
        String errors;
        if(emailErrorMessages != null) {
            for (String errorMsg : emailErrorMessages) {
                    errors = errors + errorMsg + '\n';
            }
        }
        for (Agreement__c agreement : listNewAgreement) {
            Case caseRecord;
            if (agreement.Agreement_Status__c == AGREEMENT_STATUS_EXECUTED) {
                caseRecord = CaseService.signedAgreementCase(agreement, isSignedOnline);

                if (String.isNotEmpty(errors)) {
                    caseRecord.Description += '\n \n Errors during sending email: \n' + errors;
                }
                if (String.isNotEmpty(exceptionErrorMessage)) {
                    caseRecord.Description += '\n \n' + exceptionErrorMessage;
                }
                caseToCreate.add(caseRecord);
            }
        }
        insert caseToCreate;
        return caseToCreate;

    }

    public static Boolean isOnlineSignedAgreement (Agreement__c agreement) {
        Boolean isOnlineSignedAgreement = false;
        if (String.isNotEmpty(agreement.Farm__c) &&
            String.isNotEmpty(agreement.Party__c) &&
            agreement.RecordTypeId == GlobalUtility.agreementStandardRecordTypeId &&
            (agreement.Agreement_Status__c == AGREEMENT_STATUS_EXECUTED ||
             agreement.Agreement_Status__c == AGREEMENT_STATUS_SUBMITTED) &&
            agreement.Signing_Method__c == SIGNING_METHOD_ONLINE &&
            String.isNotEmpty(agreement.User_Signed__c)
        ) {
           isOnlineSignedAgreement = true;
        }
        return isOnlineSignedAgreement;
    }

    public static List<Agreement__c> getAgreements(Id farmId, Id partyId, Date startDate, Date endDate, Boolean isStandardFarm) {
        Id agreementStandardRecordType = GlobalUtility.agreementStandardRecordTypeId;
        String query = 'SELECT  Id, '+
                    'Name, '+
                    'RecordTypeId, '+
                    'Business_Type__c, '+
                    'Signatory_1_Full_Name__c, '+
                    'Signatory_2_Full_Name__c, '+
                    'Unsigned_Document_ID__c, '+
                    'Farm__c, '+
                    'Party__c, '+
                    'Active_Milk_Supply_Agreement__c, '+
                    'Agreement_Status__c, '+
                    'Signing_Method__c, '+
                    'User_Signed__c, '+
                    'User_Signed__r.ContactId, '+
                    'Start_Date__c, '+
                    'End_Date__c, '+
                    'Execution_Date__c, '+
                    'Farm_Number__c, '+
                    'Release_Agreement_to_Farmer__c, '+
                    'Agreement_Application__c, '+
                    'Agreement_Application__r.Minimum_Price_Table__c, '+
                    'Agreement_Application__r.Agreement_Type__c, '+
                    'Agreement_Application__r.Allocated_Supply_Number__c, '+
                    'Agreement_Application__r.Region__c, '+
                    'Agreement_Application__r.Farm__c, '+
                    'Agreement_Application__r.Trading_Name__c '+
                'FROM Agreement__c '+
                'WHERE RecordTypeId = :agreementStandardRecordType '+
                'AND Farm__c = :farmId ';
        if(partyId != null) {
            query += ' AND Party__c = :partyId';
        }
        if(!isStandardFarm) {
            query += ' AND Agreement_Application__c != null';
        }
        if(startDate != null) {
            query += ' AND Start_Date__c = :startDate';
        }
        if(endDate != null) {
            query += ' AND End_Date__c = :endDate';
        }
        return Database.query(query);
    }

    public static Non_Standard_Agreement__c getNSA(Id nsaId) {
        return [
            SELECT 
                Agreement_Type__c,
                Allocated_Supply_Number__c,
                Region__c,
                Farm__c,
                Trading_Name__c
            FROM Non_Standard_Agreement__c
            WHERE Id = :nsaId
            LIMIT 1
        ];
    }

    public static Agreement__c getAgreement(Id agreementId) {
        return [
            SELECT  Id,
                    Name,
                    Agreement_Application__c,
                    Agreement_Application__r.Agreement_Type__c,
                    Agreement_Application__r.Conga_Template_Id__c,
                    RecordTypeId,
                    Business_Type__c,
                    Signatory_1_Full_Name__c,
                    Signatory_2_Full_Name__c,
                    Unsigned_Document_ID__c,
                    Farm__c,
                    Party__c,
                    Active_Milk_Supply_Agreement__c,
                    Agreement_Status__c,
                    Signing_Method__c,
                    User_Signed__c,
                    User_Signed__r.ContactId,
                    Start_Date__c,
                    End_Date__c,
                    Execution_Date__c,
                    Farm_Number__c,
                    Release_Agreement_to_Farmer__c
            FROM Agreement__c
            WHERE RecordTypeId = :GlobalUtility.agreementStandardRecordTypeId
            AND Id = :agreementId
        ];
    }

    public static List<Agreement__c> getAgreements(List<Id> agreementIds) {
        return [
            SELECT Id,
                   Name,
                   RecordTypeId,
                   Business_Type__c,
                   Signatory_1_Full_Name__c,
                   Signatory_2_Full_Name__c,
                   Unsigned_Document_ID__c,
                   Farm__c,
                   Party__c,
                   Active_Milk_Supply_Agreement__c,
                   Agreement_Status__c,
                   Signing_Method__c,
                   User_Signed__c,
                   User_Signed__r.ContactId,
                   Start_Date__c,
                   End_Date__c,
                   Execution_Date__c,
                   Farm_Number__c,
                   Release_Agreement_to_Farmer__c
            FROM Agreement__c
            WHERE RecordTypeId = :GlobalUtility.agreementStandardRecordTypeId
            AND Id IN :agreementIds
        ];
    }

    public static Agreement__c createPendingOnlineStandardAgreement(Id farmId, Id partyId, Date startDate, Date endDate) {
        Agreement__c agreement = new Agreement__c(
            Farm__c = farmId,
            Party__c = partyId,
            RecordTypeId = GlobalUtility.agreementStandardRecordTypeId,
            Start_Date__c = startDate,
            End_Date__c = endDate,
            Agreement_Status__c = AGREEMENT_STATUS_PENDING,
            Signing_Method__c = SIGNING_METHOD_ONLINE,
            Unsigned_Document_ID__c = null,
            Agreement_Type__c = AGREEMENT_TYPE_STANDARD,
            Agreement__c = 'Standard'
        );
        return agreement;
    }

    public static Agreement__c updateAgreement(Agreement__c agreement) {
        update agreement;
        return agreement;
    }

    public static Agreement__c insertAgreement(Agreement__c agreement) {
        insert agreement;
        return agreement;
    }

    public static Account getFarm(Id farmId) {
        Account farm = [
            SELECT Id,
                Agreement_Type__c,
                Rep_Area_Manager__c,
                Rep_Area_Manager__r.Name,
                Rep_Area_Manager__r.Phone
            FROM Account
            WHERE Id = :farmId
        ];
        return farm;
    }

    public static Integer getFarmOwnerCount(Id farmId) {
        return [
            SELECT COUNT()
            FROM Individual_Relationship__c
            WHERE Farm__c = :farmId
            AND RecordTypeId = :GlobalUtility.derivedRelationshipRecordTypeId
            AND PF_Role__c = 'Owner'
            AND Active__c = true
        ];
    }

    /**
     * For all content document links, are they to an agreement, if they are to an agreement then update Unsigned_Document_ID__c on that agreement
     * with this document link id.
     */
    public static void updateAgreementsFromDocumentLinks(List<ContentDocumentLink> documentLinks) {
        Map<Id, Id> agreementsToCheckAgainstLinkId = new Map<Id, Id>();

        for (ContentDocumentLink documentLink : documentLinks) {
            Id parentId = documentLink.LinkedEntityId;
            if (parentId.getSobjectType() == SObjectType.Agreement__c.getSObjectType()) {
                agreementsToCheckAgainstLinkId.put(parentId, documentLink.Id);
            }
        }

        List<Agreement__c> agreementsToUpdate = [
            SELECT Id, Agreement_Status__c
            FROM Agreement__c
            WHERE Id IN :agreementsToCheckAgainstLinkId.keySet() AND
                  RecordTypeId = :GlobalUtility.agreementStandardRecordTypeId AND
                  Farm__r.RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId
        ];

        for (Agreement__c agreement : agreementsToUpdate) {
            agreement.Unsigned_Document_ID__c = agreementsToCheckAgainstLinkId.get(agreement.Id);

            if (agreement.Agreement_Status__c == AGREEMENT_STATUS_SUBMITTED) {
                // The Signed version is now created, so change to executed.
                agreement.Agreement_Status__c = AGREEMENT_STATUS_EXECUTED;
            }
        }
        update agreementsToUpdate;
    }

    // This method can't be transferred to @future method as will be called by an asynchronous call (Conga file generation)
    // When the agreement is signed online (AgreementService.isOnlineSignedAgreement method), the status of agreement = 'Executed' and a file will be created asynchronously
    // When the file is created from an asynchronous call and the running user is External System Integration , this method will process the sending of email.
    public static void sendEmailAndCreateCaseForSignedAgreement(List<ContentDocumentLink> contentDocumentLinkNew) {
        List<Id> contentDocumentLinkIds = new List<Id>();

        for (ContentDocumentLink cdl : ContentDocumentLinkNew) {
            if (cdl.LinkedEntityId.getSObjectType().getDescribe().getName() == 'Agreement__c') {
                contentDocumentLinkIds.add(cdl.Id);
            }
        }

        if (!contentDocumentLinkIds.isEmpty()) {
            // Try and send emails from futures rather than inline, since we will usually be called from a Trigger
            if (System.IsBatch() == false && System.isFuture() == false) {
                sendEmailAndCreateCaseForSignedAgreementFuture(contentDocumentLinkIds);
            } else {
                sendEmailAndCreateCaseForSignedAgreementSync(contentDocumentLinkIds);
            }
        }
    }

    @future
    private static void sendEmailAndCreateCaseForSignedAgreementFuture(List<Id> contentDocumentLinkIds) {
        sendEmailAndCreateCaseForSignedAgreementSync(contentDocumentLinkIds);
    }

    private static void sendEmailAndCreateCaseForSignedAgreementSync(List<Id> contentDocumentLinkIds) {
        Set<Id> contentDocumentIds = new Set<Id>();
        List<Id> linkedAgreementIds = new List<Id>();
        if (countMethodRun++ > 0) {
           return;
        }

        List<ContentDocumentLink> contentDocumentLinkNew = [
            SELECT Id, ContentDocumentId, LinkedEntityId, LinkedEntity.Type
            FROM ContentDocumentLink
            WHERE Id IN :contentDocumentLinkIds
        ];

        Id externalSystemIntegratioUserProfileId = [SELECT Id FROM Profile WHERE Name = 'External System Integration User'].Id;
        Map<Id, ContentDocumentLink> contentDocumentFilteredIdsMap = new Map<Id, ContentDocumentLink>();

        if (externalSystemIntegratioUserProfileId != UserInfo.getProfileId()) {
            System.debug('Not matching the correct profile, have profile: ' + UserInfo.getProfileId());
            return;
        }

        for (ContentDocumentLink contentDocumentLink : contentDocumentLinkNew) {
            if (contentDocumentLink.LinkedEntity.Type == 'Agreement__c') {
                contentDocumentIds.add(contentDocumentLink.ContentDocumentId);
            }
        }

        if (contentDocumentIds.isEmpty()) {
            System.debug('No Documents to Process');
            return;
        }

        // Requery the Document Links so that we can get the title of the documents
        List<ContentDocumentLink> contentDocumentDetails = [
            SELECT Id, ContentDocumentId, ContentDocument.Title, LinkedEntityId
            FROM ContentDocumentLink
            WHERE ContentDocumentId IN :contentDocumentIds
        ];

        Map<String, File_Sharing_Settings__c> fileSharingSettings = File_Sharing_Settings__c.getAll();

        // For every document does its filename match any of the file sharing settings, if so
        // does that setting say send emails and create cases for this filename, if so, then do so.
        for (ContentDocumentLink contentDocumentLinkDetails : contentDocumentDetails) {
            for (String fs : fileSharingSettings.keySet()) {
                if (String.isNotEmpty(contentDocumentLinkDetails.ContentDocument.Title) &&
                    contentDocumentLinkDetails.ContentDocument.Title.startsWith(fs)) {
                    // Matched on this File Sharing Setting
                    File_Sharing_Settings__c sharingSetting = fileSharingSettings.get(fs);

                    if (sharingSetting.Actions__c == SEND_EMAIL_CREATE_CASE) {
                        contentDocumentFilteredIdsMap.put(contentDocumentLinkDetails.ContentDocumentId, contentDocumentLinkDetails);
                        linkedAgreementIds.add(contentDocumentLinkDetails.LinkedEntityId);
                        break;
                    }
                }
            }
        }
        List<String> farmList = new List<String>();
        List<String> partyList = new List<String>();
        List<Agreement__c> signedAgreements = new List<Agreement__c>();

        List<Agreement__c> allAgreements = getAgreements(linkedAgreementIds);
        // For agreements that were signed online get the area manager and regional managers emails
        for (Agreement__c agreement : allAgreements) {
            if (AgreementService.isOnlineSignedAgreement(agreement)) {
                signedAgreements.add(agreement);
                farmList.add(agreement.Farm__c);
                partyList.add(agreement.Party__c);
            }
        }

        if (signedAgreements.isEmpty()) {
            System.debug('No agreements to process');
            return;
        }

        List<Individual_Relationship__c> ownerFarmPartyList = [SELECT
                                                                    Id,
                                                                    Individual__c,
                                                                    Individual__r.Email,
                                                                    Farm__r.Rep_Area_Manager__c,
                                                                    Farm__r.Regional_Manager__c,
                                                                    Farm__r.Rep_Area_Manager__r.Email,
                                                                    Farm__r.Regional_Manager__r.Email
                                                               FROM
                                                                    Individual_Relationship__c
                                                               WHERE
                                                                    Farm__c = :farmList AND
                                                                    Party__c = :partyList AND
                                                                    PF_Role__c = :GlobalUtility.IR_PF_ROLE_OWNER AND
                                                                    Active__c = true AND
                                                                    RecordTypeId = :GlobalUtility.derivedRelationshipRecordTypeId];

        List<Messaging.SingleEmailMessage> emailMessagesForFarmOwners = new List<Messaging.SingleEmailMessage>();
        if (GlobalUtility.ORG_WIDE_EMAIL_ADDRESS_FONTERRA_AU != null) {
            emailMessagesForFarmOwners = EmailMessageService.createEmailMessageForIndividuals(
                ownerFarmPartyList,
                new List<Id>(contentDocumentFilteredIdsMap.keySet()),
                'AMS_Community_Signed_Standard_Agreement',
                GlobalUtility.ORG_WIDE_EMAIL_ADDRESS_FONTERRA_AU.Id
            );
        }

        String sendEmailExceptionError;
        List<String> sendEmailErrors = new List<String>();

        try {
            List<Messaging.SendEmailResult> sendEmailResults;
            if (!Test.isRunningTest()) {
                System.debug('Sending emails: ' + emailMessagesForFarmOwners);
                sendEmailResults = Messaging.sendEmail(emailMessagesForFarmOwners);
            }

            for (Messaging.SendEmailResult result : sendEmailResults) {
                if (!result.isSuccess()) {
                    for (Messaging.SendEmailError e : result.getErrors()) {
                        sendEmailErrors.add(e.message);
                    }
                }
            }
        } catch (Exception e) {
            sendEmailExceptionError = e.getMessage();
        }

        // Create Cases to track
        List<Case> createdCases = createCaseForSignedAgreements(signedAgreements, sendEmailErrors, sendEmailExceptionError, true);

        if(!signedAgreements.isEmpty() && !createdCases.isEmpty()) {
            List<ContentDocumentLink> shareFilesToCase = new List<ContentDocumentLink>();

            for (Id contentDocumentId : contentDocumentFilteredIdsMap.keySet()) {
                for (Case c : createdCases) {
                    shareFilesToCase.add(new ContentDocumentLink(ContentDocumentId = contentDocumentId, ShareType = 'V', LinkedEntityId = c.Id));
                }
            }
            insert shareFilesToCase;
        }

    }
}