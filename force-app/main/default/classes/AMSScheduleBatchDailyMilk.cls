public without sharing class AMSScheduleBatchDailyMilk implements Database.Batchable<AggregateResult>, Schedulable, Database.AllowsCallouts, Database.Stateful {

    public List<Id> emailTaskIds = new List<Id>();

    public static final String SMS_DEVELOPER_NAME = 'SMS';
    public static final String EMAIL_DEVELOPER_NAME = 'Email';

    public static void schedule() {
        String expression = ScheduleService.getCRONExpression('AMSScheduleBatchDailyMilk');
        String jobName = ScheduleService.getCRONJobName('AMSScheduleBatchDailyMilk');
        if (String.isNotBlank(expression)) {
            AMSScheduleBatchDailyMilk job = new AMSScheduleBatchDailyMilk();
            System.schedule(jobName, expression, job);
        }
    }

    public void execute(SchedulableContext schedulableContext) {
        Database.executeBatch(new AMSScheduleBatchDailyMilk(), 1);
    }

    public List<AggregateResult> start(Database.BatchableContext batchableContext) {
        return [SELECT
                    Farm_ID__c
                FROM
                    Collection__c
                WHERE
                    (Pick_Up_Date__c >= LAST_N_DAYS:3)
                    AND Result_Sent__c = false
                    AND Farm_ID__r.RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId
                GROUP BY Farm_ID__c];
    }

    private static final String AUFARMSOURCE_WEBSITE_BLURB = 'For full details go to www.farmsource.com.au';

    public class DailyMilkResultFormatter {
        private Collection__c collection;
        private Individual_Relationship__c individualRelationship;

        public DailyMilkResultFormatter(Collection__c collection, Individual_Relationship__c individualRelationship) {
            this.collection = collection;
            this.individualRelationship = individualRelationship;
        }

        public String getSMSText() {
            String smsText = getHeader();
            smsText += getBody();
            return smsText;
        }

        public String getEmailText() {
            String emailText = getHeader();
            emailText += getBody();
            emailText += '\n';
            emailText += AUFARMSOURCE_WEBSITE_BLURB;
            return emailText;
        }

        public String getHeader() {
            return AMSScheduleBatchDailyMilk.getFarmRegionCodeAndName(collection.Farm_ID__r, individualRelationship.Entity_Nickname__c) + ' ' +
                'Farm Source pickup summary ' + ((DateTime) collection.Pick_Up_Date__c).format('dd/MM/yyyy');
        }

        public String getEmailSubject() {
            String header = getHeader();
            header += ' MP' + collection.Pick_Up_Code__c;

            return header;
        }

        private String getBody() {
            String body = '';

            Decimal BMCCTotal = 0.0;
            Decimal bactoscanTotal = 0.0;

            for (Milk_Quality__c milkQuality : collection.Milk_Quality__r) {
                if (milkQuality.name == GlobalUtility.COLLECTION_PERIOD_BMCC_TEST_NAME) {
                    BMCCTotal += Decimal.valueOf(milkQuality.Test_Result__c);
                }
                if (milkQuality.name == GlobalUtility.COLLECTION_PERIOD_BACTOSCAN_TEST_NAME) {
                    bactoscanTotal += Decimal.valueOf(milkQuality.Test_Result__c);
                }
            }

            body += ' MP' + collection.Pick_Up_Code__c + '\n' +
                'Litres ' + collection.Volume_Ltrs__c + '\n' +
                'Temp ' + collection.Temperature__c + '\n' +
                'Fat ' + collection.Fat_Percent__c + '\n' +
                'Pro ' + collection.Prot_Percent__c + '\n' +
                'KgMS ' + collection.Total_KgMS__c + '\n' +
                'BMCC ' + BMCCTotal.intValue() + '\n' +
                (bactoscanTotal > 0 ? 'Bacto ' + bactoscanTotal.intValue() : '') + ' ';


            return body;
        }
    }

    public static String getFarmRegionCodeAndName(Account farm, String nickname) {
        String codeAndName = '';

        if (farm.Farm_Region__r != null && farm.Farm_Region__r.Name != null) {
            codeAndName = farm.Farm_Region__r.Name.substring(0, 1);
        }

        codeAndName += farm.Name;

        if (!String.isBlank(nickname)) {
            if (nickname.length() > 30) {
                nickname = nickname.substring(0, 30);
            }
            codeAndName += ' (' + nickname + ')';
        }

        return codeAndName;
    }

    public Channel_Preference__c getIndividualChannelPreferenceForDailyMilkResult(Id individualId) {
        Channel_Preference__c channelPreference = null;
        List<Channel_Preference__c> channelPreferences = [SELECT
                                                            DeliveryByEmail__c,
                                                            NotifyBySMS__c
                                                        FROM
                                                            Channel_Preference__c
                                                        WHERE
                                                            Contact_ID__c = :individualId
                                                        AND Channel_Preference_Setting__c = :AMSChannelPreferencesUtil.DAILY_MILK_RESULT_SETTING.Id];

        if (!channelPreferences.isEmpty()) {
            channelPreference = channelPreferences[0];
        }

        return channelPreference;
    }

    public void execute(Database.BatchableContext batchableContext, List<AggregateResult> farms) {
        System.assertEquals(farms.size(), 1);

        Id farmId = (Id) farms[0].get('Farm_ID__c');
        Account farm = [SELECT Id, Name FROM Account WHERE Id = :farmId];

        List<Collection__c> collections = [SELECT
                                                Id,
                                                Pick_Up_Date__c,
                                                Pick_Up_Code__c,
                                                Total_KgMS__c,
                                                Protein_to_Fat_Ratio__c,
                                                KgMS_Percent__c,
                                                Farm_ID__c,
                                                Farm_ID__r.Name,
                                                Farm_ID__r.Farm_Region__r.Name,
                                                Volume_Ltrs__c,
                                                Fat_Kgs__c,
                                                Prot_Kgs__c,
                                                Temperature__c,
                                                Fat_Percent__c,
                                                Prot_Percent__c,
                                                (SELECT Id,
                                                    Name,
                                                    Test_Result__c,
                                                    Result_Interpretation__c,
                                                    Test_Type_Long_Description__c
                                                FROM Milk_Quality__r)
                                            FROM
                                                Collection__c
                                            WHERE
                                                (Pick_Up_Date__c >= LAST_N_DAYS:3)
                                                AND Result_Sent__c = false
                                                AND Farm_ID__c = :farm.Id];

        List<Collection__c> collectionsToSendNotifications = new List<Collection__c>();

        // Daily milk results should be sent if there is a Cell Count record and Fat % and Protein % > 0
        for (Collection__c collection : collections) {
            Boolean hasCellCountRecord = false;
            if (!collection.Milk_Quality__r.isEmpty()) {
                for(Milk_Quality__c milkQuality : collection.Milk_Quality__r) {
                    if(milkQuality.Test_Type_Long_Description__c == GlobalUtility.COLLECTION_PERIOD_BMCC_TEST_NAME) {
                        hasCellCountRecord = true;
                        break;
                    }
                }
            }
            if (hasCellCountRecord && collection.Fat_Percent__c > 0 && collection.Prot_Percent__c > 0) {
                collection.Result_Sent__c = true;
                collectionsToSendNotifications.add(collection);
            }
        }

        if (!collectionsToSendNotifications.isEmpty()) {
            List<Individual_Relationship__c> individualRelationships = RelationshipAccessService.individualRelationshipsWithProductionAndQualityNotifications(new List<Id> { farmId });

            List<Task> emailTasksForInsert = new List<Task>();
            Set<Id> individualsToNotifySMS = new Set<Id>();
            Set<Id> individualsToNotifyEmail = new Set<Id>();
            Set<Id> individualsWithInvalidNumbers = new Set<Id>();
            Map<Id, Contact> individualsByContactId = new Map<Id, Contact>();

            Map<Id, Individual_Relationship__c> individualRelationshipByContactId = new Map<Id, Individual_Relationship__c>();

            for (Individual_Relationship__c individualRelationship : individualRelationships) {
                Contact individual = AMSContactService.getContact(individualRelationship.Individual__c);
                individualsByContactId.put(individual.Id, individual);
                Channel_Preference__c individualChannelPreference = getIndividualChannelPreferenceForDailyMilkResult(individual.Id);

                if (individualChannelPreference != null) {
                    if (individualChannelPreference.NotifyBySMS__c) {
                        if (String.isNotBlank(individualRelationship.Individual__r.MobilePhone) && AMSContactService.isValidMobile(individualRelationship.Individual__r.MobilePhone)){
                            individualsToNotifySMS.add(individual.Id);
                        } else {
                            individualsWithInvalidNumbers.add(individual.Id);
                        }
                    }
                    if (individualChannelPreference.DeliveryByEmail__c) {
                        individualsToNotifyEmail.add(individual.Id);
                    }
                }

                //TODO JOHN PLEASE CONFIRM IF DUPLICATES EXIST
                if (!individualRelationshipByContactId.containsKey(individualRelationship.Individual__c)) {
                    individualRelationshipByContactId.put(individualRelationship.Individual__c, individualRelationship);
                }
            }

            List<Task> smsTasksForInsert = new List<Task>();

            for (Collection__c collection : collectionsToSendNotifications) {
                for (Id contactId : individualRelationshipByContactId.keySet()) {
                    Contact individual = individualsByContactId.get(contactId);

                    DailyMilkResultFormatter formatter = new DailyMilkResultFormatter(collection, individualRelationshipByContactId.get(contactId));

                    if (individualsToNotifySMS.contains(contactId)) {
                        MarketingCloudUtil.MessageContactResponse mcResponse = MarketingCloudUtil.sendSMSRest(
                            new List<Contact> { individual },
                            AMSChannelPreferencesUtil.DAILY_MILK_RESULT_SETTING.Exact_Target_Message_Keyword__c,
                            true,
                            formatter.getSMSText(),
                            AMSChannelPreferencesUtil.DAILY_MILK_RESULT_SETTING.Exact_Target_Message_ID__c,
                            true,
                            new MarketingCloudUtil.BlackoutWindow(
                                AMSChannelPreferencesUtil.DAILY_MILK_RESULT_SETTING.Exact_Target_Time_Zone__c,
                                AMSChannelPreferencesUtil.DAILY_MILK_RESULT_SETTING.Exact_Target_Window_Start__c,
                                AMSChannelPreferencesUtil.DAILY_MILK_RESULT_SETTING.Exact_Target_Window_End__c
                            )
                        );

                        Task t = createSMSTask(individual, farm.Id, formatter.getSMSText());
                        smsTasksForInsert.add(t);

                        if (mcResponse.responseCode == 400 || mcResponse.tokenId == null) {
                            t.SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSFAILEDSEND;
                        } else {
                            t.SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSSENT;
                            t.SMS_Token__c = mcResponse.tokenId;
                        }

                        t.Status = 'Complete';
                    }
                    if (individualsToNotifyEmail.contains(contactId)) {
                        Task t = createEmailTask(individual, farm.Id, formatter.getEmailText(), formatter.getEmailSubject());
                        emailTasksForInsert.add(t);
                    }
                    if (individualsWithInvalidNumbers.contains(contactId)) {
                        Task t = createSMSTask(individual, farm.Id, 'Invalid mobile number. \n' + formatter.getSMSText());
                        smsTasksForInsert.add(t);
                        t.Status = 'Complete';
                    }
                }
            }

            //save any logs created during the send sms process
            Logger.saveLog();

            update (collectionsToSendNotifications);

            insert smsTasksForInsert;
            insert emailTasksForInsert;

            for (Task t : emailTasksForInsert) {
                emailTaskIds.add(t.Id);
            }
        }

    }

    private static Task createSMSTask(Contact individual, Id farmId, String msg) {
        Task task = createTask(individual, farmId, msg, SMS_DEVELOPER_NAME);
        task.Subject = AMSChannelPreferencesUtil.DAILY_MILK_RESULT_SETTING.Name;

        return task;
    }

    private static Task createEmailTask(Contact individual, Id farmId, String msg, String subject) {
        Task task = createTask(individual, farmId, msg, EMAIL_DEVELOPER_NAME);
        task.Subject = subject;

        return task;
    }

    private static Task createTask(Contact individual, Id farmId, String msg, String taskType) {
        Task task = new Task();
        task.Status = 'Scheduled';
        Task.Type = taskType;
        task.WhoId = individual.Id;
        task.WhatId = farmId;
        task.Description = msg;
        task.SMS_Sent_To__c = individual.MobilePhone;
        task.ActivityDate = Date.today();
        task.RecordTypeId = GlobalUtility.activityTaskSystemGeneratedTaskRecordTypeId;

        return task;
    }

    public void finish(Database.BatchableContext results) {
        if (emailTaskIds.size() > 0) {
            List<Task> tasks = [SELECT Id, Description, Subject, WhoId FROM Task WHERE Id In :emailTaskIds];
            Database.executeBatch(new ScheduleBatchSendEmail(tasks), 1);
        }
    }
}