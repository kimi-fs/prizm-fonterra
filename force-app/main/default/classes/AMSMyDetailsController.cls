/**
 * Description: Controller for AMSMyDetails
 * @author: Amelia (Trineo)
 * @date: August 2017
 */
public with sharing class AMSMyDetailsController {

    public static final String AUSTRALIA_NUMBER_PREFIX = '+61';

    public User user { get; set; }
    public Contact contact { get; set; }

    public Boolean showError { get; set; }
    public Boolean showEmailError { get; set; }
    public Boolean showMyDetailsError { get; set; }
    public Boolean showMyRewardsError { get; set; }
    public Boolean showMobileError { get; set; }
    public Boolean showAddressError { get; set; }

    public Boolean showSuccess { get; set; }
    public String message { get; set; }

    public Address__c postalAddress { get; set; }
    public Address__c physicalAddress { get; set; }

    public transient Blob document {get; set;}
    public transient String contentType {get; set;}
    public transient String filename {get; set;}

    public String addressesSame { get; set; }

    private static Map<String, String> messages;
    static {
        messages = AMSCommunityUtil.initialisePageMessages('AMSMyDetails');
    }

    public AMSMyDetailsController() {
        resetValidation();

        this.contact = AMSContactService.getContact();
        this.user = AMSUserService.getUserFromContactId(this.contact.Id);

        //set the uses addresses and check if they are the same or not
        setAddresses();
        this.addressesSame = getIsSameAddress() ? 'Yes' : 'No';
    }

    public Boolean getFirstTimeSetup() {
        return user != null ? user.First_Time_Setup_Complete__c != true : false;
    }

    public String getWelcomeName() {
        String name = AMSContactService.getWelcomeName(contact);
        return name.toUpperCase();
    }

    public String getFullName() {
        String name = '';
        name += String.isNotBlank(contact.FirstName) ? contact.FirstName + ' ' : '';
        name += String.isNotBlank(contact.MiddleName) ? contact.MiddleName + ' ' : '';
        name += String.isNotBlank(contact.LastName) ? contact.LastName + ' ' : '';
        name = name.removeEnd(' ');
        return name;
    }

    public String getUserPhoto() {
        return AMSUserPhotoService.getFullUserPhoto(user);
    }

    public void uploadUserPhoto() {
        try {
            AMSUserPhotoService.uploadUserPhoto(this.document, filename, this.contentType, user);
        } catch (Exception e) {
            this.message = ExceptionService.friendlyMessage(e);
            this.showError = false;
        } finally {
            this.document = null;
        }
    }

    public PageReference saveEmail() {
        resetValidation();

        if (AMSUserService.anotherUserHasEmail(contact.email)) {
            this.showEmailError = true;
            this.message = messages.get('email-used');
            return null;
        }

        try {
            // update the email on both the contact and the user - importaint because this is used to log on
            // this can possibly be removed in the future - hopefully there will be a trigger keeping these in sync or something
            updateContact();
            if (user != null) {
                this.user.email = contact.Email;
                AMSUserService.updateUser(user);
            }
            saveSuccess();
        } catch (Exception e) {
            //this.message = e.getMessage();
            this.message = ExceptionService.friendlyMessage(e);

            this.showEmailError = true;
            return null;
        }

        return null;
    }

    public PageReference saveContact() {
        resetValidation();

        if (String.isBlank(this.contact.Phone)) {
            this.showMyDetailsError = true;
            this.message = messages.get('missing-details').replace('{!fieldName}', 'Main Phone');
        } else if (!String.isBlank(this.contact.Woolworths_Rewards_Card_Nbr__c) && (!this.contact.Woolworths_Rewards_Card_Nbr__c.isNumeric() || this.contact.Woolworths_Rewards_Card_Nbr__c.length() > 20)) {
            this.showMyRewardsError = true;
            this.message = messages.get('invalid-number').replace('{!fieldName}', 'Woolworths Rewards Card');
        } else if (!String.isBlank(this.contact.Ritchies_IGA_Community_Benefits_Card_Nbr__c) && (!this.contact.Ritchies_IGA_Community_Benefits_Card_Nbr__c.isNumeric() || this.contact.Ritchies_IGA_Community_Benefits_Card_Nbr__c.length() > 20)) {
            this.showMyRewardsError = true;
            this.message = messages.get('invalid-number').replace('{!fieldName}', 'Ritchies IGA Community Benefits Card');
        } else {
            try {
                updateContact();
                saveSuccess();
                resetContact();
            } catch (Exception e) {
                if (e.getMessage().contains('[MobilePhone]')) {
                    this.message = messages.get('invalid-mobile');
                } else {
                    this.message = ExceptionService.friendlyMessage(e);
                }
                this.showMyDetailsError = true;
                return null;
            }
        }

        return null;
    }

    public PageReference resetContact() {
        this.contact = AMSContactService.getContact();
        return null;
    }

    public PageReference saveAddress() {
        resetValidation();
        this.postalAddress = trimAddress(this.postalAddress);
        this.physicalAddress = trimAddress(this.physicalAddress);

        //check if the use same address flag has been set and make the postal address the same as the physical
        if (addressesSame.equalsIgnoreCase('Yes')) {
            this.postalAddress.Street_Number__c = this.physicalAddress.Street_Number__c;
            this.postalAddress.Street_1__c = this.physicalAddress.Street_1__c;
            this.postalAddress.Street_2__c = this.physicalAddress.Street_2__c;
            this.postalAddress.Suburb__c = this.physicalAddress.Suburb__c;
            this.postalAddress.City__c = this.physicalAddress.City__c;
            this.postalAddress.State__c = this.physicalAddress.State__c;
            this.postalAddress.Postcode__c = this.physicalAddress.Postcode__c;
            this.postalAddress.Country_Code__c = this.physicalAddress.Country_Code__c;
        }

        if (String.isBlank(this.physicalAddress.Postcode__c)) {
            this.showAddressError = true;
            this.message = messages.get('missing-postcode').replace('{!RecordType}', 'Home');
            return null;
        }

        if (String.isBlank(this.postalAddress.Postcode__c)) {
            this.showAddressError = true;
            this.message = messages.get('missing-postcode').replace('{!RecordType}', 'Postal');
            return null;
        }

        try {
            if (!this.showAddressError) {
                AMSContactService.upsertAddress(this.postalAddress);
                AMSContactService.upsertAddress(this.physicalAddress);
                saveSuccess();
            }
            return null;
        } catch (Exception e) {
            this.showAddressError = true;
            this.message = ExceptionService.friendlyMessage(e);
            return null;
        }
    }

    public PageReference completeSetup() {
        resetValidation();

        try {
            user.First_Time_Setup_Complete__c = true;
            user.First_Time_Setup_Complete_DateTime__c = System.now();
            this.contact.FarmSourceONE_Setup_Complete__c = true;
            AMSUserService.updateUser(user);
            updateContact();
            AMSChannelPreferenceService.subscribeUsersToNotifications(this.contact.Id);

            return Page.AMSDashboard;
        } catch (Exception e) {
            this.message = e.getMessage();
            this.showError = true;
            return null;
        }
    }

    public PageReference verifyDetails() {
        resetValidation();

        try {
            updateContact();
            showSuccess = true;
            this.message = messages.get('confirm-details');
        } catch (Exception e) {
            this.showError = true;
            this.message = ExceptionService.friendlyMessage(e);
        }
        return null;
    }

    public static List<SelectOption> getAddressSameOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Yes', 'Yes'));
        options.add(new SelectOption('No', 'No'));
        return options;
    }

    public Boolean getIsSameAddress() {
        Boolean isSame = true;
        isSame = isSame && this.postalAddress.Street_Number__c == this.physicalAddress.Street_Number__c ? true : false;
        isSame = isSame && this.postalAddress.Street_1__c == this.physicalAddress.Street_1__c ? true : false;
        isSame = isSame && this.postalAddress.Street_2__c == this.physicalAddress.Street_2__c ? true : false;
        isSame = isSame && this.postalAddress.Suburb__c == this.physicalAddress.Suburb__c ? true : false;
        isSame = isSame && this.postalAddress.City__c == this.physicalAddress.City__c ? true : false;
        isSame = isSame && this.postalAddress.State__c == this.physicalAddress.State__c ? true : false;
        isSame = isSame && this.postalAddress.Country_Code__c == this.physicalAddress.Country_Code__c ? true : false;
        return isSame;
    }

    public PageReference changePassword() {
        return Page.AMSPasswordChange;
    }

    private String updateMobileFormat(String mobileEntered) {
        if (!String.isBlank(mobileEntered) && mobileEntered.startsWith('0')) {
            return AUSTRALIA_NUMBER_PREFIX + mobileEntered.removeStart('0');
        } else {
            // note that this number still has to pass the validation rule otherwise an error will be shown
            return mobileEntered;
        }
    }

    private void setAddresses() {
        if (contact.Addresses__r != null && !contact.Addresses__r.isEmpty()) {
            for (Address__c addr : contact.Addresses__r) {
                if (addr.RecordTypeId == GlobalUtility.postalAddressRecordTypeId && this.postalAddress == null) {
                    this.postalAddress = addr;
                } else if (addr.RecordTypeId == GlobalUtility.physicalAddressRecordTypeId && this.physicalAddress == null) {
                    this.physicalAddress = addr;
                }
            }
        }

        if (this.postalAddress == null) {
            this.postalAddress = new Address__c(Individual__c = contact.Id, RecordTypeId = GlobalUtility.postalAddressRecordTypeId, Active__c = true);
        }
        if (this.physicalAddress == null) {
            this.physicalAddress = new Address__c(Individual__c = contact.Id, RecordTypeId = GlobalUtility.physicalAddressRecordTypeId, Active__c = true);
        }
    }

    private Address__c trimAddress(Address__c address) {
        address.Street_Number__c = String.isNotBlank(address.Street_Number__c) ? address.Street_Number__c.trim() : address.Street_Number__c;
        address.Street_1__c = String.isNotBlank(address.Street_1__c) ? address.Street_1__c.trim() : address.Street_1__c;
        address.Street_2__c = String.isNotBlank(address.Street_2__c) ? address.Street_2__c.trim() : address.Street_2__c;
        address.Suburb__c = String.isNotBlank(address.Suburb__c) ? address.Suburb__c.trim() : address.Suburb__c;
        address.City__c = String.isNotBlank(address.City__c) ? address.City__c.trim() : address.City__c;
        address.Postcode__c = String.isNotBlank(address.Postcode__c) ? address.Postcode__c.trim() : address.Postcode__c;
        address.Country_Code__c = String.isNotBlank(address.Country_Code__c) ? address.Country_Code__c.trim() : address.Country_Code__c;
        return address;
    }

    private Boolean addressPopulated(Address__c address) {
        return String.isNotBlank(address.Street_Number__c) ||
               String.isNotBlank(address.Street_1__c) ||
               String.isNotBlank(address.Street_2__c) ||
               String.isNotBlank(address.Suburb__c) ||
               String.isNotBlank(address.City__c) ||
               String.isNotBlank(address.PO_Box__c) ||
               String.isNotBlank(address.Country_Code__c);
    }

    private void saveSuccess() {
        this.showSuccess = true;
        this.message = messages.get('save-success');
    }

    private void updateContact() {
        this.contact.MobilePhone = updateMobileFormat(this.contact.MobilePhone);
        this.contact.Verified_No_Email__c = String.isNotBlank(contact.Email) ? false : true;
        this.contact.Verified_No_Mobile__c = String.isNotBlank(contact.MobilePhone) ? false : true;
        this.contact.Verified_No_Postal_Address__c = String.isNotBlank(contact.MailingPostalCode) ? false : true;
        this.contact.Last_Validated_Date__c = Date.today();
        AMSContactService.updateContact(this.contact);
    }

    private void resetValidation() {
        this.message = '';
        this.showSuccess = false;
        this.showError = false;
        this.showEmailError = false;
        this.showMyDetailsError = false;
        this.showMobileError = false;
        this.showAddressError = false;
        this.showMyRewardsError = false;
    }

    public static String getWooliesRewardsTermsAndConditionsLink() {
        return AMSCommunityUtil.getWooliesRewardsTermsAndConditionsLink();
    }

    public static String getRitchiesRewardsTermsAndConditionsLink() {
        return AMSCommunityUtil.getRitchiesRewardsTermsAndConditionsLink();
    }
}