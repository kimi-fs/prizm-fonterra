/*------------------------------------------------------------
Author:			Sean Soriano
Company:		Davanti Consulting
Description:	Scheduler Class for BatchSetActiveAddress Batch Class
History
11/01/2015		Sean Soriano	Created
------------------------------------------------------------*/
global class ScheduleBatchSetActiveAddress implements Schedulable, Database.Batchable<SObject> {

    global static void schedule() {
        String expression = ScheduleService.getCRONExpression('ScheduleBatchSetActiveAddress');
        String jobName = ScheduleService.getCRONJobName('ScheduleBatchSetActiveAddress');
        if (String.isNotBlank(expression)) {
            final ScheduleBatchSetActiveAddress job = new ScheduleBatchSetActiveAddress();
            System.schedule(jobName, expression, job);
        }
    }

	global void execute(SchedulableContext sc) {
		ScheduleBatchSetActiveAddress bsa = new ScheduleBatchSetActiveAddress();
		database.executebatch(bsa);
    }

	String query = 'SELECT Id, Start_Date__c,End_Date__c , Active__c FROM Address__c';

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		Date dtToday = system.today();

   		List<Address__c> lstAddresssForUpdate = new List<Address__c>();

		for(sObject so : scope) {
			Address__c agr = (Address__c)so;

			//Check dates for Active Address
			if(dtToday >= agr.Start_Date__c && (dtToday <= agr.End_Date__c || agr.End_Date__c == null) && agr.Active__c == false) {
				agr.Active__c = true;
				lstAddresssForUpdate.add(agr);

			//Check dates for Inactive Adress
			} else if((dtToday < agr.Start_Date__c || dtToday > agr.End_Date__c) && agr.Active__c == true) {
				agr.Active__c = false;
				lstAddresssForUpdate.add(agr);
			}

		}

		system.debug('##Number of Address for Update: ' + lstAddresssForUpdate.size());

		if(!lstAddresssForUpdate.isEmpty()) {
			Database.SaveResult[] srList = Database.update(lstAddresssForUpdate);

			// Iterate through each returned result
			for (Database.SaveResult sr : srList) {
			    if (sr.isSuccess()) {
			        // Operation was successful, so get the ID of the record that was processed
			        System.debug('##Successfully updated Address. Address ID: ' + sr.getId());
			    }
			    else {
			        // Operation failed, so get all errors
			        for(Database.Error err : sr.getErrors()) {
			            System.debug('##The following error has occurred.');
			            System.debug(err.getStatusCode() + ': ' + err.getMessage());
			            System.debug('##Address fields that affected this error: ' + err.getFields());
			        }
			    }
			}
		}


	}

	global void finish(Database.BatchableContext BC) {

	}

}