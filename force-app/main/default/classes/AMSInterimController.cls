/**
 * AMSProdQualSummaryController
 *
 * Controller for Interim page
 *
 * Author: NL (Trineo)
 * Date: 2017-08-08
 **/
public without sharing class AMSInterimController extends AMSProdQualReport {

    public AMSProdQualReport baseController { get { return this; } private set; }

    public final static Integer PRESETDURATION = 10;

    private static final String DEFAULT_REPORT_TYPE = 'Interim Production';

    public List<AMSCollectionService.CollectionPeriodForm> collectionPeriodFormList { get; set; }

    public AMSInterimController() {
        selectedReportTypeName = DEFAULT_REPORT_TYPE;
    }

    //Call recalculate for the forms
    public override void recalculateForm() {
        //all associated farm seasons for selected farm (first farm in list)
        collectionPeriodFormList = new List<AMSCollectionService.CollectionPeriodForm>();
        populateCollectionFormList();
    }

    //Callout to CollectionService class with details required for 10 day breakdown
    //Iterates through from toDate to fromDate making a callout for every 10 day chunk
    public void populateCollectionFormList() {
        Integer dateDifference = fromDate.daysBetween(toDate);
        Date runningFromDate = fromDate;
        Date runningToDate = runningFromDate;
        Date runningEndOfMonth = AMSCollectionService.returnEndOfMonth(runningToDate);
        while (runningToDate <= toDate) {
            if (runningToDate.Day() == 10 || runningToDate.Day() == 20 || runningToDate == runningEndOfMonth || runningToDate == toDate) {
                AMSCollectionService.CollectionPeriodForm interimCollectionPeriodForm = new AMSCollectionService.CollectionPeriodForm(
                    runningFromDate,
                    runningToDate,
                    null,
                    null,
                    null,
                    selectedFarmSeason,
                    selectedFarmId,
                    AMSCollectionService.DAY);
                interimCollectionPeriodForm.convertDateMapToListAscending(false);
                collectionPeriodFormList.add(interimCollectionPeriodForm);
                runningFromDate = runningToDate.addDays(1);
                runningToDate = runningFromDate;
                runningEndOfMonth = AMSCollectionService.returnEndOfMonth(runningToDate);
            } else {
                runningToDate = runningToDate.addDays(1);
            }
        }
    }

    //Query individual relationships
    private static List<Individual_Relationship__c> queryRelatedRelationships(Id individualId) {
        List<Individual_Relationship__c> individualRelationships = new List<Individual_Relationship__c>();
        individualRelationships = [
            SELECT Id,
                Farm__c,
                Farm__r.Name
            FROM Individual_Relationship__c
            WHERE Individual__c = :individualId
            AND Farm__c != ''
            AND Active__c = true
            ORDER BY CreatedDate DESC
        ];
        return individualRelationships;
    }
}