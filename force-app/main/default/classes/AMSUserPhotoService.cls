/**
 * Description: Service for accessing users photos
 * @author: Amelia (Trineo)
 * @date: July 2017
 * @test: AMSUserPhotoService_Test
 */
public with sharing class AMSUserPhotoService {

    public static final String SF_DEFAULT_PROFILE_PHOTO = '/profilephoto/005/';
    public static final String AMS_DEFAULT_PROFILE_PHOTO = '/resource/AMSCommunityResource/files/images/profile-image.svg';

    public static String getFullUserPhoto(User user) {
        if (user == null ) {
            return getDefaultAMSProfileImage();
        } else {
            ConnectApi.Photo photo = ConnectApi.UserProfiles.getPhoto(Network.getNetworkId(), user.Id);
            return isSFDefaultImage(photo.largePhotoUrl) ? getDefaultAMSProfileImage() : photo.largePhotoUrl;
        }
    }

    public static String getSmallUserPhoto(User user) {
        if (user == null ) {
            return getDefaultAMSProfileImage();
        } else {
            ConnectApi.Photo photo = ConnectApi.UserProfiles.getPhoto(Network.getNetworkId(), user.Id);
            return isSFDefaultImage(photo.SmallPhotoUrl) ? getDefaultAMSProfileImage() : photo.SmallPhotoUrl;
        }
    }

    /**
    * Checks if the users profile photo is the SF default profile image
    */
    private static Boolean isSFDefaultImage(String photoUrl) {
        return photoUrl.contains(SF_DEFAULT_PROFILE_PHOTO);
    }

    /**
    * Gets the path for the default AMS Profile photo
    * Need to use Site.getBaseUrl() as the community has a custom domain
    */
    private static String getDefaultAMSProfileImage() {
        String defaultImage = Site.getBaseUrl() + AMS_DEFAULT_PROFILE_PHOTO;
        return defaultImage;
    }

    public static void uploadUserPhoto(Blob document, String filename, String contentType, User user) {
        ConnectApi.BinaryInput fileUpload = new ConnectApi.BinaryInput(document, contentType, filename);
        ConnectApi.Photo photoProfile = ConnectApi.UserProfiles.setPhoto(Network.getNetworkId(), user.Id, fileUpload);
        if (user.UserPreferencesShowProfilePicToGuestUsers == false) {
            User makePhotoExernal = new User();
            makePhotoExernal.Id = user.Id;
            makePhotoExernal.UserPreferencesShowProfilePicToGuestUsers = true;
            update makePhotoExernal;
        }
    }
}