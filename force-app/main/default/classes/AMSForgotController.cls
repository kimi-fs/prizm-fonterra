/**
 * Description: Resets the password of the user with the inputted email address
 * @author: Amelia (Trineo)
 * @date: August 2017
 */
public with sharing class AMSForgotController {
    public String email {get; set;}
    public String message {get;set;}

    public Boolean emailSent {get;set;}

    private static Map<String, String> messages;
    static {
        messages = ExceptionService.getPageMessages('AMSForgot');
    }

    public AMSForgotController() {
        this.email = '';
        this.message = '';
        this.emailSent = false;
    }

     public void resetPassword() {
        this.message = '';

        if (String.isBlank(this.email)) {
            this.message = messages.get('missing-email');
        } else {
            User user = AMSUserService.getUserFromEmailAddress(this.email);
            if(user != null && user.Username != null){
                Boolean success = false;

                try {
                    success = Site.forgotPassword(user.Username);
                    if (success) {
                        this.emailSent = true;
                    }
                } catch (Exception e) {
                    this.message = messages.get('unknown-error' + e.getMessage());
                }
                if (!success) {
                    this.message = messages.get('unknown-error');
                } 
                
            } else {
                this.message = messages.get('unknown-user');
            }   
        }
     }

    public String getUserEmailDomain(){
        return AMSUserService.getEmailDomain(this.email);
    }

    public static PageReference redirectToLogin(){
        return Page.AMSLogin;
    }
}