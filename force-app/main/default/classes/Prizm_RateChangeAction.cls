public class Prizm_RateChangeAction {

    @InvocableMethod(Label = 'Rate Action')
	public static void executeRateChangeJob(List<fsCore__Index_Rate_Setup__c> pIndexRateList) {
    
     boolean runJob = false;
     if(pIndexRateList.size()>0)
     {
         for(fsCore__Index_Rate_Setup__c indexRate : pIndexRateList)
         {
             if( indexRate.Is_Approved__c == true && indexRate.fsCore__Is_Active__c == true)
             {
                runJob = true;
             }
         }
     }
        
     if(runJob = true)
     {
         Id batchJobId = Database.executeBatch(new Prizm_RateChangeJob(), 200);
      
        /*fsCore.BatchJobObject bJobObj =  fsCore.BatchJobUtil.getBatchJobRecord('Prizm_RateChangeJob');   
         Id jobStatus =  fsCore.BatchJobUtil.submitBatchJob(bJobObj);*/
         System.debug(loggingLevel.ERROR, 'Batch Job Status : '+batchJobId);
     }

	}
}