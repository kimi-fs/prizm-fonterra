/**
 * Description: Service Class for Content* related objects Including Custom Folders and links.
 *              The Folder corresponds to a library (ContentWorkspace) and the Content Document link connects a
 *              document to the folder much like the ContentWorkspaceDoc links Document to a library.
 *
 * @author: Clayde (Trineo)
 * @date: Mar 2019
 * @test: ContentManagerControllerTest
 */

public without sharing class ContentDocumentService {

    /**
     * Called in ContentDocumentLinkTriggerHandler to validate files from Classic
     */
    public static void showValidationErrorForFarmAUAgreementFiles(String errorMessage, Set<Id> contentDocumentSet, List<ContentDocumentLink> contentDocumentLinkList, SObjectField field, String strValue) {
        showValidationErrorForFarmAUAgreementFiles(errorMessage, null, contentDocumentSet, contentDocumentLinkList, field, strValue);
    }

    /**
     * Called in ContentDocumentTriggerHandler to validate files from Lightning Experience
     */
    public static void showValidationErrorForFarmAUAgreementFiles(String errorMessage, Map<Id, Object> contentDocumentMap, SObjectField field, String strValue) {
        showValidationErrorForFarmAUAgreementFiles(errorMessage, contentDocumentMap, null, null, field, strValue);
    }

    /**
     * Method to process and show error message dynamically. Users with Modify_Executed_Agreement_File will bypass this error message.
     */
    public static void showValidationErrorForFarmAUAgreementFiles(
        String errorMessage,
        Map<Id, Object> contentDocumentMap,
        Set<Id> contentDocumentSet,
        List<ContentDocumentLink> contentDocumentLinkList,
        SObjectField field,
        String strValue) {

        Map<Id, Id> agreementAndContentDocumentMap;
        if (contentDocumentMap != null) {
           agreementAndContentDocumentMap = getMapAgreementAndContentDocument(contentDocumentMap);
        }

        Set<Id> contentDocumentIds = new Set<Id>(agreementAndContentDocumentMap != null ? (Set<Id>) agreementAndContentDocumentMap.keySet() : contentDocumentSet);
        List<Agreement__c> agreementListFromContentDocumentLink = [SELECT Id, Active__c, Agreement_Status__c FROM Agreement__c WHERE Id IN :contentDocumentIds AND Farm__r.RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId];

        for (Agreement__c agreement : agreementListFromContentDocumentLink) {
            Object agreementValue = agreement.get(field);
            if (String.valueOf(agreementValue) != strValue) {
                return;
            } else if (!FeatureManagement.checkPermission('Modify_Executed_Agreement_File')) {
                processAgreementFileErrorMessage(errorMessage, agreement, contentDocumentMap, agreementAndContentDocumentMap, contentDocumentLinkList);
            }
        }
    }

    public static void processAgreementFileErrorMessage(String errorMessage, Agreement__c agreement, Map<Id, Object> contentDocumentMap, Map<Id, Id> agreementAndContentDocumentMap, List<ContentDocumentLink> contentDocumentLinkList) {
        if (contentDocumentMap != null) {
            ContentDocument contentDocument = (ContentDocument) contentDocumentMap.get(agreementAndContentDocumentMap.get(agreement.Id));
            contentDocument.addError(errorMessage);
        } else if (!contentDocumentLinkList.isEmpty()) {
            for (ContentDocumentLink cl : contentDocumentLinkList) {
                if(cl.LinkedEntityId == agreement.Id) {
                    System.debug('Marking record errored: ' + errorMessage);
                    cl.addError(errorMessage);
                }
           }
        }
    }

    /**
     * Method to get the agreement record
     */
    public static Map<Id, Id> getMapAgreementAndContentDocument(Map<Id, Object> contentDocumentMap) {
        Map<Id, Id> agreementAndContentDocumentMap = new Map<Id, Id>();

        List<ContentDocumentLink> contentDocLinkList = [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE ContentDocumentId IN :contentDocumentMap.keySet()];
        for (ContentDocumentLink contentDocLink : contentDocLinkList) {
            if (Agreement__c.SObjectType == contentDocLink.LinkedEntityId.getSObjectType()) {
                agreementAndContentDocumentMap.put(contentDocLink.LinkedEntityId, contentDocLink.ContentDocumentId);
            }
        }
        return agreementAndContentDocumentMap;
    }

}