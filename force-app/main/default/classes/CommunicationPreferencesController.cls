public with sharing class CommunicationPreferencesController {
    // Page page required variables
    public Contact individual { get; set; }
    public ApexPages.StandardController standardController { get; set; }

    public static final String PERSONAL_CHANNEL_PREFERENCE_SETTING_TYPE = 'Personal';
    public static final String FARM_CHANNEL_PREFERENCE_SETTING_TYPE = 'Farm';

    public static final String UPDATE_SUCCESS_MESSAGE = 'Preferences successfully updated.';
    public static final String MISSING_EMAIL_MESSAGE = 'Individuals can only subscribe to Email notifications if they have an email address.';
    public static final String MISSING_SMS_MESSAGE = 'Individuals can only subscribe to SMS notifications if they have an mobile phone number.';

    /* MY PREFERENCES */
    public List<Channel_Preference__c> myPreferencesChannelPreferences { get; set; }
    public List<PreferenceHistoryService> myPreferencesChannelPreferenceHistory { get; set; }

    /* MY FARM PREFERENCES */
    public List<PreferenceHistoryService> channelPreferenceHistory { get; set; }
    public List<PreferenceHistoryService> myFarmPreferenceHistory { get; set; }

    public List<PreferenceService.IndividualPreferenceWrapper> individualPreferenceWrappers { get; set; }

    public User currentUser { get; set; }
    public Communication_Settings__c communicationSettings;

    public Boolean australianIndividual { get; set; }

    public CommunicationPreferencesController(ApexPages.StandardController standardController) {
        this.currentUser = [SELECT Id, Profile.Name FROM User WHERE Id = : UserInfo.getUserId()];
        this.communicationSettings = Communication_Settings__c.getOrgDefaults();

        this.standardController = standardController;
        this.individual = [SELECT Id, Name, FirstName, LastName, MobilePhone, Email, RecordTypeId, RecordType.DeveloperName FROM Contact WHERE Id = : standardController.getRecord().Id];
        this.australianIndividual = individual.RecordType.DeveloperName == 'Individual_AU';

        initialiseMyPreferencesTab();
        initialiseFarmPreferencesTab();
    }

    public void initialiseMyPreferencesTab() {
        this.myPreferencesChannelPreferences = getMyPreferencesChannelPreferences(individual.Id);

        // Set My Personal Preference history
        this.generateMyPersonalPreferenceHistory();
    }

    public void initialiseFarmPreferencesTab() {
        this.individualPreferenceWrappers = generateIndividualPreferenceWrappers(individual.Id);

        // Set my Farm Preference history
        this.generateMyFarmPreferenceHistory();
    }

    private List<Channel_Preference__c> getMyPreferencesChannelPreferences(Id contactId) {
        List<Channel_Preference__c> returnList = new List<Channel_Preference__c>();
        Map<Id, Channel_Preference__c> mapChannelPreferenceById = PreferenceService.getChannelPreferenceMap(contactId, true, PERSONAL_CHANNEL_PREFERENCE_SETTING_TYPE);

        return mapChannelPreferenceById.values(); //no ordering
    }

    private List<PreferenceService.IndividualPreferenceWrapper> generateIndividualPreferenceWrappers(Id contactId) {
        Map<Id, Channel_Preference_Setting__c> mapChannelPreferenceSettingById = PreferenceService.getChannelPreferenceSettingMap(FARM_CHANNEL_PREFERENCE_SETTING_TYPE);
        Map<Id, Channel_Preference__c> mapChannelPreferenceById = PreferenceService.getChannelPreferenceMap(contactId, true, FARM_CHANNEL_PREFERENCE_SETTING_TYPE);

        if (mapChannelPreferenceById.keySet().size() == 0) {
            return null;
        }

        List<PreferenceService.IndividualPreferenceWrapper> individualPreferenceWrappers = new List<PreferenceService.IndividualPreferenceWrapper>();

        for (Channel_Preference__c cp : mapChannelPreferenceById.values()) {
            individualPreferenceWrappers.add(new PreferenceService.IndividualPreferenceWrapper(cp, mapChannelPreferenceSettingById.get(cp.Channel_Preference_Setting__c)));
        }

        return individualPreferenceWrappers;
    }

    public void saveMyPreferences() {
        Boolean noErrorOnPage = true;

        if (noErrorOnPage) {
            update individual;

            if (myPreferencesChannelPreferences != null) {
                update myPreferencesChannelPreferences;
            }
        }
    }

    public void saveFarmPreferences() {
        List<Channel_Preference__c> channelPreferencesForUpdate = new List<Channel_Preference__c>();

        if (individualPreferenceWrappers != null) {
            for (PreferenceService.IndividualPreferenceWrapper ipw : individualPreferenceWrappers) {
                Channel_Preference__c forUpdate = new Channel_Preference__c();

                forUpdate.Id = ipw.channelPreference.Id;

                forUpdate.DeliveryMail__c = ipw.channelPreference.DeliveryMail__c;
                forUpdate.DeliveryByEmail__c = ipw.channelPreference.DeliveryByEmail__c;
                forUpdate.NotifyBySMS__c = ipw.channelPreference.NotifyBySMS__c;

                channelPreferencesForUpdate.add(forUpdate);
            }
        }

        if (channelPreferencesForUpdate != null) {
            update channelPreferencesForUpdate;
        }
    }

    public PageReference linkToPersonalHistory() {
        return new PageReference('/apex/PreferenceHistory?scontrolCaching=1&cid=' + individual.Id + '&scope=personal');
    }

    public PageReference linkToFarmHistory() {
        return new PageReference('/apex/PreferenceHistory?scontrolCaching=1&cid=' + individual.Id + '&scope=farm');
    }

    public void saveAllTabs() {
        Boolean error = false;
        if (australianIndividual) {
            error = addErrorIfSubscribingWithoutValidData();
        }
        if (!error) {
            try {
                saveMyPreferences();
                saveFarmPreferences();
                if (!(ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.FATAL))) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, UPDATE_SUCCESS_MESSAGE));
                }
            } catch (Exception e) {
                ApexPages.addMessages(e);
            }
        }
    }

    public PageReference saveAndRefresh() {
        saveAllTabs();

        // Set My Personal Preference History
        this.generateMyPersonalPreferenceHistory();

        // Set My Farm Preference History
        this.generateMyFarmPreferenceHistory();

        return null;
    }

    public PageReference saveAndReturnToContact() {
        saveAllTabs();
        if (ApexPages.hasMessages(ApexPages.Severity.ERROR) || ApexPages.hasMessages(ApexPages.Severity.FATAL) || ApexPages.hasMessages(ApexPages.Severity.WARNING)) {
            return null;
        } else {
            return standardController.view();
        }
    }

    private static List<PreferenceHistoryService> limitHistory(List<PreferenceHistoryService> histories, Integer maxCount) {
        List<PreferenceHistoryService> limitedHistories = new List<PreferenceHistoryService>();

        for (PreferenceHistoryService history : histories) {
            if (limitedHistories.size() >= 5) {
                break;
            }

            limitedHistories.add(history);
        }

        return limitedHistories;
    }

    /**
     * Adds an error to the page if trying to subscribe to:
     * - Emails notifications without a valid email address on the contact record
     * - SMS notifications without a valid mobile phone number on the contact record
     */
    private Boolean addErrorIfSubscribingWithoutValidData() {
        Boolean error = false;
        if (String.isBlank(individual.Email)) {
            for (PreferenceService.IndividualPreferenceWrapper ipw : individualPreferenceWrappers) {
                if (ipw.channelPreference.DeliveryByEmail__c) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, MISSING_EMAIL_MESSAGE));
                    error = true;
                    break;
                }
            }
        }
        if (String.isBlank(individual.MobilePhone)) {
            for (PreferenceService.IndividualPreferenceWrapper ipw : individualPreferenceWrappers) {
                if (ipw.channelPreference.NotifyBySMS__c) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, MISSING_SMS_MESSAGE));
                    error = true;
                    break;
                }
            }
        }

        return error;
    }

    private void generateMyPersonalPreferenceHistory() {
        this.myPreferencesChannelPreferenceHistory = new List<PreferenceHistoryService>();
        this.myPreferencesChannelPreferenceHistory = PreferenceHistoryService.getMyPreferenceHistory(this.myPreferencesChannelPreferences);
        this.myPreferencesChannelPreferenceHistory.addAll(PreferenceHistoryService.getIndividualHistory(this.individual));
        this.myPreferencesChannelPreferenceHistory.sort();

        this.myPreferencesChannelPreferenceHistory = limitHistory(this.myPreferencesChannelPreferenceHistory, 5);
    }

    private void generateMyFarmPreferenceHistory() {
        this.channelPreferenceHistory = PreferenceHistoryService.getChannelPreferenceHistory(this.individualPreferenceWrappers);
        this.myFarmPreferenceHistory = channelPreferenceHistory;
        this.myFarmPreferenceHistory.sort();

        this.myFarmPreferenceHistory = limitHistory(this.myFarmPreferenceHistory, 5);
    }
}