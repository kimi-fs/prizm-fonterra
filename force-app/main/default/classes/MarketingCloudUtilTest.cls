@IsTest
private class MarketingCloudUtilTest {

    @IsTest
    private static void testSendSMS() {
        Contact individual = new Contact(LastName = 'Smith', FirstName = 'Tim', MobilePhone = '+61406133552', Phone = '+61406133552', Type__c = 'Personal');

        insert individual;

        Communication_Settings__c settings = Communication_Settings__c.getOrgDefaults();
        settings.SMS_Sending_Enabled__c = true;
        settings.SMS_Safe_Number_List__c = '+61406133552';
        upsert (settings);

        Test.setMock(HttpCalloutMock.class, new MCHttpMock());

        Test.startTest();

        MarketingCloudUtil.sendSMSRest(new List<Contact> { individual }, 'TEST', true, 'TEST MESSAGE', 'MESSAGEID');

        Test.stopTest();
    }

    @IsTest
    private static void testBlackoutWindowTimeZone() {
        MarketingCloudUtil.BlackoutWindow blackoutWindow = new MarketingCloudUtil.BlackoutWindow(UserInfo.getTimeZone().toString(), null, null);
        System.assertEquals(blackoutWindow.UtcOffset, DateTime.now().format('Z'));
    }

    public class MCHttpMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setBody('{}');
            return response;
        }
    }

}