/**
 * Description: Test class for MilkQualitySummaryController
 * @author: Eric (Trineo)
 * @date: Sept 2018
 **/

@isTest
private class MilkQualitySummaryControllerTest {
    @TestSetup
    static void testSetup() {
        Account farm = TestClassDataUtil.createAUFarms(1, true)[0];
        Collection__c collection = TestClassDataUtil.createSingleCollection(farm, false);
        collection.Pick_Up_Date__c = Date.today();
        collection.Volume_Ltrs__c = 1000;
        collection.Fat_Percent__c = 22.2;
        collection.Prot_Percent__c = 11.1;
        insert collection;

        // milk qualities
        List<Milk_Quality__c> milkQualityList = new List<Milk_Quality__c>();

        Milk_Quality__c milkQuality_Temperature = TestClassDataUtil.createMilkQuality(collection, false);
        milkQuality_Temperature.Name = GlobalUtility.TEMPERATURE_TEST_NAME;
        milkQuality_Temperature.Test_Result__c = 'OK';
        milkQualityList.add(milkQuality_Temperature);

        Milk_Quality__c milkQuality_CellCount = TestClassDataUtil.createMilkQuality(collection, false);
        milkQuality_CellCount.Name = GlobalUtility.CELL_COUNT_TEST_NAME;
        milkQuality_CellCount.Test_Result__c = 'OK';
        milkQualityList.add(milkQuality_CellCount);

        Milk_Quality__c milkQuality_Bactoscan = TestClassDataUtil.createMilkQuality(collection, false);
        milkQuality_Bactoscan.Name = GlobalUtility.BACTOSCAN_TEST_NAME;
        milkQuality_Bactoscan.Test_Result__c = 'OK';
        milkQuality_Bactoscan.Result_Interpretation__c = 'Alert';
        milkQualityList.add(milkQuality_Bactoscan);

        Milk_Quality__c milkQuality_Thermodurics = TestClassDataUtil.createMilkQuality(collection, false);
        milkQuality_Thermodurics.Name = GlobalUtility.THERMODURICS_TEST_NAME;
        milkQuality_Thermodurics.Test_Result__c = '1';
        milkQualityList.add(milkQuality_Thermodurics);

        Milk_Quality__c milkQuality_Sediment = TestClassDataUtil.createMilkQuality(collection, false);
        milkQuality_Sediment.Name = GlobalUtility.SEDIMENT_TEST_NAME;
        milkQuality_Sediment.Test_Result__c = 'OK';
        milkQuality_Sediment.Result_Type__c = 'S';
        milkQualityList.add(milkQuality_Sediment);

        Milk_Quality__c milkQuality_Antibiotics = TestClassDataUtil.createMilkQuality(collection, false);
        milkQuality_Antibiotics.Name = GlobalUtility.ANTIBIOTICS_TEST_NAME;
        milkQuality_Antibiotics.Test_Result__c = 'OK';
        milkQuality_Antibiotics.Result_Type__c = 'A';
        milkQualityList.add(milkQuality_Antibiotics);

        insert milkQualityList;
    }

    @isTest static void testCollectionSummary() {
        Account farm = [SELECT Id FROM Account LIMIT 1];
        Collection__c collection = [SELECT Id FROM Collection__c LIMIT 1];

        PageReference page = new PageReference('/apex/MilkQualitySummary?id=' + farm.Id);
        System.Test.setCurrentPage(page);
        MilkQualitySummaryController controller = new MilkQualitySummaryController();
        List<MilkQualitySummaryController.CollectionWrapper> collections = controller.getCollectionWrappers();
        System.assertEquals(collections.size(), 1);

        MilkQualitySummaryController.CollectionWrapper wrapper = collections[0];
        System.assertEquals(wrapper.temperature, 'OK');
        System.assertEquals(wrapper.cellCount, 'OK');
        System.assertEquals(wrapper.bactoscan, 'OK');
        System.assertEquals(wrapper.thermodurics, '<100');
        System.assertEquals(wrapper.sediment, 'S');
        System.assertEquals(wrapper.antibiotics, 'A');
        System.assertEquals(wrapper.volume, 1000);
        System.assertEquals(wrapper.collection.Fat_Percent__c, 22.2);
        System.assertEquals(wrapper.collection.Prot_Percent__c, 11.1);
        System.assertEquals(wrapper.alertBactoscan, true);
        System.assertEquals(wrapper.alertThermodurics, false);
    }
}