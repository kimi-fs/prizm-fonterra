/**
 * Description: Test class for AMSCaseService
 * @author: Amelia (Trineo)
 * @date: September 2017
 */
@isTest
private class AMSCaseServiceTest {

    @isTest static void testInsertCase() {
    	String CASE_DESCRIPTION = 'Description test';
    	Case c = new Case();
    	c.Description = CASE_DESCRIPTION;

    	Case insertedCase = AMSCaseService.insertCase(c);

    	System.assertNotEquals(null, insertedCase.Id, 'Case not inserted');

    }
}