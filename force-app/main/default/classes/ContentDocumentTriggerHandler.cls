/**
 * Description: Trigger handler for ContentDocument. The delete event of this trigger is only being fired when a user is in Lightning
 * @author: Irish (Trineo)
 * @date: May 2020
 * @test: ContentDocumentTriggerHandlerTest
 */

public with sharing class ContentDocumentTriggerHandler extends TriggerHandler{

    public override void beforeUpdate() {
        validateEditAccessOfAgreementFiles(Trigger.newMap);

        List<ContentDocumentLink> matchingLinks = [
            SELECT Id,
                   LinkedEntityId,
                   LinkedEntity.Name,
                   LinkedEntity.Type
            FROM ContentDocumentLink
            WHERE ContentDocumentId IN :Trigger.newMap.keySet()
        ];

        AgreementService.updateAgreementsFromDocumentLinks(matchingLinks);
        AgreementService.sendEmailAndCreateCaseForSignedAgreement(matchingLinks);
    }

    public override void beforeDelete() {
        validateDeleteAccessOfAgreementFiles(Trigger.oldMap);
    }

    // This method will prevent users in uploading a new version of an executed agreement file.
    // Only users with Modify Executed Agreement File can upload a new versionS.
    public void validateEditAccessOfAgreementFiles(Map<Id, Object> newContentDocumentMap) {
        Boolean isNotFirstVersion = false;
        for (ContentVersion cv : [SELECT ContentDocumentId, VersionNumber FROM ContentVersion WHERE ContentDocumentId IN :newContentDocumentMap.keySet()]) {
            if (cv.VersionNumber != '1') {
                isNotFirstVersion = true;
            }
        }
        if (isNotFirstVersion) {
            ContentDocumentService.showValidationErrorForFarmAUAgreementFiles('You are not allowed to edit this document. ', newContentDocumentMap, Agreement__c.Agreement_Status__c, 'Executed');
        }
    }

    public void validateDeleteAccessOfAgreementFiles(Map<Id, Object> oldContentDocumentMap) {
        ContentDocumentService.showValidationErrorForFarmAUAgreementFiles('You are not allowed to delete this document. ', oldContentDocumentMap, Agreement__c.Agreement_Status__c, 'Executed');
    }
}