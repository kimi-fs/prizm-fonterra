@isTest
private class IndividualEmailResultTest {

    static testMethod void insertIER(){
        Contact c = TestClassDataUtil.createIndividualAU(true);

        et4ae5__Automated_Send__c automatedSend = new et4ae5__Automated_Send__c();
        insert automatedSend;
        et4ae5__IndividualEmailResult__c ier = new et4ae5__IndividualEmailResult__c(
            et4ae5__TriggeredSendDefinition__c = automatedSend.Id,
            et4ae5__Contact__c = c.Id
        );
        insert ier;

        et4ae5__IndividualEmailResult__c IER2 = IER.clone(false,true);
        IER2.et4ae5__MergeId__c += 'x';
        insert IER2;
    }
}