@isTest
public class Prizm_LendingAppConversionRuleTest {

    @testSetup
    public static void create() {
        
        Prizm_TestDataHelper.createPermssions();
        
        fscore__Company_Setup__c testCompany = fsCore.TestHelperCompany.getTestCompanySetup(
            'Test Company',
            'TESTCO',
            '123456789'
        );
        insert testCompany;
        
        fsCore__Branch_Setup__c testBranch = fsCore.TestHelperCompany.getTestBranchSetup(
            'Test Branch',
            'TBRCH',
            testCompany.Id,
            null,
            null
        );
        insert testBranch;
        
        fsCore__Product_Setup__c testProduct = fsCore.TestHelperProduct.getTestProductSetup(
            'Test Product',
            'TP',
            fsCore.Constants.PRODUCT_FAMILY_LOAN,
            fsCore.Constants.CYCLE_MONTHLY,
            1
        );
        insert testProduct;
        
        fsCore__Payment_Allocation_Method_Setup__c testPaymentAlloc = fsCore.TestHelperFinancial.getTestPaymentAllocMethod(
            'Test Payment Allocation'
        );
        insert testPaymentAlloc;
        
        fsCore__Contract_Template_Setup__c testContractTemplate = fsCore.TestHelperProduct.getTestContractTemplateSetup(
            'Test Contract Template',
            fsCore.Constants.PRODUCT_FAMILY_LOAN,
            testPaymentAlloc.Id,
            1
        );
        insert testContractTemplate;
        
        Account farmAccount = Prizm_TestDataHelper.farmAccount();
        
        Database.insert(farmAccount);
        
        Contact farmContact = Prizm_TestDataHelper.createFarmIndividual(farmAccount);
        insert farmContact;
        System.assert(farmContact.id!=null, 'Test Contact created assert'); 

        Account testAccount = [
            SELECT id
            FROM Account
            WHERE Id = :farmAccount.id
        ];
        
        fsCore__Lending_Application__c testApplication = fsCore.TestHelperLendingApplication.getTestApplicationWithContract(
            testCompany,
            testBranch,
            testProduct,
            testContractTemplate,
            'Test Application'
        );
        insert testApplication;
        
        fsCore__Lending_Application_Customer__c testAppCustomer1 = fsCore.TestHelperLendingApplication.getTestCustomer(
            testApplication,
            testAccount.Id,
            farmContact.Id,
            'Primary'
        );
        insert testAppCustomer1;

    }
    
    @isTest
    public static void validateDecisionReasonDTI_Success() {
        List<fsCore__Lending_Application__c> applicationList = [SELECT id, Name FROM fsCore__Lending_Application__c];

        fsCore__Rule_Setup__c rule = new fsCore__Rule_Setup__c();
        
        Test.startTest();
        Prizm_LendingAppConversionRule ruleData = new Prizm_LendingAppConversionRule();
        ruleData.setRule(rule);
        ruleData.evaluate(applicationList);
        ruleData.getResults();
        fsCore.RuleResultObject ruleResultObj = ruleData.getResult(applicationList[0].id);
        Test.stopTest();
        
        System.assertEquals(
			'The Current User have Permission to convert Lending Application to Lending Contract',
			ruleResultObj.getResultMessage(),
			'Lending Application Conversion Validation assert failed'
		);

    }
}