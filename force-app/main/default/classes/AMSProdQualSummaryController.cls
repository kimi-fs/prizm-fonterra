/**
 * Created by Nick on 07/08/17.
 *
 * AMSProdQualSummaryController
 *
 * Controller for ProdQualSummary page
 *
 * Author: NL (Trineo)
 * Date: 2017-08-07
 **/
public without sharing class AMSProdQualSummaryController extends AMSProdQualReport {

    public AMSProdQualReport baseController { get { return this; } private set; }

    public final Date TODAYYEARPRIOR = System.today().addYears(-1);

    private static final String DEFAULT_REPORT_TYPE = 'Prod/Qual Summary (MTD/STD)';

    public String todayDisplayDate { get; set; }

    public Map<Id, String> farmIdToFarmName { get; set; }

    public String currentYear { get; set; }
    public String priorYear { get; set; }

    public AMSCollectionService.CollectionPeriodForm collectionPeriodFormForMonth { get; set; }
    public AMSCollectionService.CollectionPeriodForm collectionPeriodFormForSeason { get; set; }

    public AMSProdQualSummaryController() {
        todayDisplayDate = AMSCollectionService.convertDateToString(TODAY, 'Day', null);

        selectedReportTypeName = DEFAULT_REPORT_TYPE;
    }

    public override void defaultFromToDate() {
        currentYear = String.valueOf(TODAY.year());
        priorYear = String.valueOf(TODAY.year() - 1);

        fromDate = Date.newInstance(TODAY.year(), TODAY.month(), 1);
        toDate = TODAY;
    }

    //Call recalculation for tables on the page
    public override void recalculateForm() {
        populateMonthToDateForm();
        populateSeasonToDateForm();
    }

    //Callout to CollectionService class with details required for Month breakdown
    //Using start of month to today for default values
    public void populateMonthToDateForm() {
        collectionPeriodFormForMonth = new AMSCollectionService.CollectionPeriodForm(
            fromDate,
            toDate,
            Date.newInstance(TODAYYEARPRIOR.year(), TODAYYEARPRIOR.month(), 1),
            TODAYYEARPRIOR,
            priorFarmSeason,
            selectedFarmSeason,
            selectedFarmId,
            'Month');
    }

    //Callout to CollectionService class with details required for Month breakdown
    //Using start of season to today
    public void populateSeasonToDateForm() {
        collectionPeriodFormForSeason = new AMSCollectionService.CollectionPeriodForm(
            selectedFarmSeason.Reference_Period__r.Start_Date__c,
            TODAY,
            priorFarmSeason.Reference_Period__r.Start_Date__c,
            TODAYYEARPRIOR,
            priorFarmSeason,
            selectedFarmSeason,
            selectedFarmId,
            'Season');
    }
}