public class Prizm_AdvanceProcessor implements fsCore.ActionProcessor{
    
    private static final String CLASS_NAME = 'Prizm_AdvanceProcessor';
    private static final String LENDING_CONTRACT_ADVANCE_MAPPING = 'Lending_Contract_Advance_Mapping';
    public fsCore.ActionInput mActionInput;
    public fsCore.ActionOutput mActionOutput;
    
    public Prizm_AdvanceProcessor(){
        mActionInput = new fsCore.ActionInput();
        mActionOutput = new fsCore.ActionOutput();
    }
    
    public String getClassName() {
        return CLASS_NAME;
    }
    
    public void setInput(fsCore.ActionInput pInput) {
        mActionInput = pInput;
    }
    
    public fsCore.ActionOutput getOutput() {
        return mActionOutput;
    }
    
    public void process() {
        List<Advance__c> advanceRecs = getAdvanceRecs();
        Set<Id> advRecIds= new Set<Id>();
        if(!advanceRecs.isEmpty()){
            System.debug(loggingLevel.Error,'advanceRecs:--'+advanceRecs);
            insert advanceRecs;
            for(Advance__c advanceRec : advanceRecs){
                advRecIds.add(advanceRec.id);
            }

            if(!advRecIds.isEmpty()){
               createDisbursementAdvanceTrnx(advRecIds);
            }
        }
    }
    
    public List<Advance__c> getAdvanceRecs(){
        List<fsServ__Lending_Contract__c> contracts =new List<fsServ__Lending_Contract__c>();
        List<Advance__c> advanceRecs = new List<Advance__c>();
        if (mActionInput.getRecords().size() >0){
            contracts = Prizm_LendingContractUtil.getContracts(mActionInput.getRecords());
        }
        
        if(!contracts.isEmpty()){
            for(fsServ__Lending_Contract__c contract: contracts){
                fsCore.ObjectRecordMapper objectMapper = new fsCore.ObjectRecordMapper(LENDING_CONTRACT_ADVANCE_MAPPING);
                Advance__c advanceRec = (Advance__c)objectMapper.getTargetRecord(contract);
                advanceRec.Name = 'ADV'+contract.Name;
                advanceRec.Lender__c ='R';       //' Rabo'; need to confirm
                advanceRec.Rate_Type__c = 'V';   //'Variable'; need to confirm
                // advanceRec.Source_ID__c =     // need to confirm
                // advanceRec.Farm__c            //need to confirm; Mapped in Custom Metadata
                // advanceRec.Party__C           // need to confirm; Mapped in Custom Metadata
                advanceRecs.add(advanceRec);
            }
        }
        return advanceRecs;            
    }
    
    public static void createDisbursementAdvanceTrnx(Set<Id> pAdvanceIds){
         fsCore.ActionInput actionIP = new fsCore.ActionInput();
                actionIP.addRecords(pAdvanceIds);
                System.debug('actionIP'+actionIP);
                Prizm_DisbursementAdvTrnxProcessor actionObj = new Prizm_DisbursementAdvTrnxProcessor();
                actionObj.setInput(actionIP);
                actionObj.process();
    }
}