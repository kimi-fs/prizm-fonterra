global with sharing class Prizm_AdvanceTransactionTriggerManager implements fsCore.TriggerManager{
    global void beforeInsert(List<sObject> pNewRecList) { }
    global void beforeUpdate(
        List<sObject> pNewRecList,
        List<sObject> pOldRecList,
        Map<Id, sObject> pNewRecMap,
        Map<Id, sObject> pOldRecMap
    ) {
         Prizm_AdvanceTransactionTriggerHandler.updateProcessedOnDate((List<Advance_Transaction__c>)pNewRecList,
                                                                                    (Map<Id,Advance_Transaction__c>)pOldRecMap);
     
    }
    global void beforeDelete(List<sObject> pOldRecList, Map<Id, sObject> pOldRecMap) {
    }
    global void afterInsert(List<sObject> pNewRecList, Map<Id, sObject> pNewRecMap){
    }
    global void afterUpdate(
        List<sObject> pNewRecList,
        List<sObject> pOldRecList,
        Map<Id, sObject> pNewRecMap,
        Map<Id, sObject> pOldRecMap ) {
            Prizm_AdvanceTransactionTriggerHandler.createContractDisbursementRecord((List<Advance_Transaction__c>)pNewRecList,
                                                                                    (Map<Id,Advance_Transaction__c>)pOldRecMap);
        }
    global void afterDelete(List<sObject> pOldRecList, Map<Id, sObject> pOldRecMap) {
    }
    global void afterUndelete(List<sObject> pNewRecList, Map<Id, sObject> pNewRecMap) {
    }
}