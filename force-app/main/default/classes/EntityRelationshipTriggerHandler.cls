/**
 * Description: Handler class for EntityRelationshipTrigger
 * @author: Salman Zafar (Davanti Consulting)
 * @date: June 2015
 * @test: EntityRelationshipTrigger_Test
 */
public class EntityRelationshipTriggerHandler extends TriggerHandler {

    private static Id partyFarmRecordTypeId = GlobalUtility.partyFarmRecordTypeId;
    private static String partyFarmRecordName = GlobalUtility.partyFarmRecordName;
    private static Id individualPartyRecordTypeId = GlobalUtility.individualPartyRecordTypeId;
    private static Id derivedRelationshipRecordTypeId = GlobalUtility.derivedRelationshipRecordTypeId;

    private static final List<String> RELEVANT_FARM_SEASON_STATUS = new List<String> {
        GlobalUtility.REFERENCE_PERIOD_STATUS_CURRENT,
        GlobalUtility.REFERENCE_PERIOD_STATUS_PAST
    };

    public override void beforeInsert() {
        populateRelationshipId(Trigger.new);
    }
    public override void beforeUpdate() {
        populateRelationshipId(Trigger.new);
    }
    public override void afterInsert() {
        createDerivedIndividualRelationship(Trigger.new, (Map<Id, Entity_Relationship__c>) Trigger.oldMap);
    }
    public override void afterUpdate() {
        createDerivedIndividualRelationship(Trigger.new, (Map<Id, Entity_Relationship__c>) Trigger.oldMap);
    }
    public override void afterDelete() {
        deleteDerivedIndividualRelationship((Map<Id, Entity_Relationship__c>) Trigger.oldMap);
    }

    /**
     * Populate partner Id field on records.
     */
    public static void populateRelationshipId(List<Entity_Relationship__c> newList) {
        Set<Id> accountIds = new Set<Id>();
        for (Entity_Relationship__c er : newList) {
            accountIds.add(er.Farm__c);
            accountIds.add(er.Party__c);
        }
        Map<Id, Account> relatedAccounts = new Map<Id, Account>([
            SELECT Id, Name
            FROM Account
            WHERE Id IN :accountIds
        ]);

        for (Entity_Relationship__c er : newList) {
            List<String> partnerIdComponents = new List<String>();

            if (er.Party__c != null) {
                partnerIdComponents.add(relatedAccounts.containsKey(er.Party__c) ? relatedAccounts.get(er.Party__c).Name : '');
            }
            if (er.Farm__c != null) {
                partnerIdComponents.add(relatedAccounts.containsKey(er.Farm__c) ? relatedAccounts.get(er.Farm__c).Name : '');
            }

            partnerIdComponents.add(Schema.SObjectType.Entity_Relationship__c.getRecordTypeInfosById().get(er.RecordTypeId).getDeveloperName());
            partnerIdComponents.add(er.Role__c);
            partnerIdComponents.add(String.valueOf(er.Active__c));
            if (!er.Active__c) {
                partnerIdComponents.add(String.valueOf(er.Creation_Date__c));
            }

            er.Relationship_Id__c = String.join(partnerIdComponents, '-');
        }
    }

    public static void deleteDerivedIndividualRelationship(Map<Id, Entity_Relationship__c> mapOldEntityRelationship) {
        if (trigger.isDelete && !mapOldEntityRelationship.isEmpty()) {
            List<String> pfQueryFilters = new List<String>();

            for (Entity_Relationship__c erel : mapOldEntityRelationship.values()) {
                if (erel.Party__c != null && erel.Farm__c != null && erel.Active__c && (erel.RecordTypeId == partyFarmRecordTypeId || erel.Record_Type_Developer_Name__c == partyFarmRecordName)) {
                    pfQueryFilters.add(' (Party__c = \'' + erel.Party__c + '\' AND Farm__c = \'' + erel.Farm__c + '\') ');
                }
            }

            if (!pfQueryFilters.isEmpty()) {
                // Delete all related derived IRels
                String queryStr = ' SELECT Id ' +
                    ' FROM Individual_Relationship__c ' +
                    ' WHERE RecordTypeId =: derivedRelationshipRecordTypeId ' +
                    ' AND ( ' +
                    String.join(pfQueryFilters, ' OR ') +
                    ' ) ';

                List<Individual_Relationship__c> derivedRelationshipsForDelete = Database.query(queryStr);

                if (!derivedRelationshipsForDelete.isEmpty()) {
                    delete derivedRelationshipsForDelete;
                }
            }
        }
    }

    // FSF-523 View detailed (derived) relationships
    public static void createDerivedIndividualRelationship(List<Entity_Relationship__c> listNewEntityRelationship, Map<Id, Entity_Relationship__c> mapOldEntityRelationship) {
        // Map<Id,Id> mapPartyId2FarmId = new Map<Id,Id>();
        Map<Id, List<Id>> mapParty2ListFarm = new Map<Id, List<Id>>();

        // sets for deleting derived relationships
        Set<Id> setPartyId = new Set<Id>();
        Set<Id> setFarmId = new Set<Id>();
        Set<String> setRolesER = GlobalUtility.getRolesER();
        Set<String> setRolesIR = GlobalUtility.getRolesIR();
        Map<String, String> mapPartyFarm2Role = new Map<String, String>();

        for (Entity_Relationship__c er : listNewEntityRelationship) {
            if (setRolesER.contains(er.Role__c) && er.Party__c != null && er.Farm__c != null) {
                if (er.Active__c) {
                    mapPartyFarm2Role.put(String.valueOf(er.Party__c) + String.valueOf(er.Farm__c), er.Role__c);
                }

                if (trigger.isUpdate) {
                    if ((er.Farm__c != mapOldEntityRelationship.get(er.Id).Farm__c || er.Party__c != mapOldEntityRelationship.get(er.Id).Party__c ||
                        (er.Active__c && !mapOldEntityRelationship.get(er.Id).Active__c)) && er.Party__c != null
                        && er.Farm__c != null && er.Active__c && (er.RecordTypeId == partyFarmRecordTypeId || er.Record_Type_Developer_Name__c == partyFarmRecordName))
                    {
                        List<Id> listFarmId = new List<Id>();

                        if (mapParty2ListFarm.containsKey(er.Party__c)) {
                            listFarmId = mapParty2ListFarm.get(er.Party__c);
                            listFarmId.add(er.Farm__c);
                            mapParty2ListFarm.put(er.Party__c, listFarmId);
                        } else {
                            listFarmId.add(er.Farm__c);
                            mapParty2ListFarm.put(er.Party__c, listFarmId);
                        }
                    } else if (!er.Active__c && mapOldEntityRelationship.get(er.Id).Active__c && er.Party__c != null
                        && er.Farm__c != null && !er.Active__c && (er.RecordTypeId == partyFarmRecordTypeId || er.Record_Type_Developer_Name__c == partyFarmRecordName))
                    {
                        setPartyId.add(er.Party__c);
                        setFarmId.add(er.Farm__c);
                    }
                } else if (trigger.isInsert && er.Farm__c != null && er.Active__c && er.Party__c != null && (er.RecordTypeId == partyFarmRecordTypeId || er.Record_Type_Developer_Name__c == partyFarmRecordName))   {
                    List<Id> listFarmId = new List<Id>();
                    if (mapParty2ListFarm.containsKey(er.Party__c)) {
                        listFarmId = mapParty2ListFarm.get(er.Party__c);
                        listFarmId.add(er.Farm__c);
                        mapParty2ListFarm.put(er.Party__c, listFarmId);
                    } else {
                        listFarmId.add(er.Farm__c);
                        mapParty2ListFarm.put(er.Party__c, listFarmId);
                    }
                }
            }
        }

        Set<String> setUpsertRecordIds = new Set<String>();

        if (!mapParty2ListFarm.keySet().isEmpty()) {
            List<Individual_Relationship__c> listExistingIndividualRelationship = [
                SELECT Id, Party__c, Individual__c, Role__c, Active__c, PF_Role__c, Derived_Relationship_Upsert_Key__c
                FROM Individual_Relationship__c
                WHERE RecordTypeId = : individualPartyRecordTypeId
                    AND Party__c IN : mapParty2ListFarm.keySet()
                    AND Individual__c != null
                    AND Role__c IN : setRolesIR
                    AND Active__c = true
            ];
            List<Individual_Relationship__c> listIndividualRelationship2Upsert = new List<Individual_Relationship__c>();

            for (Individual_Relationship__c ir : listExistingIndividualRelationship) {
                for (Id farmId : mapParty2ListFarm.get(ir.Party__c)) {
                    String pfRole = mapPartyFarm2Role.get(String.valueOf(ir.Party__c) + farmId);
                    string upsertKey = String.valueOf(farmId) + String.valueOf(ir.Party__c) + String.valueOf(ir.Individual__c) + derivedRelationshipRecordTypeId + ir.Role__c + pfRole;

                    Individual_Relationship__c derivedIrel = new Individual_Relationship__c(
                        Party__c = ir.Party__c,
                        Farm__c = farmId,
                        RecordTypeId = derivedRelationshipRecordTypeId,
                        Individual__c = ir.Individual__c,
                        Derived_Relationship_Upsert_Key__c = upsertKey,
                        Role__c = ir.Role__c,
                        Active__c = ir.Active__c
                    );
                    if (String.isNotBlank(pfRole)) {
                        derivedIrel.PF_Role__c = pfRole;
                    }
                    listIndividualRelationship2Upsert.add(derivedIrel);
                }
            }

            Schema.SObjectField f = Individual_Relationship__c.Fields.Derived_Relationship_Upsert_Key__c;
            List<Database.UpsertResult> listCaseUpdateResult = Database.upsert(listIndividualRelationship2Upsert, f, false);

            Map<Id, String> mapRecId2ErrorMessage = new Map<Id, String>();
            integer i = 0;
            for (Database.UpsertResult res : listCaseUpdateResult) {
                if (res.getErrors().size() > 0) {
                    mapRecId2ErrorMessage.put(listCaseUpdateResult[i].Id, res.getErrors()[0].getMessage());
                }

                setUpsertRecordIds.add(res.getId());

                i++;
            }
        }

        if (!setPartyId.isEmpty() && !setFarmId.isEmpty()) {
            Set<String> activeErels = new Set<String>();
            for (Entity_Relationship__c e : [SELECT Id, Party__c, Farm__c, Role__c FROM Entity_Relationship__c WHERE Party__c IN : setPartyId AND Farm__c IN : setFarmId AND Active__c = true]) {
                activeErels.add(String.valueOf(e.Party__c) + String.valueOf(e.Farm__c) + e.Role__c);
            }

            List<Individual_Relationship__c> irels2delete = new List<Individual_Relationship__c>();
            for (Individual_Relationship__c i : [SELECT Id, Party__c, Farm__c, PF_Role__c FROM Individual_Relationship__c WHERE Party__c IN : setPartyId AND Id NOT IN : setUpsertRecordIds AND Farm__c IN : setFarmId AND RecordTypeId = : derivedRelationshipRecordTypeId]) {
                if (!activeErels.contains(String.valueOf(i.Party__c) + String.valueOf(i.Farm__c) + i.PF_Role__c)) {
                    irels2delete.add(i);
                }
            }

            if (irels2delete.size() > 0) {
                delete irels2delete;
            }
        }

        // if the Entity Relationship Role has changed find any related Derived Relationships and update their PF Role
        Map<String, String> derivedRelationshipKeys = new Map<String, String>();
        Set<Id> farms = new Set<Id>();
        Set<Id> parties = new Set<Id>();
        Set<String> oldRoles = new Set<String>();

        for (Entity_Relationship__c er : listNewEntityRelationship) {
            if (trigger.isUpdate && er.Role__c != mapOldEntityRelationship.get(er.Id).Role__c && setRolesER.contains(er.Role__c) && er.Party__c != null && er.Farm__c != null) {
                string irKey = String.valueOf(er.Farm__c) + String.valueOf(er.Party__c) + mapOldEntityRelationship.get(er.Id).Role__c;
                derivedRelationshipKeys.put(irKey, er.Role__c);
                farms.add(er.Farm__c);
                parties.add(er.Party__c);
                oldRoles.add(mapOldEntityRelationship.get(er.Id).Role__c);
            }
        }

        if (derivedRelationshipKeys.size() > 0) {
            List<Individual_Relationship__c> roleChanges = [SELECT Id, Derived_Relationship_Upsert_Key__c, Individual__c, Party__c, Farm__c, Role__c, PF_Role__c FROM Individual_Relationship__c WHERE Party__c IN : parties AND Farm__c IN : farms AND PF_Role__c IN : oldRoles AND RecordTypeId = : derivedRelationshipRecordTypeId AND Active__c = true];
            for (Individual_Relationship__c ir : roleChanges) {
                string irKey = String.valueOf(ir.Farm__c) + String.valueOf(ir.Party__c) + ir.PF_Role__c;
                if (derivedRelationshipKeys.containsKey(irKey)) {
                    ir.PF_Role__c = derivedRelationshipKeys.get(irKey);
                    ir.Derived_Relationship_Upsert_Key__c = String.valueOf(ir.Farm__c) + String.valueOf(ir.Party__c) + String.valueOf(ir.Individual__c) + derivedRelationshipRecordTypeId + ir.Role__c + ir.PF_Role__c;
                }
            }
            update roleChanges;
        }
    }

}