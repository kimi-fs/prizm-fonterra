/**
 * Description: Test for AMSMyDetailsController
 * @author: Amelia (Trineo)
 * @date: August 2017
 */
@isTest
private class AMSMyDetailsControllerTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';
    private static final String STREET_NUMBER = '666';
    private static final String STREET_1 = 'Street 1';
    private static final String STREET_2 = 'Street 2';
    private static final String SUBURB = 'Suburb';
    private static final String CITY = 'City';
    private static final String STATE = 'QLD';
    private static final String POSTCODE = 'Postcode';
    private static final String COUNTRY_CODE = 'AU';

    @testSetup static void testSetup(){
        SetupAMSCommunityPageMessages.createPageMessages();
        TestClassDataUtil.individualDefaultAccountAU();
    }

    @isTest static void testConstructor_DifferentAddress() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        Address__c postal = TestClassDataUtil.createIndividualAddress(communityUser.ContactId, '1', 'postal street', 'postal street 2');
        postal.Active__c = true;
        postal.RecordTypeId = Schema.SObjectType.Address__c.getRecordTypeInfosByName().get('Postal').getRecordTypeId();
        update postal;
        Address__c physical = TestClassDataUtil.createIndividualAddress(communityUser.ContactId, '1', 'physical street', 'physical street 2');
        physical.Active__c = true;
        physical.RecordTypeId = Schema.SObjectType.Address__c.getRecordTypeInfosByName().get('Physical').getRecordTypeId();
        update physical;

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            System.assertEquals('', controller.message, 'message was not reset');
            System.assertEquals(false, controller.showSuccess, 'showSuccess was not reset');
            System.assertEquals(false, controller.showError, 'showError was not reset');
            System.assertEquals(false, controller.showEmailError, 'showEmailError was not reset');
            System.assertEquals(false, controller.showMyDetailsError, 'showMyDetailsError was not reset');
            System.assertEquals(false, controller.showMobileError, 'showMobileError was not reset');
            System.assertEquals(false, controller.showAddressError, 'showAddressError was not reset');

            System.assertEquals('No', controller.addressesSame, 'Addresses were different');

            System.assertEquals(communityUser.Id, controller.user.Id, 'Incorrect user');
            System.assertEquals(communityUser.ContactId, controller.contact.Id, 'Incorrect contact');

            System.assertEquals(postal.Id, controller.postalAddress.Id, 'Incorrect postal address');
            System.assertEquals(physical.Id, controller.physicalAddress.Id, 'Incorrect physical address');
        }
    }

    @isTest static void testConstructor_SameAddress() {
        String STREET_NUMBER = '123';
        String STREET = 'street';

        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        Address__c postal = TestClassDataUtil.createIndividualAddress(communityUser.ContactId, STREET_NUMBER, STREET, STREET);
        postal.Active__c = true;
        postal.RecordTypeId = Schema.SObjectType.Address__c.getRecordTypeInfosByName().get('Postal').getRecordTypeId();
        update postal;
        Address__c physical = TestClassDataUtil.createIndividualAddress(communityUser.ContactId, STREET_NUMBER, STREET, STREET);
        physical.Active__c = true;
        physical.RecordTypeId = Schema.SObjectType.Address__c.getRecordTypeInfosByName().get('Physical').getRecordTypeId();
        update physical;

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            System.assertEquals('Yes', controller.addressesSame, 'Addresses were the same');

            System.assertEquals(postal.Id, controller.postalAddress.Id, 'Incorrect postal address');
            System.assertEquals(physical.Id, controller.physicalAddress.Id, 'Incorrect physical address');
        }
    }

    @isTest static void testGetFirstTimeSetup(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();
            System.assertEquals(true, controller.getFirstTimeSetup(), 'first time setup was incorrect');
        }
    }

    @isTest static void testGetWelcomeName_PreferredName(){
        String PREFERRED_NAME = 'Preferred Name';
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        Contact communityUserContact = new Contact(Id = communityUser.ContactId, Preferred_Name__c = PREFERRED_NAME, Type__c = 'Personal');
        update communityUserContact;

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();
            System.assertEquals(PREFERRED_NAME.toUpperCase(), controller.getWelcomeName(), 'Wrong welcome name');
        }
    }

    @isTest static void testGetWelcomeName_WithoutPreferredName(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        Contact communityUserContact = [SELECT Id, FirstName FROM Contact WHERE Id =: communityUser.ContactId];

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();
            System.assertEquals(communityUserContact.FirstName.toUpperCase(), controller.getWelcomeName(), 'Wrong welcome name');
        }
    }

    @isTest static void testGetFullName(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        Contact communityUserContact = [SELECT Id, FirstName, MiddleName, LastName FROM Contact WHERE Id =: communityUser.ContactId];

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();
            System.assertEquals(communityUserContact.FirstName + ' ' + communityUserContact.LastName, controller.getFullName(), 'Wrong name');
        }
    }

    @isTest static void testGetUserPhoto(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            try {
                controller.getUserPhoto();
            } catch (Exception e){
                System.assert(true);
            }
        }
    }

    @isTest static void testUploadUserPhoto(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            controller.document = Blob.valueOf('1');
            controller.filename = 'test blob';
            controller.contentType = 'image/jpeg';
            controller.uploadUserPhoto();

            System.assertEquals(false, controller.showError, 'showError was not reset');
            System.assertEquals('ConnectApi methods are not supported in data siloed tests. Please use @IsTest(SeeAllData=true).', controller.message);
            System.assertEquals(null, controller.document, 'document was populated');
        }
    }

    @isTest static void testSaveEmail_UniqueEmail(){
        String NEW_EMAIL = 'new@amstest.co.nz';
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            controller.contact.email = NEW_EMAIL;
            controller.saveEmail();

            System.assertEquals(NEW_EMAIL, [SELECT Id, Email FROM Contact WHERE Id =: communityUser.ContactId].Email, 'Email was not updated');
            System.assertEquals(true, controller.showSuccess, 'showSuccess was not updated');
            System.assert(String.isNotBlank(controller.message), 'Missing message');
        }
    }

    @isTest static void testSaveEmail_DuplicateEmail(){
        String NEW_EMAIL = 'new@amstest.co.nz';
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        User existingEmailUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(NEW_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            controller.contact.email = NEW_EMAIL;
            controller.saveEmail();

            System.assertNotEquals(NEW_EMAIL, [SELECT Id, Email FROM Contact WHERE Id =: communityUser.ContactId].Email, 'Email was not updated');
            System.assertEquals(true, controller.showEmailError, 'showEmailError was not updated');
            System.assert(String.isNotBlank(controller.message), 'Missing message');
        }
    }

    @isTest static void testSaveEmail_InvalidEmail(){
        String NEW_EMAIL = 'I am not an email';
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            controller.contact.email = NEW_EMAIL;
            controller.saveEmail();

            System.assertNotEquals(NEW_EMAIL, [SELECT Id, Email FROM Contact WHERE Id =: communityUser.ContactId].Email, 'Email was not updated');
            System.assertEquals(true, controller.showEmailError, 'showEmailError was not updated');
            System.assert(String.isNotBlank(controller.message), 'Missing message');
        }
    }

    @isTest static void testSaveContact_ValidInputs(){
        String PREFERRED_NAME = 'Preferred Name';
        String MAIN_PHONE = '123456789';
        String MOBILE = '+61987654321';

        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            controller.contact.Preferred_Name__c = PREFERRED_NAME;
            controller.contact.Phone = MAIN_PHONE;
            controller.contact.MobilePhone = MOBILE;
            controller.saveContact();

            Contact updatedContact = [SELECT Id, Email, Preferred_Name__c, Phone, MobilePhone FROM Contact WHERE Id =: communityUser.ContactId];
            System.assertEquals(PREFERRED_NAME, updatedContact.Preferred_Name__c, 'Preferred Name was not updated');
            System.assertEquals(MAIN_PHONE, updatedContact.Phone, 'Phone was not updated');
            System.assertEquals(MOBILE, updatedContact.MobilePhone, 'Mobile was not updated');
            System.assertEquals(true, controller.showSuccess, 'showSuccess was not updated');
            System.assert(String.isNotBlank(controller.message), 'Missing message');
        }
    }

    @isTest static void testSaveContact_InvalidInputs(){
        String PREFERRED_NAME = 'Preferred Name LOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONG';
        String MAIN_PHONE = '123456789LOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONG';
        String MOBILE = '987654321LOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONG';

        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            controller.contact.Preferred_Name__c = PREFERRED_NAME;
            controller.contact.Phone = MAIN_PHONE;
            controller.contact.MobilePhone = MOBILE;
            controller.saveContact();

            Contact updatedContact = [SELECT Id, Email, Preferred_Name__c, Phone, MobilePhone FROM Contact WHERE Id =: communityUser.ContactId];
            System.assertNotEquals(PREFERRED_NAME, updatedContact.Preferred_Name__c, 'Preferred Name was updated');
            System.assertNotEquals(MAIN_PHONE, updatedContact.Phone, 'Phone was updated');
            System.assertNotEquals(MOBILE, updatedContact.MobilePhone, 'Mobile was updated');
            System.assertEquals(true, controller.showMyDetailsError, 'showMyDetailsError was not updated');
            System.assert(String.isNotBlank(controller.message), 'Missing message');
        }
    }

    @isTest static void testSaveContact_WithoutCountryPrefix(){
        String MOBILE = '0 987 654 321';

        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            controller.contact.MobilePhone = MOBILE;
            controller.saveContact();

            Contact updatedContact = [SELECT Id, Email, Preferred_Name__c, Phone, MobilePhone FROM Contact WHERE Id =: communityUser.ContactId];
            System.assertEquals(AMSMyDetailsController.AUSTRALIA_NUMBER_PREFIX + MOBILE.removeStart('0'), updatedContact.MobilePhone, 'Mobile was updated');
            System.assertEquals(true, controller.showSuccess, 'showSuccess was not updated');
            System.assert(String.isNotBlank(controller.message), 'Missing message');
        }
    }

    @isTest static void testResetContact(){
        String PREFERRED_NAME = 'New Preferred Name';
        String MAIN_PHONE = '9999999';
        String MOBILE = '+61999999';

        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            controller.contact.Preferred_Name__c = PREFERRED_NAME;
            controller.contact.Phone = MAIN_PHONE;
            controller.contact.MobilePhone = MOBILE;
            controller.resetContact();

            System.assertNotEquals(PREFERRED_NAME, controller.contact.Preferred_Name__c, 'Preferred Name was updated');
            System.assertNotEquals(MAIN_PHONE, controller.contact.Phone, 'Phone was updated');
            System.assertNotEquals(MOBILE, controller.contact.MobilePhone, 'Mobile was updated');
        }
    }

    @isTest static void testSaveAddress_NoExistingAddresses(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();
            controller.physicalAddress.Street_Number__c = STREET_NUMBER;
            controller.physicalAddress.Street_1__c = STREET_1;
            controller.physicalAddress.Street_2__c = STREET_2;
            controller.physicalAddress.Suburb__c = SUBURB;
            controller.physicalAddress.City__c = CITY;
            controller.physicalAddress.State__c = STATE;
            controller.physicalAddress.Postcode__c = POSTCODE;
            controller.physicalAddress.Country_Code__c = COUNTRY_CODE;

            controller.saveAddress();

            System.assertEquals(true, controller.showSuccess, 'showSuccess was not updated');
            System.assert(String.isNotBlank(controller.message), 'Missing message');
        }
        List<Address__c> updatedAddress = [SELECT Id, Street_Number__c, Street_1__c, Street_2__c, Suburb__c, City__c, State__c, Postcode__c, Country_Code__c FROM Address__c WHERE Individual__c =: communityUser.ContactId AND RecordType.Name = 'Postal'];
        System.assertEquals(STREET_NUMBER, updatedAddress[0].Street_Number__c, 'Address not updated');
        System.assertEquals(STREET_1, updatedAddress[0].Street_1__c, 'Address not updated');
        System.assertEquals(STREET_2, updatedAddress[0].Street_2__c, 'Address not updated');
        System.assertEquals(SUBURB, updatedAddress[0].Suburb__c, 'Address not updated');
        System.assertEquals(CITY, updatedAddress[0].City__c, 'Address not updated');
        System.assertEquals(STATE, updatedAddress[0].State__c, 'Address not updated');
        System.assertEquals(POSTCODE, updatedAddress[0].Postcode__c, 'Address not updated');
        System.assertEquals(COUNTRY_CODE, updatedAddress[0].Country_Code__c, 'Address not updated');
    }

    @isTest static void testSaveAddress_BlankPostCode(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            controller.physicalAddress.Street_Number__c = STREET_NUMBER;
            controller.physicalAddress.Street_1__c = STREET_1;
            controller.physicalAddress.Street_2__c = STREET_2;
            controller.physicalAddress.Suburb__c = SUBURB;
            controller.physicalAddress.City__c = CITY;
            controller.physicalAddress.State__c = STATE;
            controller.physicalAddress.Postcode__c = '';
            controller.physicalAddress.Country_Code__c = COUNTRY_CODE;

            controller.postalAddress.Street_Number__c = STREET_NUMBER;
            controller.postalAddress.Street_1__c = STREET_1;
            controller.postalAddress.Street_2__c = STREET_2;
            controller.postalAddress.Suburb__c = SUBURB;
            controller.postalAddress.City__c = CITY;
            controller.postalAddress.State__c = STATE;
            controller.postalAddress.Postcode__c = '';
            controller.postalAddress.Country_Code__c = COUNTRY_CODE;

            controller.saveAddress();

            System.assertEquals(true, controller.showAddressError, 'showAddressError was not updated');
            System.assert(String.isNotBlank(controller.message), 'Missing message');
        }
    }

    @isTest static void testSaveAddress_AddressesSame(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.assertEquals(0, [SELECT count() FROM Address__c]);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();

            controller.physicalAddress.Street_Number__c = STREET_NUMBER;
            controller.physicalAddress.Street_1__c = STREET_1;
            controller.physicalAddress.Street_2__c = STREET_2;
            controller.physicalAddress.Suburb__c = SUBURB;
            controller.physicalAddress.City__c = CITY;
            controller.physicalAddress.State__c = STATE;
            controller.physicalAddress.Postcode__c = POSTCODE;
            controller.physicalAddress.Country_Code__c = COUNTRY_CODE;

            controller.addressesSame = 'Yes';
            controller.saveAddress();

            System.assertEquals(2, [SELECT count() FROM Address__c]);
            System.assertEquals(true, controller.showSuccess, 'showSuccess was not updated');
            System.assert(String.isNotBlank(controller.message), 'Missing message');

            List<Address__c> updatedAddress = [SELECT Id, Street_Number__c, Street_1__c, Street_2__c, Suburb__c, City__c, State__c, Postcode__c, Country_Code__c FROM Address__c WHERE Individual__c =: communityUser.ContactId ];

            System.assertEquals(STREET_NUMBER, updatedAddress[0].Street_Number__c, 'Address not updated');
            System.assertEquals(STREET_1, updatedAddress[0].Street_1__c, 'Address not updated');
            System.assertEquals(STREET_2, updatedAddress[0].Street_2__c, 'Address not updated');
            System.assertEquals(SUBURB, updatedAddress[0].Suburb__c, 'Address not updated');
            System.assertEquals(CITY, updatedAddress[0].City__c, 'Address not updated');
            System.assertEquals(STATE, updatedAddress[0].State__c, 'Address not updated');
            System.assertEquals(POSTCODE, updatedAddress[0].Postcode__c, 'Address not updated');
            System.assertEquals(COUNTRY_CODE, updatedAddress[0].Country_Code__c, 'Address not updated');

            System.assertEquals(updatedAddress[0].Street_Number__c, updatedAddress[1].Street_Number__c, 'Address not the same');
            System.assertEquals(updatedAddress[0].Street_1__c, updatedAddress[1].Street_1__c, 'Address not the same');
            System.assertEquals(updatedAddress[0].Street_2__c, updatedAddress[1].Street_2__c, 'Address not the same');
            System.assertEquals(updatedAddress[0].Suburb__c, updatedAddress[1].Suburb__c, 'Address not the same');
            System.assertEquals(updatedAddress[0].City__c, updatedAddress[1].City__c, 'Address not the same');
            System.assertEquals(updatedAddress[0].State__c, updatedAddress[1].State__c, 'Address not the same');
            System.assertEquals(updatedAddress[0].Postcode__c, updatedAddress[1].Postcode__c, 'Address not the same');
            System.assertEquals(updatedAddress[0].Country_Code__c, updatedAddress[1].Country_Code__c, 'Address not the same');
        }

    }

    @isTest static void testCompleteSetup(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();
            PageReference pr = controller.completeSetup();

            System.assert(TestClassDataUtil.checkPageReferenceMatches(Page.AMSDashboard, pr), 'Wrong page redirected');
            System.assertEquals(true, [SELECT Id, First_Time_Setup_Complete__c FROM User WHERE Id =: communityUser.Id].First_Time_Setup_Complete__c, 'First time setup complete not updated');
        }
    }

    @isTest static void testCompleteSetup_InvalidUser(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();
            controller.user.Email = 'Invalid email';
            PageReference pr = controller.completeSetup();

            System.assert(TestClassDataUtil.checkPageReferenceMatches(null, pr), 'Wrong page redirected');
            System.assertEquals(false, [SELECT Id, First_Time_Setup_Complete__c FROM User WHERE Id =: communityUser.Id].First_Time_Setup_Complete__c, 'First time setup complete not updated');
        }
    }

    @isTest static void testVerifyDetails(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();
            controller.verifyDetails();

            System.assertEquals(Date.today(), [SELECT Id, Last_Validated_Date__c FROM Contact WHERE Id =: communityUser.ContactId].Last_Validated_Date__c, 'Last Validated Date not updated');
        }
    }

    @isTest static void testVerifyDetails_InvalidUser(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
		update new Contact(
            Id = communityUser.ContactId,
            Last_Validated_Date__c = Date.today().addDays(-5)
        );
        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();
            controller.contact.Email = 'Invalid email';
            controller.verifyDetails();

            System.assertNotEquals(Date.today(), [SELECT Id, Last_Validated_Date__c FROM Contact WHERE Id =: communityUser.ContactId].Last_Validated_Date__c, 'Last Validated Date not updated');
            System.assertEquals(true, controller.showError, 'showError was not updated');
        }
    }

    @isTest static void testGetAddressSameOptions(){
        System.assertEquals(2, AMSMyDetailsController.getAddressSameOptions().size());
    }

    @isTest static void testChangePassword(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSMyDetailsController controller = new AMSMyDetailsController();
            System.assert(TestClassDataUtil.checkPageReferenceMatches(Page.AMSPasswordChange, controller.changePassword()), 'Wrong page redirected');
        }
    }

}