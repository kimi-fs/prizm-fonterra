/* 
* Class Name   - Prizm_CommonUtil
*
* Description  - Util Class for Prizm Custom Classes
*
* Developer(s) - Financial Spectra
*/

public class Prizm_CommonUtil {
    
    public static Map<String, fsCore__Branch_Setup__c> getBranches(Set<String> pBranchNames){
        List<fsCore__Branch_Setup__c> branches =
            [SELECT Id, Name, fsCore__Branch_Code__c
             , fsCore__Company_Name__c
             , fsCore__Business_Date__c
             FROM fsCore__Branch_Setup__c
             WHERE Name IN :pBranchNames
             AND fsCore__Is_Active__c = true];
        
        Map<String, fsCore__Branch_Setup__c> branchMap = new Map<String, fsCore__Branch_Setup__c>();
        for (fsCore__Branch_Setup__c branch : branches){
            branchMap.put(branch.Name, branch);
        }
        return branchMap;
    }
    
    public static Map<String, fsCore__Product_Setup__c> getProducts(Set<String> pProductCodes){
        List<fsCore__Product_Setup__c> productsWithItmzs =
            [SELECT Id, Name
             , fsCore__Product_Code__c
             , fsCore__Product_Family__c  
             , fsCore__Start_Date__c 
             , fsCore__End_Date__c
             , fsCore__Is_Active__c
             , fsCore__Collateral_Family__c
             , fsCore__Is_Secured__c
             , fsCore__Is_Syndication_Allowed__c
             , fsCore__Is_Billing_Cycle_Flexible__c
             , fsCore__Is_Funding_In_Tranches_Allowed__c
             , Default_Contract_Template__c
             , Default_Index_Setup__c
             , Default_Pricing_Setup__c
             , (SELECT Id, Name
                , fsCore__Itemization_Code__c 
                , fsCore__Itemization_Family__c 
                , fsCore__Itemization_Name__c 
                , fsCore__Plus_Or_Minus__c
                , fsCore__Display_Order__c
                , fsCore__Disbursement_Allowed__c
                , fsCore__Fee_Category__c 
                , fsCore__Is_Override_Allowed__c
                FROM fsCore__Product_Itemization_Setup__r
                WHERE fsCore__Is_Active__c = true
                ORDER BY fsCore__Display_Order__c)
             FROM fsCore__Product_Setup__c
             WHERE fsCore__Product_Code__c IN :pProductCodes
             AND fsCore__Is_Active__c = true];
        
        Map<String, fsCore__Product_Setup__c> productMapByCode = new Map<String, fsCore__Product_Setup__c>();
        for(fsCore__Product_Setup__c prod : productsWithItmzs){
            productMapByCode.put(prod.fsCore__Product_Code__c, prod);
        }
        System.debug('Product  : '+productsWithItmzs);
        return productMapByCode;
    }
    
    public static Map<Id, fsCore__Contract_Template_Setup__c> getContractTemplates(Set<Id> pContractTemplateIds){
         fsCore.DynamicQueryBuilder contractTemplateQuery = fsCore.DynamicQueryFactory.createQuery(Schema.SObjectType.fsCore__Contract_Template_Setup__c.getName())
                .addFields()
              	.addWhereConditionWithBind(1,'Id','IN','pContractTemplateIds')
        		.addWhereConditionWithValue(2,'fsCore__Is_Active__c','=',true);
        List<fsCore__Contract_Template_Setup__c> contractTemplates = 
            (List<fsCore__Contract_Template_Setup__c>)Database.query(contractTemplateQuery.getQueryString());
        
            /*[SELECT Id, Name, fsCore__Contract_Template_Code__c
             , fsCore__Product_Family__c  
             , fsCore__Start_Date__c 
             , fsCore__End_Date__c
             , fsCore__Is_Active__c
             , fsCore__Index_Name__c
             , fsCore__Direct_Debit_Advance_Days__c
             , fsCore__Is_Advance_Draw_Preferences_Allowed__c
             , fsCore__Is_Advance_Holiday_Preferences_Allowed__c
             , fsCore__Is_Advance_Payment_Preferences_Allowed__c
             FROM fsCore__Contract_Template_Setup__c
             WHERE Id IN :pContractTemplateIds
             AND fsCore__Is_Active__c = true];*/
        
        Map<Id, fsCore__Contract_Template_Setup__c> contractTemplateMapById = new Map<Id, fsCore__Contract_Template_Setup__c>();
        for(fsCore__Contract_Template_Setup__c ctTemplt : contractTemplates){
            contractTemplateMapById.put(ctTemplt.Id, ctTemplt);
        }
        
        System.debug(loggingLevel.ERROR,'Contract Template : '+contractTemplates);
        System.debug(loggingLevel.ERROR,'Contract Template size : '+contractTemplates.size());
        System.debug(loggingLevel.ERROR,'Contract Template Map : '+ contractTemplateMapById);
        return contractTemplateMapById;
    }
    
    public static Map<Id, fsCore__Pricing_Setup__c> getPricings(Set<Id> pPricingIds){
        List<fsCore__Pricing_Setup__c> pricings = 
            [SELECT Id, Name, fsCore__Pricing_Code__c
             , fsCore__Billing_Method__c  
             , fsCore__Start_Date__c 
             , fsCore__End_Date__c
             , fsCore__Is_Active__c
             , fsCore__Number_Of_Payments__c
             , fsCore__Index_Name__c
             , fsCore__Financed_Amount_Maximum__c
             , fsCore__Financed_Amount_Minimum__c
             , fsCore__Payment_Cycle__c
             , fsCore__Pricing_Method__c
             , fsCore__Rate_Value__c
             , fsCore__Is_Index_Based__c
             , fsCore__Interest_Accrual_Start_From__c
             , fsCore__Default_Calculate_Option__c
             FROM fsCore__Pricing_Setup__c
             WHERE Id IN :pPricingIds
             AND fsCore__Is_Active__c = true];
        
        Map<Id, fsCore__Pricing_Setup__c> pricingMapById = new Map<Id, fsCore__Pricing_Setup__c>();
        for(fsCore__Pricing_Setup__c pricing : pricings){
            pricingMapById.put(pricing.Id, pricing);
        }
        
        System.debug(loggingLevel.ERROR,'Contract Template : '+pricings);
        System.debug(loggingLevel.ERROR,'Contract Template size : '+pricings.size());
        System.debug(loggingLevel.ERROR,'Contract Template Map : '+ pricingMapById);
        return pricingMapById;
    }
    
    public static  Map<Id,List<fsCore__Index_Rate_Setup__c>> getIndexRates(Set<Id> pIndexIds){
        system.debug(loggingLevel.ERROR, 'inside method');
        Map<Id,List<fsCore__Index_Rate_Setup__c>> IdtoIndexRateSetUpMap = new Map<Id, List<fsCore__Index_Rate_Setup__c>>();
        List<fsCore__Index_Rate_Setup__c> indexRateList = [Select Id
                                                           , Name
                                                           , fsCore__Index_Rate__c
                                                           , fsCore__Start_Date__c
                                                           , fsCore__End_Date__c
                                                           , fsCore__Index_Name__c 
                                                           from fsCore__Index_Rate_Setup__c 
                                                           where fsCore__Index_Name__c IN :pIndexIds
                                                           AND fsCore__Is_Active__c = true];
        
        for(fsCore__Index_Rate_Setup__c indexRate : indexRateList)
        {
            
            system.debug(loggingLevel.ERROR, 'inside for');
            if(IdtoIndexRateSetUpMap.containsKey(indexRate.fsCore__Index_Name__c))
            {
                system.debug(loggingLevel.ERROR, 'inside if');
                IdtoIndexRateSetUpMap.get(indexRate.fsCore__Index_Name__c).add(indexRate);
            }
            else
            {
                system.debug(loggingLevel.ERROR, 'inside else');
                IdtoIndexRateSetUpMap.put(indexRate.fsCore__Index_Name__c,new List<fsCore__Index_Rate_Setup__c>{indexRate});
            }
        }
        system.debug(loggingLevel.ERROR, IdtoIndexRateSetUpMap);
        system.debug(loggingLevel.ERROR, IdtoIndexRateSetUpMap.size());
        return IdtoIndexRateSetUpMap;        
        
    }
    
    public static Map<String, String> getCustomAttributesMap(){
        Map<String, String> attributeNameToValueMap = new Map<String, String>();
        List<fsCore__Custom_Attribute__mdt> attributesList = [Select DeveloperName
                                                              , fsCore__Description__c
                                                              , fsCore__Type__c
                                                              , fsCore__Number_Value__c
                                                              , fsCore__String_Value__c
                                                              , fsCore__Value_Set__c
                                                              From fsCore__Custom_Attribute__mdt];
        
        for(fsCore__Custom_Attribute__mdt attribute : attributesList){
            if(attribute.fsCore__Type__c == 'String'){
                attributeNameToValueMap.put(attribute.DeveloperName, attribute.fsCore__String_Value__c);
            }
            else if(attribute.fsCore__Type__c == 'Number'){
                attributeNameToValueMap.put(attribute.DeveloperName, attribute.fsCore__Number_Value__c);
            }
            else if(attribute.fsCore__Type__c == 'Value Set'){
                attributeNameToValueMap.put(attribute.DeveloperName, attribute.fsCore__Value_Set__c);
            }     
        }
        return attributeNameToValueMap;
    }
    
    public static Map<Id, Account> getAccounts(Set<Id> pAccountIds){
        return new Map<Id, Account>([SELECT Id
                                     , Farm_Region_Name__c
                                     , Primary_On_Farm_Contact__c
                                     FROM Account 
                                     WHERE Id IN : pAccountIds]);
    }
    
    public static void insertErrorLog(List<fsCore.ErrorObject> pErrorObjectList, String pClassName, String pMethodName) {
        if (!pErrorObjectList.isEmpty()) {
            List<fscore__Diagnostic_Log__c> errorLogs = fsCore.DiagnosticLogUtil.getErrorLogs(
                pClassName,
                pMethodName,
                pErrorObjectList
            );
            Database.insert(errorLogs, false);
        }
    }
    
    public static fsCore.ErrorObject getErrorObject(Exception pException) {
        fsCore.ErrorObject errObj = new fsCore.ErrorObject();
        errObj.setErrorMessage(pException.getMessage());
        errObj.setErrorCode(fsCore.Constants.PROCESSING_ERROR);
        errObj.setErrorStackTrace(pException.getStackTraceString());
        
        return errObj;
    }
}