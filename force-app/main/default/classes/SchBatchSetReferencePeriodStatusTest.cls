/*------------------------------------------------------------
Author:			Sean Soriano
Company:		Davanti Consulting
Description:	Test Class for BatchSetReferencePeriodStatus Batch Class
History
12/01/2015		Sean Soriano	Created
------------------------------------------------------------*/
@isTest
private class SchBatchSetReferencePeriodStatusTest {

	@testSetup static void createReferencePeriod() {

		List<Reference_Period__c> lstTestReferencePeriod = new List<Reference_Period__c>();

		//Addresss
		Reference_Period__c rp1 = new  Reference_Period__c();
		rp1.Start_Date__c = system.today();

		Reference_Period__c rp2 = new  Reference_Period__c();
		rp2.Start_Date__c = system.today()+1;
		rp2.End_Date__c = system.today()+2;

		Reference_Period__c rp3 = new  Reference_Period__c();
		rp3.Start_Date__c = system.today()-2;
		rp3.End_Date__c = system.today()-1;

		Reference_Period__c rp4 = new  Reference_Period__c();
		rp4.Start_Date__c = system.today()-1;
		rp4.End_Date__c = system.today()+2;

		lstTestReferencePeriod.add(rp1);
		lstTestReferencePeriod.add(rp2);
		lstTestReferencePeriod.add(rp3);
		lstTestReferencePeriod.add(rp4);

		insert lstTestReferencePeriod;
	}

	@isTest static void test_ScheduleBatchSetReferencePeriodStatus() {
        String CRON_EXP = '0 0 0 3 9 ? 2022';

        Test.startTest();
        String jobId = System.schedule('ScheduleBatchSetReferencePeriodStatus', CRON_EXP,
         new ScheduleBatchSetReferencePeriodStatus());
        Test.stopTest();
	}

	@isTest static void test_BatchSetReferencePeriodStatus() {
        Test.startTest();
            ScheduleBatchSetReferencePeriodStatus batch = new ScheduleBatchSetReferencePeriodStatus();
            Database.executeBatch(batch);
        Test.stopTest();
	}

}