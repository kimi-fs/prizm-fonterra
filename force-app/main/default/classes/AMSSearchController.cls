/**
 * AMSSearchController
 *
 * Controller for collecting the search results
 *
 * Author: Ed (Trineo)
 * Date: July 2017
 */
public with sharing class AMSSearchController {
    public class SearchException extends Exception {}
    public List<AMSKnowledgeService.SearchResult> pageResults { get; private set; }
    public String searchString { get; set; }

    @TestVisible private static final Integer PAGE_SIZE = 10;
    @TestVisible private static final Integer PAGINATION_NUMBER_OF_LINKED_PAGES = 4;

    public Integer pageNumber { get; set; }
    public Integer numberOfPages { get; set; }
    public Integer numberOfArticles { get; set; }

    private Boolean withSnippet;
    private List<String> searchedArticleTypes;
    private AMSAdviceController adviceController;

    private Integer offset {
        get {
            return (pageNumber-1)*PAGE_SIZE;
        }
        private set;
    }

    /**
     * The constructor simply sets up the search term for a later search
     */
    public AMSSearchController() {
        initialise();
        this.withSnippet = true;
        this.searchedArticleTypes = AMSKnowledgeService.searchableArticleRecordTypes;
    }

    public AMSSearchController(AMSAdviceController adviceController) {
        initialise();
        this.withSnippet = false;
        this.adviceController = adviceController;
        this.adviceController.initialiseFAQIfSelected();
        this.searchedArticleTypes = new List<String>{'AMS_FAQ'};
    }

    private void initialise() {
        // Grab the search terms
        String searchParameter = ApexPages.currentPage().getParameters().get(getSearchParameterName());
        this.pageNumber = 1;
        this.numberOfPages = 1;
        this.numberOfArticles = 0;

        // Sanitise the search parameter
        if (String.isEmpty(searchParameter)) {
            this.searchString = '';
            return;
        }

        this.searchString = searchParameter;
    }

    /**
     * Actually run the search (as an action off of the page).
     */
    public void performSearch() {
        fetchResults();
        // Recalculate the size of the results;
        this.numberOfPages = null;
        this.numberOfArticles = null;

        getNumberOfArticles();
        getNumberOfPages();
        this.pageNumber = 1;

        // When performing a search on the advice page lend a hand and clear the current article
        // every time we search.
        if (this.adviceController != null) {
            this.adviceController.clearSelectedFAQ();
        }
    }

    /**
     * Do a redirect for the search rather than do it on page - this sanitises the browser history
     * so that we can use the browser back button after clicking through to the search results.
     */
    public PageReference pageRequestSearch() {
        return search(this.searchString);
    }

    public void clearSearch() {
        this.searchString = null;
        this.pageNumber = 1;
        this.numberOfPages = 1;
        this.numberOfArticles = 0;
        this.pageResults = null;
    }

    private void fetchResults() {
        System.debug('fetching articles offset: ' + this.offset);
        this.pageResults = AMSKnowledgeService.findMatchingArticles(this.searchString, this.searchedArticleTypes,
                                                                    PAGE_SIZE, this.offset, this.withSnippet);
    }

    public void jumpToPage() {
        System.debug('Jump to page ' + this.pageNumber);
        fetchResults();
    }

    /**
     * Perform the search and return the page reference for the search results
     */
    public static PageReference search(String searchString) {
        PageReference searchPage = Page.AMSSearch;
        searchPage.getParameters().put(getSearchParameterName(), searchString);
        searchPage.setRedirect(true);
        return searchpage;
    }

    public List<PaginationLink> getPaginationLinks() {
        List<PaginationLink> paginationLinks = new List<PaginationLink>();

        //create the prev link if we're not on the first page
        if(getCurrentPageNumber() != 1){
            paginationLinks.add(new PaginationLink('&laquo; Prev', getCurrentPageNumber() - 1, getCurrentPageNumber()));
        }

        // always create the first page
        paginationLinks.add(new PaginationLink('1', 1, getCurrentPageNumber()));

        //create if the current page is not within the limit from 1
        if(getCurrentPageNumber() > PAGINATION_NUMBER_OF_LINKED_PAGES+1){
            paginationLinks.add(new PaginationLink('...', null, getCurrentPageNumber()));
        }

        for(Integer i=2; i<getNumberOfPages(); i++){
            //create if within limit the current page
            if(i > getCurrentPageNumber()-PAGINATION_NUMBER_OF_LINKED_PAGES && i < getCurrentPageNumber()+PAGINATION_NUMBER_OF_LINKED_PAGES){
                paginationLinks.add(new PaginationLink(String.valueOf(i), i, getCurrentPageNumber()));
            }
        }

        //create if the current page is not within the limit from the total number of pages
        if(getCurrentPageNumber() < (getNumberOfPages()-PAGINATION_NUMBER_OF_LINKED_PAGES)){
            paginationLinks.add(new PaginationLink('...', null, getCurrentPageNumber()));
        }

        //create the last page if there is more the one
        if(getNumberOfPages() > 1){
            paginationLinks.add(new PaginationLink(String.valueOf(getNumberOfPages()), getNumberOfPages(), getCurrentPageNumber()));
        }

        // create the next link if we're not on the last page
        if(getCurrentPageNumber() != getNumberOfPages()){
            paginationLinks.add(new PaginationLink('Next &raquo;', getCurrentPageNumber() + 1, getCurrentPageNumber()));
        }

        return paginationLinks;
    }

    public Integer getCurrentPageNumber() {
        if (this.pageNumber == null || this.pageNumber < 1) {
            this.pageNumber = 1;
        }
        return this.pageNumber;
    }

    public Integer getNumberOfArticles() {
        if (this.numberOfArticles == null) {
            this.numberOfArticles = AMSKnowledgeService.countMatchingArticles(this.searchString, this.searchedArticleTypes);
        }
        return this.numberOfArticles;
    }

    private Integer getNumberOfPages() {
        if(this.numberOfPages == null){
            this.numberOfPages = (this.numberOfArticles / PAGE_SIZE);
            if (Math.mod(this.numberOfArticles, PAGE_SIZE) != 0) {
                this.numberOfPages++;
            }
            this.numberOfPages = this.numberOfPages < 1 ? 1 : this.numberOfPages;
        }
        return this.numberOfPages;
    }

    // wrapper for the pagination links
    public class PaginationLink{
        public String label {get;set;}
        public Integer pageNumber {get;set;}

        public PaginationLink(String label, Integer pageNumber, Integer currentPage){
            this.label = label;
            if (pageNumber != currentPage) {
                this.pageNumber = pageNumber;
            }
        }

        public Boolean getShowAsLink(){
            return this.pageNumber != null;
        }
    }

    public static String getArticleParameterName() {
        return AMSCommunityUtil.getArticleParameterName();
    }

    public static String getSearchParameterName() {
        return AMSCommunityUtil.getSearchParameterName();
    }

    public static String getFAQParameterName() {
        return AMSCommunityUtil.getFAQParameterName();
    }
}