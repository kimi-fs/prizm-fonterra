/**
 * Description: Test class for TestAMSMilkQualityTriggerHandler
 * @author: John (Trineo)
 * @date: December 2017
 */
@isTest
private class AMSMilkQualityTriggerHandlerTest {

    @TestSetup
    static void integrationUserSetup() {
        //Create a integration user
        TestClassDataUtil.individualDefaultAccountAu();
        TestClassDataUtil.integrationUserProfile();
    }

    @IsTest
    private static void testSend() {
        List<Channel_Preference_Setting__c> channelPreferenceSettings = TestClassDataUtil.createAuChannelPreferenceSettings();
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
        User integrationUser = TestClassDataUtil.createIntegrationUser();

        System.runAs(integrationUser) {

            List<Account> farms = [
                    SELECT Id
                    FROM Account
                    WHERE Id IN (
                            SELECT Farm__c
                            FROM Individual_Relationship__c
                            WHERE Individual__c = :communityUser.ContactId
                            AND Active__c = true
                    )
            ];

            Collection__c collection = TestClassDataUtil.createSingleCollection(farms[0], false);
            collection.Pick_Up_Date__c = Date.today();
            insert (collection);

            Milk_Quality__c milkQuality = TestClassDataUtil.createMilkQuality(collection, false);
            milkQuality.Milk_Quality_ID__c = 'AU_Test_MQ';
            milkQuality.Result_Interpretation__c = 'Alert';
            milkQuality.Test_Type_Long_Description__c = GlobalUtility.COLLECTION_PERIOD_THERMODURICS_TEST_NAME;
            milkQuality.Farm_No__c = farms[0].Id;
            insert (milkQuality);
        }
    }
}