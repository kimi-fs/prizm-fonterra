/**
 * AMSMilkCollectionController
 *
 * Test the AMSMilkCollectionController
 *
 * Author: Ed (Trineo)
 * Date: Aug 2017
 */
@isTest
private class AMSMilkCollectionControllerTest {

	@testSetup
    static void testSetup() {
        // Prereqs
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.createIntegrationUser();

        // Setup some web text articles, our phone numbers are in there.
        SetupAMSWebText.createWebText();
        SetupAMSCommunityPageMessages.createPageMessages();
    }

    @isTest
    static void testPage() {
        User communityUser;

        User integrationUser = TestClassDataUtil.createIntegrationUser();

        System.runAs(integrationUser) {

            Contact theIndividual = TestClassDataUtil.createIndividualAU(false);
            communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual('test@example.com', theIndividual);

            Account party = TestClassDataUtil.createSingleAccountPartyAU();

            List<Reference_Region__c> auReferenceRegions = TestClassDataUtil.createAuReferenceRegions();
            insert auReferenceRegions;
            List<Account> farms = TestClassDataUtil.createAuFarms(auReferenceRegions);
            insert farms;

            // Now connect this individual to each of the farms that we created via an IR.
            TestClassDataUtil.createPrimaryAccessRelationships(theIndividual.Id, party, farms);
        }

        System.runAs(communityUser) {
            Test.startTest();

            AMSMilkCollectionController controller = new AMSMilkCollectionController();

            System.assertEquals(4, controller.farmWrappers.size());

            for (AMSMilkCollectionController.FarmWrapper wrapper : controller.farmWrappers) {
                System.assertNotEquals('NA', wrapper.milkCollectionNumberDisplay);
            }

            controller.farmWrappers[0].farm.Milking_Frequency_List__c = 'Once Daily';
            controller.save();
            System.assertEquals('Once Daily', controller.farmWrappers[0].farm.Milking_Frequency_List__c);
            controller.cancel();
            System.assertEquals('Once Daily', controller.farmWrappers[0].farm.Milking_Frequency_List__c);
            controller.farmWrappers[0].farm.Milking_Frequency_List__c = 'Twice Daily';
            System.assertEquals('Twice Daily', controller.farmWrappers[0].farm.Milking_Frequency_List__c);
            controller.cancel();
            System.assertEquals('Once Daily', controller.farmWrappers[0].farm.Milking_Frequency_List__c);
            Test.stopTest();
        }


    }
}