/**
 * Description: Test for AMSFarmNamesController
 *
 * @author: John Au (Trineo)
 * @date: October 2018
 */
@IsTest
private class AMSFarmNamesControllerTest {

    private static final String testUserEmail = 'bubbasuperduperfakemail@fonterra.com';

    @TestSetup
    private static void setup() {
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.integrationUserProfile();

        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(testUserEmail);

        Map<String, Account> farms = new Map<String, Account>();
        farms.put('Farm1', new Account( Name = 'Farm1',
                                        RecordTypeId = GlobalUtility.auAccountFarmRecordTypeId,
                                        Primary_Business_Contact__c = communityUser.ContactId,
                                        Primary_On_Farm_Contact__c = communityUser.ContactId));
        farms.put('Farm2', new Account( Name = 'Farm2',
                                        RecordTypeId = GlobalUtility.auAccountFarmRecordTypeId,
                                        Primary_Business_Contact__c = communityUser.ContactId,
                                        Primary_On_Farm_Contact__c = communityUser.ContactId));
        insert farms.values();

        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        List<Entity_Relationship__c> farmsParty = new List<Entity_Relationship__c>();

        farmsParty.add(TestClassDataUtil.createPartyFarmEntityRelationship(party, farms.get('Farm1'), false));
        farmsParty.add(TestClassDataUtil.createPartyFarmEntityRelationship(party, farms.get('Farm2'), false));

        insert farmsParty;

        List<Individual_Relationship__c> individualRelationships = new List<Individual_Relationship__c>();
        individualRelationships.add(new Individual_Relationship__c( Active__c = true,
                                                                    Individual__c = communityUser.ContactId,
                                                                    Role__c = 'Director',
                                                                    Party__c = party.Id,
                                                                    RecordTypeId = GlobalUtility.individualPartyRecordTypeId));
        insert individualRelationships;

        // We will now have derived relationships with both farms
    }

    @IsTest
    private static void testController() {
        User communityUser = [SELECT Id, ContactId FROM User WHERE Email = :testUserEmail];

        System.runAs(communityUser) {
            AMSFarmNamesController controller = new AMSFarmNamesController();

            System.assertEquals(2, controller.derivedRelationships.size());
        }
    }

}