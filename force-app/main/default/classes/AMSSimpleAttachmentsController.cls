/**
 * AMSSimpleAttachmentsController.cls
 * Description: Controller for the AMSSimpleAttachments component- without sharing because
 * apex:inputFile doesn't support re-renders, and we need to do a lot of dodgy shit to get around that
 *
 * @author: John Au
 * @date: December 2018
 */
public without sharing class AMSSimpleAttachmentsController {

    public List<AttachmentWrapper> attachmentWrappers { get; set; }
    public AttachmentWrapper newAttachmentWrapper { get; set; }
    public final Id parentIdParam {
        get;
        set {
            //one time initialization
            if (this.parentIdParam == null) {
                this.parentIdParam = value;
                refreshAttachments();
            }
        }
    }

    public final String parentPageNameParam { get; set; }
    public final String parentPageParamNameParam  { get; set; }

    public AMSSimpleAttachmentsController() {

    }

    public PageReference insertNewAttachment() {
        newAttachmentWrapper.save();

        refreshAttachments();

        return new PageReference(parentPageNameParam + '?' + parentPageParamNameParam + '=' + parentIdParam);
    }

    private void refreshAttachments() {
        this.newAttachmentWrapper = new AttachmentWrapper(this, parentIdParam);
        this.attachmentWrappers = getAttachmentsForObject(parentIdParam);
    }

    public List<AttachmentWrapper> getAttachmentsForObject(Id objectId) {
        List<AttachmentWrapper> attachmentWrappers = new List<AttachmentWrapper>();
        List<Attachment> attachments = [SELECT Id, ContentType, Description, Name, CreatedBy.Id, CreatedBy.Name, ParentId, CreatedDate FROM Attachment WHERE ParentId = :objectId ORDER BY CreatedDate DESC];

        for (Attachment attachment : attachments) {
            AttachmentWrapper wrapper = new AttachmentWrapper(this, attachment);

            attachmentWrappers.add(wrapper);
        }

        return attachmentWrappers;
    }

    public without sharing class AttachmentWrapper {
        public Id parentId { get; set; }
        public Attachment attachment { get; set; }
        public String formattedCreatedDate { get; set; }

        private AMSSimpleAttachmentsController controller;

        private AttachmentWrapper(AMSSimpleAttachmentsController controller) {
            this.controller = controller;
        }

        public AttachmentWrapper(AMSSimpleAttachmentsController controller, Id parentId) {
            this(controller);

            this.parentId = parentId;
            this.attachment = new Attachment(ParentId = this.parentId);
        }

        public AttachmentWrapper(AMSSimpleAttachmentsController controller, Attachment attachment) {
            this(controller);

            this.attachment = attachment;
            setCreatedDateToUsersTimezone();
        }

        private void setCreatedDateToUsersTimezone() {
            formattedCreatedDate = this.attachment.CreatedDate.format('d MMMMM yyyy h:mma', UserInfo.getTimeZone().getID());
        }

        public Boolean getDeleteAllowed() {
            Boolean deleteAllowed = false;

            if (attachment != null) {
                if (attachment.CreatedBy.Id == UserInfo.getUserId()) {
                    deleteAllowed = true;
                }
            }

            return deleteAllowed;
        }

        public void del() {
            delete (this.attachment);

            controller.refreshAttachments();
        }

        public void save() {
            insert (this.attachment);
        }
    }
}