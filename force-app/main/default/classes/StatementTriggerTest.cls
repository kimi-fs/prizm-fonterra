/**
 * Description:
 * Test for statement notifications
 *
 * @author: John Au (Trineo)
 * @date: August 2018
 */
@IsTest
private class StatementTriggerTest {

    @TestSetup
    static void integrationUserSetup() {
        //Create a integration user
        TestClassDataUtil.individualDefaultAccountAu();
        TestClassDataUtil.integrationUserProfile();
    }

    @IsTest
    private static void testSend() {
        List<Channel_Preference_Setting__c> channelPreferenceSettings = TestClassDataUtil.createAuChannelPreferenceSettings();

        User integrationUser = TestClassDataUtil.createIntegrationUser();
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual('hello@fonterra.com');

        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        List<Individual_Relationship__c> irels = new List<Individual_Relationship__c>();
        irels.add(new Individual_Relationship__c(Individual__c = communityUser.ContactId,
                                                Party__c = party.Id,
                                                RecordTypeId = GlobalUtility.individualPartyRecordTypeId,
                                                Role__c = 'Owner'));

        List<Account> farms = TestClassDataUtil.createAUFarms(1, true);

        Channel_Preference__c channelPreference = new Channel_Preference__c(
            Channel_Preference_Setting__c = channelPreferenceSettings[2].Id,
            NotifyBySMS__c = true,
            DeliveryByEmail__c = true,
            Contact_ID__c = communityUser.ContactId
        );

        List<Contact> contacts = [SELECT Id FROM Contact Where Id = :communityUser.ContactId];

        TestClassDataUtil.createPrimaryAccessRelationships(communityUser.ContactId, party, farms);
        TestClassDataUtil.createIndividualPartyRelationships(contacts, party.Id, true);
        //this test is currently incomplete because the above methods don't correctly create a working relationship?

        List<Statement__c> statements = TestClassDataUtil.createStatements(party, farms, Date.today());
    }
}