global class ScheduleBatchSendSMS implements Database.Batchable<Task>, Database.AllowsCallouts {

    private List<Task> smsTasksToComplete;

    global ScheduleBatchSendSMS(List<Task> smsTasksToComplete) {
        this.smsTasksToComplete = smsTasksToComplete;
    }

    global List<Task> start(Database.BatchableContext batchableContext) {
        return [Select Id, WhoId, WhatId, Subject, Description, SMS_Message_ID__c From Task Where Id In :smsTasksToComplete];
    }

    global void execute(Database.BatchableContext batchableContext, List<Task> tasks) {
        System.assertEquals(tasks.size(), 1);
        //bypass sending updates to aspire
        TriggerHandler.bypass('CaseTrigger');

        List<Id> taskContactIds = new List<Id>();
        Set<String> taskChannelPreferenceSettingNames = new Set<String>();

        for (Task task : tasks) {
            taskContactIds.add(task.WhoId);
            taskChannelPreferenceSettingNames.add(task.Subject);
        }

        Map<Id, Contact> taskContactMap = new Map<Id, Contact>([Select Id, MobilePhone From Contact Where Id In :taskContactIds]);

        List<Channel_Preference_Setting__c> channelPreferenceSettings = [SELECT
                                                                            Id,
                                                                            Name,
                                                                            Exact_Target_Message_Id__c,
                                                                            Exact_Target_Message_Keyword__c,
                                                                            Exact_Target_Time_Zone__c,
                                                                            Exact_Target_Window_Start__c,
                                                                            Exact_Target_Window_End__c,
                                                                            RecordType.DeveloperName
                                                                        FROM
                                                                            Channel_Preference_Setting__c
                                                                        WHERE
                                                                            Name In :taskChannelPreferenceSettingNames];

        Map<String, Channel_Preference_Setting__c> channelPreferenceSettingsByName = new Map<String, Channel_Preference_Setting__c>();

        for (Channel_Preference_Setting__c channelPreferenceSetting : channelPreferenceSettings) {
            channelPreferenceSettingsByName.put(channelPreferenceSetting.Name, channelPreferenceSetting);
        }

        for (Task task : tasks) {
            Contact taskContact = taskContactMap.get(task.WhoId);
            Channel_Preference_Setting__c channelPreferenceSetting = channelPreferenceSettingsByName.get(task.Subject);

            String messageText = task.Description;
            String batchUniqueKey = task.SMS_Message_ID__c;

            MarketingCloudUtil.MessageContactResponse response;

            Boolean australianNotification = channelPreferenceSetting.RecordType.DeveloperName.equals('Australia');

            try {
                response = MarketingCloudUtil.sendSMSRest(
                    new List<Contact> { taskContact },
                    channelPreferenceSetting.Exact_Target_Message_Keyword__c,
                    true,
                    messageText,
                    channelPreferenceSetting.Exact_Target_Message_Id__c,
                    australianNotification,
                    new MarketingCloudUtil.BlackoutWindow(
                        channelPreferenceSetting.Exact_Target_Time_Zone__c,
                        channelPreferenceSetting.Exact_Target_Window_Start__c,
                        channelPreferenceSetting.Exact_Target_Window_End__c
                    )
                );

                if (response.responseCode == 400 || response.tokenId == null) {
                    if(!australianNotification){
                        insert createCaseOnFailure(task, messageText, batchUniqueKey, String.valueOf(response.errors));
                    }
                    task.SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSFAILEDSEND;
                } else {
                    task.SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSSENT;
                    task.SMS_Token__c = response.tokenId;
                }
            } catch (Exception e) {
                Logger.log('MC callout failure', String.valueOf(e) + ' ' + task.Id, Logger.LogType.ERROR);

                if(!australianNotification){
                    insert createCaseOnFailure(task, messageText, batchUniqueKey, String.valueOf(e));
                }
                task.SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSFAILEDSEND;
            }

            task.Status = 'Completed';
        }

        //save any logs created during the send sms process
        Logger.saveLog();

        update (tasks);
    }

    private Case createCaseOnFailure(Task task, String messageText, String batchUniqueKey, String errors) {
        Account farm = [Select Id From Account Where Id = :task.WhatId];
        String messageTypeName = task.Subject;
        Id queueId = [SELECT Id from Group where DeveloperName = 'Outbound_Specialist' LIMIT 1].Id;
        Id rtNameId = MarketingCloudUtil.getCaseRecordTypeByMessageTypeName(messageTypeName);
        String description = 'The following text message could not be sent. Please phone instead.' + MarketingCloudUtil.LINEBREAK +
                        MarketingCloudUtil.MESSAGESTART + messageText + MarketingCloudUtil.MESSAGEEND + MarketingCloudUtil.LINEBREAK +
                        MarketingCloudUtil.BATCHKEYSTART + batchUniqueKey + MarketingCloudUtil.BATCHKEYEND + MarketingCloudUtil.LINEBREAK +
                        MarketingCloudUtil.ERRORSTART + errors + MarketingCloudUtil.ERROREND;

        Case c = MarketingCloudUtil.createCaseOnFailure(
            queueId,
            rtNameId,
            farm.Id,
            task.WhoId,
            description,
            MarketingCloudUtil.getCaseTypeByMessageTypeName(messageTypeName),
            MarketingCloudUtil.getCaseSubTypeByMessageTypeName(messageTypeName)
        );

        return c;
    }

    global void finish(Database.BatchableContext batchableContext) {

    }
}