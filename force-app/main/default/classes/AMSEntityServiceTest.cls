/**
 * Description: Test class for AMSEntityService
 * @author: Amelia (Trineo)
 * @date: September 2017
 */
@isTest
private class AMSEntityServiceTest {

    @testSetup static void testSetup(){
        TestClassDataUtil.createAuFarms(5, true);
    }

    @isTest static void testGetFarms_AccountParam() {
        List<Account> accountsToGet = [SELECT Id FROM Account];

        List<Account> gotFarms = AMSEntityService.getFarms(accountsToGet);

        System.assertEquals(accountsToGet.size(), gotFarms.size(), 'Wrong number of returned farms');

    }

    @isTest static void testGetFarms_IDParam() {
        List<Id> farmIds = new List<Id>();
        for (Account farm : [SELECT Id FROM Account]) {
            farmIds.add(farm.Id);
        }

        List<Account> gotFarms = AMSEntityService.getFarms(farmIds);

        System.assertEquals(farmIds.size(), gotFarms.size(), 'Wrong number of returned farms');
    }

    @isTest static void testGetFarms_NameParam() {
        List<String> farmNames = new List<String>();
        for (Account farm : [SELECT Id, Name FROM Account]) {
            farmNames.add(farm.Name);
        }

        List<Account> gotFarms = AMSEntityService.getFarms(farmNames);

        System.assertEquals(farmNames.size(), gotFarms.size(), 'Wrong number of returned farms');
    }

    @isTest static void testUpdateEntities(){
        String NEW_WEBSITE = 'www.test.com';
        List<Account> accountsToUpdate = [SELECT Id FROM Account];

        for(Account a : accountsToUpdate){
            a.Website = NEW_WEBSITE;
        }

        AMSEntityService.updateEntities(accountsToUpdate);

        for(Account a : accountsToUpdate){
            System.assertEquals(NEW_WEBSITE, a.Website, 'Account not updated');
        }

    }
}