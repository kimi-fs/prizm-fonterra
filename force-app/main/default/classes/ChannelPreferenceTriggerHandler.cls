public class ChannelPreferenceTriggerHandler {
    public static void setUnsubscribeKey(List<Channel_Preference__c> channelPreferences) {
        Set<Id> contactIdSet = new Set<Id>();

        for (Channel_Preference__c cp : channelPreferences) {
            contactIdSet.add(cp.Contact_ID__c);
        }

        Map<Id, Contact> getContactById = new Map<Id, Contact>([SELECT Id, CreatedDate FROM Contact WHERE Id IN: contactIdSet]);

        for (Channel_Preference__c cp : channelPreferences) {
            if (cp.Contact_ID__c != NULL && cp.Channel_Preference_Setting__c != NULL) {
                String unsubscribeKey = String.valueOf(cp.Contact_ID__c) + String.valueOf(cp.Channel_Preference_Setting__c);
                cp.Unsubscribe_Key__c = EncodingUtil.convertToHex(Crypto.generateDigest('SHA1', blob.valueOf(unsubscribeKey)));
            }
        }
    }
}