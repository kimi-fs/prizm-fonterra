/**
* Description: Test for AMSLoginController
* @author: Amelia (Trineo)
* @date: July 2017
*/
@isTest
private class AMSLoginControllerTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';

    private static final Map<String, Community_Page_Messages__c> PAGE_MESSAGES = new Map<String, Community_Page_Messages__c>();
    static {
        String PAGE_NAME = 'AMSLogin';
        PAGE_MESSAGES.put('failed-login', new Community_Page_Messages__c(Name = PAGE_NAME + '4', Page__c = PAGE_NAME, Reason__c = 'failed-login', Message__c = 'failed-login'));
    }

    @testSetup static void testSetup() {
        TestClassDataUtil.createCommunityExtendedSessionCustomSettings();
        upsert PAGE_MESSAGES.values();

        TestClassDataUtil.individualDefaultAccountAU();
    }

    @isTest static void testConstructor() {
        User guestUser = TestClassDataUtil.getAMSGuestUser();
        System.runAs(guestUser) {
            Test.startTest();
            AMSLoginController controller = new AMSLoginController();
            Test.stopTest();
            System.assertEquals('', controller.message);
        }
    }

    @isTest static void testLogin_IncorrectPassword() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        User guestUser = TestClassDataUtil.getAMSGuestUser();
        System.runAs(guestUser) {
            AMSLoginController controller = new AMSLoginController();
            controller.email = communityUser.Email;
            controller.password = '?';
            Test.startTest();
            PageReference pr = controller.login();
            Test.stopTest();

            System.assertEquals(PAGE_MESSAGES.get('failed-login').Message__c, controller.message, 'Page gave wrong error');
            System.assertEquals(null, pr, 'Page should not have redirected');
        }
    }

    @isTest static void testLogin_WrongUsername() {
        User guestUser = TestClassDataUtil.getAMSGuestUser();
        System.runAs(guestUser) {
            AMSLoginController controller = new AMSLoginController();
            controller.email = 'I am not a valid email';
            controller.password = '?';
            Test.startTest();
            PageReference pr = controller.login();
            Test.stopTest();

            System.assertEquals(PAGE_MESSAGES.get('failed-login').Message__c, controller.message, 'Page gave wrong error');
            System.assertEquals(null, pr, 'Page should not have redirected');
        }
    }

    @isTest static void testLogin_Exception() {
        delete Community_Page_Messages__c.getAll().values();

        User guestUser = TestClassDataUtil.getAMSGuestUser();
        System.runAs(guestUser) {
            AMSLoginController controller = new AMSLoginController();
            controller.email = 'I am not a valid email';
            controller.password = '?';
            Test.startTest();
            PageReference pr = controller.login();
            Test.stopTest();

            System.assertNotEquals('', controller.message, 'Page gave no error');
            System.assertEquals(null, pr, 'Page should not have redirected');
        }
    }

    @isTest static void testLogin_Redirect_StartURL() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        PageReference currentPage = Page.AMSLogin;
        currentPage.getParameters().put(AMSCommunityUtil.getStartURLParameterName(), 'newPage');
        Test.setCurrentPage(currentPage);

        User guestUser = TestClassDataUtil.getAMSGuestUser();
        System.runAs(guestUser) {
            AMSLoginController controller = new AMSLoginController();
            controller.email = communityUser.Email;
            controller.password = '?';
            Test.startTest();
            PageReference pr = controller.login();
            Test.stopTest();

            // Nothing to assert, the login function will always return null so we never will get redirected
        }
    }

    @isTest static void testLogin_Redirect_PreviousPageCookie() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        ApexPages.currentPage().setCookies(new Cookie[] {new Cookie(AMSCommunityTemplateController.PREVIOUS_PAGE_PARAMETER, 'newPage', null, 500, true)});

        User guestUser = TestClassDataUtil.getAMSGuestUser();
        System.runAs(guestUser) {
            AMSLoginController controller = new AMSLoginController();
            controller.email = communityUser.Email;
            controller.password = '?';
            Test.startTest();
            PageReference pr = controller.login();
            Test.stopTest();

            // Nothing to assert, the login function will always return null so we never will get redirected
        }
    }

    @IsTest
    private static void testLogin_Redirect_AMSLandingPage() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        update new Contact(Id = communityUser.ContactId, AMS_Landing_Page__c = 'TestPage');

        User guestUser = TestClassDataUtil.getAMSGuestUser();
        System.runAs(guestUser) {
            AMSLoginController controller = new AMSLoginController();
            controller.email = communityUser.Email;
            controller.password = '?';
            Test.startTest();
            PageReference pr = controller.login();
            Test.stopTest();

            // Nothing to assert, the login function will always return null so we never will get redirected
        }
    }

    @isTest static void testLoginIfExtendedSessionExistsFromCookie() {
        Test.setCurrentPage(Page.AMSLogin);

        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        ExtendedSessionService ess = new ExtendedSessionService(AMSLoginController.COMMUNITY_NAME);
        ess.create(communityUser);

        User guestUser = TestClassDataUtil.getAMSGuestUser();
        System.runAs(guestUser) {

            AMSLoginController controller = new AMSLoginController();
            PageReference pr = controller.loginIfExtendedSessionExistsFromCookie();

            System.assertEquals(null, pr);
        }
    }

    @isTest static void testGetSiteUrlPrefix() {
        Test.setCurrentPage(Page.AMSLogin);

        User guestUser = TestClassDataUtil.getAMSGuestUser();
        System.runAs(guestUser) {
            AMSLoginController controller = new AMSLoginController();
            String sitURLPrefix = controller.getSiteUrlPrefix();

            // Network.getNetworkId(); will always be null so nothing to assert
        }
    }

    @isTest static void testIsSandbox() {
        Test.setCurrentPage(Page.AMSLogin);

        User guestUser = TestClassDataUtil.getAMSGuestUser();
        System.runAs(guestUser) {
            AMSLoginController controller = new AMSLoginController();

            System.assertEquals([SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox, controller.isSandbox);
        }
    }

}