/* 
* Class Name   - Prizm_AdvanceApplicationConverter
*
* Description  - Action Class for Prizm_ConvertAdvanceApplication component
*
* Developer(s) - Financial Spectra
*/

public without sharing class Prizm_AdvanceApplicationConverter implements fsCore.ActionProcessor{
    
    final static String ADVANCE_TO_LENDING_APPLICATION_MAPPING_NAME = 'Advance_Lending_Application_Mapping';
    final static String CLASS_NAME = 'Prizm_AdvanceApplicationConverter';
    private String METHOD_NAME;
    
    private fsCore.ActionInput  mInput;
    private fsCore.ActionOutput mOutput;
    List<fsCore.ErrorObject> mErrorObjectList;
    
    private Set<Id> mAdvanceAppWithError;
    private List<String> mErrorsList;
    private Map<Id, Advance_Application__c> mAdvanceApplicationMap;
    private Map<String, fsCore__Branch_Setup__c> mBranchMap;
    private Map<String, fsCore__Product_Setup__c> mProductMap;
    private Map<Id, fsCore__Contract_Template_Setup__c> mTemplateMap;
    private Map<Id, fsCore__Pricing_Setup__c> mPricingMap;
    private Map<Id, fsCore__Lending_Application__c> mIdToLendingApplicationMap;
    private Map<Id, Account> mAccountMap;
    private Map<String, String> mCustomAttributeMap;
    private Set<String> mBranchNames;
    private Set<Id> mAccountIds;
    private Map<Id, List<fsCore__Index_Rate_Setup__c>> mIndexToIndexRateMap;
    private Set<Id> mInsertedAppIds;
    private List<fsCore__Lending_Application__c> mInsertedAppList;
    private Set<String> mProductCodes;
    private Set<Id> mContractTemplateIds;
    private Set<Id> mIndexIds;
    private Set<Id> mPricingIds;
    
    public Prizm_AdvanceApplicationConverter(){
        init();
    }
    
    private void init(){
        mInput = new fsCore.ActionInput();
        mOutput = new fsCore.ActionOutput();
        mErrorObjectList = new List<fsCore.ErrorObject>();
        mAdvanceAppWithError = new Set<Id>();
        mErrorsList = new List<String>();
        mBranchMap = new Map<String, fsCore__Branch_Setup__c>();
        mProductMap = new Map<String, fsCore__Product_Setup__c>();
        mTemplateMap = new Map<Id, fsCore__Contract_Template_Setup__c>();
        mPricingMap = new Map<Id, fsCore__Pricing_Setup__c>();
        mAdvanceApplicationMap = new Map<Id, Advance_Application__c>();
        mIdToLendingApplicationMap = new Map<Id, fsCore__Lending_Application__c>();
        mAccountIds = new Set<Id>();
        mAccountMap = new Map<Id, Account>();  
        mCustomAttributeMap = new Map<String, String>();
        mBranchNames = new Set<String>();
        mProductCodes = new Set<String>();
        mContractTemplateIds = new Set<Id>();
        mIndexIds = new Set<Id>();
        mPricingIds = new Set<Id>();
        mIndexToIndexRateMap = new Map<Id, List<fsCore__Index_Rate_Setup__c>>();
        mInsertedAppIds = new Set<Id>();
        mInsertedAppList = new List<fsCore__Lending_Application__c>();
    }
    
    public void setInput(fsCore.ActionInput pInput){
        mInput = pInput;
    }
    
    public fsCore.ActionOutput getOutput(){
        return mOutput;
    }
    
    public void process(){
        try{
            populateData();
            convertToLendingApplication();
            updateAdvanceAppWithLendingApp();
            createItemizations();
            createRepayments();
        }catch(Exception e){
            System.debug(e.getMessage());
            mErrorObjectList.add(Prizm_CommonUtil.getErrorObject(e));
        }
        if(!mErrorObjectList.isEmpty()){
            Prizm_CommonUtil.insertErrorLog(mErrorObjectList, CLASS_NAME, METHOD_NAME);
        }
    }
    
    private void populateData(){
        
        METHOD_NAME = 'populateData';
        
        mAdvanceApplicationMap = Prizm_AdvanceAppConversionUtil.getAdvanceApplicationMap(mInput.getRecords());
        mCustomAttributeMap = Prizm_CommonUtil.getCustomAttributesMap();
            
        for(Advance_Application__c adApp : mAdvanceApplicationMap.values()){
            mAccountIds.add((Id)adApp.Farm_Account_Id__c);
            mProductCodes.add(adApp.Product_Code__c);
        }
        
        mAccountMap = Prizm_CommonUtil.getAccounts(mAccountIds);
        
        for(Account acc : mAccountMap.values()){
            mBranchNames.add(acc.Farm_Region_Name__c);
            system.debug(loggingLevel.ERROR, acc);
            system.debug(loggingLevel.ERROR, acc.Farm_Region_Name__c);
        } 
        
        system.debug(loggingLevel.ERROR, mBranchNames);
        mBranchMap = Prizm_CommonUtil.getBranches(mBranchNames);
        mProductMap = Prizm_CommonUtil.getProducts(mProductCodes);
        
        for(fsCore__Product_Setup__c prod : mProductMap.values()){
            mContractTemplateIds.add(prod.Default_Contract_Template__c);
            mIndexIds.add(prod.Default_Index_Setup__c);
            mPricingIds.add(prod.Default_Pricing_Setup__c);
        }
        
        mTemplateMap = Prizm_CommonUtil.getContractTemplates(mContractTemplateIds);
        mIndexToIndexRateMap = Prizm_CommonUtil.getIndexRates(mIndexIds);
        mPricingMap = Prizm_CommonUtil.getPricings(mPricingIds);
    }
    
    private void convertToLendingApplication(){
        
        METHOD_NAME = 'convertToLendingApplication';
        
        List<fsCore__Lending_Application__c> appList = new List<fsCore__Lending_Application__c>();
        
        fsCore.ObjectRecordMapper objectMapper = new fsCore.ObjectRecordMapper(ADVANCE_TO_LENDING_APPLICATION_MAPPING_NAME);
        Map<Integer, String> defaultMonthNumberToFieldMap = Prizm_AdvanceAppConversionUtil.defaultMonthNumberToFieldMap();
        Decimal totalAmount = 0;
        
        for(Advance_Application__c adApp : mAdvanceApplicationMap.values()){
            
            totalAmount = Prizm_AdvanceAppConversionUtil.getSumRepaymentsAmount(adApp, defaultMonthNumberToFieldMap);
                
            if(adApp.Is_Converted_To_Lending_Application__c == true){
                system.debug(loggingLevel.ERROR, 'App already created');
                addError(adApp.id, Label.Lending_Application_Already_Converted);
                break;
            }
            
            if(mCustomAttributeMap.containsKey('New_Advance_Application')){
                if(adApp.Type__c != mCustomAttributeMap.get('New_Advance_Application')){
                    system.debug(loggingLevel.ERROR, 'Advance Application not of "New RABO Advance" type');
                    addError(adApp.id, Label.Lending_Application_Conversion_Not_Eligible + adApp.Type__c);
                    break;
                }
            }
            
            if(adApp.Current_RABO_Advances__c != totalAmount){
                system.debug(loggingLevel.ERROR, 'Loan Amount not equal to sum of All Payments');
                addError(adApp.id, Label.Incorrect_Loan_Amount);
                break;
            }
            
            system.debug(loggingLevel.ERROR, 'outside if for already created check');
            
            fsCore__Branch_Setup__c branch = new fsCore__Branch_Setup__c();
            fsCore__Product_Setup__c product = new fsCore__Product_Setup__c();
            fsCore__Contract_Template_Setup__c contractTemp = new fsCore__Contract_Template_Setup__c();
            fsCore__Pricing_Setup__c pricing = new fsCore__Pricing_Setup__c();
            
            List<fsCore__Index_Rate_Setup__c> indexRateSetUpList = new List<fsCore__Index_Rate_Setup__c>();
            
            if(mAccountMap.containsKey((Id)adApp.Farm_Account_Id__c)){
                branch = mBranchMap.get(mAccountMap.get((Id)adApp.Farm_Account_Id__c).Farm_Region_Name__c);
            }
            
            system.debug(loggingLevel.ERROR, mAccountMap);
            system.debug(loggingLevel.ERROR, mAccountMap.get((Id)adApp.Farm_Account_Id__c));
            system.debug(loggingLevel.ERROR, mAccountMap.get((Id)adApp.Farm_Account_Id__c).Farm_Region_Name__c);
            system.debug(loggingLevel.ERROR, mBranchMap);
            system.debug(loggingLevel.ERROR, mProductMap);
            
            product = mProductMap.get(adApp.Product_Code__c);
            contractTemp = mTemplateMap.get(product.Default_Contract_Template__c);
            pricing = mPricingMap.get(product.Default_Pricing_Setup__c);
            
            system.debug(loggingLevel.ERROR, product);
            system.debug(loggingLevel.ERROR,'Contract Template : '+ contractTemp);
            system.debug(loggingLevel.ERROR,'New Contract Template : '+ contractTemp.id);
            system.debug(loggingLevel.ERROR, product.Default_Index_Setup__c);
            system.debug(loggingLevel.ERROR, contractTemp.fsCore__Index_Name__c);
            
            if(mIndexToIndexRateMap.containsKey(product.Default_Index_Setup__c)){
                indexRateSetUpList = mIndexToIndexRateMap.get(product.Default_Index_Setup__c);
                system.debug(loggingLevel.ERROR, 'indexRateSetUpList---- '+indexRateSetUpList);
            }
            
            
            system.debug(loggingLevel.ERROR, indexRateSetUpList);
            
            fsCore__Lending_Application__c app = (fsCore__Lending_Application__c)objectMapper.getTargetRecord(adApp);
            
            app.fsCore__Primary_Customer_Contact__c = mAccountMap.get((Id)adApp.Farm_Account_Id__c).Primary_On_Farm_Contact__c;
            app.fsCore__Branch_Name__c = branch.id;
            system.debug(loggingLevel.ERROR, '1');
            app.fsCore__Company_Name__c = branch.fsCore__Company_Name__c;
            app.fsCore__Application_Date__c = branch.fsCore__Business_Date__c;
            app.fsCore__Application_Status__c = fsCore.Constants.STATUS_NEW;
            system.debug(loggingLevel.ERROR, '2');
            app.fsCore__Product_Name__c = product.id;
            app.fsCore__Product_Family__c = product.fsCore__Product_Family__c;
            app.fsCore__Collateral_Family__c = product.fsCore__Collateral_Family__c;
            system.debug(loggingLevel.ERROR, '3');
            app.fsCore__Is_Secured__c = product.fsCore__Is_Secured__c;
            app.fsCore__Is_Syndication_Allowed__c = product.fsCore__Is_Syndication_Allowed__c;
            app.fsCore__Is_Payment_Cycle_Flexible__c = product.fsCore__Is_Billing_Cycle_Flexible__c;
            app.fsCore__Is_Funding_In_Tranches_Allowed__c = product.fsCore__Is_Funding_In_Tranches_Allowed__c;
            app.fsCore__Is_Assign_Using_First_Stage_Setup__c = true;
            app.fsCore__Requested_Number_Of_Payments__c = pricing.fsCore__Number_Of_Payments__c; 
            app.fsCore__Application_Received_Date__c = branch.fsCore__Business_Date__c;
            app.fsCore__Application_Date__c =  branch.fsCore__Business_Date__c; 
            system.debug(loggingLevel.ERROR, '4');
            app.fsCore__Number_Of_Payments__c = pricing.fsCore__Number_Of_Payments__c;
            app.fsCore__Is_Due_Day_End_Of_Month__c = true;
            app.fsCore__Maturity_Date__c = null;
            app.fsCore__Payment_Start_Date__c = branch.fsCore__Business_Date__c.addMonths(1).toStartofMonth().addDays(-1);
            app.fsCore__Payment_Cycle__c = pricing.fsCore__Payment_Cycle__c;
            app.fsCore__Margin_Rate__c = 0;
            app.fsCore__Rate_Index_Name__c = pricing.fsCore__Index_Name__c;
            app.fsCore__Contract_Date__c = branch.fsCore__Business_Date__c;
            app.fsCore__Interest_Accrual_Start_Date__c = branch.fsCore__Business_Date__c;
            app.fsCore__Parameter_Calculated__c = pricing.fsCore__Default_Calculate_Option__c;
            
            
            if(indexRateSetUpList.size()>0)
            {
                System.debug(loggingLevel.ERROR,'Inside if');
                for(fsCore__Index_Rate_Setup__c indexRate: indexRateSetUpList)
                {
                    if(app.fsCore__Contract_Date__c>= indexRate.fsCore__Start_Date__c && app.fsCore__Contract_Date__c <= indexRate.fsCore__End_Date__c)
                    {
                        app.fsCore__Rate__c = indexRate.fsCore__Index_Rate__c;
                    }
                }
            }
            else
            {
                addError(null, 'Index Rate List is Null') ;
                System.debug(loggingLevel.ERROR,'Inside else');
            }
            
            system.debug(loggingLevel.ERROR, 'before insert app '+app);
            System.debug(loggingLevel.ERROR, app.fsCore__Contract_Template_Name__c+'Contract template name');
            appList.add(app);
        }
        
        if(!appList.isEmpty()){
            //insert appList;
            fsCore.DMLResult lendingAppDMLRslt = fsCore.LendingApplicationDMLWrapper.getInstance().insertData(appList);
            if (lendingAppDMLRslt.hasErrors()) lendingAppDMLRslt.throwError();
            
            for(fsCore__Lending_Application__c app : appList){
                mInsertedAppIds.add(app.Id);
            }
            
            fsCore.DynamicQueryBuilder applicationQuery = fsCore.DynamicQueryFactory.createQuery(Schema.SObjectType.fsCore__Lending_Application__c.getName())
                .addFields()
                .addField('CreatedById')
                .addField('CreatedDate')
                .addField('fsCore__Contract_Template_Name__r.Name')
                .addWhereConditionWithBind(1,'Id','IN','mInsertedAppIds');
            
            mInsertedAppList = (List<fsCore__Lending_Application__c>)Database.query(applicationQuery.getQueryString());
            System.debug(loggingLevel.ERROR,'mInsertedAppList:------'+mInsertedAppList);  
            for(fsCore__Lending_Application__c app : mInsertedAppList)
            {
                System.debug(loggingLevel.ERROR,'mInsertedAppList Template:------'+app.fsCore__Contract_Template_Name__c);
                System.debug(loggingLevel.ERROR,'mInsertedAppList Template:------'+app.fsCore__Contract_Template_Name__r.Name);
            }
            
            fsCore.LendingApplicationTriggerWrapper.getInstance().reset();
            system.debug(loggingLevel.ERROR, 'Lending Application created successfully');
        }
        
        if(!mInsertedAppList.isEmpty()){
            for(fsCore__Lending_Application__c app : mInsertedAppList){
                mIdToLendingApplicationMap.put(app.Id, app);
            }
        }
    }
    
    private void updateAdvanceAppWithLendingApp(){
        
        METHOD_NAME = 'updateAdvanceAppWithLendingApp';
        
        if(!mInsertedAppList.isEmpty()){
            List<Advance_Application__c> adAppList = new List<Advance_Application__c>();
            for(fsCore__Lending_Application__c app : mInsertedAppList){
                if(mAdvanceApplicationMap.containsKey(app.Advance_Application__c)){
                    Advance_Application__c adApp = mAdvanceApplicationMap.get(app.Advance_Application__c);
                    adApp.Is_Converted_To_Lending_Application__c = true;
                    adApp.Converted_By__c = app.CreatedById;
                    adApp.Conversion_Timestamp__c = app.CreatedDate;
                    adAppList.add(adApp);
                } 
            }
            if(!adAppList.isEmpty()){
                update adAppList;
            }
        }
    }
    
    private void createItemizations(){
        
        METHOD_NAME = 'createItemizations';
        
        if(!mIdToLendingApplicationMap.isEmpty() && mErrorsList.isEmpty()){
            fsCore__Product_Setup__c product = new fsCore__Product_Setup__c();
            List<fsCore__Lending_Application_Itemization__c> appItemizations = new List<fsCore__Lending_Application_Itemization__c>();
            
            for(fsCore__Lending_Application__c app : mIdToLendingApplicationMap.values()){
                
                system.debug(loggingLevel.ERROR, '5');
                
                if(mProductMap.containsKey(mAdvanceApplicationMap.get(app.Advance_Application__c).Product_Code__c)){
                    product = mProductMap.get(mAdvanceApplicationMap.get(app.Advance_Application__c).Product_Code__c);
                }
                else{
                    addError(app.Advance_Application__c ,'Product does not exist on Application');
                }
                
                system.debug(loggingLevel.ERROR, 'mProductMap ' +mProductMap);
                system.debug(loggingLevel.ERROR, '6 ');
                system.debug(loggingLevel.ERROR, 'product ' +product);
                
                for(fsCore__Product_Itemization_Setup__c prodItem : product.fsCore__Product_Itemization_Setup__r){
                    
                    fsCore__Lending_Application_Itemization__c appItem = Prizm_AdvanceAppConversionUtil.populateItemization(app, prodItem);
                    system.debug(loggingLevel.ERROR, '7');
                    
                    appItemizations.add(appItem);
                }
                system.debug(loggingLevel.ERROR, '8');
            }
            
            system.debug(loggingLevel.ERROR, '9');
            if(!appItemizations.isEmpty()){
                Insert appItemizations;
                system.debug(loggingLevel.ERROR, 'Application Itemizations created successfully: '+appItemizations);
            }                    
        }
    }       
    
    private void createRepayments(){
        
        METHOD_NAME = 'createRepayments';
        
        if(!mIdToLendingApplicationMap.isEmpty() && mErrorsList.isEmpty()){
            
            system.debug(loggingLevel.ERROR, '10');
            
            Map<Id, List<fsCore__Lending_Application_Repayment_Schedule__c>> appIdToPredefinedPmtsListMap = new Map<Id , List<fsCore__Lending_Application_Repayment_Schedule__c>>();
            Map<integer, string> monthNumberToMonthFieldMap = new Map<integer, string>();
            List<fsCore__Lending_Application_Repayment__c> repaymentDtlList = new List<fsCore__Lending_Application_Repayment__c>();
            List<fsCore__Lending_Application_Repayment_Schedule__c> repayScheduleList = new List<fsCore__Lending_Application_Repayment_Schedule__c>();
            system.debug(loggingLevel.ERROR, '11');
            
            Map<Integer, String> defaultMonthNumberToFieldMap = Prizm_AdvanceAppConversionUtil.defaultMonthNumberToFieldMap();
            
            for(fsCore__Lending_Application__c app : mIdToLendingApplicationMap.values()){
                system.debug(loggingLevel.ERROR, '12');  
                monthNumberToMonthFieldMap = Prizm_AdvanceAppConversionUtil.getMonthNumberToMonthFieldMap(app, defaultMonthNumberToFieldMap);
                system.debug(loggingLevel.ERROR, '13 '+monthNumberToMonthFieldMap);
            }
            
            system.debug(loggingLevel.ERROR, '14');
            
            for(fsCore__Lending_Application__c app : mIdToLendingApplicationMap.values()){
                
                Advance_Application__c adApp = mAdvanceApplicationMap.get(app.Advance_Application__c);
                List<fsCore__Lending_Application_Repayment_Schedule__c> predefinedPmts = Prizm_AdvanceAppConversionUtil.getPredefinedPayments(app, adApp, monthNumberToMonthFieldMap);
                
                appIdToPredefinedPmtsListMap.put(app.Id, predefinedPmts);
            }
            
            system.debug(loggingLevel.ERROR, '15 '+appIdToPredefinedPmtsListMap);
            for(Id appId : appIdToPredefinedPmtsListMap.keyset()){
                for(fsCore__Lending_Application_Repayment_Schedule__c pay : appIdToPredefinedPmtsListMap.get(appId)){
                    system.debug(loggingLevel.ERROR, '16 '+pay);
                }
            }
            
            List<fsCore__Lending_Application__c> updateApps = new List<fsCore__Lending_Application__c>();
            system.debug(loggingLevel.ERROR, appIdToPredefinedPmtsListMap);
            for(Id appId : appIdToPredefinedPmtsListMap.keyset()){
                
                system.debug(loggingLevel.ERROR, '17');
                fsCore__Lending_Application__c mApplication = mIdToLendingApplicationMap.get(appId);
                
                
                System.debug(loggingLevel.ERROR, 'application : '+ mIdToLendingApplicationMap.get(appId));
                System.debug(loggingLevel.ERROR, 'application : '+ mIdToLendingApplicationMap.get(appId).fsCore__Contract_Template_Name__c);
                System.debug(loggingLevel.ERROR, 'application contract Billing_Method__c : '+ mIdToLendingApplicationMap.get(appId).fsCore__Billing_Method__c);
                fsCore.CalculatorObject calcObj = getCalculatorObject(mIdToLendingApplicationMap.get(appId), appIdToPredefinedPmtsListMap.get(appId));
                
                System.debug(loggingLevel.ERROR, 'Calculator Object : '+ calcObj);
                
                calcObj = fsCore.Calculator.calculate(calcObj);
                
                system.debug(loggingLevel.ERROR, '18');
                system.debug(loggingLevel.ERROR, mIdToLendingApplicationMap.get(appId).Advance_Application__c);
                
                if (!calcObj.getIsSuccess()){
                    system.debug(loggingLevel.ERROR, '19');
                    
                    for (String errMsg : calcObj.getErrorStack()){
                        addError(mIdToLendingApplicationMap.get(appId).Advance_Application__c, errMsg); 
                    }
                }
                
                system.debug(loggingLevel.ERROR, '20');
                
                fsCore.LendingApplicationCalculatorExtractor calcExtract = new fsCore.LendingApplicationCalculatorExtractor(calcObj, mApplication);
                system.debug(loggingLevel.ERROR, 'calcExtract = '+calcExtract);
                mApplication = calcExtract.getApplicationDetails();
                repaymentDtlList = calcExtract.getRepaymentDetails();
                repayScheduleList = calcExtract.getRepaymentSchedule();
                system.debug(loggingLevel.ERROR, '21');
                
                system.debug(loggingLevel.ERROR, 'repayScheduleList[0]'+repayScheduleList[0]);
                
                if(repayScheduleList[0].fsCore__Payment_Amount__c != null){
                    mApplication.fsCore__Payment_Amount__c = repayScheduleList[0].fsCore__Payment_Amount__c;
                    system.debug(loggingLevel.ERROR, 'repayScheduleList[0].fsCore__Payment_Amount__c '+repayScheduleList[0].fsCore__Payment_Amount__c);
                    updateApps.add(mApplication);
                }
                
                system.debug(loggingLevel.ERROR, 'fsCore__Payment_Amount__c '+mApplication.fsCore__Payment_Amount__c);
                for(fsCore__Lending_Application_Repayment_Schedule__c pay : appIdToPredefinedPmtsListMap.get(appId)){
                    system.debug(loggingLevel.ERROR, 'pay '+pay);
                }
            }
            
            system.debug(loggingLevel.ERROR, updateApps);
            system.debug(loggingLevel.ERROR, repaymentDtlList);
            system.debug(loggingLevel.ERROR, repayScheduleList);
            
            if(!updateApps.isEmpty()){
                system.debug(loggingLevel.ERROR, updateApps);
                update updateApps;
                fsCore.LendingApplicationTriggerWrapper.getInstance().reset();
            }   
            if(!repaymentDtlList.isEmpty()){
                system.debug(loggingLevel.ERROR, '22');
                insert repaymentDtlList;
            }
            if(!repayScheduleList.isEmpty()){
                insert repayScheduleList;
                system.debug(loggingLevel.ERROR, '23');
            }
        }
    }
    
    
    private fsCore.CalculatorObject getCalculatorObject(fsCore__Lending_Application__c mApplication, List<fsCore__Lending_Application_Repayment_Schedule__c> mPredefinedPmts){
        
        METHOD_NAME = 'getCalculatorObject';
        
        System.debug(loggingLevel.ERROR, 'Application.... : '+mApplication);
        System.debug(loggingLevel.ERROR, 'repayments : '+ mPredefinedPmts);
        fsCore.LendingApplicationCalculatorBuilder calcBuilder = new fsCore.LendingApplicationCalculatorBuilder(mApplication)
            .setCalculator(mApplication.fsCore__Parameter_Calculated__c)
            .setInterestStartDate(mApplication.fsCore__Interest_Accrual_Start_Date__c)
            .setPredefinedPayments(mPredefinedPmts);
        
        system.debug(LoggingLEVEL.ERROR, calcBuilder.getCalculatorObject());
        return calcBuilder.getCalculatorObject();
    }
    
    private void addError(Id pErrRecordId, String pErrMsg){
        if (pErrRecordId != null && pErrRecordId.getSobjectType().getDescribe().getName() == 'Advance_Application__c'){
            mAdvanceAppWithError.add(pErrRecordId);
        }
        mErrorsList.add(pErrMsg);
        fsCore.ErrorObject errObj = new fsCore.ErrorObject();
        if(pErrRecordId != null){
            errObj.setErrorRecordId(pErrRecordId);
        } 
        errObj.setErrorMessage(pErrMsg);
        mOutput.addError(errObj);
    }
}