/**
 * Testing services for SF Files, including secure fetching of the file contents for community downloads
 *
 * @author Ed (Trineo)
 * @date   May 2020
 */
@isTest
private class FileServiceTest {
    private static String USER_EMAIL = 'amsUser@thisTest.com';
    private static String COMMUNITY_LASTNAME = 'Testy';

    @TestSetup
    private static void userSetup() {
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
    }

    @isTest
    private static void testFetchAgreementDocumentContents() {
        User communityUser = getCommunityUser();

        // One Party
        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        // 3 Farms
        List<Account> farms = TestClassDataUtil.createAUFarms(1, true);

        // Connect the party with the 3 farms by ERs
        List<Entity_Relationship__c> entityRelationships = TestClassDataUtil.createPartyFarmEntityRelationships(new Map<Account, List<Account>> { party => farms }, true);

        // Connect the Farmer with the Party - which will create derived IRs between the individual and the farms.
        List<Individual_Relationship__c> individualRelationships = TestClassDataUtil.createIndividualPartyRelationships(communityUser.ContactId, new List<Account> { party }, true);

        /**
         * Since SF doesn't allow insert dml on ContentDocument, we're creating it via
         * ContentVersion without ContentDocumentId
         */
        ContentVersion pdfContentDocument = new ContentVersion(
            Title = 'New_Group-000001_V1',
            PathOnClient = '/New_Group-000001_V1.pdf',
            VersionData = Blob.valueOf('PDF Content'),
            IsMajorVersion = true
        );

        insert pdfContentDocument;

        List<ContentDocument> documents = [SELECT Id, Title FROM ContentDocument];

        Agreement__c agreement = AgreementService.createPendingOnlineStandardAgreement(farms[0].Id, party.Id, Date.today(), Date.today().addMonths(6));
        insert agreement;

        // Create a link between this document and our Agreement
        ContentDocumentLink agreementContentDocumentLink = new ContentDocumentLink(
            ContentDocumentId = documents[0].Id,
            LinkedEntityId = agreement.Id,
            ShareType = 'V',
            Visibility = 'AllUsers'
        );
        ContentDocumentLink partyContentDocumentLink = new ContentDocumentLink(
            ContentDocumentId = documents[0].Id,
            LinkedEntityId = party.Id,
            ShareType = 'I',
            Visibility = 'AllUsers'
        );

        insert agreementContentDocumentLink;
        insert partyContentDocumentLink;

        System.assertEquals(1, [SELECT count() FROM ContentDocumentLink WHERE LinkedEntityId = : agreement.Id], 'ContentDocumentLink Not Created');

        Test.startTest();

        System.runAs(communityUser) {
            String agreementDocument = FileService.getAgreementDocumentContents(agreementContentDocumentLink.Id);
            System.assertEquals(EncodingUtil.base64Encode(Blob.valueOf('PDF Content')), agreementDocument);

            // Test some of the failure conditions
            try {
                FileService.getAgreementDocumentContents(partyContentDocumentLink.Id);
                System.assert(false, 'Should have thrown an exception, wrong object type');
            } catch (FileService.InvalidAccessException e) { }
        }

        try {
            FileService.getAgreementDocumentContents(agreementContentDocumentLink.Id);
            System.assert(false, 'Should have thrown an exception, not a community user');
        } catch (FileService.InvalidAccessException e) { }

        Test.stopTest();
    }

    private static User getCommunityUser() {
        User theUser = [
            SELECT Id, ContactId
            FROM User
            WHERE Email = : USER_EMAIL
        ];

        return theUser;
    }
}