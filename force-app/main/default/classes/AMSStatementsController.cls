/**
 * Description: Controller for displaying AMSStatements
 * @author: Amelia (Trineo)
 * @date: August 2017
 */
public with sharing class AMSStatementsController {
    private static final RelationshipAccessService.Access ACCESS_REQUIRED = RelationshipAccessService.Access.ACCESS_STATEMENTS;
    private static final Integer YEARS_TO_DISPLAY = 7;

    public String errorMessage { get; set; }
    public String pdfDownloadUrl { get; set; }

    private static Map<String, String> messages;
    static {
        messages = AMSCommunityUtil.initialisePageMessages('AMSStatements');
    }

    public AMSStatementsController() {
        this.accessableERelMap = RelationshipAccessService.accessToEntityRelationships(AMSContactService.determineContactId(), ACCESS_REQUIRED);
        defaultStatementSelection();
    }

    public void getStatementPdf() {
        //PDF url already exists on the page, just reuse it- it gets reset if they change the report
        if (pdfDownloadUrl != null) {
            return;
        }

        pdfDownloadUrl = null;

        try {
            AMSStatementService.SAPPOResponse poResponse = AMSStatementService.getStatementPDF(statement.Id);
            AMSStatementService.MilkCollectionStatementByElementsResponse statementResponse = poResponse.MilkCollectionStatementByElementsResponse;

            if (AMSStatementService.statementExists(poResponse)) {
                pdfDownloadUrl = Site.getBaseUrl() + '/servlet/servlet.FileDownload?file=' + createPdfDocument(statementResponse.PDFContent).Id;
            } else {
                errorMessage = poResponse.Log.Item.Note;
            }
        } catch (Exception e) {
            errorMessage = messages.get('statement-retrieval-error');
        }
    }

    private void defaultStatementSelection() {
        List<SelectOption> partyOptions = getPartyOptions();

        if (!partyOptions.isEmpty()) {
            partySelection = partyOptions[0].getValue();
        }

        List<SelectOption> farmOptions = getFarmOptions();

        if (!farmOptions.isEmpty()) {
            eRelSelection = farmOptions[0].getValue();
        }

        Statement__c latestStatement = AMSStatementService.getLatestStatement(getSelectedERel().Party__c, getSelectedERel().Farm__c);
        if (latestStatement != null) {
            this.monthSelection = monthOptions.get(latestStatement.Date__c.month() - 1).getValue();
            for (SelectOption yearOption: yearOptions) {
                if (yearOption.getValue() == String.valueOf(latestStatement.Date__c.year())) {
                    this.yearSelection = yearOption.getValue();
                    break;
                }
            }
        } else {
            this.monthSelection = monthOptions.get(Integer.valueOf(DateTime.now().addMonths(-1).format('M')) - 1).getValue();
            this.yearSelection = yearOptions[0].getValue();
        }
    }

    /**
     * Creates 'temporary' PDF documents to allow users to download via the fileDownload servlet
     *
     * AMSScheduleBatchStatementCleanup will clean the old ones out
     */
    @TestVisible
    private Document createPdfDocument(String pdfBase64) {
        Document pdfDocument = new Document();

        pdfDocument.Body = EncodingUtil.base64Decode(pdfBase64);
        pdfDocument.ContentType = 'application/pdf';
        pdfDocument.Name = getSelectedPartyName() + '-' + getSelectedFarmName() + '-Financial Statement.pdf';
        pdfDocument.Type = 'pdf';
        pdfDocument.FolderId = UserInfo.getUserId();
        pdfDocument.Keywords = AMSStatementService.PDF_DOCUMENT_KEYWORDS;

        insert pdfDocument;

        return pdfDocument;
    }

    private static final Date TCC_STATEMENTS_AVAILABLE_AFTER {
        get {
            if (TCC_STATEMENTS_AVAILABLE_AFTER == null) {
                AMS_PO_Settings__c poSettings = AMS_PO_Settings__c.getOrgDefaults();

                if (poSettings != null && poSettings.TCC_Statements_Available_After__c != null) {
                    TCC_STATEMENTS_AVAILABLE_AFTER = poSettings.TCC_Statements_Available_After__c;
                } else {
                    TCC_STATEMENTS_AVAILABLE_AFTER = Date.today();
                }
            }

            return TCC_STATEMENTS_AVAILABLE_AFTER;
        }

        private set;
    }

    public Boolean getStatementPdfAvailable() {
        return getStatementExists() && TCC_STATEMENTS_AVAILABLE_AFTER < statement.Date__c;
    }

    public static List<SelectOption> monthOptions { get; set; }
    static {
        monthOptions = new List<SelectOption>{
            new SelectOption('01', '01'),
            new SelectOption('02', '02'),
            new SelectOption('03', '03'),
            new SelectOption('04', '04'),
            new SelectOption('05', '05'),
            new SelectOption('06', '06'),
            new SelectOption('07', '07'),
            new SelectOption('08', '08'),
            new SelectOption('09', '09'),
            new SelectOption('10', '10'),
            new SelectOption('11', '11'),
            new SelectOption('12', '12')
        };
    }
    public static List<SelectOption> yearOptions { get; set; }
    static {
        yearOptions = new List<SelectOption>();
        Integer year = System.Today().year();
        for (Integer i = year; i > year - YEARS_TO_DISPLAY; i--) {
            yearOptions.add(new SelectOption(String.valueOf(i), String.valueOf(i)));

        }
    }

    public String partySelection {get; set;}
    public String eRelSelection {get; set;}

    public String monthSelection {get; set;}
    public String yearSelection {get; set;}

    public Map<String, List<StatementEntryWrapper>> recordTypeStatementEntryMap {get; private set;}

    @testVisible
    private Map<Id, Entity_Relationship__c> accessableERelMap;
    private Map<String, List<Entity_Relationship__c>> partyIdERelMap;
    private List<Statement_Entry__c> statementEntries;
    private Statement__c statement;

    public List<SelectOption> getPartyOptions() {
        Set<SelectOption> partyOptionsSet = new Set<SelectOption>();
        for (Entity_Relationship__c er : accessableERelMap.values()) {
            partyOptionsSet.add(new SelectOption(er.Party__c, er.Party__r.Name));
        }
        List<SelectOption> partyOptionsList = new List<SelectOption>();
        partyOptionsList.addAll(partyOptionsSet);
        return partyOptionsList;
    }

    public List<SelectOption> getFarmOptions() {
        List<SelectOption> farmOptions = new List<SelectOption>();
        if (partySelection != null && getPartyIdERelMap().containsKey(partySelection)) {
            for (Entity_Relationship__c er : getPartyIdERelMap().get(partySelection)) {
                farmOptions.add(new SelectOption(er.Id, er.Farm__r.Name));
            }
        }
        return farmOptions;
    }

    public String getSelectedPartyName() {
        return getSelectedERel().Party__r.Name;
    }
    public String getSelectedFarmName() {
        return getSelectedERel().Farm__r.Name;
    }

    public StatementEntryWrapper getTotalIncome() {
        return getStatementByType('Total Income');
    }
    public StatementEntryWrapper getTotalCharges() {
        return getStatementByType('Total Charges');
    }
    public StatementEntryWrapper getIncomeLessCharges() {
        return getStatementByType('Income Less Charges');
    }
    public StatementEntryWrapper getTotalDeductions() {
        return getStatementByType('Total Deductions');
    }
    public StatementEntryWrapper getNetIncome() {
        return getStatementByType('Net Income/Payable');
    }

    public StatementEntryWrapper getStatementByType(String statementType) {
        StatementEntryWrapper seToReturn;
        if (recordTypeStatementEntryMap.containsKey('Totals')) {
            for (StatementEntryWrapper se : recordTypeStatementEntryMap.get('Totals')) {
                if (se.statementEntry.Type__c.equalsIgnoreCase(statementType)) {
                    seToReturn =  se;
                    break;
                }
            }
        }
        return seToReturn;
    }

    public Boolean getStatementExists() {
        Boolean exists = false;
        if (statement != null) {
            exists = true;
        }
        return exists;
    }

    public void generateReport() {
        Id farmId = getSelectedERel().Farm__c;
        Id partyId = getSelectedERel().Party__c;

        statement = AMSStatementService.getStatement(partyId, farmId, this.monthSelection, this.yearSelection);

        if (statement != null) {
            statementEntries = AMSStatementService.getStatementEntries(statement.Id);

            recordTypeStatementEntryMap = new Map<String, List<StatementEntryWrapper>>();
            recordTypeStatementEntryMap.put('Payment', new List<StatementEntryWrapper>());
            recordTypeStatementEntryMap.put('Charge', new List<StatementEntryWrapper>());
            recordTypeStatementEntryMap.put('Deductions', new List<StatementEntryWrapper>());
            recordTypeStatementEntryMap.put('Total', new List<StatementEntryWrapper>());

            if (statementEntries != null) {
                for (Statement_Entry__c se : statementEntries) {
                    if (!recordTypeStatementEntryMap.containsKey(se.RecordType.Name)) {
                        recordTypeStatementEntryMap.put(se.RecordType.Name, new List<StatementEntryWrapper>());
                    }
                    recordTypeStatementEntryMap.get(se.RecordType.Name).add(new StatementEntryWrapper(se));
                }
            }
        }

        errorMessage = null;
        pdfDownloadUrl = null;
    }

    @TestVisible
    private Map<String, List<Entity_Relationship__c>> getPartyIdERelMap() {
        if (partyIdERelMap == null) {
            partyIdERelMap = new Map<String, List<Entity_Relationship__c>>();
            for (Entity_Relationship__c er : accessableERelMap.values()) {
                if (!partyIdERelMap.containsKey(er.Party__c)) {
                    partyIdERelMap.put(er.Party__c, new List<Entity_Relationship__c>());
                }
                partyIdERelMap.get(er.Party__c).add(er);
            }
        }
        return partyIdERelMap;
    }

    private Entity_Relationship__c getSelectedERel() {
        if (String.isNotBlank(eRelSelection) && accessableERelMap.containsKey(eRelSelection)) {
            return accessableERelMap.get(eRelSelection);
        } else {
            return new Entity_Relationship__c();
        }
    }

    public class StatementEntryWrapper {
        public Statement_Entry__c statementEntry {get; private set;}

        public StatementEntryWrapper(Statement_Entry__c statementEntry) {
            this.statementEntry = statementEntry;
        }

        public String getRates() {
            if (statementEntry.Rate__c >= 1) {
                return '$' + String.valueOf(statementEntry.Rate__c.setScale(2));
            } else {
                return String.valueOf(statementEntry.Rate__c * 100) + 'c';
            }
        }
    }
}