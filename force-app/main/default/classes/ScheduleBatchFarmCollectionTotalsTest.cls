/**
* Description:  Test Class for ScheduleBatchFarmCollectionTotalsTest
* @author Clayde Bael (Trineo)
* @date  11/07/2018
*
**/
@isTest
public with sharing class ScheduleBatchFarmCollectionTotalsTest {

    @testSetup static void setup() {

        // Farm
        List<Account> farms = TestClassDataUtil.createAUFarms(1, true);

        // Collections
        List<Collection__c> collections = new List<Collection__c>();
        Collection__c coll = TestClassDataUtil.createSingleCollection(farms[0], false);
        coll.Pick_Up_Date__c = Date.today().addMonths(-1);
        coll.Fat_Kgs__c = 10;
        coll.Prot_Kgs__c = 20;
        coll.Volume_Ltrs__c = 500;
        collections.add(coll);
        coll = TestClassDataUtil.createSingleCollection(farms[0], false);
        coll.Pick_Up_Date__c = Date.today();
        coll.Fat_Kgs__c = 30;
        coll.Prot_Kgs__c = 50;
        coll.Volume_Ltrs__c = 100;
        collections.add(coll);
        coll = TestClassDataUtil.createSingleCollection(farms[0], false);
        coll.Pick_Up_Date__c = Date.today().addYears(-1).addMonths(-1);
        coll.Fat_Kgs__c = 100;
        coll.Prot_Kgs__c = 200;
        coll.Volume_Ltrs__c = 2000;
        collections.add(coll);
        insert collections;

    }

    @isTest static void testUpdateTotals() {
        List<Account> farms = [SELECT Id FROM Account WHERE RecordType.Name = 'Farm AU'];
        Id farmId = farms[0].Id;
        System.assertNotEquals(null, farms);
        System.assertEquals(1, farms.size());
        Test.startTest();
        Database.executeBatch(new ScheduleBatchFarmCollectionTotals());
        Test.stopTest();

        List<Account> farmsAfter = [SELECT Total_Volume_12_Months__c, Total_Solids_12_Months__c FROM Account WHERE Id = :farmId];
        Account thisFarm = farmsAfter[0];
        System.assertEquals(500, thisFarm.Total_Volume_12_Months__c);
        System.assertEquals(30, thisFarm.Total_Solids_12_Months__c);
    }

    @isTest static void testScheduledTotals() {
        String jobName = 'Farm Collection Totals Daily Update';

        // Well SF won't let you create a Scheduler_CRON__mdt so we cheat and do it via JSON
        List<Map<String, Object>> mockedCronsAbstract = new List<Map<String, Object>> {
                new Map<String, Object> {
                    'Type' => Schema.Scheduler_CRON__mdt.SObjectType.getDescribe().getName(),
                    'Name' => 'ScheduleBatchFarmCollectionTotals',
                    'DeveloperName' => 'ScheduleBatchFarmCollectionTotals',
                    'CRON_Expression__c' => '0 0 4 1 * ?',
                    'Job_Name__c' => jobName,
                    'Run_As_User__c' => ScheduleService.BATCH_PROCESS_AU
                }
        };
        List<Scheduler_CRON__mdt> mockedCrons = (List<Scheduler_CRON__mdt>)JSON.deserialize(JSON.serialize(mockedCronsAbstract), List<Scheduler_CRON__mdt>.class);
        ScheduleService.mockSchedulerCrons(mockedCrons);

        Test.startTest();
        ScheduleBatchFarmCollectionTotals.abort();
        ScheduleBatchFarmCollectionTotals.schedule();
        Test.stopTest();

        List<CronTrigger> existingJob = [SELECT Id FROM CronTrigger WHERE CronjobDetail.Name = :jobName];
        System.assertEquals(1, existingJob.size());

    }


    @isTest static void testAbortSchedule() {
        String jobName = 'Farm Collection Totals Daily Update';

        // Well SF won't let you create a Scheduler_CRON__mdt so we cheat and do it via JSON
        List<Map<String, Object>> mockedCronsAbstract = new List<Map<String, Object>> {
                new Map<String, Object> {
                    'Type' => Schema.Scheduler_CRON__mdt.SObjectType.getDescribe().getName(),
                    'Name' => 'ScheduleBatchFarmCollectionTotals',
                    'DeveloperName' => 'ScheduleBatchFarmCollectionTotals',
                    'CRON_Expression__c' => '0 0 4 1 * ?',
                    'Job_Name__c' => jobName,
                    'Run_As_User__c' => ScheduleService.BATCH_PROCESS_AU
                }
        };
        List<Scheduler_CRON__mdt> mockedCrons = (List<Scheduler_CRON__mdt>)JSON.deserialize(JSON.serialize(mockedCronsAbstract), List<Scheduler_CRON__mdt>.class);
        ScheduleService.mockSchedulerCrons(mockedCrons);

        //Test aborting the schedule
        //Test.startTest();
        ScheduleBatchFarmCollectionTotals.abort();
        ScheduleBatchFarmCollectionTotals.schedule();
        //Test.stopTest();
        List<CronTrigger> existingJob = [SELECT Id FROM CronTrigger WHERE CronjobDetail.Name = :jobName];
        System.assertEquals(1, existingJob.size());

        ScheduleBatchFarmCollectionTotals.abort();
        List<CronTrigger> existingJob2 = [SELECT Id FROM CronTrigger WHERE CronjobDetail.Name = :jobName];
        System.assertEquals(0, existingJob2.size());
    }
}