/**
 * Description:
 *
 * Test for AMSCombinedProductionReportController
 *
 * @author: John Au (Trineo)
 * @date: Sep 2018
 */
@IsTest
private class AMSCombinedProductionReportContTest {

    @TestSetup
    private static void testSetup() {
        TestClassDataUtil.individualDefaultAccountAu();
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.createIntegrationUser();
    }

    private static Account getFarmAfterCreationOfFarmsAndIndividualsAu(User communityUser) {
        List<Account> farms = [
            SELECT Id
            FROM Account
            WHERE Id IN (
                SELECT Farm__c
                FROM Individual_Relationship__c
                WHERE Individual__c = :communityUser.ContactId
                AND Active__c = true
            )
        ];

        return farms[0];
    }

    @IsTest
    private static void testControllerInitializationAndFunctions() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
        Account farm = getFarmAfterCreationOfFarmsAndIndividualsAu(communityUser);
        TestClassDataUtil.createFarmSeasonAU(new List<Account> { farm });

        System.runAs(communityUser) {
            AMSCombinedProductionReportController controller = new AMSCombinedProductionReportController();

            List<SelectOption> reportTypeSelectOptions = controller.reportTypeSelectList;
            controller.setReportType(reportTypeSelectOptions[1].getValue());

            System.assertEquals(reportTypeSelectOptions[1].getValue(), controller.getReportType());
        }
    }
}