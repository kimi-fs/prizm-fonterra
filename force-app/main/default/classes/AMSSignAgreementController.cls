/**
 * Provide services for SF Files, including secure fetching of the file contents for community downloads
 *
 * @author Joe (Trineo)
 * @date   May 2020
 * @test:  AMSSignAgreementControllerTest
 */
public with sharing class AMSSignAgreementController {

    class AMSSignAgreementException extends Exception {}

    public static final Date DEFAULT_START_DATE = Date.newInstance(2021, 7, 1);
    public static final Date DEFAULT_END_DATE = Date.newInstance(2022, 6, 30);
    private static final String NO_FARM_PARTY_ID = 'FarmId and/or PartyId are required to be specified as a URL parameter(s).';
    private static final String NO_CUSTOM_SETTING = 'Custom Setting AMS_Agreement_Settings__c not defined.';
    private static final String AGREEMENT_NOT_VISIBLE = 'Agreement Signing feature not avaliable.';
    private static final String ACCESS_DENIED = 'Agreement Signing not available on that farm / party.';
    private static final String FARM_NO_OWNERS = 'Farm must have at least one active owner to be able to sign an e-agreement.';
    private static final String DUPLICATE_AGREEMENT_FOUND = 'Only one agreement with the same farmId, partyId, startDate and endDate with recordType Standard Agreement should exist.';
    private static final String FILE_FAIL = 'Whoops, failed to generate agreement.';
    private static final String FILE_FAIL_2 = 'Whoops, failed to generate signed agreement.';
    private static final String UPDATE_FAIL = 'Whoops, failed to update agreement record.';

    public Id farmId;
    public Id partyId;
    public Account farm;
    public transient String agreementPDFContents { get; set; }
    public String pricingOption {get; set; }
    public Agreement__c agreement { get; set;}
    public Boolean showLoadingSpinner { get; set; }
    public Boolean hasMultipleOwners { get; set; }
    public Boolean isSelectingPricingOption { get; set; }
    public String areaManagerText { get; set; }
    public List<SelectOption> picklistOptions { get; set; }
    public List<SelectOption> priceTablePicklistOptions { get; set; }
    public Integer fiveSecondCounts { get; set; }
    public Boolean is2021Season {get; set; }

    public AMSSignAgreementController() {
        this.is2021Season = true;//Date.today() >= Date.newInstance(2021, 7, 1);
        if (this.is2021Season) {
            this.isSelectingPricingOption = true;
        } else {
            this.isSelectingPricingOption = false;
        }
        this.showLoadingSpinner = true;
        this.fiveSecondCounts = 0;

        // Throw error if checkbox not ticked or Conga IDs not present.
        AMS_Agreement_Settings__c settings = AMS_Agreement_Settings__c.getOrgDefaults();
        if (settings == null) {
            throw new AMSSignAgreementException(NO_CUSTOM_SETTING);
        } else if (settings.Show_Agreement_Section__c == false || settings.Conga_Solution__c == null || settings.Conga_Template_Id__c == null) {
            throw new AMSSignAgreementException(AGREEMENT_NOT_VISIBLE);
        }

        // Get URL parameters, writing into Ids, so they don't need sanitisation
        this.farmId = System.currentPageReference().getParameters().get('FarmId');
        this.partyId = System.currentPageReference().getParameters().get('PartyId');
        if (this.farmId == null || this.partyId == null) {
            throw new AMSSignAgreementException(NO_FARM_PARTY_ID);
        }

        // Does this user have access to this farmId
        Id contactId = AMSContactService.determineContactId();

        if (contactId == null) {
            User usr = AMSUserService.getUser();
            contactId = usr.ContactId;
            System.debug('No contactId found, falling back to the current user, ' + usr);
        }
        Boolean accessToFarm = RelationshipAccessService.isAccessGranted(contactId, this.farmId, RelationshipAccessService.Access.ACCESS_PRIMARY);
        Boolean accessToParty = RelationshipAccessService.isAccessGranted(contactId, this.partyId, RelationshipAccessService.Access.ACCESS_PRIMARY);

        if (!accessToFarm || !accessToParty) {
            throw new AMSSignAgreementException(AGREEMENT_NOT_VISIBLE);
        }

        // Query for fields page needs
        this.farm = AgreementService.getFarm(this.farmId);
        Integer farmOwnerCount = AgreementService.getFarmOwnerCount(this.farmId);

        if (farmOwnerCount == 0) {
            throw new AMSSignAgreementException(FARM_NO_OWNERS);
        }

        this.hasMultipleOwners = (farmOwnerCount > 1);

        // Populate the company type picklist options
        this.picklistOptions = new List<SelectOption>();
        this.priceTablePicklistOptions = new List<SelectOption>();

        List<Schema.PicklistEntry> allBusinessTypeValues = Agreement__c.Business_Type__c.getDescribe().getPicklistValues();
        //Default Business Type to force the farmer to select a type before signing the agreement
        picklistOptions.add(new SelectOption('', '--Select a business type--'));
        for(Schema.PicklistEntry f : allBusinessTypeValues){
            // The picklist values are prefixed with "Single:" and "Multiple:", and are mutually exclusive
            if (this.hasMultipleOwners && f.getValue().startsWith('Multiple:')) {
                picklistOptions.add(new SelectOption(f.getValue(), f.getLabel()));
            } else if (!this.hasMultipleOwners && f.getValue().startsWith('Single:')) {
                picklistOptions.add(new SelectOption(f.getValue(), f.getLabel()));
            }
        }
        picklistOptions.sort();

        // Populate the area manager text
        this.areaManagerText = farm.Rep_Area_Manager__r.Name + ((farm.Rep_Area_Manager__r.Phone != null) ? ' ' + farm.Rep_Area_Manager__r.Phone : '');
        Boolean isStandardFarm = this.farm.Agreement_Type__c == 'Standard';
        // Check if we already have an agreement otherwise create one.
        Id partyIdToUse = isStandardFarm ? this.partyId : null;
        List<Agreement__c> agreements = AgreementService.getAgreements(this.farmId, partyIdToUse, DEFAULT_START_DATE, null, isStandardFarm);

        if (agreements.size() > 1) {
            System.debug('Multiple agreements found, bailing');
            throw new AMSSignAgreementException(DUPLICATE_AGREEMENT_FOUND);
        } else if (agreements.size() == 0) {
            System.debug('No agreement, creating one');
            this.agreement = AgreementService.createPendingOnlineStandardAgreement(this.farmId, this.partyId, DEFAULT_START_DATE, DEFAULT_END_DATE);
        } else {
            System.debug('Found agreement');
            this.agreement = agreements[0];
        }

        if(this.agreement?.Agreement_Application__r.Minimum_Price_Table__c != null && this.agreement?.Agreement_Application__r.Agreement_Type__c != 'Organic') {
            this.isSelectingPricingOption = false;
            this.pricingOption = this.agreement?.Agreement_Application__r.Minimum_Price_Table__c;
        } else if (this.agreement?.Agreement_Application__r.Agreement_Type__c == 'Organic') {
            this.isSelectingPricingOption = false;
            this.pricingOption = '7/5';
        }

        priceTablePicklistOptions.add(new SelectOption('', '--Select a pricing option--'));
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Agreement__c.Pricing_Option__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : ple) {
            pickListValuesList.add(pickListVal.getLabel());
            priceTablePicklistOptions.add(new SelectOption(pickListVal.getValue(), pickListVal.getLabel()));
        }
    }


    // Called on initialise from VF page action.
    public PageReference setupAgreement() {
        PageReference redirect = null;
        if (this.agreement != null && this.agreement.Agreement_Status__c == AgreementService.AGREEMENT_STATUS_EXECUTED) {
            // Already signed, go to pricing.
            redirect = Page.AMSPricing;
            redirect.setRedirect(true);
            System.debug('already signed, going to pricing page');
        } else if (this.showLoadingSpinner) {
            try {
                if (this.agreement.Id == null) {
                    // This is a new agreement, insert it first and then generate the document
                    // for it.
                    this.agreement = AgreementService.insertAgreement(this.agreement);
                }

                // Always clear any references to an existing document, we will regenerate
                this.agreement.Unsigned_Document_ID__c = null;
                this.agreement.Signed_IP__c = null;
                this.agreement.Signed_Timestamp__c = null;
                this.agreement.Execution_Date__c = null;
                this.agreement.User_Signed__c = null;
                this.agreement.Agreement_Status__c = AgreementService.AGREEMENT_STATUS_PENDING;
                this.agreement.Signing_Method__c = AgreementService.SIGNING_METHOD_ONLINE;
                this.agreement.Release_Agreement_to_Farmer__c = true;

                AgreementService.updateAgreement(this.agreement);
                System.debug('Cleared signing information ' + this.agreement);

                // Now generate the document.

                if (!Test.isRunningTest() && (!is2021Season || !isSelectingPricingOption)) {
                    CongaDocumentService.createCongaDocumentFuture(this.agreement.Id);
                }
            } catch (Exception e) {
                throw new AMSSignAgreementException(FILE_FAIL + ' ' + e.getMessage() + ' : ' + e.getStackTraceString());
            }
        }
        return redirect;
    }

    public void updateReleaseAgreement() {
        if(this.agreement != null && is2021Season) {
            Agreement__c agreement = AgreementService.getAgreement(this.agreement.Id);
            agreement.Pricing_Option__c = this.pricingOption;
            AgreementService.updateAgreement(agreement);
        }
        isSelectingPricingOption = false;
        generateCongaDocument();
    }
    
    // Removed the initial conga generation so user can select pricing option
    public void generateCongaDocument() {
        // Now generate the document.
        if (!Test.isRunningTest()) {
            CongaDocumentService.createCongaDocumentFuture(this.agreement.Id);
        }
        System.debug('kicked off document generation');
    }

    public void checkFileGenerated() {
        System.debug('checkFileGenerated, existing agreement: ' + this.agreement);
        this.agreement = AgreementService.getAgreement(this.agreement.Id);
        System.debug('updated agreement: ' + this.agreement);
        if (this.agreement.Unsigned_Document_ID__c != null) {
            System.debug('unsigned document Id is now set, setting the documents contents ' + this.agreement.Unsigned_Document_ID__c);
            if (!Test.isRunningTest()) {
                this.agreementPDFContents = FileService.getAgreementDocumentContents(this.agreement.Unsigned_Document_ID__c);
            } else {
                this.agreementPDFContents = 'testing PDF';
            }
            this.showLoadingSpinner = false;
            System.debug('set the document');
        } else {
            System.debug('document is not yet ready, waiting.');
            this.fiveSecondCounts++;
        }
    }

    public PageReference signAndSubmit() {
        this.showLoadingSpinner = true;

        // ReGenerate Conga document with signatures now, leaving the original one
        // in place.
        try {
            if (!Test.isRunningTest()) {
                CongaDocumentService.createCongaDocumentFuture(this.agreement.Id);
            }
        } catch (Exception e) {
             throw new AMSSignAgreementException(FILE_FAIL_2);
        }

        // Set Agreement fields.
        this.agreement.Signed_IP__c = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        this.agreement.Signed_Timestamp__c = Datetime.now();
        this.agreement.Execution_Date__c = Date.today();
        this.agreement.User_Signed__c = UserInfo.getUserId();
        this.agreement.Agreement_Status__c = AgreementService.AGREEMENT_STATUS_SUBMITTED;
        this.agreement.Signing_Method__c = AgreementService.SIGNING_METHOD_ONLINE;
        this.agreement.Release_Agreement_to_Farmer__c = true;
        try {
            AgreementService.updateAgreement(this.agreement);
        } catch (Exception e) {
            throw new AMSSignAgreementException(UPDATE_FAIL + e.getMessage() + ' : ' + e.getStackTraceString());
        }

        PageReference pricingPage = Page.AMSPricing;
        pricingPage.setRedirect(true);
        // Using cookie as we want to retain state between two pages
        // 60 Seconds as this is enought time to load the next page and it will be deleted after that
        Cookie cookie = new Cookie('agreementSignedCookie', 'true', null, 60, false);
        pricingPage.setCookies(new Cookie[]{cookie});
        return pricingPage;
    }
}