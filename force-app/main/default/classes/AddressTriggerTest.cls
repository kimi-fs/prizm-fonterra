@isTest
private class AddressTriggerTest {

    @testSetup static void createAddresses() {

        Profile p = [SELECT Id FROM Profile WHERE Name='Integration User'];
        User u = new User(Alias = 'standt', Email='standarduser@fonterra.com',
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
            LocaleSidKey='en_US', ProfileId = p.Id,
            TimeZoneSidKey='Pacific/Auckland', UserName='integration@testorg.com');

        insert u;

        // This is needed for the Address
        Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();

        //Entities
        List<Account> lAcc = new List<Account>();
        Account frmAcc = TestClassDataUtil.createAUFarms(1, false)[0];
        frmAcc.Name = 'FarmAcc';
        Account parAcc = TestClassDataUtil.createEntity(GlobalUtility.auAccountPartyRecordTypeName, 'Test party');
        parAcc.Name = 'PartyAcc';
        lAcc.add(frmAcc);
        lAcc.add(parAcc);
        insert lAcc;

        List<Contact> lCon = new List<Contact>();
        Contact individualCon1 = new Contact(FirstName = 'Test', LastName = 'Individual', Phone = '+64 3 7005550', Type__c = 'Personal');
        Contact individualCon2 = new Contact(FirstName = 'Test', LastName = 'Individual2', Phone = '+64 3 7005551', Type__c = 'Personal');
        lCon.add(individualCon1);
        lCon.add(individualCon2);
        insert lCon;


        // Lead
        Id auLeadRecordType = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Fonterra Lead AU').getRecordTypeId();
        Lead newLead = new Lead(Company = 'Lead Farm', FirstName = 'Test', LastName = 'LeadIndividual');
        insert newLead;

        // Opportunity
        Id auOppRecordType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Fonterra Opportunity AU').getRecordTypeId();
        Opportunity newOpp = new Opportunity(Name = 'Test Opp', CloseDate = System.today().addDays(10), StageName = 'Appointment Made');
        insert newOpp;

        //Addresses
        RecordType PHYSICAL= [SELECT Id FROM RecordType WHERE sObjectType = 'Address__c' AND Name = 'Physical'];
        RecordType POSTAL = [SELECT Id FROM RecordType WHERE sObjectType = 'Address__c' AND Name = 'Postal'];

        List<Address__c> lstAddr = new List<Address__c>();
        //Address
        Address__c updateAddr1 = new Address__c();
        updateAddr1.Street_Number__c = '9';
        updateAddr1.Street_1__c = 'Princess Street';
        updateAddr1.Street_2__c = '';
        updateAddr1.Suburb__c = 'Auckland';
        updateAddr1.City__c = 'Auckland';
        updateAddr1.Country_Code__c = 'NZ';
        updateAddr1.Postcode__c = '1010';
        updateAddr1.End_Date__c = System.today()+2;
        updateAddr1.Start_Date__c = System.today();
        updateAddr1.Entity__c = frmAcc.Id;
        updateAddr1.RecordTypeID = PHYSICAL.Id;
        lstAddr.add(updateAddr1);

        Address__c updateAddr2 = new Address__c();
        updateAddr2.Street_Number__c = '120';
        updateAddr2.Street_1__c = 'Symonds Street';
        updateAddr2.Street_2__c = '';
        updateAddr2.Suburb__c = 'Auckland';
        updateAddr2.City__c = 'Auckland';
        updateAddr2.Country_Code__c = 'NZ';
        updateAddr2.Postcode__c = '1010';
        updateAddr2.End_Date__c = System.today()+3;
        updateAddr2.Start_Date__c = System.today();
        updateAddr2.Entity__c = frmAcc.Id;
        updateAddr2.RecordTypeID = PHYSICAL.Id;
        lstAddr.add(updateAddr2);

        Address__c updateAddr3 = new Address__c();
        updateAddr3.Street_Number__c = '150';
        updateAddr3.Street_1__c = 'Symonds Street';
        updateAddr3.Street_2__c = '';
        updateAddr3.Suburb__c = 'Auckland';
        updateAddr3.City__c = 'Auckland';
        updateAddr3.Country_Code__c = 'NZ';
        updateAddr3.Postcode__c = '1010';
        //updateAddr3.End_Date__c = null;
        updateAddr3.Start_Date__c = System.today();
        updateAddr3.Entity__c = frmAcc.Id;
        updateAddr3.RecordTypeID = PHYSICAL.Id;
        lstAddr.add(updateAddr3);

        Address__c updateAddr4 = new Address__c();
        updateAddr4.Street_Number__c = '9';
        updateAddr4.Street_1__c = 'Princess Street';
        updateAddr4.Street_2__c = '';
        updateAddr4.Suburb__c = 'Auckland';
        updateAddr4.City__c = 'Auckland';
        updateAddr4.Country_Code__c = 'NZ';
        updateAddr4.Postcode__c = '1010';
        updateAddr4.End_Date__c = System.today()+2;
        updateAddr4.Start_Date__c = System.today();
        updateAddr4.Entity__c = parAcc.Id;
        updateAddr4.Individual__c = individualCon1.Id;
        updateAddr4.RecordTypeID = POSTAL.Id;
        lstAddr.add(updateAddr4);

        Address__c updateAddr5 = new Address__c();
        updateAddr5.Street_Number__c = '120';
        updateAddr5.Street_1__c = 'Symonds Street';
        updateAddr5.Street_2__c = '';
        updateAddr5.Suburb__c = 'Auckland';
        updateAddr5.City__c = 'Auckland';
        updateAddr5.Country_Code__c = 'NZ';
        updateAddr5.Postcode__c = '1010';
        updateAddr5.End_Date__c = System.today()+3;
        updateAddr5.Start_Date__c = System.today();
        updateAddr5.Entity__c = parAcc.Id;
        updateAddr5.Individual__c = individualCon1.Id;
        updateAddr5.RecordTypeID = POSTAL.Id;
        lstAddr.add(updateAddr5);

        Address__c updateAddr6 = new Address__c();
        updateAddr6.Street_Number__c = '150';
        updateAddr6.Street_1__c = 'Symonds Street';
        updateAddr6.Street_2__c = '';
        updateAddr6.Suburb__c = 'Auckland';
        updateAddr6.City__c = 'Auckland';
        updateAddr6.Country_Code__c = 'NZ';
        updateAddr6.Postcode__c = '1010';
        //updateAddr3.End_Date__c = null;
        updateAddr6.Start_Date__c = System.today();
        updateAddr6.Entity__c = parAcc.Id;
        updateAddr6.Individual__c = individualCon1.Id;
        updateAddr6.RecordTypeID = POSTAL.Id;
        lstAddr.add(updateAddr6);

        Address__c updateAddr10 = new Address__c();
        updateAddr10.Street_Number__c = '1';
        updateAddr10.Street_1__c = 'Opp Street';
        updateAddr10.City__c = 'Auckland';
        updateAddr10.Country_Code__c = 'NZ';
        updateAddr10.Postcode__c = '1010';
        updateAddr10.Start_Date__c = System.today();
        updateAddr10.End_Date__c = System.today().addDays(10);
        updateAddr10.Opportunity__c = newOpp.Id;
        updateAddr10.RecordTypeID = PHYSICAL.Id;
        lstAddr.add(updateAddr10);

        System.runAs(u) {
            insert lstAddr;
        }
    }

    @isTest static void createAddressTest() {
        //FARM INSERT

        User iUser = [SELECT Id FROM User WHERE UserName = 'integration@testorg.com'];

        RecordType PHYSICAL= [SELECT Id FROM RecordType WHERE sObjectType = 'Address__c' AND Name = 'Physical'];
        RecordType POSTAL = [SELECT Id FROM RecordType WHERE sObjectType = 'Address__c' AND Name = 'Postal'];

        Account frmAcc = [SELECT Id FROM Account WHERE Name = 'FarmAcc'];
        Account parAcc = [SELECT Id FROM Account WHERE Name = 'PartyAcc'];

        Contact indvCon = [SELECT Id FROM Contact LIMIT 1];

        Address__c insertAddr1 = new Address__c();
        insertAddr1.Street_Number__c = '126';
        insertAddr1.Street_1__c = 'Symonds Street';
        insertAddr1.Street_2__c = '';
        insertAddr1.Suburb__c = 'Royal Oak';
        insertAddr1.City__c = 'Auckland';
        insertAddr1.Country_Code__c = 'NZ';
        insertAddr1.Postcode__c = '1026';
        insertAddr1.End_Date__c = System.today()+5;
        insertAddr1.Start_Date__c = System.today();
        insertAddr1.Entity__c = frmAcc.Id;
        insertAddr1.RecordTypeID = PHYSICAL.Id;

        //FARM INSERT BLANK END DATE
        Address__c insertAddr2 = new Address__c();
        insertAddr2.Street_Number__c = '12';
        insertAddr2.Street_1__c = 'Harrison Street';
        insertAddr2.Street_2__c = '';
        insertAddr2.Suburb__c = 'Auckland';
        insertAddr2.City__c = 'Auckland';
        insertAddr2.Country_Code__c = 'NZ';
        insertAddr2.Postcode__c = '1010';
        insertAddr2.End_Date__c = null;
        insertAddr2.Start_Date__c = System.today();
        insertAddr2.Entity__c = frmAcc.Id;
        insertAddr2.RecordTypeID = PHYSICAL.Id;

        Address__c insertAddr5 = new Address__c();
        insertAddr5.Street_Number__c = '898';
        insertAddr5.Street_1__c = 'Symonds Street';
        insertAddr5.Street_2__c = '';
        insertAddr5.Suburb__c = 'Auckland';
        insertAddr5.City__c = 'Auckland';
        insertAddr5.Country_Code__c = 'NZ';
        insertAddr5.Postcode__c = '655';
        insertAddr5.End_Date__c = System.today()+5;
        insertAddr5.Start_Date__c = System.today();
        insertAddr5.Entity__c = parAcc.Id;
        insertAddr5.Individual__c = indvCon.Id;
        insertAddr5.RecordTypeID = POSTAL.Id;

        Address__c insertAddr6 = new Address__c();
        insertAddr6.Street_Number__c = '555';
        insertAddr6.Street_1__c = 'Symonds Street';
        insertAddr6.Street_2__c = '';
        insertAddr6.Suburb__c = 'Auckland';
        insertAddr6.City__c = 'Auckland';
        insertAddr6.Country_Code__c = 'NZ';
        insertAddr6.Postcode__c = '655';
        insertAddr6.End_Date__c = null;
        insertAddr6.Start_Date__c = System.today();
        insertAddr6.Entity__c = parAcc.Id;
        insertAddr6.Individual__c = indvCon.Id;
        insertAddr6.RecordTypeID = POSTAL.Id;

        System.runAs(iUser) {
            Test.startTest();
            insert insertAddr1;
            insert insertAddr2;
            insert insertAddr5;
            insert insertAddr6;
            Test.stopTest();
        }

    }

    @isTest static void updateAddressTest() {

        User intUser = [SELECT Id FROM User WHERE UserName = 'integration@testorg.com'];

        String farmAddID = [SELECT Id, Physical_Address_Id__c FROM Account WHERE Name = 'FarmAcc'].Physical_Address_Id__c;
        String partyAddID = [SELECT Id, Postal_Address_Id__c FROM Account WHERE Name = 'PartyAcc'].Postal_Address_Id__C;

        Account acc = [SELECT Id,Physical_Address_Id__c FROM Account WHERE Name = 'FarmAcc'];

        Contact indvCon = [SELECT Id FROM Contact LIMIT 1];

        System.debug('##ACC: ' + acc.Physical_Address_Id__c);
        System.debug('##FARMADDID: ' + farmAddId);
        System.debug('##PARTYADDID: ' + partyAddID);
        //List<Address__c> adds = [SELECT Id FROM Address__c WHERE Entity__c =: acc.Id];
        //System.debug('##ADDRESS: ' + adds);

        Address__c updateFarmPhyAdd = [SELECT Id, Entity__c, End_Date__c,City__c FROM Address__c WHERE Id =: farmAddId];
        Address__c updatePartyPostAdd = [SELECT Id, Entity__c, End_Date__c,City__c FROM Address__c WHERE Id =: partyAddID];

        Test.startTest();

        List<Address__c> updateAdd = new List<Address__c>();

        System.runAs(intUser) {
            updateFarmPhyAdd.City__c = 'Wellington';
            updateAdd.add(updateFarmPhyAdd);

            updatePartyPostAdd.City__c = 'Wellington';
            updateAdd.add(updatePartyPostAdd);
            update updateAdd;

            //Check if the address has been copied
            Map<Id,Account> mapAcc = new Map<Id,Account>([SELECT BillingCity,ShippingCity FROM Account WHERE Name IN ('FarmAcc','PartyAcc')]);
            System.assertEquals(updateFarmPhyAdd.City__c,mapAcc.get(updateFarmPhyAdd.Entity__c).BillingCity);
            System.assertEquals(updatePartyPostAdd.City__c,mapAcc.get(updatePartyPostAdd.Entity__c).ShippingCity);

            //DEACTIVATE
            updateFarmPhyAdd.End_Date__c = System.today()-1;
            updatePartyPostAdd.End_Date__c = System.today()-1;

            //Check if Address has been changed
            update updateAdd;
            Map<Id,Account> mapAcc2 = new Map<Id,Account>([SELECT Id, Physical_Address_Id__c,Postal_Address_Id__c FROM Account WHERE Name IN ('FarmAcc','PartyAcc','FSSAcc')]);
            System.assert(updateFarmPhyAdd.Id != mapAcc2.get(updateFarmPhyAdd.Entity__c).Physical_Address_ID__c);
            System.assert(updatePartyPostAdd.Id != mapAcc2.get(updatePartyPostAdd.Entity__c).Postal_Address_Id__c);
        }

        Test.stopTest();
    }

    @isTest static void deleteAddressTest() {
        String farmAddID = [SELECT Id, Physical_Address_Id__c FROM Account WHERE Name = 'FarmAcc'].Physical_Address_Id__c;
        String partyAddID = [SELECT Id, Postal_Address_Id__c FROM Account WHERE Name = 'PartyAcc'].Postal_Address_Id__C;

        Account acc = [SELECT Id,Physical_Address_Id__c FROM Account WHERE Name = 'FarmAcc'];
        System.debug('##ACC: ' + acc.Physical_Address_Id__c);
        System.debug('##FARMADDID: ' + farmAddId);
        System.debug('##PARTYADDID: ' + partyAddID);
        //List<Address__c> adds = [SELECT Id FROM Address__c WHERE Entity__c =: acc.Id];
        //System.debug('##ADDRESS: ' + adds);

        Address__c updateFarmPhyAdd = [SELECT Id, Entity__c, End_Date__c,City__c FROM Address__c WHERE Id =: farmAddId];
        Address__c updatePartyPostAdd = [SELECT Id, Entity__c, End_Date__c,City__c FROM Address__c WHERE Id =: partyAddID];

        Test.startTest();
        List<Address__c> delAdds = new List<Address__c>();

        delAdds.add(updateFarmPhyAdd);
        delAdds.add(updatePartyPostAdd);

        delete delAdds;

        //Check if Addresses were deleted
        Map<Id,Account> mapAcc2 = new Map<Id,Account>([SELECT Id, Physical_Address_Id__c,Postal_Address_Id__c FROM Account WHERE Name IN ('FarmAcc','PartyAcc','FSSAcc')]);
        System.assert(updateFarmPhyAdd.Id != mapAcc2.get(updateFarmPhyAdd.Entity__c).Physical_Address_ID__c);
        System.assert(updatePartyPostAdd.Id != mapAcc2.get(updatePartyPostAdd.Entity__c).Postal_Address_Id__c);

        Test.stopTest();
    }

    @isTest static void createLeadOppAddressTest() {
        //FARM INSERT

        User iUser = [SELECT Id FROM User WHERE UserName = 'integration@testorg.com'];

        RecordType PHYSICAL= [SELECT Id FROM RecordType WHERE sObjectType = 'Address__c' AND Name = 'Physical'];

        Id oppId = [SELECT Id, Physical_Address_ID__c FROM Opportunity WHERE Name = 'Test Opp'].Id;

        Address__c insertAddr2 = new Address__c();
        insertAddr2.Street_Number__c = '2';
        insertAddr2.Street_1__c = 'Opp Street';
        insertAddr2.City__c = 'Auckland';
        insertAddr2.Country_Code__c = 'NZ';
        insertAddr2.Postcode__c = '1010';
        insertAddr2.Start_Date__c = System.today();
        insertAddr2.Opportunity__c = oppId;
        insertAddr2.RecordTypeID = PHYSICAL.Id;

        Test.startTest();
        System.runAs(iUser) {
            insert insertAddr2;
        }
        Test.stopTest();

        Opportunity opp = [SELECT Id, Physical_Address_ID__c FROM Opportunity WHERE Id = :oppId];
        System.assertEquals(insertAddr2.Id, opp.Physical_Address_ID__c);

    }
}