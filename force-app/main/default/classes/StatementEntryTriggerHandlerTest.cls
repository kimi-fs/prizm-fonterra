@IsTest
private class StatementEntryTriggerHandlerTest {

	@testSetup static void testSetup(){
        Account party = TestClassDataUtil.createEntity(GlobalUtility.auAccountPartyRecordTypeName, 'Test party');
        insert party;
		List<Statement__c> statements = TestClassDataUtil.createStatements(new List<Account>{party});
		List<Statement_Entry__c> statementEntries = TestClassDataUtil.createStatementEntries(statements);
	}

    @IsTest private static void deleteStatementsWithNoStatementEntries() {
        List<Statement_Entry__c> statementEntries = new List<Statement_Entry__c>([SELECT Id, Statement__c FROM Statement_Entry__c]);

        Test.startTest();
        	delete statementEntries[0];
        Test.stopTest();

        System.assertEquals(0, new List<Statement__c>([SELECT Id FROM Statement__c WHERE Id =: statementEntries[0].Statement__c]).size(), 'Statement was not deleted');

    }
}