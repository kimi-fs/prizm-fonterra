public without sharing class AMSChannelPreferencesUtil {

    public static final String AU_DAILY_MILK_RESULT = 'ADMR';
    public static final String AU_MILK_QUALITY_ALERT = 'AMQA';
    public static final String AU_STATEMENT_NOTIFICATIONS = 'ASN';
    public static final String AU_MARKETING_COMMUNICATIONS_NAME = 'Marketing Communications';

    public static final Channel_Preference_Setting__c STATEMENT_NOTIFICATION_SETTING {
        get {
            if (STATEMENT_NOTIFICATION_SETTING == null) {
                List<Channel_Preference_Setting__c> statementNotificationSettings =
                    [SELECT
                        Id,
                        Name,
                        Exact_Target_Message_ID__c,
                        Exact_Target_Message_Keyword__c,
                        Exact_Target_Time_Zone__c,
                        Exact_Target_Window_Start__c,
                        Exact_Target_Window_End__c
                    FROM
                        Channel_Preference_Setting__c
                    WHERE CME_Message_Type_Id__c = :AU_STATEMENT_NOTIFICATIONS LIMIT 1];

                if (!statementNotificationSettings.isEmpty()) {
                    STATEMENT_NOTIFICATION_SETTING = statementNotificationSettings[0];
                }
            }

            return STATEMENT_NOTIFICATION_SETTING;
        }

        private set;
    }

    public static final Channel_Preference_Setting__c MARKETING_COMMUNICATIONS_SETTING {
        get {
            if (MARKETING_COMMUNICATIONS_SETTING == null) {
                List<Channel_Preference_Setting__c> marketingCommunicationSettings =
                    [SELECT
                        Id,
                        Name,
                        Exact_Target_Message_ID__c,
                        Exact_Target_Message_Keyword__c,
                        Exact_Target_Time_Zone__c,
                        Exact_Target_Window_Start__c,
                        Exact_Target_Window_End__c
                    FROM
                        Channel_Preference_Setting__c
                    WHERE Name = :AU_MARKETING_COMMUNICATIONS_NAME LIMIT 1];

                if (!marketingCommunicationSettings.isEmpty()) {
                    MARKETING_COMMUNICATIONS_SETTING = marketingCommunicationSettings[0];
                }
            }

            return MARKETING_COMMUNICATIONS_SETTING;
        }

        private set;
    }

    public static final Channel_Preference_Setting__c DAILY_MILK_RESULT_SETTING {
        get {
            if (DAILY_MILK_RESULT_SETTING == null) {
                List<Channel_Preference_Setting__c> dailyMilkResultSettings =
                    [SELECT
                        Id,
                        Name,
                        Exact_Target_Message_ID__c,
                        Exact_Target_Message_Keyword__c,
                        Exact_Target_Time_Zone__c,
                        Exact_Target_Window_Start__c,
                        Exact_Target_Window_End__c
                    FROM
                        Channel_Preference_Setting__c
                    WHERE CME_Message_Type_Id__c = :AU_DAILY_MILK_RESULT LIMIT 1];

                if (!dailyMilkResultSettings.isEmpty()) {
                    DAILY_MILK_RESULT_SETTING = dailyMilkResultSettings[0];
                }
            }

            return DAILY_MILK_RESULT_SETTING;
        }

        private set;
    }

    public static final Channel_Preference_Setting__c MILK_QUALITY_ALERT_SETTING {
        get {
            if (MILK_QUALITY_ALERT_SETTING == null) {
                List<Channel_Preference_Setting__c> milkQualityAlertSettings =
                    [SELECT
                        Id,
                        Name,
                        Exact_Target_Message_ID__c,
                        Exact_Target_Message_Keyword__c,
                        Exact_Target_Time_Zone__c,
                        Exact_Target_Window_Start__c,
                        Exact_Target_Window_End__c
                    FROM
                        Channel_Preference_Setting__c
                    WHERE CME_Message_Type_Id__c = :AU_MILK_QUALITY_ALERT LIMIT 1];

                if (!milkQualityAlertSettings.isEmpty()) {
                    MILK_QUALITY_ALERT_SETTING = milkQualityAlertSettings[0];
                }
            }

            return MILK_QUALITY_ALERT_SETTING;
        }

        private set;
    }

}