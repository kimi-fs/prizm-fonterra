/**
 * Superclass for all Production & Quality reports
 * Handles farm/season defaulting, date/month selection
 *
 * Delegates date defaulting and "form" calculation to all subclasses
 *
 * Needs further work to simplify and remove unnecessary cruft, but this is just the first step in removing all the rubbish from/standardizing each P&Q report controller
 *
 * Author: John Au (Trineo)
 * Date: 2017-10-09
 */
public abstract class AMSProdQualReport {

    public Contact individual;
    public Boolean individualHasFarms { get; set; }

    public final Date TODAY = System.today();

    protected final String SELECTED_MONTH_PARAM = 'month';
    protected final String SELECTED_TAB_PARAM = 'tab';
    protected final String SELECTED_SEASON_PARAM = 'season';

    public AMSProdQualReport() {
        //find a 'farmid' parameter to pre-select a farm if this page is being linked to from anywhere else
        selectedFarmParameter = ApexPages.currentPage().getParameters().get(AMSCommunityUtil.getFarmIdParameterName());

        //populate navigation drop down for reports
        reportTypeOptionList = AMSCollectionService.populateReportTypeList();

        graphOptionList = AMSCollectionService.graphSelectOptions;
        plotOptionList = AMSCollectionService.PLOT_OPTIONS;
        selectedPlotOption = plotOptionList[0].getValue();

        seasonOptionList = new List<SelectOption>();
        referencePeriodIdToFarmSeason = new Map<Id, Farm_Season__c>();
        farmSeasonsInDescendingDates = new List<Farm_Season__c>();
        monthList = new List<String>();

        //user has no farms unless proven otherwise
        individualHasFarms = false;

        //Get current users contact to obtain related farms
        Id contactId = AMSContactService.determineContactId();
        if (contactId != null) {
            individual = AMSCollectionService.queryMatchingUserContacts(contactId);
            individualHasFarms = individualHasFarms(individual.Id);
        }

        initProperties();
        initPropertiesAgain();

        if (individualHasFarms) {
            //select relevant farms for the user
            populateFarmList();
            //populate farm season list for farms selected
            populateFarmSeasonOptionList();
            defaultFromToDate();
            populateMonthList();
            //run calculation for tables on page
            recalculateForm();
        }
    }

    //overridden when a subclass needs to populate some stuff before the rest of the constructor fires (namely, recalculateForm)
    public virtual void initProperties() {

    }

    //because apex is stupid and won't let you override a method more than once
    public virtual void initPropertiesAgain() {

    }

    protected List<Account> farmsForIndividual;

    public List<String> monthList { get; set; }

    public List<SelectOption> farmOptionList { get; set; }
    public List<SelectOption> seasonOptionList { get; set; }
    public List<SelectOption> graphOptionList { get; set; }
    public List<SelectOption> reportTypeOptionList { get; set; }
    public List<SelectOption> plotOptionList { get; set; }

    public Farm_Season__c selectedFarmSeason { get; set; }
    public Farm_Season__c priorFarmSeason { get; set; }

    public Map<Id, Farm_Season__c> referencePeriodIdToFarmSeason { get; set; }
    public String referencePeriodIdToFarmSeasonSerialized { get; set; }
    public List<Farm_Season__c> farmSeasonsInDescendingDates { get; set; }
    public List<Farm_Season__c> paddedSeasonList { get; set; }

    public String fromDateString { get; set; }
    public String toDateString { get; set; }

    public Date fromDate { get; set; }
    public Date toDate { get; set; }

    public Date fromDatePriorSeason { get; set; }
    public Date toDatePriorSeason { get; set; }

    public Date scopedCurrentSeasonsFromDate { get; set; }
    public Date scopedCurrentSeasonsToDate { get; set; }
    public Date scopedPriorSeasonsFromDate { get; set; }
    public Date scopedPriorSeasonsToDate { get; set; }

    public String selectedPlotOption { get; set; }
    public Account selectedFarm { get; set; }
    public String selectedFarmId {
        get;
        set {
            this.selectedFarmId = value;

            if (!String.isBlank(value)) {
                for (Account farm : farmsForIndividual) {
                    if (farm.Id == value) {
                        this.selectedFarm = farm;
                        break;
                    }
                }
            }
        }
    }
    public String selectedSeasonId { get; set; }
    public String selectedMonth { get; set; }

    private String selectedFarmParameter;

    public String selectedReportTypeName { get; set; }

    protected void populateMonthList() {
        monthList = new List<String>();
        Date dateIterative = Date.newInstance(fromDate.year(), fromDate.month(), 1);
        Date endMonth = Date.newInstance(toDate.year(), toDate.month(), 1);
        while (dateIterative <= endMonth) {
            monthList.add(AMSCollectionService.MONTH_NAMES[dateIterative.month() - 1]);
            dateIterative = dateIterative.addMonths(1);
        }
        selectedMonth = monthList[0];
    }

    public void nextMonth() {
        Integer selectedMonthNumber = indexOf(monthList, selectedMonth);
        if (selectedMonthNumber < monthList.size() - 1) {
            selectedMonth = monthList[selectedMonthNumber + 1];
        }
        recalculateForm();
    }

    public void previousMonth() {
        Integer selectedMonthNumber = indexOf(monthList, selectedMonth);
        if (selectedMonthNumber > 0) {
            selectedMonth = monthList[selectedMonthNumber - 1];
        }
        recalculateForm();
    }

    protected Integer indexOf(List<Object> l, Object o) {
        Integer index = -1;
        for (Integer i = 0; i < l.size(); i++) {
            if (l[i].equals(o)) {
                index = i;
                break;
            }
        }

        return index;
    }

    //Select all relevant Farms associated with running user and populate selected list
    //If no farms are found, trigger the no farms flag
    public void populateFarmList() {
        farmOptionList = new List<SelectOption>();
        farmsForIndividual = new List<Account>();
        farmsForIndividual = RelationshipAccessService.accessToFarms(individual.Id, RelationshipAccessService.Access.ACCESS_PRODUCTION_AND_QUALITY);
        if (!farmsForIndividual.isEmpty()) {
            Set<String> farmIds = new Set<String>();
            for (Account farm : farmsForIndividual) {
                farmOptionList.add(new SelectOption(farm.Id, farm.Name));
                farmIds.add(farm.Id);
            }

            AMSCommunityUtil.sortSelectOptionList(farmOptionList, AMSCommunityUtil.FieldToSort.Label);

            if (selectedFarmParameter != null && farmIds.contains(selectedFarmParameter)) {
                selectedFarmId = selectedFarmParameter;
            } else {
                selectedFarmId = farmOptionList[0].getValue();
            }
        }
    }

    //Scan selected farm for farm seasons
    private void populateFarmSeasonOptionList() {
        seasonOptionList = new List<SelectOption>();

        farmSeasonsInDescendingDates = AMSCollectionService.queryRelatedFarmSeasons(selectedFarmId);
        paddedSeasonList = AMSCollectionService.populateFarmSeasonListInDescendingOrder(farmSeasonsInDescendingDates);

        for (Farm_Season__c farmSeason : paddedSeasonList) {
            referencePeriodIdToFarmSeason.put(farmSeason.Reference_Period__r.Id, farmSeason);
            seasonOptionList.add(new SelectOption(farmSeason.Reference_Period__r.Id, farmSeason.Reference_Period__r.Reference_Period__c));
        }
        if (seasonOptionList.isEmpty()) {
            seasonOptionList.add(new SelectOption('', '-None-'));
        }
        if (String.isEmpty(selectedSeasonId)) {
            selectedSeasonId = seasonOptionList[0].getValue();
        }
        if (String.isNotEmpty(selectedSeasonId)) {
            setCurrentPriorSeason();
        }
        seasonOptionList.remove(seasonOptionList.size() - 1);
        serializeReferencePeriodMap();
    }

    //Serialize referencePeriodMap to String
    private void serializeReferencePeriodMap() {
        referencePeriodIdToFarmSeasonSerialized = JSON.serialize(referencePeriodIdToFarmSeason);
    }

    //Specify the default from and to date
    //If a previous season is selected then the From Date and To Date defaults to the start date and end date of the selected season, respectively.
    //If the current season is selected, then the From Date defaults to the start of the season and To Date is today's date.
    public virtual void defaultFromToDate() {
        fromDate = selectedFarmSeason.Reference_Period__r.Start_Date__c;
        if (selectedFarmSeason.Reference_Period__r.Status__c == 'Past') {
            toDate = selectedFarmSeason.Reference_Period__r.End_Date__c;
        } else {
            toDate = TODAY;
        }

        fromDatePriorSeason = fromDate.addYears(-1);
        toDatePriorSeason = toDate.addYears(-1);
        populateDateStringFromDates();
        populateMonthList();
    }

    public abstract void recalculateForm();

    //Scan selected farm for farm seasons
    protected void setCurrentPriorSeason() {
        selectedFarmSeason = null;
        priorFarmSeason = null;

        for (Farm_Season__c farmSeason : paddedSeasonList) {
            if (selectedSeasonId == farmSeason.Reference_Period__c) {
                selectedFarmSeason = farmSeason;
            } else if (selectedFarmSeason != null && priorFarmSeason == null) {
                priorFarmSeason = farmSeason;
            }
        }
    }

    public void populateDateStringFromDates() {
        fromDateString = fromDate != null ? AMSCollectionService.australianFormatDate(fromDate) : '';
        toDateString = toDate != null ? AMSCollectionService.australianFormatDate(toDate) : '';
    }

    //populate dateStrings from Dates
    public void populateDateFromDateString() {
        fromDate = null;
        toDate = null;
        if (fromDateString != null) {
            fromDate = AMSCollectionService.australianParseDate(fromDateString);
        }
        if (fromDateString != null) {
            toDate = AMSCollectionService.australianParseDate(toDateString);
        }
    }

    public void updatePriorDates() {
        fromDatePriorSeason = fromDate.addYears(-1);
        toDatePriorSeason = toDate.addYears(-1);
    }

    public void recalculateFromDate() {
        populateDateFromDateString();
        updatePriorDates();
        populateMonthList();
        recalculateForm();
    }

    //Call recalculation when farm is selected
    public virtual void recalculateFromFarm() {
        //all associated farm seasons for selected farm (first farm in list)
        populateFarmSeasonOptionList();
        recalculateForm();
    }

    //Call recalculation for tables on the page when a season is updated
    public void recalculateFromSeason() {
        //all associated farm seasons for selected farm (first farm in list)
        setCurrentPriorSeason();
        defaultFromToDate();
        recalculateForm();
    }

    //Method to check if an individual has any associated farms
    protected Boolean individualHasFarms(Id individualId) {
        List<Account> numberOfFarms = RelationshipAccessService.accessToFarms(individualId, RelationshipAccessService.Access.ACCESS_PRODUCTION_AND_QUALITY);
        return numberOfFarms.size() > 0;
    }

    //Navigate to selected report type from list
    public PageReference forwardToReport() {
        PageReference pr = AMSCollectionService.NAVIGATIONMAPFORPAGES.get(selectedReportTypeName);
        return pr;
    }

    public AMSCollectionService.UnitOfMeasurement getKgMS() { return AMSCollectionService.KgMS; }
    public AMSCollectionService.UnitOfMeasurement getTotalKgMS() { return AMSCollectionService.TotalKgMS; }
    public AMSCollectionService.UnitOfMeasurement getLitres() { return AMSCollectionService.Litres; }
    public AMSCollectionService.UnitOfMeasurement getBMCC() { return AMSCollectionService.BMCC; }
    public AMSCollectionService.UnitOfMeasurement getProteinPercentage() { return AMSCollectionService.ProteinPercentage; }
    public AMSCollectionService.UnitOfMeasurement getFatPercentage() { return AMSCollectionService.FatPercentage; }
    public AMSCollectionService.UnitOfMeasurement getKgMSPercentage() { return AMSCollectionService.KgMSPercentage; }
    public AMSCollectionService.UnitOfMeasurement getTemp() { return AMSCollectionService.Temp; }
    public AMSCollectionService.UnitOfMeasurement getFatKg() { return AMSCollectionService.FatKg; }
    public AMSCollectionService.UnitOfMeasurement getProteinKg() { return AMSCollectionService.ProteinKg; }
    public AMSCollectionService.UnitOfMeasurement getKgMSPerCow() { return AMSCollectionService.KgMSPerCow; }
    public AMSCollectionService.UnitOfMeasurement getProteinFatRatio() { return AMSCollectionService.ProteinFatRatio; }
    public AMSCollectionService.UnitOfMeasurement getKgMSPerHectare() { return AMSCollectionService.KgMSPerHectare; }

    public enum FieldToSort {
        Label, Value
    }

    //https://github.com/abhinavguptas/Apex-Select-Option-Sorting/blob/master/src/classes/SelectOptionSorter.cls
    protected void sortSelectOptionList(List<SelectOption> opts, FieldToSort sortField) {
        Map<String, SelectOption> mapping = new Map<String, SelectOption>();
        // Suffix to avoid duplicate values like same labels or values are in inbound list
        Integer suffix = 1;

        for (SelectOption opt : opts) {
            if (sortField == FieldToSort.Label) {
                mapping.put(    // Done this cryptic to save scriptlines, if this loop executes 10000 times
                                // it would every script statement would add 1, so 3 would lead to 30000.
                             (opt.getLabel() + suffix++), // Key using Label + Suffix Counter
                             opt);
            } else {
                mapping.put(
                             (opt.getValue() + suffix++), // Key using Label + Suffix Counter
                             opt);
            }
        }

        List<String> sortKeys = new List<String>();
        sortKeys.addAll(mapping.keySet());
        sortKeys.sort();
        // clear the original collection to rebuilt it
        opts.clear();

        for (String key : sortKeys) {
            opts.add(mapping.get(key));
        }
    }
}