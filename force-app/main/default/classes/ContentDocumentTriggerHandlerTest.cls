/**
 * Description: Used to test cover ContentDocumentTriggerHandler methods
 * @author: Irish (Trineo)
 * @date: May 2020
 */

@IsTest
private class ContentDocumentTriggerHandlerTest {
    private static Agreement__c agreem;
    private static User userAUProfile;
    private static User userIntegrationProfile;
    private static User userSystemAdmin;
    private static List<Account> farm;
    private static Account party;

    @TestSetup
    private static void userSetup() {
        insert TestClassDataUtil.createUserByProfile('Field Team AU - Area Manager', 'Fonterra AU');
        TestClassDataUtil.createAdminUser();
    }

    private static void dataSetup() {
        userAUProfile = [SELECT Id, isActive FROM User WHERE Profile.Name = 'Field Team AU - Area Manager' AND isActive = true LIMIT 1];
        userSystemAdmin = [SELECT Id, isActive FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
        farm = TestClassDataUtil.createAUFarms(1, true);
        party = TestClassDataUtil.createSingleAccountPartyAU();
        agreem = TestClassDataUtil.createAgreement('Standard Agreement', farm[0].Id, Date.today() + 1, false);
        agreem.Party__c = party.Id;
        insert agreem;
    }

    @IsTest
    private static void validateEditAccessOfAgreementFiles() {
        String errorMessage;
        dataSetup();

        Account updateFarm = new Account(Id = farm[0].Id, OwnerId = userAUProfile.Id);
        update updateFarm;

        party.OwnerId = userAUProfile.Id;
        update party;
        Test.startTest();
        System.runAs(userAUProfile) {

            try {
                List<ContentVersion> contentVersions = TestClassDataUtil.createDocuments(agreem.Id, new Set<String> {'Test'}, true);
                ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersions[0].Id LIMIT 1];
                agreem.Agreement_Status__c = 'Executed';
                update agreem;

                ContentVersion contentVersion2 = new ContentVersion(
                    Title = 'Test 1',
                    PathOnClient = 'Test2.pdf',
                    VersionData = Blob.valueOf('Test Content Data 1'),
                    ContentDocumentId = contentVersionSelect.ContentDocumentId
                );
                insert contentVersion2;
            } catch (Exception e) {
                 errorMessage = e.getMessage();
            }
        }
        Test.stopTest();
        System.assert(String.isNotEmpty(errorMessage));
        System.assert(errorMessage.contains('You are not allowed to edit this document.'));

    }

    @IsTest
    private static void validateDeleteAccessOfAgreementFiles() {
        String errorMessage;
        dataSetup();

        Account updateFarm = new Account(Id = farm[0].Id, OwnerId = userAUProfile.Id);
        update updateFarm;

        party.OwnerId = userAUProfile.Id;
        update party;

        Test.startTest();
        System.runAs(userAUProfile) {
            try {
                List<ContentVersion> contentVersions = TestClassDataUtil.createDocuments(agreem.Id, new Set<String> {'Test'}, true);
                ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersions[0].Id LIMIT 1];

                agreem.Agreement_Status__c = 'Executed';
                update agreem;

                ContentDocument contentToDelete = [SELECT Id FROM ContentDocument WHERE Id = :contentVersionSelect.ContentDocumentId LIMIT 1];

                delete contentToDelete;
            } catch (Exception e) {
                errorMessage = e.getMessage();
            }
        }
        Test.stopTest();
        System.assert(String.isNotEmpty(errorMessage));
        System.assert(errorMessage.contains('You are not allowed to delete this document.'));
    }

    @IsTest
    private static void validateInsertAccessOfAgreementFiles() {
        dataSetup();
        agreem.Start_Date__c = Date.today();
        agreem.Active__c = true;
        update agreem;
        String errorMessage;

        Test.startTest();
        System.runAs(userAUProfile) {
            try {
                List<ContentVersion> contentVersions = TestClassDataUtil.createDocuments(null, new Set<String> {'Test'}, true);
                ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersions[0].Id LIMIT 1];

                ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
                contentDocumentLink.LinkedEntityId = agreem.Id;
                contentDocumentLink.ContentDocumentId = contentVersionSelect.ContentDocumentId;
                contentDocumentLink.shareType = 'V';
                insert contentDocumentLink;

            } catch (Exception e) {
                errorMessage = e.getMessage();
            }
        }
        Test.stopTest();

        System.assert(errorMessage.contains('You are not allowed to upload a new file to this record.'));
    }

}