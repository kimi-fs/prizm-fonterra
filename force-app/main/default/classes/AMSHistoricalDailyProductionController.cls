/**
 * Author (John Au, Trineo) 12/04/2018
 *
 * Please excuse this mess- I'm just the cleanup crew
 *
 * Controller for 'daily production' report with last season columns
 *
 * Tested by AMSProductionController_Test
 **/
public virtual without sharing class AMSHistoricalDailyProductionController extends AMSProductionReport {

    public AMSProductionReport baseController { get { return this; } private set; }

    private static final String DEFAULT_REPORT_TYPE = 'Historical Daily Production';

    public transient String CSV { get; set; }

    public AMSHistoricalDailyProductionController() {
        selectedReportTypeName = DEFAULT_REPORT_TYPE;

        processPageParams();
    }

    private void processPageParams() {
        String monthStringParam = ApexPages.currentPage().getParameters().get(SELECTED_MONTH_PARAM);
        String selectedTabParam = ApexPages.currentPage().getParameters().get(SELECTED_TAB_PARAM);
        String selectedSeasonParam = ApexPages.currentPage().getParameters().get(SELECTED_SEASON_PARAM);

        if (String.isBlank(monthStringParam) || String.isBlank(selectedSeasonParam)) {
            return; //season is required for 'month' to work
        }
        if (!String.isBlank(selectedTabParam)) {
            setSelectedTab(selectedTabParam); //default the selected tab
        }

        Integer month = indexOf(AMSCollectionService.MONTH_NAMES, monthStringParam) + 1;

        if (!String.isBlank(selectedSeasonParam)) {
            this.selectedSeasonId = selectedSeasonParam;
        }

        setCurrentPriorSeason();

        this.fromDate = Date.newInstance(selectedFarmSeason.Reference_Period__r.Start_Date__c.year(), month, 1);

        if (this.fromDate < selectedFarmSeason.Reference_Period__r.Start_Date__c) {
            this.fromDate = Date.newInstance(selectedFarmSeason.Reference_Period__r.End_Date__c.year(), month, 1);
        }

        this.toDate = Date.newInstance(fromDate.year(), month, 1).addMonths(1).addDays(-1);

        populateDateStringFromDates();

        recalculateForm();
    }

    private void populateScopedDates() {
        if (monthList.size() < 2) {
            scopedCurrentSeasonsFromDate = fromDate;
            scopedCurrentSeasonsToDate = toDate;
        } else {
            Integer currentMonth = TODAY.month();
            Integer indexOfSelectedMonthInMonthList = indexOf(monthList, selectedMonth);

            //if the first month is selected, don't default the from date- cap the to date to the end of the month
            if (indexOfSelectedMonthInMonthList == 0) {
                scopedCurrentSeasonsFromDate = fromDate;
                scopedCurrentSeasonsToDate = fromDate.addMonths(indexOfSelectedMonthInMonthList + 1).toStartOfMonth().addDays(-1);
            //if not the last month, default the from date and to date to start and end of month
            } else if (indexOfSelectedMonthInMonthList != monthList.size() - 1) {
                scopedCurrentSeasonsFromDate = fromDate.addMonths(indexOfSelectedMonthInMonthList).toStartOfMonth();
                scopedCurrentSeasonsToDate = fromDate.addMonths(indexOfSelectedMonthInMonthList + 1).toStartOfMonth().addDays(-1);
            //if the last month, default the from date, don't default the end date
            } else {
                scopedCurrentSeasonsFromDate = fromDate.addMonths(indexOfSelectedMonthInMonthList).toStartOfMonth();
                scopedCurrentSeasonsToDate = toDate;
            }
        }

        scopedPriorSeasonsFromDate = scopedCurrentSeasonsFromDate.addYears(-1);
        scopedPriorSeasonsToDate = scopedCurrentSeasonsToDate.addYears(-1);
    }

    //Call recalculate for the forms
    public override void recalculateForm() {
        populateScopedDates();
        //all associated farm seasons for selected farm (first farm in list)
        collectionPeriodForm = new AMSCollectionService.CollectionPeriodForm(
            scopedCurrentSeasonsFromDate,
            scopedCurrentSeasonsToDate,
            scopedPriorSeasonsFromDate,
            scopedPriorSeasonsToDate,
            priorFarmSeason,
            selectedFarmSeason,
            selectedFarmId,
            AMSCollectionService.DAY);

        AMSCollectionService.CollectionPeriodForm graphForm = new AMSCollectionService.CollectionPeriodForm(
            fromDate,
            toDate,
            fromDatePriorSeason,
            toDatePriorSeason,
            priorFarmSeason,
            selectedFarmSeason,
            selectedFarmId,
            AMSCollectionService.DAY);

        CSV = new AMSProdQualCSVGenerator(graphForm, getProductionTable()).generateCSV();

        collectionGraph = new AMSCollectionGraph(graphForm, AMSCollectionService.GRAPH_SELECT_OPTIONS_TO_UNIT_OF_MEASUREMENT_MAP.get(selectedGraphOption));
    }
}