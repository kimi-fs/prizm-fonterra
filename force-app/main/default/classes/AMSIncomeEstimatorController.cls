/**
 * Description: Controller for Income Estimator
 * @author: Amelia (Trineo)
 * @date: October 2017
 */
public with sharing class AMSIncomeEstimatorController {
    public String renderingService {get; private set;}
    public static Date nextSeasonDate = Date.newInstance(2021, 7, 1);
    public Boolean currentSeasonTab {
        get;
        set {
            this.currentSeasonTab = value;
            if (value == true && farmSeasonSelectOptions != null && !farmSeasonSelectOptions.isEmpty()) {
                this.useNextSeasonRates = Date.today() >= nextSeasonDate;
                this.selectedFarmSeasonId = farmSeasonSelectOptions[0].getValue();
                setSelectedFarmSeason();
            }
        }
    }
    public static Boolean nextSeasonAdminRecordExists {
        get {
            if (nextSeasonAdminRecordExists == null) {
                nextSeasonAdminRecordExists = false;
                List<AMS_Income_Estimator_Admin_Data__c> customSettings = AMS_Income_Estimator_Admin_Data__c.getAll().values();
                Date nextSeasonStart = getNextSeasonStartDate();
                for (AMS_Income_Estimator_Admin_Data__c ad : customSettings) {
                    if (ad.Valid_From__c >= nextSeasonStart) {
                        nextSeasonAdminRecordExists = true;
                        break;
                    }
                }
            }
            return nextSeasonAdminRecordExists;
        }
        set;
    }
    public static Boolean showCurrentSeasonPriceBand {
        // Toggles the display of the price band the user has signed up to on their agreement 
        // This will only show after 1st July 2021
        get {
            return Date.today() >= nextSeasonDate; // Using a hardcoded date as this is going to be scrapped in the MA Project
        }
        set;
    }
    public Boolean noFarmsWithAccess {get; set;}
    public Boolean exceptionError {get; set;}
    public Boolean nonStandardAgreementError {get; set;}
    public Boolean collectionSummaryRecordBased {get; set;}

    // Farm select list
    public List<SelectOption> farmSelectOptions {get; set;}
    public Id selectedERelID {get; set;}

    // Company Milk Price select list
    public List<SelectOption> companyMilkPriceSelectOptions {get; set;}
    public Decimal companyMilkPrice {get; set;}
    public String selectedMilkPriceStep {get; set;}

    // Price band select list
    public List<SelectOption> priceBandSelectOptions {get; set;}
    public String currentPriceBand {get; set;}

    // Farm Season select list
    public List<SelectOption> farmSeasonSelectOptions {get; set;}
    public String selectedFarmSeasonId {get; set;}
    private List<Farm_Season__c> farmSeasons;
    public Farm_Season__c selectedFarmSeason {get; set;}

    public Boolean useNextSeasonRates {
        get;
        set{
            this.useNextSeasonRates = value;
            setCompanyMilkPricePickList();
        }
    }

    @testVisible
    private Map<Id, Entity_Relationship__c> entityRelationships;

    public AMSIncomeEstimatorDataService.IncomeEstimatorData table {get; private set;}

    // Used for creating the Company Milk Price select options
    private static final Decimal MILK_PRICE_STEP = 0.10;
    private static final Decimal MILK_PRICE_LOWER_LIMIT = 3.00;
    private static final Decimal MILK_PRICE_UPPER_LIMIT = 9.00;

    public AMSIncomeEstimatorController() {
        this.currentSeasonTab = true;
        this.exceptionError = false;
        this.collectionSummaryRecordBased = true;
        this.useNextSeasonRates = Date.today() >= nextSeasonDate || !currentSeasonTab;
        this.nonStandardAgreementError = false;

        this.entityRelationships = RelationshipAccessService.accessToEntityRelationships(AMSContactService.determineContactId(), RelationshipAccessService.Access.ACCESS_STATEMENTS);
        // select the first one when page loads
        if (!this.entityRelationships.isEmpty()) {
            noFarmsWithAccess = false;
            for (Id i : this.entityRelationships.keyset()) {
                this.selectedERelID = i;
                break;
            }

            this.farmSelectOptions = populateFarmSelectOptionsList();
            getFarmSeasonsAndGenerateTableFromCollectionSummaryRecords();
        } else {
            noFarmsWithAccess = true;
        }
        this.priceBandSelectOptions = populatePricingBandOptionList();
    }

    /**
    * Downloads a pdf file
    * Uses the pdf output panel from the page
    */
    public PageReference saveToPDF() {
        renderingService = 'PDF';
        ApexPages.currentPage().getHeaders().put('content-disposition', 'attachment; filename=' + generatePDFFileName());
        return null;
    }

    /**
    * Create the file name for the pdf file
    * Farm name + season + date time pdf is created
    * Strips any not allowed chars and replaces with a -
    */
    @testVisible
    private String generatePDFFileName() {
        String  fileName = 'Income-Estimator-';
        fileName += getFarmName() + '-';
        //fileName += getPartyName() + '-';
        fileName += selectedFarmSeason.Reference_Period__r.Reference_Period__c + '-';
        fileName += getCurrentDateTime();
        fileName = sanitiseFileName(fileName);
        fileName += '.pdf';

        return fileName;
    }

    /**
    * Date time stamp in Sydney time zone
    * Displayed on the PDF and added to the PDF file name
    */
    public String getCurrentDateTime() {
        return Datetime.now().format('dd/MM/yyyy HH:mm:ss', 'Australia/Sydney');
    }

    /**
    * Replaces any chars that are not letters or numbers with underscores
    */
    private String sanitiseFileName(String fileName) {
        return filename.replaceAll('[^0-9a-zA-Z]', '_');
    }

    public void setCompanyMilkPricePickList() {
        this.companyMilkPrice = getCompanyMilkPriceValue();
        this.companyMilkPriceSelectOptions = populateCompanyMilkPriceSelectOptionList(this.companyMilkPrice);
        this.selectedMilkPriceStep = '0';
    }

    /**
    * Gets a list of farms that the running user has access to see statements for
    * Uses Entity Relationships as we can then get the party name to display
    * Displayed on the page for both current season and scenario planner
    * Chaning this causes the whole table to be regenerated
    */
    public List<SelectOption> populateFarmSelectOptionsList() {
        List<SelectOption> farmSelectOptions = new List<SelectOption>();

        for (Entity_Relationship__c er : this.entityRelationships.values()) {
            farmSelectOptions.add(new SelectOption(er.Id, er.Farm__r.Name));
        }

        return farmSelectOptions;
    }

    /**
    * The farm name for the selected farm
    */
    public String getFarmName() {
        String farmName = '';
        if (String.isNotBlank(selectedERelID) && entityRelationships.containsKey(selectedERelID)) {
            farmName = entityRelationships.get(selectedERelID).Farm__r.Name;
        }
        return farmName;
    }

    /**
    * The party name for the selected farm
    * Displayed on the page
    */
    public String getPartyName() {
        String partyName = '';
        if (String.isNotBlank(selectedERelID) && entityRelationships.containsKey(selectedERelID)) {
            partyName = entityRelationships.get(selectedERelID).Party__r.Name;
        }
        return partyName;
    }

    /**
     * Get the pricing band the user has agreed to
     * Displayed on the current season tab
     */
    public String getPriceBand() {
        return getPricingBandOption();
    }

    /**
    * Gets the The Fonterra Announced Milk Price
    * Comes from AMS Income Estimator Admin Data custom setting
    * Displayed on the Current Season tab
    * The initially selected value for the company milk pirce pick list on the scenario planner tab
    */
    private Decimal getCompanyMilkPriceValue() {
        Decimal companyMilkPrice;

        List<AMS_Income_Estimator_Admin_Data__c> adminData = AMS_Income_Estimator_Admin_Data__c.getAll().values();
        Date settingDate = useNextSeasonRates ? getNextSeasonStartDate() : Date.today();
        for (AMS_Income_Estimator_Admin_Data__c ad : adminData) {
            if ((ad.Valid_From__c == null || ad.Valid_From__c <= settingDate) && (ad.Valid_To__c == null || ad.Valid_To__c >= settingDate )) {
                companyMilkPrice = ad.Company_Milk_Price__c;
            }
        }
        return companyMilkPrice;
    }

    /**
    * Gets the company milk price
    * Uses company milk price if current season tab
    * Finds the selected company milk price if scenario planner tab
    */
    public String getSelectedCompanyMilkPriceValue() {
        if (this.currentSeasonTab) {
            return '$' + String.valueOf(this.companyMilkPrice);
        } else {
            for (SelectOption so : companyMilkPriceSelectOptions) {
                if (so.getValue() == this.selectedMilkPriceStep) {
                    return so.getLabel();
                }
            }
            return '';
        }
    }

    /**
    * Creates the pick list for the Company Milk Price select options
    * Starts at the currently set Company Milk Price and adds or subtracts a fixed step value within defined ranges
    * Sorted numerically
    * Picklist for the scenario planner tab
    */
    private static List<SelectOption> populateCompanyMilkPriceSelectOptionList(Decimal companyMilkPrice) {
        List<SelectOption> companyMilkPriceSelectOptions = new List<SelectOption>();
        companyMilkPriceSelectOptions.add(new SelectOption(String.valueOf(0), formatCurrency(companyMilkPrice)));

        Decimal selectOptionValue = companyMilkPrice - MILK_PRICE_STEP;
        Integer stepCount = 0;
        while (selectOptionValue >= MILK_PRICE_LOWER_LIMIT) {
            stepCount --;
            companyMilkPriceSelectOptions.add(new SelectOption(String.valueOf(stepCount), formatCurrency(selectOptionValue)));
            selectOptionValue -= MILK_PRICE_STEP;
        }
        selectOptionValue = companyMilkPrice + MILK_PRICE_STEP;
        stepCount = 0;
        while (selectOptionValue <= MILK_PRICE_UPPER_LIMIT) {
            stepCount ++;
            companyMilkPriceSelectOptions.add(new SelectOption(String.valueOf(stepCount), formatCurrency(selectOptionValue)));
            selectOptionValue += MILK_PRICE_STEP;
        }

        AMSCommunityUtil.sortSelectOptionList(companyMilkPriceSelectOptions, AMSCommunityUtil.FieldToSort.Label);
        return companyMilkPriceSelectOptions;
    }

    /** 
     * Creates a pick list for the Price Band options
     * Options are stored on a global picklist on the Pricing Option field on Agreement Release
     * Picklist for the Scenario Planner tab
     */
    private static List<SelectOption> populatePricingBandOptionList() {
        List<SelectOption> priceBandOptions = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Agreement__c.Pricing_Option__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : ple) {
            priceBandOptions.add(new SelectOption(pickListVal.getValue(), pickListVal.getLabel()));
        }
        return priceBandOptions;
    }

    /**
     * Gets the start date for the next farming season
     * Farm seasons always start on the 1 July
     */
    private static Date getNextSeasonStartDate() {
        Date nextSeasonStart;
        List<Reference_Period__c> nextReferencePeriod = AMSReferencePeriodService.getFutureReferencePeriods(1);
        // if it doesnt exist let the exception error show on the page
        nextSeasonStart = nextReferencePeriod[0].Start_Date__c;
        return nextSeasonStart;
    }

    /**
     * String.valueOf() drops trailing 0's so format it ourselves.
     */
    private static String formatCurrency(Decimal dec) {
        String amount;
        if (!String.valueof(dec.format()).right(3).contains('.')) {
            amount = '$' + String.valueOf(dec.format()) + '.00';
        } else if (String.valueof(dec.format()).right(2).contains('.')) {
            amount = '$' + String.valueOf(dec.format()) + '0';
        } else {
            amount = '$' + String.valueOf(dec.format());
        }
        return amount;
    }
    /**
    * Gets the current farm season and all the previous ones for the farm
    */
    private void populateFarmSeasons() {
        this.farmSeasons = new List<Farm_Season__c>();
        System.debug('selectedERelID ' + selectedERelID);
        System.debug('entityRelationships ' + entityRelationships);

        if (String.isNotBlank(selectedERelID) && entityRelationships.containsKey(selectedERelID)) {
            this.farmSeasons = AMSFarmSeasonService.getCurrentFarmSeasonBackwards(entityRelationships.get(selectedERelID).Farm__c, 100);
            System.debug('farmSeasons: ' + this.farmSeasons);
            populateFarmSeasonSelectOptionList();
            setSelectedFarmSeason();
        }
    }

    /**
    * Gets farm seasons for the selected farm
    */
    private void populateFarmSeasonSelectOptionList() {
        this.farmSeasonSelectOptions = new List<SelectOption>();

        if (String.isNotBlank(selectedERelID) && entityRelationships.containsKey(selectedERelID)) {
            for (Farm_Season__c fs : this.farmSeasons) {
                this.farmSeasonSelectOptions.add(new SelectOption(fs.Id, fs.Reference_Period__r.Reference_Period__c));
            }
        }
    }

    /**
    * Sets the selected farm season from the selectedFarmSeasonId value in the picklist
    */
    public void setSelectedFarmSeason() {
        // if none have been selected select the first one
        if (String.isBlank(this.selectedFarmSeasonId) && !farmSeasons.isEmpty()) {
            this.selectedFarmSeasonId = farmSeasons[0].Id;
        }
        if (this.farmSeasons != null && String.isNotBlank(this.selectedFarmSeasonId)) {
            for (Farm_Season__c fs : this.farmSeasons) {
                if (this.selectedFarmSeasonId == fs.Id) {
                    this.selectedFarmSeason = fs;
                    break;
                }
            }
        }
    }

    /**
    * Queries the Farm Season records, creates the select list and sets the most recent season as the selected season
    * Called when Farm Select option is changed
    */
    public void getFarmSeasonsAndGenerateTableFromCollectionSummaryRecords() {
        populateFarmSeasons();
        generateTableFromCollectionSummaryRecords();
    }

    /**
    * Sets the selected farm season and generates the table
    * Called when Farm Season select list is changed
    */
    public void setSelectedFarmSeasonAndGenerateTableFromCollectionSummaryRecords() {
        setSelectedFarmSeason();
        generateTableFromCollectionSummaryRecords();
    }

    /**
     * Switches tabs between current season and scenario planner
     * Sets use next season rates to true if on scenario planner and next season admin record exits
     */
    public void switchTabs() {
        this.useNextSeasonRates = (this.currentSeasonTab && showCurrentSeasonPriceBand) || (!this.currentSeasonTab && nextSeasonAdminRecordExists) ? true : false;
        generateTableFromCollectionSummaryRecords();
    }

    /**
    * Creates requests from a list of Collection Summary records
    */
    private List<AMSIncomeEstimatorDataService.MonthRequest> createMonthRequestsFromColletionSummaryRecords(List<Collection_Summary__c> collectionSummaries) {
        List<AMSIncomeEstimatorDataService.MonthRequest> monthRequests = new List<AMSIncomeEstimatorDataService.MonthRequest>();

        Date seasonStart = this.selectedFarmSeason.Reference_Period__r.Start_Date__c;
        Season__c contractSeason = getContractSeason();

        for (Collection_Summary__c cs : collectionSummaries) {

            // Retrieve Contracted value for specific month
            Decimal contractedFat = 0;
            Decimal contractedProtein = 0;
            Decimal actualWDMCPaymentAmount;

            if (contractSeason != null) {
                contractedFat = (Decimal) contractSeason.get('Contracted_Fat_' + cs.Collection_Summary_Date__c.month() + '__c');
                contractedProtein = (Decimal) contractSeason.get('Contracted_Protein_' + cs.Collection_Summary_Date__c.month() + '__c');
                actualWDMCPaymentAmount = (Decimal) contractSeason.get('Actual_Payment_Amount_' + cs.Collection_Summary_Date__c.month() + '__c');
            }

            AMSIncomeEstimatorDataService.MonthRequest newRequest = new AMSIncomeEstimatorDataService.MonthRequest(
                cs.Collection_Summary_Date__c >= seasonStart ? AMSIncomeEstimatorDataService.MonthType.ACTUAL : AMSIncomeEstimatorDataService.MonthType.LAST_SEASON,
                cs.Collection_Summary_Date__c >= seasonStart ? cs.Collection_Summary_Date__c : cs.Collection_Summary_Date__c.addYears(1),
                cs.Volume_Ltrs__c,
                cs.Fat_Percent__c != null ? cs.Fat_Percent__c : 0,
                cs.Prot_Percent__c != null ? cs.Prot_Percent__c : 0,
                cs.Stop_1__c,
                cs.Stop_2__c,
                // for scenario planner perfect quality is assumed
                this.currentSeasonTab ? cs.Quality_Points__c : 0,
                // previously cs.Volume_Charge_Rate__c. Setting volume charge to 0 temporarily as Volume Charge may later come back in scope.
                0,
                cs.Total_KgMS_Produced_by_Linked_Farms__c,
                contractedFat,
                contractedProtein,
                this.currentSeasonTab ? actualWDMCPaymentAmount : null
            );
            newRequest.priceBand = this.currentPriceBand;
            newRequest.useNextSeasonRates = (this.useNextSeasonRates || (this.currentSeasonTab && Date.today() >= nextSeasonDate));
            monthRequests.add(newRequest);
        }
        return monthRequests;
    }

    /**
     * Find what tab the user is in and return the correct pricing option
     * If in current season tab, return the option from their agreement
     * If in the scenario planner return the base 7/5 as default
     */
    public String getPricingBandOption() {
        if(currentSeasonTab) {
            if (String.isNotBlank(selectedERelID) && entityRelationships.containsKey(selectedERelID)) {
                Id farmId = entityRelationships.get(selectedERelID).Farm__c;
                return RelationshipAccessService.getActiveAgreementPriceOption(farmId);
            }
        }

        // In the future tab, so just return 7/5
        return '7/5';
    }

    /**
    * Queries the collection summary records, creates month requests from them and generates the table
    */
    public void generateTableFromCollectionSummaryRecords() {
        this.currentPriceBand = getPricingBandOption();

        this.collectionSummaryRecordBased = true;
        this.selectedMilkPriceStep = '0';
        List<Collection_Summary__c> collectionSummaries = getCollectionSummaryRecords();
        List<AMSIncomeEstimatorDataService.MonthRequest> monthRequests = createMonthRequestsFromColletionSummaryRecords(collectionSummaries);
        generateTable(monthRequests);
    }

    /**
    * Creates sharefarmer split requests from queried payment split records
    * Always used the active splits at the current time (doesn't matter if they were different for previous months)
    */
    private List<AMSIncomeEstimatorDataService.SharefarmerSplitRequest> createSharefarmerSplitRequests() {
        List<String> shareFarmerPaymentSplitTypes = new List<String>{'Override', 'Dollars/KgMS'};

        List<AMSIncomeEstimatorDataService.SharefarmerSplitRequest> sharefarmerSplitRequests = new List<AMSIncomeEstimatorDataService.SharefarmerSplitRequest>();

        if (String.isNotBlank(selectedERelID) && entityRelationships.containsKey(selectedERelID)) {
            List<Payment_Split__c> paymentSplits = AMSPaymentSplitService.getPaymentSplits(entityRelationships.get(selectedERelID).Farm__c);
            for (Payment_Split__c ps : paymentSplits) {
                if (shareFarmerPaymentSplitTypes.contains(ps.Payment_Split_Type__c)) {
                    this.nonStandardAgreementError = true;
                    break;
                }
                AMSIncomeEstimatorDataService.SharefarmerSplitRequest ssr = new AMSIncomeEstimatorDataService.SharefarmerSplitRequest(ps.Id, ps.Party__r.Name, ps.Percentage__c);
                sharefarmerSplitRequests.add(ssr);
            }
        }
        return sharefarmerSplitRequests;
    }

    /**
    * Creates requests from values updated in the existing table on the page
    * West fresh only has inputs on scenario planner and only input for the first month with gets duplicated across subsequent months
    */
    private List<AMSIncomeEstimatorDataService.MonthRequest> createMonthRequestsFromInputsOnPage() {
        List<AMSIncomeEstimatorDataService.MonthRequest> monthRequests = new List<AMSIncomeEstimatorDataService.MonthRequest>();

        for (AMSIncomeEstimatorDataService.MonthData existingMonth : table.monthData) {
            monthRequests.add(new AMSIncomeEstimatorDataService.MonthRequest(existingMonth, this.currentPriceBand, this.useNextSeasonRates));
        }
        return monthRequests;
    }

    /**
    * Creates month requests from from values updated in the existing table on the page and generates the table
    */
    public void generateTableFromInputsOnPage() {
        this.collectionSummaryRecordBased = false;

        List<AMSIncomeEstimatorDataService.MonthRequest> monthRequests = createMonthRequestsFromInputsOnPage();
        generateTable(monthRequests);
    }

    /**
    * Generates the table from a list of month requests
    */
    private void generateTable(List<AMSIncomeEstimatorDataService.MonthRequest> monthRequests) {
        this.exceptionError = false;
        this.nonStandardAgreementError = false;

        List<AMSIncomeEstimatorDataService.SharefarmerSplitRequest> paymentSplits = createSharefarmerSplitRequests();
        Id farmId = entityRelationships.get(selectedERelID).Farm__c;
        Id partyId = entityRelationships.get(selectedERelID).Party__c;
        Agreement__c latestAgreement = AgreementService.getLatestAgreement(farmId, partyId, GlobalUtility.agreementStandardRecordTypeId, null, null);
        String specialtyMilkPrice = '';
        if (latestAgreement != null) {
            specialtyMilkPrice = latestAgreement.Specialty_Milk_Price__c;
        }
        Integer companyMilkPriceStep = getCompanyMilkPriceStep();
        String farmRegion = getFarmRegion();

        AMSIncomeEstimatorDataService.IncomeEstimatorRequest request = new AMSIncomeEstimatorDataService.IncomeEstimatorRequest(this.currentSeasonTab, this.useNextSeasonRates, specialtyMilkPrice, companyMilkPriceStep, farmRegion, monthRequests, paymentSplits);
        try {
            table = AMSIncomeEstimatorDataService.getIncomeEstimatorData(request);
        } catch (AMSIncomeEstimatorCalculationService.AMSIncomeEstimatorCalculationException calculationException) {
            this.exceptionError = true;
            System.debug('Calculation exception caught: ' + calculationException.getMessage());
        } catch (Exception e) {
            this.exceptionError = true;
            System.debug('Exception caught: ' + e.getMessage());
        }
    }

    /**
    * Sets the company milk price step from the selected value
    * If it cant be converted to a Decimal it uses 0
    */
    private Integer getCompanyMilkPriceStep() {
        Integer step = 0;
        try {
            step = Integer.valueOf(this.selectedMilkPriceStep);
        } catch (Exception e) {
            step = 0;
        }
        return step;
    }

    /**
    * Gets the region of the selected farm
    */
    private String getFarmRegion() {
        String farmRegion;
        if (String.isNotBlank(selectedERelID) && entityRelationships.containsKey(selectedERelID)) {
            farmRegion = entityRelationships.get(selectedERelID).Farm__r.Farm_Region__r.Name;
        }
        return farmRegion;
    }

    /**
    * Gets the collection summary records and orders them from June - May
    * Creates empty ones if none exist for that month
    */
    private List<Collection_Summary__c> getCollectionSummaryRecords() {
        List<Collection_Summary__c> collectionSummaries = new List<Collection_Summary__c>();

        System.debug('this.selectedFarmSeason: ' + this.selectedFarmSeason);

        if (String.isNotBlank(selectedERelID) && entityRelationships.containsKey(selectedERelID)) {
            String farmId = entityRelationships.get(selectedERelID).Farm__c;
            Date seasonStart = this.selectedFarmSeason.Reference_Period__r.Start_Date__c;
            Date seasonEnd = this.selectedFarmSeason.Reference_Period__r.End_Date__c;

            if (currentSeasonTab || seasonEnd > Date.today()) {
                // We get 13 months in case the previous months data is not yet available, in which case we use the
                // collection summary from 14 months ago.
                collectionSummaries = AMSCollectionSummaryService.getCollectionSummariesLastFourteenMonths(farmId);
                System.debug('Getting last 14 summaries, season start ' + seasonStart + ' End ' + seasonEnd);
            } else {
                collectionSummaries = AMSCollectionSummaryService.getCollectionSummaries(farmId, seasonStart, seasonEnd);
                System.debug('Getting summaries between season start ' + seasonStart + ' End ' + seasonEnd);
            }
        }

        // Ordering the collection summaries from June - May
        Map<Integer, Collection_Summary__c> collectionSummariesMonthMap = new Map<Integer, Collection_Summary__c>();
        for (Collection_Summary__c cs : collectionSummaries) {
            collectionSummariesMonthMap.put(cs.Collection_Summary_Date__c.month(), cs);
            System.debug('Collection Summary: ' + cs);
        }

        // The volume charge rate for future months should be pinned to the latest actual volume charge
        // rate and not reuse the charge rate from last year

        // The following is kinda complex looking, we work our way forwards through the year looking for
        // the last non 0.0 volume charge rate before the current month. We then use that for the rest of
        // the year.
        //
        // Don't use this months data since we won't have it yet.
        Decimal latestVolumeCharge;
        Integer thisMonth = Date.today().month();
        Date recentThreshold = Date.today().addMonths(-6);
        Boolean reachedThisMonth = false;

        // Months are Jul -> Jun (7 -> 12, 1 -> 6)
        //
        // Check the first part of the year first, and then if required the second half
        for (Integer i = 7; i <= 12; i++) {
            if (i == thisMonth) {
                reachedThisMonth = true;
                break;
            }
            if (collectionSummariesMonthMap.containsKey(i)) {
                Collection_Summary__c cs = collectionSummariesMonthMap.get(i);
                if (cs.Collection_Summary_Date__c < recentThreshold) {
                    // Skip old values
                    continue;
                }

                Decimal lastVolumeCharge = cs.Volume_Charge_Rate__c;
                if (lastVolumeCharge != 0.0) {
                    latestVolumeCharge = lastVolumeCharge;
                }
            }
        }

        if (!reachedThisMonth || latestVolumeCharge == null) {
            // Still searching... either this month is in Jan-Jun or we were in July and so need to use the latest value from Jan-Jun
            for (Integer i = 1; i <= 6; i++) {
                if (i == thisMonth) {
                    reachedThisMonth = true;
                    break;
                }
                if (collectionSummariesMonthMap.containsKey(i)) {
                    Collection_Summary__c cs = collectionSummariesMonthMap.get(i);
                    if (cs.Collection_Summary_Date__c < recentThreshold) {
                        // Skip old values
                        continue;
                    }

                    Decimal lastVolumeCharge = cs.Volume_Charge_Rate__c;
                    if (lastVolumeCharge != 0.0) {
                        latestVolumeCharge = lastVolumeCharge;
                    }
                }
            }
        }

        Date seasonStart = this.selectedFarmSeason.Reference_Period__r.Start_Date__c;

        // first half, so update what is left of the first half, and then all of the second half
        for (Integer i = 1; i <= 12; i++) {
            if (collectionSummariesMonthMap.containsKey(i)) {
                Collection_Summary__c cs = collectionSummariesMonthMap.get(i);

                if (cs.Collection_Summary_Date__c < seasonStart) {
                    if (latestVolumeCharge != null) {
                        cs.Volume_Charge_Rate__c = latestVolumeCharge;
                    }
                    cs.Quality_Points__c = 0;
                }
            }
        }

        List<Collection_Summary__c> collectionSummariesOrdered = new List<Collection_Summary__c>();
        if (this.selectedFarmSeason != null) {
            for (Integer i = 7; i <= 12; i++) {
                if (collectionSummariesMonthMap.containsKey(i)) {
                    collectionSummariesOrdered.add(collectionSummariesMonthMap.get(i));
                } else {
                    collectionSummariesOrdered.add(createEmptyCollectionSummary(i, this.selectedFarmSeason.Reference_Period__r.Start_Date__c.year()));
                }
            }
            for (Integer i = 1; i <= 6; i++) {
                if (collectionSummariesMonthMap.containsKey(i)) {
                    collectionSummariesOrdered.add(collectionSummariesMonthMap.get(i));
                } else {
                    collectionSummariesOrdered.add(createEmptyCollectionSummary(i, this.selectedFarmSeason.Reference_Period__r.End_Date__c.year()));
                }
            }
        }
        return collectionSummariesOrdered;
    }

    /**
    * Creates a collection summary record with 0 values to be used as months where collection summary data does not exist,
    * using last years date so that it doesn't look like an actual for the future.
    */
    private Collection_Summary__c createEmptyCollectionSummary(Integer month, Integer year) {
        Date csDate = Date.newInstance(year, month, 1).addYears(-1);

        Collection_Summary__c cs = new Collection_Summary__c();

        cs.Collection_Summary_Date__c = csDate;
        cs.Volume_Ltrs__c = 0;
        cs.Stop_1__c = 0;
        cs.Stop_2__c = 0;
        cs.Quality_Points__c = 0;
        cs.Volume_Charge_Rate__c = 0;
        cs.Total_KgMS_Produced_by_Linked_Farms__c = 0;

        return cs;
    }


    private Season__c getContractSeason() {

        Date seasonStart = this.selectedFarmSeason.Reference_Period__r.Start_Date__c;
        Date seasonEnd = this.selectedFarmSeason.Reference_Period__r.End_Date__c;

        Id farmId = entityRelationships.get(selectedERelID).Farm__c;
        Id partyId = entityRelationships.get(selectedERelID).Party__c;
        Agreement__c contractedAgreement = AgreementService.getLatestAgreement(farmId, partyId, GlobalUtility.agreementContractSupplyAURecordTypeId, seasonStart, seasonEnd);

        if (contractedAgreement != null && !contractedAgreement.Agreement_Seasons__r.isEmpty()) {
            return contractedAgreement.Agreement_Seasons__r[0];
        }
        return null;
    }

}