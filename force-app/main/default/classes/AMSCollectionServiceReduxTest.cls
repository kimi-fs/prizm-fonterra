/**
 * Description:
 *
 * Test for AMSCollectionServiceRedux
 *
 * @author: John Au (Trineo)
 * @date: Sep 2018
 */
@IsTest
private class AMSCollectionServiceReduxTest {

    private static final Date TODAY = Date.today();
    private static final Date TOMORROW = Date.today().addDays(1);
    private static final Integer COLLECTIONS_TO_CREATE_PER_DAY = 5;

    private static final Decimal VOLUME_LITRES_PER_COLLECTION = 100.00;
    private static final Decimal FAT_KGS_PER_COLLECTION = 50.00;
    private static final Decimal PROTEIN_KGS_PER_COLLECTION = 50.00;
    private static final Decimal KGMS_PER_COLLECTION = 50.00;

    @TestSetup
    private static void testSetup() {
        TestClassDataUtil.individualDefaultAccountAu();
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.createIntegrationUser();
    }

    private static Account getFarmAfterCreationOfFarmsAndIndividualsAu(User communityUser) {
        List<Account> farms = [
            SELECT Id
            FROM Account
            WHERE Id IN (
                SELECT Farm__c
                FROM Individual_Relationship__c
                WHERE Individual__c = :communityUser.ContactId
                AND Active__c = true
            )
        ];

        return farms[0];
    }

    private static List<Collection__c> createCollections(Account farm) {
        List<Collection__c> collections = new List<Collection__c>();

        Collection__c collection = TestClassDataUtil.createSingleCollection(farm, false);
        collection.Pick_Up_Date__c = TODAY;
        collection.Volume_Ltrs__c = VOLUME_LITRES_PER_COLLECTION;
        collection.Fat_Kgs__c = FAT_KGS_PER_COLLECTION;
        collection.Prot_Kgs__c = PROTEIN_KGS_PER_COLLECTION;

        for (Integer i = 0; i < COLLECTIONS_TO_CREATE_PER_DAY; i++) {
            collections.add(collection.clone(false, true, true, false));
        }

        collection.Pick_Up_Date__c = TOMORROW;
        collection.Volume_Ltrs__c = VOLUME_LITRES_PER_COLLECTION;
        collection.Fat_Kgs__c = FAT_KGS_PER_COLLECTION;
        collection.Prot_Kgs__c = PROTEIN_KGS_PER_COLLECTION;

        for (Integer i = 0; i < COLLECTIONS_TO_CREATE_PER_DAY; i++) {
            collections.add(collection.clone(false, true, true, false));
        }

        insert (collections);
        return collections;
    }


    @IsTest
    private static void testQueryAndCountCollections() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
        Account farm = getFarmAfterCreationOfFarmsAndIndividualsAu(communityUser);
        List<Collection__c> collections = createCollections(farm);
        List<Collection__c> queriedCollections = AMSCollectionServiceRedux.queryCollections(new List<Id> { farm.Id }, TODAY, TOMORROW);
        Integer collectionCount = AMSCollectionServiceRedux.countCollections(new List<Id> { farm.Id }, TODAY, TOMORROW);

        System.assertEquals(collections.size(), queriedCollections.size());
        System.assertEquals(collections.size(), collectionCount);
    }

    @IsTest
    private static void testGetDailyAggregatesForFarms() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
        Account farm = getFarmAfterCreationOfFarmsAndIndividualsAu(communityUser);
        List<Collection__c> collections = createCollections(farm);
        List<AMSCollectionServiceRedux.CollectionAggregate> queriedCollections = AMSCollectionServiceRedux.getAggregatesForFarms(new List<Id> { farm.Id }, TODAY, TOMORROW, AMSCollectionServiceRedux.AggregateType.DAILY);

        System.assertEquals(2, queriedCollections.size());

        System.assertEquals(VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY, queriedCollections[0].volumeLitresTotal);
        System.assertEquals(FAT_KGS_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY, queriedCollections[0].fatKgsTotal);
        System.assertEquals(PROTEIN_KGS_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY, queriedCollections[0].proteinKgsTotal);
        System.assertEquals(VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY, queriedCollections[0].kgMSTotal);
        System.assertEquals((FAT_KGS_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY / (VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY)) * VOLUME_LITRES_PER_COLLECTION, queriedCollections[0].fatPercentageTotal);
        System.assertEquals((PROTEIN_KGS_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY / (VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY)) * VOLUME_LITRES_PER_COLLECTION, queriedCollections[0].proteinPercentageTotal);
        System.assertEquals((VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY / (VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY)) * VOLUME_LITRES_PER_COLLECTION, queriedCollections[0].kgMSPercentageTotal);
    }

    @IsTest
    private static void testGetMonthlyAggregatesForFarms() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
        Account farm = getFarmAfterCreationOfFarmsAndIndividualsAu(communityUser);
        List<Collection__c> collections = createCollections(farm);
        List<AMSCollectionServiceRedux.CollectionAggregate> queriedCollections = AMSCollectionServiceRedux.getAggregatesForFarms(new List<Id> { farm.Id }, TODAY, TODAY, AMSCollectionServiceRedux.AggregateType.MONTHLY);

        System.assertEquals(1, queriedCollections.size());

        System.assertEquals(VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY, queriedCollections[0].volumeLitresTotal);
        System.assertEquals(FAT_KGS_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY, queriedCollections[0].fatKgsTotal);
        System.assertEquals(PROTEIN_KGS_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY, queriedCollections[0].proteinKgsTotal);
        System.assertEquals(VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY, queriedCollections[0].kgMSTotal);
        System.assertEquals((FAT_KGS_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY / (VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY)) * VOLUME_LITRES_PER_COLLECTION, queriedCollections[0].fatPercentageTotal);
        System.assertEquals((PROTEIN_KGS_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY / (VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY)) * VOLUME_LITRES_PER_COLLECTION, queriedCollections[0].proteinPercentageTotal);
        System.assertEquals((VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY / (VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY)) * VOLUME_LITRES_PER_COLLECTION, queriedCollections[0].kgMSPercentageTotal);
    }

    @IsTest
    private static void testGetTotalAggregatesForFarms() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
        Account farm = getFarmAfterCreationOfFarmsAndIndividualsAu(communityUser);
        List<Collection__c> collections = createCollections(farm);
        AMSCollectionServiceRedux.CollectionAggregate totalAggregate = AMSCollectionServiceRedux.getTotalAggregateForFarms(new List<Id> { farm.Id }, TODAY, TOMORROW);

        System.assertEquals(VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY * 2, totalAggregate.volumeLitresTotal);
        System.assertEquals(FAT_KGS_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY * 2, totalAggregate.fatKgsTotal);
        System.assertEquals(PROTEIN_KGS_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY * 2, totalAggregate.proteinKgsTotal);
        System.assertEquals(VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY * 2, totalAggregate.kgMSTotal);
        System.assertEquals((FAT_KGS_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY / (VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY)) * 100, totalAggregate.fatPercentageTotal);
        System.assertEquals((PROTEIN_KGS_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY / (VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY)) * 100, totalAggregate.proteinPercentageTotal);
        System.assertEquals((VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY / (VOLUME_LITRES_PER_COLLECTION * COLLECTIONS_TO_CREATE_PER_DAY)) * 100, totalAggregate.kgMSPercentageTotal);
    }

}