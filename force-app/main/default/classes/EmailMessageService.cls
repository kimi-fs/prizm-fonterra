/**
 * Description: Service for sending email message
 * @author: Irish (Trineo)
 * @date: May 2020
 * @test: AgreementServiceTest
 */
public without sharing class EmailMessageService {

    /**
    * Method to create email message to be sent to the individuals
    */
    public static List<Messaging.SingleEmailMessage> createEmailMessageForIndividuals(
        List<Individual_Relationship__c> individuals,
        List<Id> contentDocumentIds,
        String templateDeveloperName,
        Id orgWideEmailId
    ) {

        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage> ();
        List<Messaging.EmailFileAttachment> fileAttachments = documentsToEmailFileAttachments(contentDocumentIds);

        for (Individual_Relationship__c owner : individuals) {
            List<String> ccAddressesList = new List<String>();
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();

            if (String.isNotEmpty(owner.Farm__r.Rep_Area_Manager__c)) {
                ccAddressesList.add(owner.Farm__r.Rep_Area_Manager__r.Email);
            }
            if (String.isNotEmpty(owner.Farm__r.Regional_Manager__c)) {
                ccAddressesList.add(owner.Farm__r.Regional_Manager__r.Email);
            }
            if (String.isEmpty(owner.Individual__r.Email)) {
                continue;
            }

            message = createSingleEmailMessage(owner.Individual__c, orgWideEmailId, false, new List<String> {owner.Individual__r.Email}, ccAddressesList, fileAttachments);
            message.setTemplateID(GlobalUtility.getEmailTemplate(templateDeveloperName).Id);
            messages.add(message);
        }
       return messages;
    }

    /**
    * Method to get documents for email attachment
    */
    public static List<Messaging.EmailFileAttachment> documentsToEmailFileAttachments(List<Id> contentDocumentIds) {
        List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();

        Map<String, String> mimeTypeMap = GlobalUtility.getMIMETypeMap();

        List<ContentVersion> agreementFiles = [SELECT Id, Title, FileType, VersionData, isLatest, ContentDocumentId, ContentDocument.FileType
                                               FROM ContentVersion
                                               WHERE isLatest = true AND ContentDocumentId IN :contentDocumentIds];

        for (ContentVersion file: agreementFiles) {
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            attachment.setBody(file.VersionData);
            attachment.setFileName(file.Title);
            if (mimeTypeMap.containsKey(file.FileType)) {
                attachment.setContentType(mimeTypeMap.get(file.FileType));
            }
            attachments.add(attachment);
        }

        return attachments;

    }

    /**
    * Method to create single email message
    */
    public static Messaging.SingleEmailMessage createSingleEmailMessage(Id targetObjectId, Id orgWideEmailId, Boolean setSaveAsActivity, List<String> listToAddress, List<String> listToCcAddress, List<Messaging.EmailFileAttachment> fileAttachments) {

        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.setTargetObjectId(targetObjectId);
        message.setOrgWideEmailAddressId(orgWideEmailId);
        message.setSaveAsActivity(setSaveAsActivity);
        message.toAddresses = listToAddress;
        message.ccaddresses = listToCcAddress;
        if (!fileAttachments.isEmpty()) {
            message.setFileAttachments(fileAttachments);
        }

        return message;
    }

}