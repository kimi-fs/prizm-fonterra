/**
 * Superclass for all Daily and Monthly reports
 *
 * Allows the tables to be split out into a separate component (they need an 'interface' so we can pass the controllers in)
 *
 * Author: John Au (Trineo)
 * Date: 2017-10-10
 */
public abstract class AMSProductionReport extends AMSProdQualReport {

    protected final String PRODUCTION_TAB_NAME = 'production';
    protected final String QUALITY_TAB_NAME = 'quality';
    protected final String PROTEIN_AND_FAT_TAB_NAME = 'protein-and-fat';
    protected final String OTHER_TAB_NAME = 'other';

    public class ProductionTable {
        public ProductionTableTab productionTab { get; set; }
        public ProductionTableTab qualityTab { get; set; }
        public ProductionTableTab proteinAndFatTab { get; set; }
        public ProductionTableTab otherTab { get; set; }

        public Boolean showPreviousSeasonColumns { get; set; }
        public Boolean showTabs { get; set; }

        public List<ProductionTableTab> tabs { get; set; }

        public ProductionTable(ProductionTableTab productionTab, ProductionTableTab qualityTab, ProductionTableTab proteinAndFatTab, ProductionTableTab otherTab, Boolean showPreviousSeasonColumns, Boolean showTabs) {
            this.productionTab = productionTab;
            this.qualityTab = qualityTab;
            this.proteinAndFatTab = proteinAndFatTab;
            this.otherTab = otherTab;

            this.showPreviousSeasonColumns = showPreviousSeasonColumns;
            this.showTabs = showTabs;

            this.tabs = new List<ProductionTableTab> { productionTab, qualityTab, proteinAndFatTab, otherTab };
        }
    }

    public class ProductionTableTab {
        public String tabType { get; set; }
        public Boolean showAverages { get; set; }
        public Boolean showTotals { get; set; }

        public List<ProductionTableColumn> columns { get; set; }

        public ProductionTableTab(String tabType, Boolean showAverages, Boolean showTotals, List<ProductionTableColumn> columns) {
            this.tabType = tabType;
            this.showAverages = showAverages;
            this.showTotals = showTotals;

            this.columns = columns;
        }
    }

    public class ProductionTableColumn {
        public AMSCollectionService.UnitOfMeasurement unitOfMeasurement { get; set; }

        public ProductionTableColumn(AMSCollectionService.UnitOfMeasurement unitOfMeasurement) {
            this.UnitOfMeasurement = unitOfMeasurement;
        }
    }

    public virtual ProductionTable getProductionTable() {
        return new AMSProductionReport.ProductionTable(
            new AMSProductionReport.ProductionTableTab(
                PRODUCTION_TAB_NAME,
                true,
                true,
                new List<AMSProductionReport.ProductionTableColumn> {
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.Litres),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.KgMS),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.TotalKgMS),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.KgMSPercentage),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.BMCC)
                }
            ),
            new AMSProductionReport.ProductionTableTab(
                QUALITY_TAB_NAME,
                true,
                false,
                new List<AMSProductionReport.ProductionTableColumn> {
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.BMCC),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.Temp),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.OtherTestResult)
                }
            ),
            new AMSProductionReport.ProductionTableTab(
                PROTEIN_AND_FAT_TAB_NAME,
                true,
                true,
                new List<AMSProductionReport.ProductionTableColumn> {
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.ProteinFatRatio),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.FatPercentage),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.ProteinPercentage),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.FatKg),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.ProteinKg)
                }
            ),
            new AMSProductionReport.ProductionTableTab(
                OTHER_TAB_NAME,
                true,
                true,
                new List<AMSProductionReport.ProductionTableColumn> {
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.KgMSPerCow),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.KgMSPerHectare)
                }
            ),
            true,
            true
        );
    }

    public AMSCollectionService.CollectionPeriodForm collectionPeriodForm { get; set; }
    public AMSCollectionGraph collectionGraph { get; set; }

    public override void initProperties() {
        setSelectedGraphOption(AMSCollectionService.graphSelectOptions[0].getValue());
    }

    public String selectedGraphOption;

    public String getSelectedGraphOption() {
        return selectedGraphOption;
    }

    public virtual void setSelectedGraphOption(String selectedGraphOption) {
        this.selectedGraphOption = selectedGraphOption;
        selectedTab = AMSCollectionService.GRAPH_SELECT_OPTIONS_TO_TAB_NAME_MAP.get(selectedGraphOption);
    }

    public String selectedTab;

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
        setSelectedGraphOption(AMSCollectionService.TAB_NAME_TO_GRAPH_SELECT_OPTION_MAP.get(selectedTab));
        recalculateForm();
    }

    public String getSelectedTab() {
        return selectedTab;
    }
}