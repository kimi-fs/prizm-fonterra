/*------------------------------------------------------------
Author:         Sean Soriano
Company:        Davanti Consulting
Description:    Trigger Handler for ReferencePeriodTrigger
Test Class:     BatchSetReferencePeriodStatus_Test
History
25/01/2016    Sean Soriano    Created
------------------------------------------------------------*/
public class ReferencePeriodTriggerHandler {

	public static void setReferencePeriodStatus(List<Reference_Period__c> lRefPeriods) {

		Date dtToday = system.today();

		for(Reference_Period__c rp : lRefPeriods) {
			//Check dates for current status
			if(dtToday >= rp.Start_Date__c && (dtToday <= rp.End_Date__c || rp.End_Date__c == null)) {
				rp.Status__c = 'Current';
			//Check dates for past status
			} else if(dtToday > rp.Start_Date__c && dtToday > rp.End_Date__c && rp.End_Date__c != null) {
				rp.Status__c = 'Past';
			//Check dates for future status
			} else if(dtToday < rp.Start_Date__c && (dtToday < rp.End_Date__c || rp.End_Date__c == null)) {
				rp.Status__c = 'Future';
			}
		}
	}
}