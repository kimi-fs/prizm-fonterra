/*
* Test for AMSPricingController
* @author: John Au (Trineo)
* @date: 20/04/2020
*/
@IsTest
private class AMSPricingControllerTest {

    @TestSetup
    private static void testSetup() {
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.integrationUserProfile();
    }

    @IsTest
    private static void testCaseCreationGuest() {
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser) {
            AMSPricingController controller = new AMSPricingController();

            controller.region = 'North';
            controller.supplierType = 'Current';
            controller.farmNumber = 'Bob';
            controller.name = 'Bob';
            controller.phoneNumber = '0234567890';

            controller.attachment.fileName = 'bob.pdf';
            controller.attachment.content = Blob.valueOf('bob');

            controller.createCase();
        }
        List<Case> cases = [SELECT Id, OwnerId FROM Case];
        System.assertEquals(1, cases.size());
        System.assertEquals(cases[0].OwnerId, GlobalUtility.getQueueIdFromDeveloperName('Supplier_Service_Centre_AU'));
        System.assertEquals(1, [SELECT COUNT() FROM Attachment]);
    }

    @IsTest
    private static void testCaseCreationLoggedIn() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();

        Account party = [SELECT Id FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountPartyRecordTypeId LIMIT 1];
        Account farm = [SELECT Id FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId LIMIT 1];
        farm.Farm_Region_Name__c = 'North';
        update farm;

        TestClassDataUtil.createDerivedRelationship(communityUser.ContactId, party.Id, farm.Id, true);
        Test.startTest();
        System.runAs(communityUser) {
            AMSPricingController controller = new AMSPricingController();

            System.assert(!controller.farmSelectOptions.isEmpty());

            controller.region = 'North';
            controller.farmNumber = controller.farmSelectOptions[0].getValue();
            controller.name = 'Bob';
            controller.phoneNumber = '0234567890';

            controller.attachment.fileName = 'bob.pdf';
            controller.attachment.content = Blob.valueOf('bob');

            controller.createCase();
        }
        Test.stopTest();

        List<Case> cases = [SELECT Id, OwnerId FROM Case];
        System.assertEquals(1, cases.size());
        System.assertEquals(cases[0].OwnerId, GlobalUtility.getQueueIdFromDeveloperName('Regional_Team_North_AU'));
        System.assertEquals(1, [SELECT COUNT() FROM Attachment]);
    }

    @IsTest
    private static void testGoToAMSSignAgreementPage() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();

        Account party = [SELECT Id FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountPartyRecordTypeId LIMIT 1];
        Account farm = [SELECT Id FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId LIMIT 1];

        TestClassDataUtil.createDerivedRelationship(communityUser.ContactId, party.Id, farm.Id, true);
        Agreement__c standardAgreement = TestClassDataUtil.createAgreement('Standard Agreement', farm.Id, Date.today() + 1, false);
        standardAgreement.Party__c = party.Id;
        insert standardAgreement;
        Test.startTest();
        System.runAs(communityUser) {
            AMSPricingController controller = new AMSPricingController();
            System.assert(!controller.agreementList.isEmpty());
            controller.selectedFarmToSign = farm.Id;
            controller.selectedPartyRelatedToFarm = party.Id;
            controller.showMessageForElectronicAgreement = true;

            PageReference newPage = controller.goToAMSSignAgreementPage();
            System.assertEquals(true, newPage.getRedirect());
            Map <String, String> newPageParameters = newPage.getParameters();
            system.assertEquals(farm.Id, newPageParameters.get('FarmId'));
            system.assertEquals(party.Id, newPageParameters.get('PartyId'));

        }
        Test.stopTest();
    }
}