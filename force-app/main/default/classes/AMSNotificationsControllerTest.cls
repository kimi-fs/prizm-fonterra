/**
 * AMSNotificationsControllerTest.cls
 * Description: Test for AMSNotificationsController
 * @author: John Au
 * @date: December 2018
 */
@IsTest
private class AMSNotificationsControllerTest {
    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @TestSetup
    private static void testSetup() {
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.individualDefaultAccountAU();

        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu(USER_EMAIL);
    }

    @IsTest
    private static void testController() {
        User integrationUser = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name = 'Integration User' Limit 1];
        User communityUser = [SELECT Id, ContactId FROM User WHERE Email = :USER_EMAIL];
        Contact individual = [SELECT Id FROM Contact WHERE Id = :communityUser.ContactId];
        Goal__c goal = TestClassDataUtil.createGoal(false, individual);

        goal.Visible_in_Farmsource__c = true;

        insert (goal);

        System.runAs(integrationUser) {
            insert new FeedItem(ParentId = goal.Id, Body = 'test feed item');
        }

        System.runAs(communityUser) {
            AMSNotificationsController controller = new AMSNotificationsController();

            System.assertEquals(controller.feedItemWrappers.size(), 1);
            System.assertEquals(controller.hasNewNotifications, true);

            controller.notificationBellClicked();

            controller = new AMSNotificationsController();

            System.assertEquals(controller.hasNewNotifications, false);
        }
    }
}