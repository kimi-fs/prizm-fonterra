/**
 * AMSFarmSeasonService
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately.
 *
 * Author: Amelia (Trineo)
 * Date: September 2017
 */
public without sharing class AMSFarmSeasonService {

    private static List<String> seasonFields = new List<String>{
                    'Id',
                    'Name',
                    'Farm__c',
                    'Farm__r.Name',
                    'Peak_Cows__c',
                    'Cows_January__c',
                    'Cows_February__c',
                    'Cows_March__c',
                    'Cows_April__c',
                    'Cows_May__c',
                    'Cows_June__c',
                    'Cows_July__c',
                    'Cows_August__c',
                    'Cows_September__c',
                    'Cows_October__c',
                    'Cows_November__c',
                    'Cows_December__c',
                    'Dairy_Hectares__c',
                    'Reference_Period__r.Status__c',
                    'Reference_Period__r.Start_Date__c',
                    'Reference_Period__r.End_Date__c',
                    'Reference_Period__r.Reference_Period__c'
    };

    private static List<String> targetsFields = new List<String>{
                    'Id',
                    'Month__c',
                    'Season__c',
                    'Target_KgMS__c',
                    'Daily_Target_KgMS__c'
    };

    public static List<Farm_Season__c> getFarmSeasons(Set<Id> farmIds){

        String query =  'SELECT ' + String.join(seasonFields, ',') + ' , ' +
                        ' (SELECT ' + String.join(targetsFields, ',') +
                        '  FROM Targets__r) ' +
                        'FROM Farm_Season__c ' +
                        'WHERE Farm__c IN :farmIds ' +
                        'ORDER BY Reference_Period__r.Start_Date__c ASC';

        List<Farm_Season__c> farmSeasons = Database.query(query);
        return farmSeasons;
    }

    public static List<Farm_Season__c> getCurrentFarmSeason(Id farmId){

        String query =  'SELECT ' + String.join(seasonFields, ',') + ' , ' +
                        ' (SELECT ' + String.join(targetsFields, ',') +
                        '  FROM Targets__r) ' +
                        'FROM Farm_Season__c ' +
                        'WHERE Farm__c = :farmId ' +
                        'AND Reference_Period__r.Status__c = \'Current\'' + ' AND Reference_Period__r.Type__c = \'Milking Season AU\'';

        List<Farm_Season__c> farmSeasons = Database.query(query);

        return farmSeasons;
    }

    /**
     * Get the specified farm seasons backwards from the current season, it is limited by backwards,
     * so 1 will mean just the current season
     */
    public static List<Farm_Season__c> getCurrentFarmSeasonBackwards(Id farmId, Integer backwards){

        String query =  'SELECT ' + String.join(seasonFields, ',') + ' , ' +
                        ' (SELECT ' + String.join(targetsFields, ',') +
                        '  FROM Targets__r) ' +
                        'FROM Farm_Season__c ' +
                        'WHERE Farm__c = :farmId ' +
                        'AND Reference_Period__r.Name != \'\' AND ' +
                        '    Reference_Period__r.Start_Date__c <= TODAY ' + ' AND Reference_Period__r.Type__c = \'Milking Season AU\' ' +
                        'ORDER BY Reference_Period__r.End_Date__c DESC ' +
                        'LIMIT :backwards';

        List<Farm_Season__c> farmSeasons = Database.query(query);

        return farmSeasons;
    }

    /**
     * Get the specified farm seasons forwards from the current season, it is limited by backwards,
     * so 1 will mean just the current season
     */
    public static List<Farm_Season__c> getCurrentFarmSeasonForwards(Id farmId, Integer forwards){

        String query =  'SELECT ' + String.join(seasonFields, ',') + ' , ' +
                        ' (SELECT ' + String.join(targetsFields, ',') +
                        '  FROM Targets__r) ' +
                        'FROM Farm_Season__c ' +
                        'WHERE Farm__c = :farmId ' +
                        'AND Reference_Period__r.Name != \'\' AND ' +
                        '    Reference_Period__r.End_Date__c >= TODAY ' + ' AND Reference_Period__r.Type__c = \'Milking Season AU\' ' +
                        'ORDER BY Reference_Period__r.End_Date__c ASC ' +
                        'LIMIT :forwards';

        List<Farm_Season__c> farmSeasons = Database.query(query);

        return farmSeasons;
    }

    //Method to update the specified Farm Season
    public static void updateFarmSeason(Farm_Season__c farmSeason) {
        update farmSeason;
    }

    public static Boolean isHerdSizeSetByMonth(Farm_Season__c farmSeason) {
        return farmSeason.Cows_January__c != null ||
                farmSeason.Cows_February__c != null ||
                farmSeason.Cows_March__c != null ||
                farmSeason.Cows_April__c != null ||
                farmSeason.Cows_May__c != null ||
                farmSeason.Cows_June__c != null ||
                farmSeason.Cows_July__c != null ||
                farmSeason.Cows_August__c != null ||
                farmSeason.Cows_September__c != null ||
                farmSeason.Cows_October__c != null ||
                farmSeason.Cows_November__c != null ||
                farmSeason.Cows_December__c != null;
    }
}