/**
 * Created by Nick on 14/08/17.
 *
 * AMSLineChartController
 *
 * Controller for AMSLineChart component
 *
 * Author: NL (Trineo)
 * Date: 2017-08-14
 **/
public with sharing class AMSHistoricalLineChartController {
    public List<AMSCollectionGraph> collectionGraphs { get; set; }

    public AMSHistoricalLineChartController() {
    }
}