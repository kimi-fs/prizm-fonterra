/**
 * Description: Controller for the AMSFarmNames page
 * Allows the user to assign a personal [sits on drel] 'nickname' to any farms that they can see
 *
 * @author: John Au (Trineo)
 * @date: October 2018
 */
public with sharing class AMSFarmNamesController {

    public List<Individual_Relationship__c> derivedRelationships { get; set; }

    public AMSFarmNamesController() {
        this.derivedRelationships = new List<Individual_Relationship__c>();
        Id contactId = AMSContactService.determineContactId();

        List<Individual_Relationship__c> allIndividualRelationships = RelationshipAccessService.allIndividualRelationshipsForIndividual(contactId);
        for (Individual_Relationship__c individualRelationship : allIndividualRelationships) {
            if (individualRelationship.RecordTypeId == GlobalUtility.derivedRelationshipRecordTypeId || individualRelationship.RecordTypeId == GlobalUtility.individualRestrictedFarmRecordTypeId) {
                this.derivedRelationships.add(individualRelationship);
            }
        }
    }

    public void save() {
        RelationshipAccessService.updateIndividualRelationships(derivedRelationships, new List<String> { 'Entity_Nickname__c' });
    }

}