/**
 * Description: Test for AMSUserCreation
 * @author: Amelia (Trineo)
 * @date: July 2017
 */
 @isTest private class AMSUserCreationControllerTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup static void testSetup(){
        TestClassDataUtil.individualDefaultAccountAU();
    }

    @isTest static void testController_NoUser(){
        Contact contact = TestClassDataUtil.createIndividualAU(true);

        ApexPages.StandardController sc = new ApexPages.StandardController(contact);
        AMSUserCreationController controller = new AMSUserCreationController(sc);

        System.assertEquals(null, controller.communityUser.Id, 'User set incorrectly');
        System.assertEquals(contact.Id, controller.individual.Id, 'Individual set incorrectly');
        System.assertEquals(false, controller.userExists, 'userExists set incorrectly');
    }

    @isTest static void testController_UserExists(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        Contact contact = [SELECT Id FROM Contact WHERE Email =: USER_EMAIL];

        ApexPages.StandardController sc = new ApexPages.StandardController(contact);
        AMSUserCreationController controller = new AMSUserCreationController(sc);

        System.assertEquals(communityUser.Id, controller.communityUser.Id, 'User set incorrectly');
        System.assertEquals(contact.Id, controller.individual.Id, 'Individual set incorrectly');
        System.assertEquals(true, controller.userExists, 'userExists set incorrectly');
    }

    @isTest static void testCreateCommunityUser_NoEmail(){
        Contact contact = TestClassDataUtil.createIndividualAU(false);
        contact.Email = '';
        insert contact;

        ApexPages.StandardController sc = new ApexPages.StandardController(contact);
        AMSUserCreationController controller = new AMSUserCreationController(sc);

        controller.createCommunityUser();

        System.assert(TestClassDataUtil.pageMessagesContainsMessage(ApexPages.getMessages(), AMSUserCreationController.NO_EMAIL_MESSAGE), 'Page did not have error');
    }

    @isTest static void testCreateCommunityUser_ExistingUserWithEmail(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        Contact contact =TestClassDataUtil.createIndividualAU(false);
        contact.Email = USER_EMAIL;
        insert contact;

        ApexPages.StandardController sc = new ApexPages.StandardController(contact);
        AMSUserCreationController controller = new AMSUserCreationController(sc);

        controller.createCommunityUser();

        System.assert(TestClassDataUtil.pageMessagesContainsMessage(ApexPages.getMessages(), AMSUserCreationController.USER_EXISTS_MESSAGE), 'Page did not have error');
    }

    @isTest static void testCreateCommunityUser_ValidInputs(){
        TestClassDataUtil.individualDefaultAccountAU();

        Contact contact = TestClassDataUtil.createIndividualAU(false);
        contact.Email = USER_EMAIL;
        insert contact;

        ApexPages.StandardController sc = new ApexPages.StandardController(contact);
        AMSUserCreationController controller = new AMSUserCreationController(sc);

        controller.createCommunityUser();

        User insertedUser = [SELECT Id FROM User WHERE ContactId =: contact.Id LIMIT 1];
        System.assertEquals(insertedUser.Id, controller.communityUser.Id, 'User not inserted correctly');
        System.assertEquals(true, controller.userExists, 'userExists not updated');

    }

    @isTest static void testResetUserPassword(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        Contact contact = [SELECT Id, FarmSourceONE_Setup_Complete__c FROM Contact WHERE Email =: USER_EMAIL];

        ApexPages.StandardController sc = new ApexPages.StandardController(contact);
        AMSUserCreationController controller = new AMSUserCreationController(sc);

        controller.resetUserPassword();

        System.assert(TestClassDataUtil.pageMessagesContainsMessage(ApexPages.getMessages(), AMSUserCreationController.EMAIL_SENT_MESSAGE), 'Page did not have error');
    }

    @isTest static void testDeactivateUser(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        Contact contact = [SELECT Id, FarmSourceONE_Setup_Complete__c FROM Contact WHERE Email =: USER_EMAIL];

        ApexPages.StandardController sc = new ApexPages.StandardController(contact);
        AMSUserCreationController controller = new AMSUserCreationController(sc);

        System.runAs([SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1][0]){
            controller.deactivateUser();
        }

        System.assertEquals(false, [SELECT Id, IsActive FROM User WHERE Id =: communityUser.Id].IsActive, 'IsActive not updated');
    }

    @isTest static void testReactivateUser(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        //communityUser.IsActive = false;
        //update communityUser;

        Contact contact = [SELECT Id, FarmSourceONE_Setup_Complete__c FROM Contact WHERE Email =: USER_EMAIL];

        ApexPages.StandardController sc = new ApexPages.StandardController(contact);
        AMSUserCreationController controller = new AMSUserCreationController(sc);

        System.runAs([SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1][0]){
            controller.reActivateUser();
        }

        System.assertEquals(true, [SELECT Id, IsActive FROM User WHERE Id =: communityUser.Id].IsActive, 'IsActive not updated');
    }

    @isTest static void testCreateAMSUserCookie(){
        Contact contact = TestClassDataUtil.createIndividualAU(true);

        ApexPages.StandardController sc = new ApexPages.StandardController(contact);
        AMSUserCreationController controller = new AMSUserCreationController(sc);

        controller.createAMSUserCookie();

        Cookie individualCookie = ApexPages.currentPage().getCookies().get(AMSCommunityUtil.LOGIN_AS_COOKIE);
        System.assertNotEquals(null, individualCookie);
    }
}