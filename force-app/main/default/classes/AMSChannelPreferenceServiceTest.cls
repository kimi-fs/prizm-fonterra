/**
 * Description: Test class for AMSChannelPreferenceService
 * @author: Amelia (Trineo)
 * @date: March 2018
 */
@isTest
private class AMSChannelPreferenceServiceTest {

    public static final String FIRSTNAME = 'Harry';
    public static final String LASTNAME = 'Potter';

    @testSetup static void testSetup(){
        TestClassDataUtil.createAuChannelPreferenceSettings();
    }

    @isTest static void testSubscribeUsersToNotifications(){
        Test.startTest();
        Contact testContact = TestClassDataUtil.createIndividualAU(true);
        Test.stopTest();
        AMSChannelPreferenceService.subscribeUsersToNotifications(testContact.Id);

        for (Channel_Preference__c cp : AMSChannelPreferenceService.getChannelPreferencesFromContact(testContact.Id)){
            System.assertEquals(true, cp.DeliveryByEmail__c, 'DeliveryByEmail__c not updated');
            System.assertEquals(true, cp.NotifyBySMS__c, 'NotifyBySMS__c not updated');
        }
    }
}