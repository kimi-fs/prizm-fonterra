@isTest
public class AMSFarmDetailsControllerTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup static void testSetup() {
        SetupAMSCommunityPageMessages.createPageMessages();
        TestClassDataUtil.individualDefaultAccountAU();
    }

    @isTest static void testConstructor() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            AMSFarmDetailsController controller = new AMSFarmDetailsController();
            System.assertEquals(communityUser.ContactId, controller.contact.Id, 'Wrong contact');
            System.assertNotEquals(null, controller.farmWrappers, 'Farm wrappers not initalised');
        }
    }

    @isTest static void testGetWelcomeName_PreferredName() {
        String PREFERRED_NAME = 'Preferred Name';
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        Contact communityUserContact = new Contact(Id = communityUser.ContactId, Preferred_Name__c = PREFERRED_NAME, Type__c = 'Personal');
        update communityUserContact;

        System.runAs(communityUser) {
            AMSFarmDetailsController controller = new AMSFarmDetailsController();
            System.assertEquals(PREFERRED_NAME, controller.getWelcomeName(), 'Wrong welcome name');
        }
    }

    @isTest static void testGetWelcomeName_WithoutPreferredName() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        Contact communityUserContact = [SELECT Id, FirstName FROM Contact WHERE Id =: communityUser.ContactId];

        System.runAs(communityUser) {
            AMSFarmDetailsController controller = new AMSFarmDetailsController();
            System.assertEquals(communityUserContact.FirstName, controller.getWelcomeName(), 'Wrong welcome name');
        }
    }

    @isTest static void testAddNewFarmDetailsWrapper() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            AMSFarmDetailsController controller = new AMSFarmDetailsController();
            controller.addNewFarmDetailsWrapper();
            controller.addNewFarmDetailsWrapper();
            controller.addNewFarmDetailsWrapper();
            System.assertEquals(4, controller.farmWrappers.size(), 'Farm wrappers not added');
        }
    }

    @isTest static void testSaveAndContinue_NoExistingFarm() {
        String FARM_NAME = '1234';
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            AMSFarmDetailsController controller = new AMSFarmDetailsController();
            controller.farmWrappers[0].farmNumber = FARM_NAME;
            controller.farmWrappers[0].farmBusinessName = FARM_NAME;

            controller.saveAndContinue();

        }
        System.assertEquals(1, [SELECT count() FROM Case WHERE AccountId =: GlobalUtility.defaultAccountIdAU], 'Case not created');
    }

    @isTest static void testSaveAndContinue_FarmExists() {
        Account farm = TestClassDataUtil.createAUFarms(1, true)[0];
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        Address__c address = TestClassDataUtil.createIndividualAddress(communityUser.ContactId, '123', 'Street', 'Street');
        address.Active__c = true;
        address.RecordTypeId = GlobalUtility.postalAddressRecordTypeId;
        update address;

        System.runAs(communityUser) {
            AMSFarmDetailsController controller = new AMSFarmDetailsController();

            controller.addNewFarmDetailsWrapper();
            controller.farmWrappers[0].farmNumber = '';
            controller.farmWrappers[0].farmBusinessName = '';

            controller.farmWrappers[1].farmNumber = farm.Name;
            controller.farmWrappers[1].farmBusinessName = farm.Name;

            controller.saveAndContinue();

        }
        System.assertEquals(1, [SELECT count() FROM Case WHERE AccountId =: farm.Id], 'Case not created');
    }

    @isTest static void testSaveAndContinue_FarmExistsMultipleUnsortedFarms() {
        Account farm = TestClassDataUtil.createAUFarms(1, true)[0];
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        Address__c address = TestClassDataUtil.createIndividualAddress(communityUser.ContactId, '123', 'Street', 'Street');
        address.Active__c = true;
        address.RecordTypeId = GlobalUtility.physicalAddressRecordTypeId;
        update address;

        System.runAs(communityUser) {
            AMSFarmDetailsController controller = new AMSFarmDetailsController();

            controller.farmWrappers[0].farmNumber = '';
            controller.farmWrappers[0].farmBusinessName = '';

            controller.addNewFarmDetailsWrapper();
            controller.farmWrappers[1].farmNumber = 'yyyyy';
            controller.farmWrappers[1].farmBusinessName = 'yyyyy';

            controller.addNewFarmDetailsWrapper();
            controller.farmWrappers[2].farmNumber = '';
            controller.farmWrappers[2].farmBusinessName = '';

            controller.addNewFarmDetailsWrapper();
            controller.farmWrappers[3].farmNumber = 'zzzzz';
            controller.farmWrappers[3].farmBusinessName = 'zzzzz';

            controller.addNewFarmDetailsWrapper();
            controller.farmWrappers[3].farmNumber = farm.Name;
            controller.farmWrappers[3].farmBusinessName = farm.Name;

            controller.addNewFarmDetailsWrapper();
            controller.farmWrappers[4].farmNumber = 'xxxxx';
            controller.farmWrappers[4].farmBusinessName = 'xxxxx';

            controller.addNewFarmDetailsWrapper();
            controller.farmWrappers[5].farmNumber = 'yyyyy';
            controller.farmWrappers[5].farmBusinessName = 'yyyyy';

            controller.addNewFarmDetailsWrapper();
            controller.farmWrappers[6].farmNumber = 'xxxxx';
            controller.farmWrappers[6].farmBusinessName = 'xxxxx';

            controller.saveAndContinue();

        }
        System.assertEquals(1, [SELECT count() FROM Case WHERE AccountId =: farm.Id], 'Case not created');
    }

    @isTest static void testSaveAndContinue_CaseInsertException() {
        String longString = 'I am going to be a very long string. ';
        for (Integer i = 0; i < 1000; i++) {
            longString += 'I am adding loads and loads of length to this string. ';
        }

        System.assert(longString.length() > 32000, 'String not long enough');

        Account farm = TestClassDataUtil.createAUFarms(1, true)[0];
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            AMSFarmDetailsController controller = new AMSFarmDetailsController();

            controller.farmWrappers[0].farmNumber = longString;
            controller.farmWrappers[0].farmBusinessName = longString;
            controller.farmWrappers[0].bankAccount = longString;
            controller.farmWrappers[0].businessABN = longString;
            controller.farmWrappers[0].dairyLicenseNumber = longString;

            controller.addNewFarmDetailsWrapper();
            controller.farmWrappers[1].farmNumber = farm.Name;
            controller.farmWrappers[1].farmBusinessName = farm.Name;
            controller.farmWrappers[1].bankAccount = farm.Name;
            controller.farmWrappers[1].businessABN = farm.Name;
            controller.farmWrappers[1].dairyLicenseNumber = farm.Name;

            PageReference pr = controller.saveAndContinue();

            System.assertEquals(null, pr, 'Page should not have redirected');
        }
        System.assertEquals(0, [SELECT count() FROM Case WHERE AccountId =: farm.Id], 'Case not created');
    }
}