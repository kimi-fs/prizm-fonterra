/**
 * New ContentDocumentLink has been created, if it is for a Season then
 * we have some extra work to do.
 *
 * @author Ed (Trineo)
 * @test: ContentTriggerHandlerTest
 */
public with sharing class ContentDocumentLinkTriggerHandler extends TriggerHandler {
    private static final String PUBLIC_SHARE = 'PUBLIC_SHARE';
    private static final String PP_VARS = 'PP_VARS';

    public override void afterInsert() {

    }

    public override void afterDelete() {

    }

    public override void beforeInsert() {
        validateAgreementFileCreateAccess(Trigger.new);

        // Switch the visibility for ContentDocuments to Agreements and Farm Seasons to all users (so that community users can see them), this is required
        // because Customer Community Licences do *not* support Content. And so the only sharing of Files with community
        // users is by making them public. This is concerning and we need to revisit this issue.
        for (ContentDocumentLink documentLink : (List<ContentDocumentLink>)Trigger.new) {
            Id parentId = documentLink.LinkedEntityId;
            if (parentId.getSobjectType() == SObjectType.Agreement__c.getSObjectType() ||
                parentId.getSobjectType() == SObjectType.Farm_Season__c.getSObjectType()) {
                documentLink.Visibility = 'AllUsers';
            }
        }
    }

    public override void beforeDelete() {
        validateAgreementFileDeleteAccess(Trigger.old);
    }

    private void validateAgreementFileCreateAccess(List<ContentDocumentLink> contentDocumentLinkNew) {
        // insert validation for classic and lightning experience users
        Set<Id> linkedIdSet = getLinkedAgreementEntityIds(contentDocumentLinkNew);
        if (!linkedIdSet.isEmpty()) {
            ContentDocumentService.showValidationErrorForFarmAUAgreementFiles(
                'You are not allowed to upload a new file to this record. ',
                linkedIdSet,
                contentDocumentLinkNew,
                Agreement__c.Active__c,
                'true'
            );
        }
    }

    private void validateAgreementFileDeleteAccess(List<ContentDocumentLink> contentDocumentLinkOld) {
        // delete validation for classic users
        Set<Id> linkedIdSet = getLinkedAgreementEntityIds(contentDocumentLinkOld);
        if (!linkedIdSet.isEmpty()) {
            ContentDocumentService.showValidationErrorForFarmAUAgreementFiles('You are not allowed to delete this document. ', linkedIdSet, contentDocumentLinkOld, Agreement__c.Agreement_Status__c, 'Executed');
        }
    }

    private static Set<Id> getLinkedAgreementEntityIds(List<ContentDocumentLink> contentDocumentLink) {
        Set<Id> linkedIdSet = new Set<Id>();
        for (ContentDocumentLink contentDocLink : contentDocumentLink) {
            if (contentDocLink.LinkedEntityId != null) {
                System.debug('ContentDocumentLink onto Object: ' + contentDocLink.LinkedEntityId.getSObjectType().getDescribe().getName());
                if (contentDocLink.LinkedEntityId.getSObjectType().getDescribe().getName() == 'Agreement__c') {
                    linkedIdSet.add(contentDocLink.LinkedEntityId);
                }
            }
        }

        return linkedIdSet;
    }

}