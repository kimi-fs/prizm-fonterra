/**
 * TelephonySearchControllerTest.cls
 * Description: Test class for the Telephony Search
 * @author: Sarah Hay (Trineo)
 * @date: February 2016
 */
@isTest
private class TelephonySearchControllerTest {
    private static Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();

    private static Id farmAuAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Farm AU').getRecordTypeId();
    private static Id organisationAuAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Organisation AU').getRecordTypeId();
    private static Id partyAuAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Party AU').getRecordTypeId();

    private static Id derivedRelationshipRecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Derived Relationship').getRecordTypeId();
    private static Id individualFarmRecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Farm').getRecordTypeId();
    private static Id individualPartyRecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Party').getRecordTypeId();
    private static Id individualOrganisationRecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Organisation').getRecordTypeId();

    @isTest static void phoneAndIdsTest() {
        Contact ironMan = new Contact(FirstName = 'Tony', LastName = 'Stark', Phone = '+64 3 3774001', Type__c = 'Personal');
        insert ironMan;

        List<Account> farms = new List<Account>();
        farms.add(new Account(Name = 'farm1', RecordTypeId = farmAuAccountRecordTypeId));
        farms.add(new Account(Name = 'farm2', RecordTypeId = farmAuAccountRecordTypeId));
        insert farms;

        List<Account> parties = new List<Account>();
        parties.add(new Account(Name = 'party1', RecordTypeId = partyAuAccountRecordTypeId));
        parties.add(new Account(Name = 'party2', RecordTypeId = partyAuAccountRecordTypeId));
        insert parties;

        List<Account> orgs = new List<Account>();
        orgs.add(new Account(Name = 'org1', RecordTypeId = organisationAuAccountRecordTypeId));
        orgs.add(new Account(Name = 'org2', RecordTypeId = organisationAuAccountRecordTypeId));
        insert orgs;

        List<Individual_Relationship__c> individualRelationships = new List<Individual_Relationship__c>();
        individualRelationships.add(new Individual_Relationship__c(Individual__c = ironMan.Id, Farm__c = farms[0].Id, Role__c = 'Director', Party__c = parties[0].Id, PF_Role__c = 'Owner', Active__c = true, RecordTypeId = derivedRelationshipRecordTypeId));
        individualRelationships.add(new Individual_Relationship__c(Individual__c = ironMan.Id, Farm__c = farms[1].Id, Role__c = 'Director', Party__c = parties[1].Id, PF_Role__c = 'Owner', Active__c = true, RecordTypeId = derivedRelationshipRecordTypeId));

        individualRelationships.add(new Individual_Relationship__c(Individual__c = ironMan.Id, Farm__c = farms[0].Id, Role__c = 'Director', Active__c = true, RecordTypeId = individualFarmRecordTypeId));
        individualRelationships.add(new Individual_Relationship__c(Individual__c = ironMan.Id, Farm__c = farms[1].Id, Role__c = 'Director', Active__c = true, RecordTypeId = individualFarmRecordTypeId));

        individualRelationships.add(new Individual_Relationship__c(Individual__c = ironMan.Id, Party__c = parties[0].Id, Role__c = 'Director', Active__c = true, RecordTypeId = individualPartyRecordTypeId));
        individualRelationships.add(new Individual_Relationship__c(Individual__c = ironMan.Id, Party__c = parties[1].Id, Role__c = 'Director', Active__c = true, RecordTypeId = individualPartyRecordTypeId));

        individualRelationships.add(new Individual_Relationship__c(Individual__c = ironMan.Id, Organisation__c = orgs[0].Id, Role__c = 'Director', Active__c = true, RecordTypeId = individualOrganisationRecordTypeId));
        individualRelationships.add(new Individual_Relationship__c(Individual__c = ironMan.Id, Organisation__c = orgs[1].Id, Role__c = 'Director', Active__c = true, RecordTypeId = individualOrganisationRecordTypeId));

        insert individualRelationships;

        List<Individual_Relationship__c> irsToMakeInactive = [SELECT Id, Active__c FROM Individual_Relationship__c WHERE(Farm__c = : farms[1].Id OR Party__c = : parties[1].Id OR Organisation__c = : orgs[1].Id)];
        for (Individual_Relationship__c ir : irsToMakeInactive) {
            ir.Active__c = false;
        }
        update irsToMakeInactive;

        List<Contact> indi = new List<Contact>();

        PageReference pageRef = Page.TelephonySearch;
        pageRef.getParameters().put('id', ironMan.Id);
        pageRef.getParameters().put('phone', '64 3 3774001');
        Test.setCurrentPage(pageRef);

        TelephonySearchController tsc = new TelephonySearchController();

        System.assertEquals('Multiple Matches', tsc.tabText);
        System.assertEquals('Multiple Matches for 064 3 3774001', tsc.phoneNumber);
        System.assertEquals(1, tsc.individuals.size());
        System.assertEquals(1, tsc.results.size());
        System.assertEquals(4, tsc.results[0].relationships.size());
    }

    @isTest static void noPhoneProvidedTest() {
        PageReference pageRef = Page.TelephonySearch;
        pageRef.getParameters().put('phone', null);
        Test.setCurrentPage(pageRef);

        TelephonySearchController tsc = new TelephonySearchController();

        boolean b = false;
        for (Apexpages.Message msg : ApexPages.getMessages()) {
            if (msg.getDetail().contains('No Caller Id')) {
                b = true;
            }
        }
        System.assert(b);
    }

    @isTest static void noIdProvidedTest() {
        Contact publicGuy = new Contact(FirstName = 'AU Public', LastName = 'Guy', Phone = '+64 3 3774001', Type__c = 'Personal');
        insert publicGuy;

        PageReference pageRef = Page.TelephonySearch;
        pageRef.getParameters().put('phone', '64 3 3774001');
        Test.setCurrentPage(pageRef);

        TelephonySearchController tsc = new TelephonySearchController();

        System.assertEquals('Unknown Match', tsc.tabText);
        System.assertEquals('Unknown Match for 064 3 3774001', tsc.phoneNumber);
        System.assertEquals(1, tsc.auPublicIndividuals.size());
    }

    @isTest static void pureCloudTest() {
        Contact ironMan = new Contact(FirstName = 'Tony', LastName = 'Stark', Phone = '+64 3 3774001', Type__c = 'Personal');
        insert ironMan;

        Account farm = new Account(Name = 'farm1', RecordTypeId = farmAuAccountRecordTypeId);
        Account party = new Account(Name = 'party1', RecordTypeId = partyAuAccountRecordTypeId);
        Account org = new Account(Name = 'org1', RecordTypeId = organisationAuAccountRecordTypeId);
        List<Account> accounts = new List<Account> {
            farm, party, org
        };
        insert accounts;

        List<Individual_Relationship__c> individualRelationships = new List<Individual_Relationship__c>();
        individualRelationships.add(new Individual_Relationship__c(Individual__c = ironMan.Id, Farm__c = farm.Id, Role__c = 'Director', Party__c = party.Id, PF_Role__c = 'Owner', Active__c = true, RecordTypeId = derivedRelationshipRecordTypeId));
        individualRelationships.add(new Individual_Relationship__c(Individual__c = ironMan.Id, Farm__c = farm.Id, Role__c = 'Director', Active__c = true, RecordTypeId = individualFarmRecordTypeId));
        individualRelationships.add(new Individual_Relationship__c(Individual__c = ironMan.Id, Party__c = party.Id, Role__c = 'Director', Active__c = true, RecordTypeId = individualPartyRecordTypeId));
        individualRelationships.add(new Individual_Relationship__c(Individual__c = ironMan.Id, Organisation__c = org.Id, Role__c = 'Director', Active__c = true, RecordTypeId = individualOrganisationRecordTypeId));
        insert individualRelationships;

        List<Contact> indi = new List<Contact>();

        PageReference pageRef = Page.TelephonySearch;
        pageRef.getParameters().put('key', '+6433774001');
        Test.setCurrentPage(pageRef);

        TelephonySearchController tsc = new TelephonySearchController();

        System.assertEquals('Multiple Matches', tsc.tabText);
        System.assertEquals('Multiple Matches for 033774001', tsc.phoneNumber);
        System.assertEquals(1, tsc.individuals.size());
        System.assertEquals(1, tsc.results.size());
        System.assertEquals(4, tsc.results[0].relationships.size());
    }
}