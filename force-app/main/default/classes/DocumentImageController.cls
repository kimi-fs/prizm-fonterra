/**
 * Description: Displays an images from Developer Name
 * @author: Amelia (Trineo)
 * @date: August 2017
 */
global without sharing class DocumentImageController {
    global static final String IMAGE_URL = '.content.force.com/servlet/servlet.ImageServer?id=';

    global String documentDeveloperName {
        get;
        set {
            documentDeveloperName = value;
            imageUrl = generateImageURL(value);
        }
    }

    global String imageURL { get; set; }

    private static String generateImageURL(String documentDeveloperName){
        String imageURL;
        List<Document> documents = [SELECT Id FROM Document WHERE DeveloperName =: documentDeveloperName];
        String serverInstanceName = getServerInstanceName();
        if(!documents.isEmpty() && String.isNotBlank(serverInstanceName)){
            imageURL = 'https://c.' + serverInstanceName
                    + IMAGE_URL + documents[0].Id
                    + '&oid=' + UserInfo.getOrganizationId();
        }
        return imageURL;
    }

    private static String getServerInstanceName() {
        return [SELECT InstanceName FROM Organization].InstanceName.toLowerCase();
    }
}