@isTest
private class AMSCollectionSummaryServiceTest {

    @testSetup static void testSetup(){
    }

    @isTest static void testGetCollectionSummaries() {
        Account farm = TestClassDataUtil.createAUFarms(1, true)[0];
        List<Collection_Summary__c> collectionSummaries = TestClassDataUtil.createSeasonOfMonthlyCollectionSummary(farm, true);

        Date startDate = Date.today().addMonths(-6);
        Date endDate = Date.today().addMonths(5);
        System.assertEquals(collectionSummaries.size(), AMSCollectionSummaryService.getCollectionSummaries(farm.Id, startDate, endDate).size());
    }

    @isTest static void testGetCollectionSummariesLastTwelveMonths() {
        Account farm = TestClassDataUtil.createAUFarms(1, true)[0];
        List<Collection_Summary__c> collectionSummaries = TestClassDataUtil.createSeasonOfMonthlyCollectionSummary(farm, true);

        System.assertEquals(collectionSummaries.size(), AMSCollectionSummaryService.getCollectionSummariesLastFourteenMonths(farm.Id).size());
    }


}