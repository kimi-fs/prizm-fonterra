/**
 * AMSCommunityTemplateController.cls
 * Description: Template level controller for pages within the community
 * @author: Amelia (Trineo)
 * @date: June 2017
 * @test: AMSCommunityTemplateController_Test
 */
public with sharing class AMSCommunityTemplateController {
    public static final String PREVIOUS_PAGE_PARAMETER = 'previousAMSPage';

    public User user { get; set; }
    public Contact individual { get; set; }

    public Boolean supportedBrowser { get; set; }
    public String browserWarning { get; set; }
    public String searchString { get; set; }

    public String headerName { get; set; }
    public String headerPhoto { get; set; }
    public String menuPhoto { get; set; }

    public List<AMSKnowledgeService.ArticleCategoryWrapper> adviceArticleWrappers {get; private set;}
    public List<AMSKnowledgeService.ArticleCategoryWrapper> newsArticleWrappers {get; private set;}

    private static Map<String, String> messages;
    static {
        messages = AMSCommunityUtil.initialisePageMessages('AMSProdQualReportTemplate');
    }

    public static Boolean isViewingDifferentContact {
        get {
            if (isViewingDifferentContact == null) {
                isViewingDifferentContact = AMSContactService.isViewingDifferentContact();
            }
            return isViewingDifferentContact;
        }

        set;
    }

    public String getNoDataMessage() {
        return messages.get('no-data');
    }

    public AMSCommunityTemplateController() {
        Id individualId = AMSContactService.determineContactId();
        if (individualId != null) {
            this.individual = AMSContactService.getContact(individualId);
            this.headerName = this.individual.Preferred_Name__c != null ? this.individual.Preferred_Name__c : this.individual.FirstName;
            this.user = AMSUserService.getUserFromContactId(this.individual.Id);
        }

        // These call ConnectApi which is not supported in tests
        if (!Test.isRunningTest()) {
            this.headerPhoto = AMSUserPhotoService.getSmallUserPhoto(user);
            this.menuPhoto = AMSUserPhotoService.getSmallUserPhoto(user);
        }

        this.supportedBrowser = true;
        String browser = ApexPages.currentPage().getHeaders().get('USER-AGENT');

        if (browser != null && browser.contains('MSIE')) {
            checkBrowser(browser);
        }

        this.adviceArticleWrappers = AMSKnowledgeService.getAdviceArticleCategories();
        this.newsArticleWrappers = AMSKnowledgeService.getNewsArticleCategories();

        createLastAMSPageCookie();
    }

    public void checkBrowser(String browser) {
        if (browser != null && browser.contains('MSIE')) {
            List<String> agentDetails = browser.split(';');
            for (String s : agentDetails) {
                if (s.left(6) == ' MSIE ') {
                    if (Decimal.valueOf(s.mid(6, s.length())) < 11) {
                        this.supportedBrowser = false;
                        this.browserWarning = 'Your version of Internet Explorer is no longer supported, Please update to IE11 or greater.';
                    }
                }
            }
        }
    }

    /**
     * Perform a search and return the user to the search results
     */
    public PageReference search() {
        return AMSSearchController.search(this.searchString);
    }

    public static  PageReference checkUsersAccess() {
        // If there is no contact Id redirct to session timeout page - this should be for log in as functionality only
        if (AMSUserService.checkIfUserCanViewCommunity() && AMSContactService.determineContactId() == null) {
            String url = '/';
            return new PageReference(url);
        // If the user is not logged in redirect to the login page
        } else if (!AMSUserService.checkUserLoggedIn()) {
            String currentPage = String.valueOf(ApexPages.currentPage().getUrl());
            PageReference loginPage = Page.AMSLogin;
            loginPage.getParameters().put(AMSCommunityUtil.getStartURLParameterName(), currentPage);
            return loginPage;
        // If the user is not email verified redirect to the email verification page
        } else if (!AMSUserService.checkVerified()) {
            return Page.AMSEmailVerification;
        } else {
            return null;
        }
    }

    public static String getFAQParameterName() {
        return AMSCommunityUtil.getFAQParameterName();
    }

    public Boolean getUserHasFarms() {
        Boolean individualHasFarms = false;

        if (this.individual.Id != null) {
            individualHasFarms = RelationshipAccessService.accessToFarms(this.individual.Id, RelationshipAccessService.Access.ACCESS_PRODUCTION_AND_QUALITY).size() > 0;
        }

        return individualHasFarms;
    }

    public PageReference logout() {
        ExtendedSessionService extendedSession = new ExtendedSessionService(AMSLoginController.COMMUNITY_NAME);
        extendedSession.logout();

        ApexPages.currentPage().setCookies(
            new Cookie[] {
                new Cookie(PREVIOUS_PAGE_PARAMETER, null, null, getCookieAge(), true)
            }
        );

        PageReference logoutPage = new PageReference(Site.getBaseSecureUrl() + '/secur/logout.jsp');
        logoutPage.setRedirect(true);
        return logoutPage;
    }

    /**
    * Returns the user to the contact record in internal org deletes the cookie with the individual Id
    * The button calling this function is available when viewing the community as a contact
    * The session id of the users interal session should still be valid when returning
    */
    public PageReference returnToContact() {
        ApexPages.currentPage().setCookies(
            new Cookie[] {
                new Cookie(AMSCommunityUtil.LOGIN_AS_COOKIE, this.individual.Id, null, null, true)
            }
        );
        String url = '/' + this.individual.Id;
        return new PageReference(url);
    }

    /**
    * Creates a cookie for each page the user is on
    * This is so the web app can return the user to the last visited page when it logs them in (as they get logged out every time they leave the app)
    */
    private void createLastAMSPageCookie() {
        String previousAMSPage = AMSCommunityUtil.pageReferenceToCommunityFriendlyString(ApexPages.currentPage());
        ApexPages.currentPage().setCookies(
            new Cookie[] {
                new Cookie(PREVIOUS_PAGE_PARAMETER, previousAMSPage, null, getCookieAge(), true)
            }
        );
    }

    private Integer getCookieAge() {
        // Use the custom setting to get the duration for the cookie - doesnt really make sense but for the want of a better idea...
        Community_Extended_Sessions__c communityExtSessionSettings = Community_Extended_Sessions__c.getInstance(AMSLoginController.COMMUNITY_NAME);
        Integer extSessionDurationDays = (communityExtSessionSettings == null || communityExtSessionSettings.Session_Duration__c == null) ? 30 : Integer.valueOf(communityExtSessionSettings.Session_Duration__c);
        Integer extSessionDurationSeconds = Integer.valueOf(extSessionDurationDays * 24 * 60 * 60);
        return extSessionDurationSeconds;
    }
}