@isTest
private class AccountTriggerTest {

    private static Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();

    @isTest
    private static void testUpdateIndividualMilkSupplyRelationshipManagerIndividualWithOneDrel() {
        Account farm = TestClassDataUtil.createAUFarms(1, true)[0];
        Account party = TestClassDataUtil.createEntity(GlobalUtility.auAccountPartyRecordTypeName, 'Test party');

        insert party;

        Contact individual = TestClassDataUtil.createIndividualAU(true);
        TestClassDataUtil.createDerivedRelationship(individual.Id, party.Id, farm.Id, true);

        User justAnotherUser = TestClassDataUtil.createIntegrationUser();

        farm.Rep_Area_Manager__c = justAnotherUser.Id;

        update farm;

        individual = [SELECT Id, Milk_Supply_Relationship_Manager__c FROM Contact WHERE Id = : individual.Id];

        System.assertEquals(individual.Milk_Supply_Relationship_Manager__c, justAnotherUser.Id);
    }

    @isTest
    private static void testUpdateIndividualMilkSupplyRelationshipManagerIndividualWithMoreThanOneDrel() {
        List<Account> farms = TestClassDataUtil.createAUFarms(2, true);
        Account party = TestClassDataUtil.createEntity(GlobalUtility.auAccountPartyRecordTypeName, 'Test party');

        insert party;

        Contact individual = TestClassDataUtil.createIndividualAU(true);
        TestClassDataUtil.createDerivedRelationship(individual.Id, party.Id, farms[0].Id, true);
        TestClassDataUtil.createDerivedRelationship(individual.Id, party.Id, farms[1].Id, true);

        User justAnotherUser = TestClassDataUtil.createIntegrationUser();

        farms[0].Rep_Area_Manager__c = justAnotherUser.Id;

        update farms[0];

        individual = [SELECT Id, Milk_Supply_Relationship_Manager__c FROM Contact WHERE Id = : individual.Id];

        System.assertEquals(individual.Milk_Supply_Relationship_Manager__c, null);
    }

}