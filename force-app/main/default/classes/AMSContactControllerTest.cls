/**
 * Description: Test for AMSContactController
 * @author: Ed (Trineo)
 * @date: July 2017
 */
@isTest
private class AMSContactControllerTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';

    /**
     * Setup a community user who is associated with a couple of farms, that have fonterra reps associated with them
     */
    @testSetup
    static void testSetup() {
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.integrationUserProfile();
    }

    @isTest static void getTheContacts() {
        User communityUser;
        List<Account> farms;

        User integrationUser = TestClassDataUtil.createIntegrationUser();

        System.runAs(integrationUser) {
            Test.startTest();
            communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual('test@farmuser.com');

            User fonterraUser = TestClassDataUtil.createAdminUser();

            farms = [SELECT Id FROM Account WHERE RecordTypeId = : GlobalUtility.auAccountFarmRecordTypeId];

            Account party = TestClassDataUtil.createSingleAccountPartyAU();
            TestClassDataUtil.createPrimaryAccessRelationships(communityUser.ContactId, party, farms);
            Test.stopTest();
            for (Account farm : farms) {
                farm.Rep_Area_Manager__c = fonterraUser.Id;
                farm.Quality_Specialist__c = fonterraUser.Id;
            }
            update farms;
        }



        System.runAs(communityUser) {
            System.Test.setCurrentPage(Page.AMSContact);

            AMSContactController controller = new AMSContactController();
            controller.initialiseContacts();

            System.assertNotEquals(null, controller.areaManagers);
            System.assertNotEquals(null, controller.qualitySpecialists);
        }
    }

    @isTest static void getTheContacts_NoContacts() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu(USER_EMAIL);
        System.runAs(communityUser) {
            System.Test.setCurrentPage(Page.AMSContact);

            AMSContactController controller = new AMSContactController();
            controller.initialiseContacts();

            System.assertEquals(true, controller.noContacts);
        }
    }

    @isTest static void testInitialiseEnquiry() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu(USER_EMAIL);
        System.runAs(communityUser) {
            AMSContactController controller = new AMSContactController();
            controller.initialiseEnquiry();

            System.assertNotEquals(null, controller.feedbackTypeOptions, 'feedbackTypeOptions not initialised');
            System.assertNotEquals(null, controller.farmOptions, 'farmOptions not initialised');
        }
    }

    @isTest static void testCancel() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu(USER_EMAIL);
        System.runAs(communityUser) {
            AMSContactController controller = new AMSContactController();
            controller.initialiseEnquiry();
            controller.cancel();

            System.assertEquals(false, controller.submitted, 'submitted not initialised');
            System.assertEquals('', controller.enquiryText, 'enquiryText not initialised');
        }
    }

    @isTest static void testSave_Valid() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu(USER_EMAIL);
        System.runAs(communityUser) {
            AMSContactController controller = new AMSContactController();
            controller.initialiseEnquiry();
            controller.farm = controller.farmOptions[1].getValue();
            controller.feedbackType = controller.feedbackTypeOptions[0].getValue();
            controller.enquiryText = 'A tanker driver ran over my dog!';

            controller.save();

            System.assertEquals(true, controller.submitted, 'submitted not updated');
        }
        System.assert([SELECT count() FROM Case WHERE ContactId = :communityUser.ContactId] > 0, 'Case not created');
    }

    @isTest static void testSave_NotValid() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu(USER_EMAIL);
        System.runAs(communityUser) {
            AMSContactController controller = new AMSContactController();
            controller.initialiseEnquiry();

            controller.save();

            System.assertEquals(false, controller.submitted, 'submitted not updated');
        }
        System.assert([SELECT count() FROM Case WHERE ContactId = :communityUser.ContactId] == 0, 'Case not created');
    }
}