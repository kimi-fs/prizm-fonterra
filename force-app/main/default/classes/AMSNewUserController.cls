/**
 * Description: New User registration page for AMS Community
 * @author: Amelia (Trineo)
 * @date: July 2017
 */
public with sharing class AMSNewUserController {

    public String password { get; set; }
    public String confirmPassword { get; set; }

    public String firstName { get; set; }
    public String lastName { get; set; }

    public String preferredName { get; set; }

    public String email { get; set; }
    public String phone { get; set; }

    public String message { get; set; }
    public String firstNameError {get;set;}
    public String lastNameError {get;set;}
    public String phoneError {get;set;}
    public String emailError {get;set;}
    public String passwordError { get; set; }

    private Pattern specialCharacterPattern = Pattern.compile('^[a-zA-Z\\s\\-\\\'\\,]+$');

    private static Map<String, String> messages;
    static {
        messages = AMSCommunityUtil.initialisePageMessages('AMSNewUser');
    }

    public AMSNewUserController () {
        message = '';
        firstNameError = '';
        lastNameError = '';
        phoneError = '';
        emailError = '';
        passwordError = '';
    }

    public PageReference registerUser() {
        this.firstName = cleanName(this.firstName);
        this.lastName = cleanName(this.lastName);
        this.email = this.email.trim();

        User existingUser = AMSUserService.getUserFromEmailAddress(this.email);
        if (existingUser != null){
            message = messages.get('user-exists');
            return null;
        } else {
            Boolean detailsComplete = detailsComplete();
            Boolean validUser = validUser();
            if (detailsComplete && validUser){
                String userId;
                User u = new User();
                try {
                    u.Username = AMSUserService.getUserName();
                    u.Email = this.email;
                    u.FirstName = this.firstName;
                    u.LastName = this.lastName;
                    u.Phone = this.phone;
                    u.CommunityNickname = AMSUserService.createCommunityNickname(this.firstName, this.lastName);
                    u.TimeZoneSidKey = 'Australia/Sydney';
                    u.Preferred_Name__c = this.preferredName;

                    Contact existingContact = AMSUserService.getContactFromEmail(this.email);
                    if (existingContact != null){
                        u.ContactId = existingContact.Id;
                    }

                    userId = Site.createPortalUser(u, GlobalUtility.defaultAccountIdAU, this.password, true);

                    if (userId != null) {
                        return Site.login(u.Username, this.password, AMSCommunityUtil.pageReferenceToCommunityFriendlyString(Page.AMSEmailVerification));
                    } else {
                        message = messages.get('unknown-error');
                        return null;
                    }

                 } catch(Exception e){
                    message = messages.get('unknown-error') + ' [' + e.getMessage() + ']';
                    return null;
                 }
            } else {
                return null;
            }
        }
    }

    private Boolean detailsComplete(){
        Boolean valid = true;

        if (String.isBlank(firstName)){
            valid = false;
            firstNameError = messages.get('missing-details').replace('{!fieldName}', 'First Name');
        }
        if (String.isBlank(lastName)){
            valid = false;
            lastNameError = messages.get('missing-details').replace('{!fieldName}', 'Last Name');
        }
        if (String.isBlank(phone)){
            valid = false;
            phoneError = messages.get('missing-details').replace('{!fieldName}', 'Main Phone');
        }
        if (String.isBlank(email)){
            valid = false;
            emailError = messages.get('missing-details').replace('{!fieldName}', 'Email');
        }
        return valid;
    }

    private Boolean validUser() {
        Boolean valid = true;

        Matcher firstNameMatcher = specialCharacterPattern.matcher(firstName);
        Matcher lastNameMatcher = specialCharacterPattern.matcher(lastName);

        if (!lastNameMatcher.matches() || !firstNameMatcher.matches()){
            message = messages.get('invalid-name');
            valid = false;
        }

        try{
            AMSUserService.validateEmail(email);
        } catch(ExceptionService.CustomException e){
            message = messages.get('invalid-email');
            valid = false;
        }

        if (!AMSUserService.passwordMatches(this.password, this.confirmPassword)) {
            passwordError = messages.get('password-not-matching');
            valid = false;
        }

        if (!AMSUserService.isValidPassword(this.password)){
            passwordError = messages.get('invalid-password');
            valid = false;
        }

        return valid;
    }

    public PageReference retryValidation(){
        message = messages.get('captcha-error');
        return null;
    }

    /**
     * Clean the name that was input to make it display correctly in English
     */
    private String cleanName(String input) {
        // Convert accents and odd chrs down to their non-accented equivalents.
        String accents = 'ÃÁÀÂÄÇÉÈÊËÎÏÌÍÚÛÜÙÓÔÕÖÒÑÝ' + 'àâäçéèêëîïôöùûü' + '°()§<>%^¨*$€£`#;?!+=@©®™"';
        String maj     = 'AAAAACEEEEIIIIUUUUOOOOONY' + 'aaaceeeeiioouuu' + '                         ';

        String out = '';
        for (Integer i = 0 ; i < input.length() ; i++) {
            String car = input.substring(i, i+1);
            Integer idx = accents.indexOf(car);
            if (idx != -1){
                out += maj.substring(idx, idx+1);
            } else if (car == 'Œ') {
                out += 'OE';
            } else if (car == 'Æ') {
                out += 'AE';
            } else {
                out += car;
            }
        }

        return out.trim();
    }
}