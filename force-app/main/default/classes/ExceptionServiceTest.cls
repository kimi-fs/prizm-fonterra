@isTest
private class ExceptionServiceTest {

    @isTest static void friendlyMessageTest1() {
        System.assert(ExceptionService.friendlyMessage(new ExceptionService.customException('Error: You cannot reuse this old password.')).contains('You cannot reuse this old password'));
        System.assert(ExceptionService.friendlyMessage(new ExceptionService.customException('invalid repeated password')).contains('This password has been used recently'));
        System.assert(ExceptionService.friendlyMessage(new ExceptionService.customException('Your password cannot be easy to guess')).contains('Your password cannot be easy to guess'));
        System.assert(ExceptionService.friendlyMessage(new ExceptionService.customException('fall back message')).contains('fall back message'));

        try {
            User theUser = TestClassDataUtil.createIntegrationUser();
            theUser.Username = 'not an email address';
            update theUser;

            System.assert(false, 'expected an error');
        } catch (System.DmlException e) {
            System.assert(ExceptionService.friendlyMessage(e).contains('There was an error setting your new Username'), e);
        }
    }

}