/*
 * Author: John Au (Trineo)
 *
 * Tasks are checked after being created for 15 minutes (so 15 minutes after the SMS is sent to MC)
 * Deliverability is checked for 48 hours, if the task is older than 15 minutes and less than 48 hours, the status is marked as 'DELAYED'
 * and is picked up by the next run of the job.
 * After 48 hours, if we can't ascertain deliverability, the task is marked as 'failed'
 */
global class ScheduleBatchCheckSMSDeliverability implements Schedulable, Database.Batchable<Task>, Database.AllowsCallouts {

    private static final Integer DELAYED_TIME_MINUTES_MAX = 60 * 48;

    global ScheduleBatchCheckSMSDeliverability() {

    }

    global static void schedule() {
        String expression1 = ScheduleService.getCRONExpression('ScheduleBatchCheckSMSDeliverability_1');
        String jobName1 = ScheduleService.getCRONJobName('ScheduleBatchCheckSMSDeliverability_1');

        String expression2 = ScheduleService.getCRONExpression('ScheduleBatchCheckSMSDeliverability_2');
        String jobName2 = ScheduleService.getCRONJobName('ScheduleBatchCheckSMSDeliverability_2');
        if (String.isNotBlank(expression1) && String.isNotBlank(jobName1)) {
            final ScheduleBatchCheckSMSDeliverability job1 = new ScheduleBatchCheckSMSDeliverability();
            System.schedule(jobName1, expression1, job1);
        }
        if (String.isNotBlank(expression2) && String.isNotBlank(jobName2)) {
            final ScheduleBatchCheckSMSDeliverability job2 = new ScheduleBatchCheckSMSDeliverability();
            System.schedule(jobName2, expression2, job2);
        }
    }

    global void execute(SchedulableContext schedulableContext) {
        Database.executeBatch(new ScheduleBatchCheckSMSDeliverability(), 1);
    }

    global List<Task> start(Database.BatchableContext batchableContext) {
        DateTime nowMinus15Minutes = DateTime.now().addMinutes(-15);

        return [SELECT
                    Id,
                    CreatedDate,
                    WhoId,
                    WhatId,
                    SMS_Message_ID__c,
                    SMS_Token__c,
                    Subject,
                    Description
                FROM
                    Task
                WHERE
                    (SMS_Status_Reason__c = :MarketingCloudUtil.MCSTATUSSENT
                    OR SMS_Status_Reason__c = :MarketingCloudUtil.MCSTATUSDELAYED)
                    AND CreatedDate <= :nowMinus15Minutes];
    }

    global void execute(Database.BatchableContext batchableContext, List<Task> tasks) {
        System.assertEquals(tasks.size(), 1);
        //bypass sending updates to aspire
        TriggerHandler.bypass('CaseTrigger');

        Set<String> taskChannelPreferenceSettingNames = new Set<String>();

        for (Task task : tasks) {
            taskChannelPreferenceSettingNames.add(task.Subject);
        }

        List<Channel_Preference_Setting__c> channelPreferenceSettings = [SELECT
                                                                            Id,
                                                                            Name,
                                                                            Exact_Target_Message_Id__c,
                                                                            Exact_Target_Message_Keyword__c,
                                                                            RecordType.DeveloperName
                                                                        FROM
                                                                            Channel_Preference_Setting__c
                                                                        WHERE
                                                                            Name In :taskChannelPreferenceSettingNames];

        Map<String, Channel_Preference_Setting__c> channelPreferenceSettingsByName = new Map<String, Channel_Preference_Setting__c>();

        for (Channel_Preference_Setting__c channelPreferenceSetting : channelPreferenceSettings) {
            channelPreferenceSettingsByName.put(channelPreferenceSetting.Name, channelPreferenceSetting);
        }

        List<Task> tasksForUpdate = new List<Task>();

        for (Task task : tasks) {
            String messageText = task.Description;
            String batchUniqueKey = task.SMS_Message_ID__c;
            String mcToken = task.SMS_Token__c;

            List<Contact> taskContacts = [Select Id, MobilePhone From Contact Where Id = :task.WhoId];

            Channel_Preference_Setting__c channelPreferenceSetting = channelPreferenceSettingsByName.get(task.Subject);

            MarketingCloudUtil.MessageContactDeliveryStatus deliveryStatus;

            try {
                Boolean australia = channelPreferenceSetting.RecordType.DeveloperName.equals('Australia');
                Boolean delivered = false;

                deliveryStatus = MarketingCloudUtil.checkDeliverability(
                    channelPreferenceSetting.Exact_Target_Message_Id__c,
                    mcToken,
                    australia
                );

                if (deliveryStatus.status == 'Finished') {
                    if (australia) {
                        Contact taskContact = taskContacts[0];
                        delivered = hasTransactionCompletedTrackingAU(taskContact.MobilePhone, deliveryStatus.tracking);
                    } else {
                        delivered = hasTransactionCompletedTracking(deliveryStatus.tracking);
                    }
                }

                if (delivered) {
                    task.SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSDELIVERED;
                    task.Status = 'Completed';
                    tasksForUpdate.add(task);
                } else if (task.CreatedDate > System.Now().addMinutes(-DELAYED_TIME_MINUTES_MAX)) {
                    task.SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSDELAYED;
                    tasksForUpdate.add(task);
                } else {
                    task.SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSFAILEDDELIVERY;
                    task.Status = 'Completed';
                    tasksForUpdate.add(task);
                }

            } catch (Exception e) {
                Logger.log('MC callout failure', String.valueOf(e), Logger.LogType.ERROR);
                Logger.saveLog();

                task.SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSFAILEDDELIVERY;
                task.Status = 'Completed';
                tasksForUpdate.add(task);
            }
        }

        update (tasksForUpdate);
    }

    private Boolean hasTransactionCompletedTracking(List<MarketingCloudUtil.MessageContactDeliveryStatusTracking> trackingObjects) {
        List<String> completedStatus = new List<String>{'DELIVRD', 'Transaction completed'};

        Boolean hasTransactionCompletedTracking = false;

        for (MarketingCloudUtil.MessageContactDeliveryStatusTracking trackingObject : trackingObjects) {
            if (completedStatus.contains(trackingObject.message)) {
                hasTransactionCompletedTracking = true;
                break;
            }
        }

        return hasTransactionCompletedTracking;
    }

    private Boolean hasTransactionCompletedTrackingAU(String mobileNumber, List<MarketingCloudUtil.MessageContactDeliveryStatusTracking> trackingObjects) {
        Boolean hasTransactionCompletedTracking = false;
        String normalizedMobileNumber = MarketingCloudUtil.normalizeNumber(mobileNumber);

        // Starting 2020/02/02 Marketing Cloud statuscode changed from '1' to '1000' so we are now only checking message = 'DELIVRD'
        for (MarketingCloudUtil.MessageContactDeliveryStatusTracking trackingObject : trackingObjects) {
            if (trackingObject.message == 'DELIVRD' && normalizedMobileNumber == trackingObject.mobileNumber) {
                hasTransactionCompletedTracking = true;
                break;
            }
        }

        return hasTransactionCompletedTracking;
    }

    private Case createCaseOnFailure(Task task, String messageText, String batchUniqueKey, String errors) {
        Account farm = [Select Id From Account Where Id = :task.WhatId];
        String messageTypeName = task.Subject;
        Id queueId = [SELECT Id from Group where DeveloperName = 'Outbound_Specialist' LIMIT 1].Id;
        Id rtNameId = MarketingCloudUtil.getCaseRecordTypeByMessageTypeName(messageTypeName);
        String description = 'The following text message could not be sent. Please phone instead.' + MarketingCloudUtil.LINEBREAK +
                        messageText + MarketingCloudUtil.LINEBREAK + MarketingCloudUtil.LINEBREAK + MarketingCloudUtil.LINEBREAK +
                        'Audit Trail:' + MarketingCloudUtil.LINEBREAK +
                        MarketingCloudUtil.BATCHKEYSTART + batchUniqueKey + MarketingCloudUtil.BATCHKEYEND + MarketingCloudUtil.LINEBREAK +
                        MarketingCloudUtil.ERRORSTART + errors + MarketingCloudUtil.ERROREND;

        Case c = MarketingCloudUtil.createCaseOnFailure(
            queueId,
            rtNameId,
            farm.Id,
            task.WhoId,
            description,
            MarketingCloudUtil.getCaseTypeByMessageTypeName(messageTypeName),
            MarketingCloudUtil.getCaseSubTypeByMessageTypeName(messageTypeName)
        );

        return c;
    }

    global void finish(Database.BatchableContext batchableContext) {

    }
}