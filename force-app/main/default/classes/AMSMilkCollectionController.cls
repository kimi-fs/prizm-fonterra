/**
 * AMSMilkCollectionController
 *
 * AMSMilkCollection page controller
 *
 * Author: Ed (Trineo)
 * Date: Aug 2017
 */
public with sharing class AMSMilkCollectionController {

    public class FarmWrapper {
        public Account farm { get; set;}
        public String milkCollectionNumberDisplay { get; set; }
        public String milkCollectionNumberUrl { get; set; }

        public FarmWrapper(Account farm, String milkCollectionNumber) {
            this.farm = farm;
            this.milkCollectionNumberDisplay = milkCollectionNumber;
            this.milkCollectionNumberUrl = milkCollectionNumber.deleteWhitespace();
        }
    }

    public List<FarmWrapper> farmWrappers { get; set; }

    private List<Account> usersFarms;
    private Map<Id, Account> originalFarmsMap;
    private Map<String, String> pageMessages;

    public AMSMilkCollectionController() {
        initialiseData();
    }

    private void initialiseData() {
        this.usersFarms = RelationshipAccessService.accessToFarms(AMSContactService.determineContactId(), RelationshipAccessService.Access.ACCESS_MILKING_TIMES);

        // requiry the farms to get the extra fields that we need.
        this.usersFarms = AMSEntityService.getFarms(this.usersFarms);
        this.originalFarmsMap = new Map<Id, Account>();
        for (Account f : usersFarms) {
            originalFarmsMap.put(f.Id, f.clone(true, true));
        }

        this.pageMessages = AMSCommunityUtil.initialisePageMessages('AMSMilkCollection');

        // process the users farms to get the collection numbers and create the
        // wrappers
        this.farmWrappers = new List<FarmWrapper>();
        System.debug('Messages: ' + this.pageMessages);
        for (Account farm : usersFarms) {
            String regionName = 'milk-collection-' + farm.Farm_Region_Name__c;
            System.debug('Looking for: ' + regionName);
            String collectionNumber = this.pageMessages.get(regionName.toLowerCase());
            collectionNumber = collectionNumber == null ? 'NA' : collectionNumber;
            this.farmWrappers.add(new FarmWrapper(farm, collectionNumber));
        }
    }

    public PageReference save() {
        List<Account> farmsThatHaveBeenUpdated = new List<Account>();

        for (FarmWrapper fw : farmWrappers) {
            if (farmUpdated(originalFarmsMap.get(fw.farm.Id), fw.farm)) {
                farmsThatHaveBeenUpdated.add(fw.farm);
            }
        }

        if (!farmsThatHaveBeenUpdated.isEmpty()) {
            User me = AMSUserService.getUser();
            Case milkingUpdateCase = new Case(AccountId = farmsThatHaveBeenUpdated[0].Id,
                                              ContactId = me.ContactId,
                                              RecordTypeId = GlobalUtility.caseCentralSchedulingAURecordTypeId,
                                              Type = 'Collections',
                                              Sub_Type__c = 'Collection Window',
                                              Origin = 'Web',
                                              Status = 'New',
                                              Priority = 'Normal',
                                              Description = createCaseDescription(farmsThatHaveBeenUpdated),
                                              Raised_By__c = 'External',
                                              Sentiment__c = 'NA');
            try {
                AMSCaseService.insertCase(milkingUpdateCase);
                AMSEntityService.updateEntities(farmsThatHaveBeenUpdated);
                initialiseData();
            } catch (Exception e) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ExceptionService.friendlyMessage(e)));
            }
        }

        return null;
    }

    public PageReference cancel() {
        initialiseData();
        return null;
    }

    public static String createCaseDescription(List<Account> updatedFarms) {
        String description = '';

        for (Account farm : updatedFarms) {
            description += 'Farm: ' + (String.isNotBlank(farm.Name) ? farm.Name : '') + '\n';
            description += 'Milk Frequency: ' + (String.isNotBlank(farm.Milking_Frequency_List__c) ? farm.Milking_Frequency_List__c : '') + '\n';
            description += 'Milk Window AM: ' + (String.isNotBlank(farm.Start_AM__c) ? farm.Start_AM__c : '') + ' - ' + (String.isNotBlank(farm.Finish_AM__c) ? farm.Finish_AM__c : '') + '\n';
            description += 'Milk Window PM: ' + (String.isNotBlank(farm.Start_PM__c) ? farm.Start_PM__c : '') + ' - ' + (String.isNotBlank(farm.Finish_PM__c) ? farm.Finish_PM__c : '') + '\n\n';
        }

        return description;
    }

    private Boolean farmUpdated(Account original, Account updated) {
        return original.Milking_Frequency_List__c != updated.Milking_Frequency_List__c ||
               original.Start_AM__c != updated.Start_AM__c ||
               original.Finish_AM__c != updated.Finish_AM__c ||
               original.Start_PM__c != updated.Start_PM__c ||
               original.Finish_PM__c != updated.Finish_PM__c ;
    }
}