/*------------------------------------------------------------
Author:         Sean Soriano
Company:        Davanti Consulting
Description:    Test Class for BatchSetActiveAddress Batch Class
History
11/01/2015      Sean Soriano    Created
------------------------------------------------------------*/
@isTest
private class ScheduleBatchSetActiveAddressTest {

    @testSetup static void createAddresss() {

        User u = TestClassDataUtil.createIntegrationUser();

        //Account/Entities
        Account ent1 = new Account(name = 'Acc1');
        insert ent1;


        List<Address__c> lstTestAddresss = new List<Address__c>();

        //Addresss
        //Null END DATE
        Address__c adr1 = new  Address__c();
        adr1.Start_Date__c = system.today();
        adr1.Country_Code__c = 'NZL';
        adr1.Active__c = false;
        adr1.Entity__c = ent1.id;

        //NOT YET STARTED
        Address__c adr2 = new  Address__c();
        adr2.Start_Date__c = system.today()+1;
        adr2.Country_Code__c = 'NZL';
        adr2.End_Date__c = system.today()+2;
        adr2.Active__c = true;
        adr2.Entity__c = ent1.id;

        //OVERDUE
        Address__c adr3 = new  Address__c();
        adr3.Start_Date__c = system.today()-2;
        adr3.Country_Code__c = 'NZL';
        adr3.End_Date__c = system.today()-1;
        adr3.Active__c = true;
        adr3.Entity__c = ent1.id;

        //ACTIVE
        Address__c adr4 = new  Address__c();
        adr4.Start_Date__c = system.today()-1;
        adr4.Country_Code__c = 'NZL';
        adr4.End_Date__c = system.today()+2;
        adr4.Active__c = false;
        adr4.Entity__c = ent1.id;

        lstTestAddresss.add(adr1);
        lstTestAddresss.add(adr2);
        lstTestAddresss.add(adr3);
        lstTestAddresss.add(adr4);

        system.runAs(u) {
            insert lstTestAddresss;
        }

    }

    @isTest static void test_ScheduleBatchSetActiveAddress() {
        String CRON_EXP = '0 0 0 3 9 ? 2022';

        Test.startTest();
        String jobId = System.schedule('TestScheduleBatchSetActiveAddress', CRON_EXP,
         new ScheduleBatchSetActiveAddress());
        Test.stopTest();
    }

    @isTest static void test_BatchSetActiveAddress() {
        Test.startTest();
            ScheduleBatchSetActiveAddress batch = new ScheduleBatchSetActiveAddress();
            Database.executeBatch(batch);
        Test.stopTest();
    }
}