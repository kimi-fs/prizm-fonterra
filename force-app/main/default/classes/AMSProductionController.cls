/**
 * Created by Nick on 07/08/17.
 *
 * AMSProdQualSummaryController
 *
 * Controller for ProdQualSummary page
 *
 * Author: NL (Trineo)
 * Date: 2017-08-07
 **/
public without sharing class AMSProductionController extends AMSHistoricalDailyProductionController {

    private static final String DEFAULT_REPORT_TYPE = 'Daily Production & Quality';

    public AMSProductionController() {
        super();

        selectedReportTypeName = DEFAULT_REPORT_TYPE;

        graphOptionList = new List<SelectOption>{
            AMSCollectionService.TotalKgMS.getSelectOption(),
            AMSCollectionService.Litres.getSelectOption(),
            AMSCollectionService.BMCC.getSelectOption(),
            AMSCollectionService.ProteinPercentage.getSelectOption(),
            AMSCollectionService.FatPercentage.getSelectOption(),
            AMSCollectionService.KgMSPercentage.getSelectOption(),
            AMSCollectionService.Temp.getSelectOption(),
            AMSCollectionService.FatKg.getSelectOption(),
            AMSCollectionService.ProteinKg.getSelectOption()
        };

        selectedTab = PRODUCTION_TAB_NAME;
    }

    //overridden to set the correct graph option for the AMSProductionController- because you can't override a virtual method twice- this effectively resets the selected graph option that AMSProductionReport sets
    public override void initPropertiesAgain() {
        setSelectedGraphOption(AMSCollectionService.TotalKgMS.getSelectOption().getValue());
    }

    public override AMSProductionReport.ProductionTable getProductionTable() {
        return new AMSProductionReport.ProductionTable(
            new AMSProductionReport.ProductionTableTab(
                PRODUCTION_TAB_NAME,
                true,
                true,
                new List<AMSProductionReport.ProductionTableColumn> {
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.Litres),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.FatPercentage),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.ProteinPercentage),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.KgMSPercentage),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.FatKg),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.ProteinKg),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.TotalKgMS),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.BMCC),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.Temp),
                    new AMSProductionReport.ProductionTableColumn(AMSCollectionService.OtherTestResult)
                }
            ),
            null,
            null,
            null,
            false,
            false
        );
    }

    public override void setSelectedGraphOption(String selectedGraphOption) {
        this.selectedGraphOption = selectedGraphOption;
    }
}