@isTest
private class PreferenceServiceTest {
    private static Id IREL_INDIVIDUALFARM_RECORDTYPE_ID = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Farm').getRecordTypeId();
    private static Id IREL_INDIVIDUALPARTY_RECORDTYPE_ID = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Party').getRecordTypeId();

    private static Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();

    @testSetup public static void setup() {
        TestClassDataUtil.individualDefaultAccountAU();

        List<Channel_Preference_Setting__c> auChannelPreferenceSettings = TestClassDataUtil.createAUChannelPreferenceSettings();

        Contact individual = TestClassDataUtil.createIndividualAu(true);

        List<Reference_Region__c> auReferenceRegions = TestClassDataUtil.createAuReferenceRegions();
        List<Account> farms = TestClassDataUtil.createAuFarms(auReferenceRegions);
        insert farms;

        List<Channel_Preference__c> channelPreferences = new List<Channel_Preference__c>();
        for(Channel_Preference_Setting__c cps : auChannelPreferenceSettings){
            channelPreferences.add(new Channel_Preference__c(
                Channel_Preference_Setting__c = cps.Id,
                Contact_ID__c = individual.Id
            ));
        }

        insert channelPreferences;

        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        List<Individual_Relationship__c> individualRelationships = new List<Individual_Relationship__c>();
        // individual-farm
        individualRelationships.add(new Individual_Relationship__c(
            Active__c = true,
            Individual__c = individual.Id,
            Role__c = 'Director',
            Farm__c = farms[0].Id,
            RecordTypeId = IREL_INDIVIDUALFARM_RECORDTYPE_ID
        ));
        // individual-party
        individualRelationships.add(new Individual_Relationship__c(
            Active__c = true,
            Individual__c = individual.Id,
            Role__c = 'Director',
            Party__c = party.Id,
            Farm__c = farms[0].Id,
            RecordTypeId = IREL_INDIVIDUALPARTY_RECORDTYPE_ID
        ));
        insert individualRelationships;

        List<Entity_Relationship__c> entityRelationships = new List<Entity_Relationship__c>();
        entityRelationships.add(new Entity_Relationship__c(
            Active__c = true,
            Farm__c = farms[0].Id,
            Party__c = party.Id,
            Role__c = 'Owner'

        ));
        insert entityRelationships;
    }

    @isTest static void testGetChannelPreferenceSettingMap(){
        Test.startTest();
        Map<Id, Channel_Preference_Setting__c> mapChannelPreferenceSettingById = PreferenceService.getChannelPreferenceSettingMap();
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference_Setting__c], mapChannelPreferenceSettingById.size(), 'Count does not match');
    }

    @isTest static void testGetChannelPreferenceSettingMap_WithActiveParameter(){
        Test.startTest();
        Map<Id, Channel_Preference_Setting__c> mapChannelPreferenceSettingById = PreferenceService.getChannelPreferenceSettingMap(true);
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference_Setting__c WHERE Active__c = true], mapChannelPreferenceSettingById.size(), 'Count does not match');
    }

    @isTest static void testGetChannelPreferenceSettingMap_WithTypeParameter(){
        Test.startTest();
        Map<Id, Channel_Preference_Setting__c> mapChannelPreferenceSettingById = PreferenceService.getChannelPreferenceSettingMap('Farm');
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference_Setting__c WHERE Type__c = 'Farm'], mapChannelPreferenceSettingById.size(), 'Count does not match');
    }

    @isTest static void testGetChannelPreferenceMap_WithIdParameter(){
        Contact individual = [SELECT Id FROM Contact LIMIT 1];

        Test.startTest();
        Map<Id, Channel_Preference__c> channelPreferences = PreferenceService.getChannelPreferenceMap(individual.Id);
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference__c WHERE Contact_ID__c = :individual.Id], channelPreferences.size(), 'Count does not match');
    }

    @isTest static void testGetChannelPreferenceMap_WithIdSetParameter(){
        Set<Id> contactIds = new Set<Id>();
        contactIds.add([SELECT Id FROM Contact LIMIT 1].Id);

        Test.startTest();
        Map<Id, Channel_Preference__c> channelPreferences = PreferenceService.getChannelPreferenceMap(contactIds);
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference__c], channelPreferences.size(), 'Count does not match');
    }

    @isTest static void testGetChannelPreferenceMap_WithIdAndBooleanParameter(){
        Contact individual = [SELECT Id FROM Contact LIMIT 1];

        Test.startTest();
        Map<Id, Channel_Preference__c> channelPreferences = PreferenceService.getChannelPreferenceMap(individual.Id, true);
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference__c WHERE Contact_ID__c = :individual.Id], channelPreferences.size(), 'Count does not match');
    }

    @isTest static void testGetChannelPreferenceMap_WithIdSetAndBooleanParameter(){
        Set<Id> contactIds = new Set<Id>();
        contactIds.add([SELECT Id FROM Contact LIMIT 1].Id);

        Test.startTest();
        Map<Id, Channel_Preference__c> channelPreferences = PreferenceService.getChannelPreferenceMap(contactIds, true);
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference__c], channelPreferences.size(), 'Count does not match');
    }

    @isTest static void testGetChannelPreferenceMap_WithIdAndTypeParameter(){
        Contact individual = [SELECT Id FROM Contact LIMIT 1];

        Test.startTest();
        Map<Id, Channel_Preference__c> channelPreferences = PreferenceService.getChannelPreferenceMap(individual.Id, 'Personal');
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference__c WHERE Contact_ID__c = :individual.Id AND Channel_Preference_Setting__r.Type__c = 'Personal'], channelPreferences.size(), 'Count does not match');
    }

    @isTest static void testGetChannelPreferenceMap_WithIdSetAndTypeParameter(){
        Set<Id> contactIds = new Set<Id>();
        contactIds.add([SELECT Id FROM Contact LIMIT 1].Id);

        Test.startTest();
        Map<Id, Channel_Preference__c> channelPreferences = PreferenceService.getChannelPreferenceMap(contactIds, 'Personal');
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference__c WHERE Contact_ID__c IN :contactIds AND Channel_Preference_Setting__r.Type__c = 'Personal'], channelPreferences.size(), 'Count does not match');
    }

    @isTest static void testGetChannelPreferenceMap_WithIdActiveAndTypeParameter(){
        Contact individual = [SELECT Id FROM Contact LIMIT 1];

        Test.startTest();
        Map<Id, Channel_Preference__c> channelPreferences = PreferenceService.getChannelPreferenceMap(individual.Id, true, 'Personal');
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference__c WHERE Contact_ID__c = :individual.Id AND Channel_Preference_Setting__r.Active__c = true AND Channel_Preference_Setting__r.Type__c = 'Personal'], channelPreferences.size(), 'Count does not match');
    }

    @isTest static void testGetChannelPreferenceMap_WithIdSetActiveAndTypeParameter(){
        Set<Id> contactIds = new Set<Id>();
        contactIds.add([SELECT Id FROM Contact LIMIT 1].Id);

        Test.startTest();
        Map<Id, Channel_Preference__c> channelPreferences = PreferenceService.getChannelPreferenceMap(contactIds, true, 'Personal');
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference__c WHERE Contact_ID__c IN :contactIds AND Channel_Preference_Setting__r.Active__c = true  AND Channel_Preference_Setting__r.Type__c = 'Personal'], channelPreferences.size(), 'Count does not match');
    }

    @isTest static void testCreateMissingChannelPreferences(){
        List<Contact> contacts = [SELECT Id, RecordTypeId FROM Contact LIMIT 1];

        Map<Id, Channel_Preference__c> emptyChannelPreferenceMap = new Map<Id, Channel_Preference__c>();

        Test.startTest();
        List<Channel_Preference__c> missingChannelPreferences = PreferenceService.createMissingChannelPreferences(contacts, emptyChannelPreferenceMap);
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference_Setting__c WHERE Active__c = true], [SELECT count() FROM Channel_Preference__c WHERE Contact_ID__c = :contacts[0].Id], 'The correct number of channel prefereneces was not created');

    }
}