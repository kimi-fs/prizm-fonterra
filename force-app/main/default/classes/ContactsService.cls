/*------------------------------------------------------------
Author:         Clayde Bael
Company:        Trineo
Description:    Contact Utilities primarily used for Custom Contact Merge
Test Class:     ContactsService_Test

History
12/03/2018      Clayde Bael         Created
-------------------------------------------------------------*/
public with sharing class ContactsService {

    public static final String STRING_SEPARATOR = '|';
    public static final String NOT_APPLICABLE = 'N/A';

    // Default Contact Theme color
    public static String getThemeColor() {
        return GlobalUtility.getThemeColor(SObjectType.Contact.getLabelPlural(), 'Sales', 'theme3', 'primary');
    }

    /**
    * Retrieve the List of Contact Layout Items based on the Contact Id and the Current user Profile
    */
    public static List<LayoutItem> retrieveLayoutItems(Id contactId) {

        HttpResponse response = getLayoutAndRecord(contactId);
        APIResponse data = (APIResponse)JSON.deserialize(response.getBody(), APIResponse.class);
        Layout fullView = data.layouts.Contact.values()[0].get('Full').get('View');

        List<LayoutItem> layoutItems = new List<LayoutItem>();
        for (Section section : fullView.sections) {
            for (LayoutRow layoutRow : section.layoutRows) {
                for (LayoutItem layoutItem : layoutRow.layoutItems) {
                    layoutItems.add(layoutItem);
                }
            }
        }
        return layoutItems;
    }

    /**
     * Call the UI API and retrieve the Layout and record details given an Object ID
     */
    public static HttpResponse getLayoutAndRecord(Id objectId) {

        String endpoint = Url.getSalesforceBaseUrl().toExternalForm() + '/services/data/v41.0/ui-api/record-ui/' + objectId;
        HttpRequest request = new HttpRequest();
        request.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        HttpResponse response = new Http().send(request);

        // Throw any error
        if (response.getStatusCode() != 200) {
            Map<String, Object> body = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            throw new ExceptionService.CustomException('UI-API Error: ' + body.get('message'));
        }

        return response;

    }

    // Retrieve all the Reference Regions given a list of External IDs
    // The returned Map is a Map of External ID to Reference Region
    public static Map<String, Reference_Region__c> getReferenceRegions(Set<String> externalIds){

        List<Reference_Region__c> regions =
            [SELECT Id, Name, Reference_Region_ID__c
             FROM Reference_Region__c
             WHERE Reference_Region_ID__c IN :externalIds];

        Map<String, Reference_Region__c> regionMap = new Map<String, Reference_Region__c>();
        for (Reference_Region__c region : regions) {
            regionMap.put(region.Reference_Region_ID__c, region);
        }
        return regionMap;
    }

    public static Map<Id, User> retrieveUsers(List<Id> userIds) {
        Map<Id, User> users = new Map<Id, User>([
                SELECT  ContactId,
                        FirstName, MiddleName, LastName,
                        Username,
                        Connected_App_Access__c,
                        ProfileId,
                        First_Password_Change__c,
                        Email
                FROM User
                WHERE Id IN :userIds]);

        return users;
    }


    public class APIResponse {
        public final ResponseLayouts layouts;
    }
    public class ResponseLayouts {
        public Map<String, Map<String, Map<String, Layout>>> Contact;
    }
    public class Layout {
        public final List<Section> sections;
    }
    public class Section {
        public final List<LayoutRow> layoutRows;
    }
    public class LayoutRow {
        public final List<LayoutItem> layoutItems;
    }
    public class LayoutItem {
        public final String label;
        public final Boolean editableForUpdate;
        public final List<LayoutComponent> layoutComponents;
    }
    public class LayoutComponent {
        public final String apiName;
        public final String componentType;
    }

    // A Custom Individual Relationship class for sorting
    public class IndividualRelationship implements Comparable {

        public Individual_Relationship__c iRelationship {get;set;}
        public IndividualRelationship (Individual_Relationship__c iRelationship) {
            this.iRelationship = iRelationship;
        }

        // Define sorting for IR based on Entity, Role, TPA and Creation date
        public Integer compareTo(Object otherIRelObj) {

            //The purpose of this Ordering is to
            //  - Group together Entity and Role (Group duplicates)
            //  - Prioritize TPA over non-TPA I-F Relationships (For duplicate Farms)
            //  - Prioritize earliest Creation Date in case of duplicate Entity/Role
            IndividualRelationship otherIRel = (IndividualRelationship) otherIRelObj;

            String sortString = this.iRelationship.RecordType.DeveloperName + STRING_SEPARATOR
                    + this.iRelationship.Farm__c + STRING_SEPARATOR
                    + this.iRelationship.Organisation__c + STRING_SEPARATOR
                    + this.iRelationship.Party__c + STRING_SEPARATOR
                    + this.iRelationship.Role__c + STRING_SEPARATOR
                    + this.iRelationship.Creation_Date__c;

            String otherSortString = otherIRel.iRelationship.RecordType.DeveloperName + STRING_SEPARATOR
                    + otherIRel.iRelationship.Farm__c + STRING_SEPARATOR
                    + otherIRel.iRelationship.Organisation__c + STRING_SEPARATOR
                    + otherIRel.iRelationship.Party__c + STRING_SEPARATOR
                    + otherIRel.iRelationship.Role__c + STRING_SEPARATOR
                    + otherIRel.iRelationship.Creation_Date__c;

            return sortString.toLowerCase().compareTo(otherSortString.toLowerCase());
        }

    }

    // Copies Group subscriptions from one user to another
    @future
    public static void copyCommunitiesGroups(Id fromUserId, Id toUserId, Boolean removeUserAfter) {
        List<CollaborationGroupMember>  userGroupMembers = [SELECT Id, CollaborationGroupId, MemberId, CollaborationGroup.NetworkId
                                                            FROM CollaborationGroupMember WHERE MemberId  IN (:fromUserId, :toUserId) ];

        // CollaborationGroupId => CollaborationGroupMember
        Map<Id, CollaborationGroupMember> toUserGroups = new Map<Id, CollaborationGroupMember> ();
        Map<Id, CollaborationGroupMember> fromUserGroups = new Map<Id, CollaborationGroupMember> ();

        for (CollaborationGroupMember groupMember : userGroupMembers) {
            if (groupMember.MemberId == toUserId) {
                toUserGroups.put(groupMember.CollaborationGroupId, groupMember);
            }
            else if (groupMember.MemberId == fromUserId) {
                fromUserGroups.put(groupMember.CollaborationGroupId, groupMember);
            }
        }

        Set<Id> toUserGroupIds  = toUserGroups.keySet();
        Set<Id> fromUserGroupIds = fromUserGroups.keySet();
        List<CollaborationGroupMember> groupMemberToAdd = new List<CollaborationGroupMember>();
        List<CollaborationGroupMember> groupMemberToDelete = new LIst<CollaborationGroupMember>();

        // Delete groups where fromUser user is not a member
        for (Id groupId : toUserGroupIds) {
            if (!fromUserGroupIds.contains(groupId)) {
                groupMemberToDelete.add(toUserGroups.get(groupId));
            }
        }

        List<Id> affectedNetworkIds = new List<Id>();
        // Add toUser user to same group as fromUser
        for (Id groupId : fromUserGroupIds) {

            CollaborationGroupMember groupMember = fromUserGroups.get(groupId);
            if (!toUserGroupIds.contains(groupId)) {
                groupMemberToAdd.add(new CollaborationGroupMember(MemberId = toUserId, CollaborationGroupId = groupMember.CollaborationGroupId));
                affectedNetworkIds.add(groupMember.CollaborationGroup.NetworkId);
            }

            // Remove User from group after
            if (removeUserAfter) {
                groupMemberToDelete.add(groupMember);
            }
        }

        List<NetworkMember> userMemberships = [SELECT Id, PreferencesDisableAllFeedsEmail FROM NetworkMember WHERE  NetworkId IN :affectedNetworkIds and MemberId = :toUserId];
        if (!groupMemberToAdd.isEmpty()) {
            // Temporarily Disable Notifications
            for (NetworkMember membership : userMemberships) {
                membership.PreferencesDisableAllFeedsEmail = true;
            }
            update userMemberships;

            // Add Groups
            insert groupMemberToAdd;
        }

        if (!groupMemberToDelete.isEmpty()) {
            delete groupMemberToDelete;
        }

        // Re-enable notifications
        if (!groupMemberToAdd.isEmpty()) {
            for (NetworkMember membership : userMemberships) {
                membership.PreferencesDisableAllFeedsEmail = false;
            }
            update userMemberships;
        }
    }

}