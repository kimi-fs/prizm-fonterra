/**
 * AMSGoalActionControllerTest.cls
 * Description: Test for AMSGoalActionController.cls
 * @author: Ricardo Hurtado (Trineo)
 * @date: December 2018
 */
@isTest
private class AMSGoalActionControllerTest {

    private static String USER_EMAIL = 'amsUser@thisTest.com';
    private static String GOAL_NAME = 'Some goal';
    private static String ACTION_NAME = 'Some action';
    private static String ACTION_ASSIGNED_TO = 'Assigned Person';
    private static String ACTION_NEW_ASSIGNED_TO = 'New Assigned Person';

    public static String ACTION_NOT_STARTED = 'Not Started';
    public static String ACTION_IN_PROGRESS = 'In Progress';
    public static String ACTION_COMPLETED = 'Completed';

    @testSetup
    static void testSetup() {
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.createIntegrationUser();

        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu(USER_EMAIL);
        Contact contact = [SELECT Id FROM Contact WHERE Id = :communityUser.ContactId];
        Goal__c goal = TestClassDataUtil.createGoal(true, contact);
        TestClassDataUtil.createAction(goal, true);
    }

    @isTest
    public static void testConstructor() {
        User communityUser = getCommunityUser();
        List<Action__c> actions = getActions();

        System.runAs(communityUser) {
            Test.startTest();

            AMSGoalActionController controller = setPageController(actions[0].Id);

            System.assertEquals(controller.action.Action_Name__c, ACTION_NAME);
            System.assertEquals(controller.goalId, actions[0].Goal__c);
            System.assertEquals(controller.goalName, GOAL_NAME);
            System.assertEquals(controller.actionAssignedTo, ACTION_ASSIGNED_TO);

            Test.stopTest();
        }
    }

    @isTest
    public static void testStartAction() {
        testActionStatus(ACTION_NOT_STARTED, ACTION_IN_PROGRESS);
    }

    @isTest
    public static void testStopAction() {
        testActionStatus(ACTION_IN_PROGRESS, ACTION_NOT_STARTED);
    }

    @isTest
    public static void testCompleteAction() {
        testActionStatus(ACTION_IN_PROGRESS, ACTION_COMPLETED);
    }

    @isTest
    public static void testActionAssignedTo() {
        User communityUser = getCommunityUser();
        Action__c currentAction = getAction();

        System.runAs(communityUser) {
            Test.startTest();

            System.assertEquals(currentAction.Assigned_To__c, ACTION_ASSIGNED_TO);

            AMSGoalActionController controller = setPageController(currentAction.Id);

            controller.actionAssignedTo = ACTION_NEW_ASSIGNED_TO;
            controller.saveActionAssignedTo();


            Test.stopTest();

        }
        Action__c updatedAction = getAction();
        System.assertEquals(updatedAction.Assigned_To__c, ACTION_NEW_ASSIGNED_TO);
    }

    private static User getCommunityUser() {
        User communityUser = [SELECT Id, ContactId FROM User WHERE Email = :USER_EMAIL];

        return communityUser;
    }

    private static List<Action__c> getActions() {
        Goal__c goal = [SELECT Id FROM Goal__c WHERE Name = :GOAL_NAME];
        List<Action__c> actions = [SELECT Id, Goal__c, Source_Status__c, Assigned_To__c FROM Action__c WHERE Goal__c = :goal.Id];

        return actions;
    }

    private static Action__c getAction() {
        List<Action__c> actions = getActions();

        return actions[0];
    }

    private static Action__c updateActionStatus(String status) {
        List<Action__c> actions = getActions();
        actions[0].Source_Status__c = status;
        update actions[0];

        return actions[0];
    }

    private static AMSGoalActionController setPageController(Id actionId) {
        PageReference pageRef = Page.AMSGoalAction;
        pageRef.getParameters().put('actionId', actionId);

        System.Test.setCurrentPage(pageRef);

        AMSGoalActionController controller = new AMSGoalActionController();

        return controller;
    }

    private static void testActionStatus(String currentStatus, String newStatus) {
        User communityUser = getCommunityUser();
        Action__c currentAction = updateActionStatus(currentStatus);

        System.runAs(communityUser) {
            Test.startTest();

            System.assertEquals(currentAction.Source_Status__c, currentStatus);

            AMSGoalActionController controller = setPageController(currentAction.Id);

            if (newStatus == ACTION_IN_PROGRESS) {
                controller.startAction();
            } else if (newStatus == ACTION_COMPLETED) {
                controller.completeAction();
            } else {
                controller.stopAction();
            }


            Test.stopTest();
        }
        Action__c updatedAction = getAction();
        System.assertEquals(updatedAction.Source_Status__c, newStatus);
    }
}