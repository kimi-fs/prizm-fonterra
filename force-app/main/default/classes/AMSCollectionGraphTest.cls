@isTest
private class AMSCollectionGraphTest {

    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup static void testSetup() {
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.createIntegrationUser();

        TestClassDataUtil.createFarmsAndIndividualsAu(USER_EMAIL);
    }

    @isTest static void testConstructor_Day_KgMS() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.DAY);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.KgMS);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Month_KgMS() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.MONTH);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.KgMS);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Day_Litres() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.DAY);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.Litres);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Month_BMCC() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.MONTH);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.BMCC);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Month_ProteinPercentage() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.MONTH);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.ProteinPercentage);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Month_FatPercentage() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.MONTH);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.FatPercentage);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Month_KgMSPercentage() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.MONTH);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.KgMSPercentage);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Month_Temp() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.MONTH);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.Temp);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Month_FatKg() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.MONTH);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.FatKg);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Month_ProteinKg() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.MONTH);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.ProteinKg);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Month_KgMSPerCow() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.MONTH);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.KgMSPerCow);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Month_ProteinFatRatio() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.MONTH);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.ProteinFatRatio);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Month_KgMSPerHectare() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.MONTH);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.KgMSPerHectare);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }

    @isTest static void testConstructor_Month_TotalKgMS() {
        User communityUser = getCommunityUser();
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = createCollectionPeriodForm(communityUser.ContactId, AMSCollectionService.MONTH);

        System.runAs(communityUser) {
            AMSCollectionGraph cont = new AMSCollectionGraph(collectionPeriodForm, AMSCollectionService.TotalKgMS);

            System.assertNotEquals(null, cont.categorySeries);
            System.assertNotEquals(null, cont.selectedSeasonSeries);
            System.assertNotEquals(null, cont.priorSeasonSeries);
            System.assertNotEquals(null, cont.varianceSeasonSeries);
            System.assertNotEquals(null, cont.targetSeasonSeries);
        }
    }


    public static AMSCollectionService.CollectionPeriodForm createCollectionPeriodForm(Id contactId, String degreeOfIteration){
        List<Account> farms = [
                SELECT Id
                FROM Account
                WHERE Id IN (
                        SELECT Farm__c
                        FROM Individual_Relationship__c
                        WHERE Individual__c = :contactId
                        AND Active__c = true
                )
        ];
        Date today = System.today();
        Date currentSeasonStartDate;
        Date currentSeasonEndDate;
        if (today < Date.newInstance(today.year(), 5, 1)) {
            currentSeasonStartDate = Date.newInstance(today.year() - 1, 5, 1);
        } else {
            currentSeasonStartDate = Date.newInstance(today.year(), 5, 1);
        }
        if (today > Date.newInstance(today.year(), 4, 30)) {
            currentSeasonEndDate = Date.newInstance(today.year() + 1, 4, 30);
        } else {
            currentSeasonEndDate = Date.newInstance(today.year(), 4, 30);
        }

        Reference_Period__c currentReferencePeriod = TestClassDataUtil.createReferencePeriod('Milking Season AU', 'current', false);
        currentReferencePeriod.Start_Date__c = currentSeasonStartDate;
        currentReferencePeriod.End_Date__c = currentSeasonEndDate;
        currentReferencePeriod.Status__c = 'Current';
        insert currentReferencePeriod;
        Reference_Period__c priorReferencePeriod = TestClassDataUtil.createReferencePeriod('Milking Season AU', 'prior',false);
        priorReferencePeriod.Start_Date__c = currentSeasonStartDate.addYears(-1);
        priorReferencePeriod.End_Date__c = currentSeasonEndDate.addYears(-1);
        priorReferencePeriod.Status__c = 'Past';
        insert priorReferencePeriod;

        Date fromDateCurrentSeason = Date.today().addMonths(-1).toStartOfMonth();
        Date toDateCurrentSeason = fromDateCurrentSeason.addMonths(1).addDays(-1);
        Date fromDatePriorSeason = fromDateCurrentSeason.addYears(-1);
        Date toDatePriorSeason = toDateCurrentSeason.addYears(-1);

        Farm_Season__c currentFarmSeason = TestClassDataUtil.createFarmSeasons(new List<Account>{
                farms[0]
        }, new List<Reference_Period__c>{
                currentReferencePeriod
        }, false)[0];
        currentFarmSeason.Peak_Cows__c = 10;
        currentFarmSeason.Dairy_Hectares__c = 10;
        insert currentFarmSeason;
        Farm_Season__c priorFarmSeason = TestClassDataUtil.createFarmSeasons(new List<Account>{
                farms[0]
        }, new List<Reference_Period__c>{
                priorReferencePeriod
        }, false)[0];
        priorFarmSeason.Peak_Cows__c = 10;
        priorFarmSeason.Dairy_Hectares__c = 10;
        insert priorFarmSeason;
        Id selectedFarmId = farms[0].Id;

        return new AMSCollectionService.CollectionPeriodForm(fromDateCurrentSeason, toDateCurrentSeason, fromDatePriorSeason, toDatePriorSeason, priorFarmSeason, currentFarmSeason, selectedFarmId, degreeOfIteration);

    }


    private static User getCommunityUser() {
        User theUser = [SELECT Id, ContactId FROM User WHERE Email = :USER_EMAIL ];
        return theUser;
    }
}