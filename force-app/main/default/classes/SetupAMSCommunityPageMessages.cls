/**
 * Utility class to initalise Community Page messages in an org
 *
 * This class is called from SetupOrg class when setting up a new sandbox
 * This is also called from unit tests to set up data
 *
 * @author: Amelia (Trineo)
 * @date: Augugst 2017
 */
public without sharing class SetupAMSCommunityPageMessages {

    public static Map<String, List<MessageWrapper>> pageMessageMap;

    // OCD Alphabetical ordering by page name is required, or pain will be yours.
    static {
        pageMessageMap = new Map<String, List<MessageWrapper>>();
         pageMessageMap.put('AMSContact',
            new List<MessageWrapper>{
                new MessageWrapper('unknown-error', 'Something went wrong, we were unable to record your enquiry.  Please contact our Service Team on 1800 266 674')
            }
        );
        pageMessageMap.put('AMSEmailVerification',
            new List<MessageWrapper>{
                new MessageWrapper('email-sent', 'Resent the email address verification email, check your Junk Email folder as well as your Inbox.'),
                new MessageWrapper('unknown-error', 'Something went wrong, we were unable to resend an email verification.  Please contact our Service Team on 1800 266 674')
            }
        );
        pageMessageMap.put('AMSFarmDetails',
            new List<MessageWrapper>{
                new MessageWrapper('unknown-error', 'Something went wrong.  Please contact our Service Team on 1800 266 674.')
            }
        );
        pageMessageMap.put('AMSForgot',
            new List<MessageWrapper>{
                new MessageWrapper('missing-email', 'Please enter your email address.'),
                new MessageWrapper('email-sent', 'An e-mail containing instructions on how to reset your password has been sent to the address provided.'),
                new MessageWrapper('unknown-error', 'Something went wrong.'),
                new MessageWrapper('unknown-user', 'An e-mail containing instructions on how to reset your password has been sent to the address provided.')
            }
        );
        pageMessageMap.put('AMSHerdAndHectares',
            new List<MessageWrapper>{
                new MessageWrapper('successful-update', 'Herd size and hectares successfully updated.'),
                new MessageWrapper('unknown-error', 'Something went wrong.'),
                new MessageWrapper('cows-zero', 'Peak cows must be greater than 0 and be a whole number'),
                new MessageWrapper('hectars-zero', 'Dairy Hectares must be greater than 0 and be a whole number')
            }
        );
        pageMessageMap.put('AMSLogin',
            new List<MessageWrapper>{
                new MessageWrapper('failed-login', 'There was an error trying to log you in. Please check your username and password or click the Forgot Password link below, alternatively contact the service desk on 1800 266 674 to reset your password.')
            }
        );
        pageMessageMap.put('AMSMilkCollection',
            new List<MessageWrapper>{
                new MessageWrapper('milk-collection-north', '03 54843529'),
                new MessageWrapper('milk-collection-east', '03 56242663'),
                new MessageWrapper('milk-collection-south', '03 64212123 or 03 64420230'),
                new MessageWrapper('milk-collection-west', '03 55951606')
            }
        );
        pageMessageMap.put('AMSMyDetails',
            new List<MessageWrapper>{
                new MessageWrapper('confirm-details', 'Thank you for confirming your details'),
                new MessageWrapper('email-used', 'There is already a user with that email'),
                new MessageWrapper('invalid-mobile', 'Please enter your mobile phone number in one of the two following formats: 0439 125 109 or +61 439 125 109'),
                new MessageWrapper('invalid-number', 'Please enter a valid {!fieldName} number'),
                new MessageWrapper('missing-details', '{!fieldName} is required.'),
                new MessageWrapper('missing-postcode', 'Please enter your {!RecordType} Address Post Code so we can update your details'),
                new MessageWrapper('save-success', 'Your changes have successfully been saved, this can take up to 5 minutes to be shown on all pages'),
                new MessageWrapper('unknown-error', 'Something went wrong')
            }
        );
        pageMessageMap.put('AMSNewsDetail',
            new List<MessageWrapper>{
                new MessageWrapper('no-article', 'Something went wrong, we can\'t find the article.')
            }
        );
        pageMessageMap.put('AMSNews',
            new List<MessageWrapper>{
                new MessageWrapper('no-news', 'There are no news articles to display')
            }
        );
        pageMessageMap.put('AMSNewUser',
            new List<MessageWrapper>{
                new MessageWrapper('captcha-error', 'We have not been able to validate you are a person, please try again.'),
                new MessageWrapper('invalid-email', 'Email should be with xxxx@yyyy.zzz format.'),
                new MessageWrapper('invalid-name', 'You have entered an invalid name, please try again.'),
                new MessageWrapper('invalid-password', 'You have entered an invalid password. Please try again.'),
                new MessageWrapper('invalid-phone', 'You have entered an invalid number, please try this format +61 3 55553344.'),
                new MessageWrapper('missing-details', '{!fieldName} is required.'),
                new MessageWrapper('name-too-long', 'Your name is too long, the maximum allowed for first and last name together is 30 characters'),
                new MessageWrapper('password-not-matching', 'Passwords do not match. Please try again.'),
                new MessageWrapper('unknown-error', 'Sorry, we could not complete your registration.'),
                new MessageWrapper('user-exists', 'We recognised that you are already an existing user, click on login links below.')
            }
        );
        pageMessageMap.put('AMSPasswordChange',
            new List<MessageWrapper>{
                new MessageWrapper('incorrect-current-password', 'The value you entered for your "Current Password" is incorrect. Please try again.'),
                new MessageWrapper('missing-current-password', 'Please enter your current password.'),
                new MessageWrapper('not-current-user', 'You cannot change this user’s password.'),
                new MessageWrapper('invalid-password', 'You have entered an invalid password. Please try again.'),
                new MessageWrapper('password-not-matching', 'Passwords do not match. Please try again.'),
                new MessageWrapper('unknown-error', 'Something went wrong.')
            }
        );
        pageMessageMap.put('AMSPasswordReset',
            new List<MessageWrapper>{
                new MessageWrapper('invalid-password', 'You have entered an invalid password. Please try again.'),
                new MessageWrapper('password-not-matching', 'Passwords do not match. Please try again.'),
                new MessageWrapper('unknown-error', 'Something went wrong.')
            }
        );
        pageMessageMap.put('AMSProdQualReportTemplate',
            new List<MessageWrapper>{
                new MessageWrapper('no-data', 'You have no Production or Quality farm data to view.')
            }
        );
        pageMessageMap.put('AMSStatements',
            new List<MessageWrapper>{
                new MessageWrapper('statement-retrieval-error', 'Unable to download statement.')
            }
        );
        pageMessageMap.put('AMSThirdParty',
            new List<MessageWrapper>{
                new MessageWrapper('user-not-found1', 'The Username (email address) you have entered is not an existing user in Farm Source. '),
                new MessageWrapper('user-not-found2', 'To grant someone access to your farm information, the other party first need to register on the Farm Source web site. Once they have registered themselves as a new user, they can tell you their user name and then you can add them here.'),
                new MessageWrapper('relationship-exists', 'This user already has access to {!farmName}.'),
                new MessageWrapper('role-missing', 'You must specify a role for this user.'),
                new MessageWrapper('select-one-farm', 'Please select at least 1 Farm.'),
                new MessageWrapper('select-one-category', 'Please select at least 1 category.'),
                new MessageWrapper('date-in-past', 'Please select an end date in the future')
            }
        );
    }

    public static Map<String, Map<String, Community_Page_Messages__c>> existingMap;
    static {
        List<Community_Page_Messages__c> existingPageMessages = Community_Page_Messages__c.getAll().values();

        existingMap = new Map<String, Map<String, Community_Page_Messages__c>>();
        for (Community_Page_Messages__c pm : existingPageMessages) {
            if (!existingMap.containsKey(pm.Page__c)) {
                existingMap.put(pm.Page__c, new Map<String, Community_Page_Messages__c>());
            }
            existingMap.get(pm.Page__c).put(pm.Reason__c, pm);
        }
    }

    public static void createPageMessages() {
        List<Community_Page_Messages__c> pageMessages = new List<Community_Page_Messages__c>();
        for (String page : pageMessageMap.keySet()) {
            pageMessages.addAll(createPageMessagesForPage(page, pageMessageMap.get(page), existingMap));
        }
        upsert pageMessages;
    }

    public static List<Community_Page_Messages__c> createPageMessagesForPage(String pageName, List<MessageWrapper> MessageWrappers, Map<String, Map<String, Community_Page_Messages__c>> existingMap) {
        List<Community_Page_Messages__c> pageMessages = new List<Community_Page_Messages__c>();
        Integer counter = findNumberToIncrement(pageName);
        for (MessageWrapper pmw : MessageWrappers) {
            if (!existingMap.containsKey(pageName) || !existingMap.get(pageName).containsKey(pmw.reason)) {
                pageMessages.add(new Community_Page_Messages__c(Name = pageName + counter, Page__c = pageName, Reason__c = pmw.reason, Message__c = pmw.message));
                counter ++;
            }
        }
        return pageMessages;
    }

    public static Integer findNumberToIncrement(String pageName) {
        Integer count = 0;

        if (existingMap.containsKey(pageName)) {
            List<Community_Page_Messages__c> existingPageMessages = existingMap.get(pageName).values();
            List<String> pageMessageNames = new List<String>();
            for (Community_Page_Messages__c pm : existingPageMessages) {
                pageMessageNames.add(pm.Name);
            }
            if (!pageMessageNames.isEmpty()) {
                pageMessageNames.sort();
                Integer listSize = pageMessageNames.size() - 1;
                String lastNumber = pageMessageNames[pageMessageNames.size() - 1].removeStart(existingPageMessages[existingPageMessages.size() - 1].Page__c);
                if (lastNumber.isNumeric()) {
                    count = Integer.valueOf(lastNumber);
                }
            }
        }
        return count + 1;

    }

    public class MessageWrapper {
        public String reason;
        public String message;

        public MessageWrapper(String reason, String message) {
            this.reason = reason;
            this.message = message;
        }
    }

}