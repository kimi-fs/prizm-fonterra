public class Prizm_AdvanceTransactionObject{
    public String operation_type {get; set;}
    public boolean is_active {get; set;}
    public String advance_number {get; set;}
    public decimal amount {get; set;}
    public String contract_number {get; set;}
    public String contract_transaction_number {get; set;}
    public date date_field {get; set;}
    public String description {get; set;}
    public String record_type_name {get; set;}
    public String status {get; set;}
    public String transaction_type {get; set;}
}