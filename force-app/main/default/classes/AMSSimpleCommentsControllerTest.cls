/**
 * AMSSimpleCommentsControllerTest.cls
 * Description: Test for AMSSimpleCommentsController.cls
 *
 * @author: John Au
 * @date: December 2018
 */
@IsTest
private class AMSSimpleCommentsControllerTest {

    @IsTest
    private static void testController() {
        User u = TestClassDataUtil.createAMSCommunityUserAndIndividual('testuser@test.com');

        AMSSimpleCommentsController controller = new AMSSimpleCommentsController();

        controller.parentIdParam = u.Id;
    }
}