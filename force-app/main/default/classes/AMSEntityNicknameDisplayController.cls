/**
 * Description: Controller for the AMSEntityNicknameDisplay component
 * Retrieves all irels for the individual, filters out everything that isn't a drel, populates a map with the entityId (currently just farm) and either:
 * a) the farm's nickname (if it exists) OR
 * b) defaults to the farm name
 *
 * @author: John Au (Trineo)
 * @date: October 2018
 */
public class AMSEntityNicknameDisplayController {

    public static Map<String, String> entityIdToEntityNicknameMap {
        get {
            if (AMSEntityNicknameDisplayController.entityIdToEntityNicknameMap == null) {
                AMSEntityNicknameDisplayController.entityIdToEntityNicknameMap = new Map<String, String>();

                Id currentUserContactId = AMSContactService.determineContactId();
                List<Individual_Relationship__c> allIndividualRelationships = RelationshipAccessService.allIndividualRelationshipsForIndividual(currentUserContactId);

                for (Individual_Relationship__c individualRelationship : allIndividualRelationships) {
                    if (individualRelationship.RecordTypeId != GlobalUtility.derivedRelationshipRecordTypeId && individualRelationship.RecordTypeId != GlobalUtility.individualRestrictedFarmRecordTypeId) {
                        continue; //only check for nickname on drels and third party relationships
                    }

                    String nickname = '';

                    if (!String.isBlank(individualRelationship.Entity_Nickname__c)) {
                        nickname = individualRelationship.Entity_Nickname__c;
                    }

                    AMSEntityNicknameDisplayController.entityIdToEntityNicknameMap.put(individualRelationship.Farm__r.Id, nickname);
                }
            }

            return AMSEntityNicknameDisplayController.entityIdToEntityNicknameMap;
        }
    }

    public String entityIdParam { get; set; }
    public String entityNickname {
        get {
            if (AMSEntityNicknameDisplayController.entityIdToEntityNicknameMap.containsKey(entityIdParam)) {
                return AMSEntityNicknameDisplayController.entityIdToEntityNicknameMap.get(entityIdParam);
            } else {
                return '';
            }
        }

    }

    public AMSEntityNicknameDisplayController() {

    }

}