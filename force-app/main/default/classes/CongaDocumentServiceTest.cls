/**
 * CongaDocumentServiceTest to test the CongaDocumentService
 *
 * To test the CongaDocumentService we mock the Conga Endpoint, and the service code has been modified
 * to skip JWT when in a test (I didn't want to try mocking that).
 *
 * @author Ed (trineo)
 * @date Feb 2018
 */
@isTest
global with sharing class CongaDocumentServiceTest {
    /**
     * We will be needing an Application and Applicant, not all filled out, but passing validation rules.
     */
    @testSetup static void testSetup(){
        TestClassDataUtil.createCommunityExtendedSessionCustomSettings();

        Contact individual = TestClassDataUtil.createIndividualAU('Joe', 'Smith', false);
        individual.Email = 'joe@example.com';
        insert individual;
    }

    //FIXME needs AU specific tests

    /**
     * Mock out the Conga Http calls
     */
    global class CongaMockSuccess implements HttpCalloutMock {
        global HTTPResponse respond(final HTTPRequest req) {
            final HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(200);
            res.setBody('DEADBEEF');
            return res;
        }
    }

    global class CongaMockFailure implements HttpCalloutMock {
        global HTTPResponse respond(final HTTPRequest req) {
            final HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setStatusCode(500);
            res.setBody('Failure');
            return res;
        }
    }
}