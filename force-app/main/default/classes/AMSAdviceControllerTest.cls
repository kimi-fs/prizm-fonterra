/**
 * Description: Test class for AMSAdviceContoller
 * @author: Amelia (Trineo)
 * @date: July 2017
 */
@isTest
private class AMSAdviceControllerTest {
    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup static void testSetup() {
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
    }

    @isTest static void testWrapperController_WithLabelAndNameParameters() {
        String TEST_LABEL = 'Test Label';
        String TEST_NAME = 'Test_Name';

        Test.startTest();
        AMSKnowledgeService.ArticleCategoryWrapper acw = new AMSKnowledgeService.ArticleCategoryWrapper(TEST_NAME, TEST_LABEL);
        Test.stopTest();

        System.assertEquals(TEST_NAME, acw.name, 'Name not set correctly');
        System.assertEquals(TEST_LABEL, acw.label, 'Label not set correctly');
    }

    @isTest static void testWrapperController_WithArticleParameter() {
        TestClassDataUtil.createAMSAdviceArticleAndGiveRandomCategory(1);
        Knowledge__kav adviceArticle = [
                                            SELECT Id
                                            FROM Knowledge__kav
                                            WHERE PublishStatus = 'Online'
                                                    AND Language = 'en_US'
                                        ];

        Test.startTest();
        AMSKnowledgeService.ArticleCategoryWrapper acw = new AMSKnowledgeService.ArticleCategoryWrapper(adviceArticle);
        Test.stopTest();

        System.assertNotEquals(null, acw.adviceArticle, 'Article not set correctly');
    }

    @isTest static void testSetAdviceArticles_LandingPage() {
        Integer NEWS_ARTICLE_COUNT = 50;
        TestClassDataUtil.createAMSAdviceArticleAndGiveRandomCategory(NEWS_ARTICLE_COUNT, 'Landing');

        System.runAs(getCommunityUser()) {

            AMSAdviceController controller = new AMSAdviceController();
            Test.startTest();
            System.assertEquals(NEWS_ARTICLE_COUNT, controller.adviceArticles.size(), 'Advice Articles not set correctly');
            Test.stopTest();
        }
    }

    @isTest static void testSetAdviceArticles_NotLandingPage() {
        Integer NEWS_ARTICLE_COUNT = 50;
        TestClassDataUtil.createAMSAdviceArticleAndGiveRandomCategory(NEWS_ARTICLE_COUNT, 'Expand');

        System.runAs(getCommunityUser()) {

            AMSAdviceController controller = new AMSAdviceController();
            Test.startTest();
            System.assertEquals(NEWS_ARTICLE_COUNT, controller.adviceArticles.size(), 'Advice Articles not set correctly');
            Test.stopTest();
        }
    }

    @isTest static void testSetDataCategoryLabels() {
        Map<DataCategory, List<DataCategory>> categoryMap = TestClassDataUtil.getAllAMSAdviceCategoriesMap();

        System.runAs(getCommunityUser()) {

            Test.startTest();
            AMSAdviceController controller = new AMSAdviceController();
            Test.stopTest();

            System.assertEquals(categoryMap.keySet().size(), controller.dataCategoryWrappers.size(), 'Data Categories not set correctly');

        }
    }

    @isTest static void testAddArticlesToCategories() {
        TestClassDataUtil.createAMSAdviceArticleAndGiveRandomCategory(50);

        System.runAs(getCommunityUser()) {

            Test.startTest();
            AMSAdviceController controller = new AMSAdviceController();
            Test.stopTest();

            for (AMSKnowledgeService.ArticleCategoryWrapper acw : controller.dataCategoryWrappers) {
                if (acw.adviceArticle != null && acw.name != null) {
                    System.assertEquals(acw.name, acw.adviceArticle.DataCategorySelections[0].DataCategoryName);
                }
                for (AMSKnowledgeService.ArticleCategoryWrapper child : acw.childDataCategoryWrappers) {
                    if (child.adviceArticle != null && child.name != null) {
                        System.assertEquals(child.name, child.adviceArticle.DataCategorySelections[0].DataCategoryName);
                    }
                }
            }
        }
    }

    @isTest static void testCheckIfCurrentArticle() {
        TestClassDataUtil.createAMSAdviceArticleAndGiveRandomCategory(1);

        System.runAs(getCommunityUser()) {
            Knowledge__kav adviceArticle = [
                                                SELECT Id,
                                                UrlName
                                                FROM Knowledge__kav
                                                WHERE PublishStatus = 'Online'
                                                        AND Language = 'en_US'
                                            ];

            PageReference pageRef = Page.AMSAdvice;
            pageRef.getParameters().put(AMSCommunityUtil.getArticleParameterName(), adviceArticle.UrlName);
            System.Test.setCurrentPage(pageRef);

            Test.startTest();
            AMSAdviceController controller = new AMSAdviceController();
            Test.stopTest();

            System.assertNotEquals(null, controller.currentWrappedAdviceArticle, 'no article selected');
            System.assertNotEquals(null, controller.currentWrappedAdviceArticle.adviceArticle, 'no article in selected wrapper');

            if (controller.currentWrappedAdviceArticle.adviceArticle.Id != null) {
                System.assertEquals(adviceArticle.Id, controller.currentWrappedAdviceArticle.adviceArticle.Id, 'Current Advice Article not set correctly');
            } else {
                // Data categories were probably not created
            }
        }
    }

    @isTest static void testCurrentArticleSet_WhenNotPassedInURL() {
        TestClassDataUtil.createAMSAdviceArticleAndGiveRandomCategory(1);

        System.runAs(getCommunityUser()) {
            Knowledge__kav adviceArticle = [
                                                SELECT Id,
                                                UrlName
                                                FROM Knowledge__kav
                                                WHERE PublishStatus = 'Online'
                                                        AND Language = 'en_US'
                                            ];
            PageReference pageRef = Page.AMSAdvice;
            System.Test.setCurrentPage(pageRef);

            Test.startTest();
            AMSAdviceController controller = new AMSAdviceController();
            Test.stopTest();

            System.assertNotEquals(null, controller.currentWrappedAdviceArticle, 'Current Advice Article not set correctly');
        }
    }

    @isTest static void testCheckSwitchtoFAQs() {
        TestClassDataUtil.createAMSAdviceArticleAndGiveRandomCategory(1);
        TestClassDataUtil.createAMSFAQArticles(15);

        System.runAs(getCommunityUser()) {
            Knowledge__kav adviceArticle = [
                                                SELECT Id,
                                                UrlName
                                                FROM Knowledge__kav
                                                WHERE PublishStatus = 'Online'
                                                    AND RecordTypeId = :GlobalUtility.amsAdviceRecordTypeId
                                                    AND Language = 'en_US'
                                            ];
            PageReference pageRef = Page.AMSAdvice;
            System.Test.setCurrentPage(pageRef);

            Test.startTest();
            AMSAdviceController controller = new AMSAdviceController();
            System.assertNotEquals(null, controller.currentWrappedAdviceArticle, 'Current Advice Article not set correctly');

            System.assertEquals(null, controller.topFAQs, 'Got faqs before switching to faqs');

            // Now switch the the FAQs
            controller.faqsSelected();

            System.assertNotEquals(null, controller.topFAQs, 'No faqs after switching to faqs');
            Test.stopTest();
        }
    }

    private static User getCommunityUser() {
        User theUser = [
                           SELECT Id
                           FROM User
                           WHERE Email = :USER_EMAIL
                       ];

        return theUser;
    }
}