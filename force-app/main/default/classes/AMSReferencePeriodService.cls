/**
 * Reference Period service
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately.
 *
 * @author: Amelia (Trineo)
 * @date: September 2017
 * @test: AMSReferencePeriodService_Test
 */
public without sharing class AMSReferencePeriodService {

    public static List<Reference_Period__c> getCurrentAndFutureReferencePeriods(Integer numberOfReferencePeriods) {
        return [SELECT  Id,
                        Name,
                        End_Date__c,
                        Reference_Period__c,
                        Start_Date__c,
                        Unique_Reference_Period_Name__c,
                        Status__c
                FROM Reference_Period__c
                WHERE End_Date__c > :Date.today()
                  AND Type__c = :GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU
                ORDER BY End_Date__c
                LIMIT :numberOfReferencePeriods];
    }

    public static List<Reference_Period__c> getFutureReferencePeriods(Integer numberOfReferencePeriods) {
        return [SELECT  Id,
                        Name,
                        End_Date__c,
                        Reference_Period__c,
                        Start_Date__c,
                        Unique_Reference_Period_Name__c,
                        Status__c
                FROM Reference_Period__c
                WHERE Start_Date__c > :Date.today()
                  AND Type__c = :GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU
                ORDER BY Start_Date__c
                LIMIT :numberOfReferencePeriods];
    }

    public static List<Reference_Period__c> getPastReferencePeriods() {
        return [SELECT
                    Id,
                    Name,
                    Start_Date__c,
                    End_Date__c,
                    Reference_Period__c,
                    Status__c
                FROM
                    Reference_Period__c
                WHERE
                    Start_Date__c <= :Date.today()
                    AND Type__c = :GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU
                ORDER BY End_Date__c DESC];
    }
}