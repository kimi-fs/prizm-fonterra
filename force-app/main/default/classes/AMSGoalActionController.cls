/**
 * AMSGoalActionController.cls
 * Description: Controller for AMSGoalAction.page
 * @author: Ricardo Hurtado
 * @date: December 2018
 */
public with sharing class AMSGoalActionController {

    public Action__c action { get; set; }
    public Id goalId { get; set; }
    public String goalName { get; set; }
    public String actionAssignedTo { get; set; }

    public AMSGoalActionController() {
        Id actionId = ApexPages.currentPage().getParameters().get(AMSCommunityUtil.getActionIdParameterName());

        this.action = GoalService.getActionDetails(actionId);
        this.goalId = this.action.Goal__c;
        this.goalName = this.action.Goal__r.Name;
        this.actionAssignedTo = this.action.Assigned_To__c;
    }

    public void startAction() {
        this.action = GoalService.setActionStatus(this.action, 'In Progress', null);
    }

    public void stopAction() {
        this.action = GoalService.setActionStatus(this.action, 'Not Started', null);
    }

    public void completeAction() {
        this.action = GoalService.setActionStatus(this.action, 'Completed', Date.today());
    }

    public void saveActionAssignedTo() {
        this.action = GoalService.setActionAssignedTo(this.action, this.actionAssignedTo);
    }

}