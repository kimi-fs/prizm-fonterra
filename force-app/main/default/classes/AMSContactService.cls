/**
 * AMSContactService
 *
 * Service for retrieving the contact us contacts for a user.
 *
 * We want all of the contacts that are associated with active farms for a user. This service may be
 * generalised later for other contact related operations.
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately.
 *
 * Author: Ed (Trineo)
 * Date: July 2017
 */
public without sharing class AMSContactService {
    public static final String AREA_MANAGER = 'Area Manager';
    public static final String QUALITY_SPECIALIST = 'Quality Specialist';
    public static final String INBOUND_SERVICE_SPECIALIST = 'Inbound Service Specialist';
    /**
     * package up the contact us users with their role for that farm.
     */
    public class ContactUsWrapper {
        public String farm {get; private set;}
        public String role {get; private set;}
        public User user {get; set;}

        public ContactUsWrapper(String farm, String role, User user) {
            this.farm = farm;
            this.role = role;
            this.user = user;
        }
    }

    static public Map<String, List<ContactUsWrapper>> getContactUsContacts(Id contactId) {
        Map<String, List<ContactUsWrapper>> contactUsUsers = new Map<String, List<ContactUsWrapper>>();

        // Get a list of relationships to farms - it is possible that we have more than one relationship to the
        // same farm (different roles).
        List<Individual_Relationship__c> activeRelationships = RelationshipAccessService.relationshipsForOwnedFarms(contactId);
        Set<Id> farmsProcessed = new Set<Id>();

        for (Individual_Relationship__c relationship : activeRelationships) {

            if (farmsProcessed.contains(relationship.Farm__c)) {
                // Have already handled this farm - move on
                continue;
            } else {
                farmsProcessed.add(relationship.Farm__c);
            }

            // Area Manager
            List<ContactUsWrapper> areaManagers = contactUsUsers.get(AREA_MANAGER);
            if (areaManagers == null) {
                areaManagers = new List<ContactUsWrapper>();
            }
            if (relationship.Farm__r.Rep_Area_Manager__c != null) {
                User areaManager = new User(Id = relationship.Farm__r.Rep_Area_Manager__c,
                                            LastName = relationship.Farm__r.Rep_Area_Manager__r.Name,
                                            Email = relationship.Farm__r.Rep_Area_Manager__r.Email,
                                            Phone = relationship.Farm__r.Rep_Area_Manager__r.Phone,
                                            MobilePhone = relationship.Farm__r.Rep_Area_Manager__r.MobilePhone
                                           );
                areaManagers.add(new ContactUsWrapper(relationship.Farm__r.Name, AREA_MANAGER, areaManager));
            }
            contactUsUsers.put(AREA_MANAGER, areaManagers);


            // Quality Specialist
            List<ContactUsWrapper> qualitySpecialists = contactUsUsers.get(QUALITY_SPECIALIST);
            if (qualitySpecialists == null) {
                qualitySpecialists = new List<ContactUsWrapper>();
            }

            if (relationship.Farm__r.Quality_Specialist__c != null) {
                User qualitySpecialist = new User(  Id = relationship.Farm__r.Quality_Specialist__c,
                                                    LastName = relationship.Farm__r.Quality_Specialist__r.Name,
                                                    Email = relationship.Farm__r.Quality_Specialist__r.Email,
                                                    Phone = relationship.Farm__r.Quality_Specialist__r.Phone,
                                                    MobilePhone = relationship.Farm__r.Quality_Specialist__r.MobilePhone
                                                 );
                qualitySpecialists.add(new ContactUsWrapper(relationship.Farm__r.Name, QUALITY_SPECIALIST, qualitySpecialist));
            }
            contactUsUsers.put(QUALITY_SPECIALIST, qualitySpecialists);

            // Inbound Service Specialist
            List<ContactUsWrapper> inboundServiceSpecialists = contactUsUsers.get(INBOUND_SERVICE_SPECIALIST);
            if (inboundServiceSpecialists == null) {
                inboundServiceSpecialists = new List<ContactUsWrapper>();
            }
            if (relationship.Farm__r.Rep_Inbound_Service_Specialist__c != null) {
                User inboundServiceSpecialist = new User(Id = relationship.Farm__r.Rep_Inbound_Service_Specialist__c,
                                                    LastName = relationship.Farm__r.Rep_Inbound_Service_Specialist__r.Name,
                                                    Email = relationship.Farm__r.Rep_Inbound_Service_Specialist__r.Email,
                                                    Phone = relationship.Farm__r.Rep_Inbound_Service_Specialist__r.Phone,
                                                    MobilePhone = relationship.Farm__r.Rep_Inbound_Service_Specialist__r.MobilePhone
                                                   );
                inboundServiceSpecialists.add(new ContactUsWrapper(relationship.Farm__r.Name, INBOUND_SERVICE_SPECIALIST, inboundServiceSpecialist));
            }
            contactUsUsers.put(INBOUND_SERVICE_SPECIALIST, inboundServiceSpecialists);
        }

        return contactUsUsers;
    }

    /**
    * Gets the contact Id to use to load the records on the page
    * If coming from the log in as on contact page there should be a cookie with the contact id
    * Only users with View_AMS_Community custom permission can load contact id this way
    * Otherwise uses the logged in users contact id
    */
    public static Id determineContactId() {
        Cookie individualCookie = ApexPages.currentPage().getCookies().get(AMSCommunityUtil.LOGIN_AS_COOKIE);
        if (individualCookie != null) {
            if (AMSUserService.checkIfUserCanViewCommunity()) {
                return individualCookie.getValue();
            } else {
                //  there is a parameter but no permissions - the template should redirect to an error page
                return null;
            }
        } else {
            User currentUser = AMSUserService.getUser();
            return currentUser != null ? currentUser.ContactId : null;
        }
    }

    public static Contact getContact() {
        Id contactId = determineContactId();
        return getContact(contactId);
    }

    /**
    * If the running users contact id is different to the id of the contact they are looking at the community for
    * This means they cant view somethings or update some things eg change password
    */
    public static Boolean isViewingDifferentContact() {
        User user = AMSUserService.getUser();
        Id runningContactId = determineContactId();

        if (user != null && user.ContactId != null && runningContactId != null) {
            return user.ContactId != runningContactId;
        } else {
            return true;
        }
    }

    public static Contact getContact(Id contactId) {
        List<Contact> c = [SELECT Id,
                                AMS_User_Preferences__c,
                                Salutation,
                                FirstName,
                                MiddleName,
                                LastName,
                                Birthdate,
                                Phone,
                                MobilePhone,
                                Email,
                                Name,
                                Preferred_Name__c,
                                Verified_No_Mobile__c,
                                MailingPostalCode,
                                Woolworths_Rewards_Card_Nbr__c,
                                Ritchies_IGA_Community_Benefits_Card_Nbr__c,
                                Type__c,
                                RecordTypeId,
                                RecordType.DeveloperName,
                                (SELECT Id,
                                        Address__c,
                                        State__c,
                                        Street_Number__c,
                                        Street_1__c,
                                        Street_2__c,
                                        Suburb__c,
                                        City__c,
                                        Postcode__c,
                                        Country_Code__c,
                                        PO_Box__c,
                                        RecordTypeId
                                FROM Addresses__r WHERE Active__c = true)
                            FROM Contact WHERE Id =: contactId LIMIT 1];
        return !c.isEmpty() ? c[0] : null;
    }

    public static Contact updateContact(Contact contact) {
        update contact;
        return contact;
    }

    /**
     * Update a contact, but only update the fields you specifically want to update
     *
     * Grabs the values from the contact and puts it on a new contact with only the id populated, then updates the contact
     */
    public static void updateContact(Contact contact, List<String> fieldsToUpdate) {
        Contact contactToUpdate = new Contact(Id = contact.Id);

        for (String fieldToUpdate : fieldsToUpdate) {
            contactToUpdate.put(fieldToUpdate, contact.get(fieldToUpdate));
        }

        update contactToUpdate;
    }

    public static Address__c upsertAddress(Address__c address) {
        upsert address;
        return address;
    }

    public static String getWelcomeName(Contact contact) {
        return String.isNotBlank(contact.Preferred_Name__c) ? contact.Preferred_Name__c : contact.FirstName;
    }

    public static void setPreferredNameOnUserContact(String userId, String preferredName) {
        Contact contact = [SELECT Id FROM Contact WHERE Id IN (SELECT ContactId FROM User WHERE Id = :userId)];
        contact.Preferred_Name__c = preferredName;

        updateContact(contact, new List<String> { 'Preferred_Name__c' });
    }

    /**
    * Checks if a number is valid for sending SMS
    */
    public static Boolean isValidMobile(String mobile) {
        Boolean valid = false;
        if (String.isNotBlank(mobile)) {
            // Starts with +61 and contains only numbers or spaces
            Pattern mobilePattern = Pattern.compile('^(\\+61)[\\d ]*$');
            Matcher match = mobilePattern.matcher(mobile);
            valid = match.matches();
        }
        return valid;
    }
}