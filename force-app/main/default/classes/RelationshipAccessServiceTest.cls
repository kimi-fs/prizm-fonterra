/**
 * RelationshipAccessServiceTest
 *
 * Test the different record types and scenarios for Individual Relationships and Entity Relationships.
 *
 *
 *
 * Author: Ed (Trineo)
 * Date: July 2017
 */
@isTest
private class RelationshipAccessServiceTest {
    @testSetup
    static void testSetup() {
        // Prereqs
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.createIntegrationUser();
    }

    /**
     * A simple Individual -> IR -> Farm, check that there is full access with a non restricted IR
     */
    @isTest
    static void simpleSetup() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual('test@example.com');

        List<Reference_Region__c> auReferenceRegions = TestClassDataUtil.createAuReferenceRegions();
        insert auReferenceRegions;
        List<Account> farms = TestClassDataUtil.createAuFarms(auReferenceRegions);

        insert farms;

        // Now connect this individual to each of the farms that we created via an IR.
        List<Individual_Relationship__c> individualRelationships = new List<Individual_Relationship__c>();

        for (Account farm : farms) {
            individualRelationships.add(new Individual_Relationship__c(
                Active__c = true,
                Individual__c = communityUser.ContactId,
                Role__c = 'Director',
                Farm__c = farm.Id,
                RecordTypeId = GlobalUtility.individualFarmRecordTypeId
            ));
        }
        insert individualRelationships;

        // Now check that we have access
        for (Account farm : farms) {
            System.assert(RelationshipAccessService.isAccessVisible(communityUser.ContactId, farm.Id));
        }

        for (Individual_Relationship__c relationship : individualRelationships) {
            relationship = [SELECT Id, Active__c, RecordTypeId FROM Individual_Relationship__c WHERE Id = :relationship.Id LIMIT 1];
            System.assert(RelationshipAccessService.isAccessVisible(relationship), 'Should have visibility on ' + relationship);
        }
    }

    // TODO change to use the ER when we change the service.
    @isTest
    static void thirdPartyMilkQuality() {
        // Start off with a farmer and some farms, and some parties, a successful
        // farmer with multiple companies with multiple farms\
        Contact theIndividual = TestClassDataUtil.createIndividualAU(false);
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual('test@example.com');

        List<Reference_Region__c> auReferenceRegions = TestClassDataUtil.createAuReferenceRegions();
        insert auReferenceRegions;
        List<Account> farms = TestClassDataUtil.createAuFarms(auReferenceRegions);

        insert farms;

        // Now create the parties for the farmers and those farms (5 of them).
        List<Account> parties = TestClassDataUtil.createMultipleAccountsPartyAU();

        Map<Account, List<Account>> partiesTofarms = new Map<Account, List<Account>>();

        Integer farmNo = farms.size();
        for (Account p : parties) {
            if (farmNo-- > 0) {
                partiesTofarms.put(p, new List<Account> {farms.get(farmNo)});
            }
        }

        // Create some ERs between the parties and the farms.
        List<Entity_Relationship__c> entityRelationships = TestClassDataUtil.createPartyFarmEntityRelationships(partiesTofarms, true);


        // Now connect this individual to each of the parties that we created via an IR.
        List<Individual_Relationship__c> individualRelationships = TestClassDataUtil.createIndividualPartyRelationships(communityUser.ContactId, parties, true);

        // The trigger should have now created some IRs between the farmer and the farms based
        // on the ERs.

        // Now create a third party.
        User thirdPartyUser = TestClassDataUtil.createAMSCommunityUserAndIndividual('thirdParty@example.com');

        Test.startTest();

        // Give them milk quality access to the first farm.
        Individual_Relationship__c tpRelationship = new Individual_Relationship__c(
                                                            Active__c = true,
                                                            Individual__c = thirdPartyUser.ContactId,
                                                            Role__c = 'Tester',
                                                            Farm__c = entityRelationships[0].Farm__c,
                                                            Access_Statements__c = true,
                                                            Access_Sponsoring__c = entityRelationships[0].Id,
                                                            Party__c = entityRelationships[0].Party__c,
                                                            Access_Begin__c = Date.today().addDays(-1),
                                                            Access_End__c = Date.today().addDays(10),
                                                            RecordTypeId = GlobalUtility.individualRestrictedFarmRecordTypeId);

        insert tpRelationship;

        // Now check that the third party has milk quality access to the first farm and doesn't have different ones.
        System.debug('Checking initial access');
        System.assertEquals(true, RelationshipAccessService.isAccessGranted(communityUser.ContactId, entityRelationships[0].Farm__c, RelationshipAccessService.Access.ACCESS_STATEMENTS), 'Has full access but not milk');
        System.assertEquals(true, RelationshipAccessService.isAccessGranted(thirdPartyUser.ContactId, entityRelationships[0].Farm__c, RelationshipAccessService.Access.ACCESS_STATEMENTS), 'Should have milk access');
        System.assertEquals(false, RelationshipAccessService.isAccessGranted(thirdPartyUser.ContactId, entityRelationships[0].Farm__c, RelationshipAccessService.Access.ACCESS_TARGETS), 'Should not have targets access');
        System.assertEquals(true, RelationshipAccessService.isAccessGranted(communityUser.ContactId, entityRelationships[0].Farm__c, RelationshipAccessService.Access.ACCESS_TARGETS), 'Should have targets access');

        //Test.startTest();
        // Now break the authorisation on the ER and the third party relationship should fail
        entityRelationships[0].Active__c = false;

        update entityRelationships;

        System.debug('Checking access after sponsor inactive');

        System.assertEquals(false, RelationshipAccessService.isAccessGranted(communityUser.ContactId, entityRelationships[0].Farm__c, RelationshipAccessService.Access.ACCESS_STATEMENTS), 'Should no longer have access');
        System.debug('checking access here');
        System.assertEquals(false, RelationshipAccessService.isAccessGranted(thirdPartyUser.ContactId, entityRelationships[0].Farm__c, RelationshipAccessService.Access.ACCESS_STATEMENTS), 'Should no longer have access');
        System.assertEquals(false, RelationshipAccessService.isAccessGranted(thirdPartyUser.ContactId, entityRelationships[0].Farm__c, RelationshipAccessService.Access.ACCESS_TARGETS), 'Should no longer have access');
        System.assertEquals(false, RelationshipAccessService.isAccessGranted(communityUser.ContactId, entityRelationships[0].Farm__c, RelationshipAccessService.Access.ACCESS_TARGETS), 'Should no longer have access');
        Test.stopTest();
    }

}