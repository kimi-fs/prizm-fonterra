public class Prizm_LendingContractUtil {
    
    public static List<fsServ__Lending_Contract__c> getContracts(Set<Id> pLendingContractIds){
        List<fsServ__Lending_Contract__c> contracts = new List<fsServ__Lending_Contract__c>();
        fsCore.DynamicQueryBuilder contractQuery = fsCore.DynamicQueryFactory.createQuery(Schema.SObjectType.fsServ__Lending_Contract__c.getName())
            .addFields()
            .addField('fsServ__Lending_Application_Number__r.Advance_Application__c')
            .addWhereConditionWithBind(1, 'Id', 'IN', 'pLendingContractIds');
        contracts = (List<fsServ__Lending_Contract__c>)Database.query(contractQuery.getQueryString());
        return contracts;
    }
    
    public static Map<Id,fsServ__Contract_Customer__c> getContractIdToPrimaryCustomerMap (Set<Id> pContractIds){
        Map<Id,fsServ__Contract_Customer__c> contractIdToPrimaryCustomerMap = new Map<Id,fsServ__Contract_Customer__c>();
        List<fsServ__Contract_Customer__c> contractCustomers = new List<fsServ__Contract_Customer__c>();
        fsCore.DynamicQueryBuilder contractCustomerQuery = fsCore.DynamicQueryFactory.createQuery(Schema.SObjectType.fsServ__Contract_Customer__c.getName())
            .addFields()
            .addWhereConditionWithBind(1, 'fsServ__Lending_Contract_Number__c', 'IN', 'pContractIds')
            .addWhereConditionWithValue(2,'fsServ__Is_Primary__c','=',true);
        contractCustomers = (List<fsServ__Contract_Customer__c>)Database.query(contractCustomerQuery.getQueryString());
        for(fsServ__Contract_Customer__c contractCustomer : contractCustomers){
            contractIdToPrimaryCustomerMap.put(contractCustomer.fsServ__Lending_Contract_Number__c,contractCustomer);
        }
        return contractIdToPrimaryCustomerMap;
    } 
}