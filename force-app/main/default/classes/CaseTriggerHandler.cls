/**
 * Logic class controlling behaviour of the Case trigger
 * @author Sarah Hay, Dan Fowlie
 * @date March 2016
 * @testclass Test_CaseTrigger.cls
 */
public with sharing class CaseTriggerHandler extends TriggerHandler {

    public override void afterInsert() {
        //Insert FeedItem
        Service_Centre_Settings__c scs = Service_Centre_Settings__c.getOrgDefaults();
        if (scs.Post_Description_to_Chatter__c) {
            List<Case> cases = [Select Id, Description from Case WHERE ID IN :Trigger.new];
            List<FeedItem> listfitem = new List<FeedItem>();
            for (Case c : cases) {

                if(c.Description != null) {
                    FeedItem fitem = new FeedItem();
                    fitem.Type = 'TextPost';
                    fitem.ParentId = c.Id;
                    fitem.Title = 'Description';
                    fitem.Body = 'Description: ' + c.Description;
                    listfitem.add(fitem);
                }
            }
            try {
                Database.insert(listfitem, false);
            } catch (Exception e) {
                Logger.log('CaseTrigger.afterInsert() failed','Inserting FeedItems Failed\n' + e.getMessage(), Logger.LogType.ERROR);
                Logger.saveLog();
            }
        }

        linkTasksToCases(Trigger.new);
    }


    public override void afterUpdate() {
        linkTasksToCases(Trigger.new);
    }

    public override void beforeUpdate() {
        checkAttachmentsOnCase(Trigger.new, (Map<Id, Case>) Trigger.oldMap);
        validateAdvanceApplications(Trigger.new, (Map<Id, Case>) Trigger.oldMap);
    }

    private void validateAdvanceApplications(List<Case> newCases, Map<Id, Case> oldCaseMap) {
        List<Advance_Application__c> advanceApplicationDetails = [SELECT
                                                                            Id,
                                                                            Case__c,
                                                                            All_pages_have_been_initialled__c,
                                                                            Advance_Application_has_been_verified__c,
                                                                            Proposed_new_RABO_Advance__c,
                                                                            Proposed_RABO_Restructured_Advance_Amt__c,
                                                                            RABO_Bank_Approval__c,
                                                                            RABO_Bank_Approval_File_Attached__c
                                                                        FROM
                                                                            Advance_Application__c
                                                                        WHERE
                                                                            Case__c IN :newCases];
        Map<Id, Advance_Application__c> advanceApplicationDetailsByCaseId = new Map<Id, Advance_Application__c>();

        for (Advance_Application__c advanceApplicationDetail : advanceApplicationDetails) {
            advanceApplicationDetailsByCaseId.put(advanceApplicationDetail.Case__c, advanceApplicationDetail);
        }

        for (Case newCase : newCases) {
            Case oldCase = oldCaseMap.get(newCase.Id);

            if (newCase.RecordTypeId == GlobalUtility.caseFormAdvanceApplicationRecordTypeId && advanceApplicationDetailsByCaseId.containsKey(newCase.Id) && newCase.Approval_Status__c != oldCase.Approval_Status__c) {
                Advance_Application__c detail = advanceApplicationDetailsByCaseId.get(newCase.Id);

                if (!detail.All_pages_have_been_initialled__c || !detail.Advance_Application_has_been_verified__c) {
                    newCase.addError('Please tick the checkboxes declaring that the application has been verified and all pages have been initialled');
                }

                if (oldCase.Approval_Status__c == 'Pending Approval' &&
                    newCase.Approval_Status__c == 'Approved' &&
                    detail.Proposed_new_RABO_Advance__c > 150000 &&
                    (detail.RABO_Bank_Approval__c != 'Approved' ||
                        !detail.RABO_Bank_Approval_File_Attached__c)) {

                    newCase.addError('Please ensure the RABO Bank approval checkbox has been ticked and the approval file is attached');
                }
            }
        }
    }

    private void checkAttachmentsOnCase(List<Case> newCases, Map<Id, Case> oldCaseMap) {
        List<Case> casesToValidate = new List<Case>();
        List<Id> caseIds = new List<Id>();
        
        for (Case newCase : newCases) {
            Case oldCase = oldCaseMap.get(newCase.Id);
        
            if (caseRequiresAttachmentForApproval(newCase) &&
                (newCase.Supporting_Files_Uploaded__c || newCase.Approval_Status__c == 'Pending Approval')) {
                casesToValidate.add(newCase);
                caseIds.add(newCase.Id); // because content document queries are stupid
            }
        }
        
        if (!casesToValidate.isEmpty()) { // no, really, they're stupid- bypass this block with the query otherwise it'll error out if you pass in an empty list
            Map<Id, List<ContentDocumentLink>> contentDocumentLinksByLinkedEntityId = new Map<Id, List<ContentDocumentLink>>();

            List<ContentDocumentLink> relatedContentDocuments = [SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :caseIds];
            List<ContentDocumentLink> shareFilesToCase = new List<ContentDocumentLink>();
            
            for (ContentDocumentLink contentDocument : relatedContentDocuments) {
                if (!contentDocumentLinksByLinkedEntityId.containsKey(contentDocument.LinkedEntityId)) {
                    contentDocumentLinksByLinkedEntityId.put(contentDocument.LinkedEntityId, new List<ContentDocumentLink>());
                }

                contentDocumentLinksByLinkedEntityId.get(contentDocument.LinkedEntityId).add(contentDocument);
            }
            
            for (Case c : casesToValidate) {
                if (!contentDocumentLinksByLinkedEntityId.containsKey(c.Id) ||
                    contentDocumentLinksByLinkedEntityId.get(c.Id).isEmpty()) {

                    c.addError('Please upload the required documents before attempting to submit for approval');
                }
            }
        }
    }

    private Boolean caseRequiresAttachmentForApproval(Case c) {
        return c.RecordTypeId == GlobalUtility.caseFormAdvanceApplicationRecordTypeId ||
            c.RecordTypeId == GlobalUtility.caseFormBankingInformationRecordTypeId ||
            c.RecordTypeId == GlobalUtility.caseFormHerdTestingIncentiveRecordTypeId ||
            c.RecordTypeId == GlobalUtility.caseFormABNUpdateRecordTypeId ||
            c.RecordTypeId == GlobalUtility.caseFormCeaseOrTransferSupplyRecordTypeId ||
            c.RecordTypeId == GlobalUtility.caseFormMilkCoolingPurchaseIncentiveRecordTypeId ||
            c.RecordTypeId == GlobalUtility.caseFormSharefarmingArrangementRecordTypeId ||
            c.RecordTypeId == GlobalUtility.caseFormStructuredDeductionRecordTypeId ||
            c.RecordTypeId == GlobalUtility.caseFormSupplierInformationRecordTypeId ||
            c.RecordTypeId == GlobalUtility.caseFormNewSupplierRecordTypeId;

    }

    private void linkTasksToCases(List<Case> cases){
        try {

            // Is the user part of the callcenter
            User u = [Select Id, CallCenterId from User Where Id = :UserInfo.getUserId()];
            // Are we dealing with just one case?
            if (cases.size() == 1 && u.CallCenterId != null){

                // Get the case
                Case c = [Select Id, ContactId from Case Where Id = :cases[0].Id];

                // Have any tasks been created by this user in the past 5 mins
                DateTime dt = System.now().addMinutes(-5);
                List<Task> lt = [Select Id, WhoId, WhatId from Task Where CreatedDate > :dt
                                                                  And CreatedById = :UserInfo.getUserId()
                                                                  And (WhatId = NULL OR WhatId = :c.Id)
                                                                  Order By CreatedDate DESC
                                                                  Limit 1];


                for (Task t : lt){

                    if (t.WhoId == null) t.WhoId = c.ContactId;
                    if (t.WhatId == null) t.WhatId = c.Id;
                }

                update lt;

            }
        } catch (exception e){
            Logger.log('Case Trigger - LinkTasksToCases','Raw Data: ' + e.getMessage() , Logger.LogType.ERROR);
            Logger.saveLog();
        }
    }

}