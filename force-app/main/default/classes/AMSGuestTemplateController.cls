/**
 * Description: Controller for the guest template
 * @author: Amelia (Trineo)
 * @date: September 2017
 */
public with sharing class AMSGuestTemplateController {
    public Boolean isLoggedIn {
        get {
            return AMSUserService.checkUserLoggedIn();
        }
        private set;
    }

    public PageReference redirectIfLoggedIn(){
        //Do not redirect on these pages as although they are logged in at this stage they need the guest template so they cannot navigate away.
        PageReference sessionTimeoutErrorPage = Page.AMSStaticPage;
        sessionTimeoutErrorPage.getParameters().put('page', 'AMSErrorSessionTimeout');

        Set<String> urlsToNotRedirect = new Set<String>{
            getPageName(Page.AMSNewsDetail),
            getPageName(Page.AMSEmailVerification),
            getPageName(Page.AMSPasswordReset),
            getPageName(Page.AMSTerms),
            getPageName(Page.AMSFarmDetails),
            getPageName(sessionTimeoutErrorPage)
        };

        if (!urlsToNotRedirect.contains(getPageName(ApexPages.currentPage()))) {
            if(this.isLoggedIn){
                return Page.AMSDashboard;
            } else {
                return redirectIfLastVisitedCookiePageExists();
            }
        }
        return null;
    }

    /**
    * Because the web app is installed from the AMSLanding page whenever the app opens it takes the user to this page
    * This page is a guest user page therefore it doesnt redirect to the login and then the community using extended session
    * However the requirements are that the user is automatically logged in and redirected to their last visited page in the web app
    * So we redirect to the login page (which will then log the user in and redirect them) if there is valid extended session cookies - which indicate the user has been logged into the web app before
    */
    public PageReference redirectIfLastVisitedCookiePageExists(){
        ExtendedSessionService extendedSession = new ExtendedSessionService(AMSLoginController.COMMUNITY_NAME);
        if(extendedSession.isValid()){
            String currentPageUrl = ApexPages.currentPage().getUrl();
            if (!currentPageUrl.containsIgnoreCase(AMSCommunityUtil.pageReferenceToCommunityFriendlyString(Page.AMSLogin))) {
                return Page.AMSLogin;
            }
        }
        return null;
    }

    /**
     * Get the name of the VF page. Just the name. Not the URL. The URL could potentially have params in it.
     *
     */
     private String getPageName(ApexPages.PageReference pageReference) {
        String pageReferenceURL = pageReference.getURL().toLowerCase();
        return pageReferenceURL.substringBefore('?').remove('/apex/').remove('/AMS/');
     }

}