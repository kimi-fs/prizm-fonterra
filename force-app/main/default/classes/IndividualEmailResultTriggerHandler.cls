public class IndividualEmailResultTriggerHandler {
    public static void afterInsert(List<et4ae5__IndividualEmailResult__c> individualEmailResults) {
        cloneToRelatedContacts(individualEmailResults);
    }

    private static void cloneToRelatedContacts(List<et4ae5__IndividualEmailResult__c> individualEmailResults) {
        Set<String> emailAddresses = new Set<String>();
        Set<Id> emailResultIdSet = new Set<Id>();

        for (et4ae5__IndividualEmailResult__c individualEmailResult : individualEmailResults) {
            if (!String.isBlank(String.valueOf(individualEmailResult.et4ae5__MergeId__c))) {
                emailAddresses.add(individualEmailResult.et4ae5__Email__c);
                emailResultIdSet.add(individualEmailResult.Id);
            }
        }

        SYSTEM.debug('Email Addresses: ' + JSON.serialize(emailAddresses));
        SYSTEM.debug('Email Result Records: ' + JSON.serialize(emailResultIdSet));

        if (emailResultIdSet.size() > 0 && emailAddresses.size() > 0) {
            createAllRecords(emailAddresses, emailResultIdSet);
        }
    }

    @Future(callout = true)
    private static void createAllRecords(final Set<String> emailAddresses, final Set<Id> emailResultIdSet) { //Create records of IndividualEmailResults for ALL Individual's with the specified Email Address.
        //Select Contacts which have an Email address relevant the IndividualEmailResults
        Map<Id, Contact> mapContactById = new Map<Id, Contact>([SELECT Id, Email FROM Contact WHERE Email IN: emailAddresses]);

        Map<String, List<Contact>> mapContactListByEmailAddress = new Map<String, List<Contact>>();

        Map<String, List<et4ae5__IndividualEmailResult__c>> mapEmailResultByEmailAddress = new Map<String, List<et4ae5__IndividualEmailResult__c>>();
        Map<Id, List<et4ae5__IndividualEmailResult__c>> mapEmailResultsToCreateByEmailAddress = new Map<Id, List<et4ae5__IndividualEmailResult__c>>();

        //Map all the Contacts by their Email Address
        for (Contact c : mapContactById.values()) {
            if (!mapContactListByEmailAddress.containsKey(c.Email)) {
                mapContactListByEmailAddress.put(c.Email, new List<Contact>());
            }

            mapContactListByEmailAddress.get(c.Email).add(c);
        }

        SYSTEM.debug(JSON.serialize(mapContactListByEmailAddress));

        //Map IndividualEmailResult records by the Email sent
        for (et4ae5__IndividualEmailResult__c ier : [
            SELECT
                Id,
                Name,
                et4ae5__Contact__c,
                et4ae5__DateBounced__c,
                et4ae5__DateOpened__c,
                et4ae5__DateSent__c,
                et4ae5__DateUnsubscribed__c,
                et4ae5__FromAddress__c,
                et4ae5__FromName__c,
                et4ae5__HardBounce__c,
                et4ae5__Lead__c,
                et4ae5__MergeId__c,
                et4ae5__NumberOfTotalClicks__c,
                et4ae5__NumberOfUniqueClicks__c,
                et4ae5__Opened__c,
                et4ae5__SendDefinition__c,
                et4ae5__SoftBounce__c,
                et4ae5__SubjectLine__c,
                et4ae5__Tracking_As_Of__c,
                et4ae5__TriggeredSendDefinition__c,
                et4ae5__Contact_ID__c,
                et4ae5__Email__c
            FROM
                et4ae5__IndividualEmailResult__c
            WHERE Id IN: emailResultIdSet]) {

            if (!mapEmailResultByEmailAddress.containsKey(mapContactById.get(ier.et4ae5__Contact__c).Email)) {
                mapEmailResultByEmailAddress.put(mapContactById.get(ier.et4ae5__Contact__c).Email, new List<et4ae5__IndividualEmailResult__c>());
            }

            mapEmailResultByEmailAddress.get(mapContactById.get(ier.et4ae5__Contact__c).Email).add(ier);
        }

        List<et4ae5__IndividualEmailResult__c> individualEmailResultsToCreate = new List<et4ae5__IndividualEmailResult__c>();

        for (String email : mapContactListByEmailAddress.keySet()) {
            List<Contact> relatedContacts = mapContactListByEmailAddress.get(email);
            List<et4ae5__IndividualEmailResult__c> relatedIndividualEmailResults = mapEmailResultByEmailAddress.get(email);

            for (Contact c : relatedContacts) {
                for (et4ae5__IndividualEmailResult__c individualEmailResult : relatedIndividualEmailResults) {
                    if (individualEmailResult.et4ae5__Contact__c != c.Id) {
                        et4ae5__IndividualEmailResult__c individualEmailResultToCreate = cloneIndividualEmailResult(individualEmailResult);

                        SYSTEM.debug('Email: ' + email + ', attached to: ' + C.Id);

                        individualEmailResultToCreate.et4ae5__Contact__c = c.Id;
                        individualEmailResultsToCreate.add(cloneIndividualEmailResult(individualEmailResultToCreate));
                    }
                }
            }
        }

        insert individualEmailResultsToCreate;
        SYSTEM.debug('INSERTING ' + individualEmailResultsToCreate.size() + ' records\n' + JSON.serialize(individualEmailResultsToCreate));
    }

    private static et4ae5__IndividualEmailResult__c cloneIndividualEmailResult(et4ae5__IndividualEmailResult__c individualEmailResult) {
        et4ae5__IndividualEmailResult__c individualEmailResultToCreate = new et4ae5__IndividualEmailResult__c();

        individualEmailResultToCreate.et4ae5__DateBounced__c = individualEmailResult.et4ae5__DateBounced__c;
        individualEmailResultToCreate.et4ae5__DateOpened__c = individualEmailResult.et4ae5__DateOpened__c;
        individualEmailResultToCreate.et4ae5__DateSent__c = individualEmailResult.et4ae5__DateSent__c;
        individualEmailResultToCreate.et4ae5__DateUnsubscribed__c = individualEmailResult.et4ae5__DateUnsubscribed__c;
        individualEmailResultToCreate.et4ae5__FromName__c = individualEmailResult.et4ae5__FromName__c;
        individualEmailResultToCreate.et4ae5__HardBounce__c = individualEmailResult.et4ae5__HardBounce__c;
        individualEmailResultToCreate.et4ae5__Lead__c = individualEmailResult.et4ae5__Lead__c;
        individualEmailResultToCreate.et4ae5__NumberOfTotalClicks__c = individualEmailResult.et4ae5__NumberOfTotalClicks__c;
        individualEmailResultToCreate.et4ae5__NumberOfUniqueClicks__c = individualEmailResult.et4ae5__NumberOfUniqueClicks__c;
        individualEmailResultToCreate.et4ae5__Opened__c = individualEmailResult.et4ae5__Opened__c;
        individualEmailResultToCreate.et4ae5__SendDefinition__c = individualEmailResult.et4ae5__SendDefinition__c;
        individualEmailResultToCreate.et4ae5__SoftBounce__c = individualEmailResult.et4ae5__SoftBounce__c;
        individualEmailResultToCreate.et4ae5__SubjectLine__c = individualEmailResult.et4ae5__SubjectLine__c;
        individualEmailResultToCreate.et4ae5__Tracking_As_Of__c = individualEmailResult.et4ae5__Tracking_As_Of__c;
        individualEmailResultToCreate.et4ae5__TriggeredSendDefinition__c = individualEmailResult.et4ae5__TriggeredSendDefinition__c;
        individualEmailResultToCreate.et4ae5__Contact__c = individualEmailResult.et4ae5__Contact__c;
        individualEmailResultToCreate.et4ae5__FromAddress__c = individualEmailResult.et4ae5__FromAddress__c;
        individualEmailResultToCreate.Name = individualEmailResult.Name;

        return individualEmailResultToCreate;
    }
}