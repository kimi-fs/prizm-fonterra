/*------------------------------------------------------------
Author:			Salman Zafar
Company:		Davanti Consulting
Description:	Test class for DeleteCollectionsBatchScheduler and DeleteCollectionsBatch class
History
08/04/2015		Salman Zafar	Created
------------------------------------------------------------*/

@isTest
private class AgreementTriggerTest {
	private static Account farmAccount = TestClassDataUtil.createSingleAccountFarm();
	static testMethod void myUnitTest() {

        Account partyAccount1 = TestClassDataUtil.createEntity(GlobalUtility.auAccountPartyRecordTypeName, 'Test party');
        insert partyAccount1;

        Account partyAccount2 = TestClassDataUtil.createEntity(GlobalUtility.auAccountPartyRecordTypeName, 'Test party 2');
        insert partyAccount2;

		Agreement__c a = new Agreement__c (Farm__c = farmAccount.Id,
											Party__c = partyAccount1.Id);
		Test.startTest();
		insert a;

		a.Party__c = partyAccount2.Id;

		update a;
		Test.stopTest();
	}
}