/**
 * Description: Cleans up temporary pdf files saved as documents for AMS statements
 *
 * @author: John Au (Trineo)
 * @date: September 2018
 * @test: AMSScheduleBatchStatementCleanupTest
 */
public class AMSScheduleBatchStatementCleanup implements Database.Batchable<Document>, Schedulable {

    private static DateTime getDateTimeCriteria() {
        DateTime queryDateTime = DateTime.now();

        if (!Test.isRunningTest()) {
            queryDateTime = queryDateTime.addDays(-1);
        }

        return queryDateTime;
    }

    public static void schedule() {
        String expression = ScheduleService.getCRONExpression('AMSScheduleBatchStatementCleanup');
        String jobName = ScheduleService.getCRONJobName('AMSScheduleBatchStatementCleanup');
        if (String.isNotBlank(expression)) {
            AMSScheduleBatchStatementCleanup job = new AMSScheduleBatchStatementCleanup();
            System.schedule(jobName, expression, job);
        }
    }

    public void execute(SchedulableContext schedulableContext) {
        Database.executeBatch(new AMSScheduleBatchStatementCleanup(), 100);
    }

    public List<Document> start(Database.BatchableContext batchableContext) {
        return [SELECT
                    Id
                FROM
                    Document
                WHERE
                    Keywords = :AMSStatementService.PDF_DOCUMENT_KEYWORDS
                    AND CreatedDate <= :getDateTimeCriteria()];
    }

    public void execute(Database.BatchableContext batchableContext, List<Document> documents) {
        delete (documents);
    }

    public void finish(Database.BatchableContext results) {

    }
}