/**
 * Description: Class for managing and creating AMS community users from the contact record
 * @author: Amelia (Trineo)
 * @date: August 2017
 */
public with sharing class AMSUserCreationController {
    public User communityUser { get; set; }
    public Boolean userExists { get; set; }
    public Contact individual { get; set; }

    private static String AMS_COMMUNITY_PROFILE_NAME = 'AMS Community User';

    public static String NO_EMAIL_MESSAGE = 'Individual must have an email address to become a community user.';
    public static String USER_EXISTS_MESSAGE = 'There is already an AMS Community user with that email address.';
    public static String EMAIL_SENT_MESSAGE = ' email has been sent.';

    public AMSUserCreationController(ApexPages.StandardController stdController) {
        if (!Test.isRunningTest()) {
            stdController.addFields(new List<String> {'Email', 'FirstName', 'LastName', 'Phone', 'FarmSourceONE_Setup_Complete__c'});
        }

        this.individual = (Contact) stdController.getRecord();
        findUser();
    }

    public void createCommunityUser() {
        if (String.isBlank(individual.Email)) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, NO_EMAIL_MESSAGE));
        } else {
            List<User> usersWithEmail = [SELECT Id, Email FROM User WHERE Email = : individual.Email AND Profile.Name = : AMS_COMMUNITY_PROFILE_NAME];
            if (!usersWithEmail.isEmpty()) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, USER_EXISTS_MESSAGE));

            } else {
                try {
                    this.communityUser = AMSUserService.createUserFromContact(individual);
                    AMSChannelPreferenceService.subscribeUsersToNotifications(individual.Id);
                    userExists = true;
                    findUser();

                } catch (Exception e) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));

                }
            }
        }
    }

    public void resetUserPassword() {
        System.resetPassword(this.communityUser.Id, true);
        String emailType = individual.FarmSourceONE_Setup_Complete__c ? 'Password Reset' : 'Welcome';
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, emailType + EMAIL_SENT_MESSAGE));

    }

    public void deactivateUser() {
        this.communityUser.IsActive = false;
        update this.communityUser;
    }
    public void reActivateUser() {
        this.communityUser.IsActive = true;
        update this.communityUser;
    }

    public void findUser() {
        this.communityUser = AMSUserService.getUserFromContactId(individual.Id);

        if (this.communityUser != null) {
            userExists = true;
        } else {
            this.communityUser = new User();
            userExists = false;
        }
    }

    /**
    * Creates a cookie with the individual id so the community can load with the individuals records

    */
    public void createAMSUserCookie(){
        ApexPages.currentPage().setCookies(
            new Cookie[] {
                new Cookie(AMSCommunityUtil.LOGIN_AS_COOKIE, this.individual.Id, null, null, true)
            }
        );
    }
}