/**
 * AMSNewsController.cls
 * Description: Displays the details of an AMS News knowledge article on a page
 * @author: Amelia (Trineo)
 * @date: June 2017
 */
 public with sharing class AMSNewsDetailController extends AMSKnowledgeList {

    //used for switching between the guest and community template
    public Boolean isLoggedIn {
        get {
            return AMSUserService.checkUserLoggedIn();
        }
    }

    public String newsArticleFirstPublishedDate {get;set;}
    public Knowledge__kav newsArticle { get; set; }

    public static Map<String, String> messages {get;set;}
    static {
        messages = ExceptionService.getPageMessages('AMSNewsDetail');
    }

    public Boolean newsArticlePresent { get; set; }
    public String previousPageNumber { get; set; }

    public AMSNewsDetailController() {
        this.newsArticle = new Knowledge__kav();
        this.newsArticlePresent = false;

        PageReference currentPage = ApexPages.currentPage();
        this.previousPageNumber = currentPage.getParameters().get(getPageNumberParameterName());
        this.currentArticleCategoryName = currentPage.getParameters().get(getArticleCategoryParameterName());

        this.dataCategoryWrappers = getArticleCategories(NEWS_DATA_CATEGORY);

        String articleUrlName = currentPage.getParameters().get(getArticleParameterName());
        //call set article if article Id exists from url parameter
        if (articleUrlName != null) {
            setNewsArticle(articleUrlName);
            newsArticleFirstPublishedDate = newsArticle.FirstPublishedDate.format('dd MMMM yyyy hh:mm a');
        }
    }

    public static String getArticleParameterName() {
        return AMSCommunityUtil.getArticleParameterName();
    }

    public static String getPageNumberParameterName() {
        return AMSCommunityUtil.getPageNumberParameterName();
    }

    public static String getArticleCategoryParameterName() {
        return AMSCommunityUtil.getArticleCategoryParameterName();
    }

    //sets the current news article based on url parameter and sets 'newsArticlePresent' parameter
    public void setNewsArticle(String articleUrlName) {
        newsArticlePresent = false;
        //query for knowledge article matching article urlname and set present
        for (Knowledge__kav newsKnowledgeArticle : [
            SELECT Id,
                UrlName,
                Title,
                Summary,
                Content__c,
                Author__c,
                Headline_Image__c,
                Thumbnail_Image__c,
                FirstPublishedDate
            FROM Knowledge__kav
            WHERE PublishStatus = 'Online'
            AND Language = 'en_US'
            AND UrlName = :articleUrlName
            LIMIT 1
        ]) {
            newsArticlePresent = true;
            newsArticle = newsKnowledgeArticle;
        }
    }
}