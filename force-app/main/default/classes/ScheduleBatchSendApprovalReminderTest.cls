/**
* @author  John Au
* @date  02/03/2021
**/
@IsTest
private class ScheduleBatchSendApprovalReminderTest {

    @IsTest
    private static void testScheduledTotals() {
        String jobName = '24-hour Approval Reminders';

        // Well SF won't let you create a Scheduler_CRON__mdt so we cheat and do it via JSON
        List<Map<String, Object>> mockedCronsAbstract = new List<Map<String, Object>> {
                new Map<String, Object> {
                    'Type' => Schema.Scheduler_CRON__mdt.SObjectType.getDescribe().getName(),
                    'Name' => 'ScheduleBatchSendApprovalReminder',
                    'DeveloperName' => 'ScheduleBatchSendApprovalReminder',
                    'CRON_Expression__c' => '0 0 * * * ?',
                    'Job_Name__c' => jobName,
                    'Run_As_User__c' => ScheduleService.BATCH_PROCESS_AU
                }
        };
        List<Scheduler_CRON__mdt> mockedCrons = (List<Scheduler_CRON__mdt>)JSON.deserialize(JSON.serialize(mockedCronsAbstract), List<Scheduler_CRON__mdt>.class);
        ScheduleService.mockSchedulerCrons(mockedCrons);

        Test.startTest();
        ScheduleBatchSendApprovalReminder.abort();
        ScheduleBatchSendApprovalReminder.schedule();
        Test.stopTest();

        List<CronTrigger> existingJob = [SELECT Id FROM CronTrigger WHERE CronjobDetail.Name = :jobName];
        System.assertEquals(1, existingJob.size());

    }


    @IsTest
    private static void testAbortSchedule() {
        String jobName = '24-hour Approval Reminders';

        // Well SF won't let you create a Scheduler_CRON__mdt so we cheat and do it via JSON
        List<Map<String, Object>> mockedCronsAbstract = new List<Map<String, Object>> {
                new Map<String, Object> {
                    'Type' => Schema.Scheduler_CRON__mdt.SObjectType.getDescribe().getName(),
                    'Name' => 'ScheduleBatchSendApprovalReminder',
                    'DeveloperName' => 'ScheduleBatchSendApprovalReminder',
                    'CRON_Expression__c' => '0 0 * * * ?',
                    'Job_Name__c' => jobName,
                    'Run_As_User__c' => ScheduleService.BATCH_PROCESS_AU
                }
        };
        List<Scheduler_CRON__mdt> mockedCrons = (List<Scheduler_CRON__mdt>)JSON.deserialize(JSON.serialize(mockedCronsAbstract), List<Scheduler_CRON__mdt>.class);
        ScheduleService.mockSchedulerCrons(mockedCrons);

        //Test aborting the schedule
        ScheduleBatchSendApprovalReminder.abort();
        ScheduleBatchSendApprovalReminder.schedule();

        List<CronTrigger> existingJob = [SELECT Id FROM CronTrigger WHERE CronjobDetail.Name = :jobName];
        System.assertEquals(1, existingJob.size());

        ScheduleBatchSendApprovalReminder.abort();
        List<CronTrigger> existingJob2 = [SELECT Id FROM CronTrigger WHERE CronjobDetail.Name = :jobName];
        System.assertEquals(0, existingJob2.size());
    }
}