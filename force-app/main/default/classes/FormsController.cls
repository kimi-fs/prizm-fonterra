/*
 *
 */
public with sharing class FormsController {

    private final static Integer MAX_SEARCH_RESULTS = 5;

    public FormsController() { }

    @AuraEnabled
    public static List<LookupSearchResult> searchOpportunities(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [
            FIND :searchTerm
            IN ALL FIELDS
            RETURNING
                Opportunity(Id, Name, Account.Name, Account.Trading_Name__c, Trading_Name__c WHERE Id NOT IN :selectedIds)
            LIMIT :MAX_SEARCH_RESULTS
        ];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Accounts & convert them into LookupSearchResult
        String opportunityIcon = 'standard:opportunity';
        Opportunity[] opportunities = (List<Opportunity>) searchResults[0];
        for (Opportunity opportunity : opportunities) {
            String subtitle = 'Opportunity' + (opportunity.Account == null ? '' : ' • ' + opportunity.Account.Trading_Name__c);
            results.add(new LookupSearchResult(opportunity.Id, 'Opportunity', opportunityIcon, opportunity.Name, subtitle));
        }

        // Optionally sort all results on title
        results.sort();

        return results;
    }

    @AuraEnabled
    public static List<Opportunity> getCurrentOpportunity(Id oppId){
        return [
            SELECT Id, Name
            FROM Opportunity
            WHERE Id = :oppId
            LIMIT 1
        ];
    }

    @AuraEnabled
    public static List<LookupSearchResult> searchUsers(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [
            FIND :searchTerm
            IN ALL FIELDS
            RETURNING
                User(Id, Name, Email WHERE Id NOT IN :selectedIds)
            LIMIT :MAX_SEARCH_RESULTS
        ];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Accounts & convert them into LookupSearchResult
        String userIcon = 'standard:user';
        User[] users = (List<User>) searchResults[0];
        for (User user : users) {
            String subtitle = 'User' + (user.Email == null ? '' : ' • ' + user.Email);
            results.add(new LookupSearchResult(user.Id, 'User', userIcon, user.Name, subtitle));
        }

        // Optionally sort all results on title
        results.sort();

        return results;
    }

    @AuraEnabled
    public static List<LookupSearchResult> searchIndividuals(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [
            FIND :searchTerm
            IN ALL FIELDS
            RETURNING
                Contact(Id, Name, Email WHERE Id NOT IN :selectedIds AND RecordTypeId = :GlobalUtility.individualAURecordTypeId)
            LIMIT :MAX_SEARCH_RESULTS
        ];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Accounts & convert them into LookupSearchResult
        String contactIcon = 'standard:contact';
        Contact[] contacts = (List<Contact>) searchResults[0];
        for (Contact contact : contacts) {
            String subtitle = 'Individual' + (contact.Email == null ? '' : ' • ' + contact.Email);
            results.add(new LookupSearchResult(contact.Id, 'Contact', contactIcon, contact.Name, subtitle));
        }

        // Optionally sort all results on title
        results.sort();

        return results;
    }

    @AuraEnabled
    public static List<LookupSearchResult> searchFarms(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [
            FIND :searchTerm
            IN ALL FIELDS
            RETURNING
                Account(Id, Name, Farm_Name__c WHERE Id NOT IN :selectedIds AND RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId)
            LIMIT :MAX_SEARCH_RESULTS
        ];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Accounts & convert them into LookupSearchResult
        String accountIcon = 'standard:account';
        Account[] accounts = (List<Account>) searchResults[0];
        for (Account account : accounts) {
            String subtitle = 'Farm' + (account.Farm_Name__c == null ? '' : ' • ' + account.Farm_Name__c);
            results.add(new LookupSearchResult(account.Id, 'Account', accountIcon, account.Name, subtitle));
        }

        // Optionally sort all results on title
        results.sort();

        return results;
    }

    @AuraEnabled
    public static List<Account> getCurrentFarm(Id farmId){
        return [
            SELECT Id, Name
            FROM Account
            WHERE Id = :farmId
            LIMIT 1
        ];
    }

    @AuraEnabled
    public static List<LookupSearchResult> searchParties(String searchTerm, List<String> selectedIds) {
        // Prepare query paramters
        searchTerm += '*';

        // Execute search query
        List<List<SObject>> searchResults = [
            FIND :searchTerm
            IN ALL FIELDS
            RETURNING
                Account(Id, Name, Party_ID__c WHERE Id NOT IN :selectedIds AND RecordTypeId = :GlobalUtility.auAccountPartyRecordTypeId)
            LIMIT :MAX_SEARCH_RESULTS
        ];

        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();

        // Extract Accounts & convert them into LookupSearchResult
        String accountIcon = 'standard:account';
        Account[] accounts = (List<Account>) searchResults[0];
        for (Account account : accounts) {
            String subtitle = 'Party' + (account.Party_ID__c == null ? '' : ' • ' + account.Party_ID__c);
            results.add(new LookupSearchResult(account.Id, 'Account', accountIcon, account.Name, subtitle));
        }

        // Optionally sort all results on title
        results.sort();

        return results;
    }

    // Added by Zain
    @AuraEnabled
    public static List<PickListResult> getPartiesByFarm(Id farmId) {

        // Prepare results
        List<PickListResult> results = new List<PickListResult>();
        List<Entity_Relationship__c> entities = [SELECT Party__c, Party__r.Name, Active__c FROM Entity_Relationship__c WHERE Active__c=true AND Farm__c=:farmId];
        
        for (Entity_Relationship__c entity : entities) {
            results.add(new PickListResult(entity.Party__r.Name, entity.Party__c ));
        }
        return results;
    }

    @AuraEnabled
    public static Account getFarm(Id farmId) {
        return [SELECT Id, Total_Volume_12_Months__c FROM Account WHERE Id = :farmId];
    }

    @AuraEnabled
    public static Payment_Split__c getPaymentSplitForFarmAndParty(Id farmId, Id partyId) {
        Payment_Split__c paymentSplit = null;
        List<Payment_Split__c> paymentSplits = [SELECT Id, Payee_Type__c, Percentage__c, Agreement__r.Farm__r.Total_Volume_12_Months__c, Agreement__r.Farm__r.Total_Solids_12_Months__c FROM Payment_Split__c WHERE Party__c = :partyId AND Agreement__r.Active__c = true AND Agreement__r.Farm__c = :farmId];

        if (paymentSplits.size() == 1) {
            paymentSplit = paymentSplits[0];
        }

        return paymentSplit;
    }

    @AuraEnabled
    public static List<Entity_Relationship__c> getActiveERELs(Id farmId) {
        return [SELECT Id, Party__c, Party__r.Name, Party__r.Party_ID__c, Farm__r.Name, Role__c, Active__c FROM Entity_Relationship__c WHERE Farm__c = :farmId AND Active__c = true];
    }

    @AuraEnabled
    public static Advance_Setting__mdt getAdvanceSettings() {
        return [SELECT Id, Effective_Loan_Date__c, Fonterra_Exposure_Level__c, Lending_Level__c FROM Advance_Setting__mdt WHERE DeveloperName = 'Lending_Level'];
    }

    @AuraEnabled
    public static Advance_Application__c getAdvanceApplicationDetail(Id advanceApplicationId) {
        return [
            SELECT
                Id,
                Case__c,
                Case__r.Party__r.Id,
                Case__r.Party__r.Party_ID__c,
                Case__r.Account.Id,
                Case__r.Account.Name,
                Type__c,
                RABO_Advance_Type__c,
                Reason_for_Interest_Free__c,
                Loan_Number_being_Restructured__c,
                Current_Account__c,
                Current_RABO_Advances__c,
                Percentage_of_Milk__c,
                Last_12_Months_Litres__c,
                Last_12_Months_Solids_KGs__c,
                Additional_Litres__c,
                Additional_KGs__c,
                Proposed_new_RABO_Advance__c,
                Proposed_RABO_Restructured_Advance_Amt__c,
                Over_Limit_Reason__c,
                Over_Limit_Reason_Description__c,
                AM_Risk_Evaluation__c,
                Cost_of_Production_kg_MS__c,
                Number_of_Cows__c,
                Equity_Position_perc_Equity__c,
                Equity_Position_Equity__c,
                Indicative_RABO_Lending_Level__c,
                Indicative_Fonterra_Exposure_Level__c,
                Total_Loan_Exposure__c,
                Total_Loan_Rate_Cents_Per_Litre__c,
                Dollars_Per_KG_Loan_Rate__c,
                Advance_Application_has_been_verified__c,
                Other_Attachments_have_been_verified__c,
                All_pages_have_been_initialled__c,
                Attachment_pages_have_been_initialled__c,
                Finance_Required__c,
                Finance_Purpose__c,
                July__c,
                November__c,
                March__c,
                August__c,
                December__c,
                April__c,
                September__c,
                January__c,
                May__c,
                October__c,
                February__c,
                June__c,
                Comments__c,
                Paid_by_Direct_Credit_to_Another_Account__c,
                Account_Name__c,
                Bank__c,
                Branch_Number__c,
                Branch__c,
                Account_Number__c,
                Applicant_1_Date_of_Birth__c,
                Applicant_2_Date_of_Birth__c,
                Applicant_1_Driver_s_License_Number__c,
                Applicant_2_Driver_s_License_Number__c,
                Livestock_Value_per_unit__c,
                Livestock_Market_Value__c,
                Livestock_Adjustment_to_Balance_Sheet__c,
                Land_Size__c,
                Land_Value_per_unit__c,
                Land_Market_Value__c,
                Land_Adjustment_to_Balance_Sheet__c,
                Adjustment_to_Equity_position__c,
                Adjusted_equity_position__c,
                Explanation_of_equity_adjustment__c,
                RABO_Bank_Approval__c,
                RABO_Bank_Approval_File_Attached__c,
                Interest_Free__c,
                Trading_Arrangement__c,
                Alternate_Payment_Arrangements__c,
                Alternate_Payment_Arrangements_Note__c
            FROM
                Advance_Application__c
            WHERE
                Id = :advanceApplicationId];
    }

    @AuraEnabled
    public static Non_Standard_Agreement__c getNonStandardAgreement(Id opportunityId) {

        List<Non_Standard_Agreement__c> nonStandardAgreement = [
            SELECT
                Id,
                Key_Contact_Name__c,
                Key_Contact_Name__r.Email,
                Key_Contact_Name__r.Name,
                Individual_Name_2__c,
                Individual_Name_2__r.Email,
                Individual_Name_2__r.Name,
                Individual_Name_3__c,
                Individual_Name_3__r.Email,
                Individual_Name_3__r.Name,
                Individual_Name_4__c,
                Individual_Name_4__r.Email,
                Individual_Name_4__r.Name,
                Agreement_Type__c,
                Agreement_Start_Date__c,
                Agreement_End_Date__c,
                Milk_Collection_Start_Date__c,
                Minimum_Price_Table__c,
                WEST_Monthly_fat_Kg__c,
                WEST_Monthly_Protein_Kg__c,
                WEST_February_Fat_kg__c,
                WEST_February_Protein_kg__c,
                VIC_Monthly_fat_Kg__c,
                VIC_Monthly_Protein_Kg__c,
                Cashflow__c,
                Competitor_Price_Comparison__c,
                Interest_Free_Advance__c,
                Combined_Production__c,
                Additional_Special_Condition__c,
                Special_Conditions__c,
                Will_Cash_Be_Paid_Prior_to_the_Supplier__c,
                Existing_Advances__c,
                Applicable_Season_and_Type_in_the_Year__c,
                Total_expected_income__c,
                Risk_factor__c,
                Income_available_for_distribution__c,
                CR_Aug__c,
                CR_Sep__c,
                CR_Oct__c,
                CR_Nov__c,
                CR_Dec__c,
                CR_Jan__c,
                CR_Feb__c,
                CR_Mar__c,
                CR_Apr__c,
                CR_May__c,
                CR_Jun__c,
                CR_Jul__c,
                CEP_Aug__c,
                CEP_Sep__c,
                CEP_Oct__c,
                CEP_Nov__c,
                CEP_Dec__c,
                CEP_Jan__c,
                CEP_Feb__c,
                CEP_Mar__c,
                CEP_Apr__c,
                CEP_May__c,
                CEP_Jun__c,
                CEP_Jul__c,
                Forecast_Cost__c,
                Forecast_Benefit__c,
                Supporting_Notes__c,
                Advance_Amount__c,
                Maximum_Interest_Free_Period__c,
                Supporting_Notes_Interest__c,
                Processor__c,
                Farm_Number_to_Combine_Production_with__c,
                Confidentiality__c,
                Require_a_Confidentiality_Clause__c,
                Please_Add_Additional_Special_Condition__c,
                Increase_in_Monthly_Minimum_Volume__c,
                Party__c,
                Party__r.Party_ID__c,
                Farm__c,
                Farm__r.Name,
                Opportunity__c,
                Opportunity__r.Name
            FROM
                Non_Standard_Agreement__c
            WHERE
                Opportunity__c = :opportunityId
            LIMIT 1];

        if (!nonStandardAgreement.isEmpty()) {
            return nonStandardAgreement[0];
        } else {
            return null;
        }
    }

    private static Case createCase(Id farmId, Id partyId, Id contactId, Id recordTypeId, Date incidentDate, String description, Boolean doInsert) {
        Case c = new Case(
            Status = 'Draft',
            Origin = 'Other',
            Raised_By__c = 'Internal',
            RecordTypeId = recordTypeId,
            AccountId = farmId,
            ContactId = contactId,
            Party__c = partyId,
            Incident_Date__c = incidentDate,
            Description = description
        );

        if (doInsert) {
            insert c;
        }

        return c;
    }

    // Creates parent Case record for the form and returns the Case Id
    private static Case createCase(Id farmId, Id partyId, Id recordTypeId, Date incidentDate, String description, Boolean doInsert) {
        return createCase(farmId, partyId, null, recordTypeId, incidentDate, description, doInsert);
    }

    // Creates the Case and child record of Milk Cooling Equipment Purchase Incentive Misc. Form
    @AuraEnabled
    public static Id createMilkCoolingPurchaseIncentiveCase(String formData, Id farmId, Id partyId) {
        // Create parent case
        Case c = createCase(farmId, partyId, GlobalUtility.caseFormMilkCoolingPurchaseIncentiveRecordTypeId, null, null, true);

        // Serialize the form data for the child object
        Milk_Cooling_Incentive__c detail = (Milk_Cooling_Incentive__c) JSON.deserialize(formData, Milk_Cooling_Incentive__c.class);
        detail.Case__c = c.Id;

        insert detail;

        return c.Id;
    }

    @AuraEnabled
    public static Id createSharefarmingArrangementCase(String formData, Id farmId, Id partyId) {
        // Create parent case
        Case c = createCase(farmId, partyId, GlobalUtility.caseFormSharefarmingArrangementRecordTypeId, null, null, true);

        // Serialize the form data for the child object
        Sharefarming_Arrangement__c detail = (Sharefarming_Arrangement__c) JSON.deserialize(formData, Sharefarming_Arrangement__c.class);
        detail.Case__c = c.Id;

        List<Entity_Relationship__c> activeErels = getActiveERELs(farmId);

        Integer terminatedParties = 0;

        for (Entity_Relationship__c activeErel : activeErels) {
            if (detail.Party__c != activeErel.Party__c &&
                detail.Party_2__c != activeErel.Party__c &&
                detail.Party_3__c != activeErel.Party__c &&
                detail.Party_4__c != activeErel.Party__c) {

                detail.put('Terminated_Party' + (++terminatedParties > 1 ? '_' + terminatedParties : '') + '__c', activeErel.Party__c);
            }
        }

        insert detail;

        return c.Id;
    }

    @AuraEnabled
    public static Id createHerdTestingIncentiveCase(String formData, Id farmId, Id partyId) {
        Case c = createCase(farmId, partyId, GlobalUtility.caseFormHerdTestingIncentiveRecordTypeId, null, null, true);
        Herd_Testing_Incentive__c detail = (Herd_Testing_Incentive__c) JSON.deserialize(formData, Herd_Testing_Incentive__c.class);

        detail.Case__c = c.Id;

        insert detail;

        return c.Id;
    }

    // Creates the Case and child record of ABN Update Misc. Form
    @AuraEnabled
    public static Id createABNUpdateCase(String formData, Id farmId, Id partyId) {
        // Serialize the form data for the child object
        ABN_Update__c abnUpdate = (ABN_Update__c) JSON.deserialize(formData, ABN_Update__c.class);

        // Create parent case
        Case c = createCase(farmId, partyId, GlobalUtility.caseFormABNUpdateRecordTypeId, null, null, true);

        abnUpdate.Case__c = c.Id;

        insert abnUpdate;

        return c.Id;
    }

    @AuraEnabled
    public static Id createCeaseOrTransferSupplyCase(String formData, Id farmId, Id partyId, Id individualId, Integer ownerNumber) {
        // Serialize the form data for the child object
        Cease_or_Transfer_Supply__c ceaseOrTransferSupply = (Cease_or_Transfer_Supply__c) JSON.deserialize(formData, Cease_or_Transfer_Supply__c.class);

        // Create parent case
        Case c = createCase(farmId, partyId, individualId, GlobalUtility.caseFormCeaseOrTransferSupplyRecordTypeId, null, null, false);
        c.Owner_Number__c = ownerNumber;

        insert c;

        ceaseOrTransferSupply.Case__c = c.Id;

        insert ceaseOrTransferSupply;

        return c.Id;
    }

    // Creates the Case and child record of Supplier Information Detail Misc. Form
    @AuraEnabled
    public static Id createSupplierInformationDetailCase(String formData, Id farmId, Id partyId, Id individualId, String description, Date incidentDate) {
        // Serialize the form data for the child object
        Supplier_Information__c supplierInformationDetail = (Supplier_Information__c) JSON.deserialize(formData, Supplier_Information__c.class);

        // Create parent case
        Case c = createCase(farmId, partyId, individualId, GlobalUtility.caseFormSupplierInformationRecordTypeId, incidentDate, description, true);

        supplierInformationDetail.Case__c = c.Id;

        insert supplierInformationDetail;

        return c.Id;
    }

    @AuraEnabled
    public static Id createStructuredDeductionCase(String formData, Id farmId, Id partyId) {
        // Serialize the form data for the child object
        Structured_Deduction__c structuredDeductionDetail = (Structured_Deduction__c) JSON.deserialize(formData, Structured_Deduction__c.class);

        // Create parent case
        Case c = createCase(farmId, partyId, GlobalUtility.caseFormStructuredDeductionRecordTypeId, null, null, true);

        structuredDeductionDetail.Case__c = c.Id;

        insert structuredDeductionDetail;

        return c.Id;
    }

    // Creates the Case and child record of Banking Information Misc. Form
    @AuraEnabled
    public static Id createBankingInformationCase(String formData, Id farmId, Id partyId) {
        // Serialize the form data for the child object
        Banking_Information__c bankingInformation = (Banking_Information__c) JSON.deserialize(formData, Banking_Information__c.class);

        // Create parent case
        Case c = createCase(farmId, partyId, GlobalUtility.caseFormBankingInformationRecordTypeId, null, null, true);

        bankingInformation.Case__c = c.Id;

        insert bankingInformation;

        return c.Id;
    }

    @AuraEnabled
    public static Id createAdvanceApplication(String formData, Id farmId, Id partyId) {
        Advance_Application__c advanceApplication = (Advance_Application__c) JSON.deserialize(formData, Advance_Application__c.class);

        Case c = createCase(farmId, partyId, GlobalUtility.caseFormAdvanceApplicationRecordTypeId, null, null, false);
        insert c;

        advanceApplication.Case__c = c.Id;
        insert advanceApplication;

        c.Advance_Application__c = advanceApplication.Id;
        update c;

        return c.Id;
    }

    @AuraEnabled
    public static Id createNewSupplierCase(String supplierInformationFormData, String abnFormData, String bankingInformationFormData, 
                                            String comments, String supplierName, String tradingName, Id individualId, 
                                            Date incidentDate, String supplierRegion, String supplierNumber) {
        Case c = new Case(
            Status = 'Draft',
            Origin = 'Other',
            Raised_By__c = 'Internal',
            RecordTypeId = GlobalUtility.caseFormNewSupplierRecordTypeId,
            SuppliedName = supplierName,
            SuppliedCompany = tradingName,
            // AccountId = farmId,
            ContactId = individualId,
            // Party__c = partyId,
            Incident_Date__c = incidentDate,
            Description = comments,
            Supplier_Region__c = supplierRegion,
            Supplier_Number__c = supplierNumber
        );

        insert c;

        Supplier_Information__c supplierInformationDetail = (Supplier_Information__c) JSON.deserialize(supplierInformationFormData, Supplier_Information__c.class);
        ABN_Update__c abnUpdateDetail = (ABN_Update__c) JSON.deserialize(abnFormData, ABN_Update__c.class);
        Banking_Information__c bankingInformationDetail = (Banking_Information__c) JSON.deserialize(bankingInformationFormData, Banking_Information__c.class);

        supplierInformationDetail.Case__c = c.Id;
        abnUpdateDetail.Case__c = c.Id;
        bankingInformationDetail.Case__c = c.Id;

        insert supplierInformationDetail;
        insert abnUpdateDetail;
        insert bankingInformationDetail;

        return c.Id;
    }

    @AuraEnabled
    public static Id updateAdvanceApplication(String formData) {
        Advance_Application__c advanceApplication = (Advance_Application__c) JSON.deserialize(formData, Advance_Application__c.class);
        update advanceApplication;

        return advanceApplication.Case__c;
    }

    @AuraEnabled
    public static Id upsertNonStandardAgreement(String formData) {
        Non_Standard_Agreement__c nonStandardAgreement = (Non_Standard_Agreement__c) JSON.deserialize(formData, Non_Standard_Agreement__c.class);

        upsert nonStandardAgreement;

        return nonStandardAgreement.Id;
    }

    @AuraEnabled
    public static Boolean isUserMilkSupplyOfficer() {
        Boolean isMilkSupplyOfficer = false;
        Id userId = UserInfo.getUserId();
        //Declaring a Set as we don't want Duplicate Group Ids
        Set<Id> results = new Set<Id>();

        ///Declaring a Map for Group with Role
        Map<Id,Id> grRoleMap = new Map<Id,Id>();

        //Populating the Map with RelatedID(i.e.UserRoledId) as Key
        for(Group gr : [SELECT Id , relatedId, Name FROM Group]) {
            grRoleMap.put(gr.relatedId,gr.id);
        }

        //Groups directly associated to user
        Set<Id> groupwithUser = new Set<Id>();

        //Populating the Group with User with GroupId we are filtering only  for Group of Type Regular,Role and RoleAndSubordinates
        for(GroupMember u :[
            SELECT GroupId 
            FROM GroupMember 
            WHERE UserOrGroupId=:userId 
            AND Group.Type IN ('Regular', 'Role', 'RoleAndSubordinates')]) {
            groupwithUser.add(u.groupId);
        }

        //Groups with Role
        for(User u :[SELECT UserRoleId FROM User WHERE Id=:userId]) {
            //Checking if the current User Role is part of Map or not
            if(grRoleMap.containsKey(u.UserRoleId)) {
                results.add(grRoleMap.get(u.UserRoleId));
            }
        }
        //Combining both the Set
        results.addAll(groupwithUser);

        //Traversing the whole list of Groups to check any other nested Group
        Map<Id,Id> grMap = new Map<Id,Id>();
        for(GroupMember gr : [
            SELECT Id, UserOrGroupId, GroupId 
            FROM GroupMember 
            WHERE Group.Type IN ('Regular', 'Role', 'RoleAndSubordinates')
        ]) {
            grMap.put(gr.UserOrGroupId,gr.Groupid);
        }
        for(Id i :results) {
            if(grMap.containsKey(i)) {
                results.add(grMap.get(i));
            }
        }

        List<Id> groupIdsToUse = new List<Id>(results);
        List<Group> groupNames = [
            SELECT
             Id,
             DeveloperName 
            FROM Group 
            WHERE Id IN :groupIdsToUse
        ];

        for(Group g : groupNames) {
            if(g.DeveloperName == 'Milk_Supply_Services_AU') {
                isMilkSupplyOfficer = true;
            }
        }
        return isMilkSupplyOfficer;
    }

    @AuraEnabled
    public static Boolean isApplicationDraft(Id applicationId){
        Advance_Application__c application = [
            SELECT 
                Id,
                Case__r.Status
            FROM Advance_Application__c
            WHERE Id = :applicationId
        ];

        return application?.Case__r?.Status == 'Draft';
    }
}