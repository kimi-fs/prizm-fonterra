@isTest
private class AMSLandingControllerTest {
    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup static void testSetup() {
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
    }

    @isTest static void testController() {
        User theUser = [SELECT Id FROM User WHERE Email = :USER_EMAIL];

        System.runAs(theUser) {
            Test.startTest();
            AMSLandingController controller = new AMSLandingController();
            Test.stopTest();

            System.assertEquals(true, controller.userIsLoggedIn, 'User is logged in not set correctly');
        }
    }

    @isTest static void testGetPageLinks() {
        String APPLE_STORE_LINK = 'http://www.appstoretest.com';
        String GOOGLE_STORE_LINK = 'http://www.googlestoretest.com';
        List<CommunityPageLinks__c> pageLinks = new List<CommunityPageLinks__c>();
        pageLinks.add(new CommunityPageLinks__c(
                          Name = AMSCommunityUtil.APPLE_STORE_DEVELOPERNAME,
                          Page__c = AMSCommunityUtil.APP_LINKS_PAGE,
                          Url__c = APPLE_STORE_LINK
                      ));
        pageLinks.add(new CommunityPageLinks__c(
                          Name = AMSCommunityUtil.GOOGLE_STORE_DEVELOPERNAME,
                          Page__c = AMSCommunityUtil.APP_LINKS_PAGE,
                          Url__c = GOOGLE_STORE_LINK
                      ));
        insert pageLinks;

        Test.startTest();
        System.assertEquals(APPLE_STORE_LINK, AMSLandingController.getAppStoreLink(), 'App store link not set correctly');
        System.assertEquals(GOOGLE_STORE_LINK, AMSLandingController.getGoogleStoreLink(), 'Google store link not set correctly');
        Test.stopTest();
    }

    @isTest static void testGetParameterNames() {
        Test.startTest();
        System.assertEquals(AMSCommunityUtil.getArticleParameterName(), AMSLandingController.getArticleParameterName(), 'Article parameter name not correct');
        Test.stopTest();
    }

    @isTest static void testGetNewsArticles_OneArticle() {
        Integer NEWS_ARTICLE_COUNT = 1;
        TestClassDataUtil.createAMSNewsArticle(NEWS_ARTICLE_COUNT);

        User theUser = [SELECT Id FROM User WHERE Email = :USER_EMAIL];

        System.runAs(theUser) {
            AMSLandingController controller = new AMSLandingController();

            Test.startTest();
            System.assertEquals(NEWS_ARTICLE_COUNT, controller.getNewsArticles().size(), 'Wrong news article count was returned.');
            Test.stopTest();
        }
    }

    @isTest static void testGetNewsArticles_MultipleArticles() {
        Integer NEWS_ARTICLE_COUNT = 10;
        TestClassDataUtil.createAMSNewsArticle(NEWS_ARTICLE_COUNT);

        User theUser = [SELECT Id FROM User WHERE Email = :USER_EMAIL];

        System.runAs(theUser) {
            AMSLandingController controller = new AMSLandingController();

            Test.startTest();
            System.assertEquals(AMSLandingController.NEWS_ARTICLES_LIMIT, controller.getNewsArticles().size(), 'Wrong news article count was returned.');
            Test.stopTest();
        }
    }

    @isTest static void testGetNewsArticles_NoArticles() {
        User theUser = [SELECT Id FROM User WHERE Email = :USER_EMAIL];

        System.runAs(theUser) {
            AMSLandingController controller = new AMSLandingController();

            Test.startTest();
            System.assertEquals(0, controller.getNewsArticles().size(), 'Wrong news article count was returned.');
            Test.stopTest();
        }
    }
}