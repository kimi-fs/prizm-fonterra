/**
* Description: Handler class for ContactTrigger
* @author: Salman Zafar (Davanti Consulting)
* @date: 14 May 2015
* @test: ContactTrigger_Test
*/
public class ContactTriggerHandler extends TriggerHandler {

    private static final Map<Id, Id> RECORDTYPE_DEFAULT_ACCOUNT_MAP = new Map<Id, Id>{
        GlobalUtility.individualAURecordTypeId => GlobalUtility.defaultAccountIdAU
    };

    public override void beforeInsert() {
        setDefaultAccount(Trigger.new);
    }
    public override void beforeUpdate() {
        setDefaultAccount(Trigger.new);
    }
    public override void afterInsert() {
        createChannelPreferences(Trigger.newMap.keySet());
    }
    public override void afterUpdate() {
        copyCommunityContactInfoToUser(Trigger.new, (Map<Id, Contact>) Trigger.oldMap);
    }
    public override void afterDelete() {
        cleanupChildRecordsAfterDelete((Map<Id, Contact>) Trigger.oldMap);
    }

    public static void setDefaultAccount(list<Contact> listNewContact){
        for (Contact c : listNewContact){
            if (RECORDTYPE_DEFAULT_ACCOUNT_MAP.containsKey(c.RecordTypeId) && c.AccountId == null) {
                c.AccountId = RECORDTYPE_DEFAULT_ACCOUNT_MAP.get(c.RecordTypeId);
            }
        }
    }

    public static void copyCommunityContactInfoToUser(List<Contact> contacts, Map<Id,Contact> oldContacts){

        Map<Id, Contact> contactsById = new Map<Id, Contact>(contacts);

        //to avoid mixed dml errors with users still being created only update users who have not just been created by the userProvisioning function
        List<User> users = [SELECT Id, UserName, ContactId FROM User WHERE ContactId IN :contactsById.keySet() AND IsActive = true];

        List<User> usersToUpdate = new List<User>();
        for (User u : users){
            Contact c = contactsById.get(u.ContactId);
            Contact o = oldContacts.get(u.ContactId);

            if (c.Email == null){
                c.addError('All Farm Source Web users must have an email address');
            } else {

                Boolean hasChanged = false;

                if (c.FirstName != o.FirstName ||
                    c.MiddleName != o.MiddleName ||
                    c.LastName != o.LastName ||
                    c.Email != o.Email ||
                    c.Phone != o.Phone ||
                    c.MobilePhone != o.MobilePhone
                ){
                    u.FirstName = c.FirstName;
                    u.MiddleName = c.MiddleName;
                    u.LastName = c.LastName;
                    u.Email = c.Email;
                    u.Phone = c.Phone;
                    u.MobilePhone = c.MobilePhone;

                    hasChanged = true;
                }

                if (hasChanged) {
                    usersToUpdate.add(u);
                }
            }
        }

        if (usersToUpdate.size() > 0){
            update usersToUpdate;
        }
    }


    //This method is used to identify and then process any child records which had previously belonged to the Loser record of the merge process
    //FSCRM-2875
    //Refactored by FSCRM-5530
    public static void cleanupChildRecordsAfterDelete(Map<Id, Contact> oldMap) {

        Set<Id> contactsStaying = new Set<Id>();
        for (Id contactId : oldMap.keySet()) {
            if (oldMap.get(contactId).MasterRecordId != null) {
                contactsStaying.add(oldMap.get(contactId).MasterRecordId);
            }
        }

        Map<Id, ContactMergeCleanupService.ContactChildRecordsWrapper> contactsToStay = ContactMergeCleanupService.getContactChildRecordsByContactId(contactsStaying);
        ContactMergeCleanupService.mergeContactsChildRecords(JSON.serialize(contactsToStay));
    }

    @future
    public static void createChannelPreferences(final Set<Id> contactIdSet) {
        try {
            Set<String> recordTypeDeveloperNames = new Set<String>{
                'Individual_AU'
            };

            Map<Id, Contact> mapContactById = new Map<Id, Contact>([SELECT Id, RecordTypeId FROM Contact WHERE Id IN: contactIdSet AND RecordType.DeveloperName IN: recordTypeDeveloperNames]);
            if (mapContactById.keySet().size() > 0) {
                Map<Id, Channel_Preference__c> mapChannelPreferenceById = PreferenceService.getChannelPreferenceMap(contactIdSet);
                insert PreferenceService.createMissingChannelPreferences(mapContactById.values(), mapChannelPreferenceById);
            }
        } catch (Exception e) {
            System.debug('Exception: ' + e.getMessage());
        }
    }
}