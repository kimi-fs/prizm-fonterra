/**
 * Description: The test class for the Agreement Application Trigger handler
 * @author: Max (Trineo)
 * @date: 2021
 */
@isTest
public with sharing class AgreementApplicationTriggerHandlerTest {
    @isTest
    static void testAgreementCreation() {
        Account farm = TestClassDataUtil.createSingleAccountFarm();
        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        Opportunity opp = new Opportunity(
            Name = 'Test',
            CloseDate = Date.today().addDays(20),
            StageName = 'Deferred'
        );
        insert opp;

        List<Non_Standard_Agreement__c> nsaList = new List<Non_Standard_Agreement__c> {
            new Non_Standard_Agreement__c(
                Farm__c = farm.Id,
                Party__c = party.Id,
                Agreement_Start_Date__c = Date.today(), 
                Agreement_End_Date__c = Date.today().addDays(10),
                Status__c = 'Draft'
            ),
            new Non_Standard_Agreement__c(
                Farm__c = farm.Id,
                Party__c = party.Id,
                Agreement_Start_Date__c = Date.today(), 
                Agreement_End_Date__c = Date.today().addDays(10),
                Status__c = 'Draft'
            ),
            new Non_Standard_Agreement__c(
                Opportunity__c = opp.Id,
                Farm__c = null,
                Party__c = null,
                Agreement_Start_Date__c = Date.today(), 
                Agreement_End_Date__c = Date.today().addDays(10),
                Status__c = 'Draft'
            )
        };
        insert nsaList;
        System.assertEquals(3, [SELECT COUNT() FROM Non_Standard_Agreement__c]);
        
        Test.startTest();
        nsaList[1].Status__c = 'Approved';
        nsaList[2].Status__c = 'Approved';
        update nsaList;
        Test.stopTest();

        Agreement__c agreement = [
            SELECT 
                Id, Farm__c, Agreement_Status__c
            FROM Agreement__c
            WHERE Agreement_Application__c = :nsaList[1].Id
        ];

        System.assertEquals(1, [SELECT COUNT() FROM Agreement__c WHERE Agreement_Application__c != null]);
        System.assertEquals(0, [SELECT COUNT() FROM Agreement__c WHERE Agreement_Application__c = :nsaList[0].Id]);
        System.assertEquals(1, [SELECT COUNT() FROM Agreement__c WHERE Agreement_Application__c = :nsaList[1].Id]);
        System.assertEquals(0, [SELECT COUNT() FROM Agreement__c WHERE Agreement_Application__c = :nsaList[2].Id]);
        System.assertEquals(nsaList[1].Farm__c, agreement.Farm__c);
        System.assertEquals('Pending', agreement.Agreement_Status__c);
    }    
}