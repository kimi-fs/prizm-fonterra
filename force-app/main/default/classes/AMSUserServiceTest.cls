/**
 * Description: Test for AMSUserService
 * @author: Amelia (Trineo)
 * @date: July 2017
 */
 @isTest private class AMSUserServiceTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';
    private static final String PASSWORD_8CHAR = 'Pa$$w0rd';

    @testSetup static void testSetup(){
        TestClassDataUtil.individualDefaultAccountAU();
    }

    @isTest static void testUpdateEmailVerified(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id =: communityUser.ContactId LIMIT 1].Encrypted_Username__c;
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser){
            AMSUserService.updateEmailVerified(encryptedUsername);
        }

        System.assertNotEquals(null, [SELECT Email_Verified__c FROM User WHERE Id =: communityUser.Id].Email_Verified__c, 'Email verified not updated');
    }

    @isTest static void testUpdateEmailVerified_WithFakeUsername(){
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        Boolean verifiedCheck;
        System.runAs(guestUser){
            verifiedCheck = AMSUserService.updateEmailVerified('fake-encrypted-username');
        }

        System.assertEquals(false, verifiedCheck, 'Email verified without user');
    }

    @isTest static void testCheckVerified_WithEncryptedUsername_NotVerifiedUser(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id =: communityUser.ContactId LIMIT 1].Encrypted_Username__c;
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        Boolean verifiedCheck;
        System.runAs(guestUser){
            verifiedCheck = AMSUserService.checkVerified(encryptedUsername);
        }

        System.assertEquals(false, verifiedCheck, 'Returned true when user is not verified');
    }

    @isTest static void testCheckVerified_WithEncryptedUsername_VerifiedUser(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs([SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1][0]){
            communityUser.Email_Verified__c = Datetime.now();
            update communityUser;
        }
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id =: communityUser.ContactId LIMIT 1].Encrypted_Username__c;
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        Boolean verifiedCheck;
        System.runAs(guestUser){
            verifiedCheck = AMSUserService.checkVerified(encryptedUsername);
        }

        System.assertEquals(true, verifiedCheck, 'Returned false when user is verified');
    }

    @isTest static void testCheckVerified_AsLoggedInUser_NotVerifiedUser(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id =: communityUser.ContactId LIMIT 1].Encrypted_Username__c;

        Boolean verifiedCheck;
        System.runAs(communityUser){
            verifiedCheck = AMSUserService.checkVerified();
        }

        System.assertEquals(false, verifiedCheck, 'Returned true when user is not verified');
    }

    @isTest static void testCheckVerified_AsLoggedInUser_VerifiedUser(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs([SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1][0]){
            communityUser.Email_Verified__c = Datetime.now();
            update communityUser;
        }

        Boolean verifiedCheck;
        System.runAs(communityUser){
            verifiedCheck = AMSUserService.checkVerified();
        }

        System.assertEquals(true, verifiedCheck, 'Returned false when user is verified');
    }

    @isTest static void testGetUserEmailAddress(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id =: communityUser.ContactId LIMIT 1].Encrypted_Username__c;
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        String emailCheck;
        System.runAs(guestUser){
            emailCheck = AMSUserService.getUserEmailAddress(encryptedUsername);
        }

        System.assertEquals(USER_EMAIL.toLowerCase(), emailCheck.toLowerCase(), 'Incorrect email address returned');
    }

    @isTest static void testGetEncryptedUsernameFromId(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id =: communityUser.ContactId LIMIT 1].Encrypted_Username__c;
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        String encryptedUsernameCheck;
        System.runAs(guestUser){
            encryptedUsernameCheck = AMSUserService.getEncryptedUsernameFromId(communityUser.Id);
        }

        System.assertEquals(encryptedUsername, encryptedUsernameCheck, 'Encrypted username returned');
    }

    @isTest static void testGetUserFromEmailAddress(){
    	User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser){
        	Test.startTest();
            User u = AMSUserService.getUserFromEmailAddress(USER_EMAIL);
            Test.stopTest();
        	System.assertEquals(communityUser.Id, u.Id, 'Wrong user returned');
        }
    }

    @isTest static void testGetUserPasswordLocked(){
    	User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser){
        	Test.startTest();
            Boolean locked = AMSUserService.getUserPasswordLocked(communityUser.Id);
            Test.stopTest();
        	System.assertEquals(false, locked, 'Password locked was not correct');
        }
    }

    @isTest static void testCheckUserLoggedIn_NotLoggedIn(){
    	User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser){
        	Test.startTest();
            Boolean loggedIn = AMSUserService.checkUserLoggedIn();
            Test.stopTest();
        	System.assertEquals(false, loggedIn, 'Logged In was not correct');
        }
    }

    @isTest static void testCheckUserLoggedIn_LoggedIn(){
    	User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
        	Test.startTest();
            Boolean loggedIn = AMSUserService.checkUserLoggedIn();
            Test.stopTest();
        	System.assertEquals(true, loggedIn, 'Logged In was not correct');
        }
    }

    @isTest static void testGetUser_LoggedIn(){
    	User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
        	Test.startTest();
            User u = AMSUserService.getUser();
            Test.stopTest();
        	System.assertEquals(communityUser.Id, u.Id, 'User not returned');
        }
    }

    @isTest static void testGetUser_FromEncryptedUsername(){
		User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        String encryptedUsername = [SELECT Id, Contact.Encrypted_Username__c FROM Contact WHERE Id =: communityUser.ContactId LIMIT 1].Encrypted_Username__c;
		System.debug('encryptedUsername: ' + encryptedUsername );
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser){
        	Test.startTest();
            User u = AMSUserService.getUser(encryptedUsername);
            Test.stopTest();
        	System.assertEquals(communityUser.Id, u.Id, 'User not returned');
        }
    }

    @isTest static void testGetContactFromEmail(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser){
            Test.startTest();
            Contact c = AMSUserService.getContactFromEmail(USER_EMAIL);
            Test.stopTest();
            System.assertEquals(communityUser.ContactId, c.Id, 'Contact not returned');
        }
    }

    @isTest static void testGetEmaiDomain(){
        System.assertEquals('@test.com', AMSUserService.getEmailDomain('test@test.com'), 'Wrong email domain returned');
    }

    @isTest static void testGetEmaiDomain_NotAnEmail(){
        System.assertEquals('test.com', AMSUserService.getEmailDomain('test.com'), 'Wrong email domain returned');
    }

    @isTest static void testIsValidPassword_ValidPassword(){
        System.assert(AMSUserService.isValidPassword(PASSWORD_8CHAR), 'Password was valid');
    }

    @isTest static void testIsValidPassword_ValidPassword_9Char(){
        System.assert(AMSUserService.isValidPassword(PASSWORD_8CHAR+'9'), 'Password was valid');
    }

    @isTest static void testIsValidPassword_ValidPassword_10Char(){
        System.assert(AMSUserService.isValidPassword(PASSWORD_8CHAR+'10'), 'Password was valid');
    }

    @isTest static void testIsValidPassword_InvalidPassword_Null(){
        System.assert(!AMSUserService.isValidPassword(null), 'Password was invalid');
    }

    @isTest static void testIsValidPassword_InvalidPassword_Uppercase(){
        System.assert(!AMSUserService.isValidPassword('pa$$w0rd'), 'Password was invalid');
    }

    @isTest static void testIsValidPassword_InvalidPassword_Lowercase(){
        System.assert(!AMSUserService.isValidPassword('PA$$W0RD'), 'Password was invalid');
    }

    @isTest static void testIsValidPassword_InvalidPassword_Number(){
        System.assert(!AMSUserService.isValidPassword('Pa$$word'), 'Password was invalid');
    }

    @isTest static void testIsValidPassword_InvalidPassword_Length(){
        System.assert(!AMSUserService.isValidPassword('12Short'), 'Password was invalid');
    }

    @isTest static void testPasswordMatches_MatchingPassword(){
        System.assert(AMSUserService.passwordMatches(PASSWORD_8CHAR, PASSWORD_8CHAR), 'Password was valid');
    }

    @isTest static void testPasswordMatches_MisMatchingPassword(){
        System.assert(!AMSUserService.passwordMatches(PASSWORD_8CHAR, 'I do not match'), 'Password was invalid');
    }

    @isTest static void testGetUserFromContactId(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        User testUser = AMSUserService.getUserFromContactId(communityUser.ContactId);
        System.assertEquals(communityUser.Id, testUser.Id, 'Wrong user returned');
    }

    @isTest static void testUpdateUser(){
        String NEW_NAME = 'New Name';
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        communityUser.FirstName = NEW_NAME;

        System.runAs(communityUser){
            AMSUserService.updateUser(communityUser);
        }

        System.assertEquals(NEW_NAME, [SELECT Id, FirstName FROM User WHERE Id =: communityUser.Id].FirstName, 'User not updated');
    }

    @isTest static void testFirstTimeRegistrationNextPage_PrimaryAccess(){
        TestClassDataUtil.integrationUserProfile();
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        // To get a derived relationship
        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        List<Account> farms = TestClassDataUtil.createAUFarms(1, true);

        TestClassDataUtil.createPrimaryAccessRelationships(communityUser.ContactId, party, farms);


        System.assertEquals(1, [SELECT count() From Individual_Relationship__c  WHERE Individual__c = :communityUser.ContactId AND
                                                                        Farm__c != null AND
                                                                        Active__c = true AND
                                                                        RecordTypeId = :GlobalUtility.derivedRelationshipRecordTypeId]);

        PageReference nextPage = AMSUserService.firstTimeRegistrationNextPage(communityUser);

        System.assert(TestClassDataUtil.checkPageReferenceMatches(Page.AMSMyDetails, nextPage), 'Redircted to wrong page: ' + nextPage);

    }

    @isTest static void testFirstTimeRegistrationNextPage_NoAccess(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        PageReference nextPage = AMSUserService.firstTimeRegistrationNextPage(communityUser);

        System.assert(TestClassDataUtil.checkPageReferenceMatches(Page.AMSFarmDetails, nextPage), 'Redircted to wrong page: ' + nextPage);
    }

    @isTest static void testGetWelcomeName(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        String welcomeName = AMSUserService.getWelcomeName(communityUser);

        System.assertEquals(communityUser.Contact.FirstName, welcomeName, 'Wrong welcome name ' + welcomeName);
    }

}