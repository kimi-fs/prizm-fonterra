/**
 * Service for accessing user statments and statement entries
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately.
 *
 * Author: Amelia (Trineo)
 * Date: August 2017
 */
 global without sharing class AMSStatementService {

    public static final String PDF_DOCUMENT_KEYWORDS = 'AU TEMPORARY DOCUMENT';

    public class SAPPORequest {
        public MilkCollectionStatementByElementsQuery MilkCollectionStatementByElementsQuery;

        public SAPPORequest(MilkCollectionStatementByElementsQuery query) {
            this.MilkCollectionStatementByElementsQuery = query;
        }
    }

    public class SAPPOResponse {
        public MilkCollectionStatementByElementsResponse MilkCollectionStatementByElementsResponse;
        public Log Log;
    }

    public class MilkCollectionStatementByElementsQuery {
        public String FarmNumber;
        public String PartyIdentificationNumber;
        public String SeasonPeriod;
        public String SeasonYear;
    }

    public class MilkCollectionStatementByElementsResponse {
        public String PDFContent; //base64 encoded PDF content
    }

    public class Log {
        public String BusinessDocumentProcessingResultCode;
        public String MaximumLogItemSeverityCode;
        public Item Item;
    }

    public class Item {
        public String TypeID;
        public String CategoryCode;
        public String SeverityCode;
        public String Note;
        public String WebURI;
    }

    private static final List<String> STATEMENT_ENTRY_FIELDS_TO_QUERY = new List<String> {
        'Arrears_Due__c',
        'Description__c',
        'Gross_Value__c',
        'GST__c',
        'Id',
        'Name',
        'Net_Value__c',
        'Quantity__c',
        'Rate__c',
        'Record_Type_Developer_Name__c',
        'RecordType.Name',
        'Source_ID__c',
        'Statement__c',
        'Supplementary_Description__c',
        'Type__c',
        'Unit__c',
        'Unpaid_Amount__c'
    };

    private static final List<String> STATEMENT_FIELDS_TO_QUERY = new List<String> {
        'Id',
        'Party__c',
        'Farm__c',
        'Date__c'
    };

    private static String queryStatementEntriesStart() {
        return 'SELECT ' + String.join(STATEMENT_ENTRY_FIELDS_TO_QUERY, ',') + ' FROM Statement_Entry__c ';
    }

    private static String queryStatementStart() {
        return 'SELECT ' + String.join(STATEMENT_FIELDS_TO_QUERY, ',') + ' FROM Statement__c ';
    }

    public static List<Statement_Entry__c> getStatementEntries(Id partyId, Id farmId, String month, String year) {
        List<Statement_Entry__c> statementEntries =  new List<Statement_Entry__c>();
        Date selectedDate;
        if (month.isNumeric() && year.isNumeric()) {
            selectedDate = Date.newInstance(Integer.valueOf(year), Integer.valueOf(month), 1);
        }
        if (selectedDate != null) {
            Date startDate = selectedDate.toStartOfMonth();
            Date endDate = selectedDate.toStartOfMonth().addMonths(1).addDays(-1);

            statementEntries = getStatementEntries(partyId, farmId, startDate, endDate);
        }
        return statementEntries;
    }

    public static Statement__c getStatement(Id partyId, Id farmId, String month, String year) {
        Statement__c statement;
        Date selectedDate;
        if (month.isNumeric() && year.isNumeric()) {
            selectedDate = Date.newInstance(Integer.valueOf(year), Integer.valueOf(month), 1);
        }
        if (selectedDate != null) {
            Date startDate = selectedDate.toStartOfMonth();
            Date endDate = selectedDate.toStartOfMonth().addMonths(1).addDays(-1);

            statement = getStatement(partyId, farmId, startDate, endDate);
        }
        return statement;
    }

    public static List<Statement_Entry__c> getStatementEntries(Id partyId, Id farmId, Date fromDate, Date toDate) {
        String query = queryStatementEntriesStart();
        query += ' WHERE Statement__r.Party__c = :partyId AND Statement__r.Farm__c = :farmId AND Statement__r.Date__c >= :fromDate AND Statement__r.Date__c <= :toDate ORDER BY Line_Number__c';
        return Database.query(query);
    }

    public static List<Statement_Entry__c> getStatementEntries(Id statementId) {
        String query = queryStatementEntriesStart();
        query += ' WHERE Statement__c = :statementId ORDER BY Line_Number__c';
        return Database.query(query);
    }

    public static Statement__c getStatement(Id partyId, Id farmId, Date fromDate, Date toDate) {
        String query = queryStatementStart();
        query += ' WHERE Party__c = :partyId AND Farm__c = :farmId AND Date__c >= :fromDate AND Date__c <= :toDate';

        List<Statement__c> statements = Database.query(query);
        Statement__c statement;

        if (!statements.isEmpty()) {
            statement = statements[0];
        }

        return statement;
    }

    public static Statement__c getLatestStatement(Id partyId, Id farmId) {
        String query = queryStatementStart();
        query += ' WHERE Party__c = :partyId AND Farm__c = :farmId ORDER BY Date__c DESC LIMIT 1';

        List<Statement__c> statements = Database.query(query);
        Statement__c statement;

        if (!statements.isEmpty()) {
            statement = statements[0];
        }

        return statement;
    }

    public static String doStatementsCallout(MilkCollectionStatementByElementsQuery query) {
        AMS_PO_Settings__c settings = AMS_PO_Settings__c.getOrgDefaults();

        String endpoint = settings.SAP_PO_Statements_Endpoint_URL__c;
        String userName = settings.SAP_PO_Statements_Username__c;
        String password = settings.SAP_PO_Statements_Password__c;

        HttpRequest req = new HttpRequest();

        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(userName + ':' + password)));
        req.setEndpoint(endpoint);

        req.setBody(JSON.serialize(new SAPPORequest(query)));

        Http http = new Http();
        String responseBody;

        try {
            HTTPResponse res = http.send(req);
            responseBody = res.getBody();
            System.debug(responseBody);
        } catch (Exception e) {
            Logger.log('Statements callout failed', e.getMessage(), Logger.LogType.ERROR);
            Logger.saveLog();
        }

        return responseBody;
    }

    WebService static String getStatementPDFAsJSON(Id statementId) {
        return JSON.serialize(getStatementPDF(statementId));
    }

    public static SAPPOResponse getStatementPDF(Id statementId) {
        Statement__c statement = [SELECT ID, Farm__r.Name, Party__r.Party_ID__c, Date__c FROM Statement__c WHERE ID = :statementId];

        String farmNumber = String.valueOf(Integer.valueOf(statement.Farm__r.Name)); //remove leading zero and convert it back to a string
        String partyIdentificationNumber = statement.Party__r.Party_ID__c;
        String seasonPeriod = String.valueOf(statement.Date__c.month()).leftPad(2, '0');
        String seasonYear = String.valueOf(statement.Date__c.year());

        return getStatementPDF(farmNumber, partyIdentificationNumber, seasonPeriod, seasonYear);
    }

    public static SAPPOResponse getStatementPDF(String farmNumber, String partyIdentificationNumber, String seasonPeriod, String seasonYear) {
        MilkCollectionStatementByElementsQuery query = new MilkCollectionStatementByElementsQuery();

        query.FarmNumber = farmNumber;
        query.PartyIdentificationNumber = partyIdentificationNumber;
        query.SeasonPeriod = seasonPeriod;
        query.SeasonYear = seasonYear;

        String response = doStatementsCallout(query);
        SAPPOResponse poResponse = new SAPPOResponse();

        if (!String.isBlank(response)) {
            poResponse = (SAPPOResponse) JSON.deserialize(response, SAPPOResponse.class);

            if (poResponse.MilkCollectionStatementByElementsResponse != null && poResponse.MilkCollectionStatementByElementsResponse.PDFContent != null) {
                poResponse.MilkCollectionStatementByElementsResponse.PDFContent = poResponse.MilkCollectionStatementByElementsResponse.PDFContent.replaceAll('\n', '');
            }
        }

        return poResponse;
    }

    public static Boolean statementExists(SAPPOResponse response) {
        Boolean statementExists = true;

        if (response.Log.Item.Note == 'A Statement does not exist for the parameters selected.  Please change your selection and try again.') {
            statementExists = false;
        } else if (String.isBlank(response.MilkCollectionStatementByElementsResponse.PDFContent)) {
            statementExists = false;
        }

        return statementExists;
    }


}