/**
 * Description: Controller for AMS Farm Details page - creates a case with related farms
 * @author: Amelia (Trineo)
 * @date: August 2017
 */
public with sharing class AMSFarmDetailsController {

    public static final String NEW_LINE = '\n';

    public Contact contact { get; set; }

    public List<FarmDetailsWrapper> farmWrappers {get; set;}

    private static Map<String, String> messages;
    static {
        messages = AMSCommunityUtil.initialisePageMessages('AMSFarmDetails');
    }

    private Address__c postalAddress;
    private Address__c physicalAddress;

    public AMSFarmDetailsController() {
        this.contact = AMSContactService.getContact();
        this.farmWrappers = new List<FarmDetailsWrapper> {new FarmDetailsWrapper()};
    }

    public String getWelcomeName() {
        return AMSContactService.getWelcomeName(contact);
    }

    public void addNewFarmDetailsWrapper() {
        this.farmWrappers.add(new FarmDetailsWrapper());
    }

    public PageReference saveAndContinue() {
        this.farmWrappers.sort();

        if (this.farmWrappers.isEmpty()) {
            // No farms - so effectivly they have skipped this page.
            return Page.AMSMyDetails;
        }

        Case linkFarmsCase = new Case(
            AccountId = getFarmForCase().Id,
            ContactId = this.Contact.Id,
            RecordTypeId = GlobalUtility.caseDigitalServicesAURecordTypeId,
            Type = 'Website',
            Sub_Type__c = 'Account Creation',
            Origin = 'Web',
            Status = 'New',
            Priority = 'Normal',
            Description = createCaseDescription(this.farmWrappers),
            Raised_By__c = 'External',
            Sentiment__c = 'Neutral'
        );

        try {
            AMSCaseService.insertCase(linkFarmsCase);
            return Page.AMSMyDetails;
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, messages.get('unknown-error')));
            return null;
        }
    }

    private Account getFarmForCase() {
        Account accountToLink;
        if (!farmWrappers.isEmpty() && String.isNotBlank(farmWrappers[0].farmNumber)) {
            List<Account> farms = AMSEntityService.getFarms(new List<String> {farmWrappers[0].farmNumber});
            if (!farms.isEmpty()) {
                accountToLink = farms[0];
            }
        }

        // If we can't find a farm with the details provided link the case to the default AU account
        if (accountToLink == null) {
            List<Account> defaultNoEntity = AMSEntityService.getFarms(new List<Id> {GlobalUtility.defaultAccountIdAU});
            if (!defaultNoEntity.isEmpty()) {
                accountToLink = defaultNoEntity[0];
            }
        }
        return accountToLink;
    }

    private String createCaseDescription(List<FarmDetailsWrapper> farmWrappers) {
        setAddresses();

        String description = '';
        description += 'New User Registration Request \n\n';

        description += 'User Details: \n';
        description += 'Name: ' + user.Name + ' \n';
        description += 'Email: ' + user.Email + ' \n';
        description += 'Preferred Name: ' + (String.isNotBlank(contact.Preferred_Name__c) ? contact.Preferred_Name__c  : '') + NEW_LINE;
        description += 'Main Phone: ' + (String.isNotBlank(contact.Phone) ? contact.Phone  : '') + NEW_LINE;
        description += 'Mobile Phone: ' + (String.isNotBlank(contact.MobilePhone) ? contact.MobilePhone  : '') + NEW_LINE;
        description += 'Postal Address: ' + (postalAddress != null && String.isNotBlank(postalAddress.Address__c) ? postalAddress.Address__c  : '') + NEW_LINE;
        description += 'Physical Address: ' + (physicalAddress != null && String.isNotBlank(physicalAddress.Address__c) ? physicalAddress.Address__c  : '') + NEW_LINE + NEW_LINE;

        description += 'Farm Details: \n';
        for (FarmDetailsWrapper fdw : farmWrappers) {
            if (String.isNotBlank(fdw.farmNumber) && String.isNotBlank(fdw.farmBusinessName) && String.isNotBlank(fdw.bankAccount) && String.isNotBlank(fdw.businessABN) && String.isNotBlank(fdw.dairyLicenseNumber)) {
                description += 'Farm Number: ' + fdw.farmNumber + NEW_LINE;
                description += 'Farm Business Name: ' + fdw.farmBusinessName + NEW_LINE;
                description += 'Last 4 digits of bank account: ' + fdw.bankAccount + NEW_LINE;
                description += 'Business ABN: ' + fdw.businessABN + NEW_LINE;
                description += 'Dairy License Number: ' + fdw.dairyLicenseNumber + NEW_LINE + NEW_LINE;
            }
        }
        return description;
    }

    private void setAddresses() {
        if (contact.Addresses__r != null && !contact.Addresses__r.isEmpty()) {
            for (Address__c addr : contact.Addresses__r) {
                if (addr.RecordTypeId == GlobalUtility.postalAddressRecordTypeId && this.postalAddress == null) {
                    this.postalAddress = addr;
                } else if (addr.RecordTypeId == GlobalUtility.physicalAddressRecordTypeId && this.physicalAddress == null) {
                    this.physicalAddress = addr;
                }
            }
        }
    }

    public class FarmDetailsWrapper implements Comparable {
        public String farmNumber {get; set;}
        public String farmBusinessName {get; set;}
        public String bankAccount {get; set;}
        public String businessABN {get; set;}
        public String dairyLicenseNumber {get; set;}

        public FarmDetailsWrapper() {
            this.farmNumber = '';
            this.farmBusinessName = '';
            this.bankAccount = '';
            this.businessABN = '';
            this.dairyLicenseNumber = '';
        }

        public Integer compareTo(Object compareTo) {
            FarmDetailsWrapper compareToFDW = (FarmDetailsWrapper) compareTo;

            if (this.farmNumber == compareToFDW.farmNumber) return 0;
            if (String.isBlank(this.farmNumber)) return 1;
            if (String.isBlank(compareToFDW.farmNumber)) return -1;
            if (this.farmNumber > compareToFDW.farmNumber) return 1;
            return -1;
        }
    }
}