/**
 * Generates CSVs for Production and Quality reports, currently only works with AMSProduction (or Daily Production & Quality)
 *
 * Tested by AMSProductionController_Test
 *
 * Author: John Au (Trineo)
 * Date: 2018-05-14
 */
public with sharing class AMSProdQualCSVGenerator {

    private AMSCollectionService.CollectionPeriodForm collectionPeriodForm;
    private AMSProductionReport.ProductionTable productionTable;

    public AMSProdQualCSVGenerator(AMSCollectionService.CollectionPeriodForm collectionPeriodForm, AMSProductionReport.ProductionTable productionTable) {
        this.collectionPeriodForm = collectionPeriodForm;
        this.productionTable = productionTable;
    }

    public String generateCSV() {
        String CSVString = 'Date,';

        for (AMSProductionReport.ProductionTableTab tab : productionTable.tabs) {
            if (tab != null) {
                for (AMSProductionReport.ProductionTableColumn column : tab.columns) {
                    CSVString += column.unitOfMeasurement.label + ',';
                }
            }
        }

        CSVString = CSVString.removeEnd(',');
        CSVString += '\n';

        for (AMSCollectionService.CollectionPeriodWrapper wrapper : collectionPeriodForm.collectionPeriodWrapperList) {
            String wrapperJSON = JSON.serialize(wrapper);
            Map<String, Object> wrapperMap = (Map<String, Object>) JSON.deserializeUntyped(wrapperJSON);
            Map<String, Object> collectionPeriodMap = (Map<String, Object>) wrapperMap.get('collectionPeriod');

            CSVString += collectionPeriodMap.get('dateOfCollection') + ',';

            for (AMSProductionReport.ProductionTableTab tab : productionTable.tabs) {
                if (tab != null) {
                    for (AMSProductionReport.ProductionTableColumn column : tab.columns) {
                        Object value = collectionPeriodMap.get(column.unitOfMeasurement.name);

                        if (value != null && value instanceof Decimal) {
                            Decimal decimalValue = (Decimal) collectionPeriodMap.get(column.unitOfMeasurement.name);
                            decimalValue = decimalValue.setScale(column.unitOfMeasurement.precision);
                            CSVString += String.valueOf(decimalValue) + ',';
                        } else if (value != null && column.unitOfMeasurement.name == AMSCollectionService.OtherTestResult.name) {
                            String otherTestResults = '';
                            for (Object milkQualityItemObject : (List<Object>) value) {
                                Map<String, Object> milkQualityItemMap = (Map<String, Object>) milkQualityItemObject;
                                otherTestResults += milkQualityItemMap.get('testName') + '-' + (milkQualityItemMap.get('testResultType') != null ? milkQualityItemMap.get('testResultType') : milkQualityItemMap.get('testResult')) + '; ';
                            }
                            CSVString += otherTestResults.removeEnd('; ') + ',';
                        } else {
                            CSVString += ',';
                        }
                    }
                }
            }

            CSVString = CSVString.removeEnd(',');
            CSVString += '\n';
        }

        return CSVString;
    }

}