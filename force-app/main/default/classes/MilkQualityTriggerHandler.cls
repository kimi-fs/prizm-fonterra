/**
 * Logic class controlling behaviour of the Milk Quality trigger
 * @author Greg Beeforth
 * @date August 2016
 * @testclass Test_MilkQualityTriggerHandler.cls
 */
 public with sharing class MilkQualityTriggerHandler extends TriggerHandler {

    public override void afterInsert() {
        Map<Id, SObject> mapNewMilkQualityById = Trigger.newMap;

        Map<Id, Milk_Quality__c> milkQualityResultsToSend = new Map<Id, Milk_Quality__c>();
        List<Milk_Quality__c> AUMilkQualityResultsToSend = new List<Milk_Quality__c>();

        for (Id recordId : mapNewMilkQualityById.keySet()) {
            Milk_Quality__c newMilkQualityRecord = (Milk_Quality__c) mapNewMilkQualityById.get(recordId);

            if (sendMilkQualityAlertAU(newMilkQualityRecord)) {
                AUMilkQualityResultsToSend.add(newMilkQualityRecord);
            }
        }

        if (AUMilkQualityResultsToSend.size() > 0) {
            AMSMilkQualityTriggerHandler.sendSMS(AUMilkQualityResultsToSend);
        }
    }

    private Boolean sendMilkQualityAlertAU(Milk_Quality__c newMilkQualityRecord) {
        return newMilkQualityRecord.Milk_Quality_ID__c != null &&
                newMilkQualityRecord.Milk_Quality_ID__c.startsWith('AU') &&
                (newMilkQualityRecord.Test_Type_Long_Description__c == GlobalUtility.COLLECTION_PERIOD_BACTOSCAN_TEST_NAME && newMilkQualityRecord.Result_Interpretation__c == 'Alert') ||
                newMilkQualityRecord.Test_Type_Long_Description__c == GlobalUtility.COLLECTION_PERIOD_THERMODURICS_TEST_NAME;
    }

}