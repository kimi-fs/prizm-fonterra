@isTest
private class AMSNewsControllerTest {
    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup static void testSetup() {
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
    }

    @isTest static void testController_OneNewsArticle() {
        Integer NEWS_ARTICLE_COUNT = 1;
        TestClassDataUtil.createAMSNewsArticle(NEWS_ARTICLE_COUNT);

        User theUser = [SELECT Id FROM User WHERE Email = :USER_EMAIL];

        System.runAs(theUser) {
            Test.startTest();
            System.Test.setCurrentPage(Page.AMSNews);
            AMSNewsController controller = new AMSNewsController();
            Test.stopTest();

            System.assertNotEquals(null, controller);
            System.assertEquals(NEWS_ARTICLE_COUNT, controller.getNumberOfArticles(), 'Number of articles not set correctly');
            System.assertNotEquals(null, controller.getWrappedArticles()[0].getFirstPublishedDate(), 'First published date not set correctly');
        }
    }

    @isTest static void testController_MultipleNewsArticles() {
        Integer NEWS_ARTICLE_COUNT = 50;
        TestClassDataUtil.createAMSNewsArticle(NEWS_ARTICLE_COUNT);

        User theUser = [SELECT Id FROM User WHERE Email = :USER_EMAIL];

        System.runAs(theUser) {
            Test.startTest();
            System.Test.setCurrentPage(Page.AMSNews);
            AMSNewsController controller = new AMSNewsController();
            Test.stopTest();

            System.assertNotEquals(null, controller);
            System.assertEquals(NEWS_ARTICLE_COUNT, controller.getNumberOfArticles(), 'Number of articles not set correctly');
            System.assertEquals(AMSNewsController.ARTICLES_PER_PAGE, controller.getWrappedArticles().size(), 'Number of wrapped articles not set correctly');
        }
    }

    @isTest static void testController_NoNewsArticles() {
        User theUser = [SELECT Id FROM User WHERE Email = :USER_EMAIL];

        System.runAs(theUser) {
            Test.startTest();
            System.Test.setCurrentPage(Page.AMSNews);
            AMSNewsController controller = new AMSNewsController();
            Test.stopTest();

            System.assertNotEquals(null, controller);
        }
    }

    @IsTest
    private static void testController_NoDairyCodeNewsArticles() {
        Knowledge__kav newsArticle = TestClassDataUtil.createAMSNewsArticle(1)[0];
        Knowledge__DataCategorySelection selection = new Knowledge__DataCategorySelection(DataCategoryGroupName = 'AMS_News', DataCategoryName = 'Dairy_Code', ParentId = newsArticle.Id);

        insert selection;

        User theUser = [SELECT Id FROM User WHERE Email = :USER_EMAIL];

        System.runAs(theUser) {
            Test.startTest();
            System.Test.setCurrentPage(Page.AMSDashboard);
            AMSNewsController controller = new AMSNewsController();
            Test.stopTest();

            System.assertNotEquals(null, controller);
            System.assert(controller.getWrappedArticles().isEmpty(), 'Dairy Code articles not filtered out from controller');
        }
    }

    @isTest static void testGetParameterNames() {
        Test.startTest();
        System.assertEquals(AMSCommunityUtil.getArticleParameterName(), AMSNewsController.getArticleParameterName(), 'Article parameter name not correct');
        System.assertEquals(AMSCommunityUtil.getPageNumberParameterName(), AMSNewsController.getPageNumberParameterName(), 'Page number parameter name not correct');
        System.assertEquals(AMSCommunityUtil.getArticleCategoryParameterName(), AMSNewsController.getArticleCategoryParameterName(), 'Article Category parameter name not correct');
        System.assertEquals(AMSNewsController.ARTICLES_PER_PAGE, AMSNewsController.getArticlesPerPage(), 'Articles per page not correct');
        Test.stopTest();
    }

}