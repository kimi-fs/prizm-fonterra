@isTest private class PaginationControllerTest {


    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup static void testSetup(){
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
    }

    @isTest static void testGetPageNumber_ValidValue(){
        PageReference pageRef = Page.AMSNewsDetail;
        pageRef.getParameters().put(PaginationController.getPageNumberParameterName(), '5');
        System.Test.setCurrentPage(pageRef);

        PaginationController controller = new PaginationController();
        controller.totalNumberOfRecords = 80;
        controller.recordsShownPerPage = 10;
        controller.numberOfPagesToShow = 4;

        System.assertEquals(5, PaginationController.getCurrentPageNumber(), 'Page number not correct');
    }

    @isTest static void testGetPageNumber_InvalidValue(){
        PageReference pageRef = Page.AMSNewsDetail;
        pageRef.getParameters().put(PaginationController.getPageNumberParameterName(), 'NotANumber');
        System.Test.setCurrentPage(pageRef);

        PaginationController controller = new PaginationController();
        controller.totalNumberOfRecords = 80;
        controller.recordsShownPerPage = 10;
        controller.numberOfPagesToShow = 4;

        System.assertEquals(1, PaginationController.getCurrentPageNumber(), 'Page number not correct');
    }

    @isTest static void testGetPageNumber_NegativeValue(){
        PageReference pageRef = Page.AMSNewsDetail;
        pageRef.getParameters().put(PaginationController.getPageNumberParameterName(), '-1');
        System.Test.setCurrentPage(pageRef);

        PaginationController controller = new PaginationController();
        controller.totalNumberOfRecords = 80;
        controller.recordsShownPerPage = 10;
        controller.numberOfPagesToShow = 4;

        System.assertEquals(1, PaginationController.getCurrentPageNumber(), 'Page number not correct');
    }

    @isTest static void testGetPageNumber_MissingValue(){
        PageReference pageRef = Page.AMSNewsDetail;
        System.Test.setCurrentPage(pageRef);

        PaginationController controller = new PaginationController();
        controller.totalNumberOfRecords = 80;
        controller.recordsShownPerPage = 10;
        controller.numberOfPagesToShow = 4;

        System.assertEquals(1, PaginationController.getCurrentPageNumber(), 'Page number not correct');
    }

    @isTest static void testGetPaginationLinks_CurrentPageFirst() {
        PageReference pageRef = Page.AMSNewsDetail;
        pageRef.getParameters().put(PaginationController.getPageNumberParameterName(), '1');
        System.Test.setCurrentPage(pageRef);

        PaginationController controller = new PaginationController();
        controller.totalNumberOfRecords = 80;
        controller.recordsShownPerPage = 10;
        controller.numberOfPagesToShow = 4;

        List<PaginationController.PaginationLink> links = controller.getPaginationLinks();

        System.assertEquals(7, links.size(), 'Incorrect number of pages');
        System.assertNotEquals('Prev', links[0].label, 'Prev added when on first page');
        System.assertNotEquals('...', links[2].label, '... added when on first page');
        System.assertEquals('Next', links[links.size()-1].label, 'Next not added when on first page');
        System.assertEquals('...', links[links.size()-3].label, '... not added when on first page');
    }

    @isTest static void testGetPaginationLinks_CurrentPageMid() {
        //TestClassDataUtil.createAMSNewsArticle(80);

        PageReference pageRef = Page.AMSNewsDetail;
        pageRef.getParameters().put(PaginationController.getPageNumberParameterName(), '6');
        System.Test.setCurrentPage(pageRef);

        PaginationController controller = new PaginationController();
        controller.totalNumberOfRecords = 80;
        controller.recordsShownPerPage = 10;
        controller.numberOfPagesToShow = 4;

        List<PaginationController.PaginationLink> links = controller.getPaginationLinks();

        System.assertEquals(10, links.size(), 'Incorrect number of pages');
        System.assertEquals('Prev', links[0].label, 'Prev not added when on mid page');
        System.assertEquals('...', links[2].label, '... added not when on mid page');
        System.assertEquals('Next', links[links.size()-1].label, 'Next not added when on mid page');
    }

    @isTest static void testGetPaginationLinks_CurrentPageLast() {
        //TestClassDataUtil.createAMSNewsArticle(80);

        PageReference pageRef = Page.AMSNewsDetail;
        pageRef.getParameters().put(PaginationController.getPageNumberParameterName(), '8');
        System.Test.setCurrentPage(pageRef);

        PaginationController controller = new PaginationController();
        controller.totalNumberOfRecords = 80;
        controller.recordsShownPerPage = 10;
        controller.numberOfPagesToShow = 4;

        List<PaginationController.PaginationLink> links = controller.getPaginationLinks();

        System.assertEquals(7, links.size(), 'Incorrect number of pages');
        System.assertEquals('Prev', links[0].label, 'Prev not added when on last page');
        System.assertEquals('...', links[2].label, '... added not when on last page');
        System.assertNotEquals('Next', links[links.size()-1].label, 'Next added when on last page');
        System.assertNotEquals('...', links[links.size()-3].label, '... added when on last page');
    }

    @isTest static void testGetOffset_Start(){
        PageReference pageRef = Page.AMSNewsDetail;
        pageRef.getParameters().put(PaginationController.getPageNumberParameterName(), '1');
        System.Test.setCurrentPage(pageRef);

        PaginationController controller = new PaginationController();
        controller.totalNumberOfRecords = 80;
        controller.recordsShownPerPage = 10;
        controller.numberOfPagesToShow = 4;

        System.assertEquals(0, PaginationController.getOffset(10));
    }

    @isTest static void testGetOffset_End(){
        PageReference pageRef = Page.AMSNewsDetail;
        pageRef.getParameters().put(PaginationController.getPageNumberParameterName(), '10');
        System.Test.setCurrentPage(pageRef);

        PaginationController controller = new PaginationController();
        controller.totalNumberOfRecords = 80;
        controller.recordsShownPerPage = 10;
        controller.numberOfPagesToShow = 4;

        System.assertEquals(90, PaginationController.getOffset(10));
    }

    @isTest static void testGetRecodsToDisplayOnPage_Start(){
        List<Knowledge__kav> records = TestClassDataUtil.createAMSNewsArticle(20);

        PageReference pageRef = Page.AMSNewsDetail;
        pageRef.getParameters().put(PaginationController.getPageNumberParameterName(), '1');
        System.Test.setCurrentPage(pageRef);

        PaginationController controller = new PaginationController();
        controller.totalNumberOfRecords = 80;
        controller.recordsShownPerPage = 10;
        controller.numberOfPagesToShow = 4;

        List<sObject> recordsForPage = PaginationController.getRecordsToDisplayOnPage(10, records);

        System.assertEquals(10, recordsForPage.size());
        System.assertEquals(records[0].id, recordsForPage[0].Id);
    }

    @isTest static void testGetRecodsToDisplayOnPage_End(){
        List<Knowledge__kav> records = TestClassDataUtil.createAMSNewsArticle(15);

        PageReference pageRef = Page.AMSNewsDetail;
        pageRef.getParameters().put(PaginationController.getPageNumberParameterName(), '2');
        System.Test.setCurrentPage(pageRef);

        PaginationController controller = new PaginationController();
        controller.totalNumberOfRecords = 80;
        controller.recordsShownPerPage = 10;
        controller.numberOfPagesToShow = 4;

        List<sObject> recordsForPage = PaginationController.getRecordsToDisplayOnPage(10, records);

        System.assertEquals(5, recordsForPage.size());
        System.assertEquals(records[10].id, recordsForPage[0].Id);
    }

}