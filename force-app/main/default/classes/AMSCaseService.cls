/**
 * AMSCaseService
 *
 * Deal with Cases - in particular DML to avoid permissions issues on the Accounts
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately. 
 *
 * Author: Ed (Trineo)
 * Date: Aug 2017
 */
public without sharing class AMSCaseService {
	
    /**
     * Allow insertion of a Case as a community user since the Account on the Case 
     * is private (we're running without sharing).
     */
	public static Case insertCase(case c) {
        insert c;
        return c;
    }
}