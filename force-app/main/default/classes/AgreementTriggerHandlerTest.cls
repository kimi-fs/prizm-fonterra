@IsTest
private class AgreementTriggerHandlerTest {

    private static List<Account> farm;
    private static Account party;
    private static Agreement__c agreem;
    private static User userAUProfile;
    private static User userIntegrationProfile;
    private static User userSystemAdmin;
    private static User userExternalIntegration;

    @TestSetup
    private static void userSetup() {
        insert TestClassDataUtil.createUserByProfile('Field Team AU - Area Manager', 'Fonterra AU');
        TestClassDataUtil.createAdminUser();
        TestClassDataUtil.createIntegrationUser();

        insert TestClassDataUtil.createUserByProfile('External System Integration User', 'Conga User Integration');
        TestClassDataUtil.createFileSharingSetting('Exclusive-Standard-Supply-Agreement-', 'V', '', '', '', true);
        Field_Team_Tools_Settings__c individualDefaultAccount = TestClassDataUtil.individualDefaultAccountAU();
        Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();
    }

    private static void testSetup() {
        userAUProfile = [SELECT Id, isActive FROM User WHERE Profile.Name = 'Field Team AU - Area Manager' AND isActive = true LIMIT 1];
        userIntegrationProfile = [SELECT Id, isActive FROM User WHERE Profile.Name = 'Integration User' AND isActive = true LIMIT 1];
        userSystemAdmin =  [SELECT Id, isActive FROM User WHERE Profile.Name = 'System Administrator' AND isActive = true LIMIT 1];
        userExternalIntegration = [SELECT Id, isActive FROM User WHERE Profile.Name = 'External System Integration User' AND isActive = true LIMIT 1];


        farm = TestClassDataUtil.createAUFarms(2, true);
        party = TestClassDataUtil.createSingleAccountPartyAU();
        agreem = TestClassDataUtil.createAgreement('Standard Agreement', farm[0].Id, Date.today(), true);
        agreem.Party__c = party.Id;
        insert agreem;

        List<Account> farmToUpdate = new List<Account>();
        farmToUpdate.add(new Account(Id = farm[0].Id, OwnerId = userAUProfile.Id));
        farmToUpdate.add(new Account(Id = party.Id, OwnerId = userAUProfile.Id));
        update farmToUpdate;

    }

    @IsTest
    private static void testAgreementId() {
        testSetup();

        Test.startTest();
        party.Party_ID__c = '200000';
        update party;

        agreem.Agreement_Status__c = AgreementService.AGREEMENT_STATUS_EXECUTED;
        update agreem;

        agreem = [SELECT Agreement_ID__c, Farm_Number__c FROM Agreement__c WHERE Id = :agreem.Id];
        Date today = Date.today();
        String expectedAspireID = 'AU_' + agreem.Farm_Number__c + '_' + party.Party_ID__c + '_'
                                 + DateTime.newInstance(today.year(), today.month(), today.day()).format('yyyy-MM-dd');
        System.assertEquals(expectedAspireID, agreem.Agreement_ID__c);

        agreem.Agreement_Status__c = AgreementService.AGREEMENT_STATUS_CANCELLED;
        update agreem;

        Test.stopTest();

        agreem = [SELECT Agreement_ID__c FROM Agreement__c WHERE Id = :agreem.Id];
        System.assertEquals(null, agreem.Agreement_ID__c);
    }

    @IsTest
    private static void testValidateDeleteAgreementAccess() {
        testSetup();
        List<Account> farmToUpdate = new List<Account>();
        farmToUpdate.add(new Account(Id = farm[0].Id, OwnerId = userSystemAdmin.Id));
        farmToUpdate.add(new Account(Id = party.Id, OwnerId = userSystemAdmin.Id));
        update farmToUpdate;

        Test.startTest();
        String errorMessageReadOnlyAccess;
        System.runAs(userAUProfile) {
            try {
                delete agreem;
            } catch (Exception e) {
                errorMessageReadOnlyAccess = e.getMessage();
            }
        }
        System.assert(String.isNotEmpty(errorMessageReadOnlyAccess));
        System.assert(errorMessageReadOnlyAccess.contains('You don\'t have access to delete this record.'));

        String errorMessageNoDeleteAccess;
        System.runAs(userIntegrationProfile) {
            try {
                delete agreem;
            } catch (Exception e) {
                errorMessageNoDeleteAccess = e.getMessage();
            }
        }

        System.assert(String.isNotEmpty(errorMessageNoDeleteAccess));
        System.assert(errorMessageNoDeleteAccess.contains('You don\'t have access to delete this record.'));


        System.runAs(userSystemAdmin) {
            delete agreem;
        }

        Test.stopTest();

        List<Agreement__c> agreement = [SELECT Id FROM Agreement__c WHERE Id = :agreem.Id];
        System.assert(agreement.isEmpty());

    }

    @IsTest
    private static void testValidateEditAgreementAccess() {
        testSetup();

        agreem.Start_Date__c = Date.today() + 1;
        agreem.Active__c = false;
        update agreem;
        Test.startTest();

        String errorMessage;
        System.runAs(userAUProfile) {
            try {
                agreem.Execution_Date__c = Date.today();
                agreem.Signing_Method__c = 'Paper';
                agreem.Start_Date__c = Date.today() + 2;
                agreem.End_Date__c = Date.today() + 30;
                agreem.Agreement_Status__c = AgreementService.AGREEMENT_STATUS_EXECUTED;
                update agreem;
            } catch (Exception e) {
                errorMessage = e.getMessage();
            }

        }

        System.runAs(userIntegrationProfile) {
            try {
                agreem.Start_Date__c = Date.today();
                agreem.Active__c = true;
                update agreem;
            } catch (Exception e) {
                errorMessage = e.getMessage();
            }
        }

        System.runAs(userSystemAdmin) {
           try {
                agreem.Signing_Method__c = AgreementService.SIGNING_METHOD_ONLINE;
                update agreem;
            } catch (Exception e) {
                errorMessage = e.getMessage();
            }
        }

        System.assert(String.isEmpty(errorMessage));

        String errorMessageNoEditAccess;
        System.runAs(userAUProfile) {
            try {
                agreem.Agreement_Status__c = AgreementService.AGREEMENT_STATUS_PENDING;
                update agreem;
            } catch (Exception e) {
                errorMessageNoEditAccess = e.getMessage();
            }

        }
        System.assert(String.isNotEmpty(errorMessageNoEditAccess));
        System.debug('errorMessageNoEditAccess' + errorMessageNoEditAccess);
        System.assert(errorMessageNoEditAccess.contains('You don\'t have access to update an active agreement.'));
        Test.stopTest();

        List<Agreement__c> agreement = [SELECT Id, Active__c, Agreement_Status__c, Signing_Method__c  FROM Agreement__c WHERE Id = :agreem.Id];
        System.assertEquals('Online', agreement[0].Signing_Method__c);
        System.assertEquals('Executed', agreement[0].Agreement_Status__c);
        System.assertEquals(true, agreement[0].Active__c);

    }

    @IsTest
    private static void testCheckExistingAgreement() {
        testSetup();
        Test.startTest();
        String errorMessageDuplicateAgreement;
        try {
            Agreement__c newAgreement = TestClassDataUtil.createAgreement('Standard Agreement', farm[0].Id, Date.today(), false);
            newAgreement.Party__c = party.Id;
            insert newAgreement;
        } catch (Exception e) {
            errorMessageDuplicateAgreement = e.getMessage();
        }

        System.assert(errorMessageDuplicateAgreement.contains('There\'s an existing agreement with the same farm, party and start date'));
        Test.stopTest();
    }

    @IsTest
    private static void testCreateCaseForManualSignedAgreements() {
        testSetup();
        Test.startTest();
        agreem.Start_Date__c = Date.today() + 1;
        agreem.Active__c = false;
        update agreem;

        System.runAs(userAUProfile) {
            agreem.Execution_Date__c = Date.today();
            agreem.Signing_Method__c = 'Paper';
            agreem.Start_Date__c = Date.today() + 2;
            agreem.End_Date__c = Date.today() + 30;
            agreem.Agreement_Status__c = AgreementService.AGREEMENT_STATUS_EXECUTED;
            update agreem;
        }

        Test.stopTest();


        List<Case> getCaseCreated = [SELECT Id, Description, Party__c, Raised_By__c, Origin
                                     FROM Case
                                     WHERE AccountId = :agreem.Farm__c
                                           AND RecordTypeId = :GlobalUtility.caseManageSupplyAURecordTypeId
                                           AND OwnerId = :GlobalUtility.getQueueIdFromDeveloperName('Milk_Supply_Services_AU')];
        Agreement__c agreement = [SELECT Name FROM Agreement__c WHERE Id = :agreem.Id];

        System.assert(!getCaseCreated.isEmpty());
        Case createdCase = getCaseCreated[0];
        System.assert(createdCase.Description.contains(agreement.Name));
        System.assertEquals('Internal', createdCase.Raised_By__c);
        System.assertEquals('Supplier', createdCase.Origin);
    }

    @IsTest
    private static void testCancelledAgreement() {
        testSetup();
        Test.startTest();
        String errorMessageDuplicateAgreement;
        try {
            Agreement__c newAgreement = TestClassDataUtil.createAgreement('Standard Agreement', farm[0].Id, Date.today(), false);
            newAgreement.Party__c = party.Id;
            newAgreement.Agreement_Status__c = AgreementService.AGREEMENT_STATUS_CANCELLED;
            insert newAgreement;
        } catch (Exception e) {
            errorMessageDuplicateAgreement = e.getMessage();
        }

        System.assertEquals(null, errorMessageDuplicateAgreement, 'Cancelled Agreement should not be part of duplicate agreement criteria.');
        Test.stopTest();
    }

    @IsTest
    private static void testUpdateAgreementByIntegrationUser() {
        testSetup();
        party.Party_ID__c = '200000';
        update party;
        agreem.Start_Date__c = Date.today() + 1;
        agreem.Active__c = false;
        update agreem;

        Test.startTest();

        Date today = Date.today();
        String setAspireId = 'AU_' + '0001' + '_' + '2000' + '_' + DateTime.newInstance(today.year(), today.month(), today.day()).format('yyyy-MM-dd');

        System.runAs(userIntegrationProfile) {
            Agreement__c newAgreement = TestClassDataUtil.createAgreement('Standard Agreement', farm[1].Id, Date.today(), false);
            newAgreement.Party__c = party.Id;
            newAgreement.Agreement_Id__c = setAspireId;
            upsert newAgreement Agreement_Id__c;

            newAgreement = [SELECT Id, Active__c, Agreement_Status__c, Agreement_Id__c FROM Agreement__c WHERE Id = :newAgreement.Id];
            System.assert(String.isNotEmpty(newAgreement.Id), 'A new agreement should be created.');
            System.assertEquals(null, newAgreement.Agreement_Status__c, 'Integration Agreement Status default value should be blank');
            System.assertEquals(setAspireId, newAgreement.Agreement_Id__c, 'Aspire Id should be the same as the Integration user sets.');

            newAgreement.End_Date__c = Date.today() + 30;
            update newAgreement;
            System.assertEquals(setAspireId, newAgreement.Agreement_Id__c, 'Aspire Id should be the same as the Integration user sets.');
        }

        System.runAs(userAUProfile) {
            agreem.Execution_Date__c = Date.today();
            agreem.Signing_Method__c = 'Paper';
            agreem.Start_Date__c = Date.today() + 2;
            agreem.End_Date__c = Date.today() + 30;
            agreem.Agreement_Status__c = AgreementService.AGREEMENT_STATUS_EXECUTED;
            update agreem;
        }

        agreem = [SELECT Agreement_ID__c, Farm_Number__c FROM Agreement__c WHERE Id = :agreem.Id];
        System.runAs(userIntegrationProfile) {
            Agreement__c agreementFromMilkPay = new Agreement__c();
            agreementFromMilkPay.Agreement_Id__c = agreem.Agreement_ID__c;
            agreementFromMilkPay.Start_Date__c = Date.today();
            upsert agreementFromMilkPay Agreement_Id__c;
        }

        Test.stopTest();

        agreem = [SELECT Agreement_ID__c, Active__c, Agreement_Status__c FROM Agreement__c WHERE Id = :agreem.Id];
        System.assertNotEquals(null, agreem.Agreement_ID__c, 'Aspire Id should not be blank');
        System.assertEquals(true, agreem.Active__c, 'Agreement should be active.');
        System.assertEquals('Executed', agreem.Agreement_Status__c, 'Agreement status should be remain as executed');
    }
}