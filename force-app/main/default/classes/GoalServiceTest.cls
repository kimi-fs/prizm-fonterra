/**
 * GoalServiceTest.cls
 * Description: Test for GoalService.cls
 * @author: John Au
 * @date: December 2018
 */
 @IsTest
private class GoalServiceTest {
    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @TestSetup
    private static void testSetup() {
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.individualDefaultAccountAU();

        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu(USER_EMAIL);
    }

    @IsTest
    private static void testGetGoals() {
        Contact individual = [SELECT Id FROM Contact WHERE Email = :USER_EMAIL];
        Account farm = [SELECT Id FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId LIMIT 1];
        Goal__c goal = TestClassDataUtil.createGoal(false, individual);

        goal.Visible_in_Farmsource__c = true;

        insert (goal);

        System.assertEquals(1, GoalService.getGoalsForIndividual(individual.Id).size());
        System.assert(GoalService.getGoal(goal.Id) != null);
    }

    @IsTest
    private static void testGetActions() {
        Contact individual = [SELECT Id FROM Contact WHERE Email = :USER_EMAIL];
        Account farm = [SELECT Id FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId LIMIT 1];
        Goal__c goal = TestClassDataUtil.createGoal(false, individual);

        goal.Visible_in_Farmsource__c = true;

        insert (goal);

        Action__c action = TestClassDataUtil.createAction(true, goal);

        System.assertEquals(1, GoalService.getActionsForGoals(new List<Goal__c> { goal }).size());
        System.assert(GoalService.getActionDetails(action.Id) != null);
    }
}