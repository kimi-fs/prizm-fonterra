/**
 * Description: Test class for AMSScheduleBatchDailyMilk
 * @author: John (Trineo)
 * @date: November 2017
 */
@IsTest
private class AMSScheduleBatchDailyMilkTest {

    @TestSetup
    static void integrationUserSetup() {
        //Create a integration user
        TestClassDataUtil.individualDefaultAccountAu();
        TestClassDataUtil.integrationUserProfile();
    }

    @IsTest
    private static void testSend() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
        User integrationUser = TestClassDataUtil.createIntegrationUser();

        System.runAs(integrationUser) {
            Channel_Preference_Setting__c ADMR = new Channel_Preference_Setting__c(Exact_Target_Message_ID__c = 'test', Exact_Target_Message_Keyword__c = 'test', CME_Message_Type_Id__c = 'ADMR');
            insert (ADMR);
            List<Account> farms = [
                SELECT Id
                FROM Account
                WHERE Id IN (
                        SELECT Farm__c
                        FROM Individual_Relationship__c
                        WHERE Individual__c = :communityUser.ContactId
                        AND Active__c = true
                )
            ];

            insert (new Channel_Preference__c(NotifyBySMS__c = true, DeliveryByEmail__c = true, Contact_ID__c = communityUser.ContactId, Channel_Preference_Setting__c = ADMR.Id));
            Collection__c collection = TestClassDataUtil.createSingleCollection(farms[0], false);
            collection.Fat_Percent__c = 5.5;
            collection.Prot_Percent__c = 5.5;
            collection.Pick_Up_Date__c = Date.today();
            collection.Result_Sent__c = false;
            insert (collection);

            Milk_Quality__c milkQuality = TestClassDataUtil.createMilkQuality(collection, false);
            milkQuality.Test_Type_Long_Description__c = GlobalUtility.COLLECTION_PERIOD_BMCC_TEST_NAME;
            insert milkQuality;

            Test.startTest();

            Database.executeBatch(new AMSScheduleBatchDailyMilk());

            Test.stopTest();

            collection = [Select Id, Result_Sent__c From Collection__c Where Id = :collection.Id];

            System.assertEquals(true, collection.Result_Sent__c);
        }
    }

}