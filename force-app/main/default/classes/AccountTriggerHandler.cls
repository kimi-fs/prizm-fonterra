/**
 * Description: Handler class for AccountTrigger
 * @author: Salman Zafar (Davanti Consulting)
 * @date: May 2015
 * @test: AccountTrigger_Test
 */
public class AccountTriggerHandler extends TriggerHandler {

    public override void beforeInsert() {
        setRecordTypeId(Trigger.new);
    }

    public override void afterInsert() {
        createFarmSeasons(Trigger.new);
    }
    public override void afterUpdate() {
        updateIndividualMilkSupplyRelationshipManager(Trigger.new, (Map<Id, Account>) Trigger.oldMap);
    }

    /**
     * Populate Record Type based on the value of recordTypeFieldAPIName
     * This must be run first in the before trigger because it assigns the correct record type for any new record
     **/
    public static void setRecordTypeId(List<SObject> newAccounts) {
        GlobalUtility.setRecordTypeIdFromField(Account.Record_Type_Developer_Name__c, newAccounts);
    }

    /**
     * Australian Farm Entities when created should have the current and next seasons created, this function is only
     * ever called after insert, so no need to check on existing Seasons.
     */
    public static void createFarmSeasons(List<Account> newAccounts) {
        List<Farm_Season__c> farmSeasons = new List<Farm_Season__c>();
        // We want the current ane future, so 2 reference periods.
        List<Reference_Period__c> referencePeriods = AMSReferencePeriodService.getCurrentAndFutureReferencePeriods(2);

        for (Account newAccount : newAccounts) {
            if (newAccount.RecordTypeId == GlobalUtility.auAccountFarmRecordTypeId || newAccount.Record_Type_Developer_Name__c == GlobalUtility.auAccountFarmRecordTypeName) {
                // A new AU Farm, needs some seasoning for taste,

                for (Reference_Period__c referencePeriod : referencePeriods) {
                    Farm_Season__c farmSeason = new Farm_Season__c(
                        Name = referencePeriod.Name.removeStart('Milking Season').trim(),
                        Farm__c = newAccount.Id,
                        Reference_Period__c = referencePeriod.Id
                    );
                    farmSeasons.add(farmSeason);
                }
            }
        }
        insert farmSeasons;
    }

    public static void updateIndividualMilkSupplyRelationshipManager(List<Account> newAccounts, Map<Id, Account> oldAccountsMap) {
        List<Account> farmsWithChangedAreaManagers = new List<Account>();

        for (Account acc : newAccounts) {
            if (oldAccountsMap.get(acc.Id).Rep_Area_Manager__c != acc.Rep_Area_Manager__c) {
                farmsWithChangedAreaManagers.add(acc);
            }
        }

        List<Individual_Relationship__c> drelsFromFarms = [SELECT Id, Individual__c, Farm__c, Farm__r.Rep_Area_Manager__c FROM Individual_Relationship__c WHERE Farm__c IN : farmsWithChangedAreaManagers AND RecordTypeId = : GlobalUtility.derivedRelationshipRecordTypeId];
        Map<Id, Individual_Relationship__c> drelsFromFarmsByIndividualIdMap = new Map<Id, Individual_Relationship__c>();
        List<Contact> individualsToUpdate = new List<Contact>();

        for (Individual_Relationship__c drel : drelsFromFarms) {
            drelsFromFarmsByIndividualIdMap.put(drel.Individual__c, drel);
        }

        List<Individual_Relationship__c> allDrelsForIndividuals = [SELECT Id, Individual__c, Farm__c FROM Individual_Relationship__c WHERE Individual__c IN : drelsFromFarmsByIndividualIdMap.keySet() AND RecordTypeId = : GlobalUtility.derivedRelationshipRecordTypeId];
        Map<Id, List<Individual_Relationship__c>> drelsByIndividualIdMap = new Map<Id, List<Individual_Relationship__c>>();

        for (Individual_Relationship__c drel : allDrelsForIndividuals) {
            if (!drelsByIndividualIdMap.containsKey(drel.Individual__c)) {
                drelsByIndividualIdMap.put(drel.Individual__c, new List<Individual_Relationship__c>());
            }

            drelsByIndividualIdMap.get(drel.Individual__c).add(drel);
        }

        for (Id individualId : drelsByIndividualIdMap.keySet()) {
            Boolean individualRelatedToMoreThanOneFarm = false;
            Id firstFarmId = null;

            for (Individual_Relationship__c drel : drelsByIndividualIdMap.get(individualId)) {
                if (firstFarmId == null) {
                    firstFarmId = drel.Farm__c;
                }
                if (firstFarmId != drel.Farm__c) {
                    individualRelatedToMoreThanOneFarm = true;
                    break;
                }
            }

            if (!individualRelatedToMoreThanOneFarm) {
                individualsToUpdate.add(new Contact(Id = individualId, Milk_Supply_Relationship_Manager__c = drelsFromFarmsByIndividualIdMap.get(individualId).Farm__r.Rep_Area_Manager__c));
            }
        }

        update individualsToUpdate;
    }

}