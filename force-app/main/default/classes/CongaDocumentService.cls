/**
 * Service for creating Conga Documents. Will call out to Conga and have the document attached
 * back on the pertinent record.
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately.
 *
 * @author Ed (trineo)
 * @date Feb 2018
 */
public without sharing class CongaDocumentService {
    public class CongaException extends Exception {}

    // How long to wait for Conga to create the Document (in ms).
    private static final Integer CONGA_TIMEOUT = 120000;
    private static final String CONGA_ENDPOINT = 'https://composer.congamerge.com/composer8/index.html';
    private static final String FARMSEASON_MERGEFIELD_PREFIX = '{!Farm_Season__c.';
    private static final String AGREEMENT_MERGEFIELD_PREFIX = '{!Agreement__c.';
    private static final String NSA_MERGEFIELD_PREFIX = '{!Non_Standard_Agreement__c.';
    private static final String FARMSEASON_APPENDIX_SETTING_NAME = 'DQMS Red Pages Enhanced Assessor Notif';

    private static final Set<String> FARM_SEASON_FIELDS = Schema.SObjectType.Farm_Season__c.fields.getMap().keySet();
    private static final Set<String> AGREEMENT_FIELDS = Schema.SObjectType.Agreement__c.fields.getMap().keySet();
    private static final Set<String> NON_STANDARD_AGREEMENT_FIELDS = Schema.SObjectType.Non_Standard_Agreement__c.fields.getMap().keySet();

    @future (callout=true)
    public static void createCongaDocumentFuture(Id recordId) {
        createCongaDocument(recordId);
    }

    public static void createCongaDocument(Id recordId) {
        String accessToken = getAccessToken();

        String congaUrl = createCongaUrl(recordId, accessToken);

        System.debug('Conga URL: ' + congaUrl);

        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(congaUrl);
        req.setMethod('GET');
        req.setTimeout(CONGA_TIMEOUT);

        // Send the request, and return a response
        HttpResponse res = http.send(req);

        if (res.getStatusCode() != 200) {
            Logger.log('Conga Error, failed to create document', 'Received unexpected response code from Conga: ' + res.getStatusCode() + '. Body: ' + res.getBody(), Logger.LogType.ERROR);
            Logger.saveLog();

            throw new CongaException('Received unexpected response code from Conga: ' + res.getStatusCode() + '. Body: ' + res.getBody());
        }

        Logger.log('CongaDocumentService Log', 'Created Document ' + res.getBody(), Logger.LogType.INFO);
        Logger.saveLog();

        // Document was created, the SF Id of the attachment is in the Body, which we will discard.
        System.debug('Conga Attachment created: ' + res.getBody());
    }

    private static String getAccessToken() {
        final String CONGA_EXTENDED_SESSION = 'Conga Session';

        Organization org = [SELECT id, Name, IsSandbox FROM Organization];

        String audience = org.IsSandbox ? 'https://test.salesforce.com' : 'https://login.salesforce.com';

        Community_Extended_Sessions__c communityExtSessionSettings;
        communityExtSessionSettings = Community_Extended_Sessions__c.getInstance(CONGA_EXTENDED_SESSION);

        if (communityExtSessionSettings == null) {
            throw new CongaException('Missing ' + CONGA_EXTENDED_SESSION + ' Community Extended Sessions custom settings');
        }

        Auth.JWT jwt = new Auth.JWT();
        jwt.setSub(communityExtSessionSettings.Username__c);
        jwt.setAud(audience);
        jwt.setIss(communityExtSessionSettings.Consumer_Key__c);

        System.debug('JWT: ' + jwt);

        //Setup JWS
        Auth.JWS jws = new Auth.JWS(jwt, communityExtSessionSettings.Certificate_Name__c);
        String tokenEndpoint = communityExtSessionSettings.Community_URL__c + '/services/oauth2/token';

        System.debug('JWS: ' + jws);

        if (Test.isRunningTest()) {
            // When running a test don't try and do JWT
            return 'DEADBEEF';
        }

        //Perform JWS token request
        Auth.JWTBearerTokenExchange bearer = new Auth.JWTBearerTokenExchange(tokenEndpoint, jws);
        String accessToken = bearer.getAccessToken();

        return accessToken;
    }

    /**
     * Construct the Conga URL (Application/Farm Season) for the document that we are interested in.
     *
     * Application: The application record type tells us the form we are interested in, which we can
     * use to lookup the Conga template and output filename to use from our custom settings.
     *
     * Farm Season: The farm season name tells us on what conga settings to use.
     * The congar url for farm season is dynamic which can be change via config (Conga Solution) which
     * follows a standard format.
     */
    private static String createCongaUrl(Id recordId, String accessToken) {
        final String SERVURL = Url.getSalesforceBaseUrl().toExternalForm()+'/services/Soap/u/29.0/' + UserInfo.getOrganizationId().substring(0, 15);
        String congaUrl;

        SObjectType sObjectType = ((Id) recordId).getSObjectType();

        if (Agreement__c.SObjectType == sObjectType) {
            congaUrl = createAUOnlineAgreementURL(recordId, accessToken, SERVURL);
        }

        return congaUrl;
    }

    /**
     * Create the URL for an Agreement__c record for Australia
     */
    private static String createAUOnlineAgreementURL(Id agreementId, String accessToken, String servUrl) {
        Agreement__c agreement = AgreementService.getAgreement(agreementId);
        if(agreement.Agreement_Application__c != null) {
            return createNonStandardAgreementURL(agreementId, accessToken, servUrl);
        } else {
            return createStandardAgreementURL(agreementId, accessToken, servUrl);
        }
   }

    public static String createStandardAgreementURL(Id agreementId, String accessToken, String servUrl) {
        
        String congaUrl;
        AMS_Agreement_Settings__c settings = AMS_Agreement_Settings__c.getOrgDefaults();

        Agreement__c agreement = AgreementService.getAgreement(agreementId);

        Id congaTemplateId;
        if(agreement.Agreement_Application__c != null) {
            String congaSolutionTemplateId = agreement.Agreement_Application__r.Conga_Template_Id__c;
            congaTemplateId = [
                SELECT Conga_Template_ID__c 
                FROM APXTConga4__Conga_Template__c
                WHERE APXTConga4__Key__c = :congaSolutionTemplateId 
                LIMIT 1
            ].Id;
        } else {
            congaTemplateId = settings.Conga_Template_ID__c;
        }

        APXTConga4__Conga_Solution__c congaSolution = [
            SELECT Id, APXTConga4__Button_body_field__c
            FROM APXTConga4__Conga_Solution__c
            WHERE Id = :settings.Conga_Solution__c
            LIMIT 1
        ];

        if (String.isBlank(congaSolution.APXTConga4__Button_body_field__c)) {
            throw new CongaException('Conga Solution Id on AMS_Agreement_Settings__c is invalid or does not exist');
        }

        /**
         * The field always contains a URL formatted text that we need to substitute values into. This is achieved
         * by splitting the Conga URL into arguments, adding those to a list, and then putting them all back together
         * again at the end.
         */
        String existingCongaButtonURL = congaSolution.APXTConga4__Button_body_field__c;
        List<String> existingCongaParameters = existingCongaButtonURL.deleteWhiteSpace().split('&');
        List<String> newCongaParameters = new List<String>();
        String outputFileNamePrefix = 'invalid';

        for (String congaParameter : existingCongaParameters) {
            String field;
            List<String> args = new List<String>();

            // Standard part of the url, not changing
            if (congaParameter == '/apex/APXTConga4__Conga_Composer?SolMgr=1') {
                newCongaParameters.add(createURLParameter(CONGA_ENDPOINT + '?SolMgr', '1'));
                newCongaParameters.add(createURLParameter('sessionId', accessToken));
            } else if (congaParameter.startsWith('serverUrl')) {
                newCongaParameters.add(createURLParameter('serverUrl', servUrl));
            } else if (congaParameter.startsWith('Id')) {
                newCongaParameters.add(createURLParameter('Id', agreementId));
            } else if (congaParameter.startsWith('QueryId')) {
                List<String> queryIds = new List<String>();
                for (String queryId : congaParameter.substringAfter('QueryId=').split(',')) {
                    System.debug('queryId: ' + queryId);
                    String fieldAPIName = queryId.substringAfter(AGREEMENT_MERGEFIELD_PREFIX).replace('}','');
                    System.debug('fieldAPIName1: ' + fieldAPIName);

                    if (String.isNotEmpty(fieldAPIName)) {
                        // the merge fields on the button create relationship fields like FarmID__c
                        // This however doesn't exist when querying the record, so we need to use the lookup field instead.
                        if (!AGREEMENT_FIELDS.contains(fieldAPIName.toLowerCase())) {
                            fieldAPIName = fieldAPIName.toLowerCase().replace('id__c', '__c');
                        }
                        String parameter = queryId.substringBefore('%3D') + '%3D' + String.valueOf(agreement.get(fieldAPIName));
                        System.debug('fieldAPIName2: ' + fieldAPIName + ' Parameter: ' + parameter);
                        queryIds.add(parameter);
                    } else {
                        queryIds.add(queryId);
                    }
                }

                newCongaParameters.add(createURLParameter('QueryId', String.join(queryIds,',')));
            } else if (congaParameter.startsWith('OFN')) {
                // Substitute in the agreement
                String val = congaParameter.substringAfter('OFN=');
                String fieldAPIName = val.substringAfterLast(AGREEMENT_MERGEFIELD_PREFIX).replace('}','');
                System.debug('Fetching field: ' + fieldAPIName);
                String fieldValue = String.valueOf(agreement.get(fieldAPIName));
                outputFileNamePrefix = val.substringBefore('{');
                System.debug('OFN Parameter: ' + outputFileNamePrefix + fieldValue);
                newCongaParameters.add(createURLParameter('OFN', EncodingUtil.urlEncode(outputFileNamePrefix + fieldValue, 'UTF-8')));
            } else if (congaParameter.startsWith('TemplateId')) {
                newCongaParameters.add(createURLParameter('TemplateId', congaTemplateId));
            } else {
                newCongaParameters.add(congaParameter);
            }
        }

        //Required
        newCongaParameters.add(createURLParameter('APIMode', '1'));

        // Is there an existing document against this agreement?
        List<ContentDocumentLink> contentDocumentLinks = [
            SELECT ContentDocumentId, ContentDocument.Title
            FROM ContentDocumentLink
            WHERE LinkedEntityId = :agreement.Id AND
                  ContentDocument.Title LIKE :(outputFileNamePrefix + '%')
        ];

        // I need the ContentDocumentId here
        if (!contentDocumentLinks.isEmpty()) {
            newCongaParameters.add(createURLParameter('ContentDocumentID', contentDocumentLinks[0].ContentDocumentId));
        }

        congaUrl = String.join(newCongaParameters,'&');

        return congaUrl;
    }

    public static String createNonStandardAgreementURL(Id agreementId, String accessToken, String servUrl) {
        String congaUrl;
        AMS_Agreement_Settings__c settings = AMS_Agreement_Settings__c.getOrgDefaults();

        Agreement__c agreement = AgreementService.getAgreement(agreementId);
        Non_Standard_Agreement__c nsaAgreement = AgreementService.getNSA(agreement.Agreement_Application__c);

        Id congaTemplateId = [
            SELECT Conga_Template_ID__c 
            FROM APXTConga4__Conga_Template__c
            WHERE APXTConga4__Key__c = :agreement.Agreement_Application__r.Conga_Template_Id__c 
            LIMIT 1
        ].Id;

        APXTConga4__Conga_Solution__c congaSolution = [
            SELECT Id, APXTConga4__Button_body_field__c
            FROM APXTConga4__Conga_Solution__c
            WHERE Id = :settings.Conga_NSA_Solution__c
            LIMIT 1
        ];

        if (String.isBlank(congaSolution.APXTConga4__Button_body_field__c)) {
            throw new CongaException('Conga Solution Id on AMS_Agreement_Settings__c is invalid or does not exist');
        }

        /**
         * The field always contains a URL formatted text that we need to substitute values into. This is achieved
         * by splitting the Conga URL into arguments, adding those to a list, and then putting them all back together
         * again at the end.
         */
        String existingCongaButtonURL = congaSolution.APXTConga4__Button_body_field__c;
        List<String> existingCongaParameters = existingCongaButtonURL.deleteWhiteSpace().split('&');
        List<String> newCongaParameters = new List<String>();
        String outputFileNamePrefix = 'invalid';

        // New NSA agreement conga url
        for (String congaParameter : existingCongaParameters) {
            String field;
            List<String> args = new List<String>();
            // Standard part of the url, not changing
            if (congaParameter == '/apex/APXTConga4__Conga_Composer?SolMgr=1') {
                newCongaParameters.add(createURLParameter(CONGA_ENDPOINT + '?SolMgr', '1'));
                newCongaParameters.add(createURLParameter('sessionId', accessToken));
            } else if (congaParameter.startsWith('serverUrl')) {
                newCongaParameters.add(createURLParameter('serverUrl', servUrl));
            } else if (congaParameter.startsWith('Id')) {
                newCongaParameters.add(createURLParameter('Id', nsaAgreement.Id)); // This has changed and is now a nsa
            } else if (congaParameter.startsWith('QueryId')) {  // This has been changed and needs test

                List<String> queryIds = new List<String>();
                for (String queryId : congaParameter.substringAfter('QueryId=').split(',')) {
                    System.debug('queryId: ' + queryId);
                    String fieldAPIName = queryId.substringAfter(NSA_MERGEFIELD_PREFIX).replace('}','');
                    System.debug('fieldAPIName1: ' + fieldAPIName);
                    
                    if (String.isNotEmpty(fieldAPIName)) {
                        // the merge fields on the button create relationship fields like FarmID__c
                        // This however doesn't exist when querying the record, so we need to use the lookup field instead.
                        if (!NON_STANDARD_AGREEMENT_FIELDS.contains(fieldAPIName.toLowerCase())) {
                            fieldAPIName = fieldAPIName.toLowerCase().replace('id__c', '__c');
                        }
                        String parameter = queryId.substringBefore('%3D') + '%3D' + String.valueOf(agreement.get(fieldAPIName));
                        System.debug('fieldAPIName2: ' + fieldAPIName + ' Parameter: ' + parameter);
                        queryIds.add(parameter);
                    } else {
                        queryIds.add(queryId);
                    }
                }

                newCongaParameters.add(createURLParameter('QueryId', String.join(queryIds,',')));

            } else if (congaParameter.startsWith('OFN')) { // This has changed and needs testing
                // Substitute in the agreement
                String val = congaParameter.substringAfter('OFN=');
                val = val.replace(NSA_MERGEFIELD_PREFIX, '');
                List<string> splits = val.split('}_');
                splits[splits.size()-1] = splits[splits.size()-1].replace('}', '');
                String params = '';
                for(String splitItem : splits) {
                    params += String.valueOf(nsaAgreement.get(splitItem)) + '_';
                }
                outputFileNamePrefix = String.valueOf(nsaAgreement.get(splits[1]));
                params = params.substringBeforeLast('_');
                newCongaParameters.add(createURLParameter('OFN', EncodingUtil.urlEncode(params, 'UTF-8')));
            } else if (congaParameter.startsWith('TemplateId')) {
                newCongaParameters.add(createURLParameter('TemplateId', congaTemplateId));
            } else if (congaParameter.startsWith('FP0')) {
                newCongaParameters.add(createURLParameter('FP0', '1'));
            } else if (congaParameter.startsWith('DS7')) {
            } else if (congaParameter.startsWith('AC0')) {
            } else if (congaParameter.startsWith('TemplateGroup')) {
            } else {
                newCongaParameters.add(congaParameter);
            }
        }

        newCongaParameters.add(createURLParameter('SC0', '1'));
        newCongaParameters.add(createURLParameter('SC1', 'SalesforceFile'));
        newCongaParameters.add(createURLParameter('AttachmentParentID', agreementId));

        //Required
        newCongaParameters.add(createURLParameter('APIMode', '1'));
        // Is there an existing document against this agreement?
        List<ContentDocumentLink> contentDocumentLinks = [
            SELECT ContentDocumentId, ContentDocument.Title
            FROM ContentDocumentLink
            WHERE LinkedEntityId = :agreementId AND
                  ContentDocument.Title LIKE :('%' + outputFileNamePrefix + '%')
        ];
        
        // I need the ContentDocumentId here
        if (!contentDocumentLinks.isEmpty()) {
            newCongaParameters.add(createURLParameter('ContentDocumentID', contentDocumentLinks[0].ContentDocumentId));
        }

        congaUrl = String.join(newCongaParameters,'&');
        System.debug('congaUrl---->'+congaUrl);
        return congaUrl;
    }

    private static String createURLParameter(String key, String value) {
        return String.format(
            '{0}={1}',
            new List<String>{
                key,
                value
            }
        );
    }

}