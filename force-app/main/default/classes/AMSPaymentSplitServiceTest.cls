@isTest
public class AMSPaymentSplitServiceTest {

    @testSetup static void testSetup(){
    }

    @isTest static void testGetPaymentSplits() {
        Account farm = TestClassDataUtil.createAUFarms(1, true)[0];
        List<Payment_Split__c> paymentSplits = TestClassDataUtil.insertFarmPartiesAgreementsAndPaymentSplits(farm.id);

        System.assertEquals(paymentSplits.size(), AMSPaymentSplitService.getPaymentSplits(farm.Id).size());
    }
}