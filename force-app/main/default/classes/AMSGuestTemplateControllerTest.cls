@isTest
private class AMSGuestTemplateControllerTest {

	private static String USER_EMAIL = 'amsUser@thisTest.com';

	@isTest static void testRedirect_LoggedInUser() {
		User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
		System.runAs(communityUser){
			Test.setCurrentPage(Page.AMSLanding);
			AMSGuestTemplateController controller = new AMSGuestTemplateController();
			PageReference returnedPage = controller.redirectIfLoggedIn();
			System.assert(TestClassDataUtil.checkPageReferenceMatches(Page.AMSDashboard, returnedPage), 'Redirected to wrong page');
		}
	}

	@isTest static void testRedirect_NotLoggedInUser() {
        User guestUser = TestClassDataUtil.getAMSGuestUser();
        System.runAs(guestUser){
			Test.setCurrentPage(Page.AMSLanding);
			AMSGuestTemplateController controller = new AMSGuestTemplateController();
			PageReference returnedPage = controller.redirectIfLoggedIn();
			System.assert(TestClassDataUtil.checkPageReferenceMatches(null, returnedPage), 'Should not have redirected');
		}
	}

}