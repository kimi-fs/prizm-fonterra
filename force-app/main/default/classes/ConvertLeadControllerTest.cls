@IsTest
private class ConvertLeadControllerTest {

    @IsTest
    static void testConvert_noPhone() {
        Lead testLead2 = new Lead();
        testLead2.Company    = 'ConvertLead Class Test 2';
        testLead2.Salutation = 'Mr';
        testLead2.FirstName  = 'James';
        testLead2.MiddleName = 'Ronald';
        testLead2.LastName   = 'McDonald';
        testLead2.Email      = 'ConvertLeadTestC2@Test.com';
        insert testLead2;

        Lead testLead = [SELECT Id, isConverted, FirstName, Phone, Linked_Farm__c, Prospective_Farm_Id__c, Company, Supply_Region__c, Linked_Individual__c,
                        LastName, Farm_Type__c, Description, Farm_Role__c, RecordType.Name
                        FROM Lead WHERE Company = 'ConvertLead Class Test 2' LIMIT 1][0];

        ApexPages.StandardController sc = new ApexPages.StandardController(testLead);
        ConvertLeadController ext = new ConvertLeadController(sc);

        Test.startTest();
            ext.myLead = testLead;
            ext.convert();
        Test.stopTest();
    }

    @IsTest
    static void testConvert_AURecordType() {
        Contact testContact = TestClassDataUtil.createIndividualAU(true);
        Lead testLeadAURecordType = new Lead();
        testLeadAURecordType.Company    = 'ConvertLead Class Test AU';
        testLeadAURecordType.Linked_Individual__c = testContact.Id;
        testLeadAURecordType.Salutation = 'Mr';
        testLeadAURecordType.FirstName  = 'JamesAU';
        testLeadAURecordType.MiddleName = 'RonaldAU';
        testLeadAURecordType.LastName   = 'McDonaldAU';
        testLeadAURecordType.Phone      = '+64 21 333175';
        testLeadAURecordType.Email      = 'ConvertLeadTestCAU@Test.com';
        testLeadAURecordType.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Fonterra Lead AU'].Id;
        testLeadAURecordType.Specialty_Milk__c = 'A2';
        testLeadAURecordType.Specialty_Milk_Notes__c = 'test notes';
        testLeadAURecordType.Specialty_Status__c = 'Certified';
        testLeadAURecordType.Specialty_Sub_Status__c = 'DNA testing - Scheduled';
        insert testLeadAURecordType;

        Lead testLead = [SELECT Id, isConverted, FirstName, Phone, Linked_Farm__c, Prospective_Farm_Id__c, Company, Supply_Region__c, Linked_Individual__c,
                        LastName, Farm_Type__c, Description, Farm_Role__c, RecordType.Name, Current_Supplier__c, Competitor_Supply_Number__c, Specialty_Milk__c,
                        Specialty_Milk_Notes__c, Specialty_Status__c, Specialty_Sub_Status__c
                        FROM Lead WHERE Company = 'ConvertLead Class Test AU' LIMIT 1][0];

        ApexPages.StandardController sc = new ApexPages.StandardController(testLead);
        ConvertLeadController ext = new ConvertLeadController(sc);

        Test.startTest();
            ext.myLead = testLead;
            ext.convert();
        Test.stopTest();

        System.assert([SELECT count() FROM Opportunity WHERE RecordTypeId =: GlobalUtility.oppFonterraAURecordTypeId] > 0);
        System.assert([SELECT count() FROM Account WHERE RecordTypeId =: GlobalUtility.auAccountFarmRecordTypeId] > 0);

    }
}