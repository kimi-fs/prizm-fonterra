/**
 * Description: Test for AMSHerdAndHectaresController
 * @author: Amelia (Trineo)
 * @date: Septemeber 2017
 */
@isTest private class AMSHerdAndHectaresControllerTest {
    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup static void testSetup(){
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.individualDefaultAccountAU();
        SetupAMSCommunityPageMessages.createPageMessages();

        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        List<Reference_Region__c> auReferenceRegions = TestClassDataUtil.createAuReferenceRegions();
        insert auReferenceRegions;
        List<Account> farms = TestClassDataUtil.createAuFarms(auReferenceRegions);
        insert farms;
        List<Farm_Season__c> farmSeasons = new List<Farm_Season__c>();
        farmSeasons.addAll(TestClassDataUtil.createFarmSeasonAU(farms));

        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        TestClassDataUtil.createPrimaryAccessRelationships(communityUser.ContactId, party, farms);
    }

    @isTest static void testGetFarmSelectOptions(){
        List<SelectOption> farmSelectOptions;

        System.runAs(getCommunityUser()){
            AMSHerdAndHectaresController controller = new AMSHerdAndHectaresController();
            farmSelectOptions = controller.getFarmSelectOptions();
        }
        System.assertEquals([SELECT count() FROM Individual_Relationship__c WHERE RecordTypeId = :GlobalUtility.derivedRelationshipRecordTypeId]+1, farmSelectOptions.size(), 'Incorrect number of farm select options');
    }

    @isTest static void testSetSelectedFarmId(){
        List<SelectOption> seasonSelectOptions;
        Account selectedFarm = [SELECT Id FROM Account WHERE RecordType.DeveloperName = 'Farm_AU' LIMIT 1];

        System.runAs(getCommunityUser()){
            AMSHerdAndHectaresController controller = new AMSHerdAndHectaresController();
            controller.getFarmSelectOptions();
            controller.selectedFarmId = selectedFarm.Id;
            seasonSelectOptions = controller.getSeasonSelectOptions();
        }
        System.assertEquals([SELECT count() FROM Farm_Season__c WHERE Farm__c =: selectedFarm.Id]+1, seasonSelectOptions.size(), 'Incorrect number of season select options');
    }

    @isTest static void testSetSelectedSeasonId(){
        List<SelectOption> seasonSelectOptions;
        Individual_Relationship__c selectedFarm = [SELECT Id, Farm__c FROM Individual_Relationship__c WHERE Farm__r.RecordType.DeveloperName = 'Farm_AU' LIMIT 1];
        Farm_Season__c selectedSeason = [SELECT Id FROM Farm_Season__c WHERE Farm__c =: selectedFarm.Farm__c LIMIT 1];

        System.runAs(getCommunityUser()){
            AMSHerdAndHectaresController controller = new AMSHerdAndHectaresController();
            controller.getFarmSelectOptions();
            controller.selectedFarmId = selectedFarm.Farm__c;
            controller.getSeasonSelectOptions();
            controller.selectedFarmSeason = selectedSeason.Id;
            controller.queryFarmSeason();

            System.assertEquals(selectedSeason.Id, controller.selectedSeason.season.Id, 'Incorrect season selected');
        }
    }

    @isTest static void testSaveFarmSeason(){
        Integer PEAK_COWS = 100;
        Integer DAIRY_HECTARES = 200;

        List<SelectOption> seasonSelectOptions;
        Individual_Relationship__c selectedFarm = [SELECT Id, Farm__c FROM Individual_Relationship__c WHERE Farm__r.RecordType.DeveloperName = 'Farm_AU' LIMIT 1];
        Farm_Season__c selectedSeason = [SELECT Id FROM Farm_Season__c WHERE Farm__c =: selectedFarm.Farm__c LIMIT 1];

        System.runAs(getCommunityUser()){
            AMSHerdAndHectaresController controller = new AMSHerdAndHectaresController();
            controller.getFarmSelectOptions();
            controller.selectedFarmId = selectedFarm.Farm__c;
            controller.getSeasonSelectOptions();
            controller.selectedFarmSeason = selectedSeason.Id;
            controller.queryFarmSeason();

            controller.selectedSeason.cows = PEAK_COWS;
            controller.selectedSeason.hectares = DAIRY_HECTARES;

            controller.saveFarmSeason();

        }
        Farm_Season__c updadatedFarmSeason = [SELECT Id, Peak_Cows__c, Dairy_Hectares__c FROM Farm_Season__c WHERE Id = :selectedSeason.Id];
        System.assertEquals(PEAK_COWS, updadatedFarmSeason.Peak_Cows__c, 'Farm season not updated');
        System.assertEquals(DAIRY_HECTARES, updadatedFarmSeason.Dairy_Hectares__c, 'Farm season not updated');
    }

    private static User getCommunityUser() {
        return [SELECT Id FROM User WHERE Email = :USER_EMAIL ];
    }
}