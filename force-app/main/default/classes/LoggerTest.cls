/*------------------------------------------------------------
Author:         Dan Fowlie
Company:        Trineo
Description:    Test Class for Logger.cls
------------------------------------------------------------*/

@isTest
private class LoggerTest {

    @isTest static void test_method_one() {

        Test.startTest();
        Logger.log('Summary','Details',Logger.LogType.ERROR);
        Logger.saveLog();
        Test.stopTest();

        List<Log__c> ll = [Select Summary__c, Details__c, Type__c from Log__c];
        System.assertEquals(1,ll.size());

    }

}