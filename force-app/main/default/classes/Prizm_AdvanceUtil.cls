public class Prizm_AdvanceUtil {
    public static List<Advance__c> getAdvRecs(Set<Id> pAdvRecIds){
        List<Advance__c> advRecs = new List<Advance__c>();
        fsCore.DynamicQueryBuilder advQuery = fsCore.DynamicQueryFactory.createQuery(Schema.SObjectType.Advance__c.getName())
            .addFields()
            .addWhereConditionWithBind(1, 'Id', 'IN', 'pAdvRecIds');
        advRecs = (List<Advance__c>)Database.query(advQuery.getQueryString());
        return advRecs;
    }
}