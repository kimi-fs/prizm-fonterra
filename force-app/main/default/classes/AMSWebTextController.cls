/**
* Description: Controller getting webtext for the web site
* @author: Ed (Trineo)
* @date: July 2017
* @test: AMSWebTextControllerTest
*/
public with sharing class AMSWebTextController {
    public String webText { get; set; }
    public String webTextHeading { get; set; }

    /**
     * Search for the knowledge article with the title provided. It is important to
     * not display the contents of the title that was provided since it is unsanitised
     * user input, only use it for searching.
     */
    public String webTextTitle {
        get;
        set {
            if (value != null) {
                // Strictly speaking we don't need to escape the title, but will do so anyway
                // in the case of changes in the future.
                String title = String.escapeSingleQuotes(value);
                List<Knowledge__kav> matchingText = AMSKnowledgeService.webTextArticles(title);

                if (matchingText.size() > 0) {
                    this.webText = matchingText[0].Content__c;
                    this.webTextHeading = matchingText[0].Heading__c;
                } else {
                    this.webText = 'Unable to find WebTextArticle.';
                }
            }
        }
    }

    /**
     * Search for the knowledge article with the urlName provided. It is important to
     * not display the contents of the urlName that was provided since it is unsanitised
     * user input, only use it for searching.
     */
    public String webTextURLName {
        get;
        set {
            if (value != null) {
                // Strictly speaking we don't need to escape the webTextURL, but will do so anyway
                // in the case of changes in the future.
                String webTextURL = String.escapeSingleQuotes(value);
                List<Knowledge__kav> matchingText = AMSKnowledgeService.webTextArticlesByURLName(webTextURL);

                if (matchingText.size() > 0) {
                    this.webText = matchingText[0].Content__c;
                    this.webTextHeading = matchingText[0].Heading__c;
                } else {
                    this.webText = 'We can\'t find the article you\'re looking for.';
                }
            }
        }
    }

    public AMSWebTextController() {
        this.webText = 'undefined';
    }
}