/*
 * FIXME
 */
@IsTest
private with sharing class FormsContollerTest {

    @TestSetup
    private static void testSetup() {
        FormsTestDataFactory.createAccountData();
    }

    @IsTest
    private static void createMilkCoolingPurchaseIncentiveCaseTest() {
        String jsonString = FormsTestDataFactory.createMilkCoolingPurchaseIncentiveData();
        FormsController.createMilkCoolingPurchaseIncentiveCase(jsonString, null, null);
    }

    @IsTest
    private static void createABNUpdateCaseTest() {
        String jsonString = FormsTestDataFactory.createABNUpdateData();
        FormsController.createABNUpdateCase(jsonString, null, null);
    }

    @IsTest
    private static void createSupplierInformationDetailCaseTest() {
        String jsonString = FormsTestDataFactory.createSupplierInformationDetailData();
        FormsController.createSupplierInformationDetailCase(jsonString, null, null, null, null, null);
    }

    @IsTest
    private static void createBankingInformationCaseTest() {
        String jsonString = FormsTestDataFactory.createBankingInformationData();
        FormsController.createBankingInformationCase(jsonString, null, null);
    }
}