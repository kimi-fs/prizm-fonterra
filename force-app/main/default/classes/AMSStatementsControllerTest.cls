/**
 * Description: Test for AMSStatementsController
 * @author: Amelia (Trineo)
 * @date: August 2017
 */
@isTest
private class AMSStatementsControllerTest {
    private static String USER_EMAIL = 'amsUser@thisTest.com';

    private static void setupData(Boolean createStatementEntries) {
        TestClassDataUtil.integrationUserProfile();

        TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        Contact individual = [SELECT Id FROM Contact WHERE Email =: USER_EMAIL];

        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        List<Individual_Relationship__c> irels = new List<Individual_Relationship__c>();
        irels.add(new Individual_Relationship__c(Individual__c = individual.Id,
                                                Party__c = party.Id,
                                                RecordTypeId = GlobalUtility.individualPartyRecordTypeId,
                                                Role__c = 'Owner'));
        List<Account> farms = TestClassDataUtil.createAUFarms(1, true);

        TestClassDataUtil.createPrimaryAccessRelationships(individual.Id, party, farms);

        List<Statement__c> statements = TestClassDataUtil.createStatements(party, farms, Date.today());

        if (createStatementEntries) {
            TestClassDataUtil.createStatementEntries(statements[0]);
        }
    }

    @isTest static void testConstructor() {
        setupData(true);

        System.runAs(getCommunityUser()) {
            AMSStatementsController controller = new AMSStatementsController();
            System.assertNotEquals(null, controller.accessableERelMap, 'Controller did not initalise correctly');
        }
    }

    @isTest static void testGetPartyOptions() {
        setupData(true);

        System.runAs(getCommunityUser()) {
            AMSStatementsController controller = new AMSStatementsController();
            List<SelectOption> partyOptions = controller.getPartyOptions();

            System.assertEquals(1, partyOptions.size(), 'Wrong number of party options');
        }
    }

    @isTest static void testGetFarmOptions() {
        setupData(true);

        System.runAs(getCommunityUser()) {
            AMSStatementsController controller = new AMSStatementsController();
            controller.partySelection = controller.getPartyOptions()[0].getValue();
            List<SelectOption> farmOptions = controller.getFarmOptions();

            System.assertEquals(1, farmOptions.size(), 'Wrong number of farm options');
        }
    }

    @isTest static void testGetSelectedFarmAndPartyName() {
        setupData(true);

        System.runAs(getCommunityUser()) {
            AMSStatementsController controller = new AMSStatementsController();
            String partySelectionId = new List<String>(controller.getPartyIdERelMap().keyset())[0];
            controller.partySelection = partySelectionId;
            controller.eRelSelection = controller.getPartyIdERelMap().get(partySelectionId)[0].Id;

            System.assertEquals(controller.getPartyIdERelMap().get(partySelectionId)[0].Party__r.Name, controller.getSelectedPartyName(), 'Wrong Party name');
            System.assertEquals(controller.getPartyIdERelMap().get(partySelectionId)[0].Farm__r.Name, controller.getSelectedFarmName(), 'Wrong Farm name');
        }
    }

    @isTest static void testGetSelectedFarmAndPartyName_AutoSelected() {
        setupData(true);

        System.runAs(getCommunityUser()) {
            AMSStatementsController controller = new AMSStatementsController();

            System.assertNotEquals(null, controller.getSelectedPartyName(), 'Wrong Party name');
            System.assertNotEquals(null, controller.getSelectedFarmName(), 'Wrong Farm name');
        }
    }

    @isTest static void testGetIncomeLessCharges() {
        setupData(true);

        Statement_Entry__c incomeLessCharges = [SELECT ID FROM Statement_Entry__c WHERE Type__c = 'Income Less Charges' LIMIT 1];
        System.runAs(getCommunityUser()) {
            AMSStatementsController controller = new AMSStatementsController();
            setControllerOptions(controller);
            controller.generateReport();

            System.assertEquals(incomeLessCharges.Id, controller.getIncomeLessCharges().statementEntry.Id, 'Wrong income less charges record');
        }
    }

    @isTest static void testGetNetIncome() {
        setupData(true);

        Statement_Entry__c netIncome = [SELECT ID FROM Statement_Entry__c WHERE Type__c = 'Net Income/Payable' LIMIT 1];
        System.runAs(getCommunityUser()) {
            AMSStatementsController controller = new AMSStatementsController();
            setControllerOptions(controller);
            controller.generateReport();

            System.assertEquals(netIncome.Id, controller.getNetIncome().statementEntry.Id, 'Wrong net income record');
        }
    }

    @isTest static void testGetIncomeLessCharges_NoValue() {
        setupData(false);

        System.runAs(getCommunityUser()) {
            AMSStatementsController controller = new AMSStatementsController();
            setControllerOptions(controller);
            controller.generateReport();

            System.assertEquals(null, controller.getIncomeLessCharges());
        }
    }

    @isTest static void testGetNetIncome_NoValue() {
        setupData(false);

        System.runAs(getCommunityUser()) {
            AMSStatementsController controller = new AMSStatementsController();
            setControllerOptions(controller);
            controller.generateReport();

            System.assertEquals(null, controller.getNetIncome());
        }
    }

    @isTest static void testGetStatementExists_ExistingStatements() {
        setupData(true);

        System.runAs(getCommunityUser()) {
            AMSStatementsController controller = new AMSStatementsController();
            setControllerOptions(controller);
            controller.generateReport();

            System.assertEquals(true, controller.getStatementExists(), 'Wrong statement exists value');
        }
    }

    @isTest static void testGetStatementExists_NoExistingStatements() {
        setupData(true);

        delete[SELECT Id FROM Statement__c];
        System.runAs(getCommunityUser()) {
            AMSStatementsController controller = new AMSStatementsController();
            setControllerOptions(controller);
            controller.generateReport();

            System.assertEquals(false, controller.getStatementExists(), 'Wrong statement exists value');
        }
    }

    @isTest static void testGetRatesFromWrapper_CentsValue() {
        Statement_Entry__c se = new Statement_Entry__c(Rate__c = 123.4567);
        AMSStatementsController.StatementEntryWrapper wrapper = new AMSStatementsController.StatementEntryWrapper(se);

        System.assertEquals('$123.46', wrapper.getRates(), 'Rate value not calcuated correctly');
    }

    @isTest static void testGetRatesFromWrapper_DollarValue() {
        Statement_Entry__c se = new Statement_Entry__c(Rate__c = 0.1234567);
        AMSStatementsController.StatementEntryWrapper wrapper = new AMSStatementsController.StatementEntryWrapper(se);

        System.assertEquals('12.3456700c', wrapper.getRates(), 'Rate value not calcuated correctly');
    }

    private static User getCommunityUser() {
        User theUser = [SELECT Id FROM User WHERE Email = :USER_EMAIL ];
        return theUser;
    }

    private static void setControllerOptions(AMSStatementsController controller){
        String partySelectionId = new List<String>(controller.getPartyIdERelMap().keyset())[0];
        controller.partySelection = partySelectionId;
        controller.eRelSelection = controller.getPartyIdERelMap().get(partySelectionId)[0].Id;
        controller.monthSelection = String.valueOf(Date.today().month());
        controller.yearSelection = String.valueOf(Date.today().year());
    }
}