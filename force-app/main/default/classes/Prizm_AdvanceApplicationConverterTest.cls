/* 
* Class Name   - Prizm_AdvanceApplicationConverterTest
*
* Description  - Test Class for Prizm_AdvanceApplicationConverter
*
* Developer(s) - Financial Spectra
*/

@isTest
public class Prizm_AdvanceApplicationConverterTest {

    @testSetup
	static void createData() {
        
        Map<String, fsCore__Branch_Setup__c> codeToBranchesMap = Prizm_TestDataHelper.createMandatorySetup();
        System.assert(codeToBranchesMap.get('EST')!=null);
        System.assert(codeToBranchesMap.get('NTH')!=null);
        System.assert(codeToBranchesMap.get('STH')!=null);
        System.assert(codeToBranchesMap.get('WST')!=null);
        
        Set<String> balancesCodeSet = new Set<String>{'INTEREST_BALANCE','PRINCIPAL_BALANCE','NOT_APPLICABLE'};
        Map<String, fsCore__Balance_Setup__c> codeToBalancesMap = Prizm_TestDataHelper.createTestBalances(balancesCodeSet);
        System.assert(codeToBalancesMap.get('PRINCIPAL_BALANCE')!=null);
        
        
        Set<String> transactionCodeSet = new Set<String>{'RATE_CHANGED','CONTRACT_ACTIVATED','FND_LOAN_PAYOFF','FND_CASH_ADVANCE'};
        Map<String, fsCore__Transaction_Setup__c> codeToTransactionsMap = Prizm_TestDataHelper.createTestTransactions(transactionCodeSet, codeToBalancesMap);
        System.assert(codeToTransactionsMap.get('CONTRACT_ACTIVATED').id!=null);
        System.assert(codeToTransactionsMap.get('RATE_CHANGED').id!=null);
        
        
        Set<String> itemizationsCodeSet = new Set<String>{'FUNDS_TO_CUSTOMER','LOAN_PAYOFF_PAYMENT'};
        Map<String, fsCore__Itemization_Setup__c> codeToItemizationsMap = Prizm_TestDataHelper.createTestItemization(itemizationsCodeSet,codeToTransactionsMap);
        System.debug(loggingLevel.ERROR, 'codeToItemizationsMap :'+codeToItemizationsMap);
        System.assert(codeToItemizationsMap.get('FUNDS_TO_CUSTOMER')!=null);
        System.assert(codeToItemizationsMap.get('LOAN_PAYOFF_PAYMENT')!=null);
       
        
        List<fsCore__Payment_Allocation_Method_Setup__c> paymentAllocationList = Prizm_TestDataHelper.createPaymentAllocationSetUp();
        if(paymentAllocationList.size()>0)
        {
            insert paymentAllocationList;
        }
        System.assert(paymentAllocationList.size()>0,'Payment Allocation : '+paymentAllocationList);
        
        Set<String> pricingCodeSet = new Set<String>{'RLB','IFL'};
        Map<String, fsCore__Pricing_Setup__c> codeToPricingsMap = Prizm_TestDataHelper.createTestPricings(pricingCodeSet);
        System.debug(loggingLevel.ERROR, 'codeToPricingsMap :'+codeToPricingsMap);
        System.assert(codeToPricingsMap.get('RLB')!=null);
        System.assert(codeToPricingsMap.get('IFL')!=null);
 
        Set<String> indexCodeSet = new Set<String>{'RABO_LOAN_RATE','INTEREST_FREE_LOAN_RATE'};
        Map<String, fsCore__Index_Setup__c> codeToIndexSetUpMap = Prizm_TestDataHelper.createTestIndexSetUps(indexCodeSet);
        System.debug(loggingLevel.ERROR, 'codeToIndexSetUpMap :'+codeToIndexSetUpMap);
        System.assert(codeToIndexSetUpMap.get('RABO_LOAN_RATE').id!= null);
        System.assert(codeToIndexSetUpMap.get('INTEREST_FREE_LOAN_RATE').id!= null);
        
        List<fsCore__Index_Rate_Setup__c> testIndexRateList = Prizm_TestDataHelper.createIndexRateSetUp(codeToIndexSetUpMap);
        if(testIndexRateList.size()>0)
        {
            insert testIndexRateList;
        }
        System.assert(testIndexRateList.size()>0,'Index Rate created assert');
        
        List<fsCore__Index_Setup__c> indexSetUpToUpdate = new List<fsCore__Index_Setup__c>();
        for(String indexSetUp : codeToIndexSetUpMap.keySet())
        {
            fsCore__Index_Setup__c indexToUpdate= codeToIndexSetUpMap.get(indexSetUp);
            indexToUpdate.fsCore__Is_Active__c = true;
            indexSetUpToUpdate.add(indexToUpdate);
        }
        update indexSetUpToUpdate;
        
        Set<String> contractTemplateCodeSet = new Set<String>{'RLCT','IFLCT'};
        Map<String, fsCore__Contract_Template_Setup__c> codeToContractTemplateMap = Prizm_TestDataHelper.createTestContractTemplate(contractTemplateCodeSet, paymentAllocationList[0].id, codeToIndexSetUpMap);
        System.debug(loggingLevel.ERROR, 'codeToContractTemplateMap :'+codeToContractTemplateMap);
        System.assert(codeToContractTemplateMap.get('RLCT')!=null);
        System.assert(codeToContractTemplateMap.get('IFLCT')!=null);         
       
        Set<String> productCodeSet = new Set<String>{'RB','IFL'};
        Map<String, fsCore__Product_Setup__c> codeToProductsMap = Prizm_TestDataHelper.createTestProducts(productCodeSet, codeToContractTemplateMap,  codeToIndexSetUpMap, codeToPricingsMap);
        System.debug(loggingLevel.ERROR, 'codeToProductsMap :'+codeToProductsMap);
        System.assert(codeToProductsMap.get('RB')!=null);
        System.assert(codeToProductsMap.get('IFL')!=null);
        
        List<fsCore__Product_Itemization_Setup__c> testProductItemizationsList = Prizm_TestDataHelper.createProductItemizationsSetUp(codeToProductsMap, codeToItemizationsMap);
        if(testProductItemizationsList.size()>0)
        {
            insert testProductItemizationsList;
        }
        System.assert(testProductItemizationsList.size()>0,'Product Itemizations Set Up created assert');
            
        List<fsCore__Product_Mapping_Setup__c> testProductMappingSetUpList = Prizm_TestDataHelper.createProductMappingSetUp(codeToProductsMap,codeToContractTemplateMap);
        if(testProductMappingSetUpList.size()>0)
        {
         insert  testProductMappingSetUpList;
        }
        System.assert(testProductMappingSetUpList.size()>0,'Product Mapping Set Up created assert');
       
        fsCore.SeedCustomSettings.createCustomSettings(new Set<String>{fsCore.Constants.CUSTOM_SETTING_CUSTOM_NUMBER_FORMAT});
        
        List<fsCore__Stage_Setup__c> testStagesList = Prizm_TestDataHelper.createStageSetUp();
        if(testStagesList.size()>0)
        {
            insert testStagesList;
        }
        System.assert(testStagesList.size()>0,'Stages created assert');
        
        fsCore__Process_Action_Setup__c testProcessAction = fsCore.TestHelperWorkflow.getTestProcessAction('AUTO_APPLY_CONTRACT_TEMPLATE', 'Origination', 
                                                                                                           'fscore__lending_application__c', 'ExecuteApex', 
                                                                                                           'ContractTemplateAutoApplyAction');
 		insert testProcessAction;
        
        fsCore__Stage_Process_Action_Setup__c testStageProcessAction = fsCore.TestHelperWorkflow.getTestStageProcessAction(testStagesList[0].id, testProcessAction.id, 1);
		testStageProcessAction.fsCore__Is_Auto_Execute_On_Stage_Entry__c = true;
        testStageProcessAction.fsCore__Execution_Time__c = 'Entry';
        testStageProcessAction.fsCore__Execute_For_Direction__c = 'Forward';
        insert testStageProcessAction; 
           
       
        Account raboFarmAccount = Prizm_TestDataHelper.farmAccount();
        Account raboPartyAccount = Prizm_TestDataHelper.partyAccount();
        
        insert raboFarmAccount;
        System.assert(raboFarmAccount.id!=null, 'RABO Farm Account created assert'); 
        
        insert raboPartyAccount;
        System.assert(raboPartyAccount.id!=null, 'RABO Party Account created assert'); 
       
        Contact raboIndividualContact = Prizm_TestDataHelper.createFarmIndividual(raboFarmAccount);
        insert raboIndividualContact;
        System.assert(raboIndividualContact.id!=null, 'RABO Contact created assert'); 
        
        raboFarmAccount.Primary_On_Farm_Contact__c = raboIndividualContact.id;
        update raboFarmAccount;
        System.assert(raboFarmAccount.Primary_On_Farm_Contact__c!=null, 'RABO Primary contact populated on RABO farm account assert'); 
        
        Case raboTestCase = Prizm_TestDataHelper.createCase(raboFarmAccount,raboPartyAccount);
        insert raboTestCase;
        System.assert(raboTestCase.id!=null, 'RABO case created assert');
        
       
        Advance_Application__c raboAdvanceApplication = Prizm_TestDataHelper.createAdvanceApplication(raboTestCase);
        raboAdvanceApplication.Type__c = 'New RABO Advance';
        insert raboAdvanceApplication;
        System.assert(raboAdvanceApplication.id!=null, 'RABO test advance application created assert');
        System.debug(raboAdvanceApplication+' RABO Advance Application');
        
       
     
        /* Second advance application with Interest Free Loan Type*/
        
        Account interestFreefarmAccount = Prizm_TestDataHelper.farmAccount();
      
        insert interestFreefarmAccount;
        System.assert(interestFreefarmAccount.id!=null, 'Interest Free Farm Account created assert'); 
        
        Account interestFreePartyAccount = Prizm_TestDataHelper.partyAccount();
        insert interestFreePartyAccount;
        System.assert(interestFreePartyAccount.id!=null,' Interest Free Party Account created assert');
        
        Contact interestFreeindividualContact = Prizm_TestDataHelper.createIndividual(interestFreefarmAccount);
        insert interestFreeindividualContact;
        System.assert(interestFreeindividualContact.id!=null, 'Interest Free Contact created assert'); 
        
        interestFreefarmAccount.Primary_On_Farm_Contact__c = interestFreeindividualContact.id;
        update interestFreefarmAccount;
        System.assert(interestFreefarmAccount.Primary_On_Farm_Contact__c!=null, 'Interest Free Primary contact populated on Interest Free farm account assert'); 
        
        
        Case interestFreetestCase = Prizm_TestDataHelper.createCase(interestFreefarmAccount,interestFreePartyAccount);
        insert interestFreetestCase;
        
        System.assert(interestFreetestCase.id!=null,'Interest Free Case created assert');
        System.debug(interestFreetestCase+'Interest Free Case');
       
        
        Advance_Application__c interestFreeAdvanceApplication = Prizm_TestDataHelper.createAdvanceApplication(interestFreetestCase);
        interestFreeAdvanceApplication.Finance_Purpose__c = 'Work';
        interestFreeAdvanceApplication.Trading_Arrangement__c = 'Trust';
        interestFreeAdvanceApplication.Interest_Free__c = true;
        interestFreeAdvanceApplication.Type__c = 'New RABO Advance';
        interestFreeAdvanceApplication.Finance_Required__c = 15000;
        interestFreeAdvanceApplication.February__c = 5000;
        interestFreeAdvanceApplication.Current_RABO_Advances__c = 15000;
        interestFreeAdvanceApplication.Proposed_new_RABO_Advance__c = 15000;
        interestFreeAdvanceApplication.Proposed_RABO_Restructured_Advance_Amt__c = 15000;
       
        insert interestFreeAdvanceApplication;
        System.assert(interestFreeAdvanceApplication.id!=null, 'Interest Free advance application created assert');
       
        System.debug(interestFreeAdvanceApplication+' Interest Free Advance Application');
        
    }
        
          
    
    @isTest
    public static void testConvertClientApp(){
        Advance_Application__c advanceApplication = 
                                           [Select Id
                                            , Name
                                            , Farm_Account_Id__c
                                            from Advance_Application__c
                                            where Finance_Purpose__c='Testing' and Trading_Arrangement__c = 'Company'];
        
        System.assert(advanceApplication!= null,' First Advance application created assert');
        
        Test.startTest();
        String resultMessage = Prizm_ApplicationConvertController.convertAdvanceApptoLendingApp(advanceApplication.id);
        Test.stopTest();
        
        System.assert(resultMessage != null);
        System.debug(loggingLevel.ERROR, 'Result : '+resultMessage);
    
        fsCore__Branch_Setup__c testBranch =
                                        [Select Id
                                        , Name 
                                        , fsCore__Company_Name__c 
                                        from fsCore__Branch_Setup__c 
                                        where fsCore__Branch_Code__c = 'EST'  ];
        
        fsCore__Product_Setup__c testProduct = 
                                         [Select id 
                                         from fsCore__Product_Setup__c where fsCore__Product_Code__c= 'RB'];
        
        fsCore__Contract_Template_Setup__c testContractTemplate =
                                         [Select id
                                         from fsCore__Contract_Template_Setup__c where fsCore__Contract_Template_Code__c='RLCT'];
        
       
         fsCore__Lending_Application__c testApplication = 
                                                         [Select id,fsCore__Primary_Customer_Account__c,
                                                          fsCore__Primary_Customer_Contact__c,
                                                          Farm__c,
                                                          Party__c,
                                                          Advance_Application__c,
                                                          fsCore__Company_Name__c,
                                                          fsCore__Branch_Name__c,
                                                          fsCore__Product_Name__c,
                                                          fsCore__Contract_Template_Name__c
                                                          from fsCore__Lending_Application__c limit 1];
        
         fsCore__Lending_Application_Itemization__c itemizations = 
                                                                   [Select Id,
                                                                   fsCore__Requested_Amount_Unsigned__c,
                                                                   fsCore__Approved_Amount_Unsigned__c 
                                                                   from  fsCore__Lending_Application_Itemization__c where fsCore__Lending_Application_Number__c =: testApplication.id];
       
        List<fsCore__Lending_Application_Repayment_Schedule__c> repaymentScheduleList = 
                                                                                    [Select Id,
                                                                                    fsCore__Start_Payment_Number__c,
                                                                                    fsCore__Lending_Application_Number__c,
                                                                                    fsCore__Fixed_Principal_Amount__c 
                                                                                    from fsCore__Lending_Application_Repayment_Schedule__c where fsCore__Lending_Application_Number__c =: testApplication.id];
        System.debug(loggingLevel.ERROR, 'Repayment Schedule List : '+repaymentScheduleList);
        
        System.debug(loggingLevel.Error,'Test Application :--'+testApplication);
        
        System.assertEquals(testApplication.fsCore__Company_Name__c, testBranch.fsCore__Company_Name__c,'Company linked with application assert');
        System.assertEquals(testApplication.fsCore__Branch_Name__c, testBranch.id, 'Branch linked with application assert');
        System.assertEquals(testApplication.fsCore__Product_Name__c, testProduct.id, 'Product linked with application assert');
        System.assertEquals(testApplication.fsCore__Contract_Template_Name__c, testContractTemplate.id, 'Contract Template linked with application assert');
        System.assertEquals(testApplication.Advance_Application__c, advanceApplication.id, 'Advance application mapped with Lending application assert');
        System.assertEquals(testApplication.fsCore__Primary_Customer_Account__c, advanceApplication.Farm_Account_Id__c, 'Account linked with application assert');
        System.assertEquals(10000,itemizations.fsCore__Requested_Amount_Unsigned__c, 'Itemization amount assert');
        System.assertEquals(10000, itemizations.fsCore__Approved_Amount_Unsigned__c, 'Itemization approved amount assert');
        System.assertEquals(repaymentScheduleList[0].fsCore__Lending_Application_Number__c, testApplication.id, 'Repayment schedule belongs to the inserted application');
        System.assertEquals(null,repaymentScheduleList[0].fsCore__Fixed_Principal_Amount__c, '1st Repayment Schedule Prinicipal amount inserted assert');

       }
    
    
    
    //This test method is for Interest Free Loan Conversion
    @isTest
    public static void testConvertClientAppOne(){
        Advance_Application__c advanceApplication =
                                                    [Select Id, Name
                                                     , Farm_Account_Id__c
                                                     , Interest_Free__c
                                                     , Product_Code__c 
                                                     from Advance_Application__c 
                                                     where Finance_Purpose__c='Work' 
                                                     and Trading_Arrangement__c = 'Trust'
                                                     and Interest_Free__c = true];
       
        System.assert(advanceApplication!= null,' Second Advance application created assert');
        
        system.debug(loggingLevel.ERROR, advanceApplication);
        
        Test.startTest();
        String resultMessage = Prizm_ApplicationConvertController.convertAdvanceApptoLendingApp(advanceApplication.id);
        Test.stopTest();
        System.assert(resultMessage != null);
        
         fsCore__Branch_Setup__c testBranch =
                                              [Select Id, Name, 
                                              fsCore__Company_Name__c 
                                              from fsCore__Branch_Setup__c
                                              where fsCore__Branch_Code__c = 'EST'];
        
        fsCore__Product_Setup__c testProduct = 
                                            [Select id 
                                             from fsCore__Product_Setup__c 
                                             where fsCore__Product_Code__c= 'IFL'];
        
        fsCore__Contract_Template_Setup__c testContractTemplate = 
                                            [Select id
                                             , Name 
                                             from fsCore__Contract_Template_Setup__c
                                             where fsCore__Contract_Template_Code__c = 'IFLCT'];
        
        system.debug(loggingLevel.ERROR, testContractTemplate);
       
        fsCore__Lending_Application__c testApplication = 
                                                         [Select id,fsCore__Primary_Customer_Account__c,
                                                          fsCore__Primary_Customer_Contact__c,
                                                          Farm__c,
                                                          Party__c,
                                                          Advance_Application__c,
                                                          fsCore__Company_Name__c,
                                                          fsCore__Branch_Name__c,
                                                          fsCore__Product_Name__c,
                                                          fsCore__Contract_Template_Name__c,
                                                          fsCore__Contract_Template_Name__r.Name,
                                                          fsCore__Product_Name_Formula__c
                                                          from fsCore__Lending_Application__c
                                                          where Advance_Application__c = :advanceApplication.Id];
        
        system.debug(loggingLevel.ERROR, testApplication.fsCore__Contract_Template_Name__r.Name);
		system.debug(loggingLevel.ERROR, testApplication.fsCore__Product_Name__c);
        system.debug(loggingLevel.ERROR, testApplication.fsCore__Product_Name_Formula__c);
        
        fsCore__Lending_Application_Itemization__c itemizations = 
                                                                   [Select Id,
                                                                   fsCore__Requested_Amount_Unsigned__c,
                                                                   fsCore__Approved_Amount_Unsigned__c 
                                                                   from fsCore__Lending_Application_Itemization__c 
                                                                   where fsCore__Lending_Application_Number__c =: testApplication.id];
    
        List<fsCore__Lending_Application_Repayment_Schedule__c> repaymentScheduleList = 
                                                                                    [Select Id,
                                                                                    fsCore__Start_Payment_Number__c,
                                                                                    fsCore__Lending_Application_Number__c,
                                                                                    fsCore__Fixed_Principal_Amount__c
                                                                                    from fsCore__Lending_Application_Repayment_Schedule__c 
                                                                                    where fsCore__Lending_Application_Number__c =: testApplication.id];
        
        System.debug(loggingLevel.ERROR, 'Repayment Schedule List: '+repaymentScheduleList);
        
        System.debug(loggingLevel.Error,'Test Application :--'+testApplication);
        
        System.debug(loggingLevel.Error,'Test Application :--'+testApplication);
        System.assertEquals(testApplication.fsCore__Company_Name__c, testBranch.fsCore__Company_Name__c,'Company not linked to created Application.');
        System.assertEquals(testApplication.fsCore__Branch_Name__c, testBranch.id, 'Branch not linked to created Application.');
        System.assertEquals(testApplication.fsCore__Product_Name__c, testProduct.id, 'Product not linked to created Application.');
        System.assertEquals(testApplication.fsCore__Contract_Template_Name__c, testContractTemplate.id, 'Contract template not linked to created Application.');
        System.assertEquals(testApplication.Advance_Application__c, advanceApplication.id, 'Advance application not linked to created Application.');
        System.assertEquals(testApplication.fsCore__Primary_Customer_Account__c, advanceApplication.Farm_Account_Id__c, 'Account not linked to created Application.');
        System.assertEquals(15000,itemizations.fsCore__Requested_Amount_Unsigned__c, 'Itemization amount not populated correctly on created Application.');
        System.assertEquals(15000, itemizations.fsCore__Approved_Amount_Unsigned__c, 'Itemization approved amount not populated correctly on created Application.');
        System.assertEquals(repaymentScheduleList[0].fsCore__Lending_Application_Number__c, testApplication.id, 'Created Repayment schedule does not belongs to the inserted Application.');
        //System.assertEquals(null,repaymentSchedule[0].fsCore__Fixed_Principal_Amount__c, '1st Repayment Schedule Prinicipal amount inserted assert');
        

     }
    
    
    
    
    @isTest
    public static void testConvertClientAppTwo(){
        Advance_Application__c advanceApplication = 
                                         [Select Id
                                          , Name
                                          , Farm_Account_Id__c 
                                          from Advance_Application__c 
                                          where Finance_Purpose__c='Testing' and Trading_Arrangement__c = 'Company'];
        
        System.assert(advanceApplication!= null,' First Advance application created assert');
        
        Test.startTest();
        String result = Prizm_ApplicationConvertController.convertAdvanceApptoLendingApp(advanceApplication.id);
        String resultMessage = Prizm_ApplicationConvertController.convertAdvanceApptoLendingApp(advanceApplication.id);

        Test.stopTest();
        
        System.debug(result +'Message after conversion');
        System.assert(String.isEmpty(result)==false,'Result message assert');
        
        System.assert(String.isEmpty(resultMessage)==false,'Result message assert One');
        System.debug(resultMessage +'Message after conversion of 2nd Application');
        
      
        fsCore__Branch_Setup__c testBranch =
                                          [Select Id
                                           , Name
                                           , fsCore__Company_Name__c 
                                           from fsCore__Branch_Setup__c
                                           where fsCore__Branch_Code__c = 'EST' Limit 1];
        
        fsCore__Product_Setup__c testProduct = 
                                          [Select id
                                          from fsCore__Product_Setup__c
                                          where fsCore__Product_Code__c= 'RB'];
        
        fsCore__Contract_Template_Setup__c testContractTemplate = 
                                          [Select id 
                                          from fsCore__Contract_Template_Setup__c 
                                          where fsCore__Contract_Template_Code__c='RLCT'];
        
       
         fsCore__Lending_Application__c testApplication = 
                                                          [Select id,fsCore__Primary_Customer_Account__c,
                                                          fsCore__Primary_Customer_Contact__c,
                                                          Farm__c,
                                                          Party__c,
                                                          Advance_Application__c,
                                                          fsCore__Company_Name__c,
                                                          fsCore__Branch_Name__c,
                                                          fsCore__Product_Name__c,
                                                          fsCore__Contract_Template_Name__c
                                                          from fsCore__Lending_Application__c limit 1];
        
         fsCore__Lending_Application_Itemization__c itemizations = 
                                                                   [Select Id,
                                                                   fsCore__Requested_Amount_Unsigned__c,
                                                                   fsCore__Approved_Amount_Unsigned__c 
                                                                   from  fsCore__Lending_Application_Itemization__c
                                                                   where fsCore__Lending_Application_Number__c =: testApplication.id];
       
        
         List<fsCore__Lending_Application_Repayment_Schedule__c> repaymentSchedule =
                                                                                    [Select Id,
                                                                                    fsCore__Start_Payment_Number__c,
                                                                                    fsCore__Lending_Application_Number__c,
                                                                                    fsCore__Fixed_Principal_Amount__c
                                                                                    from fsCore__Lending_Application_Repayment_Schedule__c
                                                                                    where fsCore__Lending_Application_Number__c =: testApplication.id];
        

        System.debug(loggingLevel.Error,'Test Application :--'+testApplication);
        System.assertEquals(testApplication.fsCore__Company_Name__c, testBranch.fsCore__Company_Name__c,'Company is linked');
        System.assertEquals(testApplication.fsCore__Branch_Name__c, testBranch.id, 'Branch is linked');
        System.assertEquals(testApplication.fsCore__Product_Name__c, testProduct.id, 'Product is linked');
        System.assertEquals(testApplication.fsCore__Contract_Template_Name__c, testContractTemplate.id, 'Contract is linked');
        System.assertEquals(testApplication.Advance_Application__c, advanceApplication.id, 'Advance application is mapped to application');
        System.assertEquals(testApplication.fsCore__Primary_Customer_Account__c, advanceApplication.Farm_Account_Id__c, 'Account is linked');
        System.assertEquals(10000,itemizations.fsCore__Requested_Amount_Unsigned__c, 'Itemization amount assert');
        System.assertEquals(10000, itemizations.fsCore__Approved_Amount_Unsigned__c, 'Itemization approved amount assert');
        System.assertEquals(repaymentSchedule[0].fsCore__Lending_Application_Number__c, testApplication.id, 'Repayment schedule belongs to the inserted application');
        System.assertEquals(null,repaymentSchedule[0].fsCore__Fixed_Principal_Amount__c, '1st Repayment Schedule Prinicipal amount inserted assert');
     
       
       }
   
}