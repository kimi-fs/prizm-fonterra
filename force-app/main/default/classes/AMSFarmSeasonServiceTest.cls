/**
 * Description: Test class for AMSFarmSeasonService
 * @author: Amelia (Trineo)
 * @date: September 2017
 */
@isTest
private class AMSFarmSeasonServiceTest {

    @testSetup static void testSetup(){
        List<Account> farms = TestClassDataUtil.createAUFarms(5, true);

        // 4 Periods, current and 3 forwards and seasons for each farm
        List<Reference_Period__c> referencePeriods = TestClassDataUtil.createReferencePeriods(4, GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU, true);
        List<Farm_Season__c> farmSeasons = TestClassDataUtil.createFarmSeasons(farms, referencePeriods, true);
    }

    @isTest static void testGetFarmSeasons() {
        List<Farm_Season__c> farmSeasons = [SELECT Id, Farm__c FROM Farm_Season__c];

        Set<Id> farmIds = new Set<Id>();
        for(Farm_Season__c fs : farmSeasons){
            farmIds.add(fs.Farm__c);
        }

        List<Farm_Season__c> returnedSeasons = AMSFarmSeasonService.getFarmSeasons(farmIds);

        System.assertEquals(farmSeasons.size(), returnedSeasons.size(), 'Wrong number of returned farm seasons');

    }

    @isTest static void testgetFarmSeasonsForwardsBackwards() {
        List<Account> farms = [SELECT Id FROM Account WHERE RecordTypeID = :GlobalUtility.auAccountFarmRecordTypeId];

        List<Farm_Season__c> returnedSeasons;

        returnedSeasons = AMSFarmSeasonService.getCurrentFarmSeasonForwards(farms[0].Id, 1);
        System.assertEquals(1, returnedSeasons.size());
        returnedSeasons = AMSFarmSeasonService.getCurrentFarmSeasonForwards(farms[0].Id, 2);
        System.assertEquals(2, returnedSeasons.size());
        returnedSeasons = AMSFarmSeasonService.getCurrentFarmSeasonForwards(farms[0].Id, 3);
        System.assertEquals(3, returnedSeasons.size());
        returnedSeasons = AMSFarmSeasonService.getCurrentFarmSeasonForwards(farms[0].Id, 4);
        System.assertEquals(4, returnedSeasons.size());
        returnedSeasons = AMSFarmSeasonService.getCurrentFarmSeasonForwards(farms[0].Id, 5);
        System.assertEquals(4, returnedSeasons.size());

        returnedSeasons = AMSFarmSeasonService.getCurrentFarmSeasonBackwards(farms[0].Id, 1);
        System.assertEquals(1, returnedSeasons.size());
        returnedSeasons = AMSFarmSeasonService.getCurrentFarmSeasonBackwards(farms[0].Id, 2);
        System.assertEquals(1, returnedSeasons.size());
        returnedSeasons = AMSFarmSeasonService.getCurrentFarmSeasonBackwards(farms[0].Id, 3);
        System.assertEquals(1, returnedSeasons.size());
    }
}