/**
 * Static methods to extend standard SOQL query capabilities.
 *
 * Return query results as list of sObjects.
 * Transform query string if it is in format "Select * From ..."
 *
 * LIMITATIONS:
 * - Doesnt currently work if there are brackets anywhere other than the nested query
 * - Use getFieldsFromObjectForQuery() as a workaround eg.
 * - 'SELECT ' + getFieldsFromObjectForQuery('Channel_Preference_Setting__c') + ' FROM Channel_Preference_Setting__c WHERE (Name = 'name' OR Name = 'othername')'
 *
 * Example query strings:
 * - SELECT * FROM Contact
 * - SELECT *, Name, Email FROM Contact WHERE Name = \'Bob\'
 * - SELECT *, Channel_Preference_Setting__r.* FROM Channel_Preference__c
 * - SELECT *, (SELECT * FROM Channel_Preference__r) FROM Channel_Preference_Setting__r
 * - 'SELECT ' + getFieldsFromObjectForQuery('Channel_Preference_Setting__c') + ' FROM Channel_Preference_Setting__c'
 *
 * @author: Amelia (Trineo)
 * @date: March 2018
 */
public class QueryUtil {
    public class QueryException extends Exception {}

    public static List<sObject> query(String query) {
        List<sObject> result = new List<sObject>();
        System.debug('About to transform query string: ' + query);
        String queryStr = getTransformedQueryStr(query, null);
        System.debug('About to perform query: ' + queryStr);
        result = Database.query(queryStr);
        System.debug('Returning ' + result.size() + ' result(s)...');
        return result;
    }

    /*
    * Given a query string
    * - replace * with comma-delimited list of all fields.
    * - replace __r.* with comma-delimited list of all related fields.
    * - if the field doesnt have a * add it to the query as is.
    */
    public static String getTransformedQueryStr(String query, String nestedQueryParentObject) {
        // capitalise so we can substring without worrying about case
        query = query.replaceAll('(?i)select', 'SELECT');
        query = query.replaceAll('(?i)from', 'FROM');
        query = query.replaceAll('(?i)where', 'WHERE');
        String q = query.removeStart('SELECT');

        // split FROM thats not in ()
        List<String> querySplit = q.split('FROM+(?![^(]*\\))');
        String thingsToQuery = querySplit[0];
        // We split at FROM so the next word should be the sObject which if nested might have ), directly after
        String sObjectName = querySplit[1].trim().contains(' ') ? querySplit[1].trim().substringBefore(' ') : querySplit[1].trim();
        sObjectName = sObjectName.removeEnd(',').removeEnd(')');
        String conditions = querySplit[1];

        //splits , that are NOT in ()
        List<String> thingsToQuerySplit = thingsToQuery.split('[,]+(?![^(]*\\))');

        // Create a set of fields so they are unique
        Set<String> fieldsToAddToQuery = new Set<String>();
        for (String s : thingsToQuerySplit) {
            String thingToQuery = s.trim();
            if (thingToQuery.startsWith('(')) {
                String nestedQuery = thingToQuery.removeStart('(').removeEnd(')');
                String nestedQueryToAdd = '(' + getTransformedQueryStr(nestedQuery, sObjectName) + ')';
                fieldsToAddToQuery.add(nestedQueryToAdd);
            } else if (thingToQuery.contains('*')) {
                if (thingToQuery.contains('__r.*')) {
                    // The related object field name is an arbitrary field name for the lookup,
                    // we need to get the type of that lookup field.
                    //
                    // We need to start with finding the actual type for the subquery, we only have the relationship
                    // name handed to us.
                    String relatedObjectFieldName = thingToQuery.trim().substringBefore('.*');
                    String relatedObjectType;

                    if (nestedQueryParentObject == null) {
                        // In the top level select
                        relatedObjectType = sObjectName;
                    } else {
                        // In a subquery, get the object type from the relationship name
                        relatedObjectType = getRelatedObjectFromRelationship(sObjectName, nestedQueryParentObject);
                    }

                    List<String> relatedFieldsToAdd = getFieldsFromObject(relatedObjectFieldName, true, relatedObjectType);
                    fieldsToAddToQuery.addAll(relatedFieldsToAdd);
                } else {
                    List<String> thisObjsFieldsToAdd = getFieldsFromObject(sObjectName, false, nestedQueryParentObject);
                    fieldsToAddToQuery.addAll(thisObjsFieldsToAdd);
                }
            } else {
                fieldsToAddToQuery.add(thingToQuery);
            }
        }

        // Convert to a list so we can use String.join()
        List<String> allFields = new List<String>();
        allFields.addAll(fieldsToAddToQuery);

        return 'SELECT ' + String.join(allFields, ', ') + ' FROM ' + conditions;
    }

    public static String getFieldsFromObjectForQuery(String sObjectName) {
        List<String> fields =  getFieldsFromObject(sObjectName, false, null);
        String queryString = String.join(fields, ', ');
        return queryString;
    }

    /**
     * Getting and returning the field names for an object. The function can operate in a couple of ways
     *  1. Where we have a relationship name for child objects, e.g. Contacts on Account, and want the fields from Contact
     *  2. Where we have a lookup field to an object and want the fields on the object that the lookup is to, e.g. Contact.Account
     *  3. Where we have the object type
     *
     * sObjectName:                 Relationship name, or object type, or lookup field name
     * relatedObject:               Is sObjectName a lookup field name
     * nestedQueryParentObject :    Object type for the parent object if applicable
     *
     * Some examples:
     *
     * getFieldsFromObject('Account', false, null); // All Account fields
     * getFieldsFromObject('Contacts', false, 'Account'); // All Contact fields
     * getFieldsFromObject('Account', true, 'Contact'); // All the Account fields prefixed with 'Account.'
     */
    public static List<String> getFieldsFromObject(String sObjectName, Boolean relatedObject, String nestedQueryParentObject) {
        String transformedSObjectName;
        if (String.isNotBlank(nestedQueryParentObject)) {
            if (relatedObject) {
                // For related objects sObjectName is a lookup field name
                try {
                    SObjectType objType = Schema.getGlobalDescribe().get(nestedQueryParentObject);
                    Map<String,Schema.SObjectField> mfieldsMap = objType.getDescribe().fields.getMap();
                    Schema.SObjectField fld = mfieldsMap.get(sObjectName.trim().replace('__r', '__c'));
                    transformedSObjectName = fld.getDescribe().getReferenceTo().get(0).getDescribe().getName();
                } catch (Exception e) {
                    throw new QueryException('Unable to find relationship: ' + sObjectName + ' in ' + nestedQueryParentObject);
                }
            } else {
                // For non related objects sObjectName is a child relationship name
                transformedSObjectName = getRelatedObjectFromRelationship(sObjectName, nestedQueryParentObject.trim().replace('__r', '__c'));
            }
        } else {
            // sObjectName is the actual object name
            transformedSObjectName = sObjectName.trim().replace('__r', '__c');
        }

        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(transformedSObjectName);

        List<String> fieldList = new List<String>();
        if (targetType != null) {
            Map<String, Schema.SObjectField> fieldMap = targetType.getDescribe().fields.getMap();
            if (relatedObject) {
                for (String s : fieldMap.keyset()) {
                    fieldList.add(sObjectName + '.' + s);
                }
            } else {
                fieldList.addAll(fieldMap.keyset());
            }
        } else {
            throw new QueryException('Unable to find object: ' + transformedSObjectName);
        }

        return fieldList;
    }

    /**
     * Given a relationship name in object, return its actual Object name.
     *
     * For example, given "SELECT *, (SELECT * FROM Contacts) FROM Account", relationshipName
     * being Contacts, and sObjectName being Account, then we would return "Contact" as the type
     * of the relationship.
     */
    public static String getRelatedObjectFromRelationship(String relationshipName, String sObjectName) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(sObjectName);
        if (targetType != null) {
            DescribeSObjectResult describe = targetType.getDescribe();

            for (Schema.ChildRelationship child : describe.getChildRelationships()) {
                if (child.getChildSObject().getDescribe().fields.getMap().get('Name') != null && child.getRelationshipName() != null) {
                    if (child.getRelationshipName().equalsIgnoreCase(relationshipName)) {
                        String relationshipObjectType = child.getChildSObject().getDescribe().getName();
                        return relationshipObjectType;
                    }
                }
            }
        } else {
            throw new QueryException('Unable to find object name for relationship:  ' + relationshipName + ' in object ' + sObjectName);
        }
        return null;
    }

    /**
    * CAUTION! Make sure the parameters passed are safe and cannot contain a SOQL injection
    */
    public static List<sObject> getRecords(String objectName, String queryCondition) {
        return getRecords(objectName, '', queryCondition, '');
    }

    /**
    * CAUTION! Make sure the parameters passed are safe and cannot contain a SOQL injection
    */
    public static List<sObject> getRecords(String objectName, String queryCondition, String orderCondition) {
        return getRecords(objectName, '', queryCondition, orderCondition);
    }

    /**
    * CAUTION! Make sure the parameters passed are safe and cannot contain a SOQL injection
    */
    public static List<sObject> getRecords(String objectName, String relatedFields,  String queryCondition, String orderCondition) {
        String query = 'SELECT ' + getFieldsFromObjectForQuery(objectName);

        if (String.isNotBlank(relatedFields)) {
            query += ', ' + relatedFields;
        }

        query += ' FROM ' + objectName;
        query += queryCondition;

        if (String.isNotBlank(orderCondition)) {
            query += ' ORDER BY ' + orderCondition;
        }

        List<sObject> results = Database.query(query);
        return results;
    }
}