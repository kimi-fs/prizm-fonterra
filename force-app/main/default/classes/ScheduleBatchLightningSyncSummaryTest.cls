/*------------------------------------------------------------
Author:         Damon Shaw
Company:        Trineo
Description:    Test for Identify Individuals who should be synced with Staff devices using SF's Lightning Sync
History
10/01/18        Damon Shaw      Created
------------------------------------------------------------*/
@isTest
private class ScheduleBatchLightningSyncSummaryTest {
    private static Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();

    @testSetup static void setup() {
        User u = TestClassDataUtil.createAdminUser();

        Account theAccount = new Account(Name = 'test account');
        insert theAccount;
        insert new Field_Team_Tools_Settings__c(Name = 'IndividualDefaultAccountAU', Value__c = theAccount.Id);

        Contact theIndividual = TestClassDataUtil.createIndividualAU('Terry', 'Test1', false);
        theIndividual.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual AU').getRecordTypeId();
        insert theIndividual;

        Account theFarm = TestClassDataUtil.createSingleAccountFarm(false);
        theFarm.Rep_Area_Manager__c = u.Id;
        theFarm.Primary_Business_Contact__c = theIndividual.Id;
        insert theFarm;

        Individual_Relationship__c ir = TestClassDataUtil.createIndividualRelationship(theIndividual.Id, theFarm.Id, false);
        ir.RecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Derived Relationship').getRecordTypeId();
        insert ir;

        // No schedule up front
        ScheduleBatchLightningSyncSummary.abort();
    }

    @isTest static void batchTest() {
        Test.startTest();
        ScheduleBatchLightningSyncSummary theBatch = new ScheduleBatchLightningSyncSummary();
        Database.executeBatch(theBatch);
        Test.stopTest();

        Contact theIndividual = [SELECT Id, Lightning_Sync_GUIDs__c FROM Contact WHERE FirstName = 'Terry' AND LastName = 'Test1'];
        User theUser = [SELECT Id, GUID__c FROM User WHERE Alias = 'standt'];

        System.assertEquals(theUser.GUID__c, theIndividual.Lightning_Sync_GUIDs__c);
    }

    @isTest static void scheduleTest() {
        String jobName = 'Update Individuals for Staff Lightning Sync (ScheduleBatchLightningSyncSummary)';

        // Well SF won't let you create a Scheduler_CRON__mdt so we cheat and do it via JSON
        List<Map<String, Object>> mockedCronsAbstract = new List<Map<String, Object>> {
            new Map<String, Object> {
                'Type' => Schema.Scheduler_CRON__mdt.SObjectType.getDescribe().getName(),
                'Name' => 'ScheduleBatchLightningSyncSummary',
                'DeveloperName' => 'ScheduleBatchLightningSyncSummary',
                'CRON_Expression__c' => '0 0 1 * * ?',
                'Job_Name__c' => jobName,
                'Run_As_User__c' => ScheduleService.BATCH_PROCESS_AU
            }
        };

        List<Scheduler_CRON__mdt> mockedCrons = (List<Scheduler_CRON__mdt>)JSON.deserialize(JSON.serialize(mockedCronsAbstract), List<Scheduler_CRON__mdt>.class);
        ScheduleService.mockSchedulerCrons(mockedCrons);

        List<CronTrigger> existingJob = [SELECT Id FROM CronTrigger WHERE CronjobDetail.Name = : jobName];
        System.assertEquals(0, existingJob.size());

        ScheduleBatchLightningSyncSummary.schedule();
        List<CronTrigger> existingJob2 = [SELECT Id FROM CronTrigger WHERE CronjobDetail.Name = : jobName];
        System.assertEquals(1, existingJob2.size());

        ScheduleBatchLightningSyncSummary.abort();
        List<CronTrigger> existingJob3 = [SELECT Id FROM CronTrigger WHERE CronjobDetail.Name = : jobName];
        System.assertEquals(0, existingJob3.size());
    }
}