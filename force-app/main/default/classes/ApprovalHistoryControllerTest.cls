@IsTest
public with sharing class ApprovalHistoryControllerTest {
    
    @IsTest
    private static void testRetriveProcessInstance() {
        
        Account objFarm = TestClassDataUtil.createSingleAccountFarm(false);
        objFarm.Farm_Region_Name__c='West';
        insert objFarm;

        Case objCase = TestClassDataUtil.createCaseWithDefaultRecordType(false);
        objCase.RecordTypeId = GlobalUtility.caseFormATSApplicationRecordTypeId;
        objCase.Approval_Status__c='Draft';
        objCase.Supplier_Region__c ='West';
        objCase.AccountId = objFarm.Id;
        objCase.Raised_By__c='Internal';
        insert objCase;

        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
        app.setObjectId(objCase.id);
        Approval.ProcessResult result = Approval.process(app);

        Test.startTest();
        ApprovalHistoryController.retriveProcessInstance(objCase.Id);
        ProcessInstance objProcessInstance = [SELECT Id,TargetObjectId, CreatedDate FROM ProcessInstance WHERE TargetObjectId = :objCase.Id];
        Case c1 = [SELECT Id, CreatedDate FROM Case WHERE Id = :objCase.Id];
        System.assertEquals(true,objProcessInstance.CreatedDate >= c1.CreatedDate);
        Test.stopTest();

    }
}