/**
* Description:
* @author: Amelia (Trineo)
* @date: June 2018
* @test:
*/
public without sharing class CaseService {

    //Cease case type & sub-type
    public final static String CASE_TYPE_ONBOARDING_OFFBOARDING = 'Onboarding/Offboarding';
    public final static String CASE_TYPE_AGREEMENTS = 'Agreements';
    public final static String CASE_SUB_TYPE_CEASES = 'Ceases';
    public final static String CASE_SUB_TYPE_APPOINTMENT_REQUEST = 'Appointment Request';
    public final static String CASE_SUB_TYPE_SIGNED_AGREEMENT = 'Signed Agreement';

    public final static String CASE_STATUS_PENDING_SUBMISSION = 'Pending Submission';
    public final static String CASE_STATUS_CEASE_SUBMITTED = 'Cease Submitted';
    public final static String CASE_STATUS_RECEIVED = 'Received';
    public final static String CASE_STATUS_ACCEPTED = 'Accepted';
    public final static String CASE_STATUS_REJECTED = 'Rejected';
    public final static String CASE_STATUS_WITHDRAWL_SUBMITTED = 'Withdrawal Submitted';
    public final static String CASE_STATUS_WITHDRAWN = 'Withdrawn';
    public final static String CASE_STATUS_COMPLETED = 'Completed';

    public static List<Case> getCases(String queryCondition){
        String query = 'SELECT ' + QueryUtil.getFieldsFromObjectForQuery('Case');
        //required for cease cases
        query += ' ,Party__r.Name ,Account.Rep_Area_Manager__c, Submitted_By__r.Name, Withdrawn_By__r.Name';
        query += ' FROM Case ';
        query += queryCondition;
        List<Case> cases = Database.query(query);
        return cases;
    }

    public static Case upsertCase(Case theCase){
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.allowFieldTruncation = true;
        theCase.setOptions(dml);
        upsert theCase;
        return theCase;
    }

    public static void createContractsAppointmentRequestBookingCaseAU(
        String region,
        String supplierType,
        String farmIdOrNumber,
        String name,
        String phoneNumber,
        String comments,
        String attachmentName,
        Blob attachemntContent) {

        Case c = new Case();

        c.RecordTypeId = GlobalUtility.caseManageSupplyAURecordTypeId;
        c.Type = CASE_TYPE_AGREEMENTS;
        c.Sub_Type__c = CASE_SUB_TYPE_APPOINTMENT_REQUEST;
        c.Origin = 'Web';

        User theUser = [
            SELECT ContactId
            FROM User
            WHERE Id = :UserInfo.getUserId()
        ];
        // Only set Raised_By__c to 'External' if we have a contact ID due to validation rule: Case_Individual_Req_RaisedBy_External
        // As an appointment request can be filled out by a guest user who won't have a ContactId and therefore is an 'internal' user
        if (String.isNotEmpty(theUser.ContactId)) {
            c.Raised_By__c = 'External';
            c.ContactId = theUser.ContactId;
        } else {
            c.Raised_By__c = 'Internal';
        }

        if (!String.isBlank(farmIdOrNumber) && farmIdOrNumber instanceof Id) {
            c.AccountId = farmIdOrNumber;
            c.Description = 'Name: ' + name + '\n' +
                            'Phone Number: ' + phoneNumber + '\n' +
                            'Comments: ' + comments;
        } else {
            c.AccountId = GlobalUtility.defaultAccountIdAU;
            c.Description = 'Region: ' + region + '\n' +
                            'Supplier Type: ' + supplierType + '\n' +
                            'Farm Number: ' + farmIdOrNumber + '\n' +
                            'Name: ' + name + '\n' +
                            'Phone Number: ' + phoneNumber + '\n' +
                            'Comments: ' + comments;
        }

        insert c;

        if (attachmentName != null && attachemntContent != null) {
            Attachment att = new Attachment(Name = attachmentName, Body = attachemntContent, ParentId = c.Id);

            insert att;
        }
    }

    public static Case signedAgreementCase(Agreement__c agreement, Boolean isSignedOnline) {
        Case caseRecord = new Case();
        caseRecord.RecordTypeId = GlobalUtility.caseManageSupplyAURecordTypeId;
        caseRecord.OwnerId = GlobalUtility.getQueueIdFromDeveloperName('Milk_Supply_Services_AU');
        if (String.isNotEmpty(agreement.User_Signed__c)) {
            caseRecord.ContactId = agreement.User_Signed__r.ContactId;
        }
        caseRecord.Type = CASE_TYPE_AGREEMENTS;
        caseRecord.AccountId = agreement.Farm__c;
        caseRecord.Party__c = agreement.Party__c;
        caseRecord.Sub_Type__c = CASE_SUB_TYPE_SIGNED_AGREEMENT;
        caseRecord.Description = 'Agreement: ' + agreement.Name +  '\n' +
                                 'Agreement Start Date: ' + agreement.Start_Date__c + '\n' +
                                 'Agreement End Date: ' + agreement.End_Date__c  + '\n' +
                                 'Execution Date: ' + agreement.Execution_Date__c  + '\n';

        if (isSignedOnline) {
            caseRecord.Origin = 'Web';
            caseRecord.Raised_By__c = 'External';
        } else {
            caseRecord.Origin = 'Supplier';
            caseRecord.Raised_By__c = 'Internal';
        }

        return caseRecord;
    }

}