/*------------------------------------------------------------
Author:         Amelia Clarke
Company:        Trineo
Description:    SOAP Webservice for receiving agreements
------------------------------------------------------------*/
global class AgreementWebserviceAU {

   /*
    * Agreement model for WSDL generation
    */

    global class MessageHeader {
        WebService String InterfaceBatchId;
        WebService String TransactionType;
    }

    global class Farm {
        WebService String SupplyNumber;
    }

    global class OwnerParty {
        WebService String PartyId;
    }

    global class SplitArrangement {
        WebService String SplitArrangementId;
        WebService DateTime StartDate;
        WebService List<PaymentSplit> PaymentSplits;
    }

    global class PaymentSplit {
        WebService String PaymentSplitId;
        WebService String PaymentSplitType;
        WebService Party Party;
        WebService String PayeeType;
        WebService Boolean IsFinalPay;
        WebService Boolean CanViewOwnersProdHistory;
        WebService Integer Priority;
        WebService Decimal Amount;
        WebService Decimal CapAdjustedPercentage;
        WebService Decimal DemeritPercentage;
        WebService Decimal PenaltyPercentage;
        WebService Decimal Dividend;
        WebService Decimal Percentage;
        WebService String DPAType;
    }

    global class Party {
        WebService String PartyId;
    }

    global class AgreementType {
        WebService String AgreementType;
        WebService String AgreementTypeType;
    }

    global class Contract {
        WebService String ContractId;
        WebService Party Party;
        WebService DateTime StartDate;
        WebService DateTime EndDate;
        WebService String Status;
        WebService List<WinterContract> WinterContracts;
        WebService List<WinterTransaction> WinterTransactions;
        WebService List<SeasonContract> SeasonContracts;
    }

    global class WinterContract {
        WebService String WinterContractId;
        WebService Integer DailyQuantity;
        WebService String Season;
        WebService DateTime StartDate;
        WebService DateTime EndDate;
        WebService String Note;
    }

    global class WinterTransaction {
        WebService String WinterTransactionId;
        WebService String TransactionType;
        WebService String Season;
        WebService Integer Quantity;
        WebService String Status;
        WebService DateTime RecordCreatedOn;
        WebService String Comment;
    }

    global class SeasonContract {
        WebService String SeasonContractId;
        WebService String Season;
        WebService Integer ContractQuantity;
        WebService Integer SeasonQuantity;
        WebService Integer Allocated;
        WebService Integer ReductionForGMP;
        WebService Integer SharesForContract;
        WebService String Note;
    }

    global class Agreement {
        WebService MessageHeader MessageHeader;
        WebService String AgreementId;
        WebService Farm Farm;
        WebService OwnerParty OwnerParty;
        WebService DateTime StartDate;
        WebService DateTime EndDate;
        WebService String Product;
        WebService List<SplitArrangement> SplitArrangements;
        WebService AgreementType AgreementType;
        WebService Contract Contract;
        WebService Boolean NextOwnerCanViewMyProdHistory;
        WebService Integer Priority;
        WebService String Status;
        WebService Boolean IsMaintainedAsPrime;
        WebService Boolean IsMyMilk;
        WebService Boolean IsPrime;
        WebService DateTime CertificationExpiryDate;
    }


   /*
    * Error classes
    */
    global class InvalidAspireIdException extends Exception {
        global InvalidAspireIdException(String agreementId, String partyId) {
            this(partyId + ' ' + agreementId);
        }
    }

    global class MissingTagException extends Exception {

    }

   /*
    * Static vars, record/transaction types
    */
    private static final Id STANDARD_AGREEMENT_RECORD_TYPE_ID = [SELECT Id FROM RecordType WHERE DeveloperName = 'Standard_Agreement' AND SobjectType = 'Agreement__c'].Id;

    private static final String TRANSACTION_TYPE_UPDATE = 'Update';
    private static final String TRANSACTION_TYPE_CREATE = 'Create';
    private static final String TRANSACTION_TYPE_DELETE = 'Delete';

   /*
    * Main webservice method, accepts an Agreement object
    */
    WebService static String sendAgreement(Agreement agreement) {
        Logger.log('Got an agreement from the AgreementWebserviceAU', 'Data: ' + JSON.serializePretty(agreement), Logger.LogType.INFO);
        try {
            //determine action
            Agreement__c sfAgreement;
            Map<String, Account> accountMap;
            String transactionType = agreement.MessageHeader.TransactionType;

            //clean child objects for recreation if update
            if (transactionType.equalsIgnoreCase('Update')) {
                deleteSeasonsAndTransactionsAndSplitsRelatedToAgreement(agreement.AgreementId);
            }

            //create and update follow the same path from here
            if (transactionType.equalsIgnoreCase('Create') || transactionType.equalsIgnoreCase('Update')) {
                accountMap = retrieveAllAccounts(agreement);
                sfAgreement = mapAgreementToSFObject(agreement, accountMap);

                Logger.log('sfAgreement to update', 'sfAgreement: ' + sfAgreement + 'Agreement__c.Fields.Agreement_ID__c: ' + Agreement__c.Fields.Agreement_ID__c, Logger.LogType.INFO);

                Database.upsert(sfAgreement, Agreement__c.Fields.Agreement_ID__c);

                //treat future agreements as "active" for purposes of creating erels
                sfAgreement.Active__c = agreement.EndDate == null || agreement.EndDate >= Date.today() || agreement.StartDate >= Date.today();

                List<Payment_Split__c> splitsToInsert = mapSplitsToSFObjects(agreement, sfAgreement, accountMap);

                insert (splitsToInsert);

                SplitArrangement mostRecentSplitArrangement = getMostRecentSplitArrangement(agreement.SplitArrangements);

                //only create entity relationships if we've got a current split arrangement and the agreement is a standard agreement
                if (mostRecentSplitArrangement != null && agreement.AgreementType.AgreementTypeType == 'Standard') {
                    createOrUpdateEntityRelationships(sfAgreement, splitsToInsert, accountMap, mostRecentSplitArrangement);
                }
            } else if (transactionType.equalsIgnoreCase('Delete')) {
                deleteEntityRelationships(agreement.AgreementId);
                deleteAgreement(agreement.AgreementId);
            }

        } catch (Exception e) {
            Logger.log('Encountered an error in the AgreementWebserviceAU', 'Data: ' + jsonifyException(e), Logger.LogType.ERROR);

            //NOTE: this bit of code doesn't work if you throw the error because Salesforce refuses to commit any data at/after that point.
            //throw e;
        } finally {
            Logger.saveLog();
        }

        return '';
    }

    private static void createOrUpdateEntityRelationships(Agreement__c sfAgreement, List<Payment_Split__c> paymentSplits, Map<String, Account> accountMap, SplitArrangement latestSplit) {
        Date latestSplitDate = nullSafeDatetimeToDate(latestSplit.StartDate);

        List<Entity_Relationship__c> existingRelationships  =  [SELECT
                                                            Agreement__c,
                                                            Role__c,
                                                            Party__c,
                                                            Farm__c,
                                                            Active__c,
                                                            Deactivation_Date__c
                                                        FROM
                                                            Entity_Relationship__c
                                                        WHERE Farm__c  = :sfAgreement.Farm__c
                                                        AND   Party__c = :sfAgreement.Party__c
                                                        ORDER BY Creation_Date__c DESC NULLS LAST
                                                       ];
        List<Entity_Relationship__c> relationshipsToUpsert = new List<Entity_Relationship__c>();

        Map<String, Entity_Relationship__c> existingRelationshipsByRolePartyFarmMap = new Map<String, Entity_Relationship__c>();

        for (Entity_Relationship__c existingRelationship : existingRelationships) {
            String rolePartyFarmKey = (existingRelationship.Role__c + existingRelationship.Party__c + existingRelationship.Farm__c).toLowerCase();

            existingRelationshipsByRolePartyFarmMap.put(rolePartyFarmKey, existingRelationship);
        }

        Date futureSplitDate;

        for (Payment_Split__c paymentSplit : paymentSplits) {
            if (paymentSplit.Split_Start_Date__c < latestSplitDate) {
                //ignore if not the latest split
                continue;
            } else if (paymentSplit.Split_Start_Date__c != latestSplitDate) {
                //split is in the future, record this date
                futureSplitDate = paymentSplit.Split_Start_Date__c;
                break;
            }
        }

        //only create entity relationship objects for the "latest" split and anything _after_ that
        for (Payment_Split__c paymentSplit : paymentSplits) {
            if (paymentSplit.Split_Start_Date__c < latestSplitDate) {
                //ignore if not the latest split
                continue;
            }
            if (!sfAgreement.Active__c) {
                //expire all if agreement is inactive
                continue;
            }
            String rolePartyFarmKey = (paymentSplit.Payee_Type__c + paymentSplit.Party__c + sfAgreement.Farm__c).toLowerCase();
            String previousRolePartyFarmKey = (paymentSplit.Payee_Type__c + ' (Previous)' + paymentSplit.Party__c + sfAgreement.Farm__c).toLowerCase();
            Entity_Relationship__c expiredEntityRelationship = existingRelationshipsByRolePartyFarmMap.get(previousRolePartyFarmKey);

            if (sfAgreement.End_Date__c != null && existingRelationshipsByRolePartyFarmMap.get(rolePartyFarmKey) != null) {
                //add a deactivation date to existing erels if the agreement comes in with one
                Entity_Relationship__c entityRelationshipToUpdate = existingRelationshipsByRolePartyFarmMap.get(rolePartyFarmKey);

                entityRelationshipToUpdate.Deactivation_Date__c = sfAgreement.End_Date__c;
                entityRelationshipToUpdate.Agreement__c = sfAgreement.Id;

                relationshipsToUpsert.add(entityRelationshipToUpdate);
                existingRelationshipsByRolePartyFarmMap.put(rolePartyFarmKey, null);
            }
            else if (existingRelationshipsByRolePartyFarmMap.containsKey(rolePartyFarmKey)) {
                //don't touch existing relationships
                if (futureSplitDate == null) {
                    //if no future split then we can remove this from the map to be ignored
                    existingRelationshipsByRolePartyFarmMap.put(rolePartyFarmKey, null);
                }
            }
            else if (existingRelationshipsByRolePartyFarmMap.containsKey(previousRolePartyFarmKey)
                    && expiredEntityRelationship.Deactivation_Date__c != null && paymentSplit.Split_Start_Date__c == expiredEntityRelationship.Deactivation_Date__c.addDays(1)) {
                // Relationship has already been expired
                // Reactivate the ERel if the new Agreement starts a day after the End date
                Entity_Relationship__c entityRelationshipToUpdate = new Entity_Relationship__c(Id = expiredEntityRelationship.Id);
                entityRelationshipToUpdate.Role__c = expiredEntityRelationship.Role__c.replace(' (Previous)', '');
                entityRelationshipToUpdate.Agreement__c  = sfAgreement.Id;
                entityRelationshipToUpdate.Deactivation_Date__c = sfAgreement.End_Date__c;
                entityRelationshipToUpdate.Active__c = (paymentSplit.Split_Start_Date__c == latestSplitDate && latestSplitDate <= Date.today());
                relationshipsToUpsert.add(entityRelationshipToUpdate);

                // Add to map so it won't be processed again.
                existingRelationshipsByRolePartyFarmMap.put(previousRolePartyFarmKey, null);
            }
            else {
                //create a new entity relationship
                Entity_Relationship__c newRelationship = new Entity_Relationship__c();

                newRelationship.Active__c = (paymentSplit.Split_Start_Date__c == latestSplitDate && latestSplitDate <= Date.today());
                newRelationship.Creation_Date__c = paymentSplit.Split_Start_Date__c;
                newRelationship.Deactivation_Date__c = sfAgreement.End_Date__c;
                newRelationship.Role__c = paymentSplit.Payee_Type__c;
                newRelationship.Party__c = paymentSplit.Party__c;
                newRelationship.Agreement__c = sfAgreement.Id;
                newRelationship.Farm__c = sfAgreement.Farm__c;

                relationshipsToUpsert.add(newRelationship);

                // Add to map Previous Roles so it won't be processed
                existingRelationshipsByRolePartyFarmMap.put(previousRolePartyFarmKey, null);
            }
        }
        //second pass to expire any relationships that weren't in the most recent payload
        for (Entity_Relationship__c existingRelationship : existingRelationshipsByRolePartyFarmMap.values()) {
            //skip over nulls that we're not going to touch, as defined in the previous for loop
            if (existingRelationship == null) {
                continue;
            }
            // End date the agreements not part of the most recent payload with the agreement end date.
            existingRelationship.Deactivation_Date__c = sfAgreement.End_Date__c;
            //existingRelationship.Role__c = existingRelationship.Role__c + ' (Previous)';
            relationshipsToUpsert.add(existingRelationship);
        }

        upsert (relationshipsToUpsert);
    }

    private static Map<String, Account> retrieveAllAccounts(Agreement agreement) {
        Map<String, Account> partyIdToAccountMap = new Map<String, Account>();

        List<String> partyIds = new List<String>();

        partyIds.add(agreement.OwnerParty.PartyId);

        if (agreement.SplitArrangements != null) {
            for (SplitArrangement splitArrangement : agreement.SplitArrangements) {
                if (splitArrangement.PaymentSplits != null) {
                    for (PaymentSplit paymentSplit : splitArrangement.PaymentSplits) {
                        partyIds.add(paymentSplit.Party.PartyId);
                    }
                }
            }
        }

        List<Account> partyAccounts = [SELECT Id, Source_ID__c FROM Account WHERE Source_ID__c IN :partyIds];

        for (Account account : partyAccounts) {
            partyIdToAccountMap.put(account.Source_ID__c, account);
        }

        String farmSupplyNumber = agreement.Farm.SupplyNumber;
        List<Account> farmAccounts = [SELECT Id, Source_ID__c FROM Account WHERE Source_ID__c = :agreement.Farm.SupplyNumber];


        for (Account account : farmAccounts) {
            partyIdToAccountMap.put(account.Source_ID__c, account);
        }

        return partyIdToAccountMap;
    }

   /*
    * Delete helper methods for associated objects
    */
    private static void deleteAgreement(String agreementId) {
        try {
            delete([SELECT Id FROM Agreement__c WHERE Agreement_ID__c = :agreementId]);
        } catch (Exception e) {
            Logger.log('Encountered an error in the AgreementWebserviceAU.deleteAgreement', 'Data: ' + jsonifyException(e), Logger.LogType.ERROR);
        }
    }

    private static void deleteEntityRelationships(String agreementId) {
        try {
            delete([SELECT Id FROM Entity_Relationship__c WHERE Agreement__r.Agreement_ID__c = :agreementId]);
        } catch (Exception e) {
            Logger.log('Encountered an error in the AgreementWebserviceAU.deleteEntityRelationships', 'Data: ' + jsonifyException(e), Logger.LogType.ERROR);
        }
    }

    private static void deleteSeasonsAndTransactionsAndSplitsRelatedToAgreement(String agreementId) {
        try {
            delete([SELECT Id FROM Season__c WHERE Agreement__r.Agreement_ID__c = :agreementId]);
            delete([SELECT Id FROM Payment_Split__c WHERE Agreement__r.Agreement_ID__c = :agreementId]);
        } catch (Exception e) {
            Logger.log('Encountered an error in the AgreementWebserviceAU.deleteSeasonsAndTransactionsAndSplitsRelatedToAgreement', 'Data: ' + jsonifyException(e), Logger.LogType.ERROR);
        }
    }

   /*
    * Agreement to SF Agreement__c mapping methods
    */

    private static Agreement__c mapAgreementToSFObject(Agreement agreement, Map<String, Account> accountMap) {
        Agreement__c sfAgreement = new Agreement__c();
        // Set Default Record Types
        sfAgreement.RecordTypeId = STANDARD_AGREEMENT_RECORD_TYPE_ID;
        sfAgreement.Record_Type_Developer_Name__c = 'Standard';

        SplitArrangement mostRecentSplitArrangement = getMostRecentSplitArrangement(agreement.SplitArrangements);
        List<Id> cmValues = getCMValues(mostRecentSplitArrangement, accountMap);

        if (mostRecentSplitArrangement != null) {
            sfAgreement.Split_Start_Date__c = nullSafeDatetimeToDate(mostRecentSplitArrangement.StartDate);
        }

        sfAgreement.Agreement_ID__c = agreement.AgreementId;
        sfAgreement.Agreement__c = agreement.AgreementType.AgreementType;
        sfAgreement.Contract_Type__c = agreement.AgreementType.AgreementType;
        sfAgreement.Start_Date__c = agreement.StartDate.date();
        sfAgreement.End_Date__c = nullSafeDatetimeToDate(agreement.EndDate);
        sfAgreement.Farm__c = accountMap.get(agreement.Farm.SupplyNumber).Id;
        sfAgreement.Maintained_as_Prime__c = falseIfNull(agreement.IsMaintainedAsPrime);
        sfAgreement.Next_Owner_Can_View_Prod_History__c = falseIfNull(agreement.NextOwnerCanViewMyProdHistory);
        sfAgreement.Party__c = accountMap.get(agreement.OwnerParty.PartyId).Id;
        sfAgreement.Prime__c = falseIfNull(agreement.IsPrime);

        return sfAgreement;
    }

    private static List<Payment_Split__c> mapSplitsToSFObjects(Agreement agreement, Agreement__c sfAgreement, Map<String, Account> accountMap) {
        List<Payment_Split__c> splitsToInsert = new List<Payment_Split__c>();

        for (SplitArrangement splitArrangement : agreement.SplitArrangements) {
            if (splitArrangement.PaymentSplits != null) {
                for (PaymentSplit paymentSplit : splitArrangement.PaymentSplits) {
                    Payment_Split__c sfPaymentSplit = new Payment_Split__c();

                    sfPaymentSplit.Agreement__c = sfAgreement.Id;
                    sfPaymentSplit.Amount__c = paymentSplit.Amount;
                    sfPaymentSplit.Can_View_Owner_s_Prod_History__c = falseIfNull(paymentSplit.CanViewOwnersProdHistory);
                    sfPaymentSplit.Final_Pay__c = falseIfNull(paymentSplit.IsFinalPay);
                    sfPaymentSplit.Party__c = accountMap.get(paymentSplit.Party.PartyId).Id; //lookup for account
                    sfPaymentSplit.Payee_Type__c = paymentSplit.PayeeType;
                    sfPaymentSplit.Percentage__c = paymentSplit.Percentage;
                    sfPaymentSplit.Payment_Split_Type__c = paymentSplit.PaymentSplitType;

                    sfPaymentSplit.Split_Start_Date__c = splitArrangement.StartDate.date();

                    splitsToInsert.add(sfPaymentSplit);
                }
            }
        }

        return splitsToInsert;
    }

   /*
    * Misc utility/helper methods
    */
    private static Season__c findSeasonByName(String seasonName, List<Season__c> seasons) {
        for (Season__c season : seasons) {
            if (season.Season__c == seasonName) {
                return season;
            }
        }

        return null;
    }

    private static String truncateIfLongerThan(String s, Integer maxLength) {
        if (String.isNotBlank(s) && s.length() > maxLength) {
            s = s.substring(0, maxLength);
        }

        return s;
    }

    private static SplitArrangement getMostRecentSplitArrangement(List<SplitArrangement> splitArrangements) {
        SplitArrangement mostRecentSplitArrangement;

        for (SplitArrangement splitArrangement : splitArrangements) {
            if (mostRecentSplitArrangement == null || mostRecentSplitArrangement.StartDate < splitArrangement.StartDate) {
                //skip over "future" splits
                if (splitArrangement.StartDate <= Date.today())
                    mostRecentSplitArrangement = splitArrangement;
            }
        }

        //perform a reverse ordering if only "future" splits exist on the agreement
        if (mostRecentSplitArrangement == null) {
            for (SplitArrangement splitArrangement : splitArrangements) {
                if (mostRecentSplitArrangement == null || mostRecentSplitArrangement.StartDate > splitArrangement.StartDate) {
                    mostRecentSplitArrangement = splitArrangement;
                }
            }
        }

        return mostRecentSplitArrangement;
    }

    private static String getStatusForAgreement(Agreement agreement) {
        String status;
        if (agreement.Contract != null && !nullSafeEmpty(agreement.Contract.WinterTransactions)) {
            status = agreement.Contract.WinterTransactions[0].Status;
        } else if (agreement.Contract != null && !String.isBlank(agreement.Contract.Status)) {
            status = agreement.Contract.Status;
        } else {
            status = agreement.Status;
        }

        return status;
    }

    private static String getSeasonForAgreement(Agreement agreement) {
        String season;

        if (agreement.Contract != null && !nullSafeEmpty(agreement.Contract.WinterContracts)) {
            season = agreement.Contract.WinterContracts[0].Season;
        } else if (agreement.Contract != null && !nullSafeEmpty(agreement.Contract.SeasonContracts)) {
            season = agreement.Contract.SeasonContracts[0].Season;
        }

        return season;
    }

    private static List<Id> getCMValues(SplitArrangement splitArrangement, Map<String, Account> accountMap) {
        List<Id> cmValues = new List<Id>();

        if (splitArrangement != null && splitArrangement.PaymentSplits != null) {
            for (PaymentSplit paymentSplit : splitArrangement.PaymentSplits) {
                if (!paymentSplit.PayeeType.equalsIgnoreCase('OWNER')) {
                    if (accountMap.containsKey(paymentSplit.Party.PartyId))
                        cmValues.add(accountMap.get(paymentSplit.Party.PartyId).Id);
                }
            }
        }

        while (cmValues.size() < 2)
            cmValues.add(null);

        return cmValues;
    }

    private static Boolean falseIfNull(Boolean bool) {
        if (bool == null) {
            return false;
        }

        return bool;
    }

    private static Date nullSafeDatetimeToDate(Datetime dt) {
        if (dt == null) {
            return null;
        }

        return dt.date();
    }

    //Salesforce doesn't know how to turn exceptions into JSON strings
    private static String jsonifyException(Exception e) {
        Map<String, Object> exceptionDetails = new Map<String, Object>();

        exceptionDetails.put('LineNumber', e.getLineNumber());
        exceptionDetails.put('Cause', e.getCause());
        exceptionDetails.put('Message', e.getMessage());
        exceptionDetails.put('StacktraceString', e.getStacktraceString());
        exceptionDetails.put('TypeName', e.getTypeName());

        return JSON.serializePretty(exceptionDetails);
    }

    private static Boolean nullSafeEmpty(List<Object> l) {
        return l == null || l.isEmpty();
    }

}