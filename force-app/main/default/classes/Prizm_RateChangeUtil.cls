public class Prizm_RateChangeUtil {

        public static Map<Id, fsCore__Index_Rate_Setup__c> getIndexRateForRateChange(){
        system.debug(loggingLevel.ERROR, 'inside method');
        Map<Id, fsCore__Index_Rate_Setup__c> IdtoIndexRateSetUpMap = new Map<Id, fsCore__Index_Rate_Setup__c>();
        
        List<fsCore__Index_Rate_Setup__c> indexRateList = [SELECT Id
                                                           , Name
                                                           , fsCore__Index_Rate__c
                                                           , fsCore__Start_Date__c
                                                           , fsCore__End_Date__c
                                                           , fsCore__Index_Name__c 
                                                           FROM fsCore__Index_Rate_Setup__c 
                                                           WHERE fsCore__Is_Active__c = true
                                                           AND Is_Already_Picked__c = false];
        
        for(fsCore__Index_Rate_Setup__c indexRate : indexRateList)
        {   
            system.debug(loggingLevel.ERROR, 'inside for');
            if(!IdtoIndexRateSetUpMap.containsKey(indexRate.fsCore__Index_Name__c))
            {
                system.debug(loggingLevel.ERROR, 'inside if');
                IdtoIndexRateSetUpMap.put(indexRate.fsCore__Index_Name__c, indexRate);
            }
            /*else
            {
                system.debug(loggingLevel.ERROR, 'inside else');
                add error
            }*/
        }
        system.debug(loggingLevel.ERROR, IdtoIndexRateSetUpMap);
        system.debug(loggingLevel.ERROR, IdtoIndexRateSetUpMap.size());
        return IdtoIndexRateSetUpMap;        
        
    }
    
    
        public static Map<String, List<fsCore__Transaction_Parameter_Setup__c>> getTransactionIdToParmList(String pTransactionCode){
        
        Map<String, List<fsCore__Transaction_Parameter_Setup__c>> transactionCodeToParamListMap = new Map<String, List<fsCore__Transaction_Parameter_Setup__c>>();
        fsCore__Transaction_Setup__c transactionRecord = new fsCore__Transaction_Setup__c();
        
        fsCore.DynamicQueryBuilder transactionQuery = fsCore.DynamicQueryFactory.createQuery('fsCore__Transaction_Setup__c')
            .addFields()
            .addWhereConditionWithBind(1,'fsCore__Transaction_Code__c', '=', 'pTransactionCode');
        
        transactionRecord = (fsCore__Transaction_Setup__c)Database.query(transactionQuery.getQueryString());
        
        system.debug(loggingLevel.ERROR, transactionRecord);
        
        List<fsCore__Transaction_Parameter_Setup__c> transactionParamList = [SELECT Id, Name
                                                                             , fsCore__Field_Name__c
                                                                             , fsCore__Field_Object_Name__c
                                                                             , fsCore__Is_Active__c
                                                                             , fsCore__Display_Order__c
                                                                             , fsCore__Default_Value__c
                                                                             , fsCore__Transaction_Parameter_Field_Name__c
                                                                             FROM fsCore__Transaction_Parameter_Setup__c
                                                                             WHERE fsCore__Transaction_Name__c = :transactionRecord.Id
                                                                             AND fsCore__Is_Active__c = true];
        
        system.debug(loggingLevel.ERROR, 'Transaction parameter list : '+transactionParamList);
        
        transactionCodeToParamListMap.put(pTransactionCode, transactionParamList);
        
        system.debug(loggingLevel.ERROR, transactionCodeToParamListMap);
        return transactionCodeToParamListMap;
    }

     public static fsCore.ErrorObject getErrorObject(Exception pException, String pRecordId) {
        fsCore.ErrorObject errObj = new fsCore.ErrorObject();
        errObj.setErrorMessage(pException.getMessage());
        System.debug(loggingLevel.ERROR,'Record Id : '+pRecordId);
        if (pRecordId != null) {
            errObj.setErrorRecordId(pRecordId);
        }
        errObj.setErrorCode(fsCore.Constants.PROCESSING_ERROR);
        errObj.setErrorStackTrace(pException.getStackTraceString());
        
        return errObj;
    }
    
}