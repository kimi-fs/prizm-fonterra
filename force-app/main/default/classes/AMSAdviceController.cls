/**
 * Description: Displays a list of AMS Advice knowledge articles grouped by category
 *
 * There are some flags in here which dictate what is being shown on the page. It is worthwhile listing them here
 * and explaining their function. Also be aware that this controller is also passed to the search controller as
 * an extension.
 *
 *      currentArticleUrlName   -       The article URL that should be being displayed, comes in from the URL and also
*                                       when a category is selected.
 *      currentWrappedAdviceArticle -   The actual article that should be being displayed
 *      currentFAQArticle       -       If a FAQ has been selected (Read More) then this is that article, should be
 *                                      null if not on FAQs
 *      showFAQs                -       true if we are in the FAQs "category", used to highlight the category on page
 *
 *
 * Also of note are the two parameters that we are looking for:
 *      articleURL      -       An article that should be viewed
 *      Faq             -       Whether the article is actually a FAQ and not advice.
 *
 * @author: Amelia (Trineo)
 * @date: July 2017
 */
public with sharing class AMSAdviceController {

    public AMSKnowledgeService.ArticleCategoryWrapper currentWrappedAdviceArticle { get; private set; }

    public List<AMSKnowledgeService.ArticleCategoryWrapper> dataCategoryWrappers { get; private set; }

    public Map<Id, Knowledge__kav> adviceArticles { get; private set; }
    public List<Knowledge__kav> topFAQs { get; private set; }

    public Knowledge__kav currentFAQArticle { get; set; }
    public String currentArticleUrlName { get; set; }
    public String currentArticleCategoryName { get; set; }
    public Boolean showFAQs { get; set; }

    private static final Integer MAX_POPULAR_FAQS = 10;

    public AMSAdviceController() {
        PageReference currentPage = ApexPages.currentPage();
        this.currentArticleUrlName = currentPage.getParameters().get(getArticleParameterName());
        String faqs = currentPage.getParameters().get(getFAQParameterName());

        this.currentArticleCategoryName = currentPage.getParameters().get(getArticleCategoryParameterName());

        if (String.isNotBlank(faqs)) {
            this.showFAQs = true;
            selectFAQ();
        } else {
            this.showFAQs = false;
        }

        this.dataCategoryWrappers = AMSKnowledgeService.getAdviceArticleCategories();
        setAdviceArticles();
        addArticlesToCategories();

        // If a category has been selected, select the correct article from the dataCategoryWrappers
        if (!String.isBlank(this.currentArticleCategoryName)) {
            this.currentWrappedAdviceArticle = AMSKnowledgeService.getArticleCategoryWrapperByCategoryName(this.dataCategoryWrappers, currentArticleCategoryName);
        }

        // if no current article add the first one from the list
        if (currentArticleUrlName == null && currentWrappedAdviceArticle == null && dataCategoryWrappers.size() > 0) {
            currentWrappedAdviceArticle = dataCategoryWrappers[0];
        }
    }

    // Find the correct wrapper for the selected article in currentArticleUrlName
    public void selectArticleWrapper() {
        this.currentArticleUrlName = null;

        if (currentWrappedAdviceArticle != null) {
            // Deselect the existing wrapped article
            currentWrappedAdviceArticle.currentArticle = false;
        }

        // Now find the wrapper for the selected article
        for(AMSKnowledgeService.ArticleCategoryWrapper dataCategoryWrapper : dataCategoryWrappers){
            dataCategoryWrapper.childSelected = false;
            Boolean parentMatched = checkIfCurrentArticle(dataCategoryWrapper);
            for(AMSKnowledgeService.ArticleCategoryWrapper subCategoryDataCategoryWrapper : dataCategoryWrapper.childDataCategoryWrappers){

                Boolean childMatched = checkIfCurrentArticle(subCategoryDataCategoryWrapper);
                if(childMatched){
                    dataCategoryWrapper.childSelected = true;
                }
            }
        }
        this.showFAQs = false;
    }

    // A FAQ has been selected, query for that FAQ in full
    public void selectFAQ() {
        // Query for the FAQ article - since we may have come in direct from search
        this.currentFAQArticle = AMSKnowledgeService.faqArticleByUrl(currentArticleUrlName);
    }

    public void initialiseFAQIfSelected(){
        if(this.showFAQs){
            faqsSelected();
        }
    }

    public void faqsSelected() {
        // Clear the currently selected advice category so that it isn't shown as selected on the webpage nav
        for(AMSKnowledgeService.ArticleCategoryWrapper dataCategoryWrapper : dataCategoryWrappers){
            dataCategoryWrapper.currentArticle = false;
            for(AMSKnowledgeService.ArticleCategoryWrapper subCategoryDataCategoryWrapper : dataCategoryWrapper.childDataCategoryWrappers){
                subCategoryDataCategoryWrapper.currentArticle = false;
            }
        }

        this.showFAQs = true;
        clearSelectedFAQ();

        this.topFAQs = AMSKnowledgeService.faqsArticlesTop(MAX_POPULAR_FAQS);
    }

    public void clearSelectedFAQ() {
        this.currentFAQArticle = null;
    }

    //sets the advice articles
    public void setAdviceArticles() {
        this.adviceArticles = new Map<Id, Knowledge__kav>(AMSKnowledgeService.adviceArticles());
    }

    // adds the article to the wrapper
    public void addArticlesToCategories(){
        // create a map of category to list of knowledge articles
        Map<String, List<Knowledge__kav>> categoryKnowledgeArticleMap = new Map<String, List<Knowledge__kav>>();
        for(Knowledge__kav adviceArticle : adviceArticles.values()){
            String category = '';
            if(adviceArticle.DataCategorySelections.size() > 0){
                category = adviceArticle.DataCategorySelections[0].DataCategoryName;
            }
            if(!categoryKnowledgeArticleMap.keySet().contains(category)){
                categoryKnowledgeArticleMap.put(category, new List<Knowledge__kav>());
            }
            categoryKnowledgeArticleMap.get(category).add(adviceArticle);
        }

        for(AMSKnowledgeService.ArticleCategoryWrapper dataCategoryWrapper : dataCategoryWrappers){
            setArticleInWrapper(categoryKnowledgeArticleMap, dataCategoryWrapper);

            for(AMSKnowledgeService.ArticleCategoryWrapper subCategoryDataCategoryWrapper : dataCategoryWrapper.childDataCategoryWrappers){
                setArticleInWrapper(categoryKnowledgeArticleMap, subCategoryDataCategoryWrapper);

                if(subCategoryDataCategoryWrapper.currentArticle){
                    dataCategoryWrapper.childSelected = true;
                }
            }
        }
    }

    // puts the advice article in the correct wrapper
    private void setArticleInWrapper(Map<String, List<Knowledge__kav>> categoryKnowledgeArticleMap, AMSKnowledgeService.ArticleCategoryWrapper dataCategoryWrapper){
        if(categoryKnowledgeArticleMap.containsKey(dataCategoryWrapper.name)){
            AMSKnowledgeService.ArticleCategoryWrapper categoryLandingWrapper;

            // Two loops - so that we can get the landing one first.
            for(Knowledge__kav articleInCategory : categoryKnowledgeArticleMap.get(dataCategoryWrapper.name)){
                // The Landing articles are the top ones for a category - always at the top.
                if(articleInCategory.Display_Type__c == 'Landing' ){
                    dataCategoryWrapper.adviceArticle = articleInCategory;
                    checkIfCurrentArticle(dataCategoryWrapper);
                    categoryLandingWrapper = dataCategoryWrapper;
                    break;
                }
            }
            for(Knowledge__kav articleInCategory : categoryKnowledgeArticleMap.get(dataCategoryWrapper.name)){
                if(articleInCategory.Display_Type__c != 'Landing' ){
                    AMSKnowledgeService.ArticleCategoryWrapper childArticle = new AMSKnowledgeService.ArticleCategoryWrapper(articleInCategory);
                    Boolean matched = checkIfCurrentArticle(childArticle);
                    dataCategoryWrapper.childDataCategoryWrappers.add(childArticle);

                    // If we matched on a child article then set the current article to the landing article for this
                    // category.
                    if (matched && categoryLandingWrapper != null) {
                        this.currentWrappedAdviceArticle = categoryLandingWrapper;
                        categoryLandingWrapper.currentArticle = true;
                    }
                }
            }
        }
    }

    // checks if the article has the same id as passed in the url
    private Boolean checkIfCurrentArticle(AMSKnowledgeService.ArticleCategoryWrapper article){
        Boolean matched = false;
        if((!String.isBlank(currentArticleCategoryName) && article.label == currentArticleCategoryName) || (!String.isBlank(currentArticleUrlName) && article.adviceArticle.UrlName == currentArticleUrlName)){
            article.currentArticle = true;
            this.currentWrappedAdviceArticle = article;
            matched = true;
        } else {
            article.currentArticle = false;
        }
        return matched;
    }

    public static String getArticleParameterName() {
        return AMSCommunityUtil.getArticleParameterName();
    }

    public static String getArticleCategoryParameterName() {
        return AMSCommunityUtil.getArticleCategoryParameterName();
    }

    public static String getFAQParameterName() {
        return AMSCommunityUtil.getFAQParameterName();
    }

}