/**
 * Created by Nick on 15/08/17.
 *
 * AMSCollectionGraph
 *
 * Controller for storing and calculation Graph Details
 *
 * Author: NL (Trineo)
 * Date: 2017-08-15
 **/
public class AMSCollectionGraph {
    public AMSCollectionService.CollectionPeriodForm collectionPeriodForm { get; set; }
    public AMSCollectionService.UnitOfMeasurement selectedComparisonValue { get; set; }

    public transient String categorySeries { get; set; }

    public transient String selectedSeasonSeries { get; set; }
    public transient String priorSeasonSeries { get; set; }
    public transient String varianceSeasonSeries { get; set; }
    public transient String targetSeasonSeries { get; set; }

    private List<Long> categoryList;
    private List<Double> selectedSeasonList;
    private List<Double> priorSeasonList;
    private List<Double> varianceSeasonList;
    private List<Double> targetSeasonList;

    public String selectedSeasonName { get; set; }
    public String priorSeasonName { get; set; }

    public String degreeofIteration { get; set; }

    public AMSCollectionGraph(AMSCollectionService.CollectionPeriodForm collectionPeriodForm, AMSCollectionService.UnitOfMeasurement selectedComparisonValue) {
        this.selectedComparisonValue = selectedComparisonValue;
        this.collectionPeriodForm = collectionPeriodForm;
        this.selectedSeasonName = collectionPeriodForm.selectedFarmSeason.Reference_Period__r.Reference_Period__c;
        this.priorSeasonName = collectionPeriodForm.priorFarmSeason.Reference_Period__r.Reference_Period__c;
        this.degreeofIteration = collectionPeriodForm.degreeOfIteration;
        this.smoothing = collectionPeriodForm.probablyOnSkipDayCollections;

        collectionPeriodForm.smoothDataForGraph(this.smoothing);

        createCollectionJSON(collectionPeriodForm);
    }

    public AMSCollectionGraph(AMSCollectionService.CollectionPeriodForm collectionPeriodForm, AMSCollectionService.UnitOfMeasurement selectedComparisonValue, Boolean cleanup) {
        this.selectedComparisonValue = selectedComparisonValue;
        this.collectionPeriodForm = collectionPeriodForm;
        this.selectedSeasonName = collectionPeriodForm.selectedFarmSeason.Reference_Period__r.Reference_Period__c;
        this.priorSeasonName = collectionPeriodForm.priorFarmSeason.Reference_Period__r.Reference_Period__c;
        this.degreeofIteration = collectionPeriodForm.degreeOfIteration;
        this.smoothing = collectionPeriodForm.probablyOnSkipDayCollections;

        collectionPeriodForm.smoothDataForGraph(this.smoothing);

        createCollectionJSON(collectionPeriodForm, cleanup);
    }

    public Boolean smoothing { get; set; }

    public void toggleSmoothing() {
        this.smoothing = !this.smoothing;

        collectionPeriodForm.smoothDataForGraph(this.smoothing);
        this.createCollectionJSON(collectionPeriodForm);
    }

    //Populates the 4 required Strings for the construction of the graph
    public void createCollectionJSON(AMSCollectionService.CollectionPeriodForm collectionPeriodForm, Boolean cleanup) {
        //Open bracket for JSON
        categoryList = new List<Long>();
        selectedSeasonList = new List<Double>();
        priorSeasonList = new List<Double>();
        varianceSeasonList = new List<Double>();
        targetSeasonList = new List<Double>();
        //Iterate through Collection Period Form to with the selected comparison value to populate the values
        for (Integer i = 0; i < collectionPeriodForm.collectionPeriodWrapperList.size(); i++) {
            AMSCollectionService.CollectionPeriodWrapper cpw = collectionPeriodForm.collectionPeriodWrapperList[i];

            if (cpw.collectionPeriod.dateOfCollection != null) {
                categoryList.add(((DateTime) cpw.collectionPeriod.dateOfCollection).getTime());
            } else {
                categoryList.add(null);
            }

            if (selectedComparisonValue.equals(AMSCollectionService.KgMS)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.kgMSTotal);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.kgMSTotal);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.kgMSPercentageVariance);
                if (this.degreeofIteration == AMSCollectionService.DAY) {
                    targetSeasonList = addToListIfNotZero(targetSeasonList, Double.valueOf(cpw.collectionPeriod.relevantTarget.Daily_Target_KgMS__c));
                } else if (this.degreeofIteration == AMSCollectionService.MONTH) {
                    targetSeasonList = addToListIfNotZero(targetSeasonList, Double.valueOf(cpw.collectionPeriod.relevantTarget.Target_KgMS__c));
                }
            } else if (selectedComparisonValue.equals(AMSCollectionService.Litres)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.volumeLitresTotal);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.volumeLitresTotal);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.volumeLitresPercentageVariance);
            } else if (selectedComparisonValue.equals(AMSCollectionService.BMCC)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.bMCCTotal);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.bMCCTotal);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.bMCCPercentageVariance);
            } else if (selectedComparisonValue.equals(AMSCollectionService.ProteinPercentage)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.proteinPercentageAverage);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.proteinPercentageAverage);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.proteinPercentagePercentageVariance);
            } else if (selectedComparisonValue.equals(AMSCollectionService.FatPercentage)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.fatPercentageAverage);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.fatPercentageAverage);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.fatPercentagePercentageVariance);
            } else if (selectedComparisonValue.equals(AMSCollectionService.KgMSPercentage)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.kgMSPercentageAverage);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.kgMSPercentageAverage);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.kgMSPercentagePercentageVariance);
            } else if (selectedComparisonValue.equals(AMSCollectionService.Temp)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.temperatureAverage);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.temperatureAverage);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.temperaturePercentageVariance);
            } else if (selectedComparisonValue.equals(AMSCollectionService.FatKg)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.fatKgsTotal);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.fatKgsTotal);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.fatKgsPercentageVariance);
            } else if (selectedComparisonValue.equals(AMSCollectionService.ProteinKg)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.proteinKgsTotal);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.proteinKgsTotal);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.proteinKgsPercentageVariance);
            } else if (selectedComparisonValue.equals(AMSCollectionService.KgMSPerCow)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.kgMSPerCowTotal);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.kgMSPerCowTotal);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.kgMSPerCowPercentageVariance);
            } else if (selectedComparisonValue.equals(AMSCollectionService.ProteinFatRatio)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.proteinToFatRatioAverage);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.proteinToFatRatioAverage);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.proteinToFatRatioPercentageVariance);
            } else if (selectedComparisonValue.equals(AMSCollectionService.KgMSPerHectare)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.kgMSPerHectareTotal);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.kgMSPerHectareTotal);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.kgMSPerHectarePercentageVariance);
            } else if (selectedComparisonValue.equals(AMSCollectionService.TotalKgMS)) {
                selectedSeasonList = addToListIfNotZero(selectedSeasonList, cpw.collectionPeriod.totalKGMSTotal);
                priorSeasonList = addToListIfNotZero(priorSeasonList, cpw.collectionPeriod.priorSeasonDayWrapper.collectionPeriod.totalKGMSTotal);
                varianceSeasonList = addToListIfNotZero(varianceSeasonList, cpw.collectionPeriod.totalKgMSPercentageVariance);
            }
        }
        categorySeries = JSON.serialize(categoryList);
        selectedSeasonSeries = JSON.serialize(selectedSeasonList);
        priorSeasonSeries = JSON.serialize(priorSeasonList);
        varianceSeasonSeries = JSON.serialize(varianceSeasonList);
        targetSeasonSeries = JSON.serialize(targetSeasonList);

        if (cleanup == true) {
            collectionPeriodForm.cleanupWrapperList();
        }
    }

    public void createCollectionJSON(AMSCollectionService.CollectionPeriodForm collectionPeriodForm) {
        createCollectionJSON(collectionPeriodForm, true);
    }

    /**
    * If the value is 0 it adds null to the list
    * This is because nulls are not plotted on the charts whereas 0's are
    * 0's from Collection records are not 'real values' but filler values until the actual value is sent from Milk Pay
    */
    public List<Double> addToListIfNotZero(List<Double> valueList, Double value){
        if (value != 0) {
            valueList.add(value);
        } else {
            valueList.add(null);
        }
        return valueList;
    }
}