/**
 * AMSGoalsController.cls
 * Description: Controller for AMSGoals.page
 * @author: John Au
 * @date: December 2018
 */
public with sharing class AMSGoalsController {

    private Id currentUserContactId;

    public List<Goal__c> activeGoals { get; set; }
    public List<Goal__c> archivedGoals { get; set; }

    public AMSGoalsController() {
        this.currentUserContactId = AMSContactService.determineContactId();

        activeGoals = GoalService.getGoalsForIndividual(currentUserContactId);
        archivedGoals = GoalService.getGoalsForIndividual(currentUserContactId, true);
    }

}