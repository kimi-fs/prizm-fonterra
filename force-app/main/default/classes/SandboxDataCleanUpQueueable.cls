/**
* Description:
* @author: Eric Hu (trineo)
* @date: Aug 2019
* @test: SandboxDataTest
*/
public class SandboxDataCleanUpQueueable implements Queueable {

    private List<User> users { get; set; }

    private static Boolean runningInASandbox {
        get {
            if(runningInASandbox == null){
                runningInASandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
            }
            return runningInASandbox;
        } private set;
    }

    public SandboxDataCleanUpQueueable(List<User> userList) {
        users = userList;
    }

    public void execute(QueueableContext context) {


        if(validate()) {
            if(!Test.isRunningTest()) {
                setAutomationUserPasswords( JSON.serialize(users) );
            } else {
                Logger.log('SandboxDataCleanUpQueueableTest complete', '', Logger.LogType.INFO);
                Logger.saveLog();
            }
        }
    }

    @future
    private static void setAutomationUserPasswords(String userString) {
        List<User> users = (List<User>) JSON.deserialize(userString, List<User>.class);
        PermissionSet permissionSet = [SELECT Id, Name FROM PermissionSet WHERE Name = 'Sandbox_Dont_expire_password'];
        List<PermissionSetAssignment> psAssignments = new List<PermissionSetAssignment>();


        for(User u :users) {
            System.setPassword(u.id,'reset-password-1');
            System.setPassword(u.Id, 'automationPW005');

            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = permissionSet.id, AssigneeId = u.id);
            psAssignments.add(psa);
        }
        try {
            insert psAssignments;
        } catch (exception e) {
            Logger.log('SandboxDataUserQueueable insert permission set assignment error', e.getMessage() + ' - ' + e.getStackTraceString(), Logger.LogType.ERROR);
        }
    }

    public static Boolean validate() {

        // check this is not being run in Production
        if(runningInASandbox == false && Test.isRunningTest() == false) {
            System.assert(false, '** this should not be run in a production environment outside of a test **');
        }

        return true;
    }

}