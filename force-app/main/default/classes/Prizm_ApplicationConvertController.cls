/* 
 * Class Name   - Prizm_ApplicationConvertController
 *
 * Description  - Apex Controller for Prizm_ConvertAdvanceApplication component
 *
 * Developer(s) - Financial Spectra
 */

global class Prizm_ApplicationConvertController {

    @AuraEnabled
    global static String convertAdvanceApptoLendingApp(Id pAdvanceApplicationId) {
        return convertToLendingApplication(new Set<Id>{pAdvanceApplicationId});
    }
    
    global static String convertToLendingApplication(Set<Id> pAdvanceApplicationIds){
        fsCore.ActionInput acInput = new fsCore.ActionInput();
        acInput.addRecords(pAdvanceApplicationIds);
        
        fsCore.ActionOutput acOutput = new fsCore.ActionOutput();
        try{
            Prizm_AdvanceApplicationConverter action = new Prizm_AdvanceApplicationConverter();
            action.setInput(acInput);
            action.process();
            acOutput = action.getOutput();
        }
        catch (Exception e){
            acOutput.addError(e);
        }
        
        if (acOutput.getIsSuccess()) {
            acOutput.setMessage(Label.Advance_Application_Converted);
        }
        
        return acOutput.getJSONString();
    }

}