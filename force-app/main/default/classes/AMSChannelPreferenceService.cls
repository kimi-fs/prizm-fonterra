/**
 * AMSChannelPreferenceService
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately.
 *
 * Author: Amelia (Trineo)
 * Date: March 2017
 */
public without sharing class AMSChannelPreferenceService {

    public static List<Channel_Preference__c> queryChannelPreferences(String queryCondition){
        String query = 'SELECT *, Channel_Preference_Setting__r.* ';
        query +=' FROM Channel_Preference__c ';
        query += queryCondition;
        query += ' AND Channel_Preference_Setting__r.Active__c = true ';
        query += ' AND Channel_Preference_Setting__r.RecordTypeId = \'' + GlobalUtility.australiaChannelPreferenceSettingRecordTypeId + '\'';
        List<Channel_Preference__c> channelPreferences = QueryUtil.query(query);
        return channelPreferences;
    }

    public static List<Channel_Preference__c> getChannelPreferencesFromContact(String contactId){
        return queryChannelPreferences(' WHERE Contact_ID__c = \'' + contactId + '\'');
    }

    public static List<Channel_Preference__c> upsertChannelPreferences(List<Channel_Preference__c> channelPreferences){
        upsert channelPreferences;
        return channelPreferences;
    }

    /**
    * Default setting is that all Farm Source Website users are opted in to receive notifications.
    */
    public static void subscribeUsersToNotifications(Id contactId){
        List<Channel_Preference__c> allChannelPreferences = getChannelPreferencesFromContact(contactId);
        for (Channel_Preference__c cp : allChannelPreferences){
            cp.DeliveryByEmail__c = true;
            cp.NotifyBySMS__c = true;
        }
        update allChannelPreferences;
    }

}