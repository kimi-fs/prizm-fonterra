/*------------------------------------------------------------
Author:			Salman Zafar
Company:		Davanti Consulting
Description:	Handler class for AgreementTrigger
Test Class:		AgreementTrigger_Test
History
23/03/2015		Salman Zafar	Created
24/04/2015		Salman Zafar	Logic Added in createSharemilkerContractMilker method - Point 1-C
------------------------------------------------------------*/

public class AgreementTriggerHandler {

    public static void validateDeleteAgreementAccess(Map<Id, Agreement__c> mapOldAgreement) {
        for (Agreement__c agreement : mapOldAgreement.values()) {
            if (agreement.RecordTypeId != GlobalUtility.agreementStandardRecordTypeId) {
                return;
            } else if (agreement.Farm_Record_Type__c != GlobalUtility.auAccountFarmRecordTypeName) {
                return;
            } else if (!FeatureManagement.checkPermission('Delete_Agreements')) {
                mapOldAgreement.get(agreement.Id).addError('You don\'t have access to delete this record. ');
            }
        }
    }

    public static void validateEditAgreementAccess(Map<Id, Agreement__c> listNewMapAgreement) {
        for (Agreement__c agreement : listNewMapAgreement.values()) {
            if (agreement.Active__c) {
                if (agreement.RecordTypeId != GlobalUtility.agreementStandardRecordTypeId) {
                    return;
                } else if (agreement.Farm_Record_Type__c != GlobalUtility.auAccountFarmRecordTypeName) {
                    return;
                } else if (!FeatureManagement.checkPermission('Modify_Active_Agreements')) {
                    listNewMapAgreement.get(agreement.id).addError('You don\'t have access to update an active agreement.');
                }
            }
        }
    }

    public static void checkExistingAgreement(List<Agreement__c> listNewAgreement, Boolean isInsert) {
        List<String> farmList = new List<String>();
        List<String> partyList = new List<String>();

        for (Agreement__c agreement : listNewAgreement) {
            farmList.add(agreement.Farm__c);
            partyList.add(agreement.Party__c);
        }
        List<Agreement__c> existingStandardAgreements = [SELECT
                                                               Id,
                                                               Name,
                                                               Party__c,
                                                               Start_Date__c,
                                                               Farm__c,
                                                               RecordTypeId,
                                                               Agreement_Status__c
                                                         FROM
                                                               Agreement__c
                                                         WHERE
                                                               Farm__c IN :farmList
                                                               AND
                                                                  Party__c IN :partyList
                                                               AND
                                                                  RecordTypeId = :GlobalUtility.agreementStandardRecordTypeId
                                                               AND
                                                                  Farm__r.RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId];

         for (Agreement__c newAgreement : listNewAgreement) {
             for (Agreement__c existingAgreement : existingStandardAgreements) {
                if (existingAgreement.Farm__c == newAgreement.Farm__c &&
                    existingAgreement.Party__c == newAgreement.Party__c &&
                    existingAgreement.Start_Date__c == newAgreement.Start_Date__c &&
                    existingAgreement.RecordTypeId == newAgreement.RecordTypeId &&
                    existingAgreement.Agreement_Status__c != AgreementService.AGREEMENT_STATUS_CANCELLED &&
                    newAgreement.Agreement_Status__c != AgreementService.AGREEMENT_STATUS_CANCELLED
                ) {
                    Boolean showError = isInsert ? true : (newAgreement.Id != existingAgreement.Id);
                    if (showError) {
                        newAgreement.addError('There\'s an existing agreement with the same farm, party and start date: ' + existingAgreement.Name);
                    }
                }
            }
        }
    }

    /* There are scenarios where the Area Managers need to create agreement manually like the Non Standard agreements farms.
     *  If the agreement is not signed online as determined in AgreementService.isOnlineSignedAgreement method, the agreement is created manually. */
    public static void createCaseForManualSignedAgreements(List<Agreement__c> listNewAgreement, Map<Id, Agreement__c> mapOldAgreement) {
        List<Agreement__c> createCaseAgreements = new List<Agreement__c>();
        Set<Id> agreementIds = new Set<Id>();

        for (Agreement__c agreement : listNewAgreement) {
            if (!AgreementService.isOnlineSignedAgreement(agreement) &&
                String.isNotEmpty(agreement.Agreement_Status__c) &&
                (GlobalUtility.isChanged(Agreement__c.Agreement_Status__c, agreement, mapOldAgreement) &&
                agreement.Agreement_Status__c == AgreementService.AGREEMENT_STATUS_EXECUTED))
            {
                createCaseAgreements.add(agreement);
                agreementIds.add(agreement.Id);
            }
        }
        List<Case> createdCase = AgreementService.createCaseForSignedAgreements(createCaseAgreements, null, null, false);
        if (!agreementIds.isEmpty() && !createdCase.isEmpty()) {
            List<ContentDocumentLink> getRelatedContentDocument = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN : agreementIds];
            List<ContentDocumentLink> shareFilesToCase = new List<ContentDocumentLink>();

            for (ContentDocumentLink contentdocument : getRelatedContentDocument) {
                for (Case c : createdCase) {
                    shareFilesToCase.add(new ContentDocumentLink(ContentDocumentId = contentDocument.ContentDocumentId, ShareType = 'V', LinkedEntityId = c.Id));
                }
            }
            insert shareFilesToCase;
        }
    }

    public static void setAgreementId(List<Agreement__c> listNewAgreement, Map<Id, Agreement__c> mapOldAgreement) {
        List<String> agreementStatusSetAspireIdToNull = new List<String> {AgreementService.AGREEMENT_STATUS_CANCELLED, AgreementService.AGREEMENT_STATUS_PENDING};
        for (Agreement__c agreement : listNewAgreement) {
            if (agreement.RecordTypeId != GlobalUtility.agreementStandardRecordTypeId || agreement.Farm_Record_Type__c != GlobalUtility.auAccountFarmRecordTypeName) {
                return;
            } else if (agreement.Start_Date__c == null ||
                String.isEmpty(agreement.Farm_Number__c) ||
                String.isEmpty(agreement.Party_ID__c)
            ) {
                return;
            } else if (agreement.Agreement_Status__c == AgreementService.AGREEMENT_STATUS_EXECUTED) {
                Date startDate = agreement.Start_Date__c;
                String startDateFormatted = DateTime.newInstance(startDate.year(), startDate.month(), startDate.day()).format('yyyy-MM-dd');
                agreement.Agreement_ID__c = 'AU_' + agreement.Farm_Number__c + '_' + agreement.Party_ID__c + '_' + startDateFormatted;
            } else if (GlobalUtility.isChanged(Agreement__c.Agreement_Status__c, agreement, mapOldAgreement) && agreementStatusSetAspireIdToNull.contains(agreement.Agreement_Status__c)) {
                agreement.Agreement_ID__c = null;
            }
        }
    }
}