/**
* Description: Handler class for IndividualRelationshipTrigger
* @author: Salman Zafar (Davanti Consulting)
* @date: May 2015
* @test: IndividualRelationshipTrigger_Test
*/
public class IndividualRelationshipTriggerHandler extends TriggerHandler {

    private static Id derivedRelationshipRecordTypeId = GlobalUtility.derivedRelationshipRecordTypeId;
    private static Id individualPartyRecordTypeId = GlobalUtility.individualPartyRecordTypeId;
    private static String individualPartyRecordName = GlobalUtility.individualPartyRecordName;

    public override void beforeInsert() {

    }
    public override void beforeUpdate() {
        deactivateDate(Trigger.new, (Map<Id, Individual_Relationship__c>) Trigger.oldMap);
    }
    public override void afterInsert() {
        createDerivedIndividualRelationship(Trigger.new,(Map<Id, Individual_Relationship__c>) Trigger.oldMap);
        setRuralProfessionalFlag(Trigger.new);
    }
    public override void afterUpdate() {
        createDerivedIndividualRelationship(Trigger.new, (Map<Id, Individual_Relationship__c>) Trigger.oldMap);
        setRuralProfessionalFlag(Trigger.new);
    }

    // FSF-523 View detailed (derived) relationships
    public static void createDerivedIndividualRelationship(list<Individual_Relationship__c> listNewIndividualRelationship,
        map<Id, Individual_Relationship__c> mapOldIndividualRelationship
    ) {
        map<Id, Id> mapIR2Upsert = new map<Id, Id>();
        map<Id, list<Id>> mapListRelatedFarms = new map<Id, list<Id>>();

        // FSCRM-4810 - Change Individual Relationship Trigger to delete derived relationships, not deactivate them.
        Set<String> irsToDelete = new Set<String>();

        set<String> setRolesER = GlobalUtility.getRolesER();
        set<String> setRolesIR = GlobalUtility.getRolesIR();

        for (Individual_Relationship__c ir : listNewIndividualRelationship) {
            if (ir.RecordTypeId == individualPartyRecordTypeId || ir.Record_Type_Developer_Name__c == individualPartyRecordName) {
                if (trigger.isUpdate) {
                    // update derived relationship
                    if (ir.Active__c && setRolesIR.contains(ir.Role__c) &&
                        (!mapOldIndividualRelationship.get(ir.Id).Active__c ||
                        ir.Individual__c != mapOldIndividualRelationship.get(ir.Id).Individual__c ||
                        ir.Role__c != mapOldIndividualRelationship.get(ir.Id).Role__c ||
                        ir.Party__c != mapOldIndividualRelationship.get(ir.Id).Party__c))
                    {
                        mapIR2Upsert.put(ir.Id, ir.Party__c);
                    }
                    // deactivate derived relationship - these records are deleted by a nightly batch job
                    else if (!ir.Active__c && mapOldIndividualRelationship.get(ir.Id).Active__c && setRolesIR.contains(ir.Role__c)) {
                        mapIR2Upsert.put(ir.Id, ir.Party__c);
                    }
                }
                // insert derived relationship
                else if (trigger.isInsert && ir.Active__c && setRolesIR.contains(ir.Role__c)) {
                    mapIR2Upsert.put(ir.Id, ir.Party__c);
                }
            }
        }

        if (!mapIR2Upsert.keySet().isEmpty()) {
            // get farm to party entity relationships
            list<Entity_Relationship__c> listEntityRelationship = [select Id, Farm__c, Party__c, Role__c from Entity_Relationship__c where
                Party__c IN : mapIR2Upsert.values() and Farm__c != null and Active__c = true and
                Role__c IN : setRolesER];
            map<String, String> mapPartyFarm2Role = new map<String, String>();
            for (Entity_Relationship__c er : listEntityRelationship) {
                mapPartyFarm2Role.put(string.valueof(er.Party__c) + string.valueof(er.Farm__c), er.Role__c);
                list<Id> listFarmId = new list<Id>();
                if (mapListRelatedFarms.containsKey(er.Party__c)) {
                    listFarmId = mapListRelatedFarms.get(er.Party__c);
                    listFarmId.add(er.Farm__c);
                    mapListRelatedFarms.put(er.Party__c, listFarmId);
                } else {
                    listFarmId.add(er.Farm__c);
                    mapListRelatedFarms.put(er.Party__c, listFarmId);
                }
            }
            list<Individual_Relationship__c> listIndividualRelationship2Upsert = new list<Individual_Relationship__c>();
            // loop through individual relationships
            for (Individual_Relationship__c ir : listNewIndividualRelationship) {
                // individual relationship meets criteria for upsert
                if (mapIR2Upsert.containsKey(ir.Id)) {
                    // party has related farms
                    if (mapListRelatedFarms.containsKey(ir.Party__c)) {
                        for (Id farmId : mapListRelatedFarms.get(ir.Party__c)) {
                            String pfRole = mapPartyFarm2Role.get(string.valueof(ir.Party__c) + farmId);
                            string upsertKey = String.valueOf(farmId) + String.valueOf(ir.Party__c) + String.valueOf(ir.Individual__c) + derivedRelationshipRecordTypeId + ir.Role__c + pfRole;

                            // if the ir is active then update, else delete
                            if (ir.Active__c) {
                                listIndividualRelationship2Upsert.add(
                                    new Individual_Relationship__c(
                                        Party__c = ir.Party__c,
                                        Farm__c = farmId,
                                        RecordTypeId = derivedRelationshipRecordTypeId,
                                        Individual__c = ir.Individual__c,
                                        Derived_Relationship_Upsert_Key__c = upsertKey,
                                        Role__c = ir.Role__c,
                                        Active__c = ir.Active__c,
                                        PF_Role__c = mapPartyFarm2Role.get(string.valueof(ir.Party__c) + farmId)
                                    )
                                );
                            } else {
                                irsToDelete.add(upsertKey);
                            }
                        }
                    }
                }
            }

            upsert listIndividualRelationship2Upsert Derived_Relationship_Upsert_Key__c;
            if (irsToDelete.size() > 0) {
                delete [SELECT Id FROM Individual_Relationship__c WHERE Derived_Relationship_Upsert_Key__c IN : irsToDelete];
            }
        }
    }

    public static void setRuralProfessionalFlag(List<Individual_Relationship__c> triggerNew) {
        Id ioRecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Individual_Relationship__c' AND Name = 'Individual-Organisation'].Id;

        // if there are IO relationships in the trigger, add the Individuals to the individuals set
        Set<Id> individuals = new Set<Id>();
        for (Individual_Relationship__c i : triggerNew) {
            if (i.RecordTypeId == ioRecordTypeId) {
                individuals.add(i.Individual__c);
            }
        }

        if (individuals.size() > 0) {
            // look at all of the individual's IO relationships and add them to a map
            Map<Id, List<Individual_Relationship__c>> relationshipsByContactId = new Map<Id, List<Individual_Relationship__c>>();
            for (Individual_Relationship__c i : [SELECT Id, Individual__c, Active__c FROM Individual_Relationship__c WHERE Individual__c IN : individuals AND RecordType.Name = 'Individual-Organisation']) {
                if (!relationshipsByContactId.containsKey(i.Individual__c)) {
                    relationshipsByContactId.put(i.Individual__c, new List<Individual_Relationship__c>());
                }
                relationshipsByContactId.get(i.Individual__c).add(i);
            }

            List<Contact> contacts = [SELECT Id, Rural_Professional__c FROM Contact WHERE Id IN : individuals];

            // loop over each contact's IO relationships, if any are active, set the contact's Rural_Professional_c flag to true else set to false
            for (Contact c : contacts) {
                Boolean isRP = false;
                for (Individual_Relationship__c i : relationshipsByContactId.get(c.Id)) {
                    if (i.Active__c) {
                        isRP = true;
                    }
                }

                c.Rural_Professional__c = isRP;
            }

            update contacts;
        }
    }

    /**
     * Run before update - make sure that we set a deactivation date when an party IR goes inactive.
     */
    public static void deactivateDate(List<Individual_Relationship__c> listNewIndividualRelationship, Map<Id, Individual_Relationship__c> mapOldIndividualRelationship) {
        for (Individual_Relationship__c relationship : listNewIndividualRelationship) {
            Individual_Relationship__c oldRelationship = mapOldIndividualRelationship.get(relationship.Id);
            if (relationship.Active__c == false && oldRelationship.Active__c == true) {
                // Have transitioned to inactive, set the deactivation date to today
                relationship.Deactivation_Date__c = Date.today();
            } else if (relationship.Active__c == true && oldRelationship.Active__c == false) {
                // Reactivating - should never happen
                if (relationship.Deactivation_Date__c <= Date.today()) {
                    // Clear any old deactivation date if reactivating - leave a future one in place
                    relationship.Deactivation_Date__c = null;
                }
            }
        }
    }

}