@isTest
private class AMSNewsDetailControllerTest {

    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup static void testSetup() {
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
    }

    @isTest static void testController_WithValidNews() {
        String PREVIOUS_PAGE_NUMBER = '3';

        TestClassDataUtil.createAMSNewsArticle(1);
        User theUser = [SELECT Id FROM User WHERE Email = :USER_EMAIL];

        System.runAs(theUser) {
            Test.startTest();
            Knowledge__kav amsNews = [
                SELECT Id,
                    UrlName,
                    Title,
                    Summary,
                    Content__c,
                    Author__c,
                    Headline_Image__c,
                    Thumbnail_Image__c,
                    FirstPublishedDate
                FROM Knowledge__kav
                WHERE PublishStatus = 'Online'
                AND Language = 'en_US'
                LIMIT 1
            ];

            PageReference pageRef = Page.AMSNewsDetail;
            pageRef.getParameters().put(AMSCommunityUtil.getArticleParameterName(), amsNews.UrlName);
            pageRef.getParameters().put(AMSCommunityUtil.getPageNumberParameterName(), PREVIOUS_PAGE_NUMBER);
            System.Test.setCurrentPage(pageRef);

            AMSNewsDetailController controller = new AMSNewsDetailController();
            Test.stopTest();

            System.assertEquals(true, controller.newsArticlePresent, 'newsArticlePresent not set correctly');
            System.assertEquals(PREVIOUS_PAGE_NUMBER, controller.previousPageNumber, 'previousPageNumber not set correctly');
            System.assertEquals(amsNews.Id, controller.newsArticle.Id, 'newsArticle not set correctly');
        }
    }

    @isTest static void testController_WithNoNews() {
        User theUser = [SELECT Id FROM User WHERE Email = :USER_EMAIL];

        System.runAs(theUser) {
            Test.startTest();
            AMSNewsDetailController controller = new AMSNewsDetailController();
            Test.stopTest();

            System.assertEquals(false, controller.newsArticlePresent, 'newsArticlePresent not set correctly');
            System.assertEquals(null, controller.previousPageNumber, 'previousPageNumber not set correctly');
            System.assertEquals(null, controller.newsArticle.Id, 'newsArticle not set correctly');
        }
    }

}