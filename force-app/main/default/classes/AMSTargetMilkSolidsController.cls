/**
 * Created by Nick on 07/08/17.
 *
 * AMSTargetMilkSolidsController
 *
 * Controller for AMSTargetMilkSolids page
 *
 * Author: NL (Trineo)
 * Date: 2017-08-30
 **/
public without sharing class AMSTargetMilkSolidsController extends AMSProdQualReport {
    public AMSProdQualReport baseController { get { return this; } private set; }

    private static final String DEFAULT_REPORT_TYPE = 'Target Milksolids';

    public AMSCollectionService.CollectionPeriodForm collectionPeriodForm { get; set; }
    public transient AMSCollectionGraph collectionGraph { get; set; }

    public AMSTargetMilkSolidsController() {
        selectedReportTypeName = DEFAULT_REPORT_TYPE;
    }

    //Call recalculate for the forms
    public override void recalculateForm() {
        populateScopedDates();
        collectionPeriodForm = new AMSCollectionService.CollectionPeriodForm(
            scopedCurrentSeasonsFromDate,
            scopedCurrentSeasonsToDate,
            null,
            null,
            null,
            selectedFarmSeason,
            selectedFarmId,
            selectedPlotOption);

        AMSCollectionService.CollectionPeriodForm graphForm = new AMSCollectionService.CollectionPeriodForm(
            fromDate,
            toDate,
            null,
            null,
            null,
            selectedFarmSeason,
            selectedFarmId,
            selectedPlotOption);

        collectionGraph = new AMSCollectionGraph(graphForm, AMSCollectionService.KgMS);
        collectionPeriodForm.convertDateMapToListAscending(false);
    }

    //Call recalculation for tables on the page when a plot option is updated
    public void recalculateFromPlotOption() {
        //all associated farm seasons for selected farm (first farm in list)
        recalculateForm();
    }

    private void populateScopedDates() {
        if (monthList.isEmpty() || selectedPlotOption == AMSCollectionService.MONTH) {
            scopedCurrentSeasonsFromDate = fromDate;
            scopedCurrentSeasonsToDate = toDate;
        } else {
            Integer currentMonth = TODAY.month();
            Integer indexOfSelectedMonthInMonthList = indexOf(monthList, selectedMonth);

            //if the first month is selected, don't default the from date- cap the to date to the end of the month
            if (indexOfSelectedMonthInMonthList == 0) {
                scopedCurrentSeasonsFromDate = fromDate;
                scopedCurrentSeasonsToDate = fromDate.addMonths(indexOfSelectedMonthInMonthList + 1).toStartOfMonth().addDays(-1);
            //if not the last month, default the from date and to date to start and end of month
            } else if (indexOfSelectedMonthInMonthList != monthList.size() - 1) {
                scopedCurrentSeasonsFromDate = fromDate.addMonths(indexOfSelectedMonthInMonthList).toStartOfMonth();
                scopedCurrentSeasonsToDate = fromDate.addMonths(indexOfSelectedMonthInMonthList + 1).toStartOfMonth().addDays(-1);
            //if the last month, default the from date, don't default the end date
            } else {
                scopedCurrentSeasonsFromDate = fromDate.addMonths(indexOfSelectedMonthInMonthList).toStartOfMonth();
                scopedCurrentSeasonsToDate = toDate;
            }
        }
    }
}