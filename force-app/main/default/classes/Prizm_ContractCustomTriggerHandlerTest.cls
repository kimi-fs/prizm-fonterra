@isTest
public class Prizm_ContractCustomTriggerHandlerTest {
    @testSetup
    static void createData() {
        fsCore__User_Permission__c userPerm = fsCore.TestHelperSystem.getTestUserPermission(
            UserInfo.getUserId(),
            true,
            true,
            true
        );
        insert userPerm;
        fsCore.SeedCustomSettings.createCustomSettings(new Set<String>{fsCore.Constants.CUSTOM_SETTING_CUSTOM_NUMBER_FORMAT});
        
        fsCOre__Trigger_Execution_Settings__c testConTrgSettings = new fsCOre__Trigger_Execution_Settings__c();
        testConTrgSettings.Name = 'LendingContractTrigger';
        testConTrgSettings.fsCore__Custom_Trigger_Manager_Class__c = 'Prizm_ContractCustomTriggerManager';
        testConTrgSettings.fsCore__Number_Of_Trigger_Executions__c = 1;
        insert testConTrgSettings;
        
        List<Account> enitities = new List<Account>();       
        Account farmAccount = Prizm_TestDataHelper.farmAccount();
        Account partyAccount = Prizm_TestDataHelper.partyAccount();
        enitities.add(farmAccount);
        enitities.add(partyAccount);
        insert enitities;  
        
        Id farmRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Farm AU').getRecordTypeId();
        farmAccount = [Select id, Name from Account where RecordTypeId = :farmRecTypeId];
        
        Contact individualContact = Prizm_TestDataHelper.createFarmIndividual(farmAccount);
        insert individualContact;
        
        farmAccount.Primary_On_Farm_Contact__c = individualContact.id;
        update farmAccount;   
        
        farmAccount = [Select id, Name,Primary_On_Farm_Contact__c from Account where RecordTypeId = :farmRecTypeId];
        
        Case testCase = Prizm_TestDataHelper.createCase(farmAccount,partyAccount);
        insert testCase;
        
        Advance_Application__c advanceApplication = Prizm_TestDataHelper.createAdvanceApplication(testCase);
        insert advanceApplication;
        
        
        fscore__Company_Setup__c testCompany = fsCore.TestHelperCompany.getTestCompanySetup(
            'Test Company',
            'TESTCO',
            '123456789'
        );
        insert testCompany;
        System.assert(testCompany.Id != null, 'Test company created assert');
        
        fsCore__Branch_Setup__c testBranch = fsCore.TestHelperCompany.getTestBranchSetup(
            'Test Branch',
            'TBRCH',
            testCompany.Id,
            null,
            null
        );
        insert testBranch;
        System.assert(testBranch.Id != null, 'Test branch created assert');
        
        fsCore__Balance_Setup__c testBalance = fsCore.TestHelperFinancial.getTestBalanceSetup(
            'Test Balance Name',
            'Principal'
        );
        insert testBalance;
        System.assert(testBalance.Id != null, 'Test bal created assert');
        
        fsCore__Transaction_Setup__c testTransaction = fsCore.TestHelperFinancial.getTestTransactionSetup(
            'Test Transaction Name',
            testBalance.id,
            'Post'
        );
        testTransaction.fsCore__Is_Statement_Eligible__c =true;
        insert testTransaction;
        System.assert(testTransaction.Id != null, 'Test trnx created assert');
        
        List<fsCore__Itemization_Setup__c> testItemizations = new List<fsCore__Itemization_Setup__c>();
        fsCore__Itemization_Setup__c testItemization = fsCore.TestHelperFinancial.getTestItemizationSetup(
            'Test Itemization Name',
            'Financed Amount',
            testTransaction.Id
        );
        fsCore__Itemization_Setup__c testItemization1 = fsCore.TestHelperFinancial.getTestItemizationSetup(
            'Test Itemization Name1',
            ' Prepaid Fee',
            testTransaction.Id
        );
        testItemizations.add(testItemization);
        testItemizations.add(testItemization1);
        insert testItemizations;
        System.assert(testItemizations.size()>0, 'Test itemz created assert');
        
        fsCore__Product_Setup__c testProduct = fsCore.TestHelperProduct.getTestProductSetup(
            'Test Product',
            'PL',
            fsCore.Constants.PRODUCT_FAMILY_LOAN,
            fsCore.Constants.CYCLE_MONTHLY,
            1
        );
        insert testProduct;
        System.assert(testProduct.Id != null, 'Test product created assert');
        
        List<fsCore__Product_Itemization_Setup__c> testProductItzms = new List<fsCore__Product_Itemization_Setup__c>();
        testProductItzms.add(
            fsCore.TestHelperProduct.getTestProductItemizationSetup(
                testProduct.Id,
                testItemization,
                'Yes',
                1,
                fsCore.Constants.MINUS
            )
        );
        testProductItzms.add(
            fsCore.TestHelperProduct.getTestProductItemizationSetup(
                testProduct.Id,
                testItemization1,
                'No',
                2,
                fsCore.Constants.PLUS
            )
        );
        insert testProductItzms;
        System.assert(testProductItzms.size() > 0, 'Test product itemizations created assert');
        
        fsCore__Payment_Allocation_Method_Setup__c testPaymentAlloc = fsCore.TestHelperFinancial.getTestPaymentAllocMethod(
            'Test Payment Allocation'
        );
        insert testPaymentAlloc;
        System.assert(testPaymentAlloc.Id != null, 'Test Payment Allocation created assert');
        
        fsCore__Contract_Template_Setup__c testContractTemplate = fsCore.TestHelperProduct.getTestContractTemplateSetup(
            'Test Contract Template',
            fsCore.Constants.PRODUCT_FAMILY_LOAN,
            testPaymentAlloc.Id,
            1
        );
        insert testContractTemplate;
        System.assert(testContractTemplate.Id != null, 'Test contract template created assert');
        
        List<fsCore__Lending_Application__c> testApplications = new List<fsCore__Lending_Application__c>();
        fsCore__Lending_Application__c testApplication = fsCore.TestHelperLendingApplication.getTestApplicationWithContract(
            testCompany,
            testBranch,
            testProduct,
            testContractTemplate,
            'Test Application'
        );
        testApplication.fsCore__Primary_Customer_Account__c = farmAccount.id;
        testApplication.fsCore__Primary_Customer_Contact__c = individualContact.id;
        testApplication.Farm__c = farmAccount.id;
        testApplication.Party__c = partyAccount.id;
        testApplications.add(testApplication);
        
        insert testApplications;
        System.assert(testApplications.size()>0, 'Test Lending Applications created assert');
        
        List<fsCore__Lending_Application_Itemization__c> testAppItemizations = fsCore.TestHelperLendingApplication.getTestApplicationItemizations(
            testApplication,
            testProductItzms
        );
        insert testAppItemizations;
        System.assert(testAppItemizations.size() > 0, 'Test Lending Application itemizations created assert');
        
        
    }
    @isTest
    public static void testAdvanceRecCreation(){
        fsCore__Lending_Application__c testApplication = [Select id,fsCore__Primary_Customer_Account__c,
                                                          fsCore__Primary_Customer_Contact__c,
                                                          Farm__c,
                                                          Party__c
                                                          from fsCore__Lending_Application__c limit 1];
        System.debug('Application:--'+testApplication);
        
        fsCore__Branch_Setup__c testBranch = [Select Id, Name, 
                                              fsCore__Company_Name__c 
                                              from fsCore__Branch_Setup__c Limit 1];
        fsCore__Product_Setup__c testProduct = [Select id from fsCore__Product_Setup__c Limit 1];
        fsCore__Contract_Template_Setup__c testContractTemplate = [Select id from fsCore__Contract_Template_Setup__c];
        List<fsServ__Lending_Contract__c> testLendingContracts = new List<fsServ__Lending_Contract__c>();
        fsServ__Lending_Contract__c testLendingContract = new fsServ__Lending_Contract__c();
        testLendingContract.fsServ__Branch_Name__c = testBranch.Id;
        testLendingContract.fsServ__Company_Name__c = testBranch.fsCore__Company_Name__c;
        testLendingContract.fsServ__Product_Name__c = testProduct.Id;
        testLendingContract.fsServ__Contract_Template_Name__c = testContractTemplate.id;
        testLendingContract.fsServ__Financed_Amount__c = 1000;
        testLendingContract.fsServ__Contract_Date__c = Date.today();
        testLendingContract.fsServ__Current_Payment_Amount__c = 1000;
        testLendingContract.fsServ__Next_Payment_Due_Date__c = Date.today().addDays(30);
        testLendingContract.fsServ__Current_Payment_Cycle__c = 'Monthly';
        testLendingContract.fsServ__Lending_Application_Number__c = testApplication.id;
        testLendingContract.fsServ__Primary_Customer_Account__c = testApplication.fsCore__Primary_Customer_Account__c;
        testLendingContract.fsServ__Primary_Customer_Contact__c = testApplication.fsCore__Primary_Customer_Contact__c;
        testLendingContract.Farm__c = testApplication.Farm__c;
        testLendingContract.Party__c = testApplication.Party__c;
        
        testLendingContracts.add(testLendingContract);
        
        
        Test.startTest();
        insert testLendingContracts;
        Test.stopTest(); 
        
        
        
        Advance__c advRec = [Select id, Name, 
                             Current_Balance__c,
                             End_Date__c,
                             Farm__c,
                             Interest_Rate__c,
                             Lender__c,
                             Party__c,
                             Rate_Type__c,
                             Start_Date__c,
                             Lending_Contract_Number__c
                             from Advance__c where Lending_Contract_Number__c = :testLendingContracts[0].id];                     
        System.debug(loggingLevel.Error,'advRec:--'+advRec);
        System.assertEquals(1000, advRec.Current_Balance__c, 'Current balance is not correct');
        System.assertEquals(testLendingContracts[0].id, advRec.Lending_Contract_Number__c, 'Contract id is not mapped');
        System.assertEquals(testLendingContracts[0].Party__c, advRec.Party__c, 'Party is not mapped');
        System.assertEquals(testLendingContracts[0].Farm__c, advRec.Farm__c, 'Farm is not mapped');
        
        Id recTypeId = Schema.SObjectType.Advance_Transaction__c.getRecordTypeInfosByDeveloperName().get('Disbursement').getRecordTypeId();
        
        Advance_Transaction__c advTrnx = [Select id,Name,
                                          Transaction_Type__c,
                                          RecordTypeId,
                                          Advance_Number__c,
                                          Status__c,
                                          Lending_Contract_Number__c
                                          from Advance_Transaction__c];
        System.debug(loggingLevel.Error,'advTrnx:--'+advTrnx);
        System.assertEquals('01', advTrnx.Transaction_Type__c, 'Transaction type is not advance (01)');
        System.assertEquals(recTypeId, advTrnx.RecordTypeId, 'Record type is not disbursement');
        System.assertEquals(advRec.id, advTrnx.Advance_Number__c, 'Advance rec is not mapped');
        System.assertEquals('Ready', advTrnx.Status__c, 'Status is not correct, it should be Ready');
        
    }
}