/**
* Description: Test for AMSThirdPartyController
* @author: Ed (Trineo)
* @date: August 2017
*/
@isTest private class AMSThirdPartyControllerTest {

    private static final String USER_EMAIL = 'amsUser@thisTest.com';
    private static final String TPA_EMAIL = 'tpaUser@thisTest.com';

    /**
     * The goal is to create two individuals, the farmer and the tpa user
     * We then create a some parties and farms, and ERs between the two
     * We then create some IRs between the farmer and the parties, triggers will create
     * the derived IRs between the farmer and the farms.
     */
    @testSetup static void testSetup() {
        SetupAMSCommunityPageMessages.createPageMessages();

        TestClassDataUtil.integrationUserProfile();

        // Farmer
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        // TPA User
        User thirdPartyUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(TPA_EMAIL);

        // Reference Periods
        TestClassDataUtil.createReferencePeriods(5, GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU, true);

        // One Party
        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        // 3 Farms
        List<Account> farms = TestClassDataUtil.createAUFarms(3, true);

        // Connect the party with the 3 farms by ERs
        List<Entity_Relationship__c> entityRelationships = TestClassDataUtil.createPartyFarmEntityRelationships(new Map<Account, List<Account>>{party => farms}, true);

        // Connect the Farmer with the Party - which will create derived IRs between the individual and the farms.
        List<Individual_Relationship__c> individualRelationships = TestClassDataUtil.createIndividualPartyRelationships(communityUser.ContactId, new List<Account>{party}, true);

        // Create a Third Party to one of the farms

    }

    @isTest static void testConstructor() {
        Test.startTest();
        Map<String, Integer> farmRelationshipCountMap = new Map<String, Integer>();
        for (Individual_Relationship__c ir : [SELECT Id, Farm__c FROM Individual_Relationship__c]) {
            if (!farmRelationshipCountMap.containsKey(ir.Farm__c)) {
                farmRelationshipCountMap.put(ir.Farm__c, 0);
            }
            farmRelationshipCountMap.put(ir.Farm__c, farmRelationshipCountMap.get(ir.Farm__c) + 1);
        }

        System.runAs(getCommunityUser()) {
            AMSThirdPartyController controller = new AMSThirdPartyController();

            for (AMSThirdPartyController.FarmWrapper farmWrapper : controller.farmRelationships) {
                System.assertEquals(0, farmWrapper.relationships.size(), 'we didn\'t create any TPA relationships');
            }
        }
        Test.stopTest();
    }

    @isTest static void testGetRelationshipRoleOptions() {
        Test.startTest();
        List<Schema.PicklistEntry> irRolePicklistEntry = Individual_Relationship__c.Role__c.getDescribe().getPicklistValues();
        List<SelectOption> roleOptions = AMSThirdPartyController.getRelationshipRoleOptions();

        // Roles size + 1 as we've added a "first option" dynamically- in the controller
        System.assertEquals(irRolePicklistEntry.size() + 1, roleOptions.size(), 'Wrong number of role options');
        Test.stopTest();
    }

    @isTest static void testGetReferencePeriods() {
        Test.startTest();
        List<Reference_Period__c> refPeriods = new List<Reference_Period__c>();
        System.runAs(getCommunityUser()) {
            AMSThirdPartyController controller = new AMSThirdPartyController();
            refPeriods = controller.getReferencePeriods();
        }

        System.assertEquals(AMSThirdPartyController.NUMBER_OF_REFERENCE_PERIODS, refPeriods.size(), 'Wrong number of reference periods returned');
        Test.stopTest();
    }

    @isTest static void testEditRelationship() {
        Individual_Relationship__c tpRelationship = createTPARelationship();
        Individual_Relationship__c irToEdit = [SELECT Id, Role__c, Individual__r.Email FROM Individual_Relationship__c WHERE Id = :tpRelationship.Id LIMIT 1];

        Test.startTest();
        System.runAs(getCommunityUser()) {
            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.relationshipToEdit = irToEdit.Id;
            controller.editRelationship();

            System.debug('irToEdit: ' + irToEdit);
            System.assertEquals(irToEdit.Role__c, controller.relationshipRole, 'Role not set correctly');
            System.assertEquals(irToEdit.Individual__r.Email, controller.relationshipUsername, 'Username not set correctly');

            for (AMSThirdPartyController.FarmWrapper fw : controller.farmRelationships) {
                for (AMSThirdPartyController.RelationshipWrapper rw : fw.relationships) {
                    if (rw.relationship.Id == irToEdit.Id) {
                        System.assertEquals(true, rw.selected, 'Selected not set to true');
                        System.assertEquals(true, fw.selected, 'Selected not set to true');
                    }
                }
            }
        }
        Test.stopTest();
    }

    @isTest static void testDeleteRelationship() {
        Individual_Relationship__c tpRelationship = createTPARelationship();
        Individual_Relationship__c irToDelete = [SELECT Id, Role__c, Individual__r.Email FROM Individual_Relationship__c WHERE Id = :tpRelationship.Id LIMIT 1];

        Test.startTest();
        System.runAs(getCommunityUser()) {
            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.relationshipToDelete = irToDelete.Id;
            controller.deleteRelationship();

            Boolean matched = false;
            for (AMSThirdPartyController.FarmWrapper fw : controller.farmRelationships) {
                for (AMSThirdPartyController.RelationshipWrapper rw : fw.relationships) {
                    if (rw.relationship.Id == irToDelete.Id) {
                        matched = true;
                    }
                }
            }
            System.assert(!matched, 'Relationship still exists in wrappers');
        }
        Test.stopTest();
    }

    @isTest static void testAddRelationship() {
        Test.startTest();
        System.runAs(getCommunityUser()) {
            AMSThirdPartyController controller = new AMSThirdPartyController();

            AMSThirdPartyController.FarmWrapper farmToAddTo = controller.farmRelationships[0];
            controller.farmId = farmToAddTo.farmId;
            controller.addRelationship();

            System.assertEquals(0, controller.currentPanelNumber, 'Wrong panel number');
            System.assertEquals(false, controller.showAccess, 'Wrong showAccess value');
            System.assertEquals(true, controller.newRelationship, 'Wrong newRelationship value');
        }
        Test.stopTest();
    }

    @isTest static void testAddRelationship_NoFarmSelected() {
        Test.startTest();
        System.runAs(getCommunityUser()) {
            AMSThirdPartyController controller = new AMSThirdPartyController();

            controller.addRelationship();
        }
        Test.stopTest();
    }

    @isTest static void testCancel() {
        Test.startTest();
        System.runAs(getCommunityUser()) {
            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.showAccess = false;
            controller.currentPanelNumber = 3;
            controller.newRelationship = true;
            controller.relationshipUsername = 'test';
            controller.relationshipRole = 'test';
            controller.relationshipToEdit = 'test';

            controller.cancel();

            System.assertEquals(true, controller.showAccess);
            System.assertEquals(0, controller.currentPanelNumber);
            System.assertEquals(false, controller.newRelationship);
            System.assertEquals('', controller.relationshipUsername);
            System.assertEquals('', controller.relationshipRole);
            System.assertEquals('', controller.relationshipToEdit);
        }
        Test.stopTest();
    }

    @isTest static void testBack() {
        Test.startTest();
        System.runAs(getCommunityUser()) {
            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.currentPanelNumber = 3;

            controller.back();

            System.assertEquals(2, controller.currentPanelNumber);
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWhat() {
        Test.startTest();
        createTPARelationship();

        System.runAs(getCommunityUser()) {

            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller = selectEditingWrapper(controller);
            controller.currentPanelNumber = 2;

            controller.next();

            System.assertEquals(3, controller.currentPanelNumber, 'currentPanelNumber did not increment');
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWhat_NoAccess() {
        Test.startTest();
        createTPARelationship(false);
        System.runAs(getCommunityUser()) {

            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller = selectEditingWrapper(controller);
            controller.currentPanelNumber = 2;

            controller.next();

            System.assertEquals(2, controller.currentPanelNumber, 'currentPanelNumber did not increment');
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWhen() {
        Test.startTest();
        createTPARelationship();

        System.runAs(getCommunityUser()) {

            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller = selectEditingWrapper(controller);
            controller.currentPanelNumber = 3;

            controller.next();

            System.assertEquals(4, controller.currentPanelNumber, 'currentPanelNumber did not increment');
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWhen_DatePast() {
        Test.startTest();
        createTPARelationship();

        System.runAs(getCommunityUser()) {

            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller = selectEditingWrapper(controller);
            controller.currentPanelNumber = 3;

            for (AMSThirdPartyController.FarmWrapper fw : controller.farmRelationships) {
                for (AMSThirdPartyController.RelationshipWrapper rw : fw.relationships) {
                    if (rw.selected) {
                        rw.relationship.Access_End__c = Date.today().addDays(-1);
                    }
                    break;
                }
            }

            controller.next();

            System.assertEquals(3, controller.currentPanelNumber, 'currentPanelNumber did not increment');
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWho_NoEmail() {
        Test.startTest();
        System.runAs(getCommunityUser()) {

            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.currentPanelNumber = 0;
            controller.relationshipRole = 'role';
            controller.relationshipUsername = 'NotExistingEmail@nope.com';
            controller.next();

            System.assertNotEquals('', controller.message, 'message not populated');
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWho_BlankEmail() {
        Test.startTest();
        System.runAs(getCommunityUser()) {


            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.currentPanelNumber = 0;
            controller.relationshipRole = 'role';
            controller.relationshipUsername = '';
            controller.next();

            System.assertEquals(0, controller.currentPanelNumber, 'currentPanelNumber did not increment');
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWho_ExistingContact() {
        Test.startTest();
        Contact tpaContact = [SELECT Id FROM Contact WHERE Email = :TPA_EMAIL];

        System.runAs(getCommunityUser()) {

            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.currentPanelNumber = 0;
            controller.relationshipUsername = TPA_EMAIL;
            controller.relationshipRole = 'New Role';
            controller.next();

            System.assertEquals(1, controller.currentPanelNumber, 'currentPanelNumber did not increment');
            System.assertEquals(tpaContact.Id, controller.relationshipIndividualId, 'Contact not found');
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWho_NoContact() {
        Test.startTest();
        System.runAs(getCommunityUser()) {

            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.currentPanelNumber = 0;
            controller.relationshipUsername = 'no@contact.email';

            controller.next();

            System.assertEquals(0, controller.currentPanelNumber, 'currentPanelNumber should not have incremented');
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWho_NoInput() {
        Test.startTest();
        Contact tpaContact = [SELECT Id FROM Contact WHERE Email = :TPA_EMAIL];

        System.runAs(getCommunityUser()) {

            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.currentPanelNumber = 0;
            controller.relationshipUsername = '';

            controller.next();

            System.assertEquals(0, controller.currentPanelNumber, 'currentPanelNumber should not have incremented');
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWhich_ExistingRelationship_New() {
        Individual_Relationship__c tpRelationship = createTPARelationship();

        Test.startTest();
        System.runAs(getCommunityUser()) {

            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.currentPanelNumber = 1;

            controller.next();

            System.assertEquals(1, controller.currentPanelNumber, 'no farms selected - so should not increment');

            // Find the TPA from above and use the info from it
            for (AMSThirdPartyController.FarmWrapper wrapper : controller.farmRelationships) {
                for (AMSThirdPartyController.RelationshipWrapper relWrapper : wrapper.relationships) {
                    if (relWrapper.relationship.Id == tpRelationship.Id) {
                        wrapper.selected = true;
                        controller.relationshipIndividualId = relWrapper.relationship.Individual__c;
                        controller.relationshipRole = relWrapper.relationship.Role__c;
                        break;
                    }
                }
            }

            controller.next();

            System.assertEquals(1, controller.currentPanelNumber, 'currentPanelNumber should not increment');
            System.assertNotEquals('', controller.message, 'message should be populate');
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWhich_ExistingRelationship_Edit() {
        Individual_Relationship__c tpRelationship = createTPARelationship();

        Test.startTest();
        System.runAs(getCommunityUser()) {

            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.currentPanelNumber = 1;

            controller.next();

            System.assertEquals(1, controller.currentPanelNumber, 'no farms selected - so should not increment');

            // Find the TPA from above and use the info from it
            for (AMSThirdPartyController.FarmWrapper wrapper : controller.farmRelationships) {
                for (AMSThirdPartyController.RelationshipWrapper relWrapper : wrapper.relationships) {
                    if (relWrapper.relationship.Id == tpRelationship.Id && relWrapper.relationship.RecordTypeId == GlobalUtility.individualRestrictedFarmRecordTypeId) {
                        System.debug('*** FOUND A MATCH ***');
                        wrapper.selected = true;
                        controller.relationshipToEdit = relWrapper.relationship.Id;
                        controller.relationshipIndividualId = relWrapper.relationship.Individual__c;
                        controller.relationshipRole = relWrapper.relationship.Role__c;
                        break;
                    }
                }
            }

            controller.next();

            System.assertEquals(2, controller.currentPanelNumber, 'currentPanelNumber should increment');
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWhich_NoExistingRelationship() {
        Test.startTest();
        Contact tpaContact = [SELECT Id FROM Contact WHERE Email = :TPA_EMAIL];

        System.runAs(getCommunityUser()) {
            final String NEW_ROLE = 'New Role';

            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.currentPanelNumber = 1;

            // Creating a new relationship - so just select the first farm and our tpaContact that we created
            // earlier.
            controller.farmRelationships[0].selected = true;
            controller.relationshipIndividualId = tpaContact.Id;
            controller.relationshipRole = NEW_ROLE;

            controller.next();

            System.assertEquals(2, controller.currentPanelNumber, 'currentPanelNumber did not increment');
            Boolean newIR = false;
            for (AMSThirdPartyController.RelationshipWrapper rw : controller.farmRelationships[0].relationships) {
                if (rw.relationship.Role__c == NEW_ROLE) {
                    newIR = true;
                }
            }
            System.assertEquals(true, newIR, 'New relationship not created');
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelWhich_NoneSelected() {
        Test.startTest();
        System.runAs(getCommunityUser()) {
            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.currentPanelNumber = 1;
            controller.next();
            System.assertEquals(1, controller.currentPanelNumber);
        }
        Test.stopTest();
    }

    @isTest static void testNext_CurrentPanelConfirm() {
        Test.startTest();
        System.runAs(getCommunityUser()) {

            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller.currentPanelNumber = 4;

            controller.next();

            System.assertEquals(true, controller.showAccess);
            System.assertEquals(0, controller.currentPanelNumber);
            System.assertEquals(false, controller.newRelationship);
            System.assertEquals('', controller.relationshipUsername);
            System.assertEquals('', controller.relationshipRole);
            System.assertEquals('', controller.relationshipToEdit);
        }
        Test.stopTest();
    }

    @isTest static void testNewDateTypeSelected_Indefinitely() {
        Test.startTest();
        createTPARelationship();

        System.runAs(getCommunityUser()) {
            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller = selectEditingWrapper(controller);
            for (AMSThirdPartyController.FarmWrapper fw : controller.farmRelationships) {
                for (AMSThirdPartyController.RelationshipWrapper rw : fw.relationships) {
                    controller.populateDateStringFromDates(rw);
                }
            }
            controller.dateTypeSelected = 'INDEFINITELY';

            controller.newDateTypeSelected();
        }
        Test.stopTest();
    }

    @isTest static void testNewDateTypeSelected_Cherrypick() {
        Test.startTest();
        createTPARelationship();

        System.runAs(getCommunityUser()) {
            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller = selectEditingWrapper(controller);
            for (AMSThirdPartyController.FarmWrapper fw : controller.farmRelationships) {
                for (AMSThirdPartyController.RelationshipWrapper rw : fw.relationships) {
                    controller.populateDateStringFromDates(rw);
                }
            }
            controller.dateTypeSelected = 'CHERRYPICK';

            controller.newDateTypeSelected();
        }
        Test.stopTest();
    }

    @isTest static void testNewDateTypeSelected_Season() {
        Test.startTest();
        createTPARelationship();

        System.runAs(getCommunityUser()) {
            AMSThirdPartyController controller = new AMSThirdPartyController();
            controller = selectEditingWrapper(controller);
            for (AMSThirdPartyController.FarmWrapper fw : controller.farmRelationships) {
                for (AMSThirdPartyController.RelationshipWrapper rw : fw.relationships) {
                    controller.populateDateStringFromDates(rw);
                }
            }
            controller.dateTypeSelected = 'SEASON';

            controller.newDateTypeSelected();
        }
        Test.stopTest();
    }

    private static Individual_Relationship__c createTPARelationship(Boolean hasAccess) {
        Contact tpaContact = [SELECT Id FROM Contact WHERE Email = :TPA_EMAIL];
        Entity_Relationship__c anyER = [SELECT Id, Party__c, Farm__c FROM Entity_Relationship__c WHERE Active__c = true LIMIT 1];
        Individual_Relationship__c tpaRelationship = new Individual_Relationship__c(
                                                            Active__c = true,
                                                            Individual__c = tpaContact.Id,
                                                            Role__c = 'Tester',
                                                            Farm__c = anyER.Farm__c,
                                                            Access_Sponsoring__c = anyER.Id,
                                                            Party__c = anyER.Party__c,
                                                            Access_Begin__c = Date.today().addDays(-1),
                                                            Access_End__c = Date.today().addDays(10),
                                                            RecordTypeId = GlobalUtility.individualRestrictedFarmRecordTypeId);

        if (hasAccess) {
            tpaRelationship.Access_Statements__c = true;
        }

        insert tpaRelationship;
        return tpaRelationship;
    }

    private static Individual_Relationship__c createTPARelationship() {
        return createTPARelationship(true);
    }

    private static AMSThirdPartyController selectEditingWrapper(Individual_Relationship__c relationshipToEdit, AMSThirdPartyController controller) {
        controller.relationshipToEdit = relationshipToEdit.Id;
        controller.relationshipIndividualId = relationshipToEdit.Individual__c;
        controller.relationshipRole = relationshipToEdit.Role__c;

        controller.editRelationship();

        return controller;
    }

    private static AMSThirdPartyController selectEditingWrapper(AMSThirdPartyController controller) {
        Individual_Relationship__c relationshipToEdit;
        for (AMSThirdPartyController.FarmWrapper fw : controller.farmRelationships) {
            for (AMSThirdPartyController.RelationshipWrapper rw : fw.relationships) {
                relationshipToEdit = rw.relationship;
                controller.selectedRelationship = rw.Identity;
                break;
            }
        }
        if (relationshipToEdit == null) {
            System.assert(false, 'Couldn\'t select a relationship to edit.');
        } else {
            controller = selectEditingWrapper(relationshipToEdit, controller);
        }
        return controller;
    }

    private static User getCommunityUser() {
        User theUser = [SELECT Id FROM User WHERE Email = :USER_EMAIL ];
        return theUser;
    }
}