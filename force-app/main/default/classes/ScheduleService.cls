/**
 * Description:
 * Service class for managing the schedulable classes. Within custom metadata are stored
 * the cron expressions, this class makes those available and acts as a single point of
 * contact to start/stop all of the scheduled jobs.
 *
 * If you want to adjust the schedule of a job then edit the custom metadata Scheduler_CRON__mdt
 * cron expression, and then in Exec Anon execute ScheduleService.schedule();
 *
 * NOTE. Because this class is referencing lots of scheduled job classes to edit and save
 *       this class you may well have to run ScheduleService.terminate() first.
 *
 * @author: Ed (Trineo)
 * @date: July 2018
 * @test: ScheduleServiceTest
 */
public with sharing class ScheduleService {
    public class NoCronExpressionException extends Exception {}
    public class NotRunAsBatchProcessUserException extends Exception {}

    @testVisible
    private static final String BATCH_PROCESS_AU = 'Batch Process AU';

    private static List<Scheduler_CRON__mdt> schedulerCRONs;

    private static Set<String> jobNamesSetAU;

    static {
        //Get all records from the Scheduler_CRON__mdt metadata
        schedulerCRONs = [SELECT DeveloperName,
                                 CRON_Expression__c,
                                 Job_Name__c,
                                 Run_As_User__c
                          FROM Scheduler_CRON__mdt];

        populateCrons(schedulerCRONs);
    }

    @testVisible
    private static void mockSchedulerCrons(List<Scheduler_CRON__mdt> mockedCrons) {
        schedulerCRONs = mockedCrons;
        populateCrons(schedulerCRONs);
    }

    private static void populateCrons(List<Scheduler_CRON__mdt> theCrons) {
        jobNamesSetAU = new Set<String>();

        //Add all of our job names to either NZ or AU job name set
        for(Scheduler_CRON__mdt cron : theCrons){
            if(cron.Run_As_User__c == BATCH_PROCESS_AU){
                jobNamesSetAU.add(cron.Job_Name__c);
            }
        }
    }

    /**
     * Terminate all existing schedules - and reschedule everything. Make sure that new schedules go in here of they
     * will not be restarted!
     */
    public static void schedule() {
        String userName = UserInfo.getName();

        try {
            //Only Batch Process users can schedule all batch classes
            if (userName == BATCH_PROCESS_AU){
                terminateAll(jobNamesSetAU);
                AMSScheduleBatchDailyMilk.schedule();
                AMSScheduleBatchStatementCleanup.schedule();
                ScheduleBatchCheckSMSDeliverability.schedule();
                ScheduleBatchERELActiveToggle.schedule();
                ScheduleBatchFarmCollectionTotals.schedule();
                ScheduleBatchLightningSyncSummary.schedule();
                ScheduleBatchSetActiveAddress.schedule();
                ScheduleBatchSetActiveAgreement.schedule();
                ScheduleBatchSetReferencePeriodStatus.schedule();
                ScheduleBatchSendApprovalReminder.schedule();
            } else {
                throw new NotRunAsBatchProcessUserException('The schedule method must be run as the Batch Process or Batch Process AU user');
            }
        }
        catch (Exception e) {
            throw new NotRunAsBatchProcessUserException('The schedule method must be run as the Batch Process or Batch Process AU user' + ' Error: ' + e.getMessage());
        }
    }

    /**
     * Abort all jobs listed in the Scheduler_CRON__mdt custom metadata
     */
    public static void terminateAll(Set<String> jobNamesSet) {
        List<CronTrigger> existingJobs = [SELECT Id, CronJobDetail.Name, CronJobDetail.JobType
                                          FROM CronTrigger
                                          WHERE CronjobDetail.Name IN :jobNamesSet];

        for (CronTrigger job : existingJobs) {
            System.abortJob(job.Id);
        }
    }

    /**
     * Abort job(s) based on the job name
     */
    public static void terminate(String jobName) {
        List<CronTrigger> existingJobs = [SELECT Id, CronJobDetail.Name, CronJobDetail.JobType
                                          FROM CronTrigger
                                          WHERE CronjobDetail.Name = :jobName];

        for (CronTrigger job : existingJobs) {
            System.abortJob(job.Id);
        }
    }

    /**
     * Stop all of the scheduled tasks for both AU and NZ
     */
    public static void terminateAll() {
        terminateAll(jobNamesSetAU);
    }

    /**
     * Get the expression from the custom setting, if found, null is a valid return value if it is found
     * but has no expression.
     *
     */
    public static String getCRONExpression(String instanceName) {

        String expression = null;

        Scheduler_CRON__mdt cron = getCRON(instanceName);

        if (cron != null && cron.CRON_Expression__c != null) {
            expression = cron.CRON_Expression__c;
        }

        return expression;
    }

    /**
     * Get the job name (required field) from the custom setting
     *
     * Throw an exception if not found.
     */
    public static String getCRONJobName(String instanceName) {
        String jobName = null;
        try {
            Scheduler_CRON__mdt cron = getCRON(instanceName);

            if (cron != null && cron.Job_Name__c != null) {
                jobName = cron.Job_Name__c;
            } else {
                throw new NoCronExpressionException('No job name was found for instance "' + instanceName + '"');
            }
        } catch (Exception e) {
            throw new NoCronExpressionException('No job name was found for instance "' + instanceName + '" Error: ' + e.getMessage());
        }
        return jobName;
    }

    /**
     * Get a single scheduler CRON record from the custom metadata based on record name
     * return null if none found
     */
    public static Scheduler_CRON__mdt getCRON(String instanceName) {
        for(Scheduler_CRON__mdt cron : schedulerCRONs){
            if(cron.DeveloperName == instanceName){
                return cron;
            }
        }
        return null;
    }
}