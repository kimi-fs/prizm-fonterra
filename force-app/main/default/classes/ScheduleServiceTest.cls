/**
 * Description:
 * Test the ScheduleService.  The metadata comes from the actual Org, and
 * is an unknown. We are not allowed to create new metadata in the UT, so will instead do simple tests.
 *
 * @author: Ed (Trineo)
 * @date: July 2018
 */
@isTest
private class ScheduleServiceTest {
	@isTest
    private static void testExpressionFetch() {
        List<Scheduler_CRON__mdt> allExpressions = [SELECT DeveloperName,
                                                             CRON_Expression__c,
                                                             Job_Name__c,
                                                             Run_As_User__c
                                                      FROM Scheduler_CRON__mdt];

        String checkExpressionName;
        if (allExpressions.size() > 0) {
            checkExpressionName = allExpressions[0].DeveloperName;
        }

        if (checkExpressionName != null) {
            String expression = ScheduleService.getCRONExpression(checkExpressionName);
            String jobName = ScheduleService.getCRONJobName(checkExpressionName);
        }
    }

    @isTest
    private static void testSchedule() {
        User batchProcessAU = TestClassDataUtil.createUserByProfile('Integration User', 'Batch Process AU');
        batchProcessAU.UserName = 'batchProcessAU@fonterra.com';
        batchProcessAU.Email = 'batchProcessAU@fonterra.com';
        batchProcessAU.Alias = 'bpa';
        insert batchProcessAU;
        // Just a coverage test - nothing to really check for here
        System.runAs(batchProcessAU){
            ScheduleService.schedule();
        }

        ScheduleService.terminateAll();

        ScheduleService.terminate('No such job');

        // This will throw an exception (I hope!)
        try {
            ScheduleService.schedule();
            System.assert(false, 'Should never reach here - should have thrown an exception for being the wrong user');
        } catch (ScheduleService.NotRunAsBatchProcessUserException e) {

        }

        try {
            ScheduleService.getCRONJobName('No such job');
            System.assert(false, 'Should never reach here - should have thrown an exception for not finding a cron expression for that job');
        } catch (ScheduleService.NoCronExpressionException e) {

        }
    }
}