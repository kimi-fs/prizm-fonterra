@isTest
public class Prizm_AdvanceTrnxTriggerHandlerTest {
 @testSetup
    static void createData() {
        fsCore__User_Permission__c userPerm = fsCore.TestHelperSystem.getTestUserPermission(
            UserInfo.getUserId(),
            true,
            true,
            true
        );
        insert userPerm;
        fsCore.SeedCustomSettings.createCustomSettings(new Set<String>{fsCore.Constants.CUSTOM_SETTING_CUSTOM_NUMBER_FORMAT});
        fsServ.TestHelperGlobal.createSetupData();
        
        fsCOre__Trigger_Execution_Settings__c testAdvTrnxSettings = new fsCOre__Trigger_Execution_Settings__c();
		testAdvTrnxSettings.Name = 'Prizm_AdvanceTransactionTrigger';
		testAdvTrnxSettings.fsCore__Custom_Trigger_Manager_Class__c = 'Prizm_AdvanceTransactionTriggerManager';
		testAdvTrnxSettings.fsCore__Number_Of_Trigger_Executions__c = 1;
        insert testAdvTrnxSettings;
        
        fsCOre__Trigger_Execution_Settings__c testConTrgSettings = new fsCOre__Trigger_Execution_Settings__c();
		testConTrgSettings.Name = 'LendingContractTrigger';
		testConTrgSettings.fsCore__Custom_Trigger_Manager_Class__c = 'Prizm_ContractCustomTriggerManager';
		testConTrgSettings.fsCore__Number_Of_Trigger_Executions__c = 1;
        insert testConTrgSettings;
        
        List<Account> enitities = new List<Account>();       
        Account farmAccount = Prizm_TestDataHelper.farmAccount();
        Account partyAccount = Prizm_TestDataHelper.partyAccount();
        enitities.add(farmAccount);
        enitities.add(partyAccount);
        insert enitities;  
        
        Id farmRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Farm AU').getRecordTypeId();
        farmAccount = [Select id, Name from Account where RecordTypeId = :farmRecTypeId];
        
        Contact individualContact = Prizm_TestDataHelper.createFarmIndividual(farmAccount);
        insert individualContact;
        
        farmAccount.Primary_On_Farm_Contact__c = individualContact.id;
        update farmAccount;   
        
        farmAccount = [Select id, Name,Primary_On_Farm_Contact__c from Account where RecordTypeId = :farmRecTypeId];
        
        Case testCase = Prizm_TestDataHelper.createCase(farmAccount,partyAccount);
        insert testCase;
        
        Advance_Application__c advanceApplication = Prizm_TestDataHelper.createAdvanceApplication(testCase);
        insert advanceApplication;
        
         fsCore.DynamicQueryBuilder companyQuery = fsCore.DynamicQueryFactory.createQuery(Schema.SObjectType.fscore__Company_Setup__c.getName())
                                                  .addFields()
                                                  .setRecordLimit(1);
        fscore__Company_Setup__c testCompany = (fscore__Company_Setup__c)Database.query(companyQuery.getQueryString());
        System.assert(testCompany.Id != null, 'Test company created assert');
        
        fsCore.DynamicQueryBuilder branchQuery = fsCore.DynamicQueryFactory.createQuery(Schema.SObjectType.fsCore__Branch_Setup__c.getName())
                                                  .addFields()
                                                  .setRecordLimit(1);
        fsCore__Branch_Setup__c testBranch = (fsCore__Branch_Setup__c)Database.query(branchQuery.getQueryString());
        System.assert(testBranch.Id != null, 'Test branch created assert');
        
         fsCore.DynamicQueryBuilder productQuery = fsCore.DynamicQueryFactory.createQuery(Schema.SObjectType.fsCore__Product_Setup__c.getName())
                                                  .addFields()
                                                  .setRecordLimit(1);
        fsCore__Product_Setup__c testProduct =  (fsCore__Product_Setup__c)Database.query(productQuery.getQueryString());
        System.assert(testProduct.Id != null, 'Test product created assert');

        fsCore.DynamicQueryBuilder contractTempQuery = fsCore.DynamicQueryFactory.createQuery(Schema.SObjectType.fsCore__Contract_Template_Setup__c.getName())
                                                  .addFields()
                                                  .setRecordLimit(1);
        fsCore__Contract_Template_Setup__c testContractTemplate = (fsCore__Contract_Template_Setup__c)Database.query(contractTempQuery.getQueryString());
        System.assert(testContractTemplate.Id != null, 'Test contract template created assert');
        
        List<fsCore__Lending_Application__c> testApplications = new List<fsCore__Lending_Application__c>();
        fsCore__Lending_Application__c testApplication = fsCore.TestHelperLendingApplication.getTestApplicationWithContract(
            testCompany,
            testBranch,
            testProduct,
            testContractTemplate,
            'Test Application'
        );
        testApplication.fsCore__Primary_Customer_Account__c = farmAccount.id;
        testApplication.fsCore__Primary_Customer_Contact__c = individualContact.id;
        testApplication.Farm__c = farmAccount.id;
        testApplication.Party__c = partyAccount.id;
        testApplications.add(testApplication);
        
        insert testApplications;
        System.assert(testApplications.size()>0, 'Test Lending Applications created assert');
      
        List<fsServ__Lending_Contract__c> testLendingContracts = new List<fsServ__Lending_Contract__c>();
        fsServ__Lending_Contract__c testLendingContract = new fsServ__Lending_Contract__c();
        testLendingContract.fsServ__Branch_Name__c = testBranch.Id;
        testLendingContract.fsServ__Company_Name__c = testBranch.fsCore__Company_Name__c;
        testLendingContract.fsServ__Product_Name__c = testProduct.Id;
        testLendingContract.fsServ__Contract_Template_Name__c = testContractTemplate.id;
        testLendingContract.fsServ__Financed_Amount__c = 1000;
        testLendingContract.fsServ__Contract_Date__c = Date.today();
        testLendingContract.fsServ__Current_Payment_Amount__c = 1000;
        testLendingContract.fsServ__Next_Payment_Due_Date__c = Date.today().addDays(30);
        testLendingContract.fsServ__Current_Payment_Cycle__c = 'Monthly';
        testLendingContract.fsServ__Lending_Application_Number__c = testApplications[0].id;
        testLendingContract.fsServ__Primary_Customer_Account__c = testApplications[0].fsCore__Primary_Customer_Account__c;
        testLendingContract.fsServ__Primary_Customer_Contact__c = testApplications[0].fsCore__Primary_Customer_Contact__c;
        testLendingContract.Farm__c = testApplications[0].Farm__c;
        testLendingContract.Party__c = testApplications[0].Party__c;
        
        testLendingContracts.add(testLendingContract);
         insert testLendingContracts;
    }
    
    @isTest
    public static void testContractActivation(){

        fsServ__Lending_Contract__c contract = [Select id,
                                                Name,
                                                Party__c, 
                                                Farm__c,
                                                fsServ__Is_Active__c,
                                                fsServ__Contract_Status__c
                                                from fsServ__Lending_Contract__c Limit 1];
        System.assertEquals('New', contract.fsServ__Contract_Status__c, 'Contract Status is not Pending');
        System.assertEquals(false, contract.fsServ__Is_Active__c, 'Contract is already active');

        Advance__c advRec = [Select id, Name, 
                             Current_Balance__c,
                             End_Date__c,
                             Farm__c,
                             Interest_Rate__c,
                             Lender__c,
                             Party__c,
                             Rate_Type__c,
                             Start_Date__c,
                             Lending_Contract_Number__c
                             from Advance__c where Lending_Contract_Number__c = :contract.id];                     
        System.debug(loggingLevel.Error,'advRec:--'+advRec);
        System.assertEquals(1000, advRec.Current_Balance__c, 'Current balance is not correct');
        System.assertEquals(contract.id, advRec.Lending_Contract_Number__c, 'Contract id is not mapped');
        System.assertEquals(contract.Party__c, advRec.Party__c, 'Party is not mapped');
        System.assertEquals(contract.Farm__c, advRec.Farm__c, 'Farm is not mapped');
        
        Id recTypeId = Schema.SObjectType.Advance_Transaction__c.getRecordTypeInfosByDeveloperName().get('Disbursement').getRecordTypeId();

        Advance_Transaction__c advTrnx = [Select id,Name,
                                          Transaction_Type__c,
                                          RecordTypeId,
                                          Advance_Number__c,
                                          Status__c,
                                          Lending_Contract_Number__c
                                          from Advance_Transaction__c];
        System.debug(loggingLevel.Error,'advTrnx:--'+advTrnx);
        System.assertEquals(recTypeId, advTrnx.RecordTypeId, 'Record type is not disbursement');
        System.assertEquals(advRec.id, advTrnx.Advance_Number__c, 'Advance rec is not mapped');
        System.assertEquals('Ready', advTrnx.Status__c, 'Status is not correct, it should be Ready');
        
        advTrnx.Status__c = 'Processed';
        
        Test.startTest();
        update advTrnx;
        Test.stopTest();
        
     fsServ__Contract_Disbursement__c contractDisbursementRec = [Select id from fsServ__Contract_Disbursement__c];
     System.assert(contractDisbursementRec.Id != null, 'Contract Disbursement Record not created');
        
        contract = [Select id,
                    Name,
                    Party__c, 
                    Farm__c,
                    fsServ__Is_Active__c,
                    fsServ__Contract_Status__c
                    from fsServ__Lending_Contract__c Limit 1];
       System.assertEquals('Active', contract.fsServ__Contract_Status__c, 'Contract Status is not Active');
       System.assertEquals(true, contract.fsServ__Is_Active__c, 'Contract is activaion failed');
        
    }
}