/*------------------------------------------------------------
Author:         Damon Shaw
Company:        Trineo
Description:    Identify Individuals who should be synced with Staff devices using SF's Lightning Sync
Test Class:     ScheduleBatchLightningSyncSummaryTest
History
10/01/18        Damon Shaw      Created
------------------------------------------------------------*/
global class ScheduleBatchLightningSyncSummary implements Schedulable, Database.Batchable<sObject> {

    private static final String SCHED_JOB_NAME = ScheduleService.getCRONJobName('ScheduleBatchLightningSyncSummary');

    //Execute for Schedulable Context
    global void execute(SchedulableContext schedulableContext) {
        Database.executeBatch(new ScheduleBatchLightningSyncSummary(), 1000);
    }

    global static void schedule() {
        String expression = ScheduleService.getCRONExpression('ScheduleBatchLightningSyncSummary');
        if (String.isNotBlank(expression)) {
            final ScheduleBatchLightningSyncSummary job = new ScheduleBatchLightningSyncSummary();
            System.schedule(SCHED_JOB_NAME, expression, job);
        }
    }

    //Abort this calss if scheduled
    global static void abort() {
        ScheduleService.terminate(SCHED_JOB_NAME);
    }

    global Database.querylocator start(Database.BatchableContext batchableContext) {
        //look at all active Individuals and their Derived Relationships
        String query = 'SELECT  Id, Name, Lightning_Sync_GUIDs__c FROM Contact WHERE Individual_Dead__c = false AND (RecordType.Name = \'Individual AU\')' ;
        return Database.getQueryLocator(query);
    }

    //Execute for Batchable Context
    global void execute(Database.BatchableContext BC, List<Contact> scope) {
        List<Contact> contactsToUpdate = new List<Contact>();

        //create a map of all contact Ids and a set of the farm ids they are linked to,
        //use a set to eliminate duplicates where the contact is both the Primary_Business_Contact__c and the Primary_On_Farm_Contact__c
        Map<Id, Set<Id>> entitieIdsByPrimaryContactId = new Map<Id, Set<Id>>();
        for(Contact con :scope){
            entitieIdsByPrimaryContactId.put(con.Id, new Set<Id>());
        }

        //find all farms linked to the contacts in the batch
        Map<Id,Account> farms = new Map<Id, Account>([SELECT Id,
                                                            Name,
                                                            RecordType.Name,
                                                            Primary_Business_Contact__c,
                                                            Primary_On_Farm_Contact__c,
                                                            Rep_Area_Manager__r.Guid__c,
                                                            Regional_Manager__r.Guid__c,
                                                            Rep_Inbound_Service_Specialist__r.Guid__c,
                                                            Quality_Specialist__r.Guid__c
                                                    FROM Account
                                                    WHERE (
                                                        Primary_Business_Contact__c IN :entitieIdsByPrimaryContactId.keySet() OR
                                                        Primary_On_Farm_Contact__c IN :entitieIdsByPrimaryContactId.keySet()
                                                    )
                                                ]);

        //add the farm ids into the sets for each contact
        for(Account a :farms.values()){
            if(a.Primary_Business_Contact__c != null && entitieIdsByPrimaryContactId.containsKey(a.Primary_Business_Contact__c)){
                entitieIdsByPrimaryContactId.get(a.Primary_Business_Contact__c).add(a.Id);
            }
            if(a.Primary_On_Farm_Contact__c != null && entitieIdsByPrimaryContactId.containsKey(a.Primary_On_Farm_Contact__c)){
                entitieIdsByPrimaryContactId.get(a.Primary_On_Farm_Contact__c).add(a.Id);
            }
        }

        //loop over each contact
        for(Contact con :scope){

            Set<String> syncGuids = new Set<String>();
            String syncGuidString = '';

            //loop over any farms the contact is linked to
            for(Id a :entitieIdsByPrimaryContactId.get(con.Id)){
                Account f = farms.get(a);
                //AU farm
                if (f.RecordType.Name == GlobalUtility.auAccountFarmRecordTypeName){
                    //add the staff guids to the set to be writen to the Individual
                    if(f.Rep_Area_Manager__r.Guid__c != null){
                        syncGuids.add(f.Rep_Area_Manager__r.Guid__c);
                    }
                    if(f.Regional_Manager__r.Guid__c != null){
                        syncGuids.add(f.Regional_Manager__r.Guid__c);
                    }
                    if(f.Rep_Inbound_Service_Specialist__r.Guid__c != null){
                        syncGuids.add(f.Rep_Inbound_Service_Specialist__r.Guid__c);
                    }
                    if(f.Quality_Specialist__r.Guid__c != null){
                        syncGuids.add(f.Quality_Specialist__r.Guid__c);
                    }
                }
            }

            for(String guid :syncGuids){
                syncGuidString += guid+',';
            }

            //if there are no user guids to join then set to null, else remove the last comma
            syncGuidString = syncGuidString == '' ? null : syncGuidString.removeEnd(',');

            //if there is a difference between the current summary value and the new entities string,
            //or the Users who should get this contact in their sync has changed, update the contact
            if(con.Lightning_Sync_GUIDs__c != syncGuidString){
                con.Lightning_Sync_GUIDs__c = syncGuidString;
                contactsToUpdate.add(con);
            }
        }

        //update any Individuals who's entity summary has changed
        if(contactsToUpdate.size() > 0){
            Database.SaveResult[] srList = Database.update(contactsToUpdate, false);

            // log any update failures
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        Logger.log('Lightning Sync Batch error', err.getStatusCode()+' - '+err.getMessage()+' - '+sr.getId(), Logger.LogType.ERROR);
                    }
                    Logger.saveLog();
                }
            }
        }
    }

    global void finish(Database.BatchableContext BC) {

    }
}