/**
 * AMSKnowledgeServiceTest
 *
 * Controller for testing the AMSKnowledgeService, note that testing SOSL is a PITA since you have
 * to seed the search results by hand, and the SNIPPET is empty.
 *
 * Author: Ed (Trineo)
 * Date: July 2017
 */
@isTest
private class AMSKnowledgeServiceTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup
    static void testSetup() {
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
    }

    @isTest
    static void newsArticles() {

        System.assertEquals(0, AMSKnowledgeService.totalNewsArticles());

        TestClassDataUtil.createAMSNewsArticle(35);

        System.runAs(getCommunityUser()) {
            System.assertEquals(35, AMSKnowledgeService.totalNewsArticles());
            // Have 35 articles, get them 10 at a time, should be 10,10,10,5 returned
            List<Knowledge__kav> articles = AMSKnowledgeService.newsArticlesPaginated(0, 10);

            System.assertEquals(10, articles.size());

            articles = AMSKnowledgeService.newsArticlesPaginated(10, 10);

            System.assertEquals(10, articles.size());

            articles = AMSKnowledgeService.newsArticlesPaginated(20, 10);

            System.assertEquals(10, articles.size());

            articles = AMSKnowledgeService.newsArticlesPaginated(30, 10);

            System.assertEquals(5, articles.size());
        }
    }

    @isTest
    static void adviceArticles() {
        System.assertEquals(0, AMSKnowledgeService.adviceArticles().size());

        TestClassDataUtil.createAMSAdviceArticle(35);

        System.runAs(getCommunityUser()) {
            System.assertEquals(35, AMSKnowledgeService.adviceArticles().size());
        }
    }

    @isTest
    static void searchArticles() {
        List<Knowledge__kav> newsArticles = TestClassDataUtil.createAMSNewsArticle(10);
        List<Knowledge__kav> adviceArticles = TestClassDataUtil.createAMSAdviceArticle(10);
        List<Id> newsIds = new List<Id>(new Map<Id, Knowledge__kav>(newsArticles).keySet());
        List<Id> adviceIds = new List<Id>(new Map<Id, Knowledge__kav>(adviceArticles).keySet());
        List<Id> allIds = new List<Id>();
        allIds.addAll(newsIds);
        allIds.addAll(adviceIds);


        System.runAs(getCommunityUser()) {
            Test.setFixedSearchResults(allIds);

            // Positive case - all News and Advice articles
            List<AMSKnowledgeService.SearchResult> results = AMSKnowledgeService.findMatchingArticles('Summary', AMSKnowledgeService.searchableArticleRecordTypes, 100, 0, true);

            System.assertEquals(20, results.size());

            // Case insensitive
            results = AMSKnowledgeService.findMatchingArticles('summary', AMSKnowledgeService.searchableArticleRecordTypes, 100, 0, true);

            System.assertEquals(20, results.size());

            // Confirm matches returned.
            for (AMSKnowledgeService.SearchResult result : results) {
                System.assert(result.matchingContent.containsIgnoreCase('Summary'), 'Found: ' + result.matchingContent);
            }

            // No results in the search - yes this is crap that you can't actually do a SOSL search in a UT!
            Test.setFixedSearchResults(new List<Id>());

            // Test the negative cases
            results = AMSKnowledgeService.findMatchingArticles('NOP', AMSKnowledgeService.searchableArticleRecordTypes, 100, 0, true);  // no matches

            System.assertEquals(0, results.size());

            results = AMSKnowledgeService.findMatchingArticles(null, AMSKnowledgeService.searchableArticleRecordTypes, 100, 0, true); // no string

            System.assertEquals(0, results.size());

            results = AMSKnowledgeService.findMatchingArticles('', AMSKnowledgeService.searchableArticleRecordTypes, 100, 0, true); // empty string

            System.assertEquals(0, results.size());

            results = AMSKnowledgeService.findMatchingArticles('   ', AMSKnowledgeService.searchableArticleRecordTypes, 100, 0, true);  // whitespace

            System.assertEquals(0, results.size());

            results = AMSKnowledgeService.findMatchingArticles('s', AMSKnowledgeService.searchableArticleRecordTypes, 100, 0, true);  // too short

            System.assertEquals(0, results.size());
        }

    }

    @isTest
    static void getFAQs() {
        TestClassDataUtil.createAMSFAQArticles(1);

        System.runAs(getCommunityUser()) {
            Knowledge__kav faqArticle = [
                SELECT Id,
                    Title,
                    UrlName,
                    Summary
                FROM Knowledge__kav
                WHERE PublishStatus = 'Online'
                AND Language = 'en_US'
            ];

            Test.startTest();

            Knowledge__kav article = AMSKnowledgeService.faqArticleByUrl(faqArticle.UrlName);

            System.assertNotEquals(null, article);

            article = AMSKnowledgeService.faqArticleByUrl('bad-url');

            System.assertEquals(null, article);

            // Search for articles
            Test.setFixedSearchResults(new List<Id>{faqArticle.Id});
            List<AMSKnowledgeService.SearchResult> results = AMSKnowledgeService.findMatchingArticles('Test', new List<String>{'AMS_FAQ'},
                                                              10, 0, false);
            System.assertEquals(1, results.size());

            Test.stopTest();
        }
    }

    @isTest
    static void getWebText() {
        TestClassDataUtil.createAMSWebTextArticles(1);

        System.runAs(getCommunityUser()) {
            Knowledge__kav wtArticle = [
                SELECT Id,
                    Title,
                    Summary,
                    Content__c,
                    UrlName
                FROM Knowledge__kav
                WHERE PublishStatus = 'Online'
                AND Language = 'en_US'
            ];

            Test.startTest();
            List<Knowledge__kav> articles = AMSKnowledgeService.webTextArticles(wtArticle.Title);

            System.assertEquals(1, articles.size());

            // // Let's do the AMSWebTextController as well whilst we are here
            // AMSWebTextController controller = new AMSWebTextController();
            // System.assertEquals('undefined', controller.webtext);
            // controller.webTextTitle = wtArticle.Title;
            // System.assertEquals(wtArticle.Content__c, controller.webtext);

            Test.stopTest();
        }
    }
    private static User getCommunityUser() {
        User theUser = [
            SELECT Id
            FROM User
            WHERE Email = :USER_EMAIL
        ];

        return theUser;
    }
}