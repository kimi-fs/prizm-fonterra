@IsTest
private class AgreementWebserviceAUTest {

    private static Id PARTY_RECORD_TYPE = [Select Id From RecordType Where DeveloperName = 'Party_AU' And SobjectType = 'Account'].Id;
    private static Id FARM_RECORD_TYPE = [Select Id From RecordType Where DeveloperName = 'Farm_AU' And SobjectType = 'Account'].Id;

    @IsTest
    private static void testSendStandardAgreement() {
        Account farm = new Account(Name = 'Test Farm', Source_ID__c = '10037', RecordTypeId = FARM_RECORD_TYPE);
        Account ownerParty = new Account(Name = 'Owner', Source_ID__c = '41408', RecordTypeId = PARTY_RECORD_TYPE);
        Account smParty = new Account(Name = 'SM', Source_ID__c = '31017', RecordTypeId = PARTY_RECORD_TYPE);
        Account cmParty = new Account(Name = 'CM', Source_ID__c = '15032', RecordTypeId = PARTY_RECORD_TYPE);

        insert(new List<sObject> { farm, ownerParty, smParty, cmParty });

        try {
            AgreementWebserviceAU.sendAgreement(mockStandardAgreement());
        } catch (Exception e) {
            System.debug(e);
        }

        List<Agreement__c> agreements = [Select Id From Agreement__c];
        List<Entity_Relationship__c> entityRelationships = [Select Id From Entity_Relationship__c];

        System.assertEquals(1, agreements.size());
        System.assertEquals(3, entityRelationships.size());
    }

    @IsTest
    private static void testSendAndDeleteStandardAgreement() {
        Account farm = new Account(Name = 'Test Farm', Source_ID__c = '10037', RecordTypeId = FARM_RECORD_TYPE);
        Account ownerParty = new Account(Name = 'Owner', Source_ID__c = '41408', RecordTypeId = PARTY_RECORD_TYPE);
        Account smParty = new Account(Name = 'SM', Source_ID__c = '31017', RecordTypeId = PARTY_RECORD_TYPE);
        Account cmParty = new Account(Name = 'CM', Source_ID__c = '15032', RecordTypeId = PARTY_RECORD_TYPE);

        insert(new List<sObject> { farm, ownerParty, smParty, cmParty });

        try {
            AgreementWebserviceAU.sendAgreement(mockStandardAgreement());
        } catch (Exception e) {
            System.debug(e);
        }

        List<Agreement__c> agreements = [Select Id From Agreement__c];
        List<Entity_Relationship__c> entityRelationships = [Select Id From Entity_Relationship__c];

        System.assertEquals(1, agreements.size());
        System.assertEquals(3, entityRelationships.size());

        AgreementWebserviceAU.sendAgreement(mockAgreementDelete('8010'));

        agreements = [Select Id From Agreement__c];
        entityRelationships = [Select Id From Entity_Relationship__c];

        System.assertEquals(0, agreements.size());
        System.assertEquals(0, entityRelationships.size());
    }

    @IsTest
    private static void testSendWinterContract() {
        Account farm = new Account(Name = 'Test Farm', Source_ID__c = '10037', RecordTypeId = FARM_RECORD_TYPE);
        Account ownerParty = new Account(Name = 'Owner', Source_ID__c = '108051', RecordTypeId = PARTY_RECORD_TYPE);
        //Account smParty = new Account(Name = 'SM', Source_ID__c = '31017', RecordTypeId = PARTY_RECORD_TYPE);
        //Account cmParty = new Account(Name = 'CM', Source_ID__c = '15032', RecordTypeId = PARTY_RECORD_TYPE);

        insert(new List<sObject> { farm, ownerParty });

        try {
            AgreementWebserviceAU.sendAgreement(mockWinterAgreement());
        } catch (Exception e) {
            System.debug(e);
        }

        List<Agreement__c> agreements = [Select Id From Agreement__c];

        System.assertEquals(1, agreements.size());
    }

    @IsTest
    private static void testSendContractSupplyAgreement() {
        Account farm = new Account(Name = 'Test Farm', Source_ID__c = '10037', RecordTypeId = FARM_RECORD_TYPE);
        Account ownerParty = new Account(Name = 'Owner', Source_ID__c = '41408', RecordTypeId = PARTY_RECORD_TYPE);
        Account smParty = new Account(Name = 'SM', Source_ID__c = '31017', RecordTypeId = PARTY_RECORD_TYPE);
        Account cmParty = new Account(Name = 'CM', Source_ID__c = '15032', RecordTypeId = PARTY_RECORD_TYPE);

        insert(new List<sObject> { farm, ownerParty, smParty, cmParty });

        try {
            AgreementWebserviceAU.sendAgreement(mockContractSupplyAgreement());
        } catch (Exception e) {
            System.debug(e);
        }

        List<Agreement__c> agreements = [Select Id From Agreement__c];
        List<Entity_Relationship__c> entityRelationships = [Select Id From Entity_Relationship__c];

        System.assertEquals(1, agreements.size());
        System.assertEquals(0, entityRelationships.size());
    }

    private static AgreementWebserviceAU.Agreement mockStandardAgreement() {
        AgreementWebserviceAU.Agreement agreement = new AgreementWebserviceAU.Agreement();

        agreement.AgreementId = '8010';
        agreement.MessageHeader = createMessageHeader('Update', '7AD40023-EBD8-3BAE-866A-D49017D755F3');
        agreement.Farm = createFarm('10037');
        agreement.OwnerParty = createOwnerParty('41408');
        agreement.StartDate = Datetime.valueOf('2012-05-01 00:00:00.000Z');
        agreement.CertificationExpiryDate = Datetime.valueOf('2017-03-12 00:00:00.000Z');
        agreement.AgreementType = createAgreementType('Standard', 'Standard');
        agreement.Contract = createContract('8200113', Datetime.valueOf('2018-08-31 00:00:00.000Z'), '10767',
            null,
            null,
            null);
        agreement.SplitArrangements = new List<AgreementWebserviceAU.SplitArrangement>();
        agreement.SplitArrangements.add(
            createSplitArrangement('4597.514614', Datetime.valueOf('2010-05-01 00:00:00.000Z'),
                new List<AgreementWebserviceAU.PaymentSplit> {
                    createPaymentSplit('4138.450546', 'Percentage', '41408', 'Owner', true, true, 1, 50.00, null, 50.00, null),
                    createPaymentSplit('4138.450547', 'Percentage', '31017', 'Share Milker', true, true, 2, 50.00, 100.00, 50.00, null),
                    createPaymentSplit('4139.450547', 'Percentage', '15032', 'Contract Milker', true, true, 3, 50.00, 100.00, 50.00, null)
                }
            )
        );

        return agreement;
    }

    private static AgreementWebserviceAU.Agreement mockAgreementDelete(String agreementId) {
        AgreementWebserviceAU.Agreement agreement = new AgreementWebserviceAU.Agreement();

        agreement.AgreementId = agreementId;
        agreement.MessageHeader = createMessageHeader('Delete', null);

        return agreement;
    }

    private static AgreementWebserviceAU.Agreement mockWinterAgreement() {
        AgreementWebserviceAU.Agreement agreement = new AgreementWebserviceAU.Agreement();

        agreement.AgreementId = '8000';
        agreement.MessageHeader = createMessageHeader('Update', '7AD40023-EBD8-3BAE-866A-D49017D755F3');
        agreement.Farm = createFarm('10037');
        agreement.OwnerParty = createOwnerParty('108051');
        agreement.StartDate = Datetime.valueOf('2012-05-01 00:00:00.000Z');
        agreement.CertificationExpiryDate = Datetime.valueOf('2017-03-12 00:00:00.000Z');
        agreement.AgreementType = createAgreementType('Longburn Winter 2010-2012', 'Contract Winter Milk');
        agreement.Contract = createContract('8045.3762', Datetime.valueOf('2018-08-31 00:00:00.000Z'), '108051',
            new List<AgreementWebserviceAU.WinterContract> {
                createWinterContract(200, Datetime.valueOf('2012-05-01 00:00:00.000Z'), null, Datetime.valueOf('2012-07-31 00:00:00.000Z'), '8073.6888')
            },
            new List<AgreementWebserviceAU.WinterTransaction> {
                createWinterTransaction('200 kg/ms from 42542, party id 13239 to 43374, party id 108051 from 1/5/2012',
                200, Datetime.valueOf('2012-03-23 11:21:52.504Z'), 'Winter 2012', 'Valid', 'Transfer In', '8181.2844')
            },
            null
        );

        agreement.SplitArrangements = new List<AgreementWebserviceAU.SplitArrangement>();
        agreement.SplitArrangements.add(
            createSplitArrangement('4597.559746', Datetime.valueOf('2010-05-01 00:00:00.000Z'),
                new List<AgreementWebserviceAU.PaymentSplit> {
                    createPaymentSplit('4038.521644', 'Percentage', '108051', 'Owner', true, true, 1, 100.00, null, 100.00, null)
                }
            )
        );

        return agreement;
    }

    private static AgreementWebserviceAU.Agreement mockContractSupplyAgreement() {
        AgreementWebserviceAU.Agreement agreement = new AgreementWebserviceAU.Agreement();

        agreement.AgreementId = '54771';
        agreement.MessageHeader = createMessageHeader('Update', '84E9337B-D0F0-377B-9AFE-60A6C6CEF2B9');
        agreement.Farm = createFarm('10037');
        agreement.OwnerParty = createOwnerParty('41408');
        agreement.StartDate = Datetime.now().addYears(-1);
        agreement.EndDate = Datetime.now().addYears(5); //use dynamic date so we don't run into problems in the future

        agreement.AgreementType = createAgreementType('6 Year Contract (2015)', 'Contract Supply');
        agreement.Contract = createContract('8916.2588', Datetime.valueOf('2018-08-31 00:00:00.000Z'), '108051',
            null,
            null,
            new List<AgreementWebserviceAU.SeasonContract> {
                createSeasonContract(null, 12000, 'test', 500, '2015/2016', '8917.9071', 12000, 1000),
                createSeasonContract(null, 12000, 'test 2', null, '2016/2017', '8917.9072', 12000, null),
                createSeasonContract(null, 12000, 'test 3', null, '2017/2018', '8917.9072', 12000, null),
                createSeasonContract(null, 12000, 'test 4', null, '2018/2019', '8917.9072', 12000, null)
            }
        );

        agreement.SplitArrangements = new List<AgreementWebserviceAU.SplitArrangement>();
        agreement.SplitArrangements.add(
            createSplitArrangement('4597.580760', Datetime.valueOf('2015-06-01 00:00:00.000Z'),
                new List<AgreementWebserviceAU.PaymentSplit> {
                    createPaymentSplit('4038.521644', 'Cents/Kg MS', '41408', 'Owner', true, true, 1, 100.00, null, 100.00, null),
                    createPaymentSplit('4038.521644', 'Cents/Kg MS', '31017', 'Contract Milker', true, true, 1, 100.00, null, 100.00, null),
                    createPaymentSplit('4038.521644', 'Cents/Kg MS', '15032', 'Share Milker', true, true, 1, 100.00, null, 100.00, null)
                }
            )
        );

        return agreement;
    }

    private static AgreementWebserviceAU.Farm createFarm(String supplyNumber) {

        AgreementWebserviceAU.Farm farm = new AgreementWebserviceAU.Farm();
        farm.SupplyNumber = supplyNumber;

        return farm;
    }

    private static AgreementWebserviceAU.OwnerParty createOwnerParty(String partyId) {

        AgreementWebserviceAU.OwnerParty ownerParty = new AgreementWebserviceAU.OwnerParty();
        ownerParty.PartyId = partyId;

        return ownerParty;
    }

    private static AgreementWebserviceAU.Party createParty(String partyId) {

        AgreementWebserviceAU.Party party = new AgreementWebserviceAU.Party();
        party.PartyId = partyId;

        return party;
    }

    private static AgreementWebserviceAU.MessageHeader createMessageHeader(String transactionType, String interfaceBatchId) {

        AgreementWebserviceAU.MessageHeader messageHeader = new AgreementWebserviceAU.MessageHeader();
        messageHeader.TransactionType = transactionType;
        messageHeader.InterfaceBatchId = interfaceBatchId;

        return messageHeader;
    }

    private static AgreementWebserviceAU.AgreementType createAgreementType(String agreementType, String agreementTypeType) {

        AgreementWebserviceAU.AgreementType agreementTypeObject = new AgreementWebserviceAU.AgreementType();
        agreementTypeObject.AgreementType = agreementType;
        agreementTypeObject.AgreementTypeType = agreementTypeType;

        return agreementTypeObject;
    }

    private static AgreementWebserviceAU.Contract createContract(String contractId, DateTime endDate, String partyId,
        List<AgreementWebserviceAU.WinterContract> winterContracts, List<AgreementWebserviceAU.WinterTransaction> winterTransactions,
        List<AgreementWebserviceAU.SeasonContract> seasonContracts) {

        AgreementWebserviceAU.Contract contract = new AgreementWebserviceAU.Contract();
        contract.ContractId = contractId;
        contract.EndDate = endDate;
        contract.Party = createParty(partyId);
        contract.WinterContracts = winterContracts;
        contract.WinterTransactions = winterTransactions;
        contract.SeasonContracts = seasonContracts;

        return contract;
    }

    private static AgreementWebserviceAU.SeasonContract createSeasonContract(Integer allocated, Integer contractQuantity, String note, Integer reductionForGMP, String season,
        String seasonContractId, Integer seasonQuantity, Integer sharesForContract) {

        AgreementWebserviceAU.SeasonContract seasonContract = new AgreementWebserviceAU.SeasonContract();
        seasonContract.Allocated = allocated;
        seasonContract.ContractQuantity = contractQuantity;
        seasonContract.Note = note;
        seasonContract.ReductionForGMP = reductionForGMP;
        seasonContract.Season = season;
        seasonContract.SeasonContractId = seasonContractId;
        seasonContract.SeasonQuantity = seasonQuantity;
        seasonContract.SharesForContract = sharesForContract;

        return seasonContract;
    }

    private static AgreementWebserviceAU.WinterContract createWinterContract(Integer dailyQuantity, DateTime endDate, String note, DateTime startDate, String winterContractId) {
        AgreementWebserviceAU.WinterContract winterContract = new AgreementWebserviceAU.WinterContract();

        winterContract.DailyQuantity = dailyQuantity;
        winterContract.EndDate = endDate;
        winterContract.Note = note;
        winterContract.StartDate = startDate;
        winterContract.WinterContractId = winterContractId;

        return winterContract;
    }

    private static AgreementWebserviceAU.WinterTransaction createWinterTransaction(String comment, Integer quantity, DateTime recordCreatedOn, String season,
            String status, String transactionType, String winterTransactionId) {

        AgreementWebserviceAU.WinterTransaction winterTransaction = new AgreementWebserviceAU.WinterTransaction();

        winterTransaction.Comment = comment;
        winterTransaction.Quantity = quantity;
        winterTransaction.RecordCreatedOn = recordCreatedOn;
        winterTransaction.Season = season;
        winterTransaction.Status = status;
        winterTransaction.TransactionType = transactionType;
        winterTransaction.WinterTransactionId = winterTransactionId;

        return winterTransaction;
    }

    private static AgreementWebserviceAU.SplitArrangement createSplitArrangement(String splitArrangementId, DateTime startDate, List<AgreementWebserviceAU.PaymentSplit> paymentSplits) {
        AgreementWebserviceAU.SplitArrangement splitArrangement = new AgreementWebserviceAU.SplitArrangement();

        splitArrangement.SplitArrangementId = splitArrangementId;
        splitArrangement.StartDate = startDate;
        splitArrangement.PaymentSplits = paymentSplits;

        return splitArrangement;
    }

    private static AgreementWebserviceAU.PaymentSplit createPaymentSplit(String paymentSplitId, String paymentSplitType, String partyId, String payeeType,
            Boolean isFinalPay, Boolean canViewOwnersProdHistory, Integer priority, Decimal capAdjustedPercentage, Decimal demeritPercentage,
            Decimal percentage, String dpaType) {
        AgreementWebserviceAU.PaymentSplit paymentSplit = new AgreementWebserviceAU.PaymentSplit();

        paymentSplit.PaymentSplitId = paymentSplitId;
        paymentSplit.PaymentSplitType = paymentSplitType;
        paymentSplit.Party = createParty(partyId);
        paymentSplit.PayeeType = payeeType;
        paymentSplit.IsFinalPay = isFinalPay;
        paymentSplit.CanViewOwnersProdHistory = canViewOwnersProdHistory;
        paymentSplit.Priority = priority;
        paymentSplit.CapAdjustedPercentage = capAdjustedPercentage;
        paymentSplit.DemeritPercentage = demeritPercentage;
        paymentSplit.Percentage = percentage;
        paymentSplit.DPAType = dpaType;

        return paymentSplit;
    }
}