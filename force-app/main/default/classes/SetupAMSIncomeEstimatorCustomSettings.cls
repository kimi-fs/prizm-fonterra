/**
 * Description: Utility class to initalise Income Estimator custom settings
 * This class is called from SetupOrg class when setting up a new sandbox
 * @author: Amelia (Trineo)
 * @date: October 2017
 */
public without sharing class SetupAMSIncomeEstimatorCustomSettings {

    public static void deleteExistingIncomeEstimatorCustomSettingsAndCreateNew(){
        delete [SELECT Id FROM AMS_Quality_Penalties__c];
        delete [SELECT Id FROM AMS_Quality_Awards_and_Incentives__c];
        delete [SELECT Id FROM AMS_Production_Payment_Bands__c];
        delete [SELECT Id FROM AMS_Base_Milk_Price__c];
        delete [SELECT Id FROM AMS_Income_Estimator_Admin_Data__c];
        delete [SELECT Id FROM AMS_Specialty_Milk_Price_Data__c];
        delete [SELECT Id FROM AMS_Fresh_Milk_Supplement_Condition__c];
        SetupAMSIncomeEstimatorCustomSettings.createIncomeEstimatorCustomSettings();
    }
    public static void createIncomeEstimatorCustomSettings(){
        createAMSQualityPenalitiesCustomSettings();
        createAMSQualityAwardsAndIncentiveCustomSettings();
        createAMSProductionPaymentBandCustomSettings();
        createAMSBaseMilkPriceCustomSettings();
        createAMSIncomeEstimatorAdminData();
        createAMSSpecialtyMilkPriceData();
        createAMSFreshMilkSupplementCondition();
    }

    /* QUALITY PENALITIES */
    public class AMSQualityPenalitiesWrapper{
        public Integer lowerLimit;
        public Integer upperLimit;
        public Decimal percentageAdjustment;
        public String  specialtyMilkPrice;

        public AMSQualityPenalitiesWrapper(Integer lowerLimit, Integer upperLimit, Decimal percentageAdjustment, String specialtyMilkPrice){
            this.lowerLimit = lowerLimit;
            this.upperLimit = upperLimit;
            this.percentageAdjustment = percentageAdjustment;
            this.specialtyMilkPrice = specialtyMilkPrice;
        }
    }

    public static List<AMSQualityPenalitiesWrapper> qualityPenalities;
    static {
        qualityPenalities = new List<AMSQualityPenalitiesWrapper>{
            new AMSQualityPenalitiesWrapper(null, 10, 0, null),
            new AMSQualityPenalitiesWrapper(11, 20, 0, null),
            new AMSQualityPenalitiesWrapper(21, 30, -1, null),
            new AMSQualityPenalitiesWrapper(31, 40, -2, null),
            new AMSQualityPenalitiesWrapper(41, 50, -3, null),
            new AMSQualityPenalitiesWrapper(51, 60, -4, null),
            new AMSQualityPenalitiesWrapper(61, 70, -5, null),
            new AMSQualityPenalitiesWrapper(71, 80, -10, null),
            new AMSQualityPenalitiesWrapper(81, 100, -15, null),
            new AMSQualityPenalitiesWrapper(101, 120, -20, null),
            new AMSQualityPenalitiesWrapper(121, 140, -25, null),
            new AMSQualityPenalitiesWrapper(141, 160, -30, null),
            new AMSQualityPenalitiesWrapper(161, 180, -35, null),
            new AMSQualityPenalitiesWrapper(181, 1000, -40, null),
            new AMSQualityPenalitiesWrapper(1001, null, -50, null)
        };

        for (String specialtyMilkPrice : AMSIncomeEstimatorDataService.APPLICABLE_SPECIALTY_MILK) {
            List<AMSQualityPenalitiesWrapper> specialtyMilkQualityPenalties = new List<AMSQualityPenalitiesWrapper>{
                new AMSQualityPenalitiesWrapper(null, 5, 0, specialtyMilkPrice),
                new AMSQualityPenalitiesWrapper(6, 10, 0, specialtyMilkPrice),
                new AMSQualityPenalitiesWrapper(11, 20, -1, specialtyMilkPrice),
                new AMSQualityPenalitiesWrapper(21, 30, -2, specialtyMilkPrice),
                new AMSQualityPenalitiesWrapper(31, 40, -3, specialtyMilkPrice),
                new AMSQualityPenalitiesWrapper(41, 50, -4, specialtyMilkPrice),
                new AMSQualityPenalitiesWrapper(51, 60, -5, specialtyMilkPrice),
                new AMSQualityPenalitiesWrapper(61, 70, -10, specialtyMilkPrice),
                new AMSQualityPenalitiesWrapper(71, 80, -15, specialtyMilkPrice),
                new AMSQualityPenalitiesWrapper(81, 100, -29, specialtyMilkPrice),
                new AMSQualityPenalitiesWrapper(101, 999, -25, specialtyMilkPrice),
                new AMSQualityPenalitiesWrapper(1000, null, -50, specialtyMilkPrice)
            };
            qualityPenalities.addAll(specialtyMilkQualityPenalties);
        }


    }

    public static void createAMSQualityPenalitiesCustomSettings(){
        Integer nameCount = getAMSQualityPenalityNumberToIncrement();

        List<AMS_Quality_Penalties__c> qualityPenalitiesCustomSettings = new List<AMS_Quality_Penalties__c>();
        for(AMSQualityPenalitiesWrapper qp : qualityPenalities){
            qualityPenalitiesCustomSettings.add(new AMS_Quality_Penalties__c(
                Name = String.valueOf(nameCount++),
                Demerit_Points_Lower_Limit__c = qp.lowerLimit,
                Demerit_Points_Upper_Limit__c = qp.upperLimit,
                Percentage_Adjustment_to_Milk_Payment__c = qp.percentageAdjustment,
                Specialty_Milk_Price__c = qp.specialtyMilkPrice
            ));
        }
        insert qualityPenalitiesCustomSettings;
    }

    private static Integer getAMSQualityPenalityNumberToIncrement(){
        Integer numberToIncrement = 0;
        List<AMS_Quality_Penalties__c> existingCustomSettings = AMS_Quality_Penalties__c.getAll().values();

        if(!existingCustomSettings.isEmpty()){
            List<Integer> existingRecordNumbers = new List<Integer>();
            for(AMS_Quality_Penalties__c qp : existingCustomSettings){
                if(qp.Name.isNumeric()){
                    existingRecordNumbers.add(Integer.valueOf(qp.Name));
                }
            }
            existingRecordNumbers.sort();
            numberToIncrement = existingRecordNumbers[existingRecordNumbers.size()-1];
        }
        return numberToIncrement;
    }

    /* QUALITY AWARDS AND INCENTIVES */
    public class AMSQualityAwardsAndIncentivesWrapper{
        public Decimal cKgFat;
        public Decimal cKgProtein;
        public String awardType;

        public AMSQualityAwardsAndIncentivesWrapper(Decimal cKgFat, Decimal cKgProtein, String awardType){
            this.cKgFat = cKgFat;
            this.cKgProtein = cKgProtein;
            this.awardType = awardType;
        }
    }

    public static List<AMSQualityAwardsAndIncentivesWrapper> qualityAwardsAndIncentives;
    static {
        qualityAwardsAndIncentives = new List<AMSQualityAwardsAndIncentivesWrapper>{
            new AMSQualityAwardsAndIncentivesWrapper(0.048, 0.091, 'Annual'),
            new AMSQualityAwardsAndIncentivesWrapper(0.036, 0.068, 'Monthly')
        };
    }

    public static void createAMSQualityAwardsAndIncentiveCustomSettings(){
        Integer nameCount = getAMSQualityAwardsNumberToIncrement();

        List<AMS_Quality_Awards_and_Incentives__c> qualityAwardsAndIncentivesCustomSettings = new List<AMS_Quality_Awards_and_Incentives__c>();
        for(AMSQualityAwardsAndIncentivesWrapper qa : qualityAwardsAndIncentives){
            qualityAwardsAndIncentivesCustomSettings.add(new AMS_Quality_Awards_and_Incentives__c(
                Name = String.valueOf(nameCount++),
                c_kg_Fat__c = qa.cKgFat,
                c_kg_Protein__c = qa.cKgProtein,
                Type__c = qa.awardType
            ));
        }
        insert qualityAwardsAndIncentivesCustomSettings;
    }

    private static Integer getAMSQualityAwardsNumberToIncrement(){
        Integer numberToIncrement = 0;
        List<AMS_Quality_Awards_and_Incentives__c> existingCustomSettings = AMS_Quality_Awards_and_Incentives__c.getAll().values();

        if(!existingCustomSettings.isEmpty()){
            List<Integer> existingRecordNumbers = new List<Integer>();
            for(AMS_Quality_Awards_and_Incentives__c qp : existingCustomSettings){
                if(qp.Name.isNumeric()){
                    existingRecordNumbers.add(Integer.valueOf(qp.Name));
                }
            }
            existingRecordNumbers.sort();
            numberToIncrement = existingRecordNumbers[existingRecordNumbers.size()-1];
        }
        return numberToIncrement;
    }

    public static void createAMSFreshMilkSupplementCondition() {
        List<AMS_Fresh_Milk_Supplement_Condition__c> freshMilkSupplementSetings = new List<AMS_Fresh_Milk_Supplement_Condition__c>();
        freshMilkSupplementSetings.add(new AMS_Fresh_Milk_Supplement_Condition__c(
            Name = '0.00%',
            Minimum_Difference_Percentage__c = 0,
            Maximum_Difference_Percentage__c = null,
            FMS_Eligible_Percentage__c = 1
        ));
        freshMilkSupplementSetings.add(new AMS_Fresh_Milk_Supplement_Condition__c(
            Name = '0.01 % - 5 %',
            Minimum_Difference_Percentage__c = 0.0001,
            Maximum_Difference_Percentage__c = 0.0500,
            FMS_Eligible_Percentage__c = 0.8000
        ));
        freshMilkSupplementSetings.add(new AMS_Fresh_Milk_Supplement_Condition__c(
            Name = '5.01 % - 10 %',
            Minimum_Difference_Percentage__c = 0.0501,
            Maximum_Difference_Percentage__c = 0.1000,
            FMS_Eligible_Percentage__c = 0.6000
        ));
        freshMilkSupplementSetings.add(new AMS_Fresh_Milk_Supplement_Condition__c(
            Name = '10.01 % - 20 %',
            Minimum_Difference_Percentage__c = 0.1001,
            Maximum_Difference_Percentage__c = 0.2000,
            FMS_Eligible_Percentage__c = 0.4000
        ));
        freshMilkSupplementSetings.add(new AMS_Fresh_Milk_Supplement_Condition__c(
            Name = '> 20%',
            Minimum_Difference_Percentage__c = 0.2001,
            Maximum_Difference_Percentage__c = null,
            FMS_Eligible_Percentage__c = 0.0000
        ));
        insert freshMilkSupplementSetings;
    }

    /* PRODUCTION PAYMENT BANDS */
    public class AMSProductionPaymentsBandWrapper{
        public Integer lowerLimit;
        public Integer upperLimit;
        public Decimal cKgFat;
        public Decimal cKgProtein;

        public AMSProductionPaymentsBandWrapper(Integer lowerLimit, Integer upperLimit, Decimal cKgFat, Decimal cKgProtein){
            this.lowerLimit = lowerLimit;
            this.upperLimit = upperLimit;
            this.cKgFat = cKgFat;
            this.cKgProtein = cKgProtein;
        }
    }

    public static List<AMSProductionPaymentsBandWrapper> productionPaymentBands;
    static {
        productionPaymentBands = new List<AMSProductionPaymentsBandWrapper>{
            new AMSProductionPaymentsBandWrapper(null, 19999, 0.0, 0.0),
            new AMSProductionPaymentsBandWrapper(20000, 32499, 1.0, 1.9),
            new AMSProductionPaymentsBandWrapper(32500, 44999, 2.0, 3.8),
            new AMSProductionPaymentsBandWrapper(45000, 69999, 4.0, 7.6),
            new AMSProductionPaymentsBandWrapper(70000, 94999, 6.0, 11.4),
            new AMSProductionPaymentsBandWrapper(95000, 119999, 7.0, 13.3),
            new AMSProductionPaymentsBandWrapper(120000, 149999, 8.0, 15.2),
            new AMSProductionPaymentsBandWrapper(150000, 179999, 10.0, 19.0),
            new AMSProductionPaymentsBandWrapper(180000, 209999, 12.0, 22.8),
            new AMSProductionPaymentsBandWrapper(210000, 239999, 14.0, 26.6),
            new AMSProductionPaymentsBandWrapper(240000, 299999, 17.0, 32.3),
            new AMSProductionPaymentsBandWrapper(300000, 359999, 19.0, 36.1),
            new AMSProductionPaymentsBandWrapper(360000, 479999, 20.0, 38.0),
            new AMSProductionPaymentsBandWrapper(480000, 719999, 21.0, 39.9),
            new AMSProductionPaymentsBandWrapper(720000, 959999, 23.0, 43.7),
            new AMSProductionPaymentsBandWrapper(960000, null, 24.0, 45.6)
        };
    }

    public static void createAMSProductionPaymentBandCustomSettings(){
        Integer nameCount = getAMSProductionPaymentBandNumberToIncrement();

        List<AMS_Production_Payment_Bands__c> productionPaymentBandCustomSettings = new List<AMS_Production_Payment_Bands__c>();
        for(AMSProductionPaymentsBandWrapper pp : productionPaymentBands){
            productionPaymentBandCustomSettings.add(new AMS_Production_Payment_Bands__c(
                Name = String.valueOf(nameCount++),
                Total_Kg_Lower_Limit__c = pp.lowerLimit,
                Total_Kg_Upper_Limit__c = pp.upperLimit,
                c_kg_Fat__c = pp.cKgFat,
                c_kg_Protein__c = pp.cKgProtein
            ));
        }
        insert productionPaymentBandCustomSettings;
    }

    private static Integer getAMSProductionPaymentBandNumberToIncrement(){
        Integer numberToIncrement = 0;
        List<AMS_Production_Payment_Bands__c> existingCustomSettings = AMS_Production_Payment_Bands__c.getAll().values();

        if(!existingCustomSettings.isEmpty()){
            List<Integer> existingRecordNumbers = new List<Integer>();
            for(AMS_Production_Payment_Bands__c qp : existingCustomSettings){
                if(qp.Name.isNumeric()){
                    existingRecordNumbers.add(Integer.valueOf(qp.Name));
                }
            }
            existingRecordNumbers.sort();
            numberToIncrement = existingRecordNumbers[existingRecordNumbers.size()-1];
        }
        return numberToIncrement;
    }

    /* BASE MILK PRICE */
    public class AMSBaseMilkPrice{
        public String region;
        public String kgMSType;
        public Decimal jul;
        public Decimal aug;
        public Decimal sep;
        public Decimal oct;
        public Decimal nov;
        public Decimal dec;
        public Decimal jan;
        public Decimal feb;
        public Decimal mar;
        public Decimal apr;
        public Decimal may;
        public Decimal jun;
        public String priceBand;

        public AMSBaseMilkPrice(String region, String kgMSType, Decimal jul, Decimal aug, Decimal sep, Decimal oct, Decimal nov, Decimal dec, Decimal jan, Decimal feb, Decimal mar, Decimal apr, Decimal may, Decimal jun, String priceBand){
            this.region = region;
            this.kgMSType = kgMSType;
            this.jul = jul;
            this.aug = aug;
            this.sep = sep;
            this.oct = oct;
            this.nov = nov;
            this.dec = dec;
            this.jan = jan;
            this.feb = feb;
            this.mar = mar;
            this.apr = apr;
            this.may = may;
            this.jun = jun;
            this.priceBand = priceBand;
        }
    }

    public static List<AMSBaseMilkPrice> baseMilkPrice;
    static {
        Decimal FAT_SPRING_PRICE = 0.0;
        Decimal PROTEIN_SPRING_PRICE = 0.0;
        baseMilkPrice = new List<AMSBaseMilkPrice>{
            new AMSBaseMilkPrice('North', 'Fat', 0.71, 0.46, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, 0.46, 0.71, 0.85, 0.93, 1.06, '7/5'),
            new AMSBaseMilkPrice('North', 'Protein', 1.35, 0.87, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, 0.87, 1.35, 1.62, 1.77, 2.01, '7/5'),
            new AMSBaseMilkPrice('South', 'Fat', 0.71, 0.46, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, 0.46, 0.71, 0.85, 0.93, 1.06, '7/5'),
            new AMSBaseMilkPrice('South', 'Protein', 1.35, 0.87, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, 0.87, 1.35, 1.62, 1.77, 2.01, '7/5'),
            new AMSBaseMilkPrice('East', 'Fat', 0.71, 0.0, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, 0.46, 0.46, 0.71, 0.85, 0.93, 1.06, '7/5'),
            new AMSBaseMilkPrice('East', 'Protein', 1.35, 0.0, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, 0.87, 0.87, 1.35, 1.62, 1.77, 2.01, '7/5'),
            new AMSBaseMilkPrice('West', 'Fat', 0.71, 0.0, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, 0.46, 0.46, 0.71, 0.85, 0.93, 1.06, '7/5'),
            new AMSBaseMilkPrice('West', 'Protein', 1.35, 0.0, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, 0.87, 0.87, 1.35, 1.62, 1.77, 2.01, '7/5'),
            new AMSBaseMilkPrice('North', 'Fat', 0.71, 0.46, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, 0.46, 0.71, 0.85, 0.93, 1.06, '8/4'),
            new AMSBaseMilkPrice('North', 'Protein', 1.35, 0.87, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, 0.87, 1.35, 1.62, 1.77, 2.01, '8/4'),
            new AMSBaseMilkPrice('South', 'Fat', 0.71, 0.46, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, 0.46, 0.71, 0.85, 0.93, 1.06, '8/4'),
            new AMSBaseMilkPrice('South', 'Protein', 1.35, 0.87, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, 0.87, 1.35, 1.62, 1.77, 2.01, '8/4'),
            new AMSBaseMilkPrice('East', 'Fat', 0.71, 0.0, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, 0.46, 0.46, 0.71, 0.85, 0.93, 1.06, '8/4'),
            new AMSBaseMilkPrice('East', 'Protein', 1.35, 0.0, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, 0.87, 0.87, 1.35, 1.62, 1.77, 2.01, '8/4'),
            new AMSBaseMilkPrice('West', 'Fat', 0.71, 0.0, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, FAT_SPRING_PRICE, 0.46, 0.46, 0.71, 0.85, 0.93, 1.06, '8/4'),
            new AMSBaseMilkPrice('West', 'Protein', 1.35, 0.0, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, PROTEIN_SPRING_PRICE, 0.87, 0.87, 1.35, 1.62, 1.77, 2.01, '8/4')
        };
    }

    public static void createAMSBaseMilkPriceCustomSettings(){
        Integer nameCount = getAMSBaseMilkPriceNumberToIncrement();

        List<AMS_Base_Milk_Price__c> baseMilkPriceCustomSettings = new List<AMS_Base_Milk_Price__c>();
        for(AMSBaseMilkPrice bmp : baseMilkPrice){
            baseMilkPriceCustomSettings.add(new AMS_Base_Milk_Price__c(
                Name = String.valueOf(nameCount++),
                Region__c = bmp.region,
                Price_Type__c = bmp.kgMSType,
                Jul__c = bmp.jul,
                Aug__c = bmp.aug,
                Sep__c = bmp.sep,
                Oct__c = bmp.oct,
                Nov__c = bmp.nov,
                Dec__c = bmp.dec,
                Jan__c = bmp.jan,
                Feb__c = bmp.feb,
                Mar__c = bmp.mar,
                Apr__c = bmp.apr,
                May__c = bmp.may,
                Jun__c = bmp.jun,
                Price_Band__c = bmp.priceBand
            ));
        }
        insert baseMilkPriceCustomSettings;
    }

    private static Integer getAMSBaseMilkPriceNumberToIncrement(){
        Integer numberToIncrement = 0;
        List<AMS_Base_Milk_Price__c> existingCustomSettings = AMS_Base_Milk_Price__c.getAll().values();

        if(!existingCustomSettings.isEmpty()){
            List<Integer> existingRecordNumbers = new List<Integer>();
            for(AMS_Base_Milk_Price__c qp : existingCustomSettings){
                if(qp.Name.isNumeric()){
                    existingRecordNumbers.add(Integer.valueOf(qp.Name));
                }
            }
            existingRecordNumbers.sort();
            numberToIncrement = existingRecordNumbers[existingRecordNumbers.size()-1];
        }
        return numberToIncrement;
    }

    public static List<AMS_Income_Estimator_Admin_Data__c> adminData = new List<AMS_Income_Estimator_Admin_Data__c>{
        new AMS_Income_Estimator_Admin_Data__c(
            Company_Milk_Price__c = 6.67,
            Fat_Price_Baseline__c = 4.47,
            Increment_on_fat_per_10c_move__c = 0.06,
            Increment_on_protein_per_10c_move__c = 0.1138,
            Protein_Price_Baseline__c = 8.493,
            Second_Stop_Charge__c = 40.0,
            Monthly_Quality_Incentive_Limit__c = 0,
			Yearly_Quality_Incentive_Limit__c = 10
        )
    };

    public static void createAMSIncomeEstimatorAdminData(){
        Integer nameCount = getAMSAdminDataNumberToIncrement();

        for(AMS_Income_Estimator_Admin_Data__c ad : adminData){
            ad.Name = String.valueOf(nameCount++);
        }
        insert adminData;
    }

    private static Integer getAMSAdminDataNumberToIncrement(){
        Integer numberToIncrement = 0;
        List<AMS_Income_Estimator_Admin_Data__c> existingCustomSettings = AMS_Income_Estimator_Admin_Data__c.getAll().values();

        if(!existingCustomSettings.isEmpty()){
            List<Integer> existingRecordNumbers = new List<Integer>();
            for(AMS_Income_Estimator_Admin_Data__c qp : existingCustomSettings){
                if(qp.Name.isNumeric()){
                    existingRecordNumbers.add(Integer.valueOf(qp.Name));
                }
            }
            existingRecordNumbers.sort();
            numberToIncrement = existingRecordNumbers[existingRecordNumbers.size()-1];
        }
        return numberToIncrement;
    }

    public static List<AMS_Specialty_Milk_Price_Data__c> specialMilkPriceData = new List<AMS_Specialty_Milk_Price_Data__c>();
    static {
        for (String specialtyMilkPrice : AMSIncomeEstimatorDataService.APPLICABLE_SPECIALTY_MILK) {
            specialMilkPriceData.add(new AMS_Specialty_Milk_Price_Data__c(
                Specialty_Milk_Price__c = specialtyMilkPrice,
                Fat_Price_Baseline__c = 4.07,
                Contracted_Protein_Price_Baseline__c = 10.175,
                Non_Contracted_Protein_Price_Baseline__c = 7.733,
                Fresh_Milk_Supplement_Fat_Rate__c = 0.23,
                Fresh_Milk_Supplement_Protein_Rate__c = 0.32
            ));
        }
    }

    public static void createAMSSpecialtyMilkPriceData() {
        Integer nameCount = getAMSSpecialtyMilkDataNumberToIncrement();


        for(AMS_Specialty_Milk_Price_Data__c ad : specialMilkPriceData){
            ad.Name = String.valueOf(nameCount++);
        }
        insert specialMilkPriceData;
    }

    private static Integer getAMSSpecialtyMilkDataNumberToIncrement() {
        Integer numberToIncrement = 0;
        List<AMS_Specialty_Milk_Price_Data__c> existingCustomSettings = AMS_Specialty_Milk_Price_Data__c.getAll().values();

        if(!existingCustomSettings.isEmpty()){
            List<Integer> existingRecordNumbers = new List<Integer>();
            for(AMS_Specialty_Milk_Price_Data__c qp : existingCustomSettings){
                if(qp.Name.isNumeric()){
                    existingRecordNumbers.add(Integer.valueOf(qp.Name));
                }
            }
            existingRecordNumbers.sort();
            numberToIncrement = existingRecordNumbers[existingRecordNumbers.size()-1];
        }
        return numberToIncrement;
    }

}