/*
* Controller for AMS Contracts and Pricing page
* @author: John Au (Trineo)
* @date: 20/04/2020
* @test: AMSPricingControllerTest
*/
public with sharing class AMSPricingController {
    public String message { get; set; }
    public Boolean success { get; set; }

    public String name { get; set; }
    public String region { get; set; }
    public String supplierType { get; set; }
    public String farmNumber { get; set; }
    public String phoneNumber { get; set; }
    public String comments { get; set; }

    public FileWrapper attachment { get; set; }

    public List<SelectOption> farmSelectOptions { get; set; }

    public List<AgreementWrapper> agreementList { get; set; }
    public String selectedFarmToSign { get; set; }
    public String selectedPartyRelatedToFarm { get; set; }
    public Boolean showMessageForElectronicAgreement { get; set; }
    public Boolean is2021Season { 
        get {
            return true;//Date.today() >= Date.newInstance(2021, 7, 1);
        } 
        set; 
    }
    public Boolean isAgreementShown { 
        get {
            Boolean isShown = true;
            // Go get all the agreements for the farm
            for (AgreementWrapper agreement : this.agreementList) {
                if(agreement.farmAgreementType == 'Non-Standard' && agreement.agreementRelease == false) {
                    isShown = false;
                    break;
                }
            }
            return isShown;
        } 
        set;
    }
    public Boolean hasFormBeenSubmitted {
        get {
            Boolean isSigned = false;
            Cookie param = Apexpages.currentPage().getCookies().get('agreementSignedCookie');
            if(param != null) {
                isSigned = true;
                // Delete the cookie so it wont come up on next page reload
                // Note: Using a cookie as the user is redirected from a different page and this cookie is set there
                Cookie cookie = new Cookie('agreementSignedCookie', 'true', null, 0, false);
                Apexpages.currentPage().setCookies(new Cookie[]{cookie});
            }
            return isSigned;
        }
        set;
    }

    private Contact currentContact { get; set; }

    private static Map<String, String> messages;
    static {
        messages = AMSCommunityUtil.initialisePageMessages('AMSNewUser');
    }

    public class FileWrapper {
        public String fileName { get; set; }
        public Blob content { get; set; }
    }

    public class AgreementWrapper {
        public String farmName { get; set; }
        public String farmId { get; set; }
        public String partyId { get; set; }
        public String agreementId { get; set; }
        public String agreementStatus { get; set; }
        public Boolean isEligibleForElectronicAgreement { get; set; }
        public String areaManagerContactMessage { get; set; }
        public String farmAgreementType { get; set; }
        public Boolean agreementRelease{ get; set; }
    }

    public AMSPricingController() {
        farmSelectOptions = new List<SelectOption>();
        attachment = new FileWrapper();
        currentContact = AMSContactService.getContact();
        showMessageForElectronicAgreement = false;

        if (currentContact != null) {
            name = currentContact.Name;
            phoneNumber = currentContact.Phone;
            List<Individual_Relationship__c> derivedRelationshipList = RelationshipAccessService.getDerivedRelationshipsForOwnedFarmsAU(currentContact.Id);
            createFarmSelectOptions(derivedRelationshipList);
            getCurrentContactAgreements(derivedRelationshipList);
        }
    }

    public void createCase() {
        try {
            CaseService.createContractsAppointmentRequestBookingCaseAU(
                region,
                supplierType,
                farmNumber,
                name,
                phoneNumber,
                comments,
                attachment.fileName,
                attachment.content
            );

            success = true;
            clearFormValues();
        } catch (Exception e) {
            message = e.getMessage();
        }
    }

    private void clearFormValues() {
        region = null;
        supplierType = null;
        farmNumber = null;
        comments = null;
        attachment = new FileWrapper();
    }

    private void createFarmSelectOptions(List<Individual_Relationship__c> irels) {
        for (Individual_Relationship__c irel : irels) {
            farmSelectOptions.add(new SelectOption(irel.Farm__c, irel.Farm__r.Name));
        }
    }

    public PageReference retryValidation() {
        message = messages.get('captcha-error');
        return null;
    }

    private void getCurrentContactAgreements(List<Individual_Relationship__c> irels) {
        List<Agreement__c> agreements = AgreementService.getCurrentContactAgreements(irels);
        //Temporary static start date. This will be changed to dynamic next sprint. Ticket number:  AMS-2955. Reach out to Irish for more details.
        Date firstOfJulY21 = Date.newInstance(2021, 07, 01);
        agreementList = new List<AgreementWrapper>();
        for (Individual_Relationship__c irel : irels) {
            AgreementWrapper agreementWrapper = new AgreementWrapper();
            agreementWrapper.farmName = irel.Farm__r.Name;
            agreementWrapper.farmId = irel.Farm__c;
            agreementWrapper.partyId = irel.Party__c;
            agreementWrapper.farmAgreementType = irel.Farm__r.Agreement_Type__c;

            if (irel.Farm__r.Agreement_Type__c == 'Standard') {
                agreementWrapper.isEligibleForElectronicAgreement = true;

                for (Agreement__c agreement : agreements) {
                    if (irel.Farm__c == agreement.Farm__c && agreement.Start_Date__c == firstOfJulY21) {
                        agreementWrapper.isEligibleForElectronicAgreement = (agreement.Agreement_Status__c == 'Pending') ? true : false;
                        agreementWrapper.agreementId = agreement.Id;
                        agreementWrapper.agreementStatus = agreement.Agreement_Status__c;
                        agreementWrapper.agreementRelease = agreement.Release_Agreement_to_Farmer__c;
                    }
                }
                if (agreementWrapper.isEligibleForElectronicAgreement) {
                    showMessageForElectronicAgreement = true;
                }
            } else if (irel.Farm__r.Agreement_Type__c == 'Non-Standard' && is2021Season) {
                Boolean isOnlineAgreement = false;
                for (Agreement__c agreement : agreements) {
                    if (irel.Farm__c == agreement.Farm__c && agreement.Start_Date__c == firstOfJulY21 && agreement.Release_Agreement_to_Farmer__c != false) {
                        isOnlineAgreement = true;
                        agreementWrapper.isEligibleForElectronicAgreement = (agreement.Agreement_Status__c == 'Pending') ? true : false;
                        agreementWrapper.agreementId = agreement.Id;
                        agreementWrapper.agreementStatus = agreement.Agreement_Status__c;
                        agreementWrapper.agreementRelease = agreement.Release_Agreement_to_Farmer__c;
                    } else if (agreement.Agreement_Status__c != 'Executed' && agreement.Start_Date__c == firstOfJulY21) {
                        agreementWrapper.areaManagerContactMessage = 'Please call your Area Manager, ' + irel.Farm__r.Rep_Area_Manager__r.Name +  ' at ' + irel.Farm__r.Rep_Area_Manager__r.MobilePhone + '.';
                    } else {
                        agreementWrapper.areaManagerContactMessage = 'Please call your Area Manager, ' + irel.Farm__r.Rep_Area_Manager__r.Name +  ' at ' + irel.Farm__r.Rep_Area_Manager__r.MobilePhone + '.';
                    }
                    if (isOnlineAgreement) {
                        agreementWrapper.areaManagerContactMessage = '';
                    }
                }
                if (isOnlineAgreement && agreementWrapper?.isEligibleForElectronicAgreement == true) {
                    showMessageForElectronicAgreement = true;
                }
            } else {
                agreementWrapper.areaManagerContactMessage = 'Please call your Area Manager, ' + irel.Farm__r.Rep_Area_Manager__r.Name +  ' at ' + irel.Farm__r.Rep_Area_Manager__r.MobilePhone + '.';
            }

            agreementList.add(agreementWrapper);

        }

    }

    public PageReference goToAMSSignAgreementPage(){
        PageReference signAgreementPage = Page.AMSSignAgreement;
        signAgreementPage.getParameters().put('FarmId', selectedFarmToSign);
        signAgreementPage.getParameters().put('PartyId', selectedPartyRelatedToFarm);
        signAgreementPage.setRedirect(true);
        return signAgreementPage;

    }
}