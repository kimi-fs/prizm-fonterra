/*------------------------------------------------------------
Author:         Rory Figgins
Company:        Trineo
Description:    FSCRM-2875

Test Class:     ContactTrigger_Test.cls
History
25/05/2017      Rory Figgins        Created
31/08/2017      Damon Shaw          Refactored to compare child Channel_Preference__c records to decide which ones are deleted - FSCRM-5530
-------------------------------------------------------------*/

public class ContactMergeCleanupService {

    //The store of the Contact's child records before the merge, this will be required in our afterDelete trigger to find the records that have came from the losing record
    //public static Map<Id, ContactChildRecordsWrapper> mapContactChildRecordsByContactId {get; set;}

    //Wrapper to store nesseciary data about a Contact's child records
    public class ContactChildRecordsWrapper {
        public Map<Id, Individual_Relationship__c> mapIndividualRelationshipById {get; set;}
        public Map<Id, Channel_Preference__c> mapChannelPreferenceById {get; set;}

        //Initialise the maps we are interested in
        public ContactChildRecordsWrapper() {
            this.mapIndividualRelationshipById = new Map<Id, Individual_Relationship__c>();
            this.mapChannelPreferenceById = new Map<Id, Channel_Preference__c>();
        }
    }

    public static Map<Id, ContactChildRecordsWrapper> getContactChildRecordsByContactId(Set<Id> contactIdSet) {
        //Query the Individual Relationships which this contact is related to
        Map<Id, Individual_Relationship__c> mapIndividualRelationshipById = new Map<Id, Individual_Relationship__c>([SELECT Id, Individual__c FROM Individual_Relationship__c WHERE Individual__c IN: contactIdSet]);
        Map<Id, List<Individual_Relationship__c>> mapIndividualRelationshipsByContactId = new Map<Id, List<Individual_Relationship__c>>();

        //Group these Individual Relationships by ContactId
        for (Id individualRelationshipId : mapIndividualRelationshipById.keySet()) {
            Id contactId = mapIndividualRelationshipById.get(individualRelationshipId).Individual__c;

            if (!mapIndividualRelationshipsByContactId.containsKey(contactId)) {
                mapIndividualRelationshipsByContactId.put(contactId, new List<Individual_Relationship__c>());
            }

            mapIndividualRelationshipsByContactId.get(contactId).add(mapIndividualRelationshipById.get(individualRelationshipId));
        }

        //Query the Channel Preference records which this contact is related to
        Map<Id, Channel_Preference__c> mapChannelPreferenceById = new Map<Id, Channel_Preference__c>([SELECT Id, Contact_ID__c, Contact_ID__r.MS_Individual_ID__c, Channel_Preference_Setting__c, Channel_Preference_Setting__r.Type__c, CreatedDate,DeliveryByEmail__c, DeliveryMail__c, NotifyBySMS__c FROM Channel_Preference__c WHERE Contact_ID__c IN: contactIdSet]);
        Map<Id, List<Channel_Preference__c>> mapChannelPreferencesByContactId = new Map<Id, List<Channel_Preference__c>>();

        //Group these Individual Relationships by ContactId
        for (Id channelPreferenceId : mapChannelPreferenceById.keySet()) {
            Id contactId = mapChannelPreferenceById.get(channelPreferenceId).Contact_ID__c;

            if (!mapChannelPreferencesByContactId.containsKey(contactId)) {
                mapChannelPreferencesByContactId.put(contactId, new List<Channel_Preference__c>());
            }

            mapChannelPreferencesByContactId.get(contactId).add(mapChannelPreferenceById.get(channelPreferenceId));
        }

        //Combine these records into a data structure which can be accessible later via looking up the contactId
        Map<Id, ContactChildRecordsWrapper> tempMapContactChildRecordsByContactId = new Map<Id, ContactChildRecordsWrapper>();

        for (Id contactId : contactIdSet) {
            //Reset the maps to be reused
            mapIndividualRelationshipById = new Map<Id, Individual_Relationship__c>();
            mapChannelPreferenceById = new Map<Id, Channel_Preference__c>();


            //Re map the related records to be accessible in Map<Id, SObject> format
            if (mapIndividualRelationshipsByContactId.containsKey(contactId)) {
                for (Individual_Relationship__c ir : mapIndividualRelationshipsByContactId.get(contactId)) {
                    mapIndividualRelationshipById.put(ir.Id, ir);
                }
            }

            if (mapChannelPreferencesByContactId.containsKey(contactId)) {
                for (Channel_Preference__c cp : mapChannelPreferencesByContactId.get(contactId)) {
                    mapChannelPreferenceById.put(cp.Id, cp);
                }
            }

            ContactChildRecordsWrapper ccrw = new ContactChildRecordsWrapper();

            ccrw.mapIndividualRelationshipById = mapIndividualRelationshipById;
            ccrw.mapChannelPreferenceById = mapChannelPreferenceById;

            tempMapContactChildRecordsByContactId.put(contactId, ccrw);
        }

        return tempMapContactChildRecordsByContactId;
    }

    @future
    public static void mergeContactsChildRecords(String mergedContactsRecords) {

        Map<Id, ContactChildRecordsWrapper> winners = (Map<Id, ContactChildRecordsWrapper>)JSON.deserialize(mergedContactsRecords, Map<Id, ContactChildRecordsWrapper>.class);
for(Id i : winners.keySet()){
    for(Channel_Preference__c c :winners.get(i).mapChannelPreferenceById.values()){
        System.debug('*** cp = '+c);
    }
}

        //in a merge, all of the deleted contact's child records get reparented to the remaining contact, this potentially results in up to 3 of each type provided each contact has 1 record each.
        //pass the records in pairs of each type to thereCanBeOnlyOne() which will decide which record should be kept and which should be deleted

        List<Channel_Preference__c> cpsToKill = new List<Channel_Preference__c>();

        for(Id c :winners.keySet()){

            Map<Id, List<Channel_Preference__c>> channelPreferencesByContact = new Map<Id, List<Channel_Preference__c>>();

            ContactChildRecordsWrapper winnersRecords = winners.get(c);
            for(Id cp :winnersRecords.mapChannelPreferenceById.keySet()){
                //put the channel preferences into lists to be compaired against each other using their Channel_Preference_Setting__c
                if(!channelPreferencesByContact.containsKey(winnersRecords.mapChannelPreferenceById.get(cp).Channel_Preference_Setting__c)){
                    channelPreferencesByContact.put(winnersRecords.mapChannelPreferenceById.get(cp).Channel_Preference_Setting__c, new List<Channel_Preference__c>());
                }
                channelPreferencesByContact.get(winnersRecords.mapChannelPreferenceById.get(cp).Channel_Preference_Setting__c).add(winnersRecords.mapChannelPreferenceById.get(cp));
            }
System.debug('*** channelPreferencesByContact = '+channelPreferencesByContact);
            for(Id cps :channelPreferencesByContact.keySet()){
                if(channelPreferencesByContact.get(cps).size() > 1){
                    //although there should only ever be a maximum of 3 records of each type to compare, who knows what madness we'll get.
                    //automatically set the first record as the winner, then loop over the remaining records and compaire them to the winner.
                    //for each result, set the new winner and send the loser to be deleted, then repeat.
                    Channel_Preference__c winner = channelPreferencesByContact.get(cps)[0];
                    for(Integer i = 1; i < channelPreferencesByContact.get(cps).size(); i++){
                        HighlanderResult result = thereCanBeOnlyOne(winner, channelPreferencesByContact.get(cps)[i]);
                        winner = result.cpWinner;
                        cpsToKill.add(result.cpLoser);
                    }
                }
            }
        }
System.debug('*** cpsToKill = '+cpsToKill);
    for(Channel_Preference__c c :cpsToKill){
        System.debug('*** cptoKill = '+c);
    }
        if(cpsToKill.size() > 0){
            delete cpsToKill;
        }
    }

    public class HighlanderResult{

        public Channel_Preference__c cpWinner { get; set; }
        public Channel_Preference__c cpLoser { get; set; }

        public HighlanderResult(Channel_Preference__c w, Channel_Preference__c l){
            this.cpWinner = w;
            this.cpLoser = l;
        }
    }

    public static HighlanderResult thereCanBeOnlyOne(Channel_Preference__c cp1, Channel_Preference__c cp2){
        /* compaire the provided records and pass back the winner and loser

                                                                        ]
                                                                        ]          //
          _-----------------------------------------------------------{o}_________/|
        <       -==============================================:::::::{*}//////////]
          `-----------------------------------------------------------{o}~~~~~~~~~\|
                                                                        ]          \\
                                                                        ]
        */

        //if the cp1 contact has a Farm Source One Id and the cp2 contact doesn't, cp1 wins
        if(cp1.Contact_ID__r.MS_Individual_ID__c != null && cp2.Contact_ID__r.MS_Individual_ID__c == null){
            System.debug('*** the winner is a member of clan Farm Source One');
            return new HighlanderResult(cp1, cp2);
        }

        //if the cp2 contact has a Farm Source One Id and the cp1 contact doesn't, cp2 wins
        else if(cp1.Contact_ID__r.MS_Individual_ID__c != null && cp2.Contact_ID__r.MS_Individual_ID__c == null){
            System.debug('*** the winner is a member of clan Farm Source One');
            return new HighlanderResult(cp2, cp1);
        }

        //if neither cp contacts have a Farm Source One Id or both cp contacts have a Farm Source One Id
        //check if either has any channels set to true
        else{
            Boolean cp1SettingsAllFalse = true;
            Boolean cp2SettingsAllFalse = true;

            if(cp1.DeliveryByEmail__c ||  cp1.DeliveryMail__c ||  cp1.NotifyBySMS__c){
                cp1SettingsAllFalse = false;
            }
            if(cp2.DeliveryByEmail__c || cp2.DeliveryMail__c || cp2.NotifyBySMS__c){
                cp2SettingsAllFalse = false;
            }

            //if cp1 has some settings selected and cp2 doesn't, cp1 wins
            if(cp1SettingsAllFalse == false && cp2SettingsAllFalse == true){
                System.debug('*** the winner has more weapons than the loser');
                return new HighlanderResult(cp1, cp2);
            }

            //if cp2 has some settings selected and cp1 doesn't, cp2 wins
            else if(cp1SettingsAllFalse == true && cp2SettingsAllFalse == false){
                System.debug('*** the winner has more weapons than the loser');
                return new HighlanderResult(cp2, cp1);
            }

            //if both have settings or neither have settings
            //return the newest to be deleted
            else{
                if(cp1.CreatedDate <= cp2.CreatedDate){
                    System.debug('*** the winner is the more experienced record');
                    return new HighlanderResult(cp1, cp2);
                }
                else{
                    System.debug('*** the winner is the more experienced record');
                    return new HighlanderResult(cp2, cp1);
                }
            }
        }
    }
}