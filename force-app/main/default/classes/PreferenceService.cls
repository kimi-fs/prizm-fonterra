public class PreferenceService {

    private static final List<String> CHANNEL_PREFERENCE_SETTING_QUERY_FIELDS = new List<String> {
        'CME_Message_Type_Id__c',
        'DeliveryByEmail__c',
        'DeliveryMail__c',
        'Id',
        'Name',
        'NotifyBySMS__c',
        'RecordTypeId',
        'Type__c'
    };

    private static final List<String> CHANNEL_PREFERENCE_QUERY_FIELDS = new List<String> {
        'Contact_ID__c',
        'DeliveryByEmail__c',
        'DeliveryMail__c',
        'Id',
        'NotifyBySMS__c',
        'Opt_Out_Reason__c',
        'Opt_Out_Reason_Other__c',
        'Channel_Preference_Setting__c',
        'Channel_Preference_Setting__r.Active__c',
        'Channel_Preference_Setting__r.CME_Message_Type_Id__c',
        'Channel_Preference_Setting__r.Name',
        'Channel_Preference_Setting__r.Type__c',
        'Channel_Preference_Setting__r.DeliveryByEmail__c',
        'Channel_Preference_Setting__r.DeliveryMail__c',
        'Channel_Preference_Setting__r.NotifyBySMS__c'
    };

    private static String constructChannelPreferenceSettingQuery(String queryCondition) {
        String query = 'SELECT ';

        query += String.join(CHANNEL_PREFERENCE_SETTING_QUERY_FIELDS, ', ');
        query += ' FROM Channel_Preference_Setting__c';

        if (!String.isBlank(queryCondition)) {
            query += ' WHERE ';
            query += queryCondition;
        }

        return query;
    }

    private static String constructChannelPreferenceQuery(String queryCondition) {
        String query = 'SELECT ';

        query += String.join(CHANNEL_PREFERENCE_QUERY_FIELDS, ', ');
        query += ' FROM Channel_Preference__c';
        query += ' WHERE ';
        query += queryCondition;

        query += ' ORDER BY Channel_Preference_Setting__r.Name NULLS LAST';

        return query;
    }

    private static List<Channel_Preference_Setting__c> queryChannelPreferenceSettings(String queryCondition) {
        return (List<Channel_Preference_Setting__c>) Database.query(constructChannelPreferenceSettingQuery(queryCondition));
    }

    private static List<Channel_Preference__c> queryChannelPreferences(Set<Id> contactIdSet, String queryCondition) {
        String queryConditionPlusContactIdCondition = 'Contact_ID__c IN (\'' + String.join(new List<Id>(contactIdSet), '\',\'') + '\')';

        if (!String.isBlank(queryCondition)) {
            queryConditionPlusContactIdCondition += ' AND ' + queryCondition;
        }

        return (List<Channel_Preference__c>) Database.query(constructChannelPreferenceQuery(queryConditionPlusContactIdCondition));
    }

    // retrieve all Channel Preference Setting records
    public static Map<Id, Channel_Preference_Setting__c> getChannelPreferenceSettingMap() {
        return new Map<Id, Channel_Preference_Setting__c>(queryChannelPreferenceSettings(null));
    }

    // retrieve all active Channel Preference Setting records
    public static Map<Id, Channel_Preference_Setting__c> getChannelPreferenceSettingMap(Boolean active) {
        return new Map<Id, Channel_Preference_Setting__c>(queryChannelPreferenceSettings('Active__c = ' + active));
    }

    // retrieve all Channel Preference Setting records with specified type
    public static Map<Id, Channel_Preference_Setting__c> getChannelPreferenceSettingMap(String type) {
        return new Map<Id, Channel_Preference_Setting__c>(queryChannelPreferenceSettings('Type__c = \'' + type + '\''));
    }

    // retrieve all Channel Preference Setting records with specified type and active
    public static Map<Id, Channel_Preference_Setting__c> getChannelPreferenceSettingMap(Boolean active, String type) {
        return new Map<Id, Channel_Preference_Setting__c>(queryChannelPreferenceSettings('Active__c = ' + active + ' AND ' + 'Type__c = \'' + type + '\''));
    }

    // retrieve existing Channel Preference records for a specific contact
    public static Map<Id, Channel_Preference__c> getChannelPreferenceMap(Id contactId) {
        Set<Id> contactIdSet = new Set<Id> {
            contactId
        };

        return getChannelPreferenceMap(contactIdSet);
    }

    // retrieve existing Channel Preference records for a set of contacts
    public static Map<Id, Channel_Preference__c> getChannelPreferenceMap(Set<Id> contactIdSet) {
        return new Map<Id, Channel_Preference__c>(queryChannelPreferences(contactIdSet, null));
    }

    // retrieve existing farm type Channel Preference records for a specific contact
    public static Map<Id, Channel_Preference__c> getChannelPreferenceMap(Id contactId, String type) {
        Set<Id> contactIdSet = new Set<Id> {
            contactId
        };

        return getChannelPreferenceMap(contactIdSet, type);
    }

    // retrieve existing farm type Channel Preference records for a set of contacts
    public static Map<Id, Channel_Preference__c> getChannelPreferenceMap(Set<Id> contactIdSet, String type) {
        return new Map<Id, Channel_Preference__c>(queryChannelPreferences(contactIdSet, 'Channel_Preference_Setting__r.Type__c = \'' + type + '\''));
    }

    // retrieve existing farm type Channel Preference records for a specific contact
    public static Map<Id, Channel_Preference__c> getChannelPreferenceMap(Id contactId, Boolean active) {
        Set<Id> contactIdSet = new Set<Id> {
            contactId
        };

        return getChannelPreferenceMap(contactIdSet, active);
    }
    // retrieve existing farm type Channel Preference records for a set of contacts
    public static Map<Id, Channel_Preference__c> getChannelPreferenceMap(Set<Id> contactIdSet, Boolean active) {
        return new Map<Id, Channel_Preference__c>(queryChannelPreferences(contactIdSet, 'Channel_Preference_Setting__r.Active__c = ' + active));
    }

    // retrieve existing farm type Channel Preference records for a specific contact
    public static Map<Id, Channel_Preference__c> getChannelPreferenceMap(Id contactId, Boolean active, String type) {
        Set<Id> contactIdSet = new Set<Id> {
            contactId
        };

        return getChannelPreferenceMap(contactIdSet, active, type);
    }

    // retrieve existing farm type Channel Preference records for a set of contacts
    public static Map<Id, Channel_Preference__c> getChannelPreferenceMap(Set<Id> contactIdSet, Boolean active, String type) {
        return new Map<Id, Channel_Preference__c>(queryChannelPreferences(contactIdSet, 'Channel_Preference_Setting__r.Active__c = ' + active + ' AND ' + 'Channel_Preference_Setting__r.Type__c = \'' + type + '\''));
    }

    public static List<Channel_Preference__c> createMissingChannelPreferences(List<Contact> contacts, Map<Id, Channel_Preference__c> mapChannelPreferenceById) {
        Map<Id, String> countryContactRecordTypeMap = new Map<Id, String> {
            GlobalUtility.individualAURecordTypeId => GlobalUtility.australiaChannelPreferenceSettingRecordTypeId
        };

        // Create a map of country to Channel Preference Settings
        Map<Id, List<Channel_Preference_Setting__c>> channelPreferenceSettingTypeMap = new Map<Id, List<Channel_Preference_Setting__c>>();
        for (Channel_Preference_Setting__c cps : PreferenceService.getChannelPreferenceSettingMap(true).values()) {
            if (channelPreferenceSettingTypeMap.containsKey(cps.RecordTypeId)) {
                channelPreferenceSettingTypeMap.get(cps.RecordTypeId).add(cps);
            } else {
                channelPreferenceSettingTypeMap.put(cps.RecordTypeId, new List<Channel_Preference_Setting__c> { cps });
            }
        }

        // Create a map of contact id to List Channel Preference Ids that have channel preferences for that contact
        Map<Id, List<Id>> contactIdChannelPreferenceMap = new Map<Id, List<Id>>();
        for (Channel_Preference__c cp : mapChannelPreferenceById.values()) {
            if (contactIdChannelPreferenceMap.containsKey(cp.Contact_ID__c)) {
                contactIdChannelPreferenceMap.get(cp.Contact_ID__c).add(cp.Channel_Preference_Setting__c);
            } else {
                contactIdChannelPreferenceMap.put(cp.Contact_ID__c, new List<Id> { cp.Channel_Preference_Setting__c });
            }
        }

        List<Channel_Preference__c> newChannelPreferences = new List<Channel_Preference__c>();
        for (Contact c : contacts) {
            String channelPreferenceSettingCountry = countryContactRecordTypeMap.get(c.RecordTypeId);

            if (channelPreferenceSettingTypeMap.containsKey(channelPreferenceSettingCountry)) {
                for (Channel_Preference_Setting__c cps : channelPreferenceSettingTypeMap.get(channelPreferenceSettingCountry)) {
                    // If the keyset doesnt contain the contact Id - the contact has not channel preferences - create it
                    // If the list of existing ones for that contacnt doesnt contain the channel preference setting id - create it

                    System.debug('contactIdChannelPreferenceMap: ' + contactIdChannelPreferenceMap);
                    if (contactIdChannelPreferenceMap.containsKey(c.Id)) {
                        System.debug('contactIdChannelPreferenceMap.get(c.Id): ' + contactIdChannelPreferenceMap.get(c.Id));
                        if (contactIdChannelPreferenceMap.get(c.Id).contains(cps.Id)) {
                            continue;
                        }
                    }
                    newChannelPreferences.add(createChannePreferenceFromSetting(cps, c.Id));
                }
            }
        }

        return newChannelPreferences;
    }

    private static Channel_Preference__c createChannePreferenceFromSetting(Channel_Preference_Setting__c channelPreferenceSetting, Id contactId) {
        Channel_Preference__c cp = new Channel_Preference__c();
        cp.Channel_Preference_Setting__c = channelPreferenceSetting.Id;
        cp.Contact_ID__c = contactId;

        // for the validation rules
        if (String.isNotBlank(channelPreferenceSetting.DeliveryByEmail__c) && channelPreferenceSetting.DeliveryByEmail__c.equalsIgnoreCase('Default Enabled')) {
            cp.DeliveryByEmail__c = true;
        }
        if (String.isNotBlank(channelPreferenceSetting.NotifyBySMS__c) && channelPreferenceSetting.NotifyBySMS__c.equalsIgnoreCase('Default Enabled')) {
            cp.NotifyBySMS__c = true;
        }
        if (String.isNotBlank(channelPreferenceSetting.DeliveryMail__c) && channelPreferenceSetting.DeliveryMail__c.equalsIgnoreCase('Mandatory')) {
            cp.DeliveryMail__c = true;
        }

        return cp;
    }

    public static Map<Id, Individual_Relationship__c> getActiveIndividualFarmRelationshipsForContact(Id contactId) {
        return new Map<Id, Individual_Relationship__c>(
            [
                SELECT Id, Name, Farm__c, Farm__r.Name
                FROM Individual_Relationship__c
                WHERE Individual__c =: contactId
                AND Farm__c != NULL
                AND Access_Valid__c = true
            ]
        );
    }

    public class IndividualPreferenceWrapper {
        public Channel_Preference__c channelPreference {
            get;
            set;
        }
        public Channel_Preference_Setting__c channelPreferenceSetting {
            get;
            set;
        }

        public IndividualPreferenceWrapper(Channel_Preference__c channelPreference, Channel_Preference_Setting__c channelPreferenceSetting) {
            this.channelPreference = channelPreference;
            this.channelPreferenceSetting = channelPreferenceSetting;
        }
    }

    public static Boolean getHasDerivedRelationship(Id contactId) {
        return [SELECT count() FROM Individual_Relationship__c WHERE Individual__c =: contactId AND RecordType.DeveloperName = 'Derived_Relationship' AND Farm__c != '' AND Active__c = true] > 0;
    }

}