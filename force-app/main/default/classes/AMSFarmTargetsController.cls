/**
 * Description: Controller for Farm Targets
 * @author: Damon (Trineo)
 * @date: Septemeber 2017
 */
public without sharing class AMSFarmTargetsController {

    public static Map<Integer, String> months {
        get{
            if (months == null) {
                months = new Map<Integer, String> {
                    1 => 'January',
                    2 => 'February',
                    3 => 'March',
                    4 => 'April',
                    5 => 'May',
                    6 => 'June',
                    7 => 'July',
                    8 => 'August',
                    9 => 'September',
                    10 => 'October',
                    11 => 'November',
                    12 => 'December'
                };
            }
            return months;
        }
        private set;
    }

    private List<Individual_Relationship__c> iRels { get; set; }
    public List<FarmSeason> farmSeasons { get; set; }
    public String selectedFarm { get; set; }

    public AMSFarmTargetsController() {

        this.iRels = new List<Individual_Relationship__c>();

        this.farmSeasons = new List<FarmSeason>();

        //if no contact Id is passed to the page, try to find one connected to the running user
        Id theContactId = AMSContactService.determineContactId();

        //if a contact has been found, get all of their Individial-Farm relationships
        if (theContactId != null) {

            // Put each farm into a set, and populate the iRels so
            // that we only have one iRel per farm (each Role has its own IR, so it is possible for the
            // same Individual to have multiple roles to the same Farm)
            List<Individual_Relationship__c> allRelationships = RelationshipAccessService.accessToIndividualRelationships(theContactId, RelationshipAccessService.Access.ACCESS_TARGETS);
            Set<Id> farmIds = new Set<Id>();
            for (Individual_Relationship__c ir : allRelationships) {
                if (!farmIds.contains(ir.Farm__c)) {
                    farmIds.add(ir.Farm__c);
                    this.iRels.add(ir);
                }
            }

            if (!this.iRels.isEmpty()) {
                //if there are individual relationships, find the related farms seasons
                this.farmSeasons = findSeasons(this.iRels[0].Farm__c);
            }
        }
    }

    public void saveSelectedFarm() {
        List<Target__c> targetsToUpsert = new List<Target__c>();
        for (FarmSeason fs : farmSeasons) {
            for (Target t : fs.targets) {
                targetsToUpsert.add(t.record);
            }
        }

        upsert targetsToUpsert;
    }

    public void switchFarm() {
        this.farmSeasons = findSeasons(selectedFarm);
    }

    public List<FarmSeason> findSeasons(Id farmId) {

        List<Farm_Season__c> farmSeasons = AMSFarmSeasonService.getFarmSeasons(new Set<Id> {farmId});
        List<FarmSeason> seasonsToReturn = new List<FarmSeason>();

        for (Integer i = 0; i < farmSeasons.size(); i++) {
            //find the current farm season
            if (farmSeasons[i].Reference_Period__r.Status__c == 'Current') {

                //create a FarmSeason wrapper with the Current Season and the previous
                //if there was no last season, then pass a dummy one
                Farm_Season__c lastSeason = i == 0 ? new Farm_Season__c(Farm__c = farmSeasons[0].Farm__c) : farmSeasons[i - 1];
                seasonsToReturn.add(new FarmSeason(farmSeasons[i], lastSeason));

                //check if there is a next season, if so create a second FarmSeason wrapper with next season and the current one
                if (farmSeasons.size() > i + 1) {
                    seasonsToReturn.add(new FarmSeason(farmSeasons[i + 1], farmSeasons[i]));
                }
            }
        }
        return seasonsToReturn;
    }

    public List<SelectOption> getFarms() {
        List<SelectOption> options = new List<SelectOption>();
        for (Individual_Relationship__c i : this.iRels) {
            options.add(new SelectOption(i.Farm__c, i.Farm__r.Name));
        }
        return options;
    }

    public class FarmSeason {

        public Farm_Season__c season { get; set; }
        public Farm_Season__c lastSeason { get; set; }
        public List<Target> targets { get; set; }

        public FarmSeason(Farm_Season__c fs, Farm_Season__c lastfs) {

            this.season = fs;
            this.lastSeason = lastfs;
            this.targets = new List<Target>();

            //get the current Target records, if there are none create new ones
            Map<String, Target__c> currentTargets = sortTargets(fs);

            //get the collection data for last year using the year before the farm season's reference period year
            //if there is no reference period data then put in last year
            String year = fs.Reference_Period__r.Start_Date__c != null ? String.valueOf(fs.Reference_Period__r.Start_Date__c.addYears(-1).year()) : String.valueOf(System.now().addYears(-1).year());
            Map<String, CollectionDataWrapper> collections = findCollectionData(year, fs.Farm__c);

            for (String m : currentTargets.keySet()) {
                //if there are no collection results found i.e. for months that have not been collected, set last season monthly kgms to 0
                Decimal lastSeasonMonthly = collections.get(m) != null ? collections.get(m).monthlyKgMS : 0;
                Decimal lastSeasonDailyAverageKgMS = collections.get(m) != null ? collections.get(m).getDailyAverageKgMS() : 0;
                targets.add(new Target(currentTargets.get(m), lastSeasonMonthly, lastSeasonDailyAverageKgMS, getDaysInMonthForReferencePeriod(Integer.valueOf(currentTargets.get(m).Month__c), fs.Reference_Period__r)));
            }
        }

        private Integer getDaysInMonthForReferencePeriod(Integer month, Reference_Period__c referencePeriod) {
            Integer daysInMonth;

            if (month > 6) {
                daysInMonth = Date.daysInMonth(referencePeriod.End_Date__c.year(), month);
            } else {
                daysInMonth = Date.daysInMonth(referencePeriod.Start_Date__c.year(), month);
            }

            return daysInMonth;
        }
    }

    public static Map<String, Target__c> sortTargets(Farm_Season__c fs) {

        //Sort the farm season's target records, if there are any missing add in new ones
        Map<String, Target__c> targetMap = new Map<String, Target__c>();
        for (Target__c t : fs.Targets__r) {
            targetMap.put(months.get(Integer.valueOf(t.Month__c)), t);
        }

        Map<String, Target__c> sortedTargetMap = new Map<String, Target__c>();
        if (targetMap.containsKey('July')) {
            sortedTargetMap.put('July', targetMap.get('July'));
        } else {
            sortedTargetMap.put('July', new Target__c(Season__c = fs.Id, Month__c = '7', Target_KgMS__c = 0));
        }

        if (targetMap.containsKey('August')) {
            sortedTargetMap.put('August', targetMap.get('August'));
        } else {
            sortedTargetMap.put('August', new Target__c(Season__c = fs.Id, Month__c = '8', Target_KgMS__c = 0));
        }

        if (targetMap.containsKey('September')) {
            sortedTargetMap.put('September', targetMap.get('September'));
        } else {
            sortedTargetMap.put('September', new Target__c(Season__c = fs.Id, Month__c = '9', Target_KgMS__c = 0));
        }

        if (targetMap.containsKey('October')) {
            sortedTargetMap.put('October', targetMap.get('October'));
        } else {
            sortedTargetMap.put('October', new Target__c(Season__c = fs.Id, Month__c = '10', Target_KgMS__c = 0));
        }

        if (targetMap.containsKey('November')) {
            sortedTargetMap.put('November', targetMap.get('November'));
        } else {
            sortedTargetMap.put('November', new Target__c(Season__c = fs.Id, Month__c = '11', Target_KgMS__c = 0));
        }

        if (targetMap.containsKey('December')) {
            sortedTargetMap.put('December', targetMap.get('December'));
        } else {
            sortedTargetMap.put('December', new Target__c(Season__c = fs.Id, Month__c = '12', Target_KgMS__c = 0));
        }

        if (targetMap.containsKey('January')) {
            sortedTargetMap.put('January', targetMap.get('January'));
        } else {
            sortedTargetMap.put('January', new Target__c(Season__c = fs.Id, Month__c = '1', Target_KgMS__c = 0));
        }

        if (targetMap.containsKey('February')) {
            sortedTargetMap.put('February', targetMap.get('February'));
        } else {
            sortedTargetMap.put('February', new Target__c(Season__c = fs.Id, Month__c = '2', Target_KgMS__c = 0));
        }

        if (targetMap.containsKey('March')) {
            sortedTargetMap.put('March', targetMap.get('March'));
        } else {
            sortedTargetMap.put('March', new Target__c(Season__c = fs.Id, Month__c = '3', Target_KgMS__c = 0));
        }

        if (targetMap.containsKey('April')) {
            sortedTargetMap.put('April', targetMap.get('April'));
        } else {
            sortedTargetMap.put('April', new Target__c(Season__c = fs.Id, Month__c = '4', Target_KgMS__c = 0));
        }

        if (targetMap.containsKey('May')) {
            sortedTargetMap.put('May', targetMap.get('May'));
        } else {
            sortedTargetMap.put('May', new Target__c(Season__c = fs.Id, Month__c = '5', Target_KgMS__c = 0));
        }

        if (targetMap.containsKey('June')) {
            sortedTargetMap.put('June', targetMap.get('June'));
        } else {
            sortedTargetMap.put('June', new Target__c(Season__c = fs.Id, Month__c = '6', Target_KgMS__c = 0));
        }

        return sortedTargetMap;
    }

    public class CollectionDataWrapper {
        public Decimal monthlyKgMS { get; set; }
        private Date collectionDate { get; set; }

        public CollectionDataWrapper(Date collectionDate) {
            this.collectionDate = collectionDate;
            this.monthlyKgMS = 0.0;
        }

        public void addEntry(Decimal kgMS) {
            monthlyKgMS += kgMS;
        }

        public Decimal getDailyAverageKgMS() {
            Decimal dailyAverage = monthlyKgMS / Date.daysInMonth(collectionDate.year(), collectionDate.month());
            return dailyAverage.setScale(2);
        }
    }

    public static Map<String, CollectionDataWrapper> findCollectionData(String year, Id farmId) {
        //find and summarise Collection records for the given farm and season

        //define the season's start and end dates based on the supplied year
        Date startDate = date.parse('01/07/' + year);
        Date endDate = date.parse('30/06/' + year).addYears(1);

        //find all of the collection records for the season
        List<Collection__c> collections = AMSCollectionService.getCollections(startDate, endDate, farmId);

        //loop over the collection records and add them to a map totalling each months collections
        Map<String, CollectionDataWrapper> collectionTotals = new Map<String, CollectionDataWrapper>();
        for (Collection__c c : collections) {
            if (!collectionTotals.containsKey(months.get(c.Pick_Up_Date__c.month()))) {
                collectionTotals.put(months.get(c.Pick_Up_Date__c.month()), new CollectionDataWrapper(c.Pick_Up_Date__c));
            }

            CollectionDataWrapper wrapper = collectionTotals.get(months.get(c.Pick_Up_Date__c.month()));
            wrapper.addEntry(c.Total_KgMS__c);
        }

        return collectionTotals;
    }

    public class Target {
        public Target__c record { get; set; }
        public Decimal lastSeasonMonthlyKgMS { get; set; }
        public Decimal lastSeasonDailyAverageKgMS { get; set; }
        public Integer daysInCurrentMonth { get; set; }

        public Target(Target__c t, Decimal lsm, Decimal lsda, Integer dicm) {
            this.record = t;
            this.lastSeasonMonthlyKgMS = lsm;
            this.lastSeasonDailyAverageKgMS = lsda;
            this.daysInCurrentMonth = dicm;
        }
    }
}