/**
 * Description: The trigger handler for the Agreement Application Trigger
 * @author: Max (Trineo)
 * @date: 2021
 */
public with sharing class AgreementApplicationTriggerHandler extends TriggerHandler {
    
    public override void afterUpdate() {
        createNewAgreements(Trigger.new);
    }

    public static void createNewAgreements(List<Non_Standard_Agreement__c> nsaList) {
        List<Agreement__c> newAgreements = new List<Agreement__c>();
        for (Non_Standard_Agreement__c nsa : nsaList) {
            if ((nsa.Opportunity__c == null) && nsa.Status__c == 'Approved') {
                Agreement__c newAgreement = new Agreement__c(
                    Farm__c = nsa.Farm__c,
                    Party__c = nsa.Party__c,
                    Start_Date__c = nsa.Agreement_Start_Date__c,
                    End_Date__c = nsa.Agreement_End_Date__c,
                    Agreement_Status__c = 'Pending',
                    Agreement_Application__c = nsa.Id,
                    Agreement__c = 'Standard'
                );
                newAgreements.add(newAgreement);
            }
        }
        insert newAgreements;
    }
}