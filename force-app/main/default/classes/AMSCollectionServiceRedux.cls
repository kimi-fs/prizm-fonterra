/**
 * Description:
 *
 * Eventual replacement for the mess that is AMSCollectionService- currently only does the basic retrieval of collections
 * and the aggregation of collection records by farm/date. Support aggregation across multiple farms.
 *
 * @author: John Au (Trineo)
 * @date: Sep 2018
 * @test: AMSCollectionServiceReduxTest
 */
public without sharing class AMSCollectionServiceRedux {

    public enum AggregateType {
        DAILY,
        MONTHLY,
        TOTAL
    }

    public class CollectionAggregate {
        public Decimal kgMSTotal { get; set; }
        public Decimal volumeLitresTotal { get; set; }
        public Decimal fatKgsTotal { get; set; }
        public Decimal proteinKgsTotal { get; set; }

        public Decimal fatPercentageTotal { get; set; }
        public Decimal proteinPercentageTotal { get; set; }
        public Decimal kgMSPercentageTotal { get; set; }

        public Date pickUpDate { get; set; }
        public AggregateType aggregateType { get; set; }

        public CollectionAggregate() {

        }

        public CollectionAggregate(Date pickUpDate, AggregateType aggregateType) {
            this.pickUpDate = pickUpDate;
            this.aggregateType = aggregateType;
        }

        public void add(CollectionAggregate otherAggregate) {
            if (otherAggregate.kgMSTotal != null) {
                this.kgMSTotal = AMSCollectionServiceRedux.nullValue(this.kgMSTotal, 0) + otherAggregate.kgMSTotal;
            }
            if (otherAggregate.volumeLitresTotal != null) {
                this.volumeLitresTotal = AMSCollectionServiceRedux.nullValue(this.volumeLitresTotal, 0) + otherAggregate.volumeLitresTotal;
            }
            if (otherAggregate.fatKgsTotal != null) {
                this.fatKgsTotal = AMSCollectionServiceRedux.nullValue(this.fatKgsTotal, 0) + otherAggregate.fatKgsTotal;
            }
            if (otherAggregate.proteinKgsTotal != null) {
                this.proteinKgsTotal = AMSCollectionServiceRedux.nullValue(this.proteinKgsTotal, 0) + otherAggregate.proteinKgsTotal;
            }


        }
    }


    public class CollectionVariance {
        public CollectionAggregate thisSeason { get;set; }
        public CollectionAggregate lastSeason { get;set; }

        public Decimal varianceLitres { get;set; }
        public Decimal varianceLitresPct  { get;set; }
        public Decimal varianceKGs  { get;set; }
        public Decimal varianceKGsPct  { get;set; }
        public String  month { get;set; }

        public CollectionVariance() {

        }

        public CollectionVariance (CollectionAggregate thisSeason, CollectionAggregate lastSeason) {
            this.thisSeason = thisSeason;
            this.lastSeason = lastSeason;
        }

    }

    public class UnitOfMeasurement {
        public String name { get; set; }
        public String label { get; set; }
        public Integer precision { get; set; }
        public Boolean isPercentage { get; set; }
        public Boolean showTotal { get; set; }
        public Boolean showAverage { get; set; }

        public UnitOfMeasurement(String name, String label, Integer precision, Boolean isPercentage, Boolean showTotal, Boolean showAverage) {
            this.name = name;
            this.label = label;
            this.precision = precision;
            this.isPercentage = isPercentage;
            this.showTotal = showTotal;
            this.showAverage = showAverage;
        }

        public SelectOption getSelectOption() {
            return new SelectOption(this.name, this.label);
        }

        public Boolean equals(Object uom) {
            if (!(uom instanceof AMSCollectionServiceRedux.UnitOfMeasurement)) {
                return false;
            }

            Boolean equals = false;

            AMSCollectionServiceRedux.UnitOfMeasurement otherUom = (AMSCollectionServiceRedux.UnitOfMeasurement) uom;
            if (this.name == otherUom.name) {
                equals = true;
            }

            return equals;
        }

        public Integer hashCode() {
            return name.hashCode();
        }
    }

    private static final String INVALID_COLLECTION_QUERY_CRITERIA = ' (Volume_Ltrs__c != 0 AND Total_KgMS__c != 0 AND Fat_Kgs__c != 0 AND Prot_Kgs__c != 0 AND Pick_Up_Date__c != null)';

    private static final List<String> COLLECTION_FIELDS_TO_QUERY = new List<String> {
        'Id',
        'Pick_Up_Date__c',
        'Total_KgMS__c',
        'Protein_to_Fat_Ratio__c',
        'KgMS_Percent__c',
        'Farm_ID__c',
        'Volume_Ltrs__c',
        'Fat_Kgs__c',
        'Prot_Kgs__c',
        'Temperature__c',
        'Fat_Percent__c',
        'Prot_Percent__c',
        '(SELECT Id, Name, Test_Result__c, Result_Type__c, Result_Interpretation__c From Milk_Quality__r)'
    };

    public static final UnitOfMeasurement KgMS = new UnitOfMeasurement('kgMSTotal', 'KgMS', 2, false, true, true);
    public static final UnitOfMeasurement Litres = new UnitOfMeasurement('volumeLitresTotal', 'Litres', 0, false, true, true);
    public static final UnitOfMeasurement ProteinPercentage = new UnitOfMeasurement('proteinPercentageTotal', 'Protein (%)', 2, true, false, true);
    public static final UnitOfMeasurement FatPercentage = new UnitOfMeasurement('fatPercentageTotal', 'Fat (%)', 2, true, false, true);
    public static final UnitOfMeasurement KgMSPercentage = new UnitOfMeasurement('kgMSPercentageTotal', 'KgMS (%)', 2, true, false, true);
    public static final UnitOfMeasurement FatKg = new UnitOfMeasurement('fatKgsTotal', 'Fat (kg)', 2, false, true, true);
    public static final UnitOfMeasurement ProteinKg = new UnitOfMeasurement('proteinKgsTotal', 'Protein (kg)', 2, false, true, true);

    private static final List<String> COLLECTION_FIELDS_TO_AGGREGATE = new List<String> {
        'COUNT(Id) collectionCount',
        'CALENDAR_MONTH(Pick_Up_Date__c) collectionMonth',
        'Pick_Up_Date__c pickUpDate',
        'SUM(Total_KgMS__c) ' + KgMS.name,
        'SUM(Volume_Ltrs__c) ' + Litres.name,
        'SUM(Fat_Kgs__c) ' + FatKg.name,
        'SUM(Prot_Kgs__c) ' + ProteinKg.name
    };

    private static final List<String> COLLECTION_FIELDS_TO_AGGREGATE_2 = new List<String> {
        'CALENDAR_YEAR(Pick_Up_Date__c)  collectionYear',
        'CALENDAR_MONTH(Pick_Up_Date__c) collectionMonth',
        'SUM(Total_KgMS__c) ' + KgMS.name,
        'SUM(Volume_Ltrs__c) ' + Litres.name,
        'SUM(Fat_Kgs__c) ' + FatKg.name,
        'SUM(Prot_Kgs__c) ' + ProteinKg.name
    };

    //select options for the AMSLineChartRedux component
    public final static List<SelectOption> GRAPH_SELECT_OPTIONS = new List<SelectOption>{
        KgMS.getSelectOption(),
        Litres.getSelectOption(),
        ProteinPercentage.getSelectOption(),
        FatPercentage.getSelectOption(),
        KgMSPercentage.getSelectOption(),
        ProteinKg.getSelectOption(),
        FatKg.getSelectOption()
    };

    public final static Map<String, UnitOfMeasurement> GRAPH_SELECT_OPTIONS_TO_UNIT_OF_MEASUREMENT_MAP = new Map<String, UnitOfMeasurement>{
        KgMS.name => KgMS,
        Litres.name => Litres,
        ProteinPercentage.name => ProteinPercentage,
        FatPercentage.name => FatPercentage,
        KgMSPercentage.name => KgMSPercentage,
        FatKg.name => FatKg,
        ProteinKg.name => ProteinKg
    };

    /**
     * Returns a raw list of Collection__c records
     *
     * @param farmIds List of farm IDs
     * @param startDate Start of pick-up date range inclusive
     * @param endDate End of pick-up date range inclusive
     */
    public static List<Collection__c> queryCollections(List<Id> farmIds, Date startDate, Date endDate) {
        String query = 'SELECT ';

        query += String.join(COLLECTION_FIELDS_TO_QUERY, ', ');

        query += ' FROM Collection__c';
        query += ' WHERE';
        query += ' Pick_Up_Date__c >= :startDate AND Pick_Up_Date__c <= :endDate AND';
        query += INVALID_COLLECTION_QUERY_CRITERIA;
        query += createFarmConditionQuery(farmIds);

        return Database.query(query);
    }

    /**
     * Returns a list of aggregates for all the specified farms-
     * Each CollectionAggregate object relates to a period of time defined by AggregateType
     *
     * @param farmIds List of farm IDs
     * @param startDate Start of pick-up date range inclusive
     * @param endDate End of pick-up date range inclusive
     * @param aggregateType The aggregate type
     */
    public static List<CollectionAggregate> getAggregatesForFarms(List<Id> farmIds, Date startDate, Date endDate, AggregateType aggregateType) {
        return getAggregatesForFarmsByAggregateDate(farmIds, startDate, endDate, aggregateType).values();
    }

    /**
     * Returns a single aggregate for all the specified farms over the defined period
     *
     * @param farmIds List of farm IDs
     * @param startDate Start of pick-up date range inclusive
     * @param endDate End of pick-up date range inclusive
     */
    public static CollectionAggregate getTotalAggregateForFarms(List<Id> farmIds, Date startDate, Date endDate) {
        CollectionAggregate collectionAggregate = new CollectionAggregate(null, AMSCollectionServiceRedux.AggregateType.TOTAL);
        List<AggregateResult> aggregateResults = queryCollectionAggregates(farmIds, startDate, endDate);

        Decimal kgMSTotal = 0.0;
        Decimal volumeLitresTotal = 0.0;
        Decimal fatKgsTotal = 0.0;
        Decimal proteinKgsTotal = 0.0;

        for (AggregateResult aggregateResult : aggregateResults) {
            kgMSTotal += (Decimal) aggregateResult.get('kgMSTotal');
            volumeLitresTotal += (Decimal) aggregateResult.get('volumeLitresTotal');
            fatKgsTotal += (Decimal) aggregateResult.get('fatKgsTotal');
            proteinKgsTotal += (Decimal) aggregateResult.get('proteinKgsTotal');
        }

        collectionAggregate.kgMSTotal = kgMSTotal;
        collectionAggregate.volumeLitresTotal = volumeLitresTotal;
        collectionAggregate.fatKgsTotal = fatKgsTotal;
        collectionAggregate.proteinKgsTotal = proteinKgsTotal;

        if (volumeLitresTotal > 0) {
            collectionAggregate.fatPercentageTotal = (fatKgsTotal / volumeLitresTotal) * 100;
            collectionAggregate.proteinPercentageTotal = (proteinKgsTotal / volumeLitresTotal) * 100;
            collectionAggregate.kgMSPercentageTotal = (kgMSTotal / volumeLitresTotal) * 100;
        }

        return collectionAggregate;
    }


    public static Integer countCollections(List<Id> farmIds, Date startDate, Date endDate) {
        return [SELECT COUNT() FROM Collection__c WHERE Farm_Id__c In :farmIds AND Pick_Up_Date__c >= :startDate AND Pick_Up_Date__c <= :endDate];
    }


    /**
     * Returns Farm aggregates and variances with the previous season
     * Last Entry stores the Totals
     *`
     * @param farmIds List of farm IDs
     * @param startDate Start of pick-up date range inclusive
     * @param endDate End of pick-up date range inclusive
     */
    public static List<CollectionVariance> getCollectionVarianceForFarms(List<Id> farmIds, Date startDate, Date endDate) {
        List<CollectionVariance> collVarianceList = new List<CollectionVariance>();

        Map<Date, CollectionAggregate> thisSeason = new Map<Date, CollectionAggregate>();
        Map<Date, CollectionAggregate> lastSeason = new Map<Date, CollectionAggregate>();


        List<AggregateResult> aggrResults = queryCollectionAggregatesByMonth(farmIds, startDate, endDate);

        for (AggregateResult result : aggrResults) {

            Integer year  = (Integer) result.get('collectionYear');
            Integer month = (Integer) result.get('collectionMonth');

            Date summaryDate = Date.newInstance(year, month, 1);

            CollectionAggregate collectionAggregate = new CollectionAggregate(summaryDate, AggregateType.MONTHLY);
            collectionAggregate.kgMSTotal = (Decimal) result.get(KgMS.name);
            collectionAggregate.volumeLitresTotal = (Decimal) result.get(Litres.name);
            collectionAggregate.fatKgsTotal = (Decimal) result.get(FatKg.name);
            collectionAggregate.proteinKgsTotal = (Decimal) result.get(ProteinKg.name);

            if (summaryDate >= startDate.toStartOfMonth()) {
                // This is for the current season
                thisSeason.put(summaryDate, collectionAggregate);
            }
            else {
                // This is for the last season
                lastSeason.put(summaryDate, collectionAggregate);
            }

        }

        // Populate variance list starting from the End date up to the Start date
        Date summaryDate = endDate.toStartOfMonth();
        CollectionVariance totals = new CollectionVariance(new CollectionAggregate(), new CollectionAggregate());
        totals.month = 'Total';

        while (summaryDate >= startDate.toStartOfMonth()) {

            CollectionVariance collectionVariance = new CollectionVariance(thisSeason.get(summaryDate), lastSeason.get(summaryDate.addYears(-1)));
            collectionVariance.month = DateTime.newInstance(summaryDate.year(), summaryDate.month(), 1).format('MMM');

            if (collectionVariance.thisSeason != null && collectionVariance.lastSeason != null) {

                if (collectionVariance.thisSeason.volumeLitresTotal != null && collectionVariance.lastSeason.volumeLitresTotal != null) {
                    collectionVariance.varianceLitres     = collectionVariance.thisSeason.volumeLitresTotal - collectionVariance.lastSeason.volumeLitresTotal;
                    collectionVariance.varianceLitresPct  = getPercentage(collectionVariance.varianceLitres, collectionVariance.lastSeason.volumeLitresTotal);

                }

                if (collectionVariance.thisSeason.kgMSTotal != null && collectionVariance.lastSeason.kgMSTotal != null) {
                    collectionVariance.varianceKGs     = collectionVariance.thisSeason.kgMSTotal - collectionVariance.lastSeason.kgMSTotal;
                    collectionVariance.varianceKGsPct  = getPercentage(collectionVariance.varianceKGs, collectionVariance.lastSeason.kgMSTotal);
                }

            }
            // Keep track of totals
            if (collectionVariance.thisSeason != null) {
                totals.thisSeason.add(collectionVariance.thisSeason);
            }
            if (collectionVariance.lastSeason != null) {
                totals.lastSeason.add(collectionVariance.lastSeason);
            }


            collVarianceList.add(collectionVariance);
            summaryDate = summaryDate.addMonths(-1);
        }

        // Calculate Totals variance
        if (totals.thisSeason.volumeLitresTotal != null && totals.lastSeason.volumeLitresTotal != null) {
            totals.varianceLitres     = totals.thisSeason.volumeLitresTotal - totals.lastSeason.volumeLitresTotal;
            totals.varianceLitresPct  = getPercentage(totals.varianceLitres, totals.lastSeason.volumeLitresTotal);
        }

        if (totals.thisSeason.kgMSTotal != null && totals.lastSeason.kgMSTotal != null) {
            totals.varianceKGs     = totals.thisSeason.kgMSTotal - totals.lastSeason.kgMSTotal;
            totals.varianceKGsPct  = getPercentage(totals.varianceKGs, totals.lastSeason.kgMSTotal);
        }
        // Add totals to list
        collVarianceList.add(totals);


        return collVarianceList;
    }



    private static String createFarmConditionQuery(List<Id> farmIds) {
        String query = '';

        if (!farmIds.isEmpty()) {
            query += ' AND (';

            for (Id farmId : farmIds) {
                query += 'Farm_Id__c = \'' + farmId + '\' OR ';
            }

            query = query.removeEnd(' OR ');
            query += ')';
        }

        return query;
    }

    private static List<AggregateResult> queryCollectionAggregates(List<Id> farmIds, Date startDate, Date endDate) {
        String query = 'SELECT ';

        query += String.join(COLLECTION_FIELDS_TO_AGGREGATE, ', ');

        query += ' FROM Collection__c';
        query += ' WHERE';
        query += ' Pick_Up_Date__c >= :startDate AND Pick_Up_Date__c <= :endDate AND';
        query += INVALID_COLLECTION_QUERY_CRITERIA;
        query += createFarmConditionQuery(farmIds);
        query += ' GROUP BY Pick_Up_Date__c';
        query += ' ORDER BY Pick_Up_Date__c DESC';

        return Database.query(query);
    }

    private static Map<Date, CollectionAggregate> getAggregatesForFarmsByAggregateDate(List<Id> farmIds, Date startDate, Date endDate, AggregateType aggregateType) {
        Map<Date, CollectionAggregate> resultsByCollectionDate = new Map<Date, CollectionAggregate>();
        List<AggregateResult> aggregateResults = queryCollectionAggregates(farmIds, startDate, endDate);

        for (AggregateResult aggregateResult : aggregateResults) {
            Date collectionDate;
            if (aggregateType == AMSCollectionServiceRedux.AggregateType.DAILY) {
                collectionDate = ((DateTime) aggregateResult.get('pickUpDate')).date();
            } else if (aggregateType == AMSCollectionServiceRedux.AggregateType.MONTHLY) {
                collectionDate = ((DateTime) aggregateResult.get('pickUpDate')).date().toStartOfMonth();
            }

            if (!resultsByCollectionDate.containsKey(collectionDate)) {
                CollectionAggregate collectionAggregate = new CollectionAggregate(collectionDate, aggregateType);

                collectionAggregate.kgMSTotal = 0.0;
                collectionAggregate.volumeLitresTotal = 0.0;
                collectionAggregate.fatKgsTotal = 0.0;
                collectionAggregate.proteinKgsTotal = 0.0;

                resultsByCollectionDate.put(collectionDate, collectionAggregate);
            }

            CollectionAggregate collectionAggregate = resultsByCollectionDate.get(collectionDate);

            collectionAggregate.kgMSTotal = collectionAggregate.kgMSTotal + (Decimal) aggregateResult.get('kgMSTotal');
            collectionAggregate.volumeLitresTotal = collectionAggregate.volumeLitresTotal + (Decimal) aggregateResult.get('volumeLitresTotal');
            collectionAggregate.fatKgsTotal = collectionAggregate.fatKgsTotal + (Decimal) aggregateResult.get('fatKgsTotal');
            collectionAggregate.proteinKgsTotal = collectionAggregate.proteinKgsTotal + (Decimal) aggregateResult.get('proteinKgsTotal');
        }

        for (Date collectionDate : resultsByCollectionDate.keySet()) {
            CollectionAggregate collectionAggregate = resultsByCollectionDate.get(collectionDate);

            collectionAggregate.fatPercentageTotal = (collectionAggregate.fatKgsTotal / collectionAggregate.volumeLitresTotal) * 100;
            collectionAggregate.proteinPercentageTotal = (collectionAggregate.proteinKgsTotal / collectionAggregate.volumeLitresTotal) * 100;
            collectionAggregate.kgMSPercentageTotal = (collectionAggregate.kgMSTotal / collectionAggregate.volumeLitresTotal) * 100;
        }

        return resultsByCollectionDate;
    }



    private static List<AggregateResult> queryCollectionAggregatesByMonth(List<Id> farmIds, Date startDate, Date endDate) {

        Date lastSeasonStartDate = startDate.addYears(-1);
        Date lastSeasonEndDate   = endDate.addYears(-1);

        String query = 'SELECT ';

        query += String.join(COLLECTION_FIELDS_TO_AGGREGATE_2, ', ');

        query += ' FROM Collection__c';
        query += ' WHERE';
        query += ' ((Pick_Up_Date__c >= :startDate AND Pick_Up_Date__c <= :endDate) ';
        query += '     OR (Pick_Up_Date__c >= :lastSeasonStartDate AND Pick_Up_Date__c <= :lastSeasonEndDate)) ';
        query += createFarmConditionQuery(farmIds);
        query += ' GROUP BY Calendar_Year(Pick_Up_Date__c), Calendar_Month(Pick_Up_Date__c)';

        return Database.query(query);
    }


    private static Decimal getPercentage(Decimal variance, Decimal oldValue) {

        if (variance == null || oldValue == null) {
            return null;
        }

        if (variance == 0 && oldValue == 0) {
            return 0;
        }

        Decimal denom = (oldValue != 0) ? oldValue : variance;
        return variance * 100 / denom;

    }

    public static Decimal nullValue(Decimal value, Decimal altValue) {
        return (value == null) ? altValue : value;
    }
}