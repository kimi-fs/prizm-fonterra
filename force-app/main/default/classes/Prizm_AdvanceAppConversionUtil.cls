/* 
* Class Name   - Prizm_AdvanceAppConversionUtil
*
* Description  - Util Class for Prizm_AdvanceApplicationConverter
*
* Developer(s) - Financial Spectra
*/

public class Prizm_AdvanceAppConversionUtil {
    
    public static Map<Id, Advance_Application__c> getAdvanceApplicationMap(Set<Id> pAdvanceApplicationIDs){
        fsCore.DynamicQueryBuilder advanceApplicationQuery = fsCore.DynamicQueryFactory.createQuery('Advance_Application__c')
            .addFields()
            .addField('Lending_Application__r.Name')
            .addWhereConditionWithBind(1,'Id', 'IN', 'pAdvanceApplicationIDs');
        
        Map<Id, Advance_Application__c> advanceApplicationMap = new Map<Id, Advance_Application__c>(
            (List<Advance_Application__c>)Database.query(advanceApplicationQuery.getQueryString()));
        
        return advanceApplicationMap;
    }
    
    public static fsCore__Lending_Application_Itemization__c populateItemization(fsCore__Lending_Application__c pApp, fsCore__Product_Itemization_Setup__c pProdItem){
        
        fsCore__Lending_Application_Itemization__c appItem = new fsCore__Lending_Application_Itemization__c();   
        
        appItem.fsCore__Lending_Application_Number__c = pApp.Id;
        appItem.fsCore__Itemization_Name__c = pProdItem.fsCore__Itemization_Name__c;
        appItem.fsCore__Itemization_Family__c = pProdItem.fsCore__Itemization_Family__c;
        appItem.fsCore__Plus_Or_Minus__c = pProdItem.fsCore__Plus_Or_Minus__c;
        appItem.fsCore__Disbursement_Allowed__c = pProdItem.fsCore__Disbursement_Allowed__c;
        appItem.fsCore__Fee_Category__c = pProdItem.fsCore__Fee_Category__c;
        appItem.fsCore__Is_Override_Allowed__c = pProdItem.fsCore__Is_Override_Allowed__c;
        appItem.fsCore__Source__c = fsCore.Constants.ITMZ_SOURCE_PRODUCT;
        appItem.fsCore__Requested_Amount_Unsigned__c = pApp.fsCore__Loan_Amount__c;
        appItem.fsCore__Approved_Amount_Unsigned__c = pApp.fsCore__Loan_Amount__c;
        appItem.fsCore__Actual_Amount_Unsigned__c = pApp.fsCore__Loan_Amount__c;
        
        return appItem;
    }
    
    public static Map<Integer, String> getMonthNumberToMonthFieldMap(fsCore__Lending_Application__c pApp, Map<Integer, String> pNumberToMonthFieldMap){
        
        Map<Integer, String> monthNumberToMonthFieldMap = new Map<Integer, String>();
        Integer totalMonths = pNumberToMonthFieldMap.keyset().size();
        Integer startMonthNumber = pApp.fsCore__Contract_Date__c.month();
        
        for(integer month = 1; month <= totalMonths; month++){
            system.debug(loggingLevel.ERROR, month);
            if(month < startMonthNumber){
                monthNumberToMonthFieldMap.put((month + totalMonths - startMonthNumber + 1), pNumberToMonthFieldMap.get(month));
            }
            else{
                monthNumberToMonthFieldMap.put((month - startMonthNumber + 1), pNumberToMonthFieldMap.get(month));
            }
        }
        
        system.debug(loggingLevel.ERROR, monthNumberToMonthFieldMap);
        return monthNumberToMonthFieldMap;
    }
    
    public static List<fsCore__Lending_Application_Repayment_Schedule__c> getPredefinedPayments(fsCore__Lending_Application__c pApp, Advance_Application__c pAdApp, Map<integer, string> pMonthNumberToMonthFieldMap){
        
        List<fsCore__Lending_Application_Repayment_Schedule__c> predefinedPmts = new List<fsCore__Lending_Application_Repayment_Schedule__c>();
        date contractDate = pApp.fsCore__Contract_Date__c;

        for(integer month = 1; month <= 11; month++){
            
            decimal monthAmount = 0;
            if((Decimal)pAdApp.get(pMonthNumberToMonthFieldMap.get(month)) != null){
                monthAmount = (Decimal)pAdApp.get(pMonthNumberToMonthFieldMap.get(month));
            }
            
            fsCore__Lending_Application_Repayment_Schedule__c predefinedPmt = new fsCore__Lending_Application_Repayment_Schedule__c();
            
            predefinedPmt.fsCore__Start_Payment_Number__c = month;
            predefinedPmt.fsCore__Number_Of_Payments__c = 1;
            predefinedPmt.fsCore__Fixed_Principal_Amount__c = monthAmount;
            predefinedPmt.fsCore__Is_Predefined_Payment__c = true;
            predefinedPmt.fsCore__Lending_Application_Number__c = pApp.Id;
            predefinedPmt.fsCore__Start_Date__c = contractDate.addMonths(month).toStartofMonth().addDays(-1);
            
            predefinedPmts.add(predefinedPmt);
        }    
        
        return predefinedPmts;
        
    }
    
    public static Map<Integer, String> defaultMonthNumberToFieldMap(){
        Map<Integer, String> numberToMonthFieldMap = new Map<Integer, String>();
        List<Month_Number_to_Month_Field_Mapping__mdt> numberToMonthMdtRecords = [Select Month_Field_API_Name__c
                                                                                  , Month_Number__c
                                                                                  From Month_Number_to_Month_Field_Mapping__mdt];
        
        for(Month_Number_to_Month_Field_Mapping__mdt mdtRec : numberToMonthMdtRecords){
            numberToMonthFieldMap.put(Integer.valueOf(mdtRec.Month_Number__c), mdtRec.Month_Field_API_Name__c);
        }
        
        system.debug(loggingLevel.ERROR, numberToMonthFieldMap);
        
        return numberToMonthFieldMap;
    }
    
    public static Decimal getSumRepaymentsAmount(Advance_Application__c pAdApp, Map<integer, string> pMonthNumberToMonthFieldMap){

        decimal totalAmount = 0;
        for(integer month = 1; month <= 12; month++){
            
            decimal monthAmount = 0;
            if((Decimal)pAdApp.get(pMonthNumberToMonthFieldMap.get(month)) != null){
                monthAmount = (Decimal)pAdApp.get(pMonthNumberToMonthFieldMap.get(month));
            }
            totalAmount = totalAmount + monthAmount;
        }
        return totalAmount;
    }
}