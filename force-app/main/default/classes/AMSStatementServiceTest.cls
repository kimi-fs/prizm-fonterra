/**
 * Description: Test for AMSStatementService
 * @author: Amelia (Trineo)
 * @date: August 2017
 */
@isTest private class AMSStatementServiceTest {

    private class SAPPOCalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();

            AMSStatementService.SAPPOResponse sapResponse = new AMSStatementService.SAPPOResponse();

            sapResponse.Log = new AMSStatementService.Log();
            sapResponse.MilkCollectionStatementByElementsResponse = new AMSStatementService.MilkCollectionStatementByElementsResponse();
            sapResponse.MilkCollectionStatementByElementsResponse.PDFContent = 'boogaloo';

            response.setStatusCode(200);
            response.setBody(JSON.serialize(sapResponse));

            return response;
        }
    }

    @isTest static void testGetStatementEntries_WithDateInputs() {
        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        List<Account> farms = TestClassDataUtil.createAUFarms(1, true);
        List<Statement__c> statements = TestClassDataUtil.createStatements(party, farms, Date.today());
        List<Statement_Entry__c> statementEntries1 = TestClassDataUtil.createStatementEntries(statements[0]);

        Date fromDate = Date.today().addMonths(-1);
        Date toDate = Date.today().addMonths(1);
        List<Statement_Entry__c> statementEntries = AMSStatementService.getStatementEntries(party.Id, farms[0].Id, fromDate, toDate);

        System.assert(statementEntries.size() > 0, 'No records returned');
        System.assertEquals([SELECT count() FROM Statement_Entry__c WHERE Statement__r.Farm__c =: farms[0].Id AND Statement__r.Date__c >=: fromDate AND Statement__r.Date__c <=: toDate], statementEntries.size(), 'Wrong number of records returned');
    }

    @isTest static void testGetStatement_WithDateInputs() {
        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        List<Account> farms = TestClassDataUtil.createAUFarms(1, true);
        List<Statement__c> statements = TestClassDataUtil.createStatements(party, farms, Date.today());
        List<Statement_Entry__c> statementEntries1 = TestClassDataUtil.createStatementEntries(statements[0]);

        Date fromDate = Date.today().addMonths(-1);
        Date toDate = Date.today().addMonths(1);
        Statement__c statement = AMSStatementService.getStatement(party.Id, farms[0].Id, fromDate, toDate);

        System.assert(statement != null, 'No statement returned');
    }

    @isTest static void testGetLatestStatement_WithDateInputs() {
        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        List<Account> farms = TestClassDataUtil.createAUFarms(1, true);
        List<Statement__c> statements = new List<Statement__c>();
        statements.addAll(TestClassDataUtil.createStatements(party, farms, Date.newInstance(2011, 1, 1)));
        List<Statement_Entry__c> statementEntries1 = TestClassDataUtil.createStatementEntries(statements[0]);

        statements.addAll(TestClassDataUtil.createStatements(party, farms, Date.newInstance(2012, 2, 2)));
        List<Statement_Entry__c> statementEntries2 = TestClassDataUtil.createStatementEntries(statements[1]);

        Statement__c statement = AMSStatementService.getLatestStatement(party.Id, farms[0].Id);

        System.assertEquals(statement.Date__c.Year(), 2012);
        System.assertEquals(statement.Date__c.Month(), 2);
    }

    @isTest static void testGetStatementPDFAndGetStatementPDFAsJSON() {
        TestClassDataUtil.createAMSPOSettings(true);

        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        List<Account> farms = TestClassDataUtil.createAUFarms(1, true);
        List<Statement__c> statements = TestClassDataUtil.createStatements(party, farms, Date.today());
        List<Statement_Entry__c> statementEntries1 = TestClassDataUtil.createStatementEntries(statements[0]);

        Date fromDate = Date.today().addMonths(-1);
        Date toDate = Date.today().addMonths(1);

        Test.setMock(HttpCalloutMock.class, new SAPPOCalloutMock());

        Test.startTest();
        AMSStatementService.SAPPOResponse poResponse = AMSStatementService.getStatementPDF(statements[0].Id);
        String poResponseJSON = AMSStatementService.getStatementPDFAsJSON(statements[0].Id);
        Test.stopTest();

        System.assertNotEquals(null, poResponse.MilkCollectionStatementByElementsResponse.PDFContent, 'No PDF data returned');
        System.assertEquals(poResponseJSON, JSON.serialize(poResponse), 'JSON doesn\'t match object');
    }

}