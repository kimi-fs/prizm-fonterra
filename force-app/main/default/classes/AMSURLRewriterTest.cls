/*
* Test for AMSURLRewriter
*
* @author: John Au (Trineo)
* @date: 02/04/2020
*/
@IsTest
private class AMSURLRewriterTest {

    @IsTest
    private static void testURLRewriter() {
        AMSURLRewriter rewriter = new AMSURLRewriter();

        List<PageReference> mappedPageReferences = rewriter.generateUrlFor(new List<PageReference> { AMSURLRewriter.PRICING_PAGE });

        System.assertEquals(AMSURLRewriter.PRICING_PAGE, rewriter.mapRequestUrl(AMSURLRewriter.PRICING_PAGE_FRIENDLY));
        System.assertEquals(1, mappedPageReferences.size());
        System.assertEquals(AMSURLRewriter.PRICING_PAGE_FRIENDLY, mappedPageReferences[0]);
    }
}