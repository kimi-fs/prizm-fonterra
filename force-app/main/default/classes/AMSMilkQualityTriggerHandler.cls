public without sharing class AMSMilkQualityTriggerHandler {

    public static final String SMS_DEVELOPER_NAME = 'SMS';
    public static final String EMAIL_DEVELOPER_NAME = 'Email';

    public class MilkQualityResultFormatter {
        private Milk_Quality__c milkQuality;
        private Individual_Relationship__c individualRelationship;
        private Boolean alert { get; set; }

        public MilkQualityResultFormatter(Milk_Quality__c milkQuality, Individual_Relationship__c individualRelationship) {
            this.milkQuality = milkQuality;
            this.individualRelationship = individualRelationship;
            this.alert = milkQuality.Result_Interpretation__c != 'Alert';
        }

        private String getSubject() {
            String messageSubject = AMSScheduleBatchDailyMilk.getFarmRegionCodeAndName(milkQuality.Farm_No__r, individualRelationship.Entity_Nickname__c);

            messageSubject += ' Milk Quality';
            messageSubject += alert ? ' Information' : ' Alert';
            messageSubject += ' - ';
            messageSubject += milkQuality.Test_Type_Long_Description__c + ' test result ' + milkQuality.Test_Result__c + ' ';
            messageSubject += ((DateTime) milkQuality.Collection__r.Pick_Up_Date__c).format('dd/MM/yyyy');
            messageSubject += ' MP' + milkQuality.Collection__r.Pick_Up_Code__c + '.';

            return messageSubject;
        }

        public String getText() {
            String messageText = getSubject();

            messageText += alert ? ' This is within spec.' : ' This is outside the preferred spec.';
            messageText += ' For full details go to www.farmsource.com.au';

            return messageText;
        }
    }

    private static Map<Id, Channel_Preference__c> getChannelPreferenceByIndividualIdMap(List<Id> individualIds) {
        List<Channel_Preference__c> channelPreferences = [Select
                                                                Contact_ID__c,
                                                                DeliveryByEmail__c,
                                                                NotifyBySMS__c
                                                            From
                                                                Channel_Preference__c
                                                            Where
                                                                Contact_ID__c IN :individualIds
                                                                AND Channel_Preference_Setting__c = :AMSChannelPreferencesUtil.MILK_QUALITY_ALERT_SETTING.Id];

        Map<Id, Channel_Preference__c> channelPreferenceByIndividualIdMap = new Map<Id, Channel_Preference__c>();

        for (Channel_Preference__c channelPreference : channelPreferences) {
            channelPreferenceByIndividualIdMap.put(channelPreference.Contact_ID__c, channelPreference);
        }

        return channelPreferenceByIndividualIdMap;
    }

    public static void sendSMS(List<Milk_Quality__c> milkQualityAlerts) {
        //requery to get lookup fields
        milkQualityAlerts = [Select
                                Id,
                                Test_Result__c,
                                Test_Type_Long_Description__c,
                                Result_Interpretation__c,
                                Farm_No__c,
                                Farm_No__r.Name,
                                Farm_No__r.Farm_Region__c,
                                Farm_No__r.Farm_Region__r.Name,
                                Collection__r.Pick_Up_Date__c,
                                Collection__r.Pick_Up_Code__c
                            From
                                Milk_Quality__c
                            Where
                                Id In :milkQualityAlerts];

        List<Id> farmIds = new List<Id>();
        List<Id> individualIds = new List<Id>();
        Map<Id, Milk_Quality__c> milkQualiyAlertsByFarmIdMap = new Map<Id, Milk_Quality__c>();
        Map<Id, List<Individual_Relationship__c>> individualRelationshipsByFarmIdMap = new Map<Id, List<Individual_Relationship__c>>();
        Map<Id, Channel_Preference__c> channelPreferenceByIndividualIdMap;

        for (Milk_Quality__c milkQualityAlert : milkQualityAlerts) {
            farmIds.add(milkQualityAlert.Farm_No__c);
            milkQualiyAlertsByFarmIdMap.put(milkQualityAlert.Farm_No__c, milkQualityAlert);
        }

        List<Individual_Relationship__c> individualRelationships = RelationshipAccessService.individualRelationshipsWithProductionAndQualityNotifications(farmIds);

        for (Individual_Relationship__c individualRelationship : individualRelationships) {
            individualIds.add(individualRelationship.Individual__c);

            if (!individualRelationshipsByFarmIdMap.containsKey(individualRelationship.Farm__c)) {
                individualRelationshipsByFarmIdMap.put(individualRelationship.Farm__c, new List<Individual_Relationship__c>());
            }
            individualRelationshipsByFarmIdMap.get(individualRelationship.Farm__c).add(individualRelationship);
        }

        channelPreferenceByIndividualIdMap = getChannelPreferenceByIndividualIdMap(individualIds);

        List<Task> smsTasksForInsert = new List<Task>();
        List<Task> emailTasksForInsert = new List<Task>();

        for (Milk_Quality__c milkQualityAlert : milkQualityAlerts) {
            if (individualRelationshipsByFarmIdMap.containsKey(milkQualityAlert.Farm_No__c)) { //in case there are no valid irels
                for (Individual_Relationship__c individualRelationship : individualRelationshipsByFarmIdMap.get(milkQualityAlert.Farm_No__c)) {
                    MilkQualityResultFormatter formatter = new MilkQualityResultFormatter(milkQualityAlert, individualRelationship);
                    Channel_Preference__c individualChannelPreference = channelPreferenceByIndividualIdMap.get(individualRelationship.Individual__c);

                    if (individualChannelPreference != null){
                        if (individualChannelPreference.NotifyBySMS__c) {
                            Task t = createSMSTask(individualRelationship.Individual__c, individualRelationship.Individual__r.MobilePhone, milkQualityAlert.Farm_No__c, formatter.getText());
                            smsTasksForInsert.add(t);
                        }
                        if (individualChannelPreference.DeliveryByEmail__c) {
                            Task t = createEmailTask(individualRelationship.Individual__c, milkQualityAlert.Farm_No__c, formatter.getText(), formatter.getSubject());
                            emailTasksForInsert.add(t);
                        }
                    }
                }
            }
        }

        insert smsTasksForInsert;
        insert emailTasksForInsert;

        if (smsTasksForInsert.size() > 0) {
            Database.executeBatch(new ScheduleBatchSendSMS(smsTasksForInsert), 1);
        }
        if (emailTasksForInsert.size() > 0) {
            Database.executeBatch(new ScheduleBatchSendEmail(emailTasksForInsert), 1);
        }
    }

    private static Task createSMSTask(Id individualId, String mobileNumber, Id farmId, String msg) {
        Task task = createTask(individualId, mobileNumber, farmId, msg, SMS_DEVELOPER_NAME);
        task.Subject = AMSChannelPreferencesUtil.MILK_QUALITY_ALERT_SETTING.Name;

        return task;
    }

    private static Task createEmailTask(Id individualId, Id farmId, String msg, String subject) {
        Task task = createTask(individualId, null, farmId, msg, EMAIL_DEVELOPER_NAME);
        task.Subject = subject;

        return task;
    }

    private static Task createTask(Id individualId, String mobileNumber, Id farmId, String msg, String taskType) {
        Task task = new Task();

        task.Status = 'Scheduled';
        Task.Type = taskType;
        task.WhoId = individualId;
        task.WhatId = farmId;
        task.Description = msg;
        task.SMS_Sent_To__c = mobileNumber;
        task.ActivityDate = Date.today();
        task.RecordTypeId = GlobalUtility.activityTaskSystemGeneratedTaskRecordTypeId;

        return task;
    }
}