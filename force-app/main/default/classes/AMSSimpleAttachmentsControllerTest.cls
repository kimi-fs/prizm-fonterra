/**
 * AMSSimpleAttachmentsControllerTest.cls
 * Description: Test for AMSSimpleAttachmentsController.cls
 *
 * @author: John Au
 * @date: December 2018
 */
@IsTest
private class AMSSimpleAttachmentsControllerTest {

    private static final String TEST_USER_EMAIL = 'testuser@test.com';

    @IsTest
    private static void testController() {
        User u = TestClassDataUtil.createAMSCommunityUserAndIndividual(TEST_USER_EMAIL);
        Contact c = [SELECT Id FROM Contact Where Email = :TEST_USER_EMAIL];

        insert new Attachment(
            Body = Blob.valueOf('test'),
            ParentId = c.Id,
            Name = 'Test Attachment'
        );

        AMSSimpleAttachmentsController controller = new AMSSimpleAttachmentsController();

        // controller.parentIdParam = c.Id;
    }
}