/**
* Class Name  : Prizm_RateChangeJob
* Description : This batch job executes the logic to fulfill
the index rate change in active contracts.
*               
* Author      : Financial Spectra
*
*/

global with sharing class Prizm_RateChangeJob extends fsCore.BatchQueryableBase{
    private static final String CLASS_NAME = 'Prizm_RateChangeJob';
    private static final String RATE_CHANGE_TRANSACTION_CODE = 'RATE_CHANGED';
    private Map<Id, fsCore__Index_Rate_Setup__c> indexToIndexRateMap = new Map<Id, fsCore__Index_Rate_Setup__c>();
    private Map<String, List<fsCore.ErrorObject>> rateChangeProcessErrorsMap = new Map<String, List<fsCore.ErrorObject>>();
    private List<Id> diagnosticLogsGeneratedId = new List<Id>();
    private List<fsServ__Lending_Contract__c> scopeGenerated = new List<fsServ__Lending_Contract__c>();

    
    global Prizm_RateChangeJob(){
        setJobQuery(getDefaultQuery());
    }
    
    global override void setParameterizedJobQuery(){
        setJobQuery(getParameterizedQuery()); 
    }
    
    private fsCore.DynamicQueryBuilder getQuery(){
        
        fsCore.DynamicQueryBuilder jobQuery = fsCore.DynamicQueryFactory.createQuery(Schema.SObjectType.fsServ__Lending_Contract__c.getName())
            .addWhereConditionWithValue(1, 'fsServ__Is_Active__c', '=', true)
            .addWhereConditionWithValue(2, 'Index_Rate_Setup__c', '=',  null);
        jobQuery.addFields();
        jobQuery.addField('fsServ__Contract_Template_Name__r.fsCore__Index_Name__c');
        System.debug(logginglevel.ERROR, 'Query => ' + Database.query(jobQuery.getQueryString()));
        return jobQuery;
        
    }    
    
    
    private String getDefaultQuery(){
        return getQuery().getQueryString();
    }
    
    private String getParameterizedQuery(){
        fsCore.DynamicQueryBuilder jobQuery = getQuery();
        return jobQuery.getQueryString();
    }
    
    global override String getClassName(){
        return CLASS_NAME;
    }
    
    global override void startJob(Database.BatchableContext pContext){}
    
    global override List<fsCore.ErrorObject> executeJob(Database.BatchableContext pContext, List<sObject> pScope) {
        
        List<fsCore.ErrorObject> errorObjectList = new List<fsCore.ErrorObject>();
        List<fsServ__Lending_Contract__c> contractsToUpdate = new List<fsServ__Lending_Contract__c>();
        Map<String, List<fsCore.ErrorObject>> rateChangeProcessErrorsMap = new Map<String, List<fsCore.ErrorObject>>();
        
        System.debug(loggingLevel.ERROR, 'Executing Job ' + CLASS_NAME + '...');
        System.debug(loggingLevel.ERROR, 'Scope Count : ' + pScope.size());
        System.debug(loggingLevel.ERROR, 'Scope List : '+ pScope);
        scopeGenerated = (List<fsServ__Lending_Contract__c>)pScope;
        System.debug(loggingLevel.ERROR, 'Scope Generated : '+scopeGenerated);
        
        if (pScope.size() == 0){ return null; }
        
        try{
            
            indexToIndexRateMap = Prizm_RateChangeUtil.getIndexRateForRateChange();
            System.debug(loggingLevel.ERROR,indexToIndexRateMap+'Index Rate Map');
            List<fsServ.QueuedTransactionObject> rateChangeTransactionList = new List<fsServ.QueuedTransactionObject>();
            Map<String, List<fsCore__Transaction_Parameter_Setup__c>> transactionCodeToParamListMap = Prizm_RateChangeUtil.getTransactionIdToParmList(RATE_CHANGE_TRANSACTION_CODE);
          
            for(fsServ__Lending_Contract__c contract : (List<fsServ__Lending_Contract__c>)pScope)
            {
                
                fsCore__Index_Rate_Setup__c indexRateSetup = new fsCore__Index_Rate_Setup__c();
                if(indexToIndexRateMap.containsKey(contract.fsServ__Contract_Template_Name__r.fsCore__Index_Name__c))
                {
                    indexRateSetup = indexToIndexRateMap.get(contract.fsServ__Contract_Template_Name__r.fsCore__Index_Name__c);
                    if(contract.fsServ__Contract_Date__c < indexRateSetup.fsCore__Start_Date__c)
                    { 
                        rateChangeTransactionList.add(createRateChangeTransaction(contract, indexRateSetup.fsCore__Start_Date__c, indexRateSetup, transactionCodeToParamListMap));
                        
                    }
                    else if(contract.fsServ__Contract_Date__c >= indexRateSetup.fsCore__Start_Date__c)
                    {
                        rateChangeTransactionList.add(createRateChangeTransaction(contract, contract.fsServ__Contract_Date__c, indexRateSetup, transactionCodeToParamListMap));
                        
                    }
              
                }
            }
            
            System.debug(loggingLevel.ERROR, 'Rate change transaction List : '+ rateChangeTransactionList);
            
            if(rateChangeTransactionList.size()>0)
            {
            fsServ.QueuedTransactionProcessor txnProcessor = new fsServ.QueuedTransactionProcessor(rateChangeTransactionList);
            txnProcessor.create();
            Set<Id> queuedTxnIds = txnProcessor.getQueuedTransactionIDs();
                            
            
            txnProcessor.process();
                
                if(txnProcessor.hasErrors()){
                    errorObjectList.addAll(txnProcessor.getErrors());
                    rateChangeProcessErrorsMap = txnProcessor.getErrorsMap();
                    System.debug(loggingLevel.ERROR, 'rateChangeProcessErrorsMap = '+ rateChangeProcessErrorsMap);
                }
            }
            System.debug(loggingLevel.ERROR, 'Error Object List : '+ errorObjectList);
            System.debug(loggingLevel.ERROR, 'rateChangeProcessErrorsMap : '+ rateChangeProcessErrorsMap);
               
            for (fsServ__Lending_Contract__c contract :  (List<fsServ__Lending_Contract__c>)pScope){
                String contractId = String.valueOf(contract.Id);
                fsCore__Index_Rate_Setup__c indexRateSetup = new fsCore__Index_Rate_Setup__c();
                if (!rateChangeProcessErrorsMap.containsKey(contractId)){
                     indexRateSetup = indexToIndexRateMap.get(contract.fsServ__Contract_Template_Name__r.fsCore__Index_Name__c);
                     contract.Index_Rate_Setup__c = indexRateSetup.id;
                     contractsToUpdate.add(contract);
                    }
                else
                {
                   System.debug(loggingLevel.ERROR, 'Rate Change Transaction Creation Error in Contract : '+ contractId);
                }
                   
              }
            
            if(contractsToUpdate.size()>0)
            {
                 update contractsToUpdate;
            }
            
        }
        catch(Exception exp){
            System.debug(exp.getMessage());
            errorObjectList.add(Prizm_RateChangeUtil.getErrorObject(exp, null));
        }
        if(!errorObjectList.isEmpty()){
            insertErrorLog(errorObjectList);
        }
        
        return errorObjectList;
    }
    
    
    @TestVisible
    private void insertErrorLog(List<fsCore.ErrorObject> errorObjectList) {
        if (!errorObjectList.isEmpty()) {
            List<fscore__Diagnostic_Log__c> errorLogs = fsCore.DiagnosticLogUtil.getErrorLogs(
                CLASS_NAME,
                'execute',
                errorObjectList
            );
            insert errorLogs;
            for(fscore__Diagnostic_Log__c diagnosticLog : errorLogs)
                {
                    diagnosticLogsGeneratedId.add(diagnosticLog.id);
                }
            System.debug(loggingLevel.ERROR, 'diagnosticLogId Id : '+diagnosticLogsGeneratedId);
            System.debug(loggingLevel.ERROR, 'Insertion done' );
            System.debug(loggingLevel.ERROR, 'ERROR LOGS ID : '+errorLogs.size());
            System.debug(loggingLevel.ERROR, 'ERROR LOGS : '+errorLogs);
        }
    }
    
    
    private fsServ.QueuedTransactionObject createRateChangeTransaction(fsServ__Lending_Contract__c lendingContract, Date transactionDate, fsCore__Index_Rate_Setup__c indexRateSetup, Map<String, List<fsCore__Transaction_Parameter_Setup__c>> transactionCodeToParamListMap){
        fsServ.QueuedTransactionObject txnObj = new fsServ.QueuedTransactionObject();
        
        txnObj.lendingContractID = lendingContract.Id;
        txnObj.transactionCode = RATE_CHANGE_TRANSACTION_CODE; 
        txnObj.transactionDate = transactionDate;
        txnObj.branchCode = lendingContract.fsServ__Branch_Code__c;
        txnObj.creationMethod = null;
        txnObj.uniqueReferenceID = String.valueOf(lendingContract.Id);
       // txnObj.relatedRecordID = lendingContract.Id;
       // txnObj.relatedRecordName = lendingContract.Name;
        txnObj.relatedRecordID = indexRateSetup.Id;
        txnObj.relatedRecordName = indexRateSetup.Name;
        txnObj.saveAsDraft = false;
        txnObj.parameters = new List<fsServ.QueuedTransactionObject.ParameterObject>();
        
       for(fsCore__Transaction_Parameter_Setup__c transactionParam : transactionCodeToParamListMap.get(RATE_CHANGE_TRANSACTION_CODE)){
            fsServ.QueuedTransactionObject.ParameterObject param = new fsServ.QueuedTransactionObject.ParameterObject();
            param.parameterFieldName = transactionParam.fsCore__Field_Name__c;
            param.parameterOrder = Integer.valueOf(transactionParam.fsCore__Display_Order__c);
            if(transactionParam.fsCore__Default_Value__c != null){
                param.parameterValue = transactionParam.fsCore__Default_Value__c;
            }
            else{
                param.parameterValue = string.valueOf(indexRateSetup.fsCore__Index_Rate__c);
            }          
            txnObj.parameters.add(param);
        }
        
        
        return txnObj;
    }
    
    
    global override void finishJob(Database.BatchableContext pContext){
        
        Id jobId = pContext.getJobId();
       
        List<fsCore__Index_Rate_Setup__c> indexRateListToBeUpdated = new List<fsCore__Index_Rate_Setup__c>();
        List<fsCore__Index_Rate_Setup__c> indexRateUpdation = new List<fsCore__Index_Rate_Setup__c>();
        List<fsServ__Lending_Contract__c> rateUpdation = new List<fsServ__Lending_Contract__c>();
        
        for(Id indexMapId : indexToIndexRateMap.keySet())
        {
            indexRateListToBeUpdated.add(indexToIndexRateMap.get(indexMapId)); 
        }
         System.debug(loggingLevel.ERROR,'Index Rate List To be Updated : '+indexRateListToBeUpdated);  
    
        if(diagnosticLogsGeneratedId.size()==0 || rateChangeProcessErrorsMap.isEmpty()==true)
        {
        if(indexRateListToBeUpdated.size()>0)
        {
            for(fsCore__Index_Rate_Setup__c indexRate : indexRateListToBeUpdated)
            {
                indexRate.Job_Id__c = pContext.getJobId();
                indexRate.Is_Already_Picked__c = true;
                indexRateUpdation.add(indexRate);
            }
        }
       /* for(fsServ__Lending_Contract__c con : (List<fsServ__Lending_Contract__c>)scopeGenerated)
        {
            con.Index_Rate_Setup__c = null;
            rateUpdation.add(con);
        }*/
        }
        else
        {
        if(indexRateListToBeUpdated.size()>0)
        {
            for(fsCore__Index_Rate_Setup__c indexRate : indexRateListToBeUpdated)
            {
                indexRate.Job_Id__c = pContext.getJobId();
                indexRate.Is_Already_Picked__c = false;
                indexRateUpdation.add(indexRate);
            }
        }
        }
        update indexRateUpdation;
        update rateUpdation;
        
    }
}