/**
 * AMSContactServiceTest
 *
 * Test the AMSContactService
 *
 * Author: Ed (Trineo)
 * Date: July 2017
 */
@isTest
private class AMSContactServiceTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';

    /**
     * Setup a community user who is associated with a farm, that have fonterra reps associated with them
     */
    @testSetup
    static void testSetup() {
        // Prereqs
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.integrationUserProfile();
    }

    @isTest
    static void getTheContacts() {
        User communityUser;
        List<Account> farms;

         User integrationUser = TestClassDataUtil.createIntegrationUser();

        System.runAs(integrationUser) {

            communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual('test@farmuser.com');

            User fonterraUser = TestClassDataUtil.createAdminUser();

            // Connect the communityUser up to a party that owns these farms.
            farms = [SELECT Id FROM Account WHERE RecordTypeId = : GlobalUtility.auAccountFarmRecordTypeId];

            Account party = TestClassDataUtil.createSingleAccountPartyAU();
            Test.startTest();
            TestClassDataUtil.createPrimaryAccessRelationships(communityUser.ContactId, party, farms);
            Test.stopTest();
            for (Account farm : farms) {
                farm.Rep_Area_Manager__c = fonterraUser.Id;
                farm.Quality_Specialist__c = fonterraUser.Id;
            }
            update farms;

        }

        //Test.startTest();
        System.runAs(communityUser) {
            Map<String, List<AMSContactService.ContactUsWrapper>> contactUs = AMSContactService.getContactUsContacts(communityUser.ContactId);
            System.assert(contactUs.get(AMSContactService.AREA_MANAGER) != null);
            System.assert(contactUs.get(AMSContactService.QUALITY_SPECIALIST) != null);
            System.assertEquals(farms.size(), contactUs.get(AMSContactService.AREA_MANAGER).size(), 'Should only be one area manager, found ' + contactUs.get(AMSContactService.AREA_MANAGER));
            System.assertEquals(farms.size(), contactUs.get(AMSContactService.QUALITY_SPECIALIST).size(), 'Should only be one quality specialist, found ' + contactUs.get(AMSContactService.QUALITY_SPECIALIST));
        }

    }

    @isTest static void testIsValidMobile(){
        System.assertEquals(true, AMSContactService.isValidMobile('+61123123'), 'Mobile is valid');
        System.assertEquals(true, AMSContactService.isValidMobile('+61 12 31 23'), 'Mobile is valid');
        System.assertEquals(false, AMSContactService.isValidMobile('+64123123'), 'Mobile is invalid');
        System.assertEquals(false, AMSContactService.isValidMobile('61123123'), 'Mobile is invalid');
        System.assertEquals(false, AMSContactService.isValidMobile('abcdef'), 'Mobile is invalid');
        System.assertEquals(false, AMSContactService.isValidMobile(''), 'Mobile is invalid');
        System.assertEquals(false, AMSContactService.isValidMobile(' '), 'Mobile is invalid');
    }
}