/**
 * Description:  Controller for the MilkQualitySummary VF page
 * @author Eric Hu (Trineo)
 * @date  5/09/2018
 * @test MilkQualitySummaryControllerTest
 **/

public with sharing class MilkQualitySummaryController {
    public Id farmId { get; set; }
    public Integer pageSize { get; set; }

    public ApexPages.StandardSetController setCon {
        get {
            if (setCon == null) {
                setCon = new ApexPages.StandardSetController(
                    Database.getQueryLocator(
                        [
                            SELECT Id, Name, Pick_Up_Date__c, Volume_Ltrs__c, Fat_Percent__c, Prot_Percent__c,
                            (
                                SELECT Id, Name, Test_Result__c, Result_Type__c, Collection__c, Result_Interpretation__c
                                FROM Milk_Quality__r
                            )
                            FROM Collection__c
                            WHERE Farm_ID__c = : farmId
                            ORDER BY Pick_Up_Date__c DESC
                        ]
                    )
                );
                setCon.setPageSize(this.pageSize);
            }

            return setCon;
        }
        set;
    }

    // standard constructor used for the Milk Quality Summary page
    public MilkQualitySummaryController() {
        // get farm from URL
        this.farmId = ApexPages.currentPage().getParameters().get('id');
        this.pageSize = 30;
    }

    // overload constructor to serve as a controller extension for the Supply Dashboard page
    public MilkQualitySummaryController(ApexPages.StandardController controller) {
        // get farm from standard controller
        this.farmId = ((Account)controller.getRecord()).Id;
        this.pageSize = 5;
    }

    // Changes the size of pagination
    public void refreshPageSize() {
        setCon.setPageSize(this.pageSize);
    }

    /**
     * Takes the records from the Standard Set Controller and creates wrappers for display on page
     * Called when creating table in apex pages
     */
    public List<CollectionWrapper> getCollectionWrappers() {
        List<CollectionWrapper> wrappers = new List<CollectionWrapper>();
        List<Collection__c> collectionsOnPage = (List<Collection__c>)setCon.getRecords();
        for (Collection__c c : collectionsOnPage) {
            CollectionWrapper wrapper = new CollectionWrapper(c);
            wrappers.add(wrapper);
        }

        return wrappers;
    }

    public Class CollectionWrapper {
        public Collection__c collection { get; set; }
        public String temperature { get; set; }
        public String cellCount { get; set; }
        public String bactoscan { get; set; }
        public String thermodurics { get; set; }
        public String sediment { get; set; }
        public String antibiotics { get; set; }
        public Integer volume { get; set; }
        public Boolean alertBactoscan { get; set; }
        public Boolean alertThermodurics { get; set; }

        public CollectionWrapper(Collection__c collection) {
            this.collection = collection;

            this.volume = collection.Volume_Ltrs__c != null ? collection.Volume_Ltrs__c.intValue() : 0;

            List<Milk_Quality__c> mqList = collection.Milk_Quality__r;
            for (Milk_Quality__c mq : mqList) {
                if (mq.Name == GlobalUtility.TEMPERATURE_TEST_NAME) {
                    this.temperature = mq.Test_Result__c;
                } else if (mq.Name == GlobalUtility.CELL_COUNT_TEST_NAME) {
                    this.cellCount = mq.Test_Result__c;
                } else if (mq.Name == GlobalUtility.BACTOSCAN_TEST_NAME) {
                    this.alertBactoscan = isAlert(mq);
                    this.bactoscan = mq.Test_Result__c;
                } else if (mq.Name == GlobalUtility.THERMODURICS_TEST_NAME) {
                    this.alertThermodurics = isAlert(mq);
                    this.thermodurics = mq.Test_Result__c == '1' ? '<100' : mq.Test_Result__c;
                } else if (mq.Name == GlobalUtility.SEDIMENT_TEST_NAME) {
                    this.sediment = mq.Result_Type__c;
                } else if (mq.Name == GlobalUtility.ANTIBIOTICS_TEST_NAME) {
                    this.antibiotics = mq.Result_Type__c;
                }
            }
        }

        private Boolean isAlert(Milk_Quality__c mq) {
            return mq.Result_Interpretation__c == 'Alert';
        }
    }
}