/**
 * AMSKnowledgeService
 *
 * Service for retrieving Knowledge Articles, is also a kind of DAO class in disguise.
 *
 * Using dynamic queries so that we can easily have a consistent set of fields returned for the queries.
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately.
 *
 * Author: Ed (Trineo)
 * Date: July 2017
 */
public without sharing class AMSKnowledgeService {

    public static final String ADVICE_DATA_CATEGORY = 'AMS_Advice';
    public static final String NEWS_DATA_CATEGORY = 'AMS_News';

    public static final String AMS_ADVICE_RECORD_TYPE_DEVELOPER_NAME = 'AMS_Advice';
    public static final String AMS_FAQ_RECORD_TYPE_DEVELOPER_NAME = 'AMS_FAQ';
    public static final String AMS_NEWS_RECORD_TYPE_DEVELOPER_NAME = 'AMS_News';
    public static final String AMS_WEB_TEXT_RECORD_TYPE_DEVELOPER_NAME = 'AMS_Web_Text';

    // Fields that we always want for a news article
    private static List<String> newsFields = new List<String>{  'Id',
                                                                'UrlName',
                                                                'Title',
                                                                'Summary',
                                                                'FirstPublishedDate',
                                                                'Content__c',
                                                                'Author__c',
                                                                'Headline_Image__c',
                                                                'Thumbnail_Image__c'};

    private static List<String> adviceFields = new List<String>{'Id',
                                                                'Title',
                                                                'UrlName',
                                                                'Summary',
                                                                'FirstPublishedDate',
                                                                'Content__c',
                                                                'Summary_Content__c',
                                                                'Highlighted_Content__c',
                                                                'Display_Type__c',
                                                                'Sequence__c'};

    private static List<String> faqFields = new List<String>{   'Id',
                                                                'KnowledgeArticleId',
                                                                'Title',
                                                                'UrlName',
                                                                'Summary',
                                                                'FirstPublishedDate',
                                                                'Content__c'};

    private static List<String> webTextFields = new List<String>{   'Id',
                                                                    'Title',
                                                                    'UrlName',
                                                                    'Summary',
                                                                    'FirstPublishedDate',
                                                                    'Content__c',
                                                                    'Heading__c'};

    public static List<String> searchableArticleRecordTypes = new List<String>{
                                                                    'AMS_News',
                                                                    'AMS_Advice',
                                                                    'AMS_FAQ'
                                                                    };

    // Amount of text around the search match
    private static final Integer searchTextLength = 250;

    // Wrapper class for search results since they will be coming from different knowledge article
    // types.
    public class SearchResult implements Comparable {
        public String articleType { get; private set; }
        public String matchingTitle { get; private set; }
        public String matchingContent { get; private set; }
        public String matchingURL { get; private set; }
        public DateTime firstPublished { get; private set; }

        public SearchResult(String articleType, String matchingTitle, String matchingContent, String matchingURL, DateTime firstPublished) {
            this.articleType = articleType;
            this.matchingTitle = matchingTitle;
            this.matchingContent = matchingContent;
            this.matchingURL = matchingURL;
            this.firstPublished = firstPublished;
        }

        public Integer compareTo(Object compareTo) {
            SearchResult compareToKnowledge = (SearchResult)compareTo;
            if (this.firstPublished == compareToKnowledge.firstPublished) {
                return 0;
            } else if (this.firstPublished > compareToKnowledge.firstPublished) {
                return -1;
            } else {
                return 1;
            }
        }
    }

    /**
     * Wrapper for the Article Knowledge article categories.
     */
    public class ArticleCategoryWrapper {
        public String name {get;set;}
        public String label {get;set;}

        // Used to highlight the category when selected
        public Boolean currentArticle {get;set;}
        public Boolean childSelected {get;set;}

        public Knowledge__kav adviceArticle {get;set;}
        public List<ArticleCategoryWrapper> childDataCategoryWrappers {get;set;}

        public ArticleCategoryWrapper(String categoryName, String categoryLabel){
            this.name = categoryName;
            this.label = categoryLabel;
            this.currentArticle = false;
            this.childSelected = false;
            this.childDataCategoryWrappers = new List<ArticleCategoryWrapper>();
        }

        public ArticleCategoryWrapper(Knowledge__kav article){
            this.adviceArticle = article;
            this.currentArticle = false;
            this.childSelected = false;
        }
    }

    public static Integer totalNewsArticles() {
        Integer numberNewsArticles = [SELECT count() FROM Knowledge__kav WHERE PublishStatus = 'Online' AND Language = 'en_US' AND RecordType.DeveloperName = :AMS_NEWS_RECORD_TYPE_DEVELOPER_NAME];
        return numberNewsArticles;
    }

    public static Integer totalNewsArticles(String category) {
        category = (category != null) ? String.escapeSingleQuotes(category) : null;
        String categoryDeveloperName = category + '__c';
        Integer numberNewsArticles = Database.countQuery('SELECT count() FROM Knowledge__kav WHERE PublishStatus = \'Online\' AND RecordType.DeveloperName = :AMS_NEWS_RECORD_TYPE_DEVELOPER_NAME AND Language = \'en_US\' WITH DATA CATEGORY AMS_News__c ABOVE_OR_BELOW ' + categoryDeveloperName);
        return numberNewsArticles;
    }

    public static List<Knowledge__kav> newsArticlesPaginated(Integer offset, Integer pageSize) {
        String query =  'SELECT ' + String.join(newsFields, ',') + ' , ' +
                        ' (SELECT Id, ' +
                        '        ParentId, ' +
                        '        DataCategoryGroupName, ' +
                        '        DataCategoryName ' +
                        '  FROM DataCategorySelections) ' +
                        ' FROM Knowledge__kav ' +
                        ' WHERE PublishStatus = \'Online\' ' +
                        ' AND Language = \'en_US\' ' +
                        ' AND RecordType.DeveloperName = :AMS_NEWS_RECORD_TYPE_DEVELOPER_NAME' +
                        ' ORDER BY FirstPublishedDate DESC ' +
                        ' LIMIT :pageSize ' +
                        ' OFFSET :offset';

        List<Knowledge__kav> newsArticles = Database.query(query);
        return newsArticles;
    }

    public static List<Knowledge__kav> newsArticlesPaginated(Integer offset, Integer pageSize, String category) {
        category = (category != null) ? String.escapeSingleQuotes(category) : null;
        String categoryDeveloperName = category + '__c';
        String query =  'SELECT ' + String.join(newsFields, ',') + ' , ' +
                        ' (SELECT Id, ' +
                        '        ParentId, ' +
                        '        DataCategoryGroupName, ' +
                        '        DataCategoryName ' +
                        '  FROM DataCategorySelections) ' +
                        ' FROM Knowledge__kav ' +
                        ' WHERE PublishStatus = \'Online\' ' +
                        ' AND Language = \'en_US\' ' +
                        ' AND RecordType.DeveloperName = :AMS_NEWS_RECORD_TYPE_DEVELOPER_NAME' +
                        ' WITH DATA CATEGORY AMS_News__c ABOVE_OR_BELOW ' + categoryDeveloperName +
                        ' ORDER BY FirstPublishedDate DESC ' +
                        ' LIMIT :pageSize ' +
                        ' OFFSET :offset';

        List<Knowledge__kav> newsArticles = Database.query(query);
        return newsArticles;
    }

    public static List<Knowledge__kav> adviceArticles() {
        String query =  'SELECT ' + String.join(adviceFields, ',') + ' , ' +
                        ' (SELECT Id, ' +
                        '        ParentId, ' +
                        '        DataCategoryGroupName, ' +
                        '        DataCategoryName ' +
                        '  FROM DataCategorySelections) ' +
                        ' FROM Knowledge__kav ' +
                        ' WHERE PublishStatus = \'Online\' ' +
                        ' AND RecordType.DeveloperName = :AMS_ADVICE_RECORD_TYPE_DEVELOPER_NAME' +
                        ' AND Language = \'en_US\' ' +
                        ' ORDER BY Sequence__c ASC';


        List<Knowledge__kav> adviceArticles = Database.query(query);
        return adviceArticles;
    }

    public static Knowledge__kav faqArticleByUrl(String urlName) {
        Knowledge__kav faqArticle;

        String query =  'SELECT ' + String.join(faqFields, ',') + ' , ' +
                        ' (SELECT Id, ' +
                        '        ParentId, ' +
                        '        DataCategoryGroupName, ' +
                        '        DataCategoryName ' +
                        '  FROM DataCategorySelections) ' +
                        ' FROM Knowledge__kav ' +
                        ' WHERE PublishStatus = \'Online\' ' +
                        ' AND RecordType.DeveloperName = :AMS_FAQ_RECORD_TYPE_DEVELOPER_NAME' +
                        ' AND UrlName = :UrlName ' +
                        ' AND Language = \'en_US\' ' +
                        ' ORDER BY FirstPublishedDate DESC ' +
                        ' UPDATE VIEWSTAT';


        List<Knowledge__kav> faqArticles = Database.query(query);

        if (faqArticles.size() > 0) {
            faqArticle = faqArticles[0];
        }
        return faqArticle;
    }

    /**
     * We want the top viewed articles. This is a little awkward since the objects we want to query
     * are not directly related, but they share the same parent.
     *
     * So what we are doing is getting all of the FAQs, grabbing the parent Id (KnowledgeArticleId),
     * and then using that to grab the top 'maximum' articles, then pick out those articles for
     * returning - in the correct order.
     *
     * Note that we are using 'AllChannels' for the view stats, let's not limit ourselves.
     */
    public static List<Knowledge__kav> faqsArticlesTop(Integer maximum) {

        String query =  'SELECT ' + String.join(faqFields, ',') + ' , ' +
                        ' (SELECT Id, ' +
                        '        ParentId, ' +
                        '        DataCategoryGroupName, ' +
                        '        DataCategoryName ' +
                        '  FROM DataCategorySelections) ' +
                        ' FROM Knowledge__kav ' +
                        ' WHERE PublishStatus = \'Online\' ' +
                        ' AND RecordType.DeveloperName = :AMS_FAQ_RECORD_TYPE_DEVELOPER_NAME' +
                        ' AND Language = \'en_US\' ' +
                        ' ORDER BY FirstPublishedDate DESC';


        Map<Id, Knowledge__kav> knowledgeArticleIdMap = new Map<Id, Knowledge__kav>();
        List<Knowledge__kav> faqArticles = Database.query(query);
        for (Knowledge__kav faq : faqArticles) {
            knowledgeArticleIdMap.put(faq.KnowledgeArticleId, faq);
        }

        // Now get the view stats
        List<KnowledgeArticleViewStat> viewStats = [SELECT NormalizedScore, ParentId
                                                    FROM KnowledgeArticleViewStat
                                                    WHERE ParentId IN :knowledgeArticleIdMap.keySet()
                                                    AND Channel = 'AllChannels'
                                                    ORDER BY NormalizedScore DESC
                                                    LIMIT :maximum];

        System.debug(viewStats);

        // And match them up.
        List<Knowledge__kav> orderedFaqs = new List<Knowledge__kav>();
        for (KnowledgeArticleViewStat viewStat : viewStats) {
            Knowledge__kav matchedFaq = knowledgeArticleIdMap.get(viewStat.ParentId);
            if (matchedFaq != null) {
                orderedFaqs.add(matchedFaq);
            }
        }
        System.debug(orderedFaqs);

        return orderedFaqs;
    }

    public static List<Knowledge__kav> webTextArticles(String title) {
        String query =  'SELECT ' + String.join(webTextFields, ',') + ' , ' +
                        ' (SELECT Id, ' +
                        '        ParentId, ' +
                        '        DataCategoryGroupName, ' +
                        '        DataCategoryName ' +
                        '  FROM DataCategorySelections) ' +
                        ' FROM Knowledge__kav ' +
                        ' WHERE PublishStatus = \'Online\' ' +
                        ' AND RecordType.DeveloperName = :AMS_WEB_TEXT_RECORD_TYPE_DEVELOPER_NAME' +
                        ' AND Title = :title ' +
                        ' AND Language = \'en_US\' ' +
                        ' ORDER BY FirstPublishedDate DESC';


        List<Knowledge__kav> webTextArticles = Database.query(query);
        return webTextArticles;
    }

    public static List<Knowledge__kav> webTextArticlesByURLName(String urlName) {
        String query =  'SELECT ' + String.join(webTextFields, ',') + ' , ' +
                        ' (SELECT Id, ' +
                        '        ParentId, ' +
                        '        DataCategoryGroupName, ' +
                        '        DataCategoryName ' +
                        '  FROM DataCategorySelections) ' +
                        ' FROM Knowledge__kav ' +
                        ' WHERE PublishStatus = \'Online\' ' +
                        ' AND RecordType.DeveloperName = :AMS_WEB_TEXT_RECORD_TYPE_DEVELOPER_NAME' +
                        ' AND URLName = :urlName ' +
                        ' AND Language = \'en_US\' ' +
                        ' ORDER BY FirstPublishedDate DESC';


        List<Knowledge__kav> webTextArticles = Database.query(query);
        return webTextArticles;
    }

    /**
     * Count the number of results so that we can do our pagination, there aren't aggregation functions, so
     * perform the actual full search and count the matches.
     */
    public static Integer countMatchingArticles(String searchString, List<String> searchedArticleTypes) {
        if (String.isBlank(searchString) || searchString.length() < 2) {
            return 0;
        }

        // Don't allow any single quotes in here it will break the query.
        searchString = String.escapeSingleQuotes(searchString);

        Search.SearchResults searchResults = Search.find(
            'FIND \'' + searchString + '\' IN ALL FIELDS RETURNING ' +
            ' Knowledge__kav(Id, Title, RecordType.Name, UrlName, FirstPublishedDate ' +
                                     ' WHERE PublishStatus = \'Online\' AND ' +
                                     ' Language = \'en_US\' AND ' +
                                     ' RecordType.DeveloperName IN :searchedArticleTypes)');
        List<Search.SearchResult> articlelist = searchResults.get('Knowledge__kav');

        return articlelist.size();
    }

    /**
     * Find any articles that have a string that matches the searchString, allow the number of results to
     * be limited.
     *
     * The results are returned in a wrapper class so that the caller doesn't need to worry about the different
     * articles types.
     *
     * We won't handle any pagination here - that's up to the caller to cache and paginate.
     */
    public static List<SearchResult> findMatchingArticles(String searchString, List<String> searchedArticleRecordTypes,
                                                          Integer pageSize, Integer offset, Boolean withSnippet) {
        List<SearchResult> results = new List<SearchResult>();
        List<Knowledge__kav> faqResults;

        // Sanity check the searchString, no empty search or short search Strings
        if (String.isBlank(searchString) || searchString.length() < 2) {
            return results;
        }

        // Don't allow any single quotes in here it will break the query.
        searchString = String.escapeSingleQuotes(searchString);

        String searchQuery = 'FIND \'' + searchString + '\' IN ALL FIELDS RETURNING ' +
            ' Knowledge__kav(Id, Title, RecordType.Name, UrlName, FirstPublishedDate ' +
            ' WHERE PublishStatus = \'Online\' AND ' +
            ' Language = \'en_US\' AND ' +
            ' RecordType.DeveloperName IN :searchedArticleRecordTypes' +
            ' LIMIT ' + pageSize +
            ' OFFSET ' + offset;

        if (withSnippet) {
            searchQuery += ') WITH SNIPPET (target_length=' + searchTextLength + ')';
        } else {
            searchQuery += ')';
        }

        System.debug('Search query: ' + searchQuery);
        System.debug('Article record types: ' + searchedArticleRecordTypes);

        Search.SearchResults searchResults = Search.find(searchQuery);

        List<Search.SearchResult> articlelist = searchResults.get('Knowledge__kav');

        if (withSnippet) {
            // with Snippet across all types of article.
            for (Search.SearchResult searchResult : articleList) {
                Knowledge__kav article = (Knowledge__kav) searchResult.getSObject();
                String matchTitle = article.Title;
                String matchContent = searchResult.getSnippet();

                // When running a SOSL search in a UT it does *not* populate the snippet above, so matchContent is empty, so when running
                // in a UT hard code a result.
                if (Test.isRunningTest()) {
                    matchContent = 'blah ' + searchString + ' blah';
                }
                searchString = searchString.replaceAll('\\p{Punct}', '');
                matchTitle = matchTitle.replaceAll('(?i)(' + searchString + ')', '<mark>$1</mark>');
                results.add(new SearchResult(article.RecordType.Name, matchTitle, matchContent, article.UrlName, article.FirstPublishedDate));
            }
        } else {
            // No snippet
            // We need to requery the results to get the Content__c which we couldn't get with the
            // SOSL query. At the moment we only support FAQs without snippets.
            List<Id> faqIds = new List<Id>();
            faqIds = new List<Id>();
            for (Search.SearchResult searchResult : articleList) {
                Knowledge__kav article = (Knowledge__kav) searchResult.getSObject();
                faqIds.add(article.Id);
            }
            String query =  'SELECT ' + String.join(faqFields, ',') + ', RecordType.Name , ' +
                        ' (SELECT Id, ' +
                        '        ParentId, ' +
                        '        DataCategoryGroupName, ' +
                        '        DataCategoryName ' +
                        '  FROM DataCategorySelections) ' +
                        ' FROM Knowledge__kav ' +
                        ' WHERE PublishStatus = \'Online\' ' +
                        ' AND RecordType.DeveloperName = :AMS_FAQ_RECORD_TYPE_DEVELOPER_NAME' +
                        ' AND Language = \'en_US\' ' +
                        ' AND Id IN :faqIds';
            faqResults = Database.query(query);

            for (Knowledge__kav article : faqResults) {
                String matchTitle = article.Title;
                String matchContent = article.Summary;
                matchContent = markUpContents(matchContent, searchString);
                matchTitle = markUpContents(matchTitle, searchString);
                results.add(new SearchResult(article.RecordType.Name, matchTitle, matchContent, article.UrlName, article.FirstPublishedDate));
            }
        }

        return results;
    }

    /**
     * Mark up the contents with the search term matches, or any matches on words in the search terms, unless it is quoted.
     */
    private static String markUpContents(String content, String searchString) {
        String markedUpContent = content;

        // Start with the actual search string.
        markedUpContent = markedUpContent.replaceAll('(?i)( ' + searchString + ' )', ' <mark>$1</mark> ');

        if (!searchString.contains('"') && searchString.containsWhitespace()) {
            // isn't quoted, so lets split on whitespace and try marking on the words (this may well result in doubling up on marks -
            // but that's ok).
            // Note that we are putting spaces around the match, that's because otherwise we end up gobbling up the white space
            // between words in a multi-word search string. So we put some back - yes it will end up with some extra white space,
            // but that's better IMHO than eating it all up. e.g. "Hello World" will become " Hello World", whereas without the
            // extra space it would be "HelloWorld".
            List<String> searchComponents = searchString.split('\\s');
            if (searchComponents.size() > 1) {
                for (String searchComponent : searchComponents) {
                    markedUpContent = markedUpContent.replaceAll('(?i)( ' + searchComponent + ' )', '&nbsp; <mark>$1</mark> ');
                }
            }
        }

        return markedUpContent;
    }

    public static List<ArticleCategoryWrapper> getAdviceArticleCategories() {
        return getArticleCategories(ADVICE_DATA_CATEGORY);
    }

    public static List<ArticleCategoryWrapper> getNewsArticleCategories() {
        return getArticleCategories(NEWS_DATA_CATEGORY);
    }

    public static ArticleCategoryWrapper getArticleCategoryWrapperByCategoryName(List<ArticleCategoryWrapper> dataCategoryWrappers, String categoryName) {
        ArticleCategoryWrapper wrapperToReturn;

        System.debug('Looking for category ' + categoryName);
        for (ArticleCategoryWrapper dataCategoryWrapper : dataCategoryWrappers) {
            System.debug('Matching against category ' + dataCategoryWrapper.label);
            if (dataCategoryWrapper.label == categoryName) {
                wrapperToReturn = dataCategoryWrapper;
                wrapperToReturn.currentArticle = true;
                break;
            }
            for (ArticleCategoryWrapper subCategoryDataCategoryWrapper : dataCategoryWrapper.childDataCategoryWrappers) {
                System.debug('Matching against subcategory ' + subCategoryDataCategoryWrapper.label);
                if (subCategoryDataCategoryWrapper.label == categoryName) {
                    wrapperToReturn = subCategoryDataCategoryWrapper;
                    wrapperToReturn.currentArticle = true;
                    break;
                }
            }
        }

        return wrapperToReturn;
    }

    public static List<ArticleCategoryWrapper> getArticleCategories(String dataCategory) {
        List<ArticleCategoryWrapper> dataCategoryWrappers = new List<ArticleCategoryWrapper>();

        List<DescribeDataCategoryGroupResult> describeCategoryResult = Schema.describeDataCategoryGroups(new List<String>{'KnowledgeArticleVersion'});
        List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();

        //Looping throught the first describe result to create the list of pairs for the second describe call
        for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult){
            DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
            p.setSobject(singleResult.getSobject());
            p.setDataCategoryGroupName(singleResult.getName());
            pairs.add(p);
        }

        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult = new List<DescribeDataCategoryGroupStructureResult>();
        describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(pairs, false);

        // for each of the categories in AMS Advice article add to the List - this is the menu
        for(DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult){
            // check if its our article type
            if (singleResult.getName() == dataCategory){
                // getTopCategories returns 'All' so we get the child categories of this which are our top level categories
                List<DataCategory> amsTopCategories = singleResult.getTopCategories()[0].getChildCategories();

                for(DataCategory amsTopCategory : amsTopCategories){

                    List<ArticleCategoryWrapper> wrappedSubCategories = new List<ArticleCategoryWrapper>();
                    for(DataCategory amsSubCategory : amsTopCategory.getChildCategories()){
                        wrappedSubCategories.add(new ArticleCategoryWrapper(amsSubCategory.getName(), amsSubCategory.getLabel()));
                    }
                    ArticleCategoryWrapper topCategory = new ArticleCategoryWrapper(amsTopCategory.getName(), amsTopCategory.getLabel());
                    topCategory.childDataCategoryWrappers = wrappedSubCategories;
                    dataCategoryWrappers.add(topCategory);
                }
            }
        }
        return dataCategoryWrappers;
    }

    public static Boolean isDairyCodeArticle(Knowledge__kav newsArticle) {
        Boolean isDairyCodeArticle = false;

        for (Knowledge__DataCategorySelection dataCategory : newsArticle.DataCategorySelections) {
            if (dataCategory.DataCategoryName == 'Dairy_Code') {
                isDairyCodeArticle = true;
                break;
            }
        }

        return isDairyCodeArticle;
    }
}