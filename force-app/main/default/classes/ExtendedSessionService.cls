/**
 * ExtendedSessionService (TRINEO)
 *
 * Create ExtendedSession records that track a Users permission to access a site without having to
 * reauthenticate.
 *
 * This is achieved by recording an extended session key in a cookie on that Users browser, when they
 * hit the community login page the login page controller checks with us whether or not that User is
 * allowed to login without entering their credentials. If they are then we create an access token
 * using JWT and return that to the login controller to redirect them to the frontdoor to log them
 * in again.
 *
 * This classes functions should only be used in the context of a controller.
 */

public without sharing class ExtendedSessionService {
    private static final String EXTSESSIONKEYCOOKIE = 'extsession';
    private static final String EXTSESSIONKEYCOOKIEUID = 'extsessionUID';

    private static final Integer TEMP_COOKIE_TIME_SECS = 60;
    private static final String TEMP_COOKIE_SID = 'tempsid';
    private static final String TEMP_COOKIE_STARTURL = 'startUrl';

    //The logged in user
    private User currentUser {get; set;}

    //Custom setting which contains Extended Session settings
    private Community_Extended_Sessions__c communityExtSessionSettings;

    //Extended Session relevant cookies
    private Cookie extSessionCookie {get; set;}
    private Cookie extSessionCookieUID {get; set;}

    public ExtendedSessionService(String communityName) {
        System.debug('ENTERING IN CONSTRUCTOR FOR CLASS: ExtendedSessionService');
        System.debug('COMMUNITY NAME: ' + communityName);

        //Retrieve cookie values (if any exist)
        this.extSessionCookie = ApexPages.currentPage().getCookies().get(EXTSESSIONKEYCOOKIE);
        this.extSessionCookieUID = ApexPages.currentPage().getCookies().get(EXTSESSIONKEYCOOKIEUID);

        System.debug('cookie token: ' + this.extSessionCookie);
        System.debug('cookie UID: ' + this.extSessionCookieUID);

        this.communityExtSessionSettings = Community_Extended_Sessions__c.getInstance(communityName);

        //Get all information for the current user
        this.currentUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        System.debug('Current user id: ' + this.currentUser.Id);
    }

    public Boolean isValid() {
        if (this.extSessionCookie != NULL && this.extSessionCookieUID != NULL) {
            //Retrieve values from stored cookies
            Blob blobUserId = EncodingUtil.base64Decode(this.extSessionCookieUID.getValue());
            Id userId = blobUserId.toString();

            String extSessionToken = this.extSessionCookie.getValue();

            //Find User
            User u = [SELECT Id, LastPasswordChangeDate FROM User WHERE Id =: userId];

            if (u == NULL) {
                return false;
            }

            Map<Id, Extended_Session__c> mapExtendedSessionById = new Map<Id, Extended_Session__c>([
                SELECT
                    Id
                FROM
                    Extended_Session__c
                WHERE
                    User__c =: u.Id
                AND CreatedDate >=: u.LastPasswordChangeDate
                AND Token__c =: extSessionToken
                AND Expiry_Date__c >=: Date.today()
                AND Active__c = true
            ]);

            if (mapExtendedSessionById.keySet().size() > 0) {
                return true;
            } else {
                clear(u);
            }
        }

        return false;
    }

    public void create(User u) {
        try {
            String extSessionKey = ExtendedSessionService.generateKey(u.Id, this.communityExtSessionSettings.Certificate_Name__c);

            Integer extSessionDurationDays = (this.communityExtSessionSettings == null || this.communityExtSessionSettings.Session_Duration__c == null) ? 30 : Integer.valueOf(this.communityExtSessionSettings.Session_Duration__c);
            Integer extSessionDurationSeconds = Integer.valueOf(extSessionDurationDays * 24 * 60 * 60);

            Extended_Session__c es = new Extended_Session__c(
                User__c = u.Id,
                Token__c = extSessionKey,
                Active__c = true,
                Expiry_Date__c = Date.today().addDays(extSessionDurationDays)
            );

            insert es;

            //If we make it here everything is ok, therefore we are free to manage cookies:
            Cookie newExtSessionCookie = new Cookie(EXTSESSIONKEYCOOKIE, extSessionKey, null, extSessionDurationSeconds, true, 'None');
            Cookie newExtSessionUidCookie = new Cookie(EXTSESSIONKEYCOOKIEUID, EncodingUtil.base64Encode(Blob.valueOf(String.valueOf(u.Id))), null, extSessionDurationSeconds, true, 'None');

            ApexPages.currentPage().setCookies(
                new Cookie[] { newExtSessionCookie, newExtSessionUidCookie }
            );
        } catch (Exception e) {
            System.debug('Exception: ' + e.getMessage());
        }
    }

    public void logout() {
        try {
            Blob blobUserId = EncodingUtil.base64Decode(this.extSessionCookieUID.getValue());
            Id userId = blobUserId.toString();

            Map<Id, Extended_Session__c> mapExtendedSessionById = new Map<Id, Extended_Session__c>([SELECT Id, Active__c FROM Extended_Session__c WHERE User__c =: userId AND Token__c =: this.extSessionCookie.getValue()]);

            for (Extended_Session__c es : mapExtendedSessionById.values()) {
                es.Active__c = false;
                mapExtendedSessionById.put(es.Id, es);
            }

            update mapExtendedSessionById.values();

            ApexPages.currentPage().setCookies(
                new Cookie[] {
                    new Cookie(EXTSESSIONKEYCOOKIE, this.extSessionCookie.getValue(), null, null, true, 'None'),
                    new Cookie(EXTSESSIONKEYCOOKIEUID, this.extSessionCookieUID.getValue(), null, null, true, 'None')
                }
            );
        } catch (Exception e) {
            System.debug('Exception: ' + e.getMessage());
        }
    }

    public void clear(User u) {
        Map<Id, Extended_Session__c> mapExtendedSessionById = new Map<Id, Extended_Session__c>([SELECT Id FROM Extended_Session__c WHERE Active__c = true AND User__c =: u.Id AND CreatedDate <=: u.LastPasswordChangeDate]);

        if (mapExtendedSessionById.keySet().size() > 0) {
            for (Extended_Session__c es : mapExtendedSessionById.values()) {
                es.Active__c = false;
                mapExtendedSessionById.put(es.Id, es);
            }

            update mapExtendedSessionById.values();
        }
    }

    private static String generateKey(String userId, String certName) {
        Long now = DateTime.now().getTime();

        //Generate Plain Text Key
        String plainKey = 'ExposedKey' + now + 'I' + userId;

        //Obfuscation of the plain key and performing truncation
        Blob signature = Crypto.signWithCertificate('rsa-sha256', Blob.valueOf(plainKey), certName);
        String stringifySignature = EncodingUtil.base64Encode(signature);

        //The first 99 characters is unique enough for our purposes.
        return stringifySignature.left(99);
    }

    public String getAccessToken() {
        String accessToken = null;

        if (this.extSessionCookieUID != null && this.extSessionCookie != null) {
            try {
                String cookieSessionKey = this.extSessionCookie.getValue();
                Blob blobUserId = EncodingUtil.base64Decode(this.extSessionCookieUID.getValue());
                Id userId = blobUserId.toString();

                List<Extended_Session__c> sessions = [SELECT Id, User__r.Username FROM Extended_Session__c WHERE Token__c =: cookieSessionKey AND User__c =: userId AND Expiry_Date__c >= :Date.today() AND Active__c = true];

                if (sessions.size() > 0 && sessions[0].User__r.Username != null) {
                    accessToken = getAccessToken(sessions[0].User__r.Username, this.communityExtSessionSettings);
                }
            } catch (Exception e) {
                System.debug('Exception: ' + e.getMessage());
            }
        }

        return accessToken;
    }

    public static String getAccessToken(String username, String communityName) {
        Community_Extended_Sessions__c extendedSessionSettings = Community_Extended_Sessions__c.getInstance(communityName);
        return getAccessToken(username, extendedSessionSettings);
    }

    public static String getAccessToken(String username, Community_Extended_Sessions__c extendedSessionSettings) {
        Auth.JWT jwt = new Auth.JWT();
        jwt.setSub(username);
        jwt.setAud(extendedSessionSettings.Community_URL__c);
        jwt.setIss(extendedSessionSettings.Consumer_Key__c);

        //Setup JWS
        Auth.JWS jws = new Auth.JWS(jwt, extendedSessionSettings.Certificate_Name__c);
        String tokenEndpoint = extendedSessionSettings.Community_URL__c + '/services/oauth2/token';

        //Perform JWS token request
        Auth.JWTBearerTokenExchange bearer = new Auth.JWTBearerTokenExchange(tokenEndpoint, jws);
        String accessToken = bearer.getAccessToken();

        return accessToken;
    }

    public static List<Cookie> createTemporarySessionCookie(String accessToken, String startUrl) {
        List<Cookie> extendedSessionCookies = new List<Cookie>();
        extendedSessionCookies.add(new Cookie(TEMP_COOKIE_SID,accessToken,null,TEMP_COOKIE_TIME_SECS,true, 'None'));

        if (startUrl != null) {
            extendedSessionCookies.add(new Cookie(TEMP_COOKIE_STARTURL,startUrl,null,TEMP_COOKIE_TIME_SECS,true, 'None'));
        }
        return extendedSessionCookies;
    }
}