/**
 * TelephonySearchController.cls
 * Description: Takes Individual IDs and/or Phone Numbers as Parameters and returns
 *              returns a list of the individuals and their related farms.
 * @author: Sarah Hay (Trineo)
 * @date: February 2016
 *
 * Update - Damon Shaw (Trineo) - July 2017 - updated data model to account for new relationship types, ordering and simplify the display
 */
public with sharing class TelephonySearchController {

    public List<Contact> individuals { get; set; }
    public List<Contact> auPublicIndividuals { get; set; }
    public String phoneNumber { get; set; }
    public List<Indv> results { get; set; }
    public String tabText { get; set; }

    public TelephonySearchController () {

        String incomingNumber;
        String softPhoneService;
        if (ApexPages.currentpage().getparameters().containsKey('key') && ApexPages.currentpage().getparameters().get('key') != null) {
            incomingNumber = ApexPages.currentpage().getparameters().get('key');
            softPhoneService = 'Pure Cloud';
        }
        else if (ApexPages.currentpage().getparameters().containsKey('phone') && ApexPages.currentpage().getparameters().get('phone') != null) {
            incomingNumber = ApexPages.currentpage().getparameters().get('phone');
            softPhoneService = 'Avaya';
        }
        else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR, 'No Caller Id') );
        }

        String fuzzySearchNumber = '';

        if (softPhoneService == 'Pure Cloud') {
            try {
                // AU doesnt have such strict phone number formats, they may contain spaces at any point so we are putting % between every digit in the fuzzy search string
                // Remove the first 3 digits as these with be country and area codes which may not be stored in the same way on the record
                // A mobile number of "0412 345 678" will come through from PureCloud as "61412345678" and must be matched to numbers stored in Salesforce with or without the "61" prefix, or the "+61" prefix, or the "0" prefix.
                // A landline number of 96781972 from the state of NSW will come through from PureCloud as "61296781972 " and must be matched to numbers stored in Salesforce with or without the "612" prefix, or the "+612" prefix, or the "02" prefix.
                // Yes these may not be matching the full numbers in some circumstances, however it merely shows a list for the service centre agents to choose from which I think is preferable to having a super strict search a missing matches
                List<String> phoneDigits = incomingNumber.replaceAll('[^0-9]', '').substring(3).split('');
                fuzzySearchNumber = '%' + String.join(phoneDigits, '%') + '%';

                incomingNumber = incomingNumber.contains('+64') ? incomingNumber.split('\\+64')[1] : incomingNumber;
                incomingNumber = incomingNumber.contains('+61') ? incomingNumber.split('\\+61')[1] : incomingNumber;

                this.individuals = [SELECT  Name, Preferred_Name__c, FirstName, LastName, Id, Birthdate,
                                            Phone, MobilePhone, Fax, MailingStreet, MailingCity, Email, Type__c, RecordTypeId,
                                            (SELECT     Id, Role__c, RecordType.Name, Active__c, Access_Valid__c,
                                                        Farm__r.Name, Farm__r.Farm_Information_Status__c,
                                                        Farm__r.RecordType.Name, Farm__r.Id,
                                                        Party__r.Name, Party__r.Farm_Information_Status__c,
                                                        Party__r.RecordType.Name, Party__r.Id, PF_Role__c,
                                                        Organisation__r.Name, Organisation__r.Farm_Information_Status__c,
                                                        Organisation__r.RecordType.Name, Organisation__r.Id
                                            FROM Individual_Relationships__r
                                            WHERE Active__c = true
                                            ORDER BY Farm__r.Name, Party__r.Name, Organisation__r.Name DESC NULLS last LIMIT 1000
                                            )
                                    FROM Contact
                                    WHERE
                                        RecordTypeId = :GlobalUtility.individualAURecordTypeId AND
                                            (MobilePhone LIKE :fuzzySearchNumber OR
                                             Phone LIKE :fuzzySearchNumber)
                                    ORDER BY Name
                ];
            }
            catch (Exception e) {
                System.debug('*** incomingNumber = ' + incomingNumber + ', error = ' + e.getMessage() + ', ' + e.getStackTraceString() );
            }
        }
        else if (softPhoneService == 'Avaya' && ApexPages.currentPage().getParameters().get('id') != null) {
            String listContacts = ApexPages.currentpage().getparameters().get('id');
            String decodedList = EncodingUtil.urlDecode(listContacts,'UTF-8');

            List<String> contactIds = decodedList.split(',');

            this.individuals = [SELECT Name, Preferred_Name__c, FirstName, LastName, Id,
                                Birthdate, Phone, Fax, MobilePhone, MailingStreet,
                                MailingCity, Email, Type__c, RecordTypeId,
                                ( SELECT Id, Role__c, RecordType.Name, Active__c, Access_Valid__c,
                                   Farm__r.Name, Farm__r.Farm_Information_Status__c,
                                   Farm__r.RecordType.Name, Farm__r.Id,
                                   Party__r.Name, Party__r.Farm_Information_Status__c, Party__r.RecordType.Name,
                                   Party__r.Id, PF_Role__c,
                                   Organisation__r.Name, Organisation__r.Farm_Information_Status__c,
                                   Organisation__r.RecordType.Name, Organisation__r.Id
                                   FROM Individual_Relationships__r
                                   WHERE Access_Valid__c = true
                                   ORDER BY Farm__r.Name, Party__r.Name, Organisation__r.Name DESC NULLS last LIMIT 1000
                                   )
                                FROM Contact
                                WHERE Id in :contactIds
                                ORDER BY Name];
        }

        if (individuals != null && individuals.size() > 0) {

            this.results = new List<Indv>();
            this.phoneNumber = 'Multiple Matches for 0' + incomingNumber;
            this.tabText = 'Multiple Matches';

            //check for any rural professionals and put the Individuals into wrappers
            for (Contact c : individuals) {
                results.add(new Indv(c, false));
            }

        }
        else {
            this.phoneNumber = 'Unknown Match for 0' + incomingNumber;
            this.auPublicIndividuals = [SELECT Id, Name, AccountId FROM Contact WHERE FirstName = 'AU Public'];
            this.tabText = 'Unknown Match';
        }
    }

    public class Indv {

        public Contact record { get; set; }
        public Boolean isRP { get; set; }
        public Boolean isAustralian { get { return record.RecordTypeId == GlobalUtility.individualAURecordTypeId;}}
        public List<Relationship> relationships { get; set; }

        public Indv (Contact c, Boolean rp) {

            this.record = c;
            this.isRP = rp;
            this.relationships = new List<Relationship>();

            //order the relationships by record type, Derived, Farm, Party, Organisation
            List<Relationship> activeDerived = new List<Relationship>();
            List<Relationship> activeIndividualFarm = new List<Relationship>();
            List<Relationship> activeIndividualRestrictedFarm = new List<Relationship>();
            List<Relationship> activeIndividualParty = new List<Relationship>();
            List<Relationship> activeIndividualOrganisation = new List<Relationship>();

            //look at each relationship, split by record type, put each type into it's own list
            for (Individual_Relationship__c ir :c.Individual_Relationships__r) {

                if (ir.RecordType.Name == 'Derived Relationship') {
                    activeDerived.add(new Relationship(ir) );
                }
                else if (ir.RecordType.Name == 'Individual-Farm') {
                    activeIndividualFarm.add(new Relationship(ir) );
                }
                else if (ir.RecordType.Name == 'Individual-Restricted-Farm') {
                    activeIndividualRestrictedFarm.add(new Relationship(ir) );
                }
                else if (ir.RecordType.Name == 'Individual-Party') {
                    activeIndividualParty.add(new Relationship(ir) );
                }
                else if (ir.RecordType.Name == 'Individual-Organisation') {
                    activeIndividualOrganisation.add(new Relationship(ir) );
                }
            }

            //add each list into the main list in the correct order
            if (activeDerived.Size() > 0) {
                relationships.addAll(activeDerived);
            }
            if (activeIndividualFarm.Size() > 0) {
                relationships.addAll(activeIndividualFarm);
            }
            if (activeIndividualRestrictedFarm.Size() > 0) {
                relationships.addAll(activeIndividualRestrictedFarm);
            }
            if (activeIndividualParty.Size() > 0) {
                relationships.addAll(activeIndividualParty);
            }
            if (activeIndividualOrganisation.Size() > 0) {
                relationships.addAll(activeIndividualOrganisation);
            }
        }
    }

    public class Relationship {

        public String validationStatus { get; set; }
        public String entityName { get; set; }
        public String entityNumber { get; set; }
        public String entityType { get; set; }
        public String role { get; set; }
        public Id entityId { get; set; }
        public String recordType { get; set; }
        public String imageName { get; set; }
        public String irId { get; set; }
        public Individual_Relationship__c individualRelationship { get; set; }

        public Relationship (Individual_Relationship__c ir) {
            this.individualRelationship = ir;

            this.recordType = ir.RecordType.Name;
            this.irId = ir.Id;

            if (recordType == 'Derived Relationship') {
                this.validationStatus = ir.Farm__r.Farm_Information_Status__c;
                this.entityName = ir.Farm__r.Name;
                this.entityType = ir.Farm__r.RecordType.Name;
                this.entityId = ir.Farm__r.Id;
                this.role = ir.Role__c + ' / ' + ir.PF_Role__c;
                this.imageName = 'farmer.png';
            }
            else if (recordType == 'Individual-Farm') {
                this.validationStatus = ir.Farm__r.Farm_Information_Status__c;
                this.entityName = ir.Farm__r.Name;
                this.entityType = ir.Farm__r.RecordType.Name;
                this.entityId = ir.Farm__r.Id;
                this.role = ir.Role__c;
                this.imageName = 'farm.png';
            }
            else if (recordType == 'Individual-Restricted-Farm') {
                this.validationStatus = ir.Farm__r.Farm_Information_Status__c;
                this.entityName = ir.Farm__r.Name;
                this.entityType = ir.Farm__r.RecordType.Name + ' (Restricted)';
                this.entityId = ir.Farm__r.Id;
                this.role = ir.Role__c;
                this.imageName = 'farm.png';
            }
            else if (recordType == 'Individual-Party') {
                this.validationStatus = ir.Party__r.Farm_Information_Status__c;
                this.entityName = ir.Party__r.Name;
                this.entityType = ir.Party__r.RecordType.Name;
                this.entityId = ir.Party__r.Id;
                this.role = ir.Role__c;
                this.imageName = 'balloons.png';
            }
            else if (recordType == 'Individual-Organisation') {
                this.validationStatus = ir.Organisation__r.Farm_Information_Status__c;
                this.entityName = ir.Organisation__r.Name;
                this.entityType = ir.Organisation__r.RecordType.Name;
                this.entityId = ir.Organisation__r.Id;
                this.role = ir.Role__c;
                this.imageName = 'city.png';
            }
        }
    }

}