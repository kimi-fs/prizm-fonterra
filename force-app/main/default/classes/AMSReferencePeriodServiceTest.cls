/**
 * Description: Test class for AMSReferencePeriodService
 * @author: Amelia (Trineo)
 * @date: September 2017
 */
@isTest
private class AMSReferencePeriodServiceTest {

    @testSetup static void testSetup() {
        List<Reference_Period__c> refPeriods = TestClassDataUtil.createReferencePeriods(6, GlobalUtility.REFERENCE_PERIOD_MILKING_SEASON_AU, true);
    }

    @isTest static void testGetCurrentAndFutureReferencePeriods() {
        List<Reference_Period__c> returnedReferencePeriods = AMSReferencePeriodService.getCurrentAndFutureReferencePeriods(3);
        System.assertEquals(3, returnedReferencePeriods.size(), 'Wrong number returned');
    }

    @isTest static void testGetFutureReferencePeriods() {
        List<Reference_Period__c> returnedReferencePeriods = AMSReferencePeriodService.getFutureReferencePeriods(3);
        System.assertEquals(3, returnedReferencePeriods.size(), 'Wrong number returned');
    }

    @isTest static void testGetPastReferencePeriods() {
        List<Reference_Period__c> returnedReferencePeriods = AMSReferencePeriodService.getPastReferencePeriods();
        System.assertEquals(1, returnedReferencePeriods.size(), 'Wrong number returned');
    }
}