global class MarketingCloudUtil {

    private static String CLIENT_ID = 'cz6ha938xoq9yh9ob1tdxmok';
    private static String CLIENT_SECRET = 'WpAvxotHYUQKkHQOK5vCiiG5';

    private static String CLIENT_ID_AU = 'r35e57b8d8osv7u6te8ybopp';
    private static String CLIENT_SECRET_AU = 'dyy3LCJPUQARA5q3R40EAkO5';

    public static final String BATCHKEYSTART = 'ID:';
    public static final STring BATCHKEYEND = ':ID';
    public static final String MCSTATUSPENDING = 'PENDING';
    public static final String MCSTATUSSENT = 'SENT';
    public static final String MCSTATUSDELAYED = 'DELAYED';
    public static final String MCSTATUSFAILEDSEND = 'FAILEDSEND';
    public static final String MCSTATUSDELIVERED = 'DELIVERED';
    public static final String MCSTATUSFAILEDDELIVERY = 'FAILEDDELIVERY';
    public static final String ERRORSTART = 'ERROR:';
    public static final String ERROREND = ':ERROR';

    public static final String LINEBREAK = '\r\n';
    public static final String MESSAGESTART = 'MESSAGESTART:';
    public static final String MESSAGEEND = ':MESSAGEEND';
    public static final String TOKENSTART = 'TOKENSTART:';
    public static final String TOKENEND = ':TOKENEND';

    global class RequestTokenRequest {
        WebService String clientId;
        WebService String clientSecret;

        global RequestTokenRequest(String clientId, String clientSecret) {
            this.clientId = clientId;
            this.clientSecret = clientSecret;
        }

        public String toJSON() {
            return JSON.serialize(this);
        }
    }

    global class SendSMSRequest {
        WebService List<Subscriber> Subscribers;
        //WebService List<String> mobileNumbers;
        WebService String keyword;
        WebService Boolean subscribe = true;
        WebService Boolean resubscribe = true;
        //WebService String SendTime = DateTime.now().formatGmt('yyyy-MM-dd hh:mm');
        WebService Boolean ovrde;
        WebService String messageText;
        WebService BlackoutWindow blackoutWindow;

        global SendSMSRequest(List<Subscriber> subscribers, String keyword, Boolean ovrde, String messageText, BlackoutWindow blackoutWindow) {
            this.Subscribers = subscribers;
            //this.mobileNumbers = mobileNumbers;
            this.keyword = keyword;
            this.ovrde = ovrde;
            this.messageText = messageText;
            this.blackoutWindow = blackoutWindow;
        }

        public String toJSON() {
            String JSONPayload = JSON.serialize(this);
            Map<String, Object> JSONPayloadMap = (Map<String, Object>) JSON.deserializeUntyped(JSONPayload);
            JSONPayloadMap.put('override', ovrde);
            JSONPayloadMap.remove('ovrde');

            return JSON.serialize(JSONPayloadMap);
        }
    }

    global class BlackoutWindow {
        WebService String UtcOffset; //The UTC offset of the blackout window start and end times. UtcOffset is required in every REST call in order for the blackout window to be honored.
        WebService String WindowStart; //The start time of the blackout window, in the UTC offset specified. To see if the SendTime is within the blackout window, convert the WindowStart and WindowEnd times to UTC and compare them to the SendTime
        WebService String WindowEnd; //The end time of the blackout window, in the UTC offset specified. To see if the SendTime is within the blackout window, convert the WindowStart and WindowEnd times to UTC and compare them to the SendTime

        global BlackoutWindow(String utcOffset, String windowStart, String windowEnd) {
            this.UtcOffset = convertTimeZoneToUtcOffset(utcOffset);
            this.WindowStart = windowStart;
            this.WindowEnd = windowEnd;
        }

        private String convertTimeZoneToUtcOffset(String timeZoneId) {
            String utcOffset = null;

            if (!String.isBlank(timeZoneId)) {
                TimeZone timeZone = TimeZone.getTimeZone(timeZoneId);
                Integer millisecondOffset = timeZone.getOffset(Date.today());
                Integer roughHoursOffset = millisecondOffset / (60 * 60 * 10); //returns 1300 for 46800000 which is roughly in the format we want, xx50 for offsets with half an hour

                if (roughHoursOffset > 0) { //positive offset
                    utcOffset = '+';
                } else {
                    utcOffset = '-';
                }

                utcOffset += String.valueOf(roughHoursOffset / 100);
                utcOffset += getOffsetMinutes(roughHoursOffset);
            }

            return utcOffset;
        }

        //convert decimal hours to minutes
        private String getOffsetMinutes(Integer roughHoursOffset) {
            String offsetMinutes = '00';

            if (Math.mod(roughHoursOffset, 100) == 50) {
                offsetMinutes = '30';
            }

            return offsetMinutes;
        }

    }

    global class Subscriber {
        WebService String MobileNumber;
        WebService String SubscriberKey; //contact ID
    }

    global class MessageContactResponse {
        WebService List<String> errors;
        WebService String tokenId;
        WebService Integer responseCode;
    }

    //Representation of https://code.exacttarget.com/apis-sdks/rest-api/v1/push/getMessageContactDeliveries.html
    //including hidden undocumented fields
    //    {
    //    "completeDate": "2016-09-20T00:11:00.587",
    //    "count": 1,
    //    "createDate": "2016-09-20T00:10:56.813",
    //    "excludedCount": 0,
    //    "message": "Farm 37802 - Notice of Cycle Change - Effective from 21/09/2016 your collection cycle has changed to every day, day shift.",
    //    "status": "Finished",
    //    "tracking": [
    //        {
    //            "message": "SENT to Dialogue",
    //            "mobileNumber": "64276605508",
    //            "statusCode": "0"
    //        },
    //        {
    //            "message": "Transaction completed",
    //            "mobileNumber": "64276605508",
    //            "statusCode": "0"
    //        }
    //    ]
    //}
    global class MessageContactDeliveryStatus {
        WebService String message;
        WebService Integer count;
        WebService DateTime createDate;
        WebService DateTime completeDate;
        WebService Integer excludedCount;
        WebService String status;
        WebService List<MessageContactDeliveryStatusTracking> tracking;
    }

    global class MessageContactDeliveryStatusTracking {
        WebService String message;
        WebService String mobileNumber;
        WebService String statusCode;
    }

    public static String requestToken(Boolean australia) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        request.setHeader('Content-Type', 'application/json');
        request.setEndpoint('https://auth.exacttargetapis.com/v1/requestToken');
        request.setMethod('GET');
        request.setTimeout(120000);

        if (australia) {
            request.setBody(new RequestTokenRequest(CLIENT_ID_AU, CLIENT_SECRET_AU).toJSON());
        } else {
            request.setBody(new RequestTokenRequest(CLIENT_ID, CLIENT_SECRET).toJSON());
        }

        HttpResponse response = http.send(request);
        Map<String, Object> responseBodyJSON = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

        return String.valueOf(responseBodyJSON.get('accessToken'));
    }

    private static List<Subscriber> createSubscribersFromContactList(List<Contact> contacts) {
        List<Subscriber> subscribers = new List<Subscriber>();

        for (Contact c : contacts) {
            if (isRunningInSandbox() && !isSafeNumber(c.MobilePhone)) {
                continue; //if sandbox and not whitelisted, ignore
            }

            Subscriber sub = new Subscriber();
            sub.MobileNumber = normalizeNumber(c.MobilePhone);
            sub.SubscriberKey = c.Id;

            subscribers.add(sub);
        }

        return subscribers;
    }

    public static MessageContactResponse sendSMSRest(List<Contact> contacts, String keyword, Boolean ovrde, String messageText, String message) {
        return sendSMSRest(contacts, keyword, ovrde, messageText, message, false, null);
    }

    public static MessageContactResponse sendSMSRest(List<Contact> contacts, String keyword, Boolean ovrde, String messageText, String message, Boolean australia, BlackoutWindow blackoutWindow) {
        Communication_Settings__c communicationSettings = getCommunicationSettings();

        if (!communicationSettings.SMS_Sending_Enabled__c) {
            return new MessageContactResponse();
        }

        //ensure that blackoutwindow object is null/empty if any of the fields are blank
        if (blackoutWindow != null) {
            if (String.isBlank(blackoutWindow.UtcOffset) || String.isBlank(blackoutWindow.WindowStart) || String.isBlank(blackoutWindow.WindowEnd)) {
                blackoutWindow = null;
            }
        }

        List<Subscriber> subscribers = createSubscribersFromContactList(contacts);

        Http http = new Http();
        HttpRequest request = new HttpRequest();

        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + requestToken(australia));
        request.setEndpoint('https://www.exacttargetapis.com/sms/v1/messageContact/' + message + '/send');
        request.setMethod('POST');
        request.setTimeout(120000);

        request.setBody(new SendSMSRequest(subscribers, keyword, ovrde, messageText, blackoutWindow).toJSON());

        System.debug(request.getBody());

        String logBody = '';
        try { logBody += 'Sent: ' + request.getBody(); } catch (Exception e) {}

        HttpResponse response = http.send(request);

        MessageContactResponse mcResponse = (MessageContactResponse) JSON.deserialize(response.getBody(), MessageContactResponse.class);
        mcResponse.responseCode = response.getStatusCode();

        try { logBody += '\r\n' + 'Received: ' + response.getBody(); } catch (Exception e) {}
        Logger.log('MC Callout Log', logBody, Logger.LogType.INFO);

        return mcResponse;
    }

    public static MessageContactDeliveryStatus checkDeliverability(String message, String tokenId) {
        return checkDeliverability(message, tokenId, false);
    }

    public static MessageContactDeliveryStatus checkDeliverability(String message, String tokenId, Boolean australia) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + requestToken(australia));
        request.setEndpoint('https://www.exacttargetapis.com/sms/v1/messageContact/' + message + '/deliveries/' + tokenId);
        request.setMethod('GET');
        request.setTimeout(120000);

        HttpResponse response = http.send(request);
        MessageContactDeliveryStatus deliveryStatus = new MessageContactDeliveryStatus();

        System.debug(response.getBody());
        if (response.getStatusCode() == 200) {
            deliveryStatus = (MessageContactDeliveryStatus) JSON.deserialize(response.getBody(), MessageContactDeliveryStatus.class);
        }

        return deliveryStatus;
    }

    public static Boolean isSafeNumber(String mobileNumber) {
        Set<String> safeNumbers = new Set<String>(normalizeNumbers(getCommunicationSettings().SMS_Safe_Number_List__c.split(',')));

        if (safeNumbers.contains(normalizeNumber(mobileNumber))) {
            return true;
        }

        return false;
    }

    public static String normalizeNumber(String mobileNumber) {
        if (!String.isBlank(mobileNumber)) {
            return mobileNumber.replaceAll('\\+', '').replaceAll(' ', '');
        }

        return '';
    }

    @TestVisible
    private static List<String> normalizeNumbers(List<String> mobileNumbers) {
        List<String> numbersToReturn = new List<String>();

        for (String mobileNumber : mobileNumbers) {
            numbersToReturn.add(normalizeNumber(mobileNumber));
        }

        return numbersToReturn;
    }

    public static Boolean isRunningInSandbox() {
        return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }

    public static Communication_Settings__c getCommunicationSettings() {
        return Communication_Settings__c.getOrgDefaults();
    }

    public static Id getCaseRecordTypeByMessageTypeName(String messageTypeName) {
        Id recordTypeId = null;

        if (messageTypeName == 'Late Off Farm') {
            recordTypeId = [Select Id from RecordType Where SobjectType = 'Case' And Name = 'Planning & Dispatch'].Id;
        } else if (messageTypeName == 'Cycle Change') {
            recordTypeId = [Select Id from RecordType Where SobjectType = 'Case' And Name = 'Planning & Dispatch'].Id;
        } else if (messageTypeName == 'Free Text') {
            recordTypeId = [Select Id from RecordType Where SobjectType = 'Case' And Name = 'Communications AU'].Id; //FSCRM-2327 'Digital Services' -> 'Communications'
        } else {
            recordTypeId = [Select Id from RecordType Where SobjectType = 'Case' And Name = 'Communications AU'].Id;
        }

        return recordTypeId;
    }

    public static String getCaseTypeByMessageTypeName(String messageTypeName) {
        String type = '';

        if (messageTypeName == 'Late Off Farm') {
            type = 'Collections';
        } else if (messageTypeName == 'Cycle Change') {
            type = 'Collections';
        } else if (messageTypeName == 'Free Text') {
            type = 'Outbound Comms';
        }

        return type;
    }

    public static String getCaseSubTypeByMessageTypeName(String messageTypeName) {
        String subType = '';

        if (messageTypeName == 'Late Off Farm') {
            subType = 'Late Off Farm';
        } else if (messageTypeName == 'Cycle Change') {
            subType = 'Cycle Change';
        } else if (messageTypeName == 'Free Text') {
            subType = 'Operational';
        }

        return subType;
    }

    public static Case createCaseOnFailure(Id queueId, Id rtNameId, Id farmId, Id primaryOnFarmContactId, String description, String type, String subType) {
        Case c = new Case();

        // Set the fields on the Case to be inserted
        c.OwnerId = queueId;
        c.RecordTypeId = rtNameId;
        c.AccountId = farmId;
        c.ContactId = primaryOnFarmContactId;
        c.Description = description;
        c.Type = type;
        c.Sub_Type__c = subType;
        c.Status = 'In Progress';
        c.Sub_Status__c = 'Supplier Follow Up';
        c.Raised_By__c = 'Internal';
        c.Origin = 'Bulk';

        return c;
    }
}