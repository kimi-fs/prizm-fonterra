/*------------------------------------------------------------
*  Author:			Sean Soriano
*  Company:		Davanti Consulting
*  Description:	Test Class for BatchSetActiveAgreement Batch Class
*  History
*  18/12/2015		Sean Soriano	Created
*  ------------------------------------------------------------*/
@isTest
private class ScheduleBatchSetActiveAgreementTest {

    @testSetup static void createAgreements() {
        Field_Team_Tools_Settings__c individualDefaultAccount = TestClassDataUtil.individualDefaultAccountAU();
        Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();
        insert TestClassDataUtil.createUserByProfile('Field Team AU - Area Manager', 'Fonterra AU');
        TestClassDataUtil.createIntegrationUser();

        // Account/Entities
        List<Account> ent1 = TestClassDataUtil.createAUFarms(1, true);

        List<Agreement__c> lstTestAgreements = new List<Agreement__c>();

        // Agreements
        // Null END DATE
        Agreement__c agr1 = new  Agreement__c();
        agr1 = TestClassDataUtil.createAgreement('Standard Agreement', ent1[0].Id, System.today(), false);
        agr1.Agreement_Status__c = null;
        agr1.End_Date__c = system.today() + 2;

        // NOT YET STARTED
        Agreement__c agr2 = new  Agreement__c();
        agr2 = TestClassDataUtil.createAgreement('Standard Agreement', ent1[0].Id, System.today() + 1, false);
        agr2.Agreement_Status__c = null;
        agr2.End_Date__c = system.today() + 2;

        // OVERDUE
        Agreement__c agr3 = new  Agreement__c();
        agr3 = TestClassDataUtil.createAgreement('Standard Agreement', ent1[0].Id, System.today() - 2, false);
        agr3.Agreement_Status__c = null;
        agr3.End_Date__c = system.today() - 1;

        // ACTIVE
        Agreement__c agr4 = new  Agreement__c();
        agr4 = TestClassDataUtil.createAgreement('Standard Agreement', ent1[0].Id, System.today() - 1, false);
        agr4.Agreement_Status__c = null;
        agr4.End_Date__c = system.today() + 2;

        // OVERDUE Contract Supply
        Agreement__c agrContractSupply = new  Agreement__c();
        agrContractSupply = TestClassDataUtil.createAgreement('Contract Supply AU', ent1[0].Id, System.today() - 2, false);
        agrContractSupply.Agreement_Status__c = null;
        agrContractSupply.End_Date__c = system.today() - 1;

        lstTestAgreements.add(agr1); // Active
        lstTestAgreements.add(agr2); // Inactive
        lstTestAgreements.add(agr3); // Inactive
        lstTestAgreements.add(agr4); // Active
        lstTestAgreements.add(agrContractSupply); // Inactive

        insert lstTestAgreements;
    }

    @isTest static void test_ScheduleBatchSetActiveAgreement() {
        String CRON_EXP = '0 0 0 3 9 ? 2022';

        Test.startTest();
        String jobId = System.schedule(
            'TestScheduleBatchSetActiveAgreement',
            CRON_EXP,
            new ScheduleBatchSetActiveAgreement()
        );
        Test.stopTest();
    }

    @isTest static void test_BatchSetActiveAgreement() {
        Test.startTest();
        ScheduleBatchSetActiveAgreement batch = new ScheduleBatchSetActiveAgreement();
        Database.executeBatch(batch);
        Test.stopTest();

        List<AggregateResult> allActiveAgreements = [SELECT count(Id) active FROM Agreement__c WHERE Active__c = true];
        List<AggregateResult> allInactiveAgreements = [SELECT count(Id) inactive FROM Agreement__c WHERE Active__c = false];
        List<AggregateResult> allAgreements = [SELECT count(Id) total FROM Agreement__c];

        System.assertEquals(5, (Integer) allAgreements[0].get('total'), 'Incorrect total of agreements');
        System.assertEquals(2, (Integer) allActiveAgreements[0].get('active'), 'Incorrect count of active agreements');
        System.assertEquals(3, (Integer) allInactiveAgreements[0].get('inactive'), 'Incorrect count of inactive agreements');
    }

    @isTest static void test_BatchSetActiveAgreementAU() {
        List<Account> farmAU = TestClassDataUtil.createAUFarms(2, true);
        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        User userAUProfile = [SELECT Id, isActive FROM User WHERE Profile.Name = 'Field Team AU - Area Manager' AND isActive = true LIMIT 1];
        User userIntegrationProfile = [SELECT Id, isActive FROM User WHERE Profile.Name = 'Integration User' AND isActive = true LIMIT 1];

        List<Account> accountToUpdate = new List<Account>();
        accountToUpdate.add(new Account(Id = farmAU[0].Id, OwnerId = userAUProfile.Id));
        accountToUpdate.add(new Account(Id = farmAU[1].Id, OwnerId = userAUProfile.Id));
        accountToUpdate.add(new Account(Id = party.Id, OwnerId = userAUProfile.Id));
        update accountToUpdate;
        Agreement__c agreemAU2;
        System.runAs(userIntegrationProfile) {
            // Agreement without Agreement Status , usually for Non Standard
            agreemAU2 = TestClassDataUtil.createAgreement('Standard Agreement', farmAU[1].Id, System.today() + 1, false);
            agreemAU2.Party__c = party.Id;
            agreemAU2.Agreement_Status__c = null;
            insert agreemAU2;
        }

        // run as AU profile to not fire the WF that activates the agreement
        System.runAs(userAUProfile) {
            Agreement__c agreemAU = TestClassDataUtil.createAgreement('Standard Agreement', farmAU[0].Id, System.today(), false);
            agreemAU.Party__c = party.Id;
            agreemAU.Agreement_Status__c = 'Pending';
            insert agreemAU; // Inactive

            agreemAU2.Start_Date__c = System.today();
            update agreemAU2; // Active
        }

        Test.startTest();
        ScheduleBatchSetActiveAgreement batch = new ScheduleBatchSetActiveAgreement();
        Database.executeBatch(batch);
        Test.stopTest();

        List<AggregateResult> allActiveAgreements = [SELECT count(Id) active FROM Agreement__c WHERE Active__c = true];
        List<AggregateResult> allInactiveAgreements = [SELECT count(Id) inactive FROM Agreement__c WHERE Active__c = false];
        List<AggregateResult> allAgreements = [SELECT count(Id) total FROM Agreement__c];

        System.assertEquals(7, (Integer) allAgreements[0].get('total'), 'Incorrect total of agreements');
        System.assertEquals(3, (Integer) allActiveAgreements[0].get('active'), 'Incorrect count of active agreements');
        System.assertEquals(4, (Integer) allInactiveAgreements[0].get('inactive'), 'Incorrect count of inactive agreements');
	}

}