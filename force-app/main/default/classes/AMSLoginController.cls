/**
 * Description: Controller for login to AMS Community
 * @author: Amelia (Trineo)
 * @date: July 2017
 * @test: AMSLoginController_Test
 */
public with sharing class AMSLoginController {

    public static final String COMMUNITY_NAME = 'Australian Milk Supply';

    public String message {get; set;}
    public Map<String, String> messages = ExceptionService.getPageMessages('AMSLogin');

    public String email {
        get;
        set {
            this.email = value.trim();
        }
    }
    public String password {get; set;}

    private PageReference loggedInRedirectPage;

    public AMSLoginController() {
        message = '';
    }

    /**
    * Logs the user in and creates the cookies that will be used to query the user for the extended session when they return
    */
    public PageReference login() {
        loggedInRedirectPage = null;
        message = '';

        try {
            //check there is a user that matches the email provided and that it's not locked out
            User user = AMSUserService.getUserFromEmailAddress(this.email);

            if (user == null) {
                // Do not signal to the user that they have entered an incorrect username, same message every time so that
                // we are preventing username enumeration.
                message = messages.get('failed-login');
                return null;
            } else {
                if (AMSUserService.getUserPasswordLocked(user.Id)) {
                    // Do not signal to the user that their account is locked, same message every time so that
                    // we are preventing username enumeration.
                    message = messages.get('failed-login');
                    return null;
                }
            }

            loggedInRedirectPage = Site.login(user.Username, password, getRedirectPageURL(user.Id));

            if (loggedInRedirectPage == null) {
                password = null;
                message = messages.get('failed-login');
                return null;
            } else {
                ExtendedSessionService ess = new ExtendedSessionService(COMMUNITY_NAME);
                ess.create(user);
            }
        } catch (exception e) {
            message = e.getMessage();
        }

        return loggedInRedirectPage;
    }

    /**
    * Gets the access token to log the user in (if they have valid extended session)
    */
    public PageReference loginIfExtendedSessionExistsFromCookie() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        ExtendedSessionService extendedSession = new ExtendedSessionService(COMMUNITY_NAME);

        PageReference returnPage = null;
        if (extendedSession.isValid()) {
            String accessToken = extendedSession.getAccessToken();
            if (accessToken != null) {
                List<Cookie> extendedSessionCookies = ExtendedSessionService.createTemporarySessionCookie(accessToken, startUrl);

                ApexPages.currentPage().setCookies(extendedSessionCookies);
            }
        }

        return returnPage;
    }

    /**
    * Gets the URL to redirect the user to
    * Priority order:
    * - landingPage (Defined by AMS_Landing_Page__c on Contact)
    * - startURL (from the page startURL param)
    * - previousPageCookie (from the browser cookie created in AMSCommunityTemplateController - used in the web app to return to the page when the app was closed)
    * - Default - AMSDashboard
    */
    private static String getRedirectPageURL(Id userId) {
        String previousPageCookieValue = null;

        try {
            Cookie previousPageCookie = ApexPages.currentPage().getCookies().get(AMSCommunityTemplateController.PREVIOUS_PAGE_PARAMETER);
            previousPageCookieValue = previousPageCookie.getValue();

        } catch (Exception e) {
            previousPageCookieValue = null;
        }

        System.debug('previousPageCookieValue: ' + previousPageCookieValue);

        String landingPage = AMSUserService.getAMSLandingPage(userId);
        String startUrl = '';

        if (String.isNotBlank(landingPage)) {
            startUrl = Site.getBaseUrl() + '/' + landingPage;
        } else if (String.isNotBlank(ApexPages.currentPage().getParameters().get(AMSCommunityUtil.getStartURLParameterName()))) {
            startUrl = ApexPages.currentPage().getParameters().get(AMSCommunityUtil.getStartURLParameterName());
        } else if (String.isNotBlank(previousPageCookieValue)) {
            // Use Site.getBaseURL() as logging in with the access token requires the /AMS for the community
            startUrl = Site.getBaseUrl() + previousPageCookieValue;
        } else {
            // Use Site.getBaseURL() as logging in with the access token requires the /AMS for the community
            startUrl = Site.getBaseUrl() + AMSCommunityUtil.pageReferenceToCommunityFriendlyString(Page.AMSDashboard);
        }
        System.debug('startUrl: ' + startUrl);

        return startUrl;
    }

    @testVisible
    private Boolean isSandbox {
        get {
            if (this.isSandbox == null) {
                this.isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
            }

            return this.isSandbox;
        }
        set;
    }

    /**
     * Returns the url prefix for a community (if it exists) in a sandbox- returns empty for production regardless of url prefix
     */
    public String getSiteUrlPrefix() {
        String networkId = Network.getNetworkId();
        String siteUrlPrefix = '';

        if (networkId != null && isSandbox) {
            Network comm = [SELECT UrlPathPrefix FROM Network WHERE Id = :networkId];
            siteUrlPrefix = '/' + comm.UrlPathPrefix;
        }

        return siteUrlPrefix;
    }

}