/**
 * A one off class for use in Unit Tests and new Orgs to seed the AMS Web Text Articles used on the AMS
 * Community site.
 *
 * It is possible that they already exist, and may have been modified, so only add them if they don't already
 * exist.
 *
 * @author Ed Groenendaal (Trineo)
 * @date Aug 2017
 */
public without sharing class SetupAMSWebText {
    private class WebTextWrapper {
        public String title;
        public String heading;
        public String content;
        public WebTextWrapper(String title, String content) {
            this.title = title;
            this.content = content;
        }

        public WebTextWrapper(String title, String heading, String content) {
            this.title = title;
            this.heading = heading;
            this.content = content;
        }
    }

    private static List<WebTextWrapper> webTexts = new List<WebTextWrapper> {
        new WebTextWrapper(
            'AMSAdvice: No Search Results',
            '<p>Sorry, we couldn&#39;t find any results&nbsp;<br />' +
            'Please have a look at the tips below and try a different search<br />' +
            'Tips:</p>' +
            '<ul>' +
            '<li>Check your spelling</li>' +
            '<li>Try a different word with the same meaning</li>' +
            '<li>Try less specific words</li>' +
            '</ul>'
        ),

        new WebTextWrapper(
            'AMSSignAgreement: Pricing Options',
            'Fonterra&#39;s 7/5 pricing construct is one our supply patners will be most familiar with. Its structure would typically suit a split calving system with calving in the Spring and Autumn.' + 
            '<br/><br/>Our new 8/4 pricing system reduces variability across months and has been introduced to acknowledge a more traditional single calving pasture based system.' +
            '<br/><br/>While these pricing systems have been designed with calving systems in mind, with variability across all supply curves, it&#39;s important you understand which pricing system best suits your particular milk curve.'
        ),

        new WebTextWrapper(
            'AMSBecomingASupplier',
            'TBD AMSBecomingASupplier: blurb'
        ),

        new WebTextWrapper(
            'AMSContact: noContactsBlurb',
            'TBD AMSContact: noContactsBlurb'
        ),

        new WebTextWrapper(
            'AMSContact: Area Manager',
            'Area Managers have a good understanding of industry and economic issues affecting the primary sector and dairying. A key part of their role is keeping you up-to-date with what’s going on with Fonterra’s strategy, the global dairy industry, and supporting you to grow your dairy business.'
        ),

        new WebTextWrapper(
            'AMSContact: Inbound Service Specialist',
            'Your Service Specialist is your first point of contact 24/7 and will cover topics such as transport queries, vat break downs & suspect antibiotics, statements and milk pay queries, supply related questions and any other general queries you may have. To speak with the Service Centre team, please call 1800 266 674.'
        ),

        new WebTextWrapper(
            'AMSContact: Quality Specialist',
            'Area Managers have a good understanding of industry and economic issues affecting the primary sector and dairying. A key part of their role is keeping you up-to-date with what’s going on with Fonterra’s strategy, the global dairy industry, and supporting you to grow your dairy business.'
        ),

        new WebTextWrapper(
            'AMSContact: Transport Specialist',
            'Area Managers have a good understanding of industry and economic issues affecting the primary sector and dairying. A key part of their role is keeping you up-to-date with what’s going on with Fonterra’s strategy, the global dairy industry, and supporting you to grow your dairy business.'
        ),

        new WebTextWrapper(
            'AMSContact: Fonterra Australia Suppliers Council FASC',
            '<h3 class="c-contact-person__title">Fonterra Australia Suppliers Council (FASC)</h3>' +
            '<br />' +
            'The Fonterra Australian Suppliers Council (FASC) comprises a group of Fonterra suppliers from each supply region, whose aim is to provide two-way consultation between Fonterra Australia and its milk suppliers (via the Forum itself and the FASC Board).<br />' +
            '<br />' +
            'The Forum provides a structure that enables grass roots level input and feedback to both FASC and Fonterra, and also for Fonterra to provide inputs and feedback to suppliers &ndash; thus improving the two way feedback process.<br />' +
            '<br />' +
            'Being part of the Forum offers suppliers the ability to be involved, and to foster the ever increasing level of understanding between all farmer suppliers and Fonterra.<br />' +
            '<br />' +
            'For any questions on FASC, please call Will Kermode (FASC Company Secretary) on 03 8541 1824.' +
            '<p><a href="https://supplierscouncil.com.au/" target="_blank">Click here</a>&nbsp;to be directed to the FASC website</p>'
        ),

        new WebTextWrapper(
            'AMSContact: Suppliers Council',
            '<h3 class="c-contact-person__title">Suppliers&rsquo; Council</h3> ' +
            '<p><b>Farmers Representing Farmers</b></p>' +
            '<ul>' +
            '    <li>Fonterra Australian Suppliers Council (FASC) represents the interests of all suppliers in Victoria and Tasmania with a collective independent voice.</li>' +
            '    <li>Acting on co-operative principles FASC have partnered with Fonterra to develop a &ldquo;modern day co-op&rdquo; to deliver significant benefits and opportunities to suppliers.</li>' +
            '</ul>' +
            '<p><br />' +
            '<b>Benefits to suppliers:</b></p>' +
            '<ul>' +
            '    <li>Farmers working collectively for farmers</li>' +
            '    <li>Supply arrangements that are in the best interests of all dairy farmers supplying Fonterra Australia</li>' +
            '    <li>Strong and active representation of supplier&rsquo;s interests at industry and government level</li>' +
            '</ul>' +
            '<p><br />' +
            '<b>What FASC do:</b></p>' +
            '<ul>' +
            '    <li>Negotiate with Fonterra Australia, on behalf of our suppliers, on conditions of milk supply</li>' +
            '    <li>Oversee the operation of the Fonterra Australia Supplier Forum which provides direct farmer feedback to FASC Board and Fonterra Australia</li>' +
            '    <li>Represent supplier&rsquo;s interests with Fonterra Australia and work with Fonterra to develop initiatives to support farmers</li>' +
            '    <li>Actively participate in supplier meetings, field days, industry conferences, industry forums etc.</li>' +
            '    <li>Govern the investments owned by FASC utilising the profits to deliver value to our suppliers</li>' +
            '</ul>' +
            '<p><br />' +
            '<b>FASC relationship with Fonterra Australia</b><br />' +
            'FASC have a formal agreement with Fonterra. This agreement formalises FASC&#39;s relationship and interactions with Fonterra.<br />' +
            'The agreement includes the rights of all FASC suppliers to have their milk collected and the negotiation and contribution to the development of the milk pricing structure and associated initiatives.<br />' +
            '<br />' +
            '<a href="https://supplierscouncil.com.au/about-fasc/fasc-councillors" target="_blank">Click here</a>&nbsp;to see the FASC Councillors&nbsp;<br />' +
            '<a href="https://supplierscouncil.com.au/about-fasc/fasc-directors" target="_blank">Click here</a>&nbsp;to see the FASC Directors&nbsp;</p>'
        ),

        new WebTextWrapper(
            'AMSContractsAndPricing: Contracts and Pricing',
            'Contracts and Pricing',
            'Filler man, filler man'
        ),

        new WebTextWrapper(
            'AMSDashboard: Mobile App Header',
            'Access Farm Source Digital on your mobile device'
        ),

        new WebTextWrapper(
            'AMSDashboard: Mobile App Body',
            '<p>Download the Google Play Farm Source AU app or the Apple iOs Web App by clicking on these buttons.</p>'
        ),

        new WebTextWrapper(
            'AMSEnquiry: topBlurb',
            'Use this form to make an enquiry or to give us feedback. Our Supplier Administration Centre will receive your request and respond within 2 to 3 days.'
        ),

        new WebTextWrapper(
            'AMSEnquiry: submitted',
            'Thank you. Your enquiry has been successfully sent.'
        ),

        new WebTextWrapper(
            'AMSError: Session Timeout',
            'Your session has timed out.'
        ),

        new WebTextWrapper(
            'AMSFarmgateMilkPrice: Farmgate Milk Price',
            'Farmgate Milk Price',
            'The Farmgate Milk Price is the base price that Fonterra pays for milk supplied across Australia. <br /><br />' +
            '<h3>How the Milk Price is Calculated?</h3>' +
            'The Farmgate Milk Price under the Milk Price Manual is based on manufacturing milk powder and related streams such as Butter and Anhydrous Milk Fat (AMF) &ndash; also called Reference Commodity Products.<br /><br />' +
            'The choice of Reference Commodity Products is deliberate. it is based on the fact that powders account for more than half the global trade in dairy products, most new investment in New Zealand has been in milk powder related assets and in the medium term the highest returns are expected to come from powders.<br /><br />' +
            'The calculation is also based on the production costs for a theoretical efficient manufacturer of Fonterra&rsquo;s size and scale. this theoretical manufacturer model effectively sets up a highly competitive benchmark against which Fonterra measures its actual performance.<br /><br />' +
            'The strength in the Milk Price Manual is that it enables this high level transparency.'
        ),

        new WebTextWrapper(
            'AMSForms: Supplier Forms',
            'Supplier Forms',
            'Below you will find a number of most used supplier forms. &nbsp;If you can&#39;t find the form you&#39;re looking for, please call the Supplier Administration Centre at 1800 266 674.<br /><br />To complete the form you need, print it off, obtain the required signatures and send it back to us by mail/fax.'
        ),

        new WebTextWrapper(
            'AMSFinancial: Results',
            '2017 Annual Results',
            '<div><b>2017 Annual Results</b><br />&nbsp;</div><div>For full details about the FY16/17 Annual Results of the Fonterra Co-Operative, please check out the information and full annual report below.<br />&nbsp;</div><div><b>Reports</b><br />&nbsp;</div><div>Fonterra Co-Operative Group Limited Annual Review 2017<br /><a href="https://view.publitas.com/fonterra/fonterra-annual-review-2017/page/1" target="_blank">https://view.publitas.com/fonterra/fonterra-annual-review-2017/page/1</a><br />&nbsp;</div><div>Fonterra Co-Operative Group Limited Results presentation 2017<br /><a href="https://www.fonterra.com/content/dam/fonterra-public-website/pdf/Fonterra_Annual_Results_presentation_2017_NZX.pdf" target="_blank">https://www.fonterra.com/content/dam/fonterra-public-website/pdf/Fonterra_Annual_Results_presentation_2017_NZX.pdf</a><br />&nbsp;</div><div><b>Media Releases</b><br />&nbsp;</div><div>Media Release - 25 September 2017 - Fonterra Accelerates growth in Australia</div><div><a href="https://www.fonterra.com/au/en/news-and-media/announcements/fonterra-accelerates-growth-in-australia-on-the-back-of-strong-demand.html" target="_blank">https://www.fonterra.com/au/en/news-and-media/announcements/fonterra-accelerates-growth-in-australia-on-the-back-of-strong-demand.html</a></div>'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: Tooltip - Company Milk Price',
            'The company average milk price per KgMS.'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: Tooltip - Use next season milk prices',
            'TBD'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: Tooltip - Your Milk Price',
            'Your farms estimated average milk price per KgMS.'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: Tooltip - Reset Button',
            'Reset will take you back to the original view, any changes will be lost.'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: Tooltip - Total Fresh Solids',
            'These are your Minimum Kgs.'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: Tooltip - Production Payment',
            'Production Payment is using combined farm volume to determine payment bands where applicable.'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: Tooltip - Less Charges',
            'Charges include quality penalties and 2nd stop charges but excludes levies.'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: Tooltip - Production season',
            'Populates the scenario with litres, fat and protein based on your production for the selected season.'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: Negative KgMS Error',
            'This change will cause the Total KgMS to be less than the Total Minimum Solids.'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: No Farms',
            'You do not have access to view Income data for any farms.'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: Exception Error',
            'Something went wrong, please try again.'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: Disclaimer',
            'This Farm Income Estimation is based on publicly available information about current or nominated milk  prices at the time of printing and information you have provided to Fonterra.  It is an estimate based on a number of variable inputs, many of which are subject to seasonal variation and farm management practices that differ from farm to farm and could significantly affect the income estimation.  <strong>You should not rely on the Farm income Estimation.</strong> This income estimation does not contain any representation, comment or commitment by the Fonterra Group of companies on the demand for, the prices paid or payable for milk by Fonterra or any other milk processor.  The Fonterra Group of companies does not warrant the accuracy of this income estimation and has no responsibility for any error or inaccuracy that may be contained in it. The Fonterra Group of companies is not providing any financial or other advice in connection with this income estimation.  It is your responsibility to check the assumptions and calculations in this income estimation and Fonterra recommends that you seek independent advice from your accountant or business advisor. Past performance is not indicative of future performance.'
        ),

        new WebTextWrapper(
            'AMSIncomeEstimator: Non Standard Agreement Error',
            'Please contact your Area Manager for assistance with Income Estimates.'
        ),

        new WebTextWrapper(
            'AMSMilkCollection: Top Blurb',
            'Need to update your milking frequency window? Please maintain the information below to keep your details up to date. For other milk collection queries, please call the Milk Collection Contact Number(s) listed below.'
        ),

        new WebTextWrapper(
            'AMSMilkCollection: No Farms',
            'You have no data to view for any farm.'
        ),

        new WebTextWrapper(
            'AMSMyDetails: Phone Number Format',
            'Please enter your mobile phone number in one of the two following formats: 0439 125 109 or +61 439 125 109.'
        ),

        new WebTextWrapper(
            'AMSNewUser: Password Requirements',
            'Your password must have:' +
            '<ul>' +
            '<li>At least one capital letter</li>' +
            '<li>At least one lowercase letter</li>' +
            '<li>At least one number</li>' +
            '<li>A minimum of 8 characters</li>' +
            '<li>No spaces</li>' +
            '</ul>'
        ),

        new WebTextWrapper(
            'AMSPasswordReset: Top blurb',
            'Please avoid using personal details, simple words, or any other words or numbers that could be easily guessed.'
        ),

        new WebTextWrapper(
            'AMSPreferences: saved',
            'Thanks. We\'ve saved your new preferences'
        ),

        new WebTextWrapper(
            'AMSSearch: No Search Results',
            '<p>Sorry, we couldn&#39;t find any results&nbsp;<br />' +
            'Please have a look at the tips below and try a different search<br />' +
            'Tips:</p>' +
            '<ul>' +
            '<li>Check your spelling</li>' +
            '<li>Try a different word with the same meaning</li>' +
            '<li>Try less specific words</li>' +
            '</ul>'
        ),

        new WebTextWrapper(
            'AMSThirdParty: viewTopBlurb',
            'The following people have access to your farm information on Farm Source.'
        ),

        new WebTextWrapper(
            'AMSThirdParty: whoExisting',
            'If the person you want to add already has a Farm Source login, please enter their Farm Source username to give them access. Please contact them to find out their username.'
        ),

        new WebTextWrapper(
            'AMSThirdParty: whoNewUser',
            'If the person you want to add does not have a Farm Source login, ask them to register online themselves and let you know their username.'
        ),

        new WebTextWrapper(
            'AMSStatments: No Statement Results',
            'There is currently no data for this report.'
        ),

        new WebTextWrapper(
            'AMSTerms: Terms of Service',
            '<p><b>Farm Source Website - Terms and Conditions</b></p>' +
            '<p><b>Acceptance</b></p>' +
            '<p>The Farm Source portal (the &quot;<b>Site</b>&quot;) is provided by Fonterra Australia Pty. Ltd. (&ldquo;<b>Fonterra</b>&rdquo;).</p>' +
            '<p>Your access to and use of the Site is subject to the following terms and conditions (&quot;Terms&quot;). By using the Site you represent that you have read, understood and accepted these Terms and agree to be bound by them.</p>' +
            '<p>If you wish to become a registered user of the Site (&quot;Member&quot;), you will be asked to accept these Terms and your use of the Site will be deemed to be acceptance by you of these Terms in their entirety.</p>' +
            '<p>If you do not agree to these Terms, and to be bound by any changes to them in future, please do not use the Site or register as a Member.</p>' +
            '<p><b>Changes to these Terms</b></p>' +
            '<p>Fonterra reserves the right, at its discretion, to update or revise these Terms. Please check these Terms periodically for changes. Your continued use of the Site following the posting of any changes to the Terms indicates your acceptance of those changes.</p>' +
            '<p><b>Australian Site</b></p>' +
            '<p>This Site and the information on it, is intended for use by Australian residents only, although residents of other countries are welcome to visit the Site. Those who choose to access this Site from locations outside Australia do so on their own initiative and are responsible for compliance with local laws. Australian law governs the Site and all agreements entered into through the Site.</p>' +
            '<p>By using this Site, you irrevocably and unconditionally submit to the exclusive jurisdiction of the Courts of Australia to hear and determine any disputes or proceedings arising out of or in connection with this Site or any agreement entered into through the Site.</p>' +
            '<p>You also waive any objection that you might have on the grounds of forum non convenient to Australia as the forum for proceedings arising out of or in connection with this Site or any agreements entered into through this Site.</p>' +
            '<p><b>Independent Advice</b></p>' +
            '<p>We strongly recommend that you seek independent advice before acting on any information on this Site. This Site is not designed for the purpose of providing personal financial or investment advice.</p>' +
            '<p><b>Limitation of liability</b></p>' +
            '<p>To the fullest extent permissible by law, Fonterra shall not be liable for any loss, damage, cost or expense (whether direct or indirect) incurred by you as a result of the use of any information on this Site or any error, omission or misrepresentation in any information on this Site, or our failure to provide this Site.</p>' +
            '<p><b>Intellectual property rights</b></p>' +
            '<p>You acknowledge and agree that all content and materials available on the Site are protected by copyright, trademarks or other intellectual property rights and laws. No content or material on the Site may be reproduced, adapted, linked to, distributed or used in any way without the prior written consent of Fonterra. However, you may print or download one copy of the materials or content on this Site on any single computer for your personal use, provided you keep intact all copyright and other proprietary notices.</p>' +
            '<p><b>Links</b></p>' +
            '<p>This Site may contain links to other sites which are not under the control of Fonterra. These links are provided for your convenience only. Fonterra makes no representations concerning the accuracy of information or the quality of products and services found on any linked site and will not be responsible for the content of any of those sites or for any loss or damages suffered as a result of you using any linked sites.</p>' +
            '<p><b>Disclaimer concerning information and materials</b></p>' +
            '<p>The Site may from time to time contain information, advice or data, including calculation or other routines for use of Members. Such information, advice or data is not intended to be a substitute for your own judgement or any advice provided by your own consultants or experts. All information, advice or data provided on this Site is supplied upon the condition that you will seek independent advice, where appropriate, and make your own determination as to its suitability for your purposes prior to use. In particular, we recommend you seek the advice of a qualified veterinarian for detailed information on animal health issues. Nothing on this Site is to be construed as a recommendation to use any product.</p>' +
            '<p>All information, advice or data provided on this Site is supplied in good faith and, although Fonterra has endeavoured to ensure that the information, advice and data on the Site is free from error, Fonterra does not warrant its accuracy, adequacy or completeness, or that it is suitable for your intended use.</p>' +
            '<p>In particular, the calculators and estimations provided on this Site are intended to be guides only and Fonterra does not warrant their accuracy, adequacy or completeness, or that it is suitable for your intended use. You shall not make any use of calculations or estimations generated through this Site in a manner which might cause a third party to rely on that information in a way which could give rise to liability on the part of Fonterra.</p>' +
            '<p>Any data, information and prices on the Site may be displayed on a delayed basis, and should be updated by clicking the refresh button on your browser.</p>' +
            '<p><b>Non-availability</b></p>' +
            '<p>This Site may not be continuously available for reasons that may include maintenance or repair.</p>' +
            '<p><b>No Warranties</b></p>' +
            '<p>Your use of the Site is at your own risk. The materials and information in this Site are provided &quot;as is&quot; and &quot;as available&quot; and without warranties of any kind, either express or implied. To the fullest extent permissible by law, Fonterra disclaims all warranties, express or implied, including but not limited to implied warranties of merchantability and fitness for a particular purpose. Fonterra does not warrant that the functions contained in the materials will be uninterrupted or error-free, that defects will be corrected, or that the Site or the server that makes it available are free of viruses or other harmful components. Fonterra does not warrant or make any representation regarding the use, or the results of the use, of the material in the Site in terms of their correctness, accuracy, sufficiency, reliability, or otherwise.</p>' +
            '<p>Under no circumstances, including but not limited to negligence, shall Fonterra be liable for your reliance on any such information nor shall Fonterra be liable for any direct, incidental, special, consequential, indirect or punitive damages that result from the use of, or the inability to use, the materials in this Site or the materials in any sites linked to this Site, even if Fonterra, or a Fonterraauthorised representative has been advised of the possibility of such damages.</p>' +
            '<p><b>Indemnity</b></p>' +
            '<p>You agree to indemnify Fonterra and keep Fonterra fully indemnified against all actions, proceedings, losses, liabilities, damages, claims, demands, costs and expenses suffered or incurred by Fonterra as a result of or in connection with any act or omission of yours or breach of any of these Terms.</p>' +
            '<p><b>User Name and Password</b></p>' +
            '<p>As a Member you will be issued with a User Name and User Password. You will be given your own User Name as part of the application process. Under no circumstances should you keep a written or electronic record of your User Password, nor disclose your User Password to any other person.</p>' +
            '<p>Your User Password must remain confidential to you alone and you must take all reasonable steps to prevent disclosure of your User Password. By utilising your User Password on this site you confirm that your use of the Site will be governed by these Terms and any other terms (including the Community Rules) placed on the Site.</p>' +
            '<p>If you are authorised by us to use the Change of Ownership facility, you may be able to grant online access to certain individuals nominated by you to some or all of your farm management data held by us. You are solely responsible for your use of this facility, for any use of this facility which uses your User Name and User Password, and for any data access provided by us in accordance with your use of the Change of Ownership facility.</p>' +
            '<p>From time to time, we may rename, update, rearrange or otherwise change the web pages within the Site which manage and display your farm management data. In these circumstances we will endeavour to ensure that all relevant links remain current and that the data will continue to be available to you and to any third parties nominated by you through the Change of Ownership facility. However, we do not warrant that the relevant functions will be uninterrupted or error-free and the exclusions of warranties and limitation of liability set out above will apply.</p>' +
            '<p>You agree to:</p>' +
            '<ul>' +
            '<li>effect and maintain security measures to prevent (i) third parties gaining access to, and/or using, the Site using your User Name or User Password, (ii) any other interference with the Site (such access, use, interference referred to in this Agreement as &quot;Unauthorised Use&quot;) and (iii) any loss or damage arising from Unauthorised Use;</li>' +
            '<li>notify us immediately if you become aware of any Unauthorised Use and, at your cost, take any action which is necessary or which we may require to prevent any further Unauthorised Use occurring and make good any damage arising from any Unauthorised Use; and</li>' +
            '<li>advise us immediately of any unauthorised disclosure of User Name or User Password. In addition to the privacy terms below (under the heading, &quot;Privacy&quot;), you further agree that we may, for the purpose of providing the services and information on this Site to you, make use of any information about you or your business or personal affairs which are made known to us through our relationship with you whether through the use of the Site or otherwise.</li>' +
            '</ul>' +
            '<p><b>Managing Access</b></p>' +
            '<p>You and other Members may be authorised by us to use the Manage Access facility to grant online access to certain third party individuals nominated by you or other Members (&ldquo;Third Party Users&rdquo;) to some or all of your information or data posted to the Site. Where you use the Manage Access facility to grant a Third Party User access rights to your information or data, you agree that:</p>' +
            '<ul>' +
            '<li>Fonterra will provide each Third Party User with a User Name. You are responsible for ensuring that the correct User Name is specified for each Third Party User you intend to allow access to your information or data.</li>' +
            '<li>Fonterra will not be responsible for any harm caused to you or others as a result of any access to your information or data by any Third Party User due to incorrect provision by you of a Third Party User&rsquo;s User Name.</li>' +
            '<li>You are responsible for granting a Third Party User you nominate access to each page on the</li>' +
            '</ul>' +
            '<p>Site.</p>' +
            '<ul>' +
            '<li>You may be able to change or remove the access rights of Third Party Users granted by other Members in respect of your information or data. Other Members who are authorised to use the Manage Access facility in respect of your information and data may change or remove the access rights which you granted to your Third Party User.</li>' +
            '<li>Fonterra has no responsibility for your use of the Manage Access facility, or the use of the facility by any other Member authorised to use the facility in respect of your information and data.</li>' +
            '<li>Fonterra will not be responsible for any harm caused to you or others by you or others allowing a Third Party User to view your information or data, or for any amendments made by a Third Party User to your information or data where you or another Member, have allowed a Third Party User access to a page that allows for the amendment or updating of information or data.</li>' +
            '</ul>' +
            '<p><b>Privacy</b></p>' +
            '<p>Fonterra will collect, use, store and disclose your personal information, and, where appropriate, will allow its related companies to use your personal information, for:</p>' +
            '<ul>' +
            '<li>business purposes;</li>' +
            '<li>providing you with supplier services;</li>' +
            '<li>collating company financial information;</li>' +
            '<li>meeting its legal obligations;</li>' +
            '<li>research;</li>' +
            '<li>administering its relationship with you; and</li>' +
            '<li>for other purposes relating to the dairy industry.</li>' +
            '</ul>' +
            '<p>You agree Fonterra (or any of its related companies) may:</p>' +
            '<ul>' +
            '<li>use your personal information and give that information to third parties, including, without limitation, those you nominate, for the purposes set out above; and</li>' +
            '<li>make enquiries about you to third parties. You also agree to those third parties providing information about you to Fonterra.</li>' +
            '</ul>' +
            '<p>You are not obliged to provide us with your personal information, although failure to do so may result in us being unable to process your registration or provide you with information you may wish to obtain through this Site.</p>' +
            '<p>If you would like to inspect or request correction, updating or deletion of the personal information we hold about you, please contact us at aufarmsource@fonterra.com</p>' +
            '<p><b>Security</b></p>' +
            '<p>We will use our best endeavours to keep information and materials on the Site, including those provided by you or on your behalf, secure and confidential. However, we will not be responsible for any breach of security caused by third parties and do not guarantee that the Site is completely secure. The internet is not a secure environment and so you must take care when transmitting any information over the internet. In particular, you should make every effort to keep your User Name and User Password secure and confidential.</p>'
        ),
        new WebTextWrapper(
            'AMSListOfBDMs : Business Development Managers',
            '<h2><strong>Business Development Managers</strong></h2>' +
            '<h3><strong>North</strong></h3>' +
            '<p><b>Ajay Agarwal</b><br />' +
            'Business Development Manager &ndash; Northern Victoria<br />' +
            'Mobile: +61 408 753 569<br />' +
            'Email: <a href="mailto:ajay.agarwal@fonterra.com" target="_blank" title="mailto:ajay.agarwal@fonterra.com">ajay.agarwal@fonterra.com</a><br />' +
            'Address: 20 Midland Highway, Stanhope VIC, 3623</p>' +
            '<h3><strong>East</strong></h3>' +
            '<p><b>Peter Panayiotou</b><br />' +
            'Business Development Manager &ndash; Gippsland<br />' +
            'Mobile: +61 407 348 752<br />' +
            'Email: <a href="mailto:peter.panayiotou@fonterra.com" target="_blank" title="mailto:peter.panayiotou@fonterra.com">peter.panayiotou@fonterra.com</a><br />' +
            'Address: Darnum Park Road, Darnum VIC, 3822</p>' +
            '<h3><strong>South</strong></h3>' +
            '<p><b>Troy Franks</b><br />' +
            'Business Development Manager &ndash; Tasmania<br />' +
            'Mobile: + 61 400 948 683<br />' +
            'Email: <a href="mailto:troy.franks@fonterra.com" target="_blank" title="mailto:troy.franks@fonterra.com">troy.franks@fonterra.com</a><br />' +
            'Address: 155 Mersey Road, Spreyton TAS, 7310</p>' +
            '<h3><strong>West</strong></h3>' +
            '<p><b>Katie O&rsquo;Toole</b><br />' +
            'Business Development Manager &ndash; Western Victoria<br />' +
            'Mobile: + 61 438 111 141<br />' +
            'Email: <a href="mailto:katie.otoole@fonterra.com" target="_blank" title="mailto:katie.otoole@fonterra.com">katie.otoole@fonterra.com</a><br />' +
            'Address: 129 Curdie Street, Cobden VIC, 3266</p>'
        )
    };

    public static void createWebText() {
        List<Knowledge__kav> articles = new List<Knowledge__kav>();

        List<Knowledge__kav> existingPublishedArticles = [SELECT UrlName
                                                            FROM Knowledge__kav
                                                            WHERE PublishStatus = 'Online'
                                                            AND RecordTypeId = :GlobalUtility.amsWebTextRecordTypeId
                                                            AND Language = 'en_US'];

        List<Knowledge__kav> existingDraftArticles = [SELECT UrlName
                                                            FROM Knowledge__kav
                                                            WHERE PublishStatus = 'Draft'
                                                            AND RecordTypeId = :GlobalUtility.amsWebTextRecordTypeId
                                                            AND Language = 'en_US'];


        List<Knowledge__kav> existingArticles = existingPublishedArticles;
        existingArticles.addAll(existingDraftArticles);

        Set<String> existingArticleUrlNames = new Set<String>();

        for (Knowledge__kav article : existingArticles) {
            existingArticleUrlNames.add(article.UrlName);
        }

        for (WebTextWrapper wrapper : webTexts) {
            String urlName = toUrlName(wrapper.title);

            System.debug('Processing ' + urlName);

            if (!existingArticleUrlNames.contains(urlName)) {
                Knowledge__kav article = new Knowledge__kav(
                    Title = wrapper.title,
                    Heading__c = wrapper.heading,
                    UrlName = urlName,
                    Language = 'en_US',
                    Content__c = wrapper.content,
                    IsVisibleInCsp = true,
                    RecordTypeId = GlobalUtility.amsWebTextRecordTypeId
                );
                articles.add(article);
            }
        }

        insert articles;

        List<Id> articleIds = new List<Id>();
        for (Knowledge__kav article : articles) {
            articleIds.add(article.Id);
        }

        List<Knowledge__kav> articleKnowledgeIds = [SELECT KnowledgeArticleId
                                                       FROM Knowledge__kav
                                                       WHERE Id IN :articleIds];

        // Unfortunately only one at a time - hope there aren't too many of them or
        // we will have to batch this up.
        for (Knowledge__kav article : articleKnowledgeIds) {
            KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
        }
    }

    private static String toUrlName(String title) {
        String urlName = title.replace(' ', '').replace(':', '');

        if (Test.isRunningTest()) {
            // For some reason Knowledge in UTs is weird, it won't allow you to create an article with the same
            // UrlName as in the Org. So add this suffix so that we don't clash.
            urlName += '-unit-test';
        }

        return urlName;
    }
}