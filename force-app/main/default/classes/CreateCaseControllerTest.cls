/**
 * CreateCaseControllerTest.cls
 * Description: Test class for the Create Case Controller
 * @author: Sarah Hay (Trineo)
 * @date: February 2016
 */
@isTest
private class CreateCaseControllerTest {
    // Getting record type and required integration profiles
    private static Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();
    private static Profile integrationProfile = GlobalUtility.getIntegrationProfile();
    private static Id individualFarmRecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Farm').getRecordTypeId();

    @isTest static void testCreateCaseIndividualRelationship() {
        // inserting objects and fields required for tests to run
        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        Account farm = TestClassDataUtil.createAUFarms(1, true)[0];
        Contact individual = TestClassDataUtil.createIndividualAU('Tim', 'Garden', true);

        Individual_Relationship__c ir = new Individual_Relationship__c(
            RecordTypeId = individualFarmRecordTypeId,
            Individual__c = individual.Id,
            Farm__c = farm.Id,
            Active__c = true,
            Role__c = '',
            Party__c = party.Id
        );
        insert ir;
        Case randomCase = new Case();
        insert randomCase;

        Test.startTest();
        // Set up page context, add necessary Individual Relationship parameter
        PageReference pageRef = Page.CreateCase;
        pageRef.getParameters().put('irId', String.valueOf(ir.Id));
        Test.setCurrentPage(pageRef);

        // Set up running admin user
        User adminUser = TestClassDataUtil.createAdminUser();
        System.runAs(adminUser) {
            // Create instance of controller
            CreateCaseController ccc = new CreateCaseController();
            ccc.setRT();

            // Setting fields input by User
            ccc.c.Sub_Type__c = 'Late Off Farm';
            ccc.c.Type = 'Collections';
            ccc.c.RecordTypeId = [Select Id from RecordType Where Name = 'Transport AU'].Id;
        }
        Test.stopTest();
    }

    @isTest static void testCreateCaseNoParameters() {
        PageReference pageRef = Page.CreateCase;
        pageRef.getParameters().put('irId', null);
        pageRef.getParameters().put('farmId', null);
        pageRef.getParameters().put('individualId', null);
        Test.setCurrentPage(pageRef);

        Test.startTest();

        // Set up running admin user
        User adminUser = TestClassDataUtil.createAdminUser();
        System.runAs(adminUser) {
            // Create instance of controller
            CreateCaseController ccc = new CreateCaseController();
            List<ApexPages.Message> msgs = ApexPages.getMessages();
            boolean b = false;
            for (ApexPages.Message msg : msgs) {
                if (msg.getDetail().contains('No Individual or Farm Id Provided')) {
                    b = true;
                }
            }
            System.assert(b);
        }
        Test.stopTest();
    }
    @isTest static void testCreateCaseFarmOnly() {
        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        Account farm = TestClassDataUtil.createAUFarms(1, true)[0];
        Contact individual = TestClassDataUtil.createIndividualAU('Tim', 'Garden', true);

        Case c = new Case(AccountId = farm.Id);
        insert c;
        PageReference pageRef = Page.CreateCase;
        pageRef.getParameters().put('farmId', String.valueOf(farm.Id));
        Test.setCurrentPage(pageRef);

        Test.startTest();

        // Set up running admin user
        User adminUser = TestClassDataUtil.createAdminUser();
        System.runAs(adminUser) {
            // Create instance of controller
            CreateCaseController ccc = new CreateCaseController();
            System.assertEquals(farm.Id, ccc.farm.Id);
            System.assertEquals(c.Id, ccc.relevantCases.get(0).Id);
        }
        Test.stopTest();
    }
    @isTest static void testCreateCaseIndividualOnly() {
        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        Account farm = TestClassDataUtil.createAUFarms(1, true)[0];
        Contact individual = TestClassDataUtil.createIndividualAU('Tim', 'Garden', true);

        Case c = new Case(ContactId = individual.Id, Incident_Date__c = Datetime.now());
        insert c;
        PageReference pageRef = Page.CreateCase;
        pageRef.getParameters().put('individualId', String.valueOf(individual.Id));
        Test.setCurrentPage(pageRef);

        Test.startTest();

        // Set up running admin user
        User adminUser = TestClassDataUtil.createAdminUser();
        System.runAs(adminUser) {
            // Create instance of controller
            CreateCaseController ccc = new CreateCaseController();
            System.assertEquals(individual.Id, ccc.individual.Id);
            System.assertEquals(c.Id, ccc.relevantCases.get(0).Id);
        }
        Test.stopTest();
    }
    @isTest static void testCreateCaseInvalidId() {
        PageReference pageRef = Page.CreateCase;
        pageRef.getParameters().put('farmId', '123');
        pageRef.getParameters().put('individualId', '123');
        pageRef.getParameters().put('irId', '123');
        Test.setCurrentPage(pageRef);

        Test.startTest();

        // Set up running admin user
        User adminUser = TestClassDataUtil.createAdminUser();
        System.runAs(adminUser) {
            // Create instance of controller
            CreateCaseController ccc = new CreateCaseController();
            List<ApexPages.Message> msgs = ApexPages.getMessages();
            boolean b = false;
            boolean c = false;
            boolean d = false;
            for (ApexPages.Message msg : msgs) {
                if (msg.getDetail().contains('Invalid Individual Id')) {
                    b = true;
                }
                if (msg.getDetail().contains('Invalid Farm Id')) {
                    c = true;
                }
                if (msg.getDetail().contains('Invalid Individual Relationship Id')) {
                    d = true;
                }
            }
            System.assert(b);
            System.assert(c);
            System.assert(d);
        }
        Test.stopTest();
    }

    @isTest static void testCreateCaseAUIndividual() {
        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        List<Reference_Region__c> referenceRegions = TestClassDataUtil.createAuReferenceRegions();
        insert referenceRegions;
        List<Account> farms = TestClassDataUtil.createAuFarms(referenceRegions);
        insert farms;
        Contact individual = TestClassDataUtil.createIndividualAU(true);

        PageReference pageRef = Page.CreateCase;
        pageRef.getParameters().put('individualId', String.valueOf(individual.Id));
        Test.setCurrentPage(pageRef);

        Test.startTest();

        User adminUser = TestClassDataUtil.createAdminUser();
        System.runAs(adminUser) {
            CreateCaseController ccc = new CreateCaseController();
            System.assertEquals(individual.Id, ccc.individual.Id);
        }
        Test.stopTest();
    }
}