/**
 * "New" graph that works with the AMSCollectionServiceRedux, just basic functions for now
 * Essentially the controller for AMSLineChartRedux.component- which is currently only used in AMSCombinedProduction
 *
 * Author: John Au (Trineo)
 * Date: October 2018
 **/
public class AMSCollectionGraphRedux {

    public AMSCollectionServiceRedux.UnitOfMeasurement selectedComparisonValue { get; set; }

    public transient String categorySeries { get; set; }

    public AMSCollectionServiceRedux.AggregateType aggregateType { get; set; }
    public String aggregateTypeName {
        get {
            return aggregateType.name();
        }
        set;
    }

    public List<Series> series { get; set; }

    public class Series {
        public String name { get; set; }
        public String color { get; set; }
        public List<Double> values { get; set; }

        public Series(String name, String color) {
            this.name = name;
            this.color = color;
            this.values = new List<Double>();
        }

        public String getValuesJSON() {
            return JSON.serialize(values);
        }
    }

    public AMSCollectionGraphRedux(
                Date fromDate,
                Date toDate,
                List<List<AMSCollectionServiceRedux.CollectionAggregate>> collectionAggregates,
                AMSCollectionServiceRedux.UnitOfMeasurement selectedComparisonValue,
                AMSCollectionServiceRedux.AggregateType aggregateType) {

        this.selectedComparisonValue = selectedComparisonValue;
        this.aggregateType = aggregateType;
        Integer comparisons = collectionAggregates.size();

        Map<Date, List<AMSCollectionServiceRedux.CollectionAggregate>> aggregatesByDate = createAggregatesByDate(fromDate, toDate, aggregateType, comparisons);

        for (Integer i = 0; i < comparisons; i++) {
            populateAggregatesByDate(aggregatesByDate, collectionAggregates[i], aggregateType, i);
        }

        createCollectionJSON(aggregatesByDate, comparisons);
    }

    private Map<Date, List<AMSCollectionServiceRedux.CollectionAggregate>> createAggregatesByDate(Date fromDate, Date toDate, AMSCollectionServiceRedux.AggregateType aggregateType, Integer comparisons) {
        Map<Date, List<AMSCollectionServiceRedux.CollectionAggregate>> aggregatesByDate = new Map<Date, List<AMSCollectionServiceRedux.CollectionAggregate>>();

        if (aggregateType == AMSCollectionServiceRedux.AggregateType.MONTHLY) {
            fromDate = fromDate.toStartOfMonth();
            toDate = toDate.toStartOfMonth();
        }

        while (fromDate <= toDate) {
            List<AMSCollectionServiceRedux.CollectionAggregate> paddedCollectionAggregates = new List<AMSCollectionServiceRedux.CollectionAggregate>();

            for (Integer i = 0; i < comparisons; i++) {
                paddedCollectionAggregates.add(null);
            }

            aggregatesByDate.put(fromDate, paddedCollectionAggregates);

            if (aggregateType == AMSCollectionServiceRedux.AggregateType.MONTHLY) {
                fromDate = fromDate.addMonths(1);
            } else {
                fromDate = fromDate.addDays(1);
            }
        }

        return aggregatesByDate;
    }

    private void populateAggregatesByDate(Map<Date, List<AMSCollectionServiceRedux.CollectionAggregate>> aggregatesByDate, List<AMSCollectionServiceRedux.CollectionAggregate> collectionAggregates, AMSCollectionServiceRedux.AggregateType aggregateType, Integer comparison) {
        for (AMSCollectionServiceRedux.CollectionAggregate aggregate : collectionAggregates) {
            Date collectionDateKey;

            if (aggregateType == AMSCollectionServiceRedux.AggregateType.MONTHLY) {
                collectionDateKey = Date.newInstance(aggregate.pickUpDate.addYears(comparison).year(), aggregate.pickupDate.month(), 1);
            } else {
                collectionDateKey = aggregate.pickUpDate.addYears(comparison); //move the date forward to match the current year for each year we go back
            }

            if (aggregatesByDate.containsKey(collectionDateKey)) { //you may occassionally have missing dates because we base this on the current year
                aggregatesByDate.get(collectionDateKey).set(comparison, aggregate);
            }
        }
    }

    public void createCollectionJSON(Map<Date, List<AMSCollectionServiceRedux.CollectionAggregate>> aggregatesByDate, Integer comparisons) {
        List<Long> categoryList = new List<Long>();
        series = new List<Series>();

        series.add(new Series('Current Season', '#000000')); //hardcoded for now
        series.add(new Series('Previous Season', '#C3553D'));

        for (Date collectionAggregateKey : aggregatesByDate.keySet()) {
            List<AMSCollectionServiceRedux.CollectionAggregate> aggregatesForDay = aggregatesByDate.get(collectionAggregateKey);

            categoryList.add(((DateTime) collectionAggregateKey).getTime());

            for (Integer i = 0; i < comparisons; i++) {
                Series currentSeries = series[i];
                AMSCollectionServiceRedux.CollectionAggregate collectionAggregate = aggregatesForDay[i];

                if (collectionAggregate == null) {
                    currentSeries.values.add(null);
                    continue; //no collection on the day
                }

                if (selectedComparisonValue.equals(AMSCollectionServiceRedux.KgMS)) {
                    addToListIfNotZero(currentSeries.values, collectionAggregate.kgMSTotal);
                } else if (selectedComparisonValue.equals(AMSCollectionServiceRedux.Litres)) {
                    addToListIfNotZero(currentSeries.values, collectionAggregate.volumeLitresTotal);
                } else if (selectedComparisonValue.equals(AMSCollectionServiceRedux.ProteinPercentage)) {
                    addToListIfNotZero(currentSeries.values, collectionAggregate.proteinPercentageTotal);
                } else if (selectedComparisonValue.equals(AMSCollectionServiceRedux.FatPercentage)) {
                    addToListIfNotZero(currentSeries.values, collectionAggregate.fatPercentageTotal);
                } else if (selectedComparisonValue.equals(AMSCollectionServiceRedux.KgMSPercentage)) {
                    addToListIfNotZero(currentSeries.values, collectionAggregate.kgMSPercentageTotal);
                } else if (selectedComparisonValue.equals(AMSCollectionServiceRedux.FatKg)) {
                    addToListIfNotZero(currentSeries.values, collectionAggregate.fatKgsTotal);
                } else if (selectedComparisonValue.equals(AMSCollectionServiceRedux.ProteinKg)) {
                    addToListIfNotZero(currentSeries.values, collectionAggregate.proteinKgsTotal);
                }
            }
        }


        this.categorySeries = JSON.serialize(categoryList);
    }

    /**
    * If the value is 0 it adds null to the list
    * This is because nulls are not plotted on the charts whereas 0's are
    * 0's from Collection records are not 'real values' but filler values until the actual value is sent from Milk Pay
    */
    private void addToListIfNotZero(List<Double> valueList, Decimal value) {
        if (value != 0) {
            valueList.add(value);
        } else {
            valueList.add(null);
        }
    }
}