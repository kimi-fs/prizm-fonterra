/*
* Description: Service class for the Address object
*
* IMPORTANT: This class is WITHOUT sharing - behave appropriately.
*
* @author: Amelia (Trineo)
* @date: July 2018
* @test: AddressServiceTest
*/
public without sharing class AddressService {

    private static final String ADDRESS_SERVICE_CLASS = 'AddressService';
    private static final Date BLANK_DATE = Date.newInstance(1700, 1, 1); // Represents Blank Dates

    /**
    * CAUTION! Make sure the parameters passed are safe and cannot contain a SOQL injection
    */
    private static List<Address__c> getAddresses(String queryCondition) {
        return getAddresses(queryCondition, '');
    }

    /**
    * CAUTION! Make sure the parameters passed are safe and cannot contain a SOQL injection
    */
    private static List<Address__c> getAddresses(String queryCondition, String orderCondition) {
        String query = 'SELECT ' + QueryUtil.getFieldsFromObjectForQuery('Address__c');
        query += ' FROM Address__c ';
        query += queryCondition;
        if (String.isNotBlank(orderCondition)) {
            query += ' ORDER BY ' + orderCondition;
        }
        List<Address__c> addresses = Database.query(query);
        return addresses;
    }

    public static Address__c getAddressFromID(Id addressId){
        String queryCondition = ' WHERE Id = \'' + addressId + '\'';
        List<Address__c> results = getAddresses(queryCondition);
        return results.isEmpty() ? null : results[0];
    }

    /**
    * Gets the future postal addresses for a party
    */
    public static List<Address__c> getFuturePostalAddressesForEntity(Id entityId){
        String queryCondition = ' WHERE Entity__c = \'' + entityId + '\'';
        queryCondition += ' AND RecordTypeId = \'' + GlobalUtility.postalAddressRecordTypeId + '\'';
        queryCondition += ' AND Start_Date__c > TODAY';
        queryCondition += ' AND (End_Date__c = null OR End_Date__c > TODAY) ';
        queryCondition += ' AND To_Delete__c = false ';

        String orderCondition = ' Start_Date__c ASC ';

        List<Address__c> results = getAddresses(queryCondition, orderCondition);
        return results;
    }

    /**
    * Gets a list of all active postal addresses for an entity
    */
    public static List<Address__c> getPostalAddressesForEntity(Id entityId) {
        String queryCondition = ' WHERE Entity__c = \'' + entityId + '\'';
        queryCondition += ' AND RecordTypeId = \'' + GlobalUtility.postalAddressRecordTypeId + '\'';
        queryCondition += ' AND Active__c = true ';

        String orderCondition = ' LastModifiedDate DESC ';

        List<Address__c> results = getAddresses(queryCondition, orderCondition);
        return results;
    }

    /**
    * Gets the most recently updated postal address for an entity
    */
    public static Address__c getCurrentPostalAddressForEntity(Id entityId){
        List<Address__c> currentPostalAddresses = getPostalAddressesForEntity(entityId);
        return currentPostalAddresses.isEmpty() ? null : currentPostalAddresses[0];
    }

    public static List<Account> getRelatedAddressesForAccounts(List<Id> accountIds, Set<Id> accountRecordTypeIds){
        String query = 'SELECT ' + QueryUtil.getFieldsFromObjectForQuery('Account');
        query += ', ' + addressSubQuery('Addresses__r');
        query += ' FROM Account ';
        query += ' WHERE Id IN :accountIds ';
        query += ' AND RecordTypeId IN :accountRecordTypeIds ';

        List<Account> records = Database.query(query);
        return records;
    }

    public static List<Contact> getRelatedAddressesForContacts(List<Id> contactIds){
        String query = 'SELECT ' + QueryUtil.getFieldsFromObjectForQuery('Contact');
        query += ', ' + addressSubQuery('Addresses__r');
        query += ' FROM Contact ';
        query += ' WHERE Id IN :contactIds ';

        List<Contact> records = Database.query(query);
        return records;
    }

    public static List<Lead> getRelatedAddressesForLeads(List<Id> leadIds){
        String query = 'SELECT ' + QueryUtil.getFieldsFromObjectForQuery('Lead');
        query += ', ' + addressSubQuery('Addresses__r');
        query += ' FROM Lead ';
        query += ' WHERE Id IN :leadIds ';

        List<Lead> records = Database.query(query);
        return records;
    }

    public static List<Opportunity> getRelatedAddressesForOpportunities(List<Id> opportunityIds){
        String query = 'SELECT ' + QueryUtil.getFieldsFromObjectForQuery('Opportunity');
        query += ', ' + addressSubQuery('Addresses__r');
        query += ' FROM Opportunity ';
        query += ' WHERE Id IN :opportunityIds ';

        List<Opportunity> records = Database.query(query);
        return records;
    }

    private static String addressSubQuery(String childRelationshipName){
        String subQuery = 'SELECT ' + QueryUtil.getFieldsFromObjectForQuery('Address__c');
        subQuery += ' FROM ' + childRelationshipName;
        subQuery += ' WHERE Active__c = true ';

        return '(' + subQuery + ')';
    }

    public static String getState(Address__c address) {
        String returnString = '';
        if (address == null) {
            returnString = '';
        } else {
            returnString = String.isBlank(address.State__c) ? '' : address.State__c;
        }

        return returnString;
    }

    public static String getCity(Address__c address) {
        String returnString = '';
        if (address == null) {
            returnString = '';
        } else {
            returnString = String.isBlank(address.City__c) ? '' : address.City__c;
        }

        return returnString;
    }

    public static String getCountry(Address__c address) {
        String returnString = '';
        if (address == null) {
            returnString = '';
        } else {
            returnString = String.isBlank(address.Country_Code__c) ? '' : address.Country_Code__c;
        }

        return returnString;
    }

    public static String getPostCode(Address__c address) {
        String returnString = '';
        if (address == null) {
            returnString = '';
        } else {
            returnString = String.isBlank(address.Postcode__c) ? '' : address.Postcode__c;
        }

        return returnString;
    }

    public static String getAddressID(Address__c address) {
        String returnString = '';
        if (address == null) {
            returnString = '';
        } else {
            returnString = address.Id;
        }

        return returnString;
    }

    public static String getStreet(Address__c address) {
        String returnString = '';
        if (address == null) {
            returnString = '';
        } else if (String.isBlank(address.Street_1__c) && String.isBlank(address.Street_2__c)) {
            returnString = address.PO_Box__c;
        } else {
            returnString = address.Street_Number__c + ' ' + address.Street_1__c + '\n' + address.Street_2__c + '\n' + address.Suburb__c;
            returnString = returnString.replaceAll('null', '');
        }

        return returnString;
    }

    public static Date getEndDate(Address__c address) {
        Date returnDate;
        if (address == null) {
            returnDate = null;
        } else if (address.End_Date__c != null) {
            returnDate = address.End_Date__c;
        } else {
            returnDate = BLANK_DATE;
        }

        return returnDate;
    }

}