/**
 * Description: Test for AMSCommunityTemplateController
 * @author: Amelia (Trineo)
 * @date: July 2017
 */
@isTest
private class AMSCommunityTemplateControllerTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup static void testSetup() {
        SetupAMSCommunityPageMessages.createPageMessages();
        TestClassDataUtil.individualDefaultAccountAu();
    }

    @isTest static void testCheckUserAccess_NotLoggedIn() {
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        PageReference currentPage = Page.AMSAdvice;
        System.Test.setCurrentPage(currentPage);

        System.runAs(guestUser) {
            PageReference pr = AMSCommunityTemplateController.checkUsersAccess();
            System.assert(pr.getUrl().tolowercase().contains(Page.AMSLogin.getUrl().tolowercase()), 'Page did not redirect to login');
        }
    }

    @isTest static void testCheckUserAccess_LoggedInNotVerified() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            PageReference pr = AMSCommunityTemplateController.checkUsersAccess();
            System.assert(pr.getUrl().tolowercase().contains(Page.AMSEmailVerification.getUrl().tolowercase()), 'Page did not redirect to email verification');
        }
    }

    @isTest static void testCheckUserAccess_LoggedInAndVerified() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs([SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1][0]) {
            communityUser.Email_Verified__c = Datetime.now();
            update communityUser;
        }

        System.runAs(communityUser) {
            PageReference pr = AMSCommunityTemplateController.checkUsersAccess();
            System.assertEquals(null, pr, 'Page should not have redirected');
        }
    }

    @isTest static void testIsViewingDifferentContact() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            Boolean isViewingDifferentContact = AMSCommunityTemplateController.isViewingDifferentContact;
            System.assertEquals(false, isViewingDifferentContact);
        }
    }

    @isTest static void testGetNoDataMessage() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            Test.setCurrentPage(Page.AMSCommunityTemplate);

            AMSCommunityTemplateController controller = new AMSCommunityTemplateController();
            String noData = controller.getNoDataMessage();
            System.assertNotEquals(null, noData);
        }
    }

    @isTest static void testConstructor_SupportedBrowser() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            Test.setCurrentPage(Page.AMSCommunityTemplate);

            AMSCommunityTemplateController controller = new AMSCommunityTemplateController();
            System.assertNotEquals(null, controller.adviceArticleWrappers);
            System.assertNotEquals(null, controller.newsArticleWrappers);
            System.assertEquals(true, controller.supportedBrowser);

            // they're not set in tests but we can cover the getters
            System.assertEquals(null, controller.headerPhoto);
            System.assertEquals(null, controller.menuPhoto);
        }
    }

    @isTest static void testConstructor_NotSupportedBrowser() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            Test.setCurrentPage(Page.AMSCommunityTemplate);
            ApexPages.currentPage().getHeaders().put('USER-AGENT', ' MSIE 09');

            AMSCommunityTemplateController controller = new AMSCommunityTemplateController();
            System.assertNotEquals(null, controller.adviceArticleWrappers);
            System.assertNotEquals(null, controller.newsArticleWrappers);
            System.assertEquals(false, controller.supportedBrowser);
            System.assertEquals('Your version of Internet Explorer is no longer supported, Please update to IE11 or greater.', controller.browserWarning);
        }
    }

    @isTest static void testSearch() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            Test.setCurrentPage(Page.AMSCommunityTemplate);

            AMSCommunityTemplateController controller = new AMSCommunityTemplateController();
            controller.searchString = 'What is the meaning of life';
            PageReference pr = controller.search();

            System.assertNotEquals(null, pr);
        }
    }

    @isTest static void testGetFAQParameterName() {
        System.assertEquals(AMSCommunityUtil.getFAQParameterName(), AMSCommunityTemplateController.getFAQParameterName());
    }

    @isTest static void testUserHasFarms() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu(USER_EMAIL);

        System.runAs(communityUser) {
            Test.setCurrentPage(Page.AMSCommunityTemplate);

            AMSCommunityTemplateController controller = new AMSCommunityTemplateController();
            Boolean userHasFarms = controller.getUserHasFarms();

            System.assertEquals(true, userHasFarms);
        }
    }

    @isTest static void testLogout() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            Test.setCurrentPage(Page.AMSCommunityTemplate);

            AMSCommunityTemplateController controller = new AMSCommunityTemplateController();
            PageReference logoutPR = controller.logout();

            System.assert(logoutPR.getUrl().contains('/logout'));
        }
    }

    @isTest static void testReturnToContact() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser) {
            Test.setCurrentPage(Page.AMSCommunityTemplate);

            AMSCommunityTemplateController controller = new AMSCommunityTemplateController();
            PageReference returnToContactPR = controller.returnToContact();

            System.assert(returnToContactPR.getUrl().contains(controller.individual.id));
        }
    }

}