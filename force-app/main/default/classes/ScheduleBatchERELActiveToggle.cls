/**
 * Schedule BatchERELActiveToggle.cls
 *
 * @author Marama (Trineo)
 * @date 2017
 **/
global class ScheduleBatchERELActiveToggle implements Schedulable, Database.Batchable<SObject> {

    global static void schedule() {
        String expression = ScheduleService.getCRONExpression('ScheduleBatchERELActiveToggle');
        String jobName = ScheduleService.getCRONJobName('ScheduleBatchERELActiveToggle');
        if (String.isNotBlank(expression)) {
            final ScheduleBatchERELActiveToggle job = new ScheduleBatchERELActiveToggle();
            System.schedule(jobName, expression, job);
        }
    }

    //To be scheduled daily at 1am preferably
    global void execute(SchedulableContext sc) {
        ScheduleBatchERELActiveToggle bsa = new ScheduleBatchERELActiveToggle();
        database.executebatch(bsa);
    }

    String query = 'SELECT Id, Active__c, Role__c, Creation_Date__c, Deactivation_Date__c, RecordType.Name FROM Entity_Relationship__c WHERE ((Active__c = false AND Creation_Date__c <= TODAY AND (Deactivation_Date__c = null OR Deactivation_Date__c >= TODAY)) OR (Active__c = true AND Deactivation_Date__c <= TODAY))';

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Entity_Relationship__c> scope) {
        Date dtToday = system.today();
        List<Entity_Relationship__c> toUpdate = new List<Entity_Relationship__c>();
        for (Entity_Relationship__c record : scope) {
            if (record.Creation_Date__c == null) {
                if (record.Deactivation_Date__c < dtToday && record.Active__c == true) {
                    record.Active__c = false;
                    if(record.RecordType.Name == 'Party-Farm' && String.isNotBlank(record.Role__c) && !record.Role__c.contains('(Previous)')){
                        record.Role__c = record.Role__c+' (Previous)';
                    }
                    toUpdate.add(record);
                }
            } else if (record.Deactivation_Date__c == null) {
                if (record.Creation_Date__c <= dtToday && record.Active__c == false) {
                    record.Active__c = true;
                    toUpdate.add(record);
                }
            } else {
                if (record.Creation_Date__c <= dtToday && record.Deactivation_Date__c < dtToday) {
                    if (record.Creation_Date__c > record.Deactivation_Date__c) {
                        if (record.Active__c == false) {
                            record.Active__c = true;
                            toUpdate.add(record);
                        }
                    } else {
                        if (record.Active__c == true) {
                            record.Active__c = false;
                            if(record.RecordType.Name == 'Party-Farm' && String.isNotBlank(record.Role__c) && !record.Role__c.contains('(Previous)')){
                                record.Role__c = record.Role__c+' (Previous)';
                            }
                            toUpdate.add(record);
                        }
                    }
                } else if (record.Creation_Date__c > dtToday && record.Deactivation_Date__c > dtToday) {
                    //Do nothing.
                } else if (record.Creation_Date__c <= dtToday ) {
                    if (record.Active__c == false) {
                        record.Active__c = true;
                        toUpdate.add(record);
                    }
                } else {
                    if (record.Active__c == true) {
                        record.Active__c = false;
                        if(record.RecordType.Name == 'Party-Farm' && String.isNotBlank(record.Role__c) && !record.Role__c.contains('(Previous)')){
                            record.Role__c = record.Role__c+' (Previous)';
                        }
                        toUpdate.add(record);
                    }
                }
            }
        }
        System.debug('Number of relationships getting updated this batch: ' + toUpdate.size());

        if(!toUpdate.isEmpty()) {
            Database.SaveResult[] srList = Database.update(toUpdate, false);
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Debug updated record
                    System.debug('Database update ' + sr.getId() + ' was a success');
                    Logger.log('BatchERELActiveToggle success', 'Database update ' + sr.getId() + ' was a success', Logger.LogType.INFO);
                }
                else {
                    // Operation failed, so get all errors
                    System.debug('Database update ' + sr.getId() + ' had the following errors.');
                    Logger.log('BatchERELActiveToggle error', 'Database update ' + sr.getId() + ' had the following errors.', Logger.LogType.ERROR);
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        Logger.log('BatchERELActiveToggle error', err.getStatusCode() + ': ' + err.getMessage(), Logger.LogType.ERROR);
                    }
                }
            }
            Logger.saveLog();
        }
    }

    global void finish(Database.BatchableContext BC) {

    }

}