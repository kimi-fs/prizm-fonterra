/**
 * Description: Test class for AMSWebTextController
 * @author: Amelia (Trineo)
 * @date: September 2017
 */
@isTest
private class AMSWebTextControllerTest {

    @testSetup static void testSetup( ){
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.createAMSWebTextArticles(2);
    }

    @isTest static void testConstructor_Title_NoArticle() {
        AMSWebTextController cont = new AMSWebTextController();
        cont.webTextTitle = 'does not exist';

        System.assertEquals('Unable to find WebTextArticle.', cont.webtext);
    }

    @isTest static void testConstructor_Title_WithArticle() {
        List<Knowledge__kav> wtArticleRecords = [
            SELECT Id,
                Title,
                Content__c
            FROM Knowledge__kav
            WHERE PublishStatus = 'Online'
            AND Language = 'en_US'
            LIMIT 1
        ];

        AMSWebTextController cont = new AMSWebTextController();
        cont.webTextTitle = wtArticleRecords[0].Title;

        System.assertNotEquals('undefined', cont.webtext);
        System.assertEquals(wtArticleRecords[0].Content__c, cont.webtext);
    }

    @isTest static void testConstructor_URLName_NoArticle() {
        AMSWebTextController cont = new AMSWebTextController();
        cont.webTextURLName = 'does not exist';

        System.assertEquals('We can\'t find the article you\'re looking for.', cont.webtext);
    }

    @isTest static void testConstructor_URLName_WithArticle() {
        List<Knowledge__kav> wtArticleRecords = [
            SELECT Id,
                Title,
                URLName,
                Content__c
            FROM Knowledge__kav
            WHERE PublishStatus = 'Online'
            AND Language = 'en_US'
            LIMIT 1
        ];

        AMSWebTextController cont = new AMSWebTextController();
        cont.webTextURLName = wtArticleRecords[0].URLName;

        System.assertNotEquals('undefined', cont.webtext);
        System.assertEquals(wtArticleRecords[0].Content__c, cont.webtext);
    }
}