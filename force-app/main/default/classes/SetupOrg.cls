/**
 * Utility class to initialise an Org with the pre-requisite data and changes.
 *
 * There are multiple purposes for this class, one is to setup a new Sandbox, another is to populate the
 * articles.
 *
 * This should be run *once* from execute-anonymous in a new Sandbox, we can change it (or have a wrapper class)
 * that 'implements SandboxPostCopy' to have this run automatically post Sandbox creation.
 *
 * @author Eddard Groenendaal (Trineo)
 * @date May 2017
 * @test SetupOrgTest
 */
global without sharing class SetupOrg implements SandboxPostCopy {

    public final static List<String> TEST_USER_ORG_NAMES = new List<String>{
        'int', 'stg', 'hsi', 'hss', 'dte'
    };

    public final static List<String> PARTIAL_FULL_ORG_NAMES = new List<String>{
        'int', 'stg', 'prj'
    };

    public final static Map<String, String> sandboxUrlMap = new Map<String, String>{'int' => 'test', 'stg' => 'qa'};

    public static String baseCommunityURL {
        get {
            if (baseCommunityURL == null){
                baseCommunityURL = [SELECT Id, Domain FROM Domain WHERE Domain LIKE '%fsau%' LIMIT 1].Domain;
            }
            return baseCommunityURL;
            } set;
        }

    public static Map<String, String> communityPrefixMap {
        get {
            if (communityPrefixMap == null){
                communityPrefixMap = new Map<String, String>();
                for(Network n : [SELECT Name, UrlPathPrefix FROM Network]) {
                    communityPrefixMap.put(n.Name, n.UrlPathPrefix);
                }
            }
            return communityPrefixMap;
        } set;
    }

    public class ExistingSandboxException extends Exception {}

    global void runApexClass(SandboxContext context) {
        //Don't create test users by default unless for spcified sandboxes
        Boolean createTestUsers = false;
        //Don't create org records (e.g. Channel Preference Settings) for partial/full sandboxes
        Boolean createOrgRecords = true;

        if (TEST_USER_ORG_NAMES.contains(context.sandboxName().toLowerCase())) {
            createTestUsers = true;
        } else if (PARTIAL_FULL_ORG_NAMES.contains(context.sandboxName().toLowerCase())) {
            createOrgRecords = false;
        }

        initialiseNewSandbox(false, createTestUsers, createOrgRecords);
    }

    public static void initialiseNewSandbox() {
        initialiseNewSandbox(false, false, true);
    }

    public static void initialiseNewSandbox(Boolean abortOnError, Boolean createTestUsers, Boolean createOrgRecords) {
        SavePoint sp = Database.setSavePoint();

        Organization org = [SELECT id, Name, IsSandbox FROM Organization];

        // Run if in a test or a sandbox
        if (Test.isRunningTest() == false && org.IsSandbox == false) {
            System.debug('Woah there - this is NOT a Sandbox!');
            System.debug('aborting');
            Database.rollback(sp);
            return;
        }

        // The sandbox name is appended to the username.
        String sandboxUrlName = UserInfo.getUserName().substringAfterLast('.');

        //update the sandbox Name based on the map due to org renaming
        if (sandboxUrlMap.containsKey(sandboxUrlName)) {
            sandboxUrlName = sandboxUrlMap.get(sandboxUrlName);
        }

        try{
            updateCommunityExtendedSessionSettings(abortOnError);
            updateCommunicationSettings(abortOnError);
            updateCommunityPageLinks(abortOnError);
            updateFieldTeamToolsCustomSettings(abortOnError);
            updateServiceCentreSettings(abortOnError);
            updateAUPOSettingsSettings(abortOnError);

            if (createOrgRecords) {
                createChannelPreferenceSettings();
                createReferencePeriods();
                createReferenceRegions();
                SetupAMSWebText.createWebText();
                SetupAMSAdvice.createArticles();
            }
            createPublicContacts();

            if (createTestUsers) {
                System.enqueueJob(new SandboxDataUserQueueable(true));
            }

            resetSFMarketingCloudConfiguration();

            // Call new scheduled jobs terminate class here


            // Need to disable the MetadataAPI and SAPPO remote site settings, this can be done via the MetadataAPI, but I'm not including
            // a whole heap of code in here just for that - so it will have to be a manual step
            System.debug('Navigate to "Setup > Administer > Security Controls > Remote Site Settings" and disable the following callouts: - MetadataAPI - SAPPO');


        } catch (Exception e){
            System.debug('aborting');
            System.debug(e.getMessage());
            Database.rollback(sp);
        }
    }

    private static String getListOfProfileIds(List<String> profileNames){
        String raisedByCaseExternalProfiles = '';
        List<Profile> profiles = [SELECT Id, Name FROM Profile WHERE Name IN :profileNames];
        for(Profile p : profiles){
            raisedByCaseExternalProfiles += p.Id + ', ';
        }
        return raisedByCaseExternalProfiles.removeEnd(', ');
    }

    public static void updateCommunityExtendedSessionSettings(Boolean abortOnError) {
        Map<String, Community_Extended_Sessions__c> settings = Community_Extended_Sessions__c.getAll();
        if (settings == null) {
            if (abortOnError) {
                throw new ExistingSandboxException('Can not find an existing Community_Extended_Sessions__c custom setting.');
            }
        } else {
            String amsCommunityURLPathPrefix = communityPrefixMap.get('Australian Milk Supply');
            for (Community_Extended_Sessions__c setting : settings.values()) {
                if (setting.Name == 'Australian Milk Supply' && amsCommunityURLPathPrefix != null) {
                    setting.Community_URL__c = 'https://' + baseCommunityURL + '/' + amsCommunityURLPathPrefix;
                } else if (setting.Name == 'Conga Session') {
                    setting.Community_URL__c = System.URL.getSalesforceBaseUrl().toExternalForm();
                }
            }
            update settings.values();
        }
    }

    public static void updateCommunicationSettings(Boolean abortOnError){
        // Communication Settings for SMS disable
        Communication_Settings__c commSettings = Communication_Settings__c.getOrgDefaults();
        if (commSettings == null) {
            if (abortOnError) {
                throw new ExistingSandboxException('Can not find an existing communication settings custom setting');
            }
        } else {
            System.debug('Disabling SMS in custom settings');
            commSettings.SMS_Sending_Enabled__c = false;
            commSettings.Free_Text_Banned_Profiles__c = getListOfProfileIds(new List<String>{'Transport & Logistics - Planning & Dispatch'});
            upsert commSettings;
        }
    }

    public static void updateCommunityPageLinks(Boolean abortOnError){
        Map<String, CommunityPageLinks__c> pageLinks = CommunityPageLinks__c.getAll();
        if (pageLinks == null) {
            if (abortOnError) {
                throw new ExistingSandboxException('Can not find an existing CommunityPageLinks__c custom setting.');
            }
        } else {
            for (String key : pageLinks.keySet()) {
                List<String> documentURLLInks = new List<String>{'Quality_PDF', 'Ritchies_TnC', 'Woolies_TnC'};
                CommunityPageLinks__c setting = pageLinks.get(key);
                if(documentURLLInks.contains(setting.Name)){
                    setting.URL__c = 'document-setup-required';
                }
            }
            update pageLinks.values();
        }
    }

    public static void updateFieldTeamToolsCustomSettings(Boolean abortOnError){
        Map<String, String> accountNameSettingNameMap = new Map<String, String>{
            'No Entity AU' => 'IndividualDefaultAccountAU'
        };

        Map<String, Account> nameAccountMap = new Map<String, Account>();
        for (Account a : [SELECT Id, Name FROM Account WHERE RecordType.DeveloperName = 'Entity']) {
            nameAccountMap.put(a.Name, a);
        }

        List<Account> newAccounts = new List<Account>();
        for (String s : accountNameSettingNameMap.keySet()) {
            if (!nameAccountMap.containsKey(s)) {
                Account a = new Account(
                    Name = s,
                    RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Entity').getRecordTypeId()
                );
                newAccounts.add(a);
                nameAccountMap.put(a.Name, a);
            }
        }
        insert newAccounts;

        for (String s : accountNameSettingNameMap.keySet()) {
            Field_Team_Tools_Settings__c defaultAccountSetting = Field_Team_Tools_Settings__c.getValues(accountNameSettingNameMap.get(s));

            if (defaultAccountSetting == null) {
                if (abortOnError) {
                    throw new ExistingSandboxException('Can not find an existing ' + s + ' field team tools custom setting.');
                } else {

                    Field_Team_Tools_Settings__c newSetting = new Field_Team_Tools_Settings__c();
                    newSetting.Name = accountNameSettingNameMap.get(s);
                    newSetting.Value__c = nameAccountMap.get(s).Id;
                    insert newSetting;
                }
            } else {
                defaultAccountSetting.Value__c = nameAccountMap.get(s).Id;
                update defaultAccountSetting;
            }
        }

        Field_Team_Tools_Settings__c accountFarmFieldId = Field_Team_Tools_Settings__c.getValues('AccountFarmFieldId');
        if (accountFarmFieldId == null) {
            if (abortOnError) {
                throw new ExistingSandboxException('Can not find an existing AccountFarmFieldId custom setting.');
            }
        } else {
            Map<String, String> communityFieldIdMap = GlobalUtility.getFieldIdsForSObject('Collection__c');
            accountFarmFieldId.Value__c = communityFieldIdMap.get('Farm_ID');
            update accountFarmFieldId;
        }

        Field_Team_Tools_Settings__c advanceAccountFarmFieldId = Field_Team_Tools_Settings__c.getValues('AdvanceAccountFarmFieldId');
        if (advanceAccountFarmFieldId == null) {
            if (abortOnError) {
                throw new ExistingSandboxException('Can not find an existing AdvanceAccountFarmFieldId custom setting.');
            }
        } else {
            Map<String, String> advanceFieldIdMap = GlobalUtility.getFieldIdsForSObject('Advance__c');
            advanceAccountFarmFieldId.Value__c = advanceFieldIdMap.get('Farm');
            update advanceAccountFarmFieldId;
        }

        Field_Team_Tools_Settings__c batchErrorReceiverEmail = Field_Team_Tools_Settings__c.getValues('BatchErrorReceiverEmail');
        if (batchErrorReceiverEmail == null) {
            if (abortOnError) {
                throw new ExistingSandboxException('Can not find an existing BatchErrorReceiverEmail custom setting.');
            }
        } else {
            batchErrorReceiverEmail.Value__c = UserInfo.getUserEmail();
            update batchErrorReceiverEmail;
        }
    }

    public static void updateServiceCentreSettings(Boolean abortOnError){
        // Disable Send/Pull
        Service_Centre_Settings__c serviceSettings = Service_Centre_Settings__c.getOrgDefaults();
        if (serviceSettings == null) {
            if (abortOnError) {
                throw new ExistingSandboxException('Can not find an existing service centre settings custom setting.');
            }
        } else {
            System.debug('Updating Service Centre custom settings');

            Map<String, String> caseFieldIdMap = GlobalUtility.getFieldIdsForSObject('Case');
            serviceSettings.At_Risk_Farmer_Flag_Icon_ID__c = 'document-setup-required';
            serviceSettings.Raised_By_Case_External_Profiles__c = getListOfProfileIds(new List<String>{'Supplier Service Centre - Super User', 'Supplier Service Centre - Agent'});
            upsert serviceSettings;
        }
    }

    public static void updateAUPOSettingsSettings(Boolean abortOnError){
        AMS_PO_Settings__c serviceSettings = AMS_PO_Settings__c.getOrgDefaults();
        if (serviceSettings == null) {
            if (abortOnError) {
                throw new ExistingSandboxException('Can not find an AMS_PO_Settings__c custom setting.');
            }
        } else {
            System.debug('Updating AMS_PO_Settings__c custom settings');

            Map<String, String> caseFieldIdMap = GlobalUtility.getFieldIdsForSObject('Case');
            serviceSettings.SAP_PO_Statements_Username__c = 'I_SF_FARMERCENTL_XD2';
            serviceSettings.SAP_PO_Statements_Password__c = 'Farm3rC3n#xd2';
            serviceSettings.SAP_PO_Statements_Endpoint_URL__c = 'https://dev.integration.test.fonterra.com/RESTAdapter/MilkCollectionStatementByElementsQueryResponse';
            serviceSettings.TCC_Statements_Available_After__c = Date.newInstance(2017,8,7);
            upsert serviceSettings;
        }
    }

    public static void createChannelPreferenceSettings(){
        List<Channel_Preference_Setting__c> channelPreferenceSettings = new List<Channel_Preference_Setting__c>{
            new Channel_Preference_Setting__c(Name = 'Milk Quality Alert (AU)', RecordTypeId = GlobalUtility.australiaChannelPreferenceSettingRecordTypeId, Active__c = true, CME_Message_Type_Id__c = 'AMQA', DeliveryByEmail__c = 'Enabled', Exact_Target_Message_Id__c = 'Mzo3ODow', Exact_Target_Message_Keyword__c = 'TEST', Exact_Target_Time_Zone__c = 'Australia/Melbourne', Exact_Target_Window_End__c = '800', Exact_Target_Window_Start__c = '1900', DeliveryMail__c = 'Disabled', NotifyBySMS__c = 'Enabled', Type__c = 'Farm'),
            new Channel_Preference_Setting__c(Name = 'Daily Milk Result (AU)', RecordTypeId = GlobalUtility.australiaChannelPreferenceSettingRecordTypeId, Active__c = true, CME_Message_Type_Id__c = 'ADMR', DeliveryByEmail__c = 'Enabled', Exact_Target_Message_Id__c = 'MTo3ODow', Exact_Target_Message_Keyword__c = 'TEST', Exact_Target_Time_Zone__c = 'Australia/Melbourne', Exact_Target_Window_End__c = '800', Exact_Target_Window_Start__c = '1900', DeliveryMail__c = 'Disabled', NotifyBySMS__c = 'Enabled', Type__c = 'Farm'),
            new Channel_Preference_Setting__c(Name = 'Statement Notification (AU)', RecordTypeId = GlobalUtility.australiaChannelPreferenceSettingRecordTypeId, Active__c = true, CME_Message_Type_Id__c = 'ASN', DeliveryByEmail__c = 'Enabled', Exact_Target_Message_Id__c = 'NDo3ODow', Exact_Target_Message_Keyword__c = 'DEFAULT', Exact_Target_Time_Zone__c = 'Australia/Melbourne', Exact_Target_Window_End__c = '800', Exact_Target_Window_Start__c = '1900', DeliveryMail__c = 'Disabled', NotifyBySMS__c = 'Enabled', Type__c = 'Farm'),
            new Channel_Preference_Setting__c(Name = 'Marketing Communications', RecordTypeId = GlobalUtility.australiaChannelPreferenceSettingRecordTypeId, Active__c = true, CME_Message_Type_Id__c = 'n/a', DeliveryByEmail__c = 'Default Enabled', Exact_Target_Message_Id__c = '', Exact_Target_Message_Keyword__c = '', Exact_Target_Time_Zone__c = '', Exact_Target_Window_End__c = '', Exact_Target_Window_Start__c = '', DeliveryMail__c = 'Disabled', NotifyBySMS__c = 'Default Enabled', Type__c = 'Personal')
        };

        upsert channelPreferenceSettings CME_Message_Type_Id__c;
    }

    public static void createPublicContacts(){
        Account noEntityAUAccount = [SELECT Id, Name FROM Account WHERE Name = 'No Entity AU'];

        insert new List<Contact>{
            new Contact(FirstName = 'AU Public', LastName = 'Tanker Feedback', RecordTypeId = GlobalUtility.individualAURecordTypeId, AccountId = noEntityAUAccount.Id, Contact_Instructions__c = 'PLEASE CREATE CASES. DO NOT MAKE NOTES IN THE FEED - THEY ARE NOT FOLLOWED UP.', Phone = '+61 99 9999999', Type__c = 'Personal'),
            new Contact(FirstName = 'AU Public', LastName = 'Environmental Feedback', RecordTypeId = GlobalUtility.individualAURecordTypeId, AccountId = noEntityAUAccount.Id, Contact_Instructions__c = 'PLEASE CREATE CASES. DO NOT MAKE NOTES IN THE FEED - THEY ARE NOT FOLLOWED UP.', Phone = '+61 99 9999999', Type__c = 'Personal'),
            new Contact(FirstName = 'AU Public', LastName = 'Farmsource', RecordTypeId = GlobalUtility.individualAURecordTypeId, AccountId = noEntityAUAccount.Id, Contact_Instructions__c = 'PLEASE CREATE CASES. DO NOT MAKE NOTES IN THE FEED - THEY ARE NOT FOLLOWED UP.', Phone = '+61 99 9999999', Type__c = 'Personal'),
            new Contact(FirstName = 'AU Public', LastName = 'Road', RecordTypeId = GlobalUtility.individualAURecordTypeId, AccountId = noEntityAUAccount.Id, Contact_Instructions__c = 'PLEASE CREATE CASES. DO NOT MAKE NOTES IN THE FEED - THEY ARE NOT FOLLOWED UP.', Phone = '+61 99 9999999', Type__c = 'Personal'),
            new Contact(FirstName = 'AU Public', LastName = 'Inquiries', RecordTypeId = GlobalUtility.individualAURecordTypeId, AccountId = noEntityAUAccount.Id, Contact_Instructions__c = 'PLEASE CREATE CASES. DO NOT MAKE NOTES IN THE FEED - THEY ARE NOT FOLLOWED UP.', Phone = '+61 99 9999999', Type__c = 'Personal'),
            new Contact(FirstName = 'AU Public', LastName = 'Animal Welfare', RecordTypeId = GlobalUtility.individualAURecordTypeId, AccountId = noEntityAUAccount.Id, Contact_Instructions__c = 'PLEASE CREATE CASES. DO NOT MAKE NOTES IN THE FEED - THEY ARE NOT FOLLOWED UP.', Phone = '+61 99 9999999', Type__c = 'Personal')
        };
    }

    public static void createReferencePeriods(){
        insert new List<Reference_Period__c>{
            new Reference_Period__c(Name = 'Milking Season 2022/2023', End_Date__c = Date.newInstance(2023,6,30), Reference_Period__c = '2022/2023', Start_Date__c = Date.newInstance(2022,7,1), Status__c = 'Future', Type__c = 'Milking Season AU', Unique_Reference_Period_Name__c = 'Milking Season AU 2022/2023'),
            new Reference_Period__c(Name = 'Financial Year 1992/1993', End_Date__c = Date.newInstance(1993,7,31), Reference_Period__c = '1992/1993', Start_Date__c = Date.newInstance(1992,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 1992/1993'),
            new Reference_Period__c(Name = 'Financial Year 1993/1994', End_Date__c = Date.newInstance(1994,7,31), Reference_Period__c = '1993/1994', Start_Date__c = Date.newInstance(1993,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 1993/1994'),
            new Reference_Period__c(Name = 'Financial Year 1994/1995', End_Date__c = Date.newInstance(1995,7,31), Reference_Period__c = '1994/1995', Start_Date__c = Date.newInstance(1994,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 1994/1995'),
            new Reference_Period__c(Name = 'Financial Year 1995/1996', End_Date__c = Date.newInstance(1996,7,31), Reference_Period__c = '1995/1996', Start_Date__c = Date.newInstance(1995,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 1995/1996'),
            new Reference_Period__c(Name = 'Financial Year 1996/1997', End_Date__c = Date.newInstance(1997,7,31), Reference_Period__c = '1996/1997', Start_Date__c = Date.newInstance(1996,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 1996/1997'),
            new Reference_Period__c(Name = 'Financial Year 1997/1998', End_Date__c = Date.newInstance(1998,7,31), Reference_Period__c = '1997/1998', Start_Date__c = Date.newInstance(1997,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 1997/1998'),
            new Reference_Period__c(Name = 'Financial Year 1998/1999', End_Date__c = Date.newInstance(1999,7,31), Reference_Period__c = '1998/1999', Start_Date__c = Date.newInstance(1998,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 1998/1999'),
            new Reference_Period__c(Name = 'Financial Year 1999/2000', End_Date__c = Date.newInstance(2000,7,31), Reference_Period__c = '1999/2000', Start_Date__c = Date.newInstance(1999,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 1999/2000'),
            new Reference_Period__c(Name = 'Financial Year 2000/2001', End_Date__c = Date.newInstance(2001,7,31), Reference_Period__c = '2000/2001', Start_Date__c = Date.newInstance(2000,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2000/2001'),
            new Reference_Period__c(Name = 'Financial Year 2001/2002', End_Date__c = Date.newInstance(2002,7,31), Reference_Period__c = '2001/2002', Start_Date__c = Date.newInstance(2001,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2001/2002'),
            new Reference_Period__c(Name = 'Financial Year 2002/2003', End_Date__c = Date.newInstance(2003,7,31), Reference_Period__c = '2002/2003', Start_Date__c = Date.newInstance(2002,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2002/2003'),
            new Reference_Period__c(Name = 'Financial Year 2003/2004', End_Date__c = Date.newInstance(2004,7,31), Reference_Period__c = '2003/2004', Start_Date__c = Date.newInstance(2003,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2003/2004'),
            new Reference_Period__c(Name = 'Financial Year 2004/2005', End_Date__c = Date.newInstance(2005,7,31), Reference_Period__c = '2004/2005', Start_Date__c = Date.newInstance(2004,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2004/2005'),
            new Reference_Period__c(Name = 'Financial Year 2005/2006', End_Date__c = Date.newInstance(2006,7,31), Reference_Period__c = '2005/2006', Start_Date__c = Date.newInstance(2005,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2005/2006'),
            new Reference_Period__c(Name = 'Financial Year 2006/2007', End_Date__c = Date.newInstance(2007,7,31), Reference_Period__c = '2006/2007', Start_Date__c = Date.newInstance(2006,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2006/2007'),
            new Reference_Period__c(Name = 'Financial Year 2007/2008', End_Date__c = Date.newInstance(2008,7,31), Reference_Period__c = '2007/2008', Start_Date__c = Date.newInstance(2007,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2007/2008'),
            new Reference_Period__c(Name = 'Financial Year 2008/2009', End_Date__c = Date.newInstance(1999,7,31), Reference_Period__c = '2008/2009', Start_Date__c = Date.newInstance(2008,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2008/2009'),
            new Reference_Period__c(Name = 'Financial Year 2009/2010', End_Date__c = Date.newInstance(2010,7,31), Reference_Period__c = '2009/2010', Start_Date__c = Date.newInstance(1999,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2009/2010'),
            new Reference_Period__c(Name = 'Financial Year 2010/2011', End_Date__c = Date.newInstance(2011,7,31), Reference_Period__c = '2010/2011', Start_Date__c = Date.newInstance(2010,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2010/2011'),
            new Reference_Period__c(Name = 'Financial Year 2011/2012', End_Date__c = Date.newInstance(2012,7,31), Reference_Period__c = '2011/2012', Start_Date__c = Date.newInstance(2011,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2011/2012'),
            new Reference_Period__c(Name = 'Financial Year 2012/2013', End_Date__c = Date.newInstance(2013,7,31), Reference_Period__c = '2012/2013', Start_Date__c = Date.newInstance(2012,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2012/2013'),
            new Reference_Period__c(Name = 'Financial Year 2013/2014', End_Date__c = Date.newInstance(2014,7,31), Reference_Period__c = '2013/2014', Start_Date__c = Date.newInstance(2013,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2013/2014'),
            new Reference_Period__c(Name = 'Financial Year 2014/2015', End_Date__c = Date.newInstance(2015,7,31), Reference_Period__c = '2014/2015', Start_Date__c = Date.newInstance(2014,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2014/2015'),
            new Reference_Period__c(Name = 'Financial Year 2015/2016', End_Date__c = Date.newInstance(2016,7,31), Reference_Period__c = '2015/2016', Start_Date__c = Date.newInstance(2015,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2015/2016'),
            new Reference_Period__c(Name = 'Financial Year 2016/2017', End_Date__c = Date.newInstance(2017,7,31), Reference_Period__c = '2016/2017', Start_Date__c = Date.newInstance(2016,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2016/2017'),
            new Reference_Period__c(Name = 'Financial Year 2017/2018', End_Date__c = Date.newInstance(2018,7,31), Reference_Period__c = '2017/2018', Start_Date__c = Date.newInstance(2017,8,1), Status__c = 'Past', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2017/2018'),
            new Reference_Period__c(Name = 'Financial Year 2018/2019', End_Date__c = Date.newInstance(2019,7,31), Reference_Period__c = '2018/2019', Start_Date__c = Date.newInstance(2018,8,1), Status__c = 'Current', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2018/2019'),
            new Reference_Period__c(Name = 'Financial Year 2019/2020', End_Date__c = Date.newInstance(2020,7,31), Reference_Period__c = '2019/2020', Start_Date__c = Date.newInstance(2019,8,1), Status__c = 'Future', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2019/2020'),
            new Reference_Period__c(Name = 'Financial Year 2020/2021', End_Date__c = Date.newInstance(2021,7,31), Reference_Period__c = '2020/2021', Start_Date__c = Date.newInstance(2020,8,1), Status__c = 'Future', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2020/2021'),
            new Reference_Period__c(Name = 'Financial Year 2021/2022', End_Date__c = Date.newInstance(2022,7,31), Reference_Period__c = '2021/2022', Start_Date__c = Date.newInstance(2021,8,1), Status__c = 'Future', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2021/2022'),
            new Reference_Period__c(Name = 'Financial Year 2022/2023', End_Date__c = Date.newInstance(2023,7,31), Reference_Period__c = '2022/2023', Start_Date__c = Date.newInstance(2022,8,1), Status__c = 'Future', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2022/2023'),
            new Reference_Period__c(Name = 'Financial Year 2023/2024', End_Date__c = Date.newInstance(2024,7,31), Reference_Period__c = '2023/2024', Start_Date__c = Date.newInstance(2023,8,1), Status__c = 'Future', Type__c = 'Financial Year', Unique_Reference_Period_Name__c = 'Financial Year 2023/2024'),
            new Reference_Period__c(Name = 'Milking Season 2014/2015', End_Date__c = Date.newInstance(2015,6,30), Reference_Period__c = '2014/2015', Start_Date__c = Date.newInstance(2014,7,1), Status__c = 'Past', Type__c = 'Milking Season AU', Unique_Reference_Period_Name__c = 'Milking Season AU 2014/2015'),
            new Reference_Period__c(Name = 'Milking Season 2015/2016', End_Date__c = Date.newInstance(2016,6,30), Reference_Period__c = '2015/2016', Start_Date__c = Date.newInstance(2015,7,1), Status__c = 'Past', Type__c = 'Milking Season AU', Unique_Reference_Period_Name__c = 'Milking Season AU 2015/2016'),
            new Reference_Period__c(Name = 'Milking Season 2016/2017', End_Date__c = Date.newInstance(2017,6,30), Reference_Period__c = '2016/2017', Start_Date__c = Date.newInstance(2016,7,1), Status__c = 'Past', Type__c = 'Milking Season AU', Unique_Reference_Period_Name__c = 'Milking Season AU 2016/2017'),
            new Reference_Period__c(Name = 'Milking Season 2017/2018', End_Date__c = Date.newInstance(2018,6,30), Reference_Period__c = '2017/2018', Start_Date__c = Date.newInstance(2017,7,1), Status__c = 'Past', Type__c = 'Milking Season AU', Unique_Reference_Period_Name__c = 'Milking Season AU 2017/2018'),
            new Reference_Period__c(Name = 'Milking Season 2018/2019', End_Date__c = Date.newInstance(2019,6,30), Reference_Period__c = '2018/2019', Start_Date__c = Date.newInstance(2018,7,1), Status__c = 'Current', Type__c = 'Milking Season AU', Unique_Reference_Period_Name__c = 'Milking Season AU 2018/2019'),
            new Reference_Period__c(Name = 'Milking Season 2019/2020', End_Date__c = Date.newInstance(2020,6,30), Reference_Period__c = '2019/2020', Start_Date__c = Date.newInstance(2019,7,1), Status__c = 'Future', Type__c = 'Milking Season AU', Unique_Reference_Period_Name__c = 'Milking Season AU 2019/2020'),
            new Reference_Period__c(Name = 'Milking Season 2020/2021', End_Date__c = Date.newInstance(2021,6,30), Reference_Period__c = '2020/2021', Start_Date__c = Date.newInstance(2020,7,1), Status__c = 'Future', Type__c = 'Milking Season AU', Unique_Reference_Period_Name__c = 'Milking Season AU 2020/2021'),
            new Reference_Period__c(Name = 'Milking Season 2021/2022', End_Date__c = Date.newInstance(2022,6,30), Reference_Period__c = '2021/2022', Start_Date__c = Date.newInstance(2021,7,1), Status__c = 'Future', Type__c = 'Milking Season AU', Unique_Reference_Period_Name__c = 'Milking Season AU 2021/2022')
        };
    }

    public static void createReferenceRegions() {
        insert new List<Reference_Region__c>{
            new Reference_Region__c(Name = 'Australia', Reference_Region_ID__c = 'AUSTRALIA', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'Canada', Reference_Region_ID__c = 'CAN', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'Chile', Reference_Region_ID__c = 'CHILE', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'China', Reference_Region_ID__c = 'CHINA', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'Fiji', Reference_Region_ID__c = 'FIJI', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'Germany', Reference_Region_ID__c = 'GERMANY', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'Ireland', Reference_Region_ID__c = 'IRELAND', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'Netherlands', Reference_Region_ID__c = 'NETHERLANDS', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'New Zealand', Reference_Region_ID__c = 'NZ', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'Singapore', Reference_Region_ID__c = 'SINGAPORE', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'South Africa', Reference_Region_ID__c = 'STH AFRICA', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'Switzerland', Reference_Region_ID__c = 'SWITZERLAND', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'England', Reference_Region_ID__c = 'UK', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1)),
            new Reference_Region__c(Name = 'U.S.A.', Reference_Region_ID__c = 'USA', Type__c = 'Country', Valid_From__c = Date.newInstance(2018,8,1))
        };
    }

    private static void resetSFMarketingCloudConfiguration(){
        // This will disconnect only the Marketing Cloud Connect product and remove tokens and configurations from your Salesforce sandbox environment.
        et4ae5.SupportUtilities.resetConfiguration();
    }
}