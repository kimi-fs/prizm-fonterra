/**
 * Testing agreement services
 *
 * @author Ed (Trineo)
 * @date   May 2020
 */
@isTest
private class AgreementServiceTest {
    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup
    private static void userSetup() {
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        // One Party
        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        // 3 Farms
        List<Account> farms = TestClassDataUtil.createAUFarms(3, false);
        for (Account farm : farms) {
            farm.Name = 'AgreementTest' + farm.Name;
        }
        insert farms;

        // Connect the party with the 3 farms by ERs
        List<Entity_Relationship__c> entityRelationships = TestClassDataUtil.createPartyFarmEntityRelationships(new Map<Account, List<Account>>{party => farms}, true);

        // Connect the Farmer with the Party - which will create derived IRs between the individual and the farms.
        List<Individual_Relationship__c> individualRelationships = TestClassDataUtil.createIndividualPartyRelationships(getCommunityUser().ContactId, new List<Account>{party}, true);

        List<Agreement__c> agreements = new List<Agreement__c>();
        agreements.add(AgreementService.createPendingOnlineStandardAgreement(farms[0].Id, party.Id, Date.today().addMonths(1), Date.today().addMonths(6)));
        agreements.add(AgreementService.createPendingOnlineStandardAgreement(farms[1].Id, party.Id, Date.today().addMonths(1), Date.today().addMonths(6)));
        agreements.add(AgreementService.createPendingOnlineStandardAgreement(farms[2].Id, party.Id, Date.today().addMonths(1), Date.today().addMonths(6)));
        insert agreements;

    }

    @isTest
    private static void testFetchingAgreements() {
        List<Account> farms = [SELECT Id, Name FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId AND Name LIKE 'AgreementTest%'];
        List<Account> parties = [SELECT Id FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountPartyRecordTypeId];

        System.assertEquals(3, farms.size(), 'Should be 3 farms: ' + farms);
        System.assertEquals(1, parties.size(), 'Should be 1 party: ' + parties);

        Test.startTest();
        System.runAs(getCommunityUser()) {
            List<Id> agreementIDs = new List<Id>();
            List<Agreement__c> agreements;
            agreements = AgreementService.getAgreements(farms[0].Id, parties[0].Id, Date.today().addMonths(1), Date.today().addMonths(6), true);
            System.assertEquals(1, agreements.size());
            agreementIDs.add(agreements[0].Id);
            agreements = AgreementService.getAgreements(farms[1].Id, parties[0].Id, Date.today().addMonths(1), Date.today().addMonths(6), true);
            System.assertEquals(1, agreements.size());
            agreementIDs.add(agreements[0].Id);
            agreements = AgreementService.getAgreements(farms[2].Id, parties[0].Id, Date.today().addMonths(1), Date.today().addMonths(6), true);
            System.assertEquals(1, agreements.size());
            agreementIDs.add(agreements[0].Id);

            List<Agreement__c> agreementsRefetch = AgreementService.getAgreements(agreementIDs);

            System.assertEquals(3, agreementsRefetch.size());

            for (Agreement__c agreement : agreementsRefetch) {
                System.assertNotEquals(null, AgreementService.getAgreement(agreement.Id));
            }
        }
        Test.stopTest();
    }

    @isTest
    private static void testDocumentLinks() {
        List<Account> farms = [SELECT Id, Name FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId AND Name LIKE 'AgreementTest%'];
        List<Account> parties = [SELECT Id FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountPartyRecordTypeId];

        System.assertEquals(3, farms.size(), 'Should be 3 farms: ' + farms);
        System.assertEquals(1, parties.size(), 'Should be 1 party: ' + parties);

         /**
         * Since SF doesn't allow insert dml on ContentDocument, we're creating it via
         * ContentVersion without ContentDocumentId
         */
        ContentVersion pdfContentDocument = new ContentVersion(
            Title = 'Exclusive-Standard-Supply-Agreement-000001',
            PathOnClient = 'Exclusive-Standard-Supply-Agreement-000001_V1.pdf',
            VersionData = Blob.valueOf('PDF Content'),
            IsMajorVersion = true
        );

        insert pdfContentDocument;

        List<ContentDocument> documents = [SELECT Id, Title FROM ContentDocument];

        List<Agreement__c> agreements = AgreementService.getAgreements(farms[0].Id, parties[0].Id, Date.today().addMonths(1), Date.today().addMonths(6), true);

        // Create a link between this document and our Agreement
        ContentDocumentLink agreementContentDocumentLink = new ContentDocumentLink(
            ContentDocumentId = documents[0].Id,
            LinkedEntityId = agreements[0].Id,
            ShareType = 'V',
            Visibility = 'AllUsers'
        );
        ContentDocumentLink partyContentDocumentLink = new ContentDocumentLink(
            ContentDocumentId = documents[0].Id,
            LinkedEntityId = parties[0].Id,
            ShareType = 'I',
            Visibility = 'AllUsers'
        );

        insert agreementContentDocumentLink;
        insert partyContentDocumentLink;

        User integrationUser = TestClassDataUtil.createUserByProfile('External System Integration User', 'Conga User Integration');
        TestClassDataUtil.createFileSharingSetting('Exclusive-Standard-Supply-Agreement-', 'V', '', '', 'SEND_EMAIL_CREATE_CASE', true);

        insert integrationUser;

        System.debug(integrationUser);


        List<Case> cases = [
            SELECT Id
            FROM Case
            WHERE Type = :CaseService.CASE_TYPE_AGREEMENTS
        ];
        System.assertEquals(0, cases.size());

        System.runAs(integrationUser) {
            Test.startTest();

            agreements[0].Agreement_Status__c = AgreementService.AGREEMENT_STATUS_SUBMITTED;
            agreements[0].User_Signed__c = getCommunityUser().Id;
            agreements[0].Execution_Date__c = Date.today();
            System.debug('emailtest 1: ' + agreements[0]);

            AgreementService.updateAgreement(agreements[0]);

            AgreementService.updateAgreementsFromDocumentLinks(new List<ContentDocumentLink>{agreementContentDocumentLink, partyContentDocumentLink});

            System.debug('emailtest 2: ' + agreements[0]);

            Agreement__c agreement = AgreementService.getAgreement(agreements[0].Id);
            System.debug('emailtest 3: ' + agreement);

            System.assertEquals(AgreementService.AGREEMENT_STATUS_EXECUTED, agreement.Agreement_Status__c, 'Agreement should be executed');

            AgreementService.sendEmailAndCreateCaseForSignedAgreement(new List<ContentDocumentLink>{agreementContentDocumentLink, partyContentDocumentLink});
            // The above will go into a future. We should have a Case created
            Test.stopTest();

        }

        cases = [
            SELECT Id, Description, Raised_By__c, Origin
            FROM Case
            WHERE Type = :CaseService.CASE_TYPE_AGREEMENTS
        ];
        System.debug('Cases: ' + cases);
        System.assertEquals(1, cases.size());
        Case createdCase = cases[0];
        System.assert(createdCase.Description.contains(agreements[0].Name), 'Missing agreement case');
        System.assertEquals('External', createdCase.Raised_By__c);
        System.assertEquals('Web', createdCase.Origin);
    }

    private static User getCommunityUser() {
        User theUser = [
                           SELECT Id, ContactId
                           FROM User
                           WHERE Email = :USER_EMAIL
                       ];

        return theUser;
    }
}