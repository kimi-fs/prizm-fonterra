public class EmailUnsubscribeController {
    public List<Channel_Preference__c> relatedChannelPreferences {get; set;}

    //Actions
    public Boolean cancelParam {get; set;}
    public Boolean successParam {get; set;}
    public Boolean noParam {get; set;} //If there is no "success" or "cancel" param then we just display the first page.

    //Parameters
    public String unsubscribeKey {get; set;}
    public String relatedEmailAddress {get; set;}
    public String relatedCommunicationName {get; set;}

    //url
    public String homeURL {get; set;}
    public Boolean isIndividualAU {get; set;}

    public EmailUnsubscribeController() {
        cancelParam = (System.currentPageReference().getParameters().get('cancel') == 'true') ? true : false;
        successParam = (System.currentPageReference().getParameters().get('success') == 'true') ? true : false;

        //Retrieve the unsubscribe key which is fetched via the url parameter, linked generated from Marketing Cloud
        this.unsubscribeKey = System.currentPageReference().getParameters().get('id');

        if (this.unsubscribeKey != null) {
            Channel_Preference__c matchedChannelPreference = [SELECT
                Id,
                Contact_ID__r.Email,
                Channel_Preference_Setting__c,
                Channel_Preference_Setting__r.Name,
                Contact_ID__r.RecordTypeId
                FROM Channel_Preference__c
                WHERE Unsubscribe_Key__c =: this.unsubscribeKey];


            noParam = (!cancelParam && !successParam && matchedChannelPreference != null) ? true : false;

            if (matchedChannelPreference != null) {
                this.relatedEmailAddress = matchedChannelPreference.Contact_ID__r.Email;
                this.relatedCommunicationName = matchedChannelPreference.Channel_Preference_Setting__r.Name;

                //Retrieve all Channel Preference records which are related to this email address & channel preference setting.
                this.relatedChannelPreferences = [SELECT Id FROM Channel_Preference__c WHERE Channel_Preference_Setting__c =: matchedChannelPreference.Channel_Preference_Setting__c AND Contact_ID__r.Email =: this.relatedEmailAddress];

                if (matchedChannelPreference.Contact_ID__r.RecordTypeId == GlobalUtility.individualAURecordTypeId) {
                    setAUDefaults(matchedChannelPreference);
                }
            }
        }
    }

    //Unsubscribe all Channel Preference records which have matched for this email address.
    public PageReference confirmUnsubscribe() {
        List<Channel_Preference__c> forUpdateList = new List<Channel_Preference__c>();

        if (this.relatedChannelPreferences != null) {
            for (Channel_Preference__c cp : this.relatedChannelPreferences) {
                Channel_Preference__c forUpdate = new Channel_Preference__c(Id = cp.Id);
                forUpdate.DeliveryByEmail__c = false;

                forUpdateList.add(forUpdate);
            }
        }

        try {
            update forUpdateList;
        } catch (Exception e) {
            Logger.log('Email Unsubscribe Error', GlobalUtility.Stringify.jsonifyException(e), Logger.LogType.ERROR);
            Logger.saveLog();
        }

        PageReference confirmPage = Page.EmailUnsubscribe;
        confirmPage.getParameters().put('id', this.unsubscribeKey);
        confirmPage.getParameters().put('success', 'true');
        confirmPage.setRedirect(true);
        return confirmPage;
    }

    public PageReference cancel() {
        PageReference cancelPage = Page.EmailUnsubscribe;
        cancelPage.getParameters().put('id', this.unsubscribeKey);
        cancelPage.getParameters().put('cancel', 'true');
        cancelPage.setRedirect(true);
        return cancelPage;
    }

    private void setAUDefaults(Channel_Preference__c matchedChannelPreference) {
        isIndividualAU = true;
        if (matchedChannelPreference.Channel_Preference_Setting__r.Name.contains('Communications')) {
            String removeComs = matchedChannelPreference.Channel_Preference_Setting__r.Name.substring(
                0,
                matchedChannelPreference.Channel_Preference_Setting__r.Name.indexOf('Communications'));

            this.relatedCommunicationName = removeComs;
        }

        this.homeURL = Site.getBaseUrl();
    }

}