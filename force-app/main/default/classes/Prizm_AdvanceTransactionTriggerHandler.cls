public class Prizm_AdvanceTransactionTriggerHandler {
    public static Map<Id,fsServ__Contract_Customer__c> mContractToCustomerMap;
    public static void createContractDisbursementRecord(List<Advance_Transaction__c>pNewAdvanceTrnxs,Map<Id,Advance_Transaction__c> pOldAdvanceTrnxMap){
        Id recTypeId = Schema.SObjectType.Advance_Transaction__c.getRecordTypeInfosByDeveloperName().get('Disbursement').getRecordTypeId();
      
        Map<Id,Advance_Transaction__c> contractIdToAdvTrnxMap = new Map<Id,Advance_Transaction__c>();
        for(Advance_Transaction__c advanceTrnx : pNewAdvanceTrnxs){
            if(advanceTrnx.RecordTypeId == recTypeId
               && advanceTrnx.Status__c == 'Processed'
               && advanceTrnx.Status__c <> pOldAdvanceTrnxMap.get(advanceTrnx.id).Status__c){
                 //  advanceTrnx.Processed_On__c = 
                   contractIdToAdvTrnxMap.put(advanceTrnx.Lending_Contract_Number__c,advanceTrnx);
               }
                    
            if(!contractIdToAdvTrnxMap.isEmpty()){
                mContractToCustomerMap = Prizm_LendingContractUtil.getContractIdToPrimaryCustomerMap(contractIdToAdvTrnxMap.keySet());
                createDisbursementRecs(contractIdToAdvTrnxMap);
                 activateContract(contractIdToAdvTrnxMap.keySet());
            }
            
        }
    }
    public static void createDisbursementRecs(Map<Id,Advance_Transaction__c> pContractIdToAdvTrnxMap){
        Id processsedRecTypeId = Schema.SObjectType.fsServ__Contract_Disbursement__c.getRecordTypeInfosByDeveloperName().get('Processed').getRecordTypeId();
        List<fsServ__Contract_Disbursement__c> conDisbursementRecs = new List<fsServ__Contract_Disbursement__c>();
        List<fsServ__Lending_Contract__c> contracts = Prizm_LendingContractUtil.getContracts(pContractIdToAdvTrnxMap.keySet());
        for(fsServ__Lending_Contract__c contract : contracts){
            if(pContractIdToAdvTrnxMap.containsKey(contract.id)){
                fsServ__Contract_Disbursement__c contractDisbursementRec = new fsServ__Contract_Disbursement__c();
                contractDisbursementRec.fsServ__Account_Name__c	=contract.fsServ__Primary_Customer_Account__c;			
                contractDisbursementRec.fsServ__Contact_Name__c	=contract.fsServ__Primary_Customer_Contact__c;	
                contractDisbursementRec.fsServ__Lending_Contract_Number__c	= contract.id;
                contractDisbursementRec.fsServ__Disbursement_Date__c =Prizm_CommonUtil.getBranches(new Set<String>{contract.fsServ__Branch_Name_Formula__c}).get(contract.fsServ__Branch_Name_Formula__c).fsCore__Business_Date__c; //pContractIdToAdvTrnxMap.get(contract.id).Processed_On__c;	
                contractDisbursementRec.fsServ__Processed_By__c	=  UserInfo.getUserId();
                contractDisbursementRec.fsServ__Disbursement_Amount__c	= pContractIdToAdvTrnxMap.get(contract.id).Amount__c;
                contractDisbursementRec.fsServ__Processed_On__c	= pContractIdToAdvTrnxMap.get(contract.id).Processed_On__c;	
                contractDisbursementRec.Advance_Transaction_Number__c = pContractIdToAdvTrnxMap.get(contract.id).id;
                contractDisbursementRec.fsServ__Processing_Status__c = 'Processed';
                contractDisbursementRec.RecordTypeId = processsedRecTypeId;
                if(mContractToCustomerMap.containsKey(contract.id)){
                contractDisbursementRec.fsServ__Customer_Reference_Number__c = mContractToCustomerMap.get(contract.id).id;//Lookup(Contract Customer)		
                }
                /*contractDisbursementRec.fsServ__Country__c	//Picklist		
contractDisbursementRec.fsServ__Is_Future_Disbursement__c	//Checkbox			
contractDisbursementRec.fsServ__Disbursement_Party_Type__c	//Picklist		
contractDisbursementRec.fsServ__Payment_Method__c	//Picklist		
contractDisbursementRec.fsServ__Is_Recalculate_Repayments__c	//Checkbox		
contractDisbursementRec.RecordTypeId	//Record Type		
contractDisbursementRec.fsServ__Reference__c	//Text(80)		
contractDisbursementRec.fsServ__Processing_Result__c	//Long Text Area(32000)		
contractDisbursementRec.fsServ__Source_Reference_Id__c	//Text(18)		
contractDisbursementRec.fsServ__State__c	//Picklist	
contractDisbursementRec.fsServ__Processing_Status__c	//Picklist	
contractDisbursementRec.fsServ__Street__c	//Text(80)			
contractDisbursementRec.fsServ__Suite__c	//Text(20)	
contractDisbursementRec.fsServ__Zip_Code__c	//Text(20)	
contractDisbursementRec.fsServ__Zip_Extension__c	//Text(20)
contractDisbursementRec.fsServ__Address_Number__c 	
contractDisbursementRec.fsServ__Bank_Account_Name__c	//Lookup(Bank Account)		
contractDisbursementRec.fsServ__City__c	//Text(80)
*/
                conDisbursementRecs.add(contractDisbursementRec);
            }
        }
        
        if(!conDisbursementRecs.isEmpty()){
            insert conDisbursementRecs;
        }
    }
    public static void activateContract(Set<Id> pContractId){
        fsCore.ActionInput actionIP = new fsCore.ActionInput();
        actionIP.addRecords(pContractId);
        System.debug(loggingLevel.Error,'actionIP'+actionIP);
        fsServ.ContractActivator activateAction = new fsServ.ContractActivator();
        activateAction.setInput(actionIP);
        activateAction.process();
        System.debug(loggingLevel.Error,'activateAction'+activateAction.getOutput());
    }
    public static void updateProcessedOnDate(List<Advance_Transaction__c>pNewAdvanceTrnxs,Map<Id,Advance_Transaction__c> pOldAdvanceTrnxMap){
       Map<Id,date> contractIdToBranchDate = getBranchDateMap(pNewAdvanceTrnxs);
        for(Advance_Transaction__c advTrnx :pNewAdvanceTrnxs){
            if(advTrnx.Status__c == 'Processed' &&
               advTrnx.Status__c <> pOldAdvanceTrnxMap.get(advTrnx.id).Status__c){
                   advTrnx.Processed_On__c = DateTime.newInstance(contractIdToBranchDate.get(advTrnx.Lending_Contract_Number__c),System.Now().Time());//fsServ__Branch_Name_Formula__c,fsServ__Lending_Contract__c
               }
        }
    }
    public static Map<Id,date> getBranchDateMap(List<Advance_Transaction__c>pNewAdvanceTrnxs){
        Map<Id,date> contractIdToBranchDate = new Map<Id,date>();
        Set<Id> contractIds = new Set<Id>();
        for(Advance_Transaction__c advTrnx :pNewAdvanceTrnxs){
            contractIds.add(advTrnx.Lending_Contract_Number__c);
        }
        List<fsServ__Lending_Contract__c> contracts = [Select Id,
                                                       Name, 
                                                       fsServ__Branch_Name__c,
                                                       fsServ__Branch_Name__r.fsCore__Business_Date__c
                                                       from fsServ__Lending_Contract__c
                                                       Where id In :contractIds];
        for(fsServ__Lending_Contract__c contract : contracts){
            contractIdToBranchDate.put(contract.id, contract.fsServ__Branch_Name__r.fsCore__Business_Date__c);
        }
        return contractIdToBranchDate;
    }
}