/**
 * Description: Test class for AMSUserPhotoService
 * @author: Amelia (Trineo)
 * @date: August 2018
 */
@isTest private class AMSUserPhotoServiceTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';

    @isTest(seeAlldata = true) static void testGetFullUserPhoto_DefaultUserPhoto() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(communityUser) {
            User u = AMSUserService.getUser();
            System.assertNotEquals(null, AMSUserPhotoService.getFullUserPhoto(u), 'Default image not returned');
        }
    }
    @isTest(seeAlldata = true) static void testGetSmallUserPhoto_DefaultUserPhoto() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(communityUser) {
            User u = AMSUserService.getUser();
            System.assertNotEquals(null, AMSUserPhotoService.getSmallUserPhoto(u), 'Default image not returned');
        }
    }

    @isTest(seeAlldata = true) static void testGetFullUserPhoto_DefaultUserPhoto_NoUser() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(communityUser) {
            System.assertNotEquals(null, AMSUserPhotoService.getFullUserPhoto(null), 'Default image not returned');
        }
    }
    @isTest(seeAlldata = true) static void testGetSmallUserPhoto_DefaultUserPhoto_NoUser() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(communityUser) {
            System.assertNotEquals(null, AMSUserPhotoService.getSmallUserPhoto(null), 'Default image not returned');
        }
    }

    @isTest(seeAlldata = true) static void testUploadUserPhoto() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(communityUser) {
            Blob document = Blob.valueOf('1');
            String fileName = 'test blob';
            try {
                AMSUserPhotoService.uploadUserPhoto(document, fileName, 'image/jpeg', communityUser);
            } catch (Exception e) {
                System.assertEquals('The file you uploaded doesn\'t appear to be a valid image.', e.getMessage());
            }
        }
    }

    @isTest(seeAlldata = true) static void testUploadUserPhoto_FalsePreference() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        List<Document> image = [SELECT Id, Body, ContentType FROM Document WHERE ContentType IN ('image/bmp', 'image/gif', 'image/jpeg', 'image/png') LIMIT 1];

        if (!image.isEmpty()) {
            System.runAs(communityUser) {
                AMSUserPhotoService.uploadUserPhoto(image[0].Body, 'test profile image', image[0].ContentType, communityUser);
            }
        }
    }

}