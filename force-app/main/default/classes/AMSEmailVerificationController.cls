/**
 * Description: Email verification page for AMS Community
 * @author: Amelia (Trineo)
 * @date: July 2017
 */
public with sharing class AMSEmailVerificationController {
    public String message { get; private set; }
    private static Map<String, String> messages;
    static {
        messages = ExceptionService.getPageMessages('AMSEmailVerification');
    }

    private String encryptedUsername;
    private Boolean emailSource;
    private User user;

    @testVisible
    private static final String AMS_COMMUNITY_NEW_MEMEBER_EMAIL_TEMPLATE_DEVELOPERNAME = 'AMS_Community_New_Member_Welcome_Email';

    private Boolean isLoggedIn = UserInfo.getUserType() != 'Guest';

    public AMSEmailVerificationController() {
        message = '';
    }

    private static String getUserIdParameterName() {
        return AMSCommunityUtil.getUserIdParameterName();
    }

    private String getEncryptedUsername() {
        if (this.encryptedUsername == null) {
            this.encryptedUsername = ApexPages.currentPage().getParameters().get(getUserIdParameterName());
        }

        return this.encryptedUsername;
    }

    @testVisible
    public String getUserEmailDomain() {
        String emailDomain = '';
        if (getUser() != null) {
            String userEmail = getUser().Email;
            emailDomain = AMSUserService.getEmailDomain(userEmail);
        }

        return emailDomain;
    }

    public void resendEmailVerification() {
        message = '';
        try {
            OrgWideEmailAddress owea = GlobalUtility.ORG_WIDE_EMAIL_ADDRESS_FARM_SOURCE_AU;
            Id orgWideEmailId = owea.Id;

            User targetUser = getUser();
            EmailTemplate emailTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = : AMS_COMMUNITY_NEW_MEMEBER_EMAIL_TEMPLATE_DEVELOPERNAME LIMIT 1];

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setTargetObjectId(targetUser.ContactId);
            email.setTemplateId(emailTemplate.Id);
            email.setSaveAsActivity(true);
            email.setOrgWideEmailAddressId(orgWideEmailId);
            List<Messaging.SendEmailResult> emailResults = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });

            if (!emailResults.isEmpty() && emailResults[0].isSuccess()) {
                message = messages.get('email-sent');
            } else {
                message = messages.get('unknown-error');
            }
        } catch (Exception e) {
            message = messages.get('unknown-error') + ' [' + e.getMessage() + ']';
        }
    }

    public User getUser() {
        if (this.user == null) {
            if (AMSUserService.checkUserLoggedIn()) {
                this.user = AMSUserService.getUser();
            } else if (getEncryptedUsername() != null) {
                this.user = AMSUserService.getUser(getEncryptedUsername());
            }
        }

        return this.user;
    }

    @testVisible
    private PageReference checkLogin() {
        if (this.isLoggedIn) {
            return Page.AMSLanding;
        } else {
            return null;
        }
    }
    private Boolean checkVerified() {
        return AMSUserService.checkVerified();
    }

    private Boolean checkVerified(String encryptedUsername) {
        return AMSUserService.checkVerified(encryptedUsername);
    }

    private Boolean checkIfUserHasAccess() {
        return this.isLoggedIn ? checkVerified() : false;
    }

    public PageReference redirectIfUserHasAccessAndLoggedIn() {
        return checkIfUserHasAccess() ? AMSUserService.firstTimeRegistrationNextPage(getUser()) : null;
    }

    @testVisible
    public PageReference setVerifiedEmail() {
        PageReference pageToReturn;
        pageToReturn = redirectIfUserHasAccessAndLoggedIn();

        if (pageToReturn == null) {
            if (getEncryptedUsername() != null) {
                Boolean verified = AMSUserService.updateEmailVerified(getEncryptedUsername());
                if (verified) {
                    if (this.isLoggedIn) {
                        pageToReturn = AMSUserService.firstTimeRegistrationNextPage(getUser());
                    } else {
                        PageReference loginPage = Page.AMSLogin;
                        loginPage.getParameters().put(AMSCommunityUtil.getStartURLParameterName(), AMSCommunityUtil.pageReferenceToCommunityFriendlyString(AMSUserService.firstTimeRegistrationNextPage(getUser())));
                        pageToReturn = loginPage;
                    }
                } else {
                    pageToReturn = null;
                }
            } else {
                pageToReturn = null;
            }
        }

        return pageToReturn;
    }
}