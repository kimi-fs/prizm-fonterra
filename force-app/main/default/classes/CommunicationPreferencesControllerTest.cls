@isTest
private class CommunicationPreferencesControllerTest {
    private static Id IREL_INDIVIDUALFARM_RECORDTYPE_ID = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Farm').getRecordTypeId();
    private static Id IREL_INDIVIDUALPARTY_RECORDTYPE_ID = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Party').getRecordTypeId();

    private static Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();

    @testSetup static void testSetup(){
        TestClassDataUtil.individualDefaultAccountAU();

        List<Channel_Preference_Setting__c> auChannelPreferenceSettings = TestClassDataUtil.createAUChannelPreferenceSettings();

        Contact individual = TestClassDataUtil.createIndividualAu(true);

        List<Reference_Region__c> auReferenceRegions = TestClassDataUtil.createAuReferenceRegions();
        List<Account> farms = TestClassDataUtil.createAuFarms(auReferenceRegions);
        insert farms;

        List<Channel_Preference__c> channelPreferences = new List<Channel_Preference__c>();
        for(Channel_Preference_Setting__c cps : auChannelPreferenceSettings){
            channelPreferences.add(new Channel_Preference__c(
                Channel_Preference_Setting__c = cps.Id,
                Contact_ID__c = individual.Id
            ));
        }

        insert channelPreferences;

        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        List<Individual_Relationship__c> individualRelationships = new List<Individual_Relationship__c>();
        // individual-farm
        individualRelationships.add(new Individual_Relationship__c(
            Active__c = true,
            Individual__c = individual.Id,
            Role__c = 'Director',
            Farm__c = farms[0].Id,
            RecordTypeId = IREL_INDIVIDUALFARM_RECORDTYPE_ID
        ));
        // individual-party
        individualRelationships.add(new Individual_Relationship__c(
            Active__c = true,
            Individual__c = individual.Id,
            Role__c = 'Director',
            Party__c = party.Id,
            Farm__c = farms[0].Id,
            RecordTypeId = IREL_INDIVIDUALPARTY_RECORDTYPE_ID
        ));
        insert individualRelationships;

        List<Entity_Relationship__c> entityRelationships = new List<Entity_Relationship__c>();
        entityRelationships.add(new Entity_Relationship__c(
            Active__c = true,
            Farm__c = farms[0].Id,
            Party__c = party.Id,
            Role__c = 'Owner'

        ));
        insert entityRelationships;
    }

    @isTest static void testConstructor_AllValuesInitialised(){
        Contact individual = [SELECT Id, Name, FirstName, LastName FROM Contact LIMIT 1];

        Test.startTest();
            ApexPages.StandardController testStdCtrl = new ApexPages.StandardController(individual);
            CommunicationPreferencesController controller = new CommunicationPreferencesController(testStdCtrl);
        Test.stopTest();

        System.assertNotEquals(null, controller.myPreferencesChannelPreferences, 'myPreferencesChannelPreferences has not been initalised');
        System.assertNotEquals(null, controller.myPreferencesChannelPreferenceHistory, 'myPreferencesChannelPreferenceHistory has not been initalised');

        // My farm preference tab
        System.assertNotEquals(null, controller.individualPreferenceWrappers, 'individualPreferenceWrappers has not been initalised');
        System.assertNotEquals(null, controller.channelPreferenceHistory, 'channelPreferenceHistory has not been initalised');
        System.assertNotEquals(null, controller.myFarmPreferenceHistory, 'myFarmPreferenceHistory has not been initalised');
    }

    @isTest static void testConstructor_GetMyPreferencesChannelPreferences(){
        Contact individual = [SELECT Id, Name, FirstName, LastName FROM Contact LIMIT 1];

        Test.startTest();
            ApexPages.StandardController testStdCtrl = new ApexPages.StandardController(individual);
            CommunicationPreferencesController controller = new CommunicationPreferencesController(testStdCtrl);
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference_Setting__c WHERE Type__c = 'Personal'], controller.myPreferencesChannelPreferences.size(), 'My preference channel preferences size does not match');
    }

    @isTest static void testConstructor_GenerateIndividualPreferenceWrappers(){
        Contact individual = [SELECT Id, Name, FirstName, LastName FROM Contact LIMIT 1];

        Test.startTest();
            ApexPages.StandardController testStdCtrl = new ApexPages.StandardController(individual);
            CommunicationPreferencesController controller = new CommunicationPreferencesController(testStdCtrl);
        Test.stopTest();

        System.assertEquals([SELECT count() FROM Channel_Preference_Setting__c WHERE Type__c = 'Farm'], controller.individualPreferenceWrappers.size(), 'Individual preference wrappers size does not match');

    }

    @isTest static void testSaveMyPrefrences(){
        Contact individual = [SELECT Id, Name, FirstName, LastName FROM Contact LIMIT 1];
        Test.startTest();
            ApexPages.StandardController testStdCtrl = new ApexPages.StandardController(individual);
            CommunicationPreferencesController controller = new CommunicationPreferencesController(testStdCtrl);

            for(Channel_Preference__c cp : controller.myPreferencesChannelPreferences){
                cp.DeliveryByEmail__c = true;
                cp.NotifyBySMS__c = false;
            }

            controller.saveMyPreferences();
        Test.stopTest();

        for(Channel_Preference__c cp : [SELECT Id, DeliveryByEmail__c, NotifyBySMS__c FROM Channel_Preference__c WHERE Contact_ID__c = :individual.Id AND Channel_Preference_Setting__r.Type__c = 'Personal']){
            System.assertEquals(true, cp.DeliveryByEmail__c, 'Deliverly by email did not update');
            System.assertEquals(false, cp.NotifyBySMS__c, 'Notify by SMS did not update');
        }
    }

    @isTest static void testSaveFarmPreferences_UpdateChannelPreferenceSettings(){
        Contact individual = [SELECT Id, Name, FirstName, LastName FROM Contact LIMIT 1];
        Test.startTest();
            ApexPages.StandardController testStdCtrl = new ApexPages.StandardController(individual);
            CommunicationPreferencesController controller = new CommunicationPreferencesController(testStdCtrl);

            for(PreferenceService.IndividualPreferenceWrapper ipw : controller.individualPreferenceWrappers){
                ipw.channelPreference.DeliveryByEmail__c = true;
                ipw.channelPreference.NotifyBySMS__c = false;
            }

            controller.saveFarmPreferences();
        Test.stopTest();

        for(Channel_Preference__c cp : [SELECT Id, DeliveryByEmail__c, NotifyBySMS__c FROM Channel_Preference__c WHERE Contact_ID__c = :individual.Id AND Channel_Preference_Setting__r.Type__c = 'Farm']){
            System.assertEquals(true, cp.DeliveryByEmail__c, 'Deliverly by email did not update');
            System.assertEquals(false, cp.NotifyBySMS__c, 'Notify by SMS did not update');
        }
    }

    @isTest static void testSaveAllTabs(){
        Contact individual = [SELECT Id, Name, FirstName, LastName FROM Contact LIMIT 1];
        Test.startTest();
            ApexPages.StandardController testStdCtrl = new ApexPages.StandardController(individual);
            CommunicationPreferencesController controller = new CommunicationPreferencesController(testStdCtrl);

            for(Channel_Preference__c cp : controller.myPreferencesChannelPreferences){
                cp.DeliveryByEmail__c = true;
                cp.NotifyBySMS__c = false;
            }

            controller.saveAllTabs();
        Test.stopTest();

        for(Channel_Preference__c cp : [SELECT Id, DeliveryByEmail__c, NotifyBySMS__c FROM Channel_Preference__c WHERE Contact_ID__c = :individual.Id AND Channel_Preference_Setting__r.Type__c = 'Personal']){
            System.assertEquals(true, cp.DeliveryByEmail__c, 'Deliverly by email did not update');
            System.assertEquals(false, cp.NotifyBySMS__c, 'Notify by SMS did not update');
        }
    }

    @isTest static void testSaveAndReturnToContact_WithNoErrors(){
        Contact individual = [SELECT Id, Name, FirstName, LastName FROM Contact LIMIT 1];
        Test.startTest();
            ApexPages.StandardController testStdCtrl = new ApexPages.StandardController(individual);
            CommunicationPreferencesController controller = new CommunicationPreferencesController(testStdCtrl);

            PageReference redirectPage = controller.saveAndReturnToContact();
        Test.stopTest();

        System.assertEquals(controller.standardController.view().getUrl(), redirectPage.getUrl(), 'Page did not redircet to contact page');

    }

}