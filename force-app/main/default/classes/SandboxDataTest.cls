/**
* Description: Test for SandboxDataUserQueueable and SandboxDataCleanUpQueueable
* @author: Eric Hu (Trineo)
* @date: Sep 2019
*
*/
@isTest
private class SandboxDataTest {

    @isTest static void SandboxDataUserQueueable() {

        SandboxDataUserQueueable userQueueable = new SandboxDataUserQueueable( 'testUser', false );

        Test.startTest();
            System.enqueueJob(userQueueable);
        Test.stopTest();

        // check the users were created
        List<User> users = [SELECT Id, Alias, IsActive FROM User WHERE FirstName = 'TheTestUser'];
        System.assertEquals(1, users.size());

    }


    @isTest static void SandboxDataCleanUpQueueableTest() {

        System.Test.setMock(WebServiceMock.class, new MetadataServiceUpdateMetadataSuccess());

        List<User> users = new List<User>{TestClassDataUtil.createUserByProfile('Field Team AU - Area Manager', 'Administrator AU - Supplier Services')};


        Test.startTest();
            System.enqueueJob(new SandboxDataCleanUpQueueable( users ));
        Test.stopTest();

        // check the test log was created
        List<Log__c> logs = [SELECT Id, Summary__c FROM Log__c];
        System.assertEquals(1, logs.size());
        System.assertEquals('SandboxDataCleanUpQueueableTest complete', logs[0].Summary__c);

    }

    private class MetadataServiceUpdateMetadataSuccess implements WebServiceMock
    {
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            if(request instanceof MetadataService.updateMetadata_element) {
                MetadataService.updateMetadataResponse_element responseMsg = new MetadataService.updateMetadataResponse_element();
                responseMsg.result = new List<MetadataService.SaveResult>();

                MetadataService.SaveResult result = new MetadataService.SaveResult();
                result.success = true;

                responseMsg.result.add(result);

                response.put('response_x', responseMsg);
            }

            return;
        }
    }

    private class MetadataServiceUpdateMetadataFail implements WebServiceMock
    {
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            if(request instanceof MetadataService.updateMetadata_element) {
                MetadataService.updateMetadataResponse_element responseMsg = new MetadataService.updateMetadataResponse_element();
                responseMsg.result = new List<MetadataService.SaveResult>();

                MetadataService.SaveResult result = new MetadataService.SaveResult();
                result.errors = new List<MetadataService.Error>();

                MetadataService.Error error = new MetadataService.Error();
                error.message = 'this didnt work';

                result.errors.add(error);
                result.success = false;

                responseMsg.result.add(result);

                response.put('response_x', responseMsg);
            }

            return;
        }
    }
}