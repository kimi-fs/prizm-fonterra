/**
* Description: Test class for IndividualRelationshipTrigger
* @author: Salman Zafar (Davanti Consulting)
* @date: May 2015
*/
@isTest
private class IndividualRelationshipTriggerTest {

    private static Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();
    private static User integrationUser = TestClassDataUtil.createIntegrationUser();
    private static Account farmAccount = TestClassDataUtil.createSingleAccountFarm();

    @isTest static void test_method_one() {
        Account partyAccount1 = TestClassDataUtil.createEntity(GlobalUtility.auAccountPartyRecordTypeName, 'Test party');
        insert partyAccount1;

        Contact newContact = new Contact(FirstName='Bruce',LastName='Wayne',AccountId=farmAccount.Id,Individual_MSCRM__c  = 'test123', Type__c = 'Personal');
        system.runAs(integrationUser){
            insert newContact;
            Test.startTest();

            Entity_Relationship__c er = new Entity_Relationship__c(Farm__c=farmAccount.Id,Party__c=partyAccount1.Id,Role__c='Owner');
            insert er;

            Individual_Relationship__c ir = new Individual_Relationship__c();
            ir = new Individual_Relationship__c(RecordTypeId = GlobalUtility.individualPartyRecordTypeId,
                                               Individual__c = newContact.Id,
                                               Party__c = partyAccount1.Id,
                                               Active__c = true,
                                               Role__c = 'Director');

            insert ir;

            ir.Active__c = false;
            update ir;

            Test.stopTest();
        }
    }

    @isTest static void setRuralProfessionalFlagTest() {
        Contact newContact = new Contact(FirstName = 'Bruce', LastName = 'Wayne', AccountId = farmAccount.Id, Individual_MSCRM__c = 'test123', Rural_Professional__c = false, Phone = '+64 3 7005550', Type__c = 'Personal');
        insert newContact;

        Account newOrganisation = new Account(Name = 'The Org', Phone = '+64 3 7005550', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Organisation AU').getRecordTypeId());
        insert newOrganisation;

        Test.startTest();

        Individual_Relationship__c ir = new Individual_Relationship__c(
            RecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Organisation').getRecordTypeId(),
            Individual__c = newContact.Id,
            Organisation__c = newOrganisation.Id,
            Active__c = true,
            Role__c = 'Director'
        );

        insert ir;

        Test.stopTest();

        Contact theContact = [SELECT Id, Rural_Professional__c FROM Contact WHERE Individual_MSCRM__c = 'test123'];
        System.assertEquals(true, theContact.Rural_Professional__c);

    }

}