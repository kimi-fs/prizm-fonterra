@isTest
private class EntityRelationshipTriggerTest {

    private static User integrationUser = TestClassDataUtil.createIntegrationUser();
    private static Account farmAccount = TestClassDataUtil.createSingleAccountFarm();

    @IsTest
    private static void testCreateDeleteIrel() {
        Account partyAccount1 = TestClassDataUtil.createEntity(GlobalUtility.auAccountPartyRecordTypeName, 'Test party');

        insert partyAccount1;

        Contact ind = TestClassDataUtil.createIndividualAU('Bruce', 'Wayne', true);
        Individual_Relationship__c irel = TestClassDataUtil.createIndividualPartyRelationship(ind.Id, partyAccount1.Id, true);
        Entity_Relationship__c erel = TestClassDataUtil.createPartyFarmEntityRelationship(partyAccount1, farmAccount, true);
        Entity_Relationship__c erel2 = TestClassDataUtil.createPartyFarmEntityRelationship(partyAccount1, farmAccount, false);
        erel2.Active__c = false;
        erel2.Role__c = 'Owner (Previous)';
        insert erel2;

        List<Individual_Relationship__c> derivedIrels = getDerivedIRel(partyAccount1.Id, farmAccount.Id);
        System.assert(!derivedIrels.isEmpty());

        // Delete Inactive EREL
        delete erel2;

        derivedIrels = getDerivedIRel(partyAccount1.Id, farmAccount.Id);
        System.assert(!derivedIrels.isEmpty());

        // Delete Active EREL
        delete erel;

        derivedIrels = getDerivedIRel(partyAccount1.Id, farmAccount.Id);
        System.assert(derivedIrels.isEmpty());
    }

    private static List<Individual_Relationship__c> getDerivedIRel (Id partyId, Id farmId) {
        return [SELECT Id
                FROM Individual_Relationship__c
                WHERE Party__c = :partyId
                AND Farm__c = :farmId
                AND RecordTypeId = :GlobalUtility.derivedRelationshipRecordTypeId
                LIMIT 1];
    }

}