/**
 * Description:
 *
 * Combined production across one or more farms
 *
 * @author: John Au (Trineo)
 * @date: Sep 2018
 * @test: AMSCombinedProductionReportContTest
 */
public class AMSCombinedProductionReportController extends AMSProdQualReport {

    public List<String> farmIds { get; set; }

    public Boolean selectAll { get; set; }

    public List<SelectOption> reportTypeSelectList {
        get {
            if (this.reportTypeSelectList == null) {
                this.reportTypeSelectList = new List<SelectOption>();

                this.reportTypeSelectList.add(new SelectOption(AMSCollectionServiceRedux.AggregateType.DAILY.name(), 'Daily Production'));
                this.reportTypeSelectList.add(new SelectOption(AMSCollectionServiceRedux.AggregateType.MONTHLY.name(), 'Monthly Production'));
            }

            return this.reportTypeSelectList;
        }
        set;
    }

    public List<AMSCollectionServiceRedux.CollectionAggregate> collections { get; set; }
    public List<AMSCollectionServiceRedux.CollectionAggregate> comparisonCollections { get; set; }
    public AMSCollectionServiceRedux.CollectionAggregate totals { get; set; }
    public AMSCollectionGraphRedux graph { get; set; }
    public String selectedGraphOption { get; set; }

    private static final String DEFAULT_REPORT_TYPE = 'Combined Production';
    private static final Integer DEFAULT_DAYS_BACK_FOR_COMBINED_PRODUCTION_REPORT = 10;
    private String reportType;

    public AMSCombinedProductionReportController() {
        selectedReportTypeName = DEFAULT_REPORT_TYPE;
    }

    public String getReportType() {
        return this.reportType;
    }

    public void setReportType(String reportType) {
        if (this.reportType != reportType) {
            this.reportType = reportType;
            defaultFromToDate();
        }
    }

    public List<SelectOption> getGraphSelectOptions() {
        return AMSCollectionServiceRedux.GRAPH_SELECT_OPTIONS;
    }

    private static AMSCollectionServiceRedux.AggregateType getAggregateTypeFromString(String aggregateTypeString) {
        for (AMSCollectionServiceRedux.AggregateType aggregateType : AMSCollectionServiceRedux.AggregateType.values()) {
            if (aggregateType.name() == aggregateTypeString) {
                return aggregateType;
            }
        }

        return null;
    }

    public void defaultSelectedFarms() {
        populateFarmList();
        this.farmIds = new List<String>();

        for (SelectOption farmOption : farmOptionList) {
            this.farmIds.add(farmOption.getValue());
        }
    }

    public override void initProperties() {
        reportType = AMSCollectionServiceRedux.AggregateType.DAILY.name();
        selectedGraphOption = AMSCollectionServiceRedux.GRAPH_SELECT_OPTIONS[0].getValue();
        selectAll = true;
        defaultSelectedFarms();
    }

    public override void recalculateForm() {
        //protection against querying all farms is here instead of being baked into the service- calling the service methods with an empty farmIds list results in the farm condition being omitted entirely
        if (farmIds.isEmpty()) {
            collections = new List<AMSCollectionServiceRedux.CollectionAggregate>();
            comparisonCollections = new List<AMSCollectionServiceRedux.CollectionAggregate>();
            totals = new AMSCollectionServiceRedux.CollectionAggregate();
        } else {
            collections = AMSCollectionServiceRedux.getAggregatesForFarms(farmIds, fromDate, toDate, getAggregateTypeFromString(reportType));
            comparisonCollections = AMSCollectionServiceRedux.getAggregatesForFarms(farmIds, fromDate.addYears(-1), toDate.addYears(-1), getAggregateTypeFromString(reportType));
            totals = AMSCollectionServiceRedux.getTotalAggregateForFarms(farmIds, fromDate, toDate);
        }

        redrawGraph();
    }

    public void redrawGraph() {
        graph = new AMSCollectionGraphRedux(
                        fromDate,
                        toDate,
                        new List<List<AMSCollectionServiceRedux.CollectionAggregate>> {
                            collections,
                            comparisonCollections
                        },
                        AMSCollectionServiceRedux.GRAPH_SELECT_OPTIONS_TO_UNIT_OF_MEASUREMENT_MAP.get(selectedGraphOption),
                        getAggregateTypeFromString(reportType));
    }

}