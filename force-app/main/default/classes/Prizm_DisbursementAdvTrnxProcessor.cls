public class Prizm_DisbursementAdvTrnxProcessor extends Prizm_AdvanceTrnxProcessorBase{
    private static final String CLASS_NAME = 'Prizm_DisbursementAdvTrnxProcessor';
    public List<Advance__c> mAdvRecs;
    public Prizm_DisbursementAdvTrnxProcessor(){
        super();
    }
    public override void doProcess() {
        checkTransactionType(mAdvanceTrnxObjList);
        if(!mAdvanceTrnxList.isEmpty()){
            System.debug(loggingLevel.ERROR,'mAdvanceTrnxList'+mAdvanceTrnxList);
            insert mAdvanceTrnxList;
        }
    }
    public override void createAdvanceTrnxObj(){
        if (mActionInput.getRecords().size() >0){
            mAdvRecs = Prizm_AdvanceUtil.getAdvRecs(mActionInput.getRecords());
        }
        if(!mAdvRecs.isEmpty()){
            for(Advance__c advRec : mAdvRecs){
                Prizm_AdvanceTransactionObject advTrnxObj = getDisbursementAdvanceTrnxObj();
                advTrnxObj.advance_number= advRec.id;
                advTrnxObj.amount = advRec.Current_Balance__c;
                advTrnxObj.contract_number = advRec.Lending_Contract_Number__c;
                mAdvanceTrnxObjlist.add(advTrnxObj);
            }
        }
        
    }
    public override string getClassName(){
        return CLASS_NAME;
    }
}