/**
 * Description:
 * @author: Zain Ameerullah (Trineo)
 * @date: April 2021
 * @test: particularly important if coverage for this class is achieved by running a test with a different name
 */
public class PickListResult implements Comparable{

    private String value;
    private String label;

    public PickListResult(String label, String value) {
        this.label = label;
        this.value = value;
    }

    @AuraEnabled
    public Id getValue() {
        return value;
    }

    @AuraEnabled
    public String getLabel() {
        return label;
    }

    /**
     * Allow to sort search results based on title
     */
    public Integer compareTo(Object compareTo) {
        PickListResult other = (PickListResult) compareTo;
        if (this.getLabel() == null) {
            return (other.getLabel() == null) ? 0 : 1;
        }
        if (other.getLabel() == null) {
            return -1;
        }
        return this.getLabel().compareTo(other.getLabel());
    }
}