/*------------------------------------------------------------
Author:        Salman Zafar
Company:       Davanti Consulting
Description:   Test class for SupplyDashboardAccountController
History
03/03/2015     Salman Zafar     Created
------------------------------------------------------------*/

@isTest
private class SupplyDashboardAccountControllerTest {
    private static Account farmAccount = TestClassDataUtil.createSingleAccountFarm();
    private static Account partyAccount1 = TestClassDataUtil.createSingleAccountPartyAU();
    private static Account partyAccount2 = TestClassDataUtil.createSingleAccountPartyAU();
    private static Collection__c collection;
    private static Contact newContact;
    private static Field_Team_Tools_Settings__c fieldTeamToolsSetting = TestClassDataUtil.createFieldTeamToolsSettingsAccountFarmFieldId();
    private static Field_Team_Tools_Settings__c advanceFieldTeamToolsSetting = TestClassDataUtil.createFieldTeamToolsSettingsAdvanceAccountFarmFieldId();

    private static list<Individual_Relationship__c> listIndividualRelationships;
    private static list<Entity_Relationship__c> listEntityRelationships{get;set;}
    private static Id individualPartyRecordTypeId = GlobalUtility.individualPartyRecordTypeId;
    private static Task task = TestClassDataUtil.createSingleTask();
    private static Field_Team_Tools_Settings__c individualDefaultAccount = TestClassDataUtil.individualDefaultAccountAU();
    private static Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();
    private static list<Contact> listContact = new list<Contact>();
    static{
        system.debug('individualDefaultAccount>>'+individualDefaultAccount);
        system.runAs(TestClassDataUtil.createIntegrationUser()){

            for(integer i=0; i<5; i++){
                newContact = new Contact(FirstName='Bruce'+i,LastName='Wayne'+i,AccountId=farmAccount.Id, Type__c = 'Personal');
                listContact.add(newContact);
            }
            insert listContact;
        }

        listIndividualRelationships = new list<Individual_Relationship__c>();
        system.debug('individualPartyRecordTypeId>>'+individualPartyRecordTypeId);
        for(integer i=0; i<5; i++){
            listIndividualRelationships.add(new Individual_Relationship__c(Farm__c = farmAccount.Id,
                                                                           RecordTypeId = individualPartyRecordTypeId,
                                                                           Individual__c = listContact[i].Id,
                                                                           Party__c = partyAccount1.Id,
                                                                           Active__c = true,
                                                                           Role__c = 'Director'));
        }

        system.debug('individualPartyRecordTypeId>>'+individualPartyRecordTypeId);
        database.insert(listIndividualRelationships,false);

        listEntityRelationships = new list<Entity_Relationship__c>();
        for(integer i=0; i<1; i++){
            listEntityRelationships.add(new Entity_Relationship__c(Farm__c=farmAccount.Id,Party__c=partyAccount2.Id,Role__c='Owner'));
        }
        insert listEntityRelationships;

        collection = new Collection__c(Farm_ID__c = farmAccount.Id);
        insert collection;

        task.WhatID = farmAccount.Id;
        //update task;
    }
    static testMethod void myUnitTest() {
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(farmAccount);
        SupplyDashboardAccountController Obj = new SupplyDashboardAccountController(sc);

        PageReference pageRef = Page.SupplyDashboardAccount;
        pageRef.getParameters().put('id', String.valueOf(farmAccount.Id));
        Test.setCurrentPage(pageRef);

        //obj.viewHistoricalCollections();
        obj.viewAllContacts();
        obj.viewAllActivities();
        obj.showPrintableView();
        Test.stopTest();
    }


    static testMethod void testAUFarm() {

        Account auFarm = TestClassDataUtil.createAUFarms(1, true)[0];
        Collection__c collection = TestClassDataUtil.createSingleCollection(auFarm, false);
        collection.Pick_Up_Date__c = Date.today();
        collection.Volume_Ltrs__c = 1000;
        collection.Fat_Percent__c = 22.2;
        collection.Prot_Percent__c = 11.1;
        insert collection;

        // Create Reference period and Farm Season
        Date referenceStartDate = Date.newInstance(Date.today().year(), 7, 1);
        Date startDate = (Date.today() >= referenceStartDate) ? referenceStartDate : referenceStartDate.addYears(-1);
        Date endDate = Date.newInstance(startDate.year() + 1, 5, 30);

        Reference_Period__c referencePeriod = new Reference_Period__c(
            Start_Date__c = startDate,
            End_Date__c = endDate,
            Reference_Period__c = 'Reference Period Test',
            Type__c = 'Milking Season AU'
        );
        insert referencePeriod;
        TestClassDataUtil.createFarmSeasons(new List<Account>{auFarm}, new List<Reference_Period__c>{referencePeriod}, true);

        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(auFarm);
        SupplyDashboardAccountController Obj = new SupplyDashboardAccountController(sc);

        PageReference pageRef = Page.SupplyDashboardAccount;
        pageRef.getParameters().put('id', String.valueOf(auFarm.Id));
        Test.setCurrentPage(pageRef);

        obj.viewAllContacts();
        obj.viewAllActivities();
        obj.showPrintableView();
        Test.stopTest();

        System.assert(!obj.prodSummaryWrappers.isEmpty());
    }
}