@IsTest
private class ScheduleBatchCheckSMSDeliverabilityTest {

    static final String successtoken = 'successtoken';
    static final String failedtoken = 'failedtoken';

    @testSetup public static void setup() {
        TestClassDataUtil.createAuChannelPreferenceSettings();
    }

    @IsTest
    private static void testSendSMSDeliverabilitySuccess() {
        Contact individual = new Contact(LastName = 'Smith', FirstName = 'Tim', MobilePhone = '+64276605508', Phone = '+64 3 1234567', Type__c = 'Personal');

        insert individual;

        Account farm = new Account(Name = 'farm3', RecordTypeId = GlobalUtility.auAccountFarmRecordTypeId);

        insert farm;

        String BatchUniqueKey = System.Now().format('YYMMdd-HHmmssSSS');
        String taskDescription = 'messageblahblahblah';

        System.debug(taskDescription);

        Task task = new Task(
                        WhoId = individual.Id,
                        WhatId = farm.Id,
                        Subject = 'Daily Milk Result (AU)',
                        Description = taskDescription,
                        SMS_Message_ID__c = BatchUniqueKey,
                        SMS_Token__c = successtoken,
                        SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSSENT,
                        CreatedDate = DateTime.now().addMinutes(-60),
                        RecordTypeId = [Select Id from RecordType Where SobjectType = 'Task' And Name = 'System Generated Task'].Id
                    );

        insert task;

        Test.setMock(HttpCalloutMock.class, new MCHttpMock());

        Test.startTest();

        Database.executeBatch(new ScheduleBatchCheckSMSDeliverability(), 1);

        Test.stopTest();

        task = [Select IsClosed,SMS_Status_Reason__c From Task Where Id = :task.Id];

        System.assertEquals(MarketingCloudUtil.MCSTATUSDELIVERED, task.SMS_Status_Reason__c);
        System.assertEquals(0,[Select count() from Case Where AccountId = :farm.Id]);
    }


    @IsTest
    private static void testSendSMSDeliverabilityFailedLessThan15() {
        Contact individual = new Contact(LastName = 'Smith', FirstName = 'Tim', MobilePhone = '+64276605508', Phone = '+64 3 1234567', Type__c = 'Personal');

        insert individual;

        Account farm = new Account(Name = 'farm3', RecordTypeId = GlobalUtility.auAccountFarmRecordTypeId);

        insert farm;

        String BatchUniqueKey = System.Now().format('YYMMdd-HHmmssSSS');
        String taskDescription = 'messageblahblahblah';

        System.debug(taskDescription);

        Task task = new Task(
                        WhoId = individual.Id,
                        WhatId = farm.Id,
                        Subject = 'Daily Milk Result (AU)',
                        Description = taskDescription,
                        SMS_Message_ID__c = BatchUniqueKey,
                        SMS_Token__c = failedtoken,
                        SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSSENT,
                        RecordTypeId = [Select Id from RecordType Where SobjectType = 'Task' And Name = 'System Generated Task'].Id
                    );

        insert task;

        Test.setMock(HttpCalloutMock.class, new MCHttpMock());

        Test.startTest();

        Database.executeBatch(new ScheduleBatchCheckSMSDeliverability(), 1);

        Test.stopTest();

        task = [Select IsClosed,SMS_Status_Reason__c From Task Where Id = :task.Id];

        System.assertEquals(MarketingCloudUtil.MCSTATUSSENT,task.SMS_Status_Reason__c);
        System.assertEquals(0,[Select count() from Case Where AccountId = :farm.Id]);
    }

    @IsTest
    private static void testSendSMSDeliverabilityDelayedLessThan60MoreThan15() {
        Contact individual = new Contact(LastName = 'Smith', FirstName = 'Tim', MobilePhone = '+64276605508', Phone = '+64 3 1234567', Type__c = 'Personal');

        insert individual;

        Account farm = new Account(Name = 'farm3', RecordTypeId = GlobalUtility.auAccountFarmRecordTypeId);

        insert farm;

        String BatchUniqueKey = System.Now().format('YYMMdd-HHmmssSSS');
        String taskDescription = 'messageblahblahblah';

        System.debug(taskDescription);

        Task task = new Task(
                        WhoId = individual.Id,
                        WhatId = farm.Id,
                        Subject = 'Daily Milk Result (AU)',
                        Description = taskDescription,
                        SMS_Message_ID__c = BatchUniqueKey,
                        SMS_Token__c = failedtoken,
                        SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSSENT,
                        CreatedDate = DateTime.now().addMinutes(-60),
                        Status = 'New',
                        RecordTypeId = [Select Id from RecordType Where SobjectType = 'Task' And Name = 'System Generated Task'].Id
                    );

        insert task;

        Test.setCreatedDate(task.Id, System.Now().addMinutes(-35));

        Test.setMock(HttpCalloutMock.class, new MCHttpMock());

        Test.startTest();

        Database.executeBatch(new ScheduleBatchCheckSMSDeliverability(), 1);

        Test.stopTest();

        task = [Select IsClosed,SMS_Status_Reason__c From Task Where Id = :task.Id];

        System.assertEquals(MarketingCloudUtil.MCSTATUSDELAYED, task.SMS_Status_Reason__c);
        System.assertEquals(0,[Select count() from Case Where AccountId = :farm.Id]);
    }

    @IsTest
    private static void testSendSMSDeliverabilityFailedMoreThan48Hours() {
        Contact individual = new Contact(LastName = 'Smith', FirstName = 'Tim', MobilePhone = '+64276605508', Phone = '+64 3 1234567', Type__c = 'Personal');

        insert individual;

        Account farm = new Account(Name = 'farm3', RecordTypeId = GlobalUtility.auAccountFarmRecordTypeId);

        insert farm;

        String BatchUniqueKey = System.Now().format('YYMMdd-HHmmssSSS');
        String taskDescription = 'messageblahblahblah';

        System.debug(taskDescription);

        Task task = new Task(
                        WhoId = individual.Id,
                        WhatId = farm.Id,
                        Subject = 'Daily Milk Result (AU)',
                        Description = taskDescription,
                        SMS_Message_ID__c = BatchUniqueKey,
                        SMS_Token__c = failedtoken,
                        SMS_Status_Reason__c = MarketingCloudUtil.MCSTATUSSENT,
                        RecordTypeId = [Select Id from RecordType Where SobjectType = 'Task' And Name = 'System Generated Task'].Id
                    );

        insert task;

        Test.setCreatedDate(task.Id, System.Now().addMinutes((-60 * 48) - 5));

        Test.setMock(HttpCalloutMock.class, new MCHttpMock());

        Test.startTest();

        Database.executeBatch(new ScheduleBatchCheckSMSDeliverability(), 1);

        Test.stopTest();

        task = [Select CreatedDate, isClosed, SMS_Status_Reason__c From Task Where Id = :task.Id];

        System.assertEquals(MarketingCloudUtil.MCSTATUSFAILEDDELIVERY,task.SMS_Status_Reason__c);
        System.assertEquals(0,[Select count() from Case Where AccountId = :farm.Id]);
    }


    public class MCHttpMock implements HttpCalloutMock {

        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setStatusCode(200);

            if (request.getEndpoint() == 'https://auth.exacttargetapis.com/v1/requestToken') {
                response.setBody('{"accessToken": "boobooboo"}');
            } else {
                System.debug(request.getEndpoint());

                if (request.getEndpoint().contains(ScheduleBatchCheckSMSDeliverabilityTest.successtoken)){
                    response.setBody('{"count": 1, "status": "Finished", "tracking": [{"message": "DELIVRD", "mobileNumber": "64276605508", "statusCode": "0"}] }');
                } else if (request.getEndpoint().contains('failedtoken')){
                    response.setBody('{"count": 1, "status": "In Progress", "tracking": [{"message": "Sent to Dialogue", "mobileNumber": "64276605508", "statusCode": "0"}] }');
                }
            }
            return response;
        }
    }
}