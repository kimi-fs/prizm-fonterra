/**
 * Description: Changes the password of the logged in user
 * @author: Amelia (Trineo)
 * @date: September 2017
 * @test: AMSPasswordChangeControllerTest
 */
public with sharing class AMSPasswordChangeController {
    public String currentPassword {get; set;}
    public String newPassword {get; set;}
    public String confirmPassword {get; set;}

    public String errorMessage {get; set;}

    public Boolean passwordUpdated {get; private set;}

    public static Boolean isViewingDifferentContact {
        get {
            if (isViewingDifferentContact == null) {
                isViewingDifferentContact = AMSContactService.isViewingDifferentContact();
            }
            return isViewingDifferentContact;
        }

        set;
    }

    private User user;

    private static Map<String, String> messages = AMSCommunityUtil.initialisePageMessages('AMSPasswordChange');

    public AMSPasswordChangeController() {
        if (isViewingDifferentContact) {
            this.errorMessage = messages.get('not-current-user');
        } else {
            user = AMSUserService.getUser();
            passwordUpdated = false;
            errorMessage = '';
        }
    }

    public PageReference updatePassword() {
        Boolean validPassword = true;
        if (!AMSUserService.isValidPassword(this.newPassword)) {
            errorMessage = messages.get('invalid-password');
            validPassword = false;
        }

        if (!AMSUserService.passwordMatches(this.newPassword, this.confirmPassword)) {
            errorMessage = messages.get('password-not-matching');
            validPassword = false;
        }

        try {
            PageReference passwordChangePageRef;
            if (validPassword) {
                if (!Test.isRunningTest()) {
                    passwordChangePageRef = Site.changePassword(newPassword, confirmPassword, currentPassword);
                } else {
                    passwordChangePageRef = Page.AMSDashboard;
                }
            }

            // get the message from the page message since it doesn't throw an error but just magically puts them in page messages
            for (ApexPages.Message msg : ApexPages.getMessages()) {
                if (msg.getDetail() != null && msg.getDetail().contains('Your old password is invalid.')) {
                    errorMessage = messages.get('incorrect-current-password');
                } else if (msg.getDetail() != null && msg.getDetail().contains('Your old password cannot be null')) {
                    errorMessage = messages.get('missing-current-password');
                } else {
                    errorMessage = msg.getDetail();
                }
            }

            if (passwordChangePageRef != null) {
                passwordUpdated = true;
                errorMessage = '';
            }
        } catch (Exception e) {
            errorMessage = messages.get('unknown-error');
            return null;
        }

        return null;
    }

    public static PageReference redirectToMyDetails() {
        return Page.AMSMyDetails;
    }
}