/**
 * A one off class for use in Unit Tests and new Orgs to seed the AMS Advice Articles used on the AMS
 * Community site.
 *
 * It is possible that they already exist, and may have been modified, so only add them if they don't already
 * exist.
 *
 * @author Amelia (Trineo)
 * @date Oct 2017
 * @test: SetupAMSAdviceTest
 */
public without sharing class SetupAMSAdvice {
    private class AdviceWrapper {
        public String title;
        public String displayType;
        public Integer sequence;
        public String summaryContent;
        public String highlightedContent;
        public String mainContent;

        public AdviceWrapper(String title, String displayType, Integer sequence, String summaryContent, String highlightedContent, String mainContent) {
            this.title = title;
            this.displayType = displayType;
            this.sequence = sequence;
            this.summaryContent = summaryContent;
            this.highlightedContent = highlightedContent;
            this.mainContent = mainContent;
        }
    }

    @testVisible
    private static List<AdviceWrapper> webTexts = new List<AdviceWrapper> {
        new AdviceWrapper(
            'Supply Fonterra',
            'Landing',
            100,
            '',
            '',
            '<p>The Supply Fonterra programme delivers to Fonterra&rsquo;s Sustainability Strategy by setting clear standards and good practice, and supporting farmers to reach these standards. It provides a framework to develop further programmes so that Fonterra can respond quickly to emerging market, regulatory or community requirements and deliver change on-farm at pace.</p> <p>The focus of current programmes is to improve milk quality, reduce our on-farm environmental impact and develop animal welfare action plans to address gaps between expectations and measures and standards.</p> <h2>What you can expect from Supply Fonterra:</h2> <h3>&bull; Clear standards and good practice</h3> <p>We&rsquo;re here to help you farm sustainably so you meet Fonterra and council environmental, food safety and animal health and welfare standards.</p> <h3>&bull; Practical advice and support</h3> <p>Fonterra has a team of Sustainable Dairying Advisors. Area Managers, and Regional Food Safety Managers who can help you put practical plans and practices in place. The programme can also put you in contact with industry experts to keep you at the forefront of dairying.</p> <h3>Advocacy and reporting</h3> <p>We collect information about the Co-op&rsquo;s sustainability progress to prove our positive changes and advocate strongly on your behalf - to show councils, customers and the public that we are proactively managing our environmental and food safety responsibilities.</p> <div class="expandableContent">The standards included in Supply Fonterra and our wider terms and conditions of Supply are driven by: <ul> <li>Industry requirements, including the standards agreed in the industry-wide Sustainable Dairying: Water Accord.</li> <li>Council requirements.</li> <li>Legislation, such as the New Zealand Code of Practice for the Design and Operation of Farm Dairies NZCP1.</li> <li>Fonterra&rsquo;s sustainability strategy, which incorporates the expectations of our customers, consumers, and communities</li> </ul> </div> <h3>More Information</h3> <p>If you&#39;d like to know more about Supply Fonterra, feel free to call your Area Manager or Sustainable Dairying Advisor. They have local knowledge and are equipped to provide assistance with council requirements, MPI requirements, and farming issues in your area.</p>'
        ),
        new AdviceWrapper(
            'Environment',
            'Landing',
            100,
            '<p>Over the past 10 years, Fonterra farmers have committed to doing right by the land made good progress to meet the targets set by the Clean Streams Accord.<br /> <br /> &nbsp;</p> ',
            '',
            '<p>Over the past 10 years, Fonterra farmers have committed to doing right by the land made good progress to meet the targets set by the Clean Streams Accord.</p> <p>Supply Fonterra environmental programmes build on this good work and align with the new Sustainable Dairying: Water Accord, each with goals that are well aligned to external commitments and industry agreements.</p> &nbsp; <h2>Nitrogen Management</h2> <p>Raising awareness of opportunities for farmers to increase their farm&rsquo;s nitrogen conversion efficiency and, where appropriate, decrease nitrogen loss risk.</p> <div class="expandableContent"> <p>The Supply Fonterra Nitrogen Management Programme aims to reduce dairying&rsquo;s impact on ground water by helping farmers use nitrogen more efficiently, leading to reduced waste and improved profitability. In turn, this helps you and the Co-operative prepare for the rapidly changing regulatory environment and will assist your business&rsquo;s nutrient planning.</p> <p>You are asked to provide your farm&rsquo;s nitrogen input information at the end of each season, which is then entered into the Overseer model to provide you with a report showing your nitrogen conversion efficiency and modelled nitrogen leaching loss risk. Your Sustainable Dairying Advisor can help you develop a plan to improve nitrogen efficiency on your farm.</p> <p><iframe frameborder="0" height="338" scrolling="auto" src="https://www.youtube.com/embed/CFdJlMKCF0g?showinfo=0" width="600"></iframe></p> <h3>Factsheets:</h3> <ol> <li><a href="#" target="_blank">Nitrogen Management Programme Factsheet</a></li> <li><a href="#" target="_blank">Article: Nitrogen know-how pays off for farming trio</a></li> <li><a href="#" target="_blank" title="Interpreting your Nitrogen Management Report (Sample)">Interpreting your Nitrogen Management Report (Sample)</a></li> </ol> </div> <h2>Effluent Management</h2> <p>Ensuring 365 day compliance with regional council regulations and aim for zero discharge to water from effluent systems.</p> <div class="expandableContent"> <p>The Supply Fonterra Effluent Management Programme offers assistance to help farmers to maximise nutrient use and minimise effects on local waterways to ensure 365-day compliance with regional council regulations. Through appropriate management of effluent we can reduce the amount of nitrogen and phosphorus entering water and reduce faecal coliforms and suspended solids getting into water, improving water quality. The Co-operative&rsquo;s aim is zero discharge to water from effluent systems.</p> <p>Effluent management systems are assessed each year during the annual Farm Dairy and Environmental Assessment. If an issue is found, your Sustainable Dairying Advisor can help by developing an Environmental Improvement Plan (EIP) and by providing one-to-one advice and support to develop a more resilient effluent system.</p> <p>Farm dairy effluent is a valuable resource. When managed effectively, it can increase grass growth and reduce the use of fertiliser. Poorly managed effluent poses a business and environmental risk.</p> <p>Farmers in the video below discuss different ways managing effluent - from state-of-the-art systems to more simple solutions - so they can apply better nutrients to their pasture to boost pasture growth and ultimately milk production.</p> <p><iframe frameborder="0" height="337" scrolling="auto" src="https://player.vimeo.com/video/118081099?title=0&amp;byline=0&amp;portrait=0" width="600"></iframe></p> <h3>Factsheets:</h3> <p>1. <a href="#" target="_blank">When to Irrigate Factsheet</a><br /> 2. <a href="#" target="_blank">Application Depth Test Factsheet</a><br /> 3. <a href="#" target="_blank">Designing an Effluent Storage Pond Factsheet</a><br /> 4. <a href="#" target="_blank">Effluent Storage Pond Lining Options Factsheet</a><br /> 5. <a href="#" target="_blank">Effluent Storage Management Factsheet</a><br /> 6. <a href="#" target="_blank">DairyNZ Farm Facts on Effluent</a></p> <p>&nbsp;</p> </div> <h2>Water Use Management</h2> <p>Ensuring that all Fonterra farms use no more water for wash down and milk cooling than is necessary to produce safe and hygienic milk</p> <div class="expandableContent"> <p>The Supply Fonterra Water Use Management Programme is being designed to protect water resources by promoting responsible, effective and efficient water use on-farm. The programme will build awareness of changing water allocation rules, and will advise on water efficiency practices that will enable responsible and sustainable water management. As part of the programme, all farms will need to have water measurement capability by the 2018/2019 season.</p> <h3>Factsheets:</h3> <p>1. <a href="#" target="_blank">Water Use Efficiency Factsheet</a><br /> 2. <a href="#" target="_blank">Installing Water Meters Factsheet</a><br /> 3. <a href="#" target="_blank">Regional Consent Factsheet</a></p> <p>&nbsp;</p> </div> <h2>Waterways Management</h2> <p>Ensuring that our waterways are clean and clear by keeping stock out and improving riparian management.</p> <div class="expandableContent"> <p>The Supply Fonterra Waterway Management Programme helps reduce the impact that our farms have on surface water quality. This includes standards for stock exclusion from defined waterways and stock crossings. The programme also helps you to identify and manage other on-farm risk areas that could contribute to a decline in water quality.</p> <p>Your Area Manager or Sustainable Dairying Advisor can help you to understand the requirements for your farm and put an Environmental Improvement Plan (EIP) in place to address any issues identified.</p> <p>The target is to achieve 100% stock exclusion from Fonterra defined waterways and to have all regular stock crossings bridged or culverted by 1 December 2013. A Fonterra defined waterway is a river, stream, drain, canal, lake or wetland that permanently contains water and is more than a metre wide and 30 centimetres deep at any time of year.</p> <p>This programme will also provide assistance with riparian planting once standards in this area have been determined.</p> <p><iframe frameborder="0" height="338" scrolling="auto" src="https://www.youtube.com/embed/AIQipVrfceI?showinfo=0" width="600"></iframe></p> <h3>Factsheets:</h3> <p>1. <a href="#" target="_blank">Waterway Management Programme Factsheet</a><br /> 2. <a href="#" target="_blank">Guide to Crossings</a><br /> 3. <a href="#" target="_blank">DairyNZ Riparian Planner</a><br /> 4. <a href="#" target="_blank">Riparian Management</a></p> <p>&nbsp;</p> </div> <h2>Living Water</h2> <p>Living water is our long-term commitment to caring for New Zealand&#39;s waterways, ensuring dairying works alongside natural habitats and ecosystems of healthy, living water - now, and for the future.</p> <div class="expandableContent"> <p>Fonterra and DOC have a common interest in protecting New Zealand&#39;s waterways. We recognise that quality water from natural habitats underpins New Zealand&#39;s environmental health and economic prosperity.</p> <p>Our initiative in key water catchments marks the start of a new determination to work together, engaging with local communities to make a real difference for their waterways, and for New Zealand&#39;s waterway health as a whole.</p> <p>To start with, we&#39;ve focused on five sensitive water catchments across New Zealand, and will work with local communities to enhance them for the future.</p> <h3>Factsheets:</h3> <p>1. <a href="#" target="_blank" title="Living Water Annual Report 2015/16">Living Water Annual Report 2015/16</a></p> <p>Click <a href="#" target="_blank">here</a> to see our recent progress.</p> <p>&nbsp;</p> </div>'
        ),
        new AdviceWrapper(
            'Food Safety & Milk Quality',
            'Landing',
            200,
            '<p>The milk supplied by Fonterra farmers must meet the food safety standards of Fonterra, New Zealand&rsquo;s government, the government authority of each overseas country we export to, and our individual customers in those countries.</p>',
            '',
            '<h2>Food Safety</h2> <p>The milk supplied by Fonterra farmers must meet the food safety standards of Fonterra, New Zealand&rsquo;s government, the government authority of each overseas country we export to, and our individual customers in those countries.</p> <p>Many customers have very detailed acceptance specifications so they can provide their consumers with traceability. A Risk Management Programme (RMP) and comprehensive milk testing and monitoring programmes ensure that all milk is &#39;fit for purpose&#39; &ndash; safe, suitable and truthfully labelled.</p> <p>This RMP helps Fonterra farmers meet the required standards of excellence and Fonterra&rsquo;s conditions of supply. Supply Fonterra food safety programmes make a big difference to the quality of the milk we produce as a Co-op:</p> <h2>Milk Quality Support</h2> <p><strong>New Milk Cooling temperature details are now on your tanker dockets, My Farm app and Farm Source. For most farms this is advisory until 1 June, 2018, although is in effect for new and upgraded farms from 1 June 2016. If you are unsure please see below or contact the service centre for more information.</strong></p> <p>Continuous improvement in Fonterra&#39;s milk quality and ensure that the milk produced on every Fonterra farm is fit for purpose, safe, sustainable and wholesome</p> <h2>Factsheets:</h2> <ol> <li><a href="" target="_blank">Locking and labelling vats</a></li> <li><a href="/assets/assets/Requirements-for-devices-on-and-modifications-to-Fonterra-owned-vats-New-Zealand.pdf" target="_blank">Requirements For Devices On And Modifications To Fonterra Owned Vats (New Zealand)</a></li> </ol> <h2>Milk Temperature Management</h2> <p>The new raw milk temperature standards set by the Ministry of Primary Industries come into effect for all farm dairies on 1 June 2018. The new standards came into effect for all new and upgraded farms on 1 August 2016.</p> <p>The new standards are in line with international regulations and require raw milk to:</p> <ul> <li>be cooled to 10&ordm;C or below within four hours of the commencement of milking; and</li> <li>be cooled to 6&ordm;C or below within the sooner of: <ul> <li>six hours from the commencement of milking, or</li> <li>two hours from the completion of milking; and</li> </ul> </li> <li>be held at or below 6&ordm;C without freezing until the collection or the next milking; and</li> <li>not exceed 10&ordm;C during subsequent milkings.</li> </ul> <p>We want to help you determine if your current system will meet the new temperature standards. From October, we will check all milk collections at tanker pick-up against the new standards and provide temperature details on tanker dockets. These will be advisory only and no penalties apply.</p> <p>We&#39;re calling these alerts &quot;Temp 2018 Alert&quot;. If you receive on of these alerts, we suggest you assess your cooling system so that you are prepared for the new standards. These tanker collection results will only be an indication of your compliance to the new standards so even if you don&#39;t receive a Temp 2018 Alert, we suggest you monitor your cooling efficiency.</p> <p>You may need to seek advice from your refrigeration specialist, milk machine specialist and/or electrician as there is not a &#39;one-size fits all&#39; solution. Factors such as daily milk volume, peak flows, primary cooling water temperature, access to water and access to increased power requirements on each farm may determine if any changes are needed and the cost involved.</p> <p>We recommend that you act earlier rather than later to assess your milk cooling system. If you need to make changes, an early assessment will ensure you are less likely to face higher costs and longer waiting times for refrigeration suppliers and equipment as the compliance date draws nearer.</p> <p>If you have any questions on MPI&#39;s new standards for milk cooling, get in touch with your Area Manager, On-Farm Regional Asset Manager or the Fonterra Farm Source Service Centre team on 0800 65 65 68.</p> <h3>Factsheets and more information:</h3> <ol> <li><a href="" target="_blank">Understanding Your Milk Cooling Systems</a></li> <li><a href="" target="_blank">Milk Collection Temperature</a></li> <li><a href="" target="_blank">Checking your systems and milk temperature</a></li> <li><a href="/assets/Comms/RP-Email-Attachment-Files/11.-Milk-Cooling-checklist.pdf" target="_blank">Milk Cooling Performance Checklist</a></li> </ol>'
        ),
        new AdviceWrapper(
            'Residue Management',
            'Expand',
            100,
            'Driving continuous improvement in milk quality by reducing residues that and helping farmers reduce residue grading risks.',
            '',
            '<div class="expanded-content"> <h3>Factsheets:</h3> <ol> <li><a href="/assets/ContentPages/Resources/Fact-Sheets/Farm-Dairy-Detergent-Factsheet-Sept-2014-pdf.pdf" target="_blank">Farm Dairy Detergent Factsheet</a></li> <li><a href="" target="_blank">Teat Spray Factsheet</a></li> <li><a href="" target="_blank">Early Season Residue</a></li> <li><a href="" target="_blank">Clearer labelling to reduce residue risk</a></li> </ol> <p><iframe frameborder="0" height="338" scrolling="auto" src="https://player.vimeo.com/video/127895040" width="600"></iframe></p> <p>&nbsp;</p> </div>'
        ),
        new AdviceWrapper(
            'Temporary Water Exclusion',
            'Expand',
            200,
            'Temporary water exclusion is a regulatory step taken when non compliant water is indentified in a farm dairy.',
            '',
            '<div class="expanded-content"> <p>It is important that the water you use to clean your milking plant meets MPI&rsquo;s standards because this water could come in contact with milk and affect its quality.</p> <p>Water is assessed by your Farm Dairy Assessor in three areas:</p> <ol> <li>E coli: water must be free of E coli (3 yearly)</li> <li>Clarity: water must be relatively free of solids (yearly)</li> <li>Checklist: the water source is assessed for risks which could compromise quality (yearly)</li> </ol> <p>Farms that are on temporary water exclusion have an increased chance of freezing point demerits and also incur a monthly charge of $100 which goes towards the costs of additional milk testing and monitoring.</p> <p>If impacted farms can show that they have undertaken the work required to come off temporary water exclusion by June 1 2015 this fee will be refunded back to October 2014.</p> <p><iframe frameborder="0" height="338" scrolling="auto" src="https://player.vimeo.com/video/109411273?color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0" width="600"></iframe></p> <h3>Factsheet:</h3> <ol> <li><a href="" target="_blank">Guide to compliant water</a></li> </ol> <p>&nbsp;</p> </div>'
        ),
        new AdviceWrapper(
            'Animal Health & Welfare',
            'Landing',
            300,
            '<p>Fonterra is a world-leading dairy manufacturer due in part to the care that New Zealand farmers have for their animals.</p>',
            '',
            '<p><iframe frameborder="0" height="338" scrolling="auto" src="https://player.vimeo.com/video/118081097?title=0&amp;byline=0&amp;portrait=0" width="600"></iframe></p> <p>Fonterra is a world-leading dairy manufacturer due in part to the care that New Zealand farmers have for their animals.</p> <p>This dedication to animal health and welfare has contributed to record milk production over recent years and lower somatic cell count levels, leading to more grade free award winners than ever before. At the same time farmers have lowered induction rates, achieving improved reproductive performance and better conception rates.</p> <p>Additionally, the Co-op has built trust with customers and consumers who can be sure that the products they buy are produced from healthy animals that are raised using responsible animal welfare practices. Supply Fonterra animal welfare programmes provide Fonterra farmers with access to the latest industry animal health and welfare information, and reference guides for key animal management processes and procedures:</p> <h2><br /> Factsheets</h2> <ul> <li><a href="#" target="_blank">Race Construction Maintenance</a></li> <li><a href="#" target="_blank">Stand-off Areas, Feed Pads and Winter Housing</a></li> <li><a href="#" target="_blank">Bobby Calves</a></li> <li><a href="#" target="_blank">Guidelines for Transporting Animals</a></li> <li><a href="#" target="_blank">Shade and Shelter</a></li> </ul> <h2><br /> Mastitis Support</h2> <p>Ensuring Fonterra farmers meet dairy industry regulations and where necessary, help identify and address the causes of mastitis and reduce its occurrence.</p> <div class="expandableContent"> <p>Tailored on-farm assistance to help farmers find the causes of mastitis and reduce its occurrence. The Supply Fonterra Mastitis Support Programme provides tailored on-farm assistance to help find the causes of mastitis and reduce its occurrence.</p> <p>It assists farmers with an average SCC above 400,000 cells/ml, as well as those who have an SCC level greater than 300,000 cells/ml in any one month with significant SCC demerits.</p> <p>If you&rsquo;d like more information about managing mastitis, call 0800 65 65 68.</p> <p><a href="http://www.dairynz.co.nz/Search/Results?Term=welfare" target="_blank">Search &quot;welfare&quot; on Dairy NZ&nbsp;</a>for helpful tools and resources.</p> </div>'
        ),
        new AdviceWrapper(
            'Animal Health & Welfare related articles',
            'Expand',
            100,
            '<h2>Body Condition Scoring</h2> <p>The Body Condition Score (BCS) is a visual estimate of an animal&#39;s body fat reserves. Best practice is to monitor the BCS of your animals throughout the year.</p> <p>Click&nbsp;<a href="https://nzfarmsource.co.nz/assets/ContentPages/Resources/Farm-Source-Magazine-Articles/Body-Condition-Scoring.pdf" target="_blank">here</a>&nbsp;to find out more about the Body Condition Score and how it can help you monitor your animal wellbeing.<br /> &nbsp;</p> <h2>Dry Cow Therapy</h2> <p>Ensuring your herd is protected from new infections over the winter months means you will be ready to kick off the next milking season.<br /> Click&nbsp;<a href="#" target="_blank">here</a>&nbsp;for our top three tips on dry cow therapy.</p> <h2><br /> Early Response</h2> <p>Helping farmers through challenging times and assist them to get their animals and farms back on track.&nbsp;<br /> &nbsp;</p> <h2>Humane Slaughter and Switch Trimming and Shortening</h2> <p>At the start of the new season, many farmers will be thinking about the humane slaughter of calves and switch trimming and shortening. Click&nbsp;<a href="http://nzfarmsource.co.nz/assets/ContentPages/Resources/Farm-Source-Magazine-Articles/On-farm-animal-welfare.pdf" target="_blank">here</a>&nbsp;for guidelines around these animal welfare practices.</p>',
            '',
            ''
        ),
        new AdviceWrapper(
            'Farm Dairy & Environmental Assessment',
            'Landing',
            400,
            '<p>Giving government, customers and consumers assurances about the quality of the milk we produce and the integrity of the system producing the milk.</p>',
            '<p><b>FDEA changes for 2017/2018 Season</b></p> <p>The launch of a new season sees some small changes to our Farm Dairy &amp; Environmental Assessments.</p> <p>While the focus of the assessment is still similar to previous seasons, there is a renewed focus on water compliance and water treatment systems, clarification on body condition score assessment and new questions about riparian management.</p>',
            '<p>Giving government, customers and consumers assurances about the quality of the milk we produce and the integrity of the system producing the milk.</p> <p>Your Farm Dairy &amp; Environmental Assessment (FDEA) is part of Fonterra&rsquo;s commitment to having the best quality milk and best dairying environment in the world.</p> <p>Each season, an appointed Farm Dairy Assessor from QCONZ or AsureQuality visits your farm to ensure that it meets standards set out by the Ministry for Primary Industries, NZCP1 and Fonterra&rsquo;s Risk Management Programme. This assessment focuses on:</p> <ul> <li>Cleanliness of premises, plant and surrounds</li> <li>Dairy Diary Quality Management and treatment records (red pages)</li> <li>Environmental sustainability</li> <li>Animal Health and Welfare</li> <li>Farm dairy water</li> <li>Minimum distances including chemical storage and mixing</li> </ul> <p>You will be notified about your assessment by post approximately two weeks before it takes place.</p> <p>If necessary, your assessment also triggers support from Supply Fonterra to ensure that you get the information and assistance you need to meet these standards.</p> <p><iframe frameborder="0" height="337" scrolling="auto" src="https://player.vimeo.com/video/96551384?color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0" width="600"></iframe></p>'
        ),
        new AdviceWrapper(
            'Support',
            'Landing',
            100,
            '',
            '',
            '<p>Get to know your Supplier Forum contacts, find all the Dairy Diary and handbook resources and learn more information about Fonterra Sponsorships.</p> <p>Farm Source team is here to help. Learn more about popular topics and find resources that will help with all of your farming needs.</p>'
        ),
        new AdviceWrapper(
            'Supplier Forum',
            'Landing',
            100,
            '<p>The aim of the Supplier Forum is to:</p> <ul> <li>Provides a structure that builds on existing organisations.</li> <li>Facilitates the inclusion of suppliers from all regions.</li> <li>Fosters the level of understanding between all of our farmer suppliers and the company.</li> </ul> <p>The forum objectives are:</p> <ul> <li>To provide two-way consultation between Fonterra Australia and its milk suppliers.</li> <li>To provide input into wider business and industry issues.</li> <li>To provide leadership in contributing to an industry-leading value proposition for farmers</li> </ul>',
            '',
            '<p>The aim of the Supplier Forum is to:</p> <ul> <li>Provides a structure that builds on existing organisations.</li> <li>Facilitates the inclusion of suppliers from all regions.</li> <li>Fosters the level of understanding between all of our farmer suppliers and the company.</li> </ul> <p>The forum objectives are:</p> <ul> <li>To provide two-way consultation between Fonterra Australia and its milk suppliers.</li> <li>To provide input into wider business and industry issues.</li> <li>To provide leadership in contributing to an industry-leading value proposition for farmers</li> </ul>'
        ),
        new AdviceWrapper(
            'Dairy Diary',
            'Landing',
            200,
            '<p>A copy of the Dairy Diary is delivered to all dairy sheds at the start of each season. We have also provided each section below for you to refer to or print extra pages, should you require them.</p>',
            '',
            '<h2 class="intro">2017/18 Dairy Diary - Including Mandatory Recording Pages</h2> <p>A copy of the Dairy Diary is delivered to all dairy sheds at the start of each season. We have also provided each section below for you to refer to or print extra pages, should you require them.</p> <p><a href="#" target="_blank" title="Download Dairy Diary">Download 2017/18 Dairy Diary now</a></p> <h3>2017/18 Farmers&#39; Handbook</h3> <p>A copy of the Farmers&rsquo; Handbook is delivered to all farms and farm owners at the start of each season. The Handbook contains Fonterra&rsquo;s terms and conditions of supply and we recommend our farmers familiarise themselves with the contents. We have provided the Handbook below for you to refer to online.</p> <p><a href="#" target="_blank">2017/18 Farmers&rsquo; Handbook</a></p> <p>If you have any queries about the Dairy Diary or the Farmers&rsquo; Handbook, please contact your Area Manager, Regional Food Safety Manager or Sustainable Dairy Advisor.</p> <h3>New Dairy and Dairy Alteration Support Pack</h3> <p>If you are considering building a new farm dairy or making alterations, this document will help you with all stages of the process. Please talk to your Farm Dairy Assessor, the On-Farm Assets Team and your Sustainable Dairy Advisor before you start.</p> <p><a href="#" target="_blank">New Dairy and Dairy Alteration Support Pack</a></p> <h3>Dairy Conversion Manual</h3> <p>If you are considering converting land to a dairy farm, <a href="#" target="_blank">this document</a> covers all the information you need to consider before you start, and what to think about during the conversion.</p> <p>If you are considering conversion, please talk to your Business Development Manager.</p> <p><a href="#" target="_blank">Dairy Conversion Manual</a></p>'
        ),
        new AdviceWrapper(
            'Sponsorships',
            'Landing',
            300,
            '<p>Fonterra supports a number of local and national dairy industry and community programmes. Our aim is to build the link between dairy farming and strong local communities.</p>',
            '',
            '<p class="intro">Fonterra supports a number of local and national dairy industry and community programmes.</p> <p>Our aim is to build the link between dairy farming and strong local communities.</p> <p>Our national awards sponsorships reflect our commitment to promoting excellence in dairying by supporting education in the dairy industry. We want to celebrate success and showcase famers of the future to help drive the industry forward.</p>'
        ),
        new AdviceWrapper(
            'Farm Source',
            'Landing',
            100,
            '',
            '',
            ''
        ),
        new AdviceWrapper(
            'Farm Source Service',
            'Landing',
            100,
            '',
            '',
            ''
        ),
        new AdviceWrapper(
            'Farm Source Professional',
            'Landing',
            200,
            '',
            '',
            ''
        ),
        new AdviceWrapper(
            'Farm Source Finances',
            'Landing',
            300,
            '',
            '',
            ''
        ),
        new AdviceWrapper(
            'Farm Source Partners',
            'Landing',
            400,
            '',
            '',
            ''
        ),
        new AdviceWrapper(
            'Farm Source Digital',
            'Landing',
            500,
            '',
            '',
            ''
        )
    };

    public static void createArticles() {
        List<Knowledge__kav> articles = new List<Knowledge__kav>();

        List<Knowledge__kav> existingPublishedArticles = [SELECT UrlName
                                                            FROM Knowledge__kav
                                                            WHERE PublishStatus = 'Online'
                                                            AND RecordTypeId = :GlobalUtility.amsAdviceRecordTypeId
                                                            AND Language = 'en_US'];

        List<Knowledge__kav> existingDraftArticles = [SELECT UrlName
                                                            FROM Knowledge__kav
                                                            WHERE PublishStatus = 'Draft'
                                                            AND RecordTypeId = :GlobalUtility.amsAdviceRecordTypeId
                                                            AND Language = 'en_US'];


        List<Knowledge__kav> existingArticles = existingPublishedArticles;
        existingArticles.addAll(existingDraftArticles);

        Set<String> existingArticleUrlNames = new Set<String>();

        for (Knowledge__kav article : existingArticles) {
            existingArticleUrlNames.add(article.UrlName);
        }

        for (AdviceWrapper wrapper : webTexts) {
            String urlName = toUrlName(wrapper.title);

            System.debug('Processing ' + urlName);

            if (!existingArticleUrlNames.contains(urlName)) {
                Knowledge__kav article = new Knowledge__kav(
                    Title = wrapper.title,
                    UrlName = urlName,
                    Display_Type__c = wrapper.displayType,
                    Sequence__c = wrapper.sequence,
                    Summary_Content__c = wrapper.summaryContent,
                    Highlighted_Content__c = wrapper.highlightedContent,
                    Content__c = wrapper.mainContent,
                    Language = 'en_US',
                    IsVisibleInCsp = true,
                    RecordTypeId = GlobalUtility.amsAdviceRecordTypeId
                );
                articles.add(article);
            }
        }

        insert articles;

        // Dont publish - we have to add the data category and any file links manually
    }

    private static String toUrlName(String title) {
        String urlName = title.replace(' ', '').replace(':', '').replace('&', '');

        if (Test.isRunningTest()) {
            // For some reason Knowledge in UTs is weird, it won't allow you to create an article with the same
            // UrlName as in the Org. So add this suffix so that we don't clash.
            urlName += '-unit-test';
        }
        return urlName;
    }
}