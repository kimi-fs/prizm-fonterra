/**
 * Description:
 * Test for KnowledgeTriggerHandler.cls
 *
 * @author: John Au (Trineo)
 * @date: August 2018
 */
@IsTest
private class KnowledgeTriggerHandlerTest {

    @TestSetup
    private static void testSetup() {
        TestClassDataUtil.integrationUserProfile();
    }

    @IsTest
    private static void testInvocableMethod() {
        final String DATA_CATEGORY_NAME = 'Dairy_News';

        Knowledge__kav newsArticle = TestClassDataUtil.createAMSNewsArticle(1, false)[0];
        newsArticle.Data_Category__c = DATA_CATEGORY_NAME;

        Test.startTest();
        System.runAs(TestClassDataUtil.createIntegrationUser()) {
            insert newsArticle;
        }
        Test.stopTest();

        List<Knowledge__DataCategorySelection> dataCategories = [SELECT ParentId, DataCategoryName FROM Knowledge__DataCategorySelection WHERE ParentId = :newsArticle.Id];

        System.assertEquals(1, dataCategories.size(), 'Incorrect number of data categories assigned to new article');
        System.assertEquals(DATA_CATEGORY_NAME, dataCategories[0].DataCategoryName, 'Incorrect data category assigned to article');

        List<Knowledge__kav> newsArticles = [SELECT Id, RecordTypeId FROM Knowledge__kav WHERE Id = :newsArticle.Id];

        System.assertEquals(GlobalUtility.amsNewsRecordTypeId, newsArticles[0].RecordTypeId, 'Record type not assigned based on Record_Type_Developer_Name__c');
    }
}