/**
* Description: Test for SetupAMSAdvice
* @author: Amelia (Trineo)
* @date: August 2018
*/
@isTest
public class SetupAMSAdviceTest {

    @isTest static void testCreateArticles() {
        TestClassDataUtil.integrationUserProfile();

        SetupAMSAdvice.createArticles();

        Integer articleCount = [SELECT count() FROM Knowledge__kav WHERE PublishStatus = 'Draft' AND Language = 'en_US'];
        System.assertEquals(SetupAMSAdvice.webTexts.size(), articleCount, 'Articles not created');
    }
}