/**
 * Test class for AMSPreferencesController
 *
 * Author: John (Trineo)
 * Date: October 2017
 */
@IsTest
private class AMSPreferencesControllerTest {
    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @TestSetup
    static void testSetup() {
        List<Channel_Preference_Setting__c> channelPreferenceSettings = TestClassDataUtil.createAuChannelPreferenceSettings();
        List<Account> farms = TestClassDataUtil.createAUFarms(1, true);
    }

    @IsTest
    private static void testController() {
        User u = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(u) {
            AMSPreferencesController controller = new AMSPreferencesController();

            System.assertEquals(controller.myPersonalPreferences.AUDailyMilkResult.preference.NotifyBySMS__c, true);
            System.assertEquals(controller.myPersonalPreferences.AUMilkQualityAlerts.preference.DeliveryByEmail__c, true);

            controller.myPersonalPreferences.AUDailyMilkResult.preference.NotifyBySMS__c = false;
            controller.myPersonalPreferences.AUMilkQualityAlerts.preference.DeliveryByEmail__c = false;

            controller.savePreferences();

            controller = new AMSPreferencesController();

            System.assertEquals(controller.myPersonalPreferences.AUDailyMilkResult.preference.NotifyBySMS__c, false);
            System.assertEquals(controller.myPersonalPreferences.AUMilkQualityAlerts.preference.DeliveryByEmail__c, false);
        }
    }
}