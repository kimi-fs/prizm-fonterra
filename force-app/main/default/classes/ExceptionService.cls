public with sharing class ExceptionService {

    //Make the Community Page Messages Custom Settings available to other pages
    private static Map<String, Map<String, String>> pageMessages;
    static{pageMessages = new Map<String, Map<String, String>>();}
    public static Map<String, String> getPageMessages( String pageName )
    {
        if ( pageMessages.containsKey( pageName ) ) {
            return pageMessages.get( pageName );
        }

        if(Test.isRunningTest()){

        }

        for(Community_Page_Messages__c msg : Community_Page_Messages__c.getAll().values()){
            if(!pageMessages.containsKey(msg.Page__c)){
                pageMessages.put(msg.Page__c, new Map<String, String>());
            }
            pageMessages.get(msg.Page__c).put(msg.Reason__c, msg.Message__c);
        }
        return pageMessages.get( pageName );
    }

    //Receive exceptions and pass back user friendly messages
	public static String friendlyMessage(Exception e) {
		System.debug('*** e = '+e);
        System.debug('*** e.getTypeName() = '+e.getTypeName());
        System.debug('*** e.getMessage() = '+e.getMessage());

        Map<String, String> messages = ExceptionService.getPageMessages('ExceptionService');

		if(e.getTypeName() == 'ExceptionService.CustomException'){
			if(e.getMessage().contains('Error: You cannot reuse this old password.')){
				return 'You cannot reuse this old password, please try again';
			}
			else if(e.getMessage().contains('invalid repeated password')){
                return 'This password has been used recently, you are unable to use any of your last 3 passwords, please choose another';
            }
            else if(e.getMessage().contains('Your password cannot be easy to guess')){
                return 'Your password cannot be easy to guess. Please choose a different one.';
            }
            else if(e.getMessage().contains('regex vaildation error')){
                return messages.get('Username must be in the form of an email address');
            }
			return e.getMessage();
		}

		else if(e.getTypeName() == 'Site.ExternalUserCreateException'){
			if(e.getMessage().contains('User already exists')){
                return 'We have an active user for the details you have provided, please use the Forgot Username or Forgot Password options to recover your user account';
            }
            else if(e.getMessage().contains('portal user already exists for contact')){
                return 'We have an active user for the details you have provided, please use the Forgot Username or Forgot Password options to recover your user account';
            }
            else if(e.getMessage().contains('You are already logged in')){
                return 'You are already logged in. If you would like to change you details please view the My Details page or contact the Service Centre on 0800 65 65 68.<br /><br /> If you would like register a new account please log out from this account first.';
            }
		}

        else if(e.getTypeName() == 'System.DmlException'){
            if(e.getMessage().contains('Username must be in the form of an email address')){
                return 'There was an error setting your new Username, if you have included the "@" symbol please make sure your new Username is in the form of an email address (for example, john@acme.com)';
            }
            else if(e.getMessage().contains('STRING_TOO_LONG, Country Code:')){
                return 'The Country Code value is limited to 20 characters, if you have entered New Zealand please try NZ.';
            }
            else if(e.getMessage().contains('STRING_TOO_LONG, Postal Zip/Postal Code:')){
                return 'The Post Code value is limited to 20 characters.';
            }
            else if(e.getMessage().contains('DUPLICATE_USERNAME')){
                return 'That Username is already taken, please try another';
            }
            else if(e.getMessage().contains('INVALID_EMAIL_ADDRESS')){
                return 'The email you entered is not a valid, please try again';
            }
            else if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION, Invalid format entered - please enter in the format +64 3 7005550: [Phone]')){
                return 'Please enter the phone number in the following format +64 3 7005550.';
            }
        }

        else if(e.getTypeName() == 'System.EmailException'){
            if(e.getMessage().contains('Missing target address')){
                return 'The email is missing a Recipient, please add a Recipient and try again.';
            }
        }

		if(e.getMessage().contains('INVALID_NEW_PASSWORD:')){
            if(e.getMessage().contains('Your password must be at least')){
                return 'This password does not meet the minimum length requirements';
            }
            else if(e.getMessage().contains('Your password must have a mix of letters and numbers')){
                return 'Your password must have a mix of letters and numbers';
            }
            else if(e.getMessage().contains('Your password cannot be easy to guess')){
                return 'Your password cannot be easy to guess. Please choose a different one.';
            }
		}

        if(e.getMessage().contains('invalid repeated password')){
            return 'This password has been used recently, you are unable to use any of your last 3 passwords, please choose another';
        }

		return e.getMessage();
	}

	public class CustomException extends Exception{

	}
}