/**
 * Description: Test for AMSForgotController
 * @author: Amelia (Trineo)
 * @date: August 2017
 */
@isTest
private class AMSForgotControllerTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';

    private static final Map<String, Community_Page_Messages__c> PAGE_MESSAGES = new Map<String, Community_Page_Messages__c>();
    static {
        String PAGE_NAME = 'AMSForgot';
        PAGE_MESSAGES.put('missing-email', new Community_Page_Messages__c(Name = PAGE_NAME+'1', Page__c = PAGE_NAME, Reason__c = 'missing-email', Message__c = 'missing-email'));
        PAGE_MESSAGES.put('email-sent', new Community_Page_Messages__c(Name = PAGE_NAME+'2', Page__c = PAGE_NAME, Reason__c = 'email-sent', Message__c = 'email-sent'));
        PAGE_MESSAGES.put('unknown-error', new Community_Page_Messages__c(Name = PAGE_NAME+'3', Page__c = PAGE_NAME, Reason__c = 'unknown-error', Message__c = 'unknown-error'));
        PAGE_MESSAGES.put('unknown-user', new Community_Page_Messages__c(Name = PAGE_NAME+'4', Page__c = PAGE_NAME, Reason__c = 'unknown-user', Message__c = 'unknown-user'));
    }

    @testSetup static void testSetup(){
        upsert PAGE_MESSAGES.values();
        TestClassDataUtil.individualDefaultAccountAU();
    }

    @isTest static void testResetPassword_ValidUser(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser){
            AMSForgotController controller = new AMSForgotController();
            controller.email = USER_EMAIL;

            Test.startTest();
            controller.resetPassword();
            Test.stopTest();

            //Site.forgotPassword always returns null in test context therefore no assertions can be made.
        }
    }

    @isTest static void testResetPassword_NoEmailInput(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser){
            AMSForgotController controller = new AMSForgotController();

            Test.startTest();
            controller.resetPassword();
            Test.stopTest();

            System.assertEquals(PAGE_MESSAGES.get('missing-email').Message__c, controller.message, 'Message not shown');
        }
    }

    @isTest static void testResetPassword_InvalidEmailInput(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        User guestUser = TestClassDataUtil.getAMSGuestUser();

        System.runAs(guestUser){
            AMSForgotController controller = new AMSForgotController();
            controller.email = 'I am not a valid email';

            Test.startTest();
            controller.resetPassword();
            Test.stopTest();

            System.assertEquals(PAGE_MESSAGES.get('unknown-user').Message__c, controller.message, 'Message not shown');
        }
    }

    @isTest static void testGetUserEmailDomain(){
        AMSForgotController controller = new AMSForgotController();
        controller.email = 'test@test.com';
        System.assertEquals('@test.com', controller.getUserEmailDomain(), 'Wrong email domain returned');
    }

    @isTest static void testRedirectToLogin(){
        System.assert(TestClassDataUtil.checkPageReferenceMatches(Page.AMSLogin, AMSForgotController.redirectToLogin()), 'Page did not redirect');
    }

}