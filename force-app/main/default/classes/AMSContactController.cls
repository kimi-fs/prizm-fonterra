/**
 * Description: Controller for displaying the my contacts, and also for handling the contact us form for creating a Case.
 * @author: Ed (Trineo)
 * @date: July 2017
 * @test: AMSLoginController_Test
 */
public with sharing class AMSContactController {
    public transient Map<String, List<AMSContactService.ContactUsWrapper>> contactUs { get; set; }
    public transient List<AMSContactService.ContactUsWrapper> areaManagers { get; set; }
    public transient List<AMSContactService.ContactUsWrapper> qualitySpecialists { get; set; }
    public transient List<AMSContactService.ContactUsWrapper> inboundServiceSpecialists { get; set; }

    public transient Boolean noContacts { get; set; }

    // enquiry elements
    public List<SelectOption> feedbackTypeOptions { get; private set; }
    public List<SelectOption> farmOptions { get; private set; }

    public String feedbackType { get; set; }
    public String farm { get; set; }
    public String enquiryText { get; set; }

    public Boolean submitted { get; set; }

    // use the same page messages for AMSContact and AMSEnquiry
    public String message {get; private set;}
    private static Map<String, String> messages;
    static {
        messages = AMSCommunityUtil.initialisePageMessages('AMSContact');
    }

    private static final String AMS_COMMUNITY_CASE_ACK_EMAIL_TEMPLATE_DEVELOPERNAME = 'AMS_Community_Case_Created_Ack';

    public AMSContactController() {
        // Initialisation is done via the initialise action functions depending on the contact us page using this
        // controller.
        this.message = '';
    }

    /**
     * If we are being used on the contacts page initialise the contacts for this user
     */
    public void initialiseContacts() {
        this.contactUs = AMSContactService.getContactUsContacts(AMSContactService.determineContactId());

        this.areaManagers = contactUs.get(AMSContactService.AREA_MANAGER);
        this.qualitySpecialists = contactUs.get(AMSContactService.QUALITY_SPECIALIST);
        this.inboundServiceSpecialists = contactUs.get(AMSContactService.INBOUND_SERVICE_SPECIALIST);

        if ((this.areaManagers == null || this.areaManagers.isEmpty()) &&
            (this.qualitySpecialists == null || this.qualitySpecialists.isEmpty()) &&
            (this.inboundServiceSpecialists == null || this.inboundServiceSpecialists.isEmpty())) {
            this.noContacts = true;
        } else {
            this.noContacts = false;
        }


    }

    /**
     * If we are being used on the contact enquiry page initialise the types of feedback and the farms for this user
     */
    public void initialiseEnquiry() {
        this.feedbackTypeOptions = new List<SelectOption>();
        this.farmOptions = new List<SelectOption>();

        this.feedbackTypeOptions.add(new SelectOption('General', 'General Feedback'));
        this.feedbackTypeOptions.add(new SelectOption('Issue', 'Complaint'));
        this.feedbackTypeOptions.add(new SelectOption('Issue', 'Service Query'));

        List<Account> farms = RelationshipAccessService.accessToFarms(AMSContactService.determineContactId());

        // Populate the Farm Options from that list - it is a required field, they *must* specify
        // a farm that it is related to, use the No Entity AU Account if it isn't farm specific
        this.farmOptions.add(new SelectOption(GlobalUtility.defaultAccountIdAU, 'Not farm specific'));
        for (Account f : farms) {
            this.farmOptions.add(new SelectOption(f.Id, f.Name));
        }
        this.submitted = false;
    }

    public PageReference cancel() {
        newCase();
        return null;
    }

    /**
     * Create and insert the Case. All Cases are going in as type Website (which seems a bit strange).
     */
    public PageReference save() {
        if (validateEnquiry()) {
            User me = AMSUserService.getUser();
            Case enquiry = new Case(AccountId = this.farm,
                                    ContactId = me.ContactId,
                                    RecordTypeId = GlobalUtility.caseDigitalServicesAURecordTypeId,
                                    Type = 'Website',
                                    Sub_Type__c = this.feedBackType,
                                    Origin = 'Web',
                                    Status = 'New',
                                    Priority = 'Normal',
                                    Description = this.enquiryText,
                                    Raised_By__c = 'External',
                                    Sentiment__c = 'NA');
            try {
                AMSCaseService.insertCase(enquiry);
                sendAcknowledgement(enquiry);
                newCase();
                this.submitted = true;
            } catch (Exception e) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, messages.get('case-create-error') + ' [' + ExceptionService.friendlyMessage(e) + ']'));
            }
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, '' + messages.get('invalid-input')));
        }
        return null;
    }

    public PageReference newCase() {
        this.submitted = false;
        this.feedbackType = feedbackTypeOptions[0].getValue();
        this.farm = farmOptions[0].getValue();
        this.enquiryText = '';
        return null;
    }

    private void sendAcknowledgement(Case enquiry) {
        OrgWideEmailAddress owea = GlobalUtility.ORG_WIDE_EMAIL_ADDRESS_FARM_SOURCE_AU;
        Id orgWideEmailId = owea.Id;

        this.message = '';
        try {
            User targetUser = AMSUserService.getUser();
            EmailTemplate emailTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = : AMS_COMMUNITY_CASE_ACK_EMAIL_TEMPLATE_DEVELOPERNAME LIMIT 1];

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setTargetObjectId(targetUser.ContactId);
            email.setTemplateId(emailTemplate.Id);
            email.setSaveAsActivity(true);
            email.setOrgWideEmailAddressId(orgWideEmailId);
            List<Messaging.SendEmailResult> emailResults = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});

            if (!emailResults.isEmpty() && emailResults[0].isSuccess()) {
                // all good.
            } else {
                this.message = messages.get('unknown-error');
            }
        } catch (Exception e) {
            this.message = messages.get('unknown-error') + ' [' + e.getMessage() + ']';
        }
    }

    private Boolean validateEnquiry() {
        Boolean valid = false;

        if (!String.isEmpty(this.farm) &&
                !String.isEmpty(this.feedbackType) &&
                !String.isEmpty(this.enquiryText)) {
            valid = true;
        }
        return valid;
    }

}