/**
 * Description: Changes the password of the logged in user
 * @author: Amelia (Trineo)
 * @date: August 2017
 */
public with sharing class AMSPasswordResetController {
    public String newPassword { get; set; }
    public String confirmPassword { get; set; }

    private User user;

    public String message { get; set; }

    private static Map<String, String> messages;
    static {
        messages = ExceptionService.getPageMessages('AMSPasswordReset');
    }

    public AMSPasswordResetController() {
        message = '';
        user = AMSUserService.getUser();
    }

    public PageReference resetPassword() {
        message = '';

        Boolean validPassword = true;
        if (!AMSUserService.passwordMatches(this.newPassword, this.confirmPassword)) {
            message = messages.get('password-not-matching');
            validPassword = false;
        }

        if (!AMSUserService.isValidPassword(this.newPassword)) {
            message = messages.get('invalid-password');
            validPassword = false;
        }

        if (validPassword) {
            try{
                if (!Test.isRunningTest()) {
                    System.setPassword(user.Id, newPassword);
                }

                if (user.Contact.FarmSourceONE_Setup_Complete__c) {
                    return Page.AMSDashboard;
                } else {
                    AMSUserService.updateEmailVerified(user);
                    return AMSUserService.firstTimeRegistrationNextPage(user);
                }
            } catch (Exception e) {
                message = ExceptionService.friendlyMessage(e);
                return null;
            }
        } else {
            return null;
        }
    }
}