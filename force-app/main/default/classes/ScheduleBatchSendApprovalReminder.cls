/**
* Description:  Sends approval reminders every 24 hours, once 24 hours have lapsed
* @author  John Au
* @date  02/03/2021
* @test ScheduleBatchSendApprovalReminderTest
**/

public without sharing class ScheduleBatchSendApprovalReminder implements Schedulable, Database.Batchable<Case>, Database.Stateful  {

    private static final String SCHED_JOB_NAME = ScheduleService.getCRONJobName('ScheduleBatchSendApprovalReminder');
    private static final String APPROVAL_REMINDER_TEMPLATE_NAME = 'Approval_Reminder';

    private Map<Id,String> errorMap = new Map<Id, String>();

    public static void schedule() {
        String expression = ScheduleService.getCRONExpression('ScheduleBatchSendApprovalReminder');
        if (String.isNotBlank(expression)) {
            final ScheduleBatchSendApprovalReminder job = new ScheduleBatchSendApprovalReminder();
            System.schedule(SCHED_JOB_NAME, expression, job);
        }
    }

    public static void abort() {
        ScheduleService.terminate(SCHED_JOB_NAME);
    }

    public void execute(SchedulableContext schedulableContext) {
        Database.executeBatch(new ScheduleBatchSendApprovalReminder(), 1);
    }

    public List<Case> start(Database.BatchableContext batchableContext) {
        return [SELECT Id, ContactId, AccountId, (SELECT ActorId, ElapsedTimeInHours From ProcessSteps WHERE StepStatus = 'Pending') FROM Case WHERE Approval_Status__c = 'Pending Approval'];
    }

    public void execute(Database.BatchableContext batchableContext, List<Case> cases) {
        EmailTemplate emailTemplate = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :APPROVAL_REMINDER_TEMPLATE_NAME LIMIT 1];

        for (Case c : cases) {
            if (is24HoursSinceApprovalPending(c.ProcessSteps)) {
                sendApprovalEmail(c, emailTemplate);
            }
        }
    }

    private static Boolean is24HoursSinceApprovalPending(List<ProcessInstanceHistory> processInstanceHistoryList) {
        for (ProcessInstanceHistory processInstanceHistory : processInstanceHistoryList) {
            Integer elapsedTime = Integer.valueOf(processInstanceHistory.ElapsedTimeInHours);

            return Math.mod(elapsedTime, 24) == 0;
        }

        return false;
    }

    private static void sendApprovalEmail(Case c, EmailTemplate emailTemplate) {
        OrgWideEmailAddress owea = GlobalUtility.ORG_WIDE_EMAIL_ADDRESS_FARM_SOURCE_AU;
        Id orgWideEmailId = owea.Id;

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTargetObjectId(getCaseContactId(c));
        email.setWhatId(c.Id);
        email.setTreatTargetObjectAsRecipient(false);
        email.setTemplateId(emailTemplate.Id);
        email.setSaveAsActivity(true);
        email.setToAddresses(new String[]{owea.Address});
        email.setOrgWideEmailAddressId(orgWideEmailId);
        List<Messaging.SendEmailResult> emailResults = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }

    private static Id getCaseContactId(Case c){
        if(c.ContactId != null) {
            return c.ContactId;
        } else {
            Account a = [SELECT Id, Primary_Business_Contact__c FROM Account WHERE Id = :c.AccountId];
            return a.Primary_Business_Contact__c;
        }
    }

    public void finish(Database.BatchableContext batchableContext) {

    }
}