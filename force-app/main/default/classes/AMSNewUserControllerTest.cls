/**
 * Description: Test for AMSNewUserController
 * @author: Amelia (Trineo)
 * @date: July 2017
 */
@isTest
private class AMSNewUserControllerTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';

    private static final String FIRST_NAME = 'Firstname';
    private static final String LAST_NAME = 'Lastname';
    private static final String PHONE_NUMBER = '123456';
    private static final String PASSWORD = 'Pa$$w0rd!';
    private static final String EMAIL = 'testEmail@thisTest.com';

    private static final Map<String, Community_Page_Messages__c> PAGE_MESSAGES = new Map<String, Community_Page_Messages__c>();
    static {
        SetupAMSCommunityPageMessages.createPageMessages();
    }

    @testSetup static void testSetup(){
        upsert PAGE_MESSAGES.values();

        TestClassDataUtil.individualDefaultAccountAU();
    }

    @isTest static void testConstructor(){
        User guestUser = TestClassDataUtil.getAMSGuestUser();
        System.runAs(guestUser){
            Test.startTest();
            AMSNewUserController controller = new AMSNewUserController();
            Test.stopTest();
            System.assertEquals('', controller.firstNameError);
            System.assertEquals('', controller.lastNameError);
            System.assertEquals('', controller.phoneError);
            System.assertEquals('', controller.emailError);
            System.assertEquals('', controller.passwordError);
        }
    }

    @isTest static void testRegisterUser_WithValidInputs(){
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        AMSNewUserController controller = new AMSNewUserController();
        controller.firstname = FIRST_NAME;
        controller.lastname = LAST_NAME;
        controller.phone = PHONE_NUMBER;
        controller.email = EMAIL;
        controller.password = PASSWORD;
        controller.confirmPassword = PASSWORD;

        System.runAs(guestUser){
            Test.startTest();
            PageReference pr = controller.registerUser();
            Test.stopTest();
        }
    }

    @isTest static void testRegisterUser_MissingFirstName(){
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        AMSNewUserController controller = new AMSNewUserController();
        controller.firstname = '';
        controller.lastname = LAST_NAME;
        controller.phone = PHONE_NUMBER;
        controller.email = EMAIL;
        controller.password = PASSWORD;
        controller.confirmPassword = PASSWORD;

        System.runAs(guestUser){
            Test.startTest();
            controller.registerUser();
            Test.stopTest();

            System.assert(String.isNotBlank(controller.firstNameError), 'Error missing');
        }
    }
    @isTest static void testRegisterUser_InvalidFirstName(){
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        AMSNewUserController controller = new AMSNewUserController();
        controller.firstname = '&';
        controller.lastname = LAST_NAME;
        controller.phone = PHONE_NUMBER;
        controller.email = EMAIL;
        controller.password = PASSWORD;
        controller.confirmPassword = PASSWORD;

        System.runAs(guestUser){
            Test.startTest();
            controller.registerUser();
            Test.stopTest();

            System.assert(String.isNotBlank(controller.message), 'Error missing');
        }
    }
    @isTest static void testRegisterUser_MissingLastName(){
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        AMSNewUserController controller = new AMSNewUserController();
        controller.firstname = FIRST_NAME;
        controller.lastname = '';
        controller.phone = PHONE_NUMBER;
        controller.email = EMAIL;
        controller.password = PASSWORD;
        controller.confirmPassword = PASSWORD;

        System.runAs(guestUser){
            Test.startTest();
            controller.registerUser();
            Test.stopTest();

            System.assert(String.isNotBlank(controller.lastNameError), 'Error missing');
        }
    }

       @isTest static void testRegisterUser_MissingPhone(){
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        AMSNewUserController controller = new AMSNewUserController();
        controller.firstname = FIRST_NAME;
        controller.lastname = LAST_NAME;
        controller.phone = '';
        controller.email = EMAIL;
        controller.password = PASSWORD;
        controller.confirmPassword = PASSWORD;

        System.runAs(guestUser){
            Test.startTest();
            controller.registerUser();
            Test.stopTest();

            System.assert(String.isNotBlank(controller.phoneError), 'Error missing');
        }
    }
    @isTest static void testRegisterUser_InvalidEmail(){
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        AMSNewUserController controller = new AMSNewUserController();
        controller.firstname = FIRST_NAME;
        controller.lastname = LAST_NAME;
        controller.phone = PHONE_NUMBER;
        controller.email = 'i am not an email';
        controller.password = PASSWORD;
        controller.confirmPassword = PASSWORD;

        System.runAs(guestUser){
            Test.startTest();
            controller.registerUser();
            Test.stopTest();

            System.assert(String.isNotBlank(controller.message), 'Error missing');
        }
    }
    @isTest static void testRegisterUser_NoEmail(){
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        AMSNewUserController controller = new AMSNewUserController();
        controller.firstname = FIRST_NAME;
        controller.lastname = LAST_NAME;
        controller.phone = PHONE_NUMBER;
        controller.email = '';
        controller.password = PASSWORD;
        controller.confirmPassword = PASSWORD;

        System.runAs(guestUser){
            Test.startTest();
            controller.registerUser();
            Test.stopTest();

            System.assert(String.isNotBlank(controller.emailError), 'Error missing');
        }
    }
    @isTest static void testRegisterUser_MismatchingPasswords(){
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        AMSNewUserController controller = new AMSNewUserController();
        controller.firstname = FIRST_NAME;
        controller.lastname = LAST_NAME;
        controller.phone = PHONE_NUMBER;
        controller.email = EMAIL;
        controller.password = PASSWORD;
        controller.confirmPassword = 'mismatching password';

        System.runAs(guestUser){
            Test.startTest();
            controller.registerUser();
            Test.stopTest();

            System.assert(String.isNotBlank(controller.passwordError), 'Error missing');
        }
    }
    @isTest static void testRegisterUser_InvalidPasswords(){
        String INVALID_PASSWORD = '1';
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        AMSNewUserController controller = new AMSNewUserController();
        controller.firstname = FIRST_NAME;
        controller.lastname = LAST_NAME;
        controller.phone = PHONE_NUMBER;
        controller.email = EMAIL;
        controller.password = INVALID_PASSWORD;
        controller.confirmPassword = INVALID_PASSWORD;

        System.runAs(guestUser){
            Test.startTest();
            controller.registerUser();
            Test.stopTest();

            System.assert(String.isNotBlank(controller.passwordError), 'Error missing');
        }
    }

    @isTest static void testRegisterUser_ExistingUser(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        AMSNewUserController controller = new AMSNewUserController();
        controller.firstname = FIRST_NAME;
        controller.lastname = LAST_NAME;
        controller.phone = PHONE_NUMBER;
        controller.email = USER_EMAIL;
        controller.password = PASSWORD;
        controller.confirmPassword = PASSWORD;

        System.runAs(guestUser){
            Test.startTest();
            PageReference pr = controller.registerUser();
            Test.stopTest();

            System.assert(String.isNotBlank(controller.message), 'Error missing');
        }
    }

    @isTest static void testRegisterUser_ExistingContact(){
        Contact existingContact = TestClassDataUtil.createIndividualAU(false);
        existingContact.Email = USER_EMAIL;
        insert existingContact;

        User guestUser = TestClassDataUtil.getAMSGuestUser();

        AMSNewUserController controller = new AMSNewUserController();
        controller.firstname = FIRST_NAME;
        controller.lastname = LAST_NAME;
        controller.phone = PHONE_NUMBER;
        controller.email = USER_EMAIL;
        controller.password = PASSWORD;
        controller.confirmPassword = PASSWORD;

        System.runAs(guestUser){
            Test.startTest();
            PageReference pr = controller.registerUser();
            Test.stopTest();
        }
    }

    @isTest static void testRetryValidation(){
        User guestUser = TestClassDataUtil.getAMSGuestUser();

        AMSNewUserController controller = new AMSNewUserController();

        System.runAs(guestUser){
            Test.startTest();
            controller.retryValidation();
            Test.stopTest();

            System.assert(String.isNotBlank(controller.message), 'Error missing');
        }
    }

}