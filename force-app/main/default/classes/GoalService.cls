/**
 * GoalService.cls
 * Description: Service for Farmsource Professionals goal and actions
 * @author: John Au
 * @date: December 2018
 */
public without sharing class GoalService {

    private static final List<String> GOAL_FIELDS_TO_QUERY = new List<String> {
        'Name',
        'Archived__c',
        'Category__c',
        'Completed_Date__c',
        'Description__c',
        'Farm__c',
        'Farm__r.Name',
        'Goal_Date__c',
        'Individual__c',
        'Individual__r.Name',
        'Party__c',
        'Party__r.Name',
        'Visible_in_Farmsource__c',
        'Visible_to_Farm__c',
        'Visible_to_Party__c',
        '(SELECT Id FROM Actions__r WHERE Source_Status__c != \'Completed\' AND Visible_in_Farmsource__c = true)'
    };

    private static final List<String> ACTION_FIELDS_TO_QUERY = new List<String> {
        'Id',
        'Goal__c',
        'Goal__r.Name',
        'Action_Name__c',
        'Description__c',
        'Category__c',
        'Assigned_To__c',
        'Completion_Date__c',
        'Target_Completion_Date__c',
        'Source_Status__c',
        'Visible_in_Farmsource__c'
    };

    private static final List<String> FEED_ITEM_FIELDS_TO_QUERY = new List<String> {
        'Id',
        'ParentId',
        'CreatedBy.Id',
        'CreatedBy.Name',
        'CreatedDate',
        'Body'
    };

    public static List<Goal__c> getGoalsForIndividual(Id individualId) {
        return getGoalsForIndividual(individualId, false);
    }

    public static List<Goal__c> getGoalsForIndividual(Id individualId, Boolean archived) {
        List<Individual_Relationship__c> individualRelationships = RelationshipAccessService.allIndividualRelationshipsForIndividual(individualId);

        Set<Id> farmIds = new Set<Id>();
        Set<Id> partyIds = new Set<Id>();

        for (Individual_Relationship__c individualRelationship : individualRelationships) {
            if (individualRelationship.Farm__c != null) {
                farmIds.add(individualRelationship.Farm__c);
            }
            if (individualRelationship.Party__c != null) {
                partyIds.add(individualRelationship.Party__c);
            }
        }

        return getGoalsForFarmPartyOrIndividual(farmIds, partyIds, individualId, archived);
    }

    public static Goal__c getGoal(Id goalId) {
        String query = constructGoalQuery('Id = :goalId');

        return Database.query(query);
    }

    private static List<Goal__c> getGoalsForFarmPartyOrIndividual(Set<Id> farmIds, Set<Id> partyIds, Id individualId, Boolean archived) {
        String query = constructGoalQuery('((Farm__c IN :farmIds AND Visible_to_Farm__c = true) OR (Party__c IN :partyIds AND Visible_to_Party__c = true) OR Individual__c = :individualId) AND Archived__c = :archived');

        return Database.query(query);
    }

    private static String constructGoalQuery(String criteria) {
        String query = '';

        query += 'SELECT ';
        query += String.join(GOAL_FIELDS_TO_QUERY, ',');
        query += ' FROM Goal__c';
        query += ' WHERE Visible_in_Farmsource__c = true';

        if (!String.isBlank(criteria)) {
            query += ' AND ' + criteria;
        }

        query += ' ORDER BY Goal_Date__c DESC';

        return query;
    }

    public static List<Action__c> getActionsForGoals(List<Goal__c> goals) {
        String query = constructActionQuery('(Goal__c IN :goals)');

        return Database.query(query);
    }

    public static Action__c getActionDetails(Id actionId) {
        String query = constructActionQuery('Id = :actionId');

        return Database.query(query);
    }

    private static String constructActionQuery(String criteria) {
        String query = '';

        query += 'SELECT ';
        query += String.join(ACTION_FIELDS_TO_QUERY, ',');
        query += ' FROM Action__c';
        query += ' WHERE Visible_in_Farmsource__c = true';

        if (!String.isBlank(criteria)) {
            query += ' AND ' + criteria;
        }

        query += ' ORDER BY Target_Completion_Date__c DESC';

        return query;
    }

    public static void updateAction(Action__c action, List<String> fieldsToUpdate) {
        Action__c actionToUpdate = new Action__c(Id = action.Id);

        for (String fieldToUpdate : fieldsToUpdate) {
            actionToUpdate.put(fieldToUpdate, action.get(fieldToUpdate));
        }

        update actionToUpdate;
    }

    public static Action__c setActionStatus(Action__c action, String status, Date completionDate) {
        action.Source_Status__c = status;
        action.Completion_Date__c = completionDate;

        List<String> fieldsToUpdate = new List<String> { 'Source_Status__c', 'Completion_Date__c' };

        updateAction(action, fieldsToUpdate);

        return action;
    }

    public static Action__c setActionAssignedTo(Action__c action, String assignedTo) {
        action.Assigned_To__c = assignedTo;

        List<String> fieldsToUpdate = new List<String> { 'Assigned_To__c' };

        updateAction(action, fieldsToUpdate);

        return action;
    }

    private static String constructFeedItemQuery(String criteria, String orderBy, Integer resultLimit) {
        String query = '';

        query += 'SELECT ';
        query += String.join(FEED_ITEM_FIELDS_TO_QUERY, ',');
        query += ' FROM FeedItem';
        query += ' WHERE Type = \'TextPost\'';

        if (!String.isBlank(criteria)) {
            query += ' AND ' + criteria;
        }

        query += ' ORDER BY CreatedDate ' + orderBy;

        if (resultLimit != null) {
            query += ' LIMIT ' + resultLimit;
        }

        return query;
    }

    public static List<FeedItemWrapper> getFeedItemsForGoalsAndActionsForIndividual(Id individualId) {
        List<Goal__c> goals = getGoalsForIndividual(individualId);

        Set<Id> goalAndActionIds = new Set<Id>();

        for (Goal__c goal : goals) {
            goalAndActionIds.add(goal.Id);

            for (Action__c action : goal.Actions__r) {
                goalAndActionIds.add(action.Id);
            }
        }

        return getFeedItemsForObjects(goalAndActionIds);
    }

    public static List<FeedItemWrapper> getFeedItemsForObject(Id objectId) {
        return getFeedItemsForObjects(new Set<Id> { objectId }, 'ASC', null);
    }

    public static List<FeedItemWrapper> getFeedItemsForObjects(Set<Id> objectIds) {
        return getFeedItemsForObjects(objectIds, 'DESC', null);
    }

    public static List<FeedItemWrapper> getFeedItemsForObjects(Set<Id> objectIds, String orderBy, Integer resultLimit) {
        return getFeedItemsForObjects(objectIds, orderBy, resultLimit, null);
    }

    public static List<FeedItemWrapper> getFeedItemsForObjects(Set<Id> objectIds, String orderBy, Integer resultLimit, Id userIdToExclude) {
        String query = 'ParentId IN :objectIds';

        if (userIdToExclude != null) {
            query += ' AND CreatedById != :userIdToExclude';
        }

        List<FeedItemWrapper> feedItemWrappers = new List<FeedItemWrapper>();
        List<FeedItem> feedItems = Database.query(constructFeedItemQuery(query, orderBy, resultLimit));

        for (FeedItem feedItem : feedItems) {
            FeedItemWrapper wrapper = new FeedItemWrapper(feedItem);

            feedItemWrappers.add(wrapper);
        }

        return feedItemWrappers;
    }

    public without sharing class FeedItemWrapper {
        public Id parentId {
            get;
            set {
                this.parentId = value;
                this.parentSObjectType = String.valueOf(value.getSObjectType());
            }
        }
        public String parentName { get; set; }
        public String parentSObjectType { get; set; }
        public FeedItem feedItem { get; set; }
        public String formattedCreatedDate { get; set; }

        public FeedItemWrapper(Id parentId) {
            this.parentId = parentId;
            this.feedItem = new FeedItem(ParentId = this.parentId);
        }

        public FeedItemWrapper(FeedItem feedItem) {
            this.parentId = feedItem.ParentId;
            this.feedItem = feedItem;

            setCreatedDateToUsersTimezone();
        }

        public Boolean getEditAllowed() {
            Boolean editAllowed = false;

            if (this.feedItem != null) {
                if (this.feedItem.CreatedBy.Id == UserInfo.getUserId()) {
                    editAllowed = true;
                }
            }

            return editAllowed;
        }

        private void setCreatedDateToUsersTimezone() {
            formattedCreatedDate = this.feedItem.CreatedDate.format('d MMMMM yyyy h:mma', UserInfo.getTimeZone().getID());
        }

        public void save() {
            upsert (this.feedItem);
        }
    }

}