/**
 * Service for accessing collection summary records
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately.
 *
 * Author: Amelia (Trineo)
 * Date: October 2017
 */
 public without sharing class AMSCollectionSummaryService {
    private static List<String> collectionSummaryFields = new List<String>{
        'Record_Type_Developer_Name__c',
        'Farm_ID__c',
        'Collection_Summary_Date__c',
        'Fat_Kgs__c',
        'Fat_Percent__c',
        'Prot_Kgs__c',
        'Prot_Percent__c',
        'Quality_Points__c',
        'Stop_1__c',
        'Stop_2__c',
        'Total_KgMS__c',
        'Total_KgMS_Produced_by_Linked_Farms__c',
        'Volume_Ltrs__c',
        'Volume_Charge_Rate__c'
    };

    public static List<Collection_Summary__c> getCollectionSummaries(String farmId, Date seasonStart, Date seasonEnd){
        String query =  'SELECT ' + String.join(collectionSummaryFields, ',') +
                        ' FROM Collection_Summary__c ' +
                        ' WHERE Farm_ID__c = :farmId ' +
                        ' AND RecordTypeId = \'' + GlobalUtility.monthlyCollectionSummaryRecordTypeId + '\'' +
                        ' AND Collection_Summary_Date__c >= :seasonStart ' +
                        ' AND Collection_Summary_Date__c <= :seasonEnd ' +
                        ' ORDER BY Collection_Summary_Date__c ASC ';

        List<Collection_Summary__c> collectionSummaries = Database.query(query);
        return collectionSummaries;
    }

    public static List<Collection_Summary__c> getCollectionSummariesLastFourteenMonths(String farmId){
        Date yearAgo = Date.today().addMonths(-14);
        String query =  'SELECT ' + String.join(collectionSummaryFields, ',') +
                        ' FROM Collection_Summary__c ' +
                        ' WHERE Farm_ID__c = :farmId ' +
                        ' AND RecordTypeId = \'' + GlobalUtility.monthlyCollectionSummaryRecordTypeId + '\'' +
                        ' AND Collection_Summary_Date__c >= :yearAgo ' +
                        ' ORDER BY Collection_Summary_Date__c ASC ';

        List<Collection_Summary__c> collectionSummaries = Database.query(query);
        return collectionSummaries;
    }
}