/**
 * Description: Test for AMSPasswordChangeController
 * @author: Amelia (Trineo)
 * @date: August 2018
 */
@isTest
private class AMSPasswordChangeControllerTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';
    private static final String PASSWORD1 = 'Pa$$w0rd!1';
    private static final String PASSWORD2 = 'Pa$$w0rd!2';
    private static final String INVALID_PASSWORD = 'invalid';

    @testSetup static void testSetup() {
        SetupAMSCommunityPageMessages.createPageMessages();
        TestClassDataUtil.individualDefaultAccountAU();
    }

    @isTest static void testConstructor() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(communityUser) {
            AMSPasswordChangeController cont = new AMSPasswordChangeController();

            System.assertEquals(false, cont.passwordUpdated, 'Incorrect value of passwordUpdated');
        }
    }

    @isTest static void testConstructor_DifferentContact() {
        User guestUser = TestClassDataUtil.getAMSGuestUser();
        // User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(guestUser) {
            AMSPasswordChangeController cont = new AMSPasswordChangeController();
            System.assertNotEquals('', cont.errorMessage, 'Incorrect message');
        }
    }

    @isTest static void testRedirectToMyDetails() {
        System.assert(TestClassDataUtil.checkPageReferenceMatches(Page.AMSMyDetails, AMSPasswordChangeController.redirectToMyDetails()), 'Incorrect redirect');
    }

    @isTest static void testUpdatePassword_NoPageMessages() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(communityUser) {
            AMSPasswordChangeController cont = new AMSPasswordChangeController();
            cont.currentPassword = PASSWORD1;
            cont.newPassword = PASSWORD2;
            cont.confirmPassword = PASSWORD2;

            cont.updatePassword();

            System.assertEquals(true, cont.passwordUpdated, 'Incorrect value of passwordUpdated');
        }
    }

    @isTest static void testUpdatePassword_InvalidPassword() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(communityUser) {
            AMSPasswordChangeController cont = new AMSPasswordChangeController();
            cont.currentPassword = PASSWORD1;
            cont.newPassword = INVALID_PASSWORD;
            cont.confirmPassword = INVALID_PASSWORD;

            cont.updatePassword();

            System.assertNotEquals('', cont.errorMessage, 'Incorrect message');
            System.assertEquals(false, cont.passwordUpdated, 'Incorrect value of passwordUpdated');
        }
    }

    @isTest static void testUpdatePassword_MismatchedPassword() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(communityUser) {
            AMSPasswordChangeController cont = new AMSPasswordChangeController();
            cont.currentPassword = PASSWORD1;
            cont.newPassword = PASSWORD2;
            cont.confirmPassword = INVALID_PASSWORD;

            cont.updatePassword();

            System.assertNotEquals('', cont.errorMessage, 'Incorrect message');
            System.assertEquals(false, cont.passwordUpdated, 'Incorrect value of passwordUpdated');
        }
    }

    @isTest static void testUpdatePassword_InvalidOldPassword() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(communityUser) {
            // Coverage hack because Site.changePassword won;t run in tests
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Your old password is invalid.'));

            AMSPasswordChangeController cont = new AMSPasswordChangeController();
            cont.updatePassword();

            System.assertNotEquals('', cont.errorMessage, 'Incorrect message');
            System.assertEquals(false, cont.passwordUpdated, 'Incorrect value of passwordUpdated');
        }
    }

    @isTest static void testUpdatePassword_MissingCurrentPassword() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(communityUser) {
            // Coverage hack because Site.changePassword won;t run in tests
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Your old password cannot be null'));

            AMSPasswordChangeController cont = new AMSPasswordChangeController();
            cont.updatePassword();

            System.assertNotEquals('', cont.errorMessage, 'Incorrect message');
            System.assertEquals(false, cont.passwordUpdated, 'Incorrect value of passwordUpdated');
        }
    }

    @isTest static void testUpdatePassword_OtherError() {
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        System.runAs(communityUser) {
            // Coverage hack because Site.changePassword won;t run in tests
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Something else'));

            AMSPasswordChangeController cont = new AMSPasswordChangeController();
            cont.updatePassword();

            System.assertNotEquals('', cont.errorMessage, 'Incorrect message');
            System.assertEquals(false, cont.passwordUpdated, 'Incorrect value of passwordUpdated');
        }
    }

}