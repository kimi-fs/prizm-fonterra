/**
 * Description: Controller for displaying third party access to the farms and configuring who has what access and until what date.
 * @author: Ed (Trineo)
 * @date: August 2017
 * @test: AMSThirdPartyController_Test
 */
public with sharing class AMSThirdPartyController {
    public Boolean showAccess { get; set; }

    private Integer identityCount;

    public String relationshipToEdit { get; set; }
    public String relationshipToDelete { get; set; }

    public Boolean editMode {
        get {
            return String.isNotEmpty(this.relationshipToEdit);
        }
        private set;
    }

    public static List<String> newRelPanels {
        get{
            return new List<String> { 'Who', 'Which', 'What', 'When', 'Confirm'};
        }
    }
    public Boolean newRelationship { get; set; }
    public Integer currentPanelNumber { get; set; }

    public String farmId { get; set; }

    private List<Reference_Period__c> futureReferencePeriods;

    private static Map<String, String> messages;
    static {
        messages = AMSCommunityUtil.initialisePageMessages('AMSThirdParty');
    }

    // Who Panel
    public String relationshipUsername { get; set; }
    public String relationshipRole { get; set; }
    @testVisible
    private Id relationshipIndividualId;
    public String relationshipIndividualName { get; set; }
    public String message { get; set;}

    // What panel


    // When panel
    @testVisible
    private static final Integer NUMBER_OF_REFERENCE_PERIODS = 4;
    public Integer selectedRelationship { get; set; }
    public String dateTypeSelected { get; set; }

    /**
     * Wrap a relationship and its access for display on the page, we are using
     * three wrappers, at the top one for each farm, and then one for each relationship
     * with that farm, and then the accesses for that relationship.
     *
     * FarmWrapper
     *   RelationshipWrapper
     */
    public class RelationshipWrapper {
        public Integer identity { get; private set; }
        public Boolean me { get; set; }
        public Boolean selected { get; set; }
        public String dateType { get; set; }
        public String startDateString { get; set; }
        public String endDateString { get; set; }
        public Boolean draft { get; set; }
        public Individual_Relationship__c relationship { get; set; }
        public String partyName { get; set; }
        public Boolean tpa { get; set; }

        public RelationshipWrapper(Integer identity) {
            this.identity = identity;
        }

        public Boolean getIsEditable() {
            return !(this.me) && this.tpa;
        }
    }

    public class FarmWrapper {
        public String name { get; set; }                                // The name of the farm
        public Id farmId { get; set; }                                  // The SF Id of the Account for this farm
        public Id partyId { get; set; }                                 // The Party through which we manage this farm
        public Entity_Relationship__c sponsor { get; set; }             // The sponsoring ER
        public Boolean selected { get; set; }                           // Has this farm been selected in an input
        public List<RelationshipWrapper> relationships { get; set; }

        public FarmWrapper() {
            this.selected = false;
        }
    }

    public List<FarmWrapper> farmRelationships { get; set; }
    //public List<String> accessNames { get; set; }

    private Id contactId;

    public AMSThirdPartyController() {
        this.showAccess = true;
        this.farmRelationships = new List<FarmWrapper>();
        this.contactId = AMSContactService.determineContactId();

        this.identityCount = 1;

        getFarmsAndRelationships();
    }

    public static List<SelectOption> getRelationshipRoleOptions() {
        List<SelectOption> relationshipRoleOptions = new List<SelectOption>();
        relationshipRoleOptions.add(AMSCommunityUtil.createFirstSelectOption());
        Schema.DescribeFieldResult roleFieldResult = Individual_Relationship__c.Role__c.getDescribe();
        List<Schema.PicklistEntry> roleple = roleFieldResult.getPicklistValues();
        for ( Schema.PicklistEntry f : roleple) {
            relationshipRoleOptions.add(new SelectOption(f.getValue(), f.getLabel()));
        }
        return relationshipRoleOptions;
    }

    public List<Reference_Period__c> getReferencePeriods() {
        if (this.futureReferencePeriods == null) {
            this.futureReferencePeriods = AMSReferencePeriodService.getCurrentAndFutureReferencePeriods(NUMBER_OF_REFERENCE_PERIODS);
        }
        return this.futureReferencePeriods;
    }

    public PageReference editRelationship() {
        this.showAccess = false;
        this.currentPanelNumber = 0;
        this.newRelationship = true;

        Boolean matched = false;
        for (FarmWrapper fw : farmRelationships) {
            for (RelationshipWrapper rw : fw.relationships) {
                if (rw.relationship.Id == relationshipToEdit) {
                    rw.selected = true;
                    fw.selected = true;
                    relationshipUsername = rw.relationship.Individual__r.Email;
                    relationshipRole = rw.relationship.Role__c;
                    System.debug('Matched on: ' + rw);
                    matched = true;
                    break;
                }
            }
            if (matched) {
                break;
            }
        }

        if (!matched) {
            System.debug('Did not match a relationship!');
        }
        return null;
    }

    /**
     * Change the relationship to inactive
     */
    public PageReference deleteRelationship() {
        List<Individual_Relationship__c> relationshipsToMakeInactive = new List<Individual_Relationship__c>();
        Boolean matched = false;
        for (FarmWrapper fw : farmRelationships) {
            List<RelationshipWrapper> updateRWList = new List<RelationshipWrapper>();
            for (RelationshipWrapper rw : fw.relationships) {
                if (rw.relationship.Id == relationshipToDelete) {
                    rw.relationship.Active__c = false;
                    relationshipsToMakeInactive.add(rw.relationship);
                    matched = true;
                } else {
                    updateRWList.add(rw);
                }
            }
            fw.relationships = updateRWList;
            if (matched) {
                break;
            }
        }

        RelationshipAccessService.upsertIndividualRelationships(relationshipsToMakeInactive);

        resetState();

        return null;
    }

    /**
     * start creating a new relationship for the selected farm
     */
    public void addRelationship() {
        this.showAccess = false;
        this.currentPanelNumber = 0;
        this.newRelationship = true;
    }

    /**
     * Navigation controls for new relationship
     */
    public void cancel() {
        resetState();
    }

    public void back() {
        this.message = null;
        this.currentPanelNumber--;
    }

    public void next() {
        this.message = null;

        if (newRelPanels[this.currentPanelNumber] == 'Confirm') {
            // Complete - update any existing relationships that have changed, and insert any new relationships.

            List<Individual_Relationship__c> upsertRelationships = new List<Individual_Relationship__c>();
            for (FarmWrapper fw : farmRelationships) {
                for (RelationshipWrapper rw : fw.relationships) {
                    upsertRelationships.add(rw.relationship);
                }
            }
            RelationshipAccessService.upsertIndividualRelationships(upsertRelationships);

            // reset state will requery the relationships so that we will pickup any active ones that we just inserted.
            resetState();
        } else {
            Boolean continueToNext = true;

            // Continue
            // find the current panel and set it to be the next one

            // Who
            if (newRelPanels[this.currentPanelNumber] == 'Who') {

                if (String.isBlank(this.relationshipRole)) {
                    this.message = messages.get('role-missing');
                    continueToNext = false;
                } else {
                    // Does this user exist?
                    if (String.isBlank(this.relationshipUsername)) {
                        continueToNext = false;
                    } else {
                        String emailAddress = String.escapeSingleQuotes(this.relationshipUsername.trim());
                        User thirdPartyUser = AMSUserService.getUserFromEmailAddress(emailAddress);
                        if (thirdPartyUser != null) {
                            this.relationshipIndividualId = thirdPartyUser.ContactId;
                            this.relationshipIndividualName = thirdPartyUser.Contact.Name;
                        } else {
                            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Can not find a user with email address ' + this.relationshipUsername));
                            this.message = messages.get('user-not-found1') + messages.get('user-not-found2');
                            continueToNext = false;
                        }
                    }
                }
            }

            // Which
            if (newRelPanels[this.currentPanelNumber] == 'Which') {
                // We know the farm and the role, so can create or tag the relationships
                // that are being proposed.
                RelationshipWrapper editingRelationship, newRelationship;

                Boolean atLeastOneSelected = false;
                for (FarmWrapper farm : this.farmRelationships) {
                    if (farm.selected == true) {
                        // we want a relationship between us and this farm, is there an existing relationship?
                        Boolean found = false;
                        if (editMode) {
                            for (RelationshipWrapper relationship : farm.relationships) {
                                if (relationship.relationship.Individual__c == this.relationshipIndividualId) {
                                    if (relationship.relationship.RecordTypeId == GlobalUtility.individualRestrictedFarmRecordTypeId) {
                                        System.debug('Found an existing relationship between this person and this farm');
                                        relationship.selected = true;
                                        relationship.relationship.Role__c = this.relationshipRole;
                                        newRelationship = relationship;
                                        // reset the start date since we are reusing it.
                                        newRelationship.relationship.Access_Begin__c = Datetime.now().addDays(-1);
                                    } else {
                                        this.message = messages.get('relationship-exists').replace('{!farmName}', farm.name);
                                        continueToNext = false;
                                    }
                                    found = true;
                                    break;
                                }
                            }
                        }

                        if (!found) {
                            Boolean relationshipExists = false;
                            List<Individual_Relationship__c> allExistingRelationshipsForSelectedIndividual = RelationshipAccessService.allIndividualRelationshipsForIndividual(this.relationshipIndividualId);
                            for (Individual_Relationship__c existingIR : allExistingRelationshipsForSelectedIndividual) {
                                if (existingIR.Farm__c == farm.farmId) {
                                    relationshipExists = true;
                                    this.message = messages.get('relationship-exists').replace('{!farmName}', farm.name);
                                    continueToNext = false;
                                    break;
                                }
                            }

                            if (!relationshipExists) {
                                // create a new one then
                                System.debug('Did not find a matching relationship, so creating a new draft one with identity ' + this.identityCount);
                                Individual_Relationship__c ir = new Individual_Relationship__c(Farm__c = farm.farmId,
                                                                                               Individual__c = this.relationshipIndividualId,
                                                                                               Role__c = this.relationshipRole,
                                                                                               RecordTypeId = GlobalUtility.individualRestrictedFarmRecordTypeId,
                                                                                               Access_Sponsoring__c = farm.sponsor.Id,
                                                                                               Party__c = farm.sponsor.Party__c,
                                                                                               Access_Begin__c = Datetime.now().addDays(-1));
                                RelationshipWrapper wrapper = new RelationshipWrapper(this.identityCount++);
                                wrapper.relationship = ir;
                                wrapper.me = false;
                                wrapper.selected = true;
                                wrapper.draft = true;
                                wrapper.partyName = farm.sponsor.Party__r.Name;
                                farm.relationships.add(wrapper);
                                newRelationship = wrapper;
                            }
                        }
                        atLeastOneSelected = true;
                    }
                }
                if (!atLeastOneSelected) {
                    this.message = messages.get('select-one-farm');
                    continueToNext = false;
                } else {
                    if (editingRelationship != null && newRelationship != null) {
                        // Copy over the access and dates from the old edited relationship to the new one.
                        newRelationship.relationship.Access_End__c = editingRelationship.relationship.Access_End__c;
                        Schema.FieldSet accessFields = Schema.SObjectType.Individual_Relationship__c.FieldSets.Relationship_Access;
                        for (Schema.FieldSetMember fieldMember : accessFields.getFields()) {
                            newRelationship.relationship.put(fieldMember.getFieldPath(), editingRelationship.relationship.get(fieldmember.getFieldPath()));
                        }
                    }
                }
            }

            if (newRelPanels[this.currentPanelNumber] == 'What') {
                Boolean missingAccess = false;
                for (FarmWrapper farm : this.farmRelationships) {
                    if (farm.selected == true) {
                        for (RelationshipWrapper relationship : farm.relationships) {
                            if (relationship.selected == true &&
                                    RelationshipAccessService.hasNoAccess(relationship.relationship)) {
                                missingAccess = true;
                                break;
                            }
                        }
                    }
                }
                if (missingAccess) {
                    this.message = messages.get('select-one-category');
                    continueToNext = false;
                }
                populateFromDates();
            }

            if (newRelPanels[this.currentPanelNumber] == 'When') {
                recalculateFromDate();

                // Validate the date
                Boolean dateInPast = false;
                Date today = Date.today();
                for (FarmWrapper farm : this.farmRelationships) {
                    if (farm.selected == true) {
                        for (RelationshipWrapper relationship : farm.relationships) {
                            if (relationship.selected == true && relationship.relationship.Access_End__c < today) {
                                dateInPast = true;
                                break;
                            }
                        }
                    }
                }
                if (dateInPast) {
                    this.message = messages.get('date-in-past');
                    continueToNext = false;
                }
            }

            if (continueToNext) {
                this.currentPanelNumber++;
            }
        }
    }

    private void resetState() {
        this.showAccess = true;
        this.currentPanelNumber = 0;
        this.newRelationship = false;
        this.relationshipUsername = '';
        this.relationshipRole = '';
        this.relationshipToEdit = '';
        this.message = null;

        this.farmRelationships = new List<FarmWrapper>();
        getFarmsAndRelationships();
    }

    /**
     * For the logged in user get all of the farms that they own and
     * obtain the relationships individuals (including themselves) have with those
     * farms.
     */
    private void getFarmsAndRelationships() {
        List<Id> farmIds = new List<Id>();
        List<Id> partyIds = new List<Id>();

        List<Individual_Relationship__c> myFarmRelationships = RelationshipAccessService.relationshipsForOwnedFarms(this.contactId);

        Map<Id, List<Individual_Relationship__c>> relationshipsByFarm = new Map<Id, List<Individual_Relationship__c>>();

        // Do one pass to split the relationships into a Map by the Farm Id
        for (Individual_Relationship__c myFarmRelationship : myFarmRelationships) {
            List<Individual_Relationship__c> relationshipsForFarm = relationshipsByFarm.get(myFarmRelationship.Farm__c);
            if (relationshipsForFarm == null) {
                relationshipsForFarm = new List<Individual_Relationship__c>();
            }
            relationshipsForFarm.add(myFarmRelationship);
            relationshipsByFarm.put(myFarmRelationship.Farm__c, relationshipsForFarm);
        }

        // Now construct the Wrappers
        for (Id farmId : relationshipsByFarm.keySet()) {
            List<Individual_Relationship__c> relationshipsForFarm = relationshipsByFarm.get(farmId);

            if (relationshipsForFarm == null || relationshipsForFarm.isEmpty()) {
                // wtf
                break;
            }
            // Top Farm wrapper
            FarmWrapper theFarm = new FarmWrapper();
            theFarm.name = relationshipsForFarm[0].Farm__r.Name;
            theFarm.farmId = relationshipsForFarm[0].Farm__c;
            theFarm.relationships = new List<RelationshipWrapper>();

            farmIds.add(theFarm.farmId);

            for (Individual_Relationship__c myFarmRelationship : relationshipsForFarm) {
                // relationship wrapper
                System.debug('Found relationship, setting to identity: ' + this.identityCount);
                RelationshipWrapper wrapper = new RelationshipWrapper(this.identityCount++);
                wrapper.relationship = myFarmRelationship;

                if (String.isNotBlank(wrapper.relationship.Party__r.Name)) {
                    wrapper.partyName = wrapper.relationship.Party__r.Name;
                } else {
                    wrapper.partyName = 'Unknown';
                }
                if (this.contactId == myFarmRelationship.Individual__c) {
                    wrapper.me = true;

                    if (myFarmRelationship.RecordTypeId == GlobalUtility.derivedRelationshipRecordTypeId) {
                        // This is my derived relationship with this farm, so that means I have a party for it
                        theFarm.partyId = myFarmRelationship.Party__c;

                        partyIds.add(theFarm.partyId);
                    }
                } else {
                    wrapper.me = false;
                }


                // Figure out the type of end date
                wrapper = populateDateType(wrapper);

                if (myFarmRelationship.RecordTypeId == GlobalUtility.individualRestrictedFarmRecordTypeId) {
                    wrapper.tpa = true;
                } else {
                    wrapper.tpa = false;
                }
                // We only actually care about the TPA relationships
                if (myFarmRelationship.RecordTypeId == GlobalUtility.individualRestrictedFarmRecordTypeId) {
                    theFarm.relationships.add(wrapper);
                }
            }
            this.farmRelationships.add(theFarm);
        }

        // We now need to know the sponsoring ERs for each of these farms, we know the farms and the party already.
        List<Entity_Relationship__c> ers = RelationshipAccessService.entityRelationshipsFromFarmParty(farmIds, partyIds);

        // For each farm/party wrapper copy the ER back to it
        for (FarmWrapper farm : this.farmRelationships) {
            for (Entity_Relationship__c er : ers) {
                if (farm.farmId == er.Farm__c &&
                        farm.partyId == er.Party__c) {
                    // Bingo!
                    farm.sponsor = er;
                }
            }
        }
    }

    private RelationshipWrapper populateDateType(RelationshipWrapper wrapper) {
        List<Reference_Period__c> seasons = getReferencePeriods();

        DateTime endDate = wrapper.relationship.Access_End__c;

        if (endDate == null) {
            wrapper.dateType = 'INDEFINITELY';
        } else {
            for (Reference_Period__c season : seasons) {
                if (endDate == season.End_Date__c) {
                    wrapper.dateType = 'SEASON';
                    break;
                }
            }
            if (wrapper.dateType == null) {
                wrapper.dateType = 'CHERRYPICK';
            }
        }
        return wrapper;
    }

    public void newDateTypeSelected() {
        // Find the correct wrapper
        for (FarmWrapper farm : this.farmRelationships) {
            for (RelationshipWrapper relationship : farm.relationships) {
                if (relationship.identity == this.selectedRelationship) {
                    System.debug('Matched the relationship, switching to dateType: ' + dateTypeSelected);
                    relationship.dateType = dateTypeSelected;
                    if (relationship.dateType == 'INDEFINITELY') {
                        relationship.endDateString = null;
                        relationship.relationship.Access_End__c = null;
                    } else if (relationship.dateType == 'CHERRYPICK') {
                        relationship.relationship.Access_End__c = System.now();
                        populateDateStringFromDates(relationship);
                    } else if (relationship.dateType == 'SEASON') {
                        // Populate the date so that we can see if they have selected a season.
                        populateDateFromDateString(relationship);
                        System.debug('End Date String: ' + relationship.endDateString + ' Date: ' + relationship.relationship.Access_End__c);
                        List<Reference_Period__c> seasons = getReferencePeriods();
                        DateTime endDate = relationship.relationship.Access_End__c;
                        Boolean matched = false;
                        for (Reference_Period__c season : seasons) {
                            if (endDate != null && season.End_Date__c != null &&
                                    endDate.date() == season.End_Date__c) {
                                matched = true;
                                System.debug('Matched Season: ' + season.End_Date__c);
                                break;
                            }
                        }

                        if (!matched) {
                            // Choose the first one if we don't already match a season.
                            relationship.relationship.Access_End__c = seasons[0].End_Date__c;
                            populateDateStringFromDates(relationship);
                            System.debug('Failed to match season, setting to first season');
                        }
                    }
                    break;
                }
            }
        }
    }

    public void populateDateStringFromDates(RelationshipWrapper relationship) {
        System.debug('populateDateStringFromDates before:' + relationship);
        relationship.startDateString = relationship.relationship.Access_Begin__c != null ? AMSCollectionService.australianFormatDate(relationship.relationship.Access_Begin__c) : '';
        relationship.endDateString = relationship.relationship.Access_End__c != null ? AMSCollectionService.australianFormatDate(relationship.relationship.Access_End__c) : '';
        System.debug('populateDateStringFromDates after:' + relationship);
    }

    /**
     * Populate the date strings from the relationship dates
     */
    public void populateFromDates() {
        for (FarmWrapper farm : this.farmRelationships) {
            for (RelationshipWrapper relationship : farm.relationships) {
                populateDateStringFromDates(relationship);
                populateDateType(relationship);
            }
        }
    }

    //populate Dates from DateStrings
    public void populateDateFromDateString(RelationshipWrapper relationship) {

        System.debug('populateDateFromDateString before:' + relationship);
        if (relationship.startDateString != null) {
            Date beginDate = AMSCollectionService.australianParseDate(relationship.startDateString);
            if (beginDate != null) {
                Time beginTime = Time.newInstance(0, 0, 0, 0);
                relationship.relationship.Access_Begin__c = DateTime.newInstance(beginDate, beginTime);
            } else {
                relationship.relationship.Access_Begin__c = null;
            }
        }
        if (relationship.endDateString != null) {
            System.debug('Found an end date String of ' + relationship.endDateString);
            Date endDate = AMSCollectionService.australianParseDate(relationship.endDateString);
            if (endDate != null) {
                Time endTime = Time.newInstance(23, 59, 59, 0);
                relationship.relationship.Access_End__c = DateTime.newInstance(endDate, endTime);
            } else {
                relationship.relationship.Access_End__c = null;
            }
            System.debug('Which turned into ' +  relationship.relationship.Access_End__c);
        }
        System.debug('populateDateFromDateString after:' + relationship);
    }

    /**
     * Populate the dates from the date strings
     */
    public void recalculateFromDate() {
        for (FarmWrapper farm : this.farmRelationships) {
            for (RelationshipWrapper relationship : farm.relationships) {
                populateDateFromDateString(relationship);
                populateDateType(relationship);
            }
        }
    }
}