/**
 * Description:
 * Handles notifications for sending SMS/email alerts
 *
 * @author: John Au (Trineo)
 * @date: August 2018
 * @test: StatementTriggerTest
 */
public class StatementTriggerHandler {

    public static final String SMS_DEVELOPER_NAME = 'SMS';
    public static final String EMAIL_DEVELOPER_NAME = 'Email';

    private static Map<Id, Channel_Preference__c> getChannelPreferenceByIndividualIdMap(List<Id> individualIds) {
        Map<Id, Channel_Preference__c> channelPreferenceByIndividualIdMap = new Map<Id, Channel_Preference__c>();

        if (AMSChannelPreferencesUtil.STATEMENT_NOTIFICATION_SETTING != null) {
            List<Channel_Preference__c> channelPreferences = [SELECT
                                                                Contact_ID__c,
                                                                DeliveryByEmail__c,
                                                                NotifyBySMS__c
                                                            FROM
                                                                Channel_Preference__c
                                                            WHERE
                                                                Contact_ID__c IN :individualIds
                                                                AND Channel_Preference_Setting__c = :AMSChannelPreferencesUtil.STATEMENT_NOTIFICATION_SETTING.Id];

            for (Channel_Preference__c channelPreference : channelPreferences) {
                channelPreferenceByIndividualIdMap.put(channelPreference.Contact_ID__c, channelPreference);
            }
        }

        return channelPreferenceByIndividualIdMap;
    }

    public static void sendStatementNotifications(List<Statement__c> statements) {
        List<Id> statementFarmIds = new List<Id>();

        for (Statement__c statement : statements) {
            statementFarmIds.add(statement.Farm__c);
        }

        List<Individual_Relationship__c> individualRelationships = RelationshipAccessService.individualRelationshipsWithStatementNotifications(statementFarmIds);

        List<Id> individualIds = new List<Id>();

        Map<Id, List<Individual_Relationship__c>> individualRelationshipsByPartyId = new Map<Id, List<Individual_Relationship__c>>();
        Map<Id, Channel_Preference__c> channelPreferenceByIndividualIdMap;

        for (Individual_Relationship__c individualRelationship : individualRelationships) {
            individualIds.add(individualRelationship.Individual__c);

            if (!individualRelationshipsByPartyId.containsKey(individualRelationship.Party__c)) {
                individualRelationshipsByPartyId.put(individualRelationship.Party__c, new List<Individual_Relationship__c>());
            }
            individualRelationshipsByPartyId.get(individualRelationship.Party__c).add(individualRelationship);
        }

        channelPreferenceByIndividualIdMap = getChannelPreferenceByIndividualIdMap(individualIds);

        List<Task> smsTasksForInsert = new List<Task>();
        List<Task> emailTasksForInsert = new List<Task>();

        for (Statement__c statement : statements) {
            if (individualRelationshipsByPartyId.containsKey(statement.Party__c)) { //in case there are no valid irels
                for (Individual_Relationship__c individualRelationship : individualRelationshipsByPartyId.get(statement.Party__c)) {
                    String text = 'Milk Statement for ' + individualRelationship.Party__r.Name + ' for ' + ((DateTime) statement.Date__c).format('MMMMMM YYYY') + ' is now available. For details go to www.farmsource.com.au';

                    Channel_Preference__c individualChannelPreference = channelPreferenceByIndividualIdMap.get(individualRelationship.Individual__c);

                    if (individualChannelPreference != null) {
                        if (individualChannelPreference.NotifyBySMS__c) {
                            Task t = createSMSTask(individualRelationship.Individual__c, individualRelationship.Individual__r.MobilePhone, statement.Party__c, text);
                            smsTasksForInsert.add(t);
                        }
                        if (individualChannelPreference.DeliveryByEmail__c) {
                            Task t = createEmailTask(individualRelationship.Individual__c, statement.Party__c, text, text);
                            emailTasksForInsert.add(t);
                        }
                    }
                }
            }
        }

        insert smsTasksForInsert;
        insert emailTasksForInsert;

        if (smsTasksForInsert.size() > 0) {
            Database.executeBatch(new ScheduleBatchSendSMS(smsTasksForInsert), 1);
        }
        if (emailTasksForInsert.size() > 0) {
            Database.executeBatch(new ScheduleBatchSendEmail(emailTasksForInsert), 1);
        }
    }

    private static Task createSMSTask(Id individualId, String mobileNumber, Id partyId, String msg) {
        Task task = createTask(individualId, mobileNumber, partyId, msg, SMS_DEVELOPER_NAME);
        task.Subject = AMSChannelPreferencesUtil.STATEMENT_NOTIFICATION_SETTING.Name;

        return task;
    }

    private static Task createEmailTask(Id individualId, Id partyId, String msg, String subject) {
        Task task = createTask(individualId, null, partyId, msg, EMAIL_DEVELOPER_NAME);
        task.Subject = subject;

        return task;
    }

    private static Task createTask(Id individualId, String mobileNumber, Id partyId, String msg, String taskType) {
        Task task = new Task();

        task.Status = 'Scheduled';
        Task.Type = taskType;
        task.WhoId = individualId;
        task.WhatId = partyId;
        task.Description = msg;
        task.SMS_Sent_To__c = mobileNumber;
        task.ActivityDate = Date.today();
        task.RecordTypeId = GlobalUtility.activityTaskSystemGeneratedTaskRecordTypeId;

        return task;
    }
}