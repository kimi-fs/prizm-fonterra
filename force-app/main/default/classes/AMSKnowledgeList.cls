/**
 * AMSKnowledgeList.cls
 *
 * Description: Common functionality for pages that use articles with data categories and display them in a hierarchical format.
 *              Currently only used by AMSNewsController and AMSNewsDetailController- AMSAdvice should be refactored to use this at some point.
 *
 * @author: John (Trineo)
 * @date: August 2018
 */
public abstract with sharing class AMSKnowledgeList {

    public static final String NEWS_DATA_CATEGORY = 'AMS_News';

    public String currentArticleCategoryName { get; set; }
    public List<DataCategoryWrapper> dataCategoryWrappers { get; set; }

    public enum Level {
        FIRST,
        SECOND,
        THIRD
    }

    public class DataCategoryWrapper {
        public String name { get; set; }
        public String label { get; set; }
        public Level level { get; set; }

        public Boolean selected { get; set; }

        public Boolean subCategorySelected {
            get {
                if (!subCategories.isEmpty()) {
                    for (DataCategoryWrapper subCategory : this.subCategories) {
                        if (subCategory.selected) {
                            this.subCategorySelected = true;
                        }
                    }
                }

                return subCategorySelected;
            }
            set;
        }

        public List<DataCategoryWrapper> subCategories { get; set; }

        public DataCategoryWrapper(String name, String label, Level level) {
            this.name = name;
            this.label = label;
            this.level = level;

            selected = false;

            this.subCategories = new List<DataCategoryWrapper>();
        }

        public void addSubCategory(DataCategoryWrapper wrapper) {
            this.subCategories.add(wrapper);
        }
    }

    public class NewsArticleWrapper {
        public Knowledge__kav article { get; set; }

        public NewsArticleWrapper(Knowledge__kav article) {
            this.article = article;
        }

        public String getFirstPublishedDate(){
            return this.article.FirstPublishedDate.format('dd MMMM yyyy hh:mm a');
        }
    }

    protected List<DataCategoryWrapper> getArticleCategories(String dataCategory) {
        List<DataCategoryWrapper> dataCategoryWrappers = new List<DataCategoryWrapper>();

        List<DescribeDataCategoryGroupResult> describeCategoryResult = Schema.describeDataCategoryGroups(new List<String>{'KnowledgeArticleVersion'});
        List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();

        //Looping throught the first describe result to create the list of pairs for the second describe call
        for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult){
            DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
            p.setSobject(singleResult.getSobject());
            p.setDataCategoryGroupName(singleResult.getName());
            pairs.add(p);
        }

        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult = new List<DescribeDataCategoryGroupStructureResult>();
        describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(pairs, false);

        // for each of the categories in AMS Advice article add to the List - this is the menu
        for (DescribeDataCategoryGroupStructureResult singleResult : describeCategoryStructureResult) {
            // check if its our article type
            if (singleResult.getName() == dataCategory) {
                // getTopCategories returns 'All' so we get the child categories of this which are our top level categories
                List<DataCategory> amsTopCategories = singleResult.getTopCategories()[0].getChildCategories();

                for (DataCategory amsTopCategory : amsTopCategories) {
                    DataCategoryWrapper topCategory = new DataCategoryWrapper(amsTopCategory.getName(), amsTopCategory.getLabel(), Level.FIRST);
                    dataCategoryWrappers.add(topCategory);

                    if (currentArticleCategoryName == amsTopCategory.getName()) {
                        topCategory.selected = true;
                    }

                    for (DataCategory amsSubCategory : amsTopCategory.getChildCategories()) {
                        DataCategoryWrapper subCategory = new DataCategoryWrapper(amsSubCategory.getName(), amsSubCategory.getLabel(), Level.SECOND);
                        topCategory.addSubCategory(subCategory);

                        if (currentArticleCategoryName == amsSubCategory.getName()) {
                            subCategory.selected = true;
                        }
                    }
                }
            }
        }
        return dataCategoryWrappers;
    }

}