/**
 * Provide services for SF Files, including secure fetching of the file contents for community downloads
 *
 * @author Ed (Trineo)
 * @date   May 2020
 * @test: FileServiceTest
 */
public without sharing class FileService {
    public class InvalidAccessException extends Exception {}

    /**
     * Fetch a ContentDocumentLink from its Id
     */
    private static ContentDocumentLink getParentRecordIdFromLink(Id contentDocumentLinkId) {
        List<ContentDocumentLink> contentDocumentLinks = [
            SELECT Id, LinkedEntityId, ContentDocumentId
            FROM ContentDocumentLink
            WHERE Id = :contentDocumentLinkId
        ];

        if (contentDocumentLinks.isEmpty()) {
            return null;
        } else {
            return contentDocumentLinks[0];
        }
    }

    /**
     * Given a Document Link Id for an Agreement document check that the currently running user should
     * have access to this document, and if so return the document contents as a String.
     *
     * An Agreement is to a farm and/or party
     */
    public static String getAgreementDocumentContents(Id contentDocumentLinkId) {
        ContentDocumentLink contentDocumentLink = getParentRecordIdFromLink(contentDocumentLinkId);

        if (contentDocumentLink == null) {
            throw new InvalidAccessException('ContentDocumentLink no longer exists');
        }

        Id agreementId = contentDocumentLink.LinkedEntityId;

        if (agreementId.getSobjectType() != SObjectType.Agreement__c.getSObjectType()) {
            throw new InvalidAccessException('ContentDocumentLink not to an Agreement object');
        }

        Agreement__c agreement = AgreementService.getAgreement(agreementId);

        if (agreement == null) {
            throw new InvalidAccessException('ContentDocumentLink not to an Agreement object');
        }

        Id contactId = AMSContactService.determineContactId();

        if (contactId == null) {
            User usr = AMSUserService.getUser();
            if (usr == null || usr.ContactId == null) {
                throw new InvalidAccessException('Not a community user');
            }
            contactId = usr.ContactId;
            System.debug('No contactId found, falling back to the current user, ' + usr);
        }

        Boolean accessToFarm = RelationshipAccessService.isAccessGranted(contactId, agreement.Farm__c, RelationshipAccessService.Access.ACCESS_PRIMARY);

        if (!accessToFarm) {
            throw new InvalidAccessException('ContentDocumentLink not to an Agreement record that is visible for this user');
        }

        // This user has access to the farm that this agreement is linked to, to get the contents of the
        // document we need to first get the document, and then the latest version of that document.

        ContentDocument contentDocument = [
            SELECT Id, Title, LatestPublishedVersionId, LatestPublishedVersion.PathOnClient
            FROM ContentDocument
            WHERE Id = :contentDocumentLink.ContentDocumentId
        ];

        ContentVersion cv = [SELECT Id, VersionData FROM ContentVersion WHERE Id = :contentDocument.LatestPublishedVersionId];

        return EncodingUtil.base64Encode(cv.VersionData);
    }

}