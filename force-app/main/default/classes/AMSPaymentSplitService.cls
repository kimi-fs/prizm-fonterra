/**
 * Service for accessing payment split records
 *
 * IMPORTANT: This class is WITHOUT sharing - behave appropriately.
 *
 * Author: Amelia (Trineo)
 * Date: November 2017
 */
public without sharing class AMSPaymentSplitService {

    public static List<Payment_Split__c> getPaymentSplits(String farmId){
        List<Payment_Split__c> paymentSplits = new List<Payment_Split__c>();

        paymentSplits = [SELECT Amount__c,
                                Percentage__c,
                                Party__c,
                                Party__r.Name,
                                Payee_Type__c,
                                Payment_Split_Type__c
                        FROM Payment_Split__c
                        WHERE Agreement__r.Farm__c = :farmId
                        AND Active_Formula__c = true
                    ];

        return paymentSplits;
    }
}