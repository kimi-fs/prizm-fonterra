/**
* Description: Test class for AddressService
* @author: Amelia (Trineo)
* @date: July 2018
*/
@isTest
private class AddressServiceTest {

    @testSetup static void createTestData () {

        // This is needed for the Address
        Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();

    }

    @isTest static void testGetEntityPostalAddress() {

        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        Address__c address = TestClassDataUtil.createAddress(GlobalUtility.postalAddressRecordTypeId, party.Id, '123', 'Queen Street', true);

        Address__c relatedPostalAddress = AddressService.getCurrentPostalAddressForEntity(party.Id);
        System.assertEquals(address.Id, relatedPostalAddress.Id);

    }

}