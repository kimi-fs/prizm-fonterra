@isTest
private class PreferenceHistoryServiceTest {
    private static Id IREL_INDIVIDUALFARM_RECORDTYPE_ID = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Farm').getRecordTypeId();
    private static Id IREL_INDIVIDUALPARTY_RECORDTYPE_ID = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Party').getRecordTypeId();

    private static Field_Team_Tools_Settings__c integrationUserProfile = TestClassDataUtil.integrationUserProfile();

    @testSetup static void testSetup() {
        TestClassDataUtil.individualDefaultAccountAU();

        List<Channel_Preference_Setting__c> auChannelPreferenceSettings = TestClassDataUtil.createAUChannelPreferenceSettings();

        Contact individual = TestClassDataUtil.createIndividualAu(true);

        List<Reference_Region__c> auReferenceRegions = TestClassDataUtil.createAuReferenceRegions();
        List<Account> farms = TestClassDataUtil.createAuFarms(auReferenceRegions);
        insert farms;

        List<Channel_Preference__c> channelPreferences = new List<Channel_Preference__c>();
        for(Channel_Preference_Setting__c cps : auChannelPreferenceSettings){
            channelPreferences.add(new Channel_Preference__c(
                Channel_Preference_Setting__c = cps.Id,
                Contact_ID__c = individual.Id
            ));
        }

        insert channelPreferences;

        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        List<Individual_Relationship__c> individualRelationships = new List<Individual_Relationship__c>();
        // individual-farm
        individualRelationships.add(new Individual_Relationship__c(
            Active__c = true,
            Individual__c = individual.Id,
            Role__c = 'Director',
            Farm__c = farms[0].Id,
            RecordTypeId = IREL_INDIVIDUALFARM_RECORDTYPE_ID
        ));
        // individual-party
        individualRelationships.add(new Individual_Relationship__c(
            Active__c = true,
            Individual__c = individual.Id,
            Role__c = 'Director',
            Party__c = party.Id,
            Farm__c = farms[0].Id,
            RecordTypeId = IREL_INDIVIDUALPARTY_RECORDTYPE_ID
        ));
        insert individualRelationships;

        List<Entity_Relationship__c> entityRelationships = new List<Entity_Relationship__c>();
        entityRelationships.add(new Entity_Relationship__c(
            Active__c = true,
            Farm__c = farms[0].Id,
            Party__c = party.Id,
            Role__c = 'Owner'

        ));
        insert entityRelationships;

        /* CREATE SOME HISTORY */
        for (Channel_Preference_Setting__c cps : auChannelPreferenceSettings) {
            updateChannelPreferenceSetting(cps, 'Disabled');
        }
        update auChannelPreferenceSettings;
        for (Channel_Preference_Setting__c cps : auChannelPreferenceSettings) {
            updateChannelPreferenceSetting(cps, 'Enabled');
        }
        update auChannelPreferenceSettings;

        for (Channel_Preference__c cp : channelPreferences) {
            updateChannelPreference(cp, false);
        }
        update channelPreferences;

        for (Channel_Preference__c cp : channelPreferences) {
            updateChannelPreference(cp, true);
        }
        update channelPreferences;
    }

    /* helpers to update records to create some field history */
    public static void updateChannelPreferenceSetting(Channel_Preference_Setting__c channelPreferenceSetting, String value) {
        channelPreferenceSetting.Active__c = (value == 'Enabled' ? true : false);
        channelPreferenceSetting.DeliveryByEmail__c = value;
        channelPreferenceSetting.DeliveryMail__c = value;
        channelPreferenceSetting.NotifyBySMS__c = value;
    }
    public static void updateChannelPreference(Channel_Preference__c channelPreference, Boolean value) {
        channelPreference.DeliveryByEmail__c = value;
        // channelPreference.Fax__c = value;
        channelPreference.DeliveryMail__c = value;
        channelPreference.NotifyBySMS__c = value;
    }
    @isTest static void testConstructor() {
        Map<String, String> mapFieldLabelByDeveloperName = new Map<String, String> {
            'created' => 'Created',
            'Stop_the_post__c' => 'Stop the post'
        };

        PreferenceHistoryService history = new PreferenceHistoryService();
        history.user = [SELECT Id FROM User LIMIT 1][0];
        history.account = [SELECT Id FROM Account LIMIT 1][0];
        history.setting = 'Setting name';
        history.dt = PreferenceHistoryService.populateTimeValue(Datetime.now());
        history.field = PreferenceHistoryService.populateFieldValue('created', mapFieldLabelByDeveloperName);
        history.oldValue = 'Old value';
        history.newValue = 'New value';
    }

    // these tests are all rubbish because Salesforce does not store history in unit test context
    @isTest static void testGetIndividualHistory() {
        Contact individual = [SELECT Id FROM Contact LIMIT 1];

        Test.startTest();
        List<PreferenceHistoryService> history = PreferenceHistoryService.getIndividualHistory(individual);
        Test.stopTest();

        Set<String> historyFields = new Set<String> {'Preferred_Store__c'};
        Integer historyCount = [SELECT count() FROM ContactHistory WHERE ContactId = :individual.Id AND Field IN :historyFields];
        System.assertEquals(historyCount, history.size(), 'History count does not match');
    }

    @isTest static void testGetMyPreferenceHistory() {
        Contact individual = [SELECT Id FROM Contact LIMIT 1];
        Map<Id, Channel_Preference__c> channelPreferences = new Map<Id, Channel_Preference__c>([SELECT Id FROM Channel_Preference__c WHERE Contact_ID__c = : individual.Id]);

        Test.startTest();
        List<PreferenceHistoryService> history = PreferenceHistoryService.getMyPreferenceHistory(channelPreferences.values());
        Test.stopTest();

        Set<String> historyFields = new Set<String> {'created', 'Active__c', 'NotifyBySMS__c', 'DeliveryByEmail__c', 'Fax__c', 'Phone__c', 'DeliveryMail__c', 'Location_Reference__c'};
        Integer historyCount = [SELECT count() FROM Channel_Preference__History WHERE ParentId IN: channelPreferences.keySet() AND Field IN :historyFields];
        System.assertEquals(historyCount, history.size(), 'History count does not match');
    }

    @isTest static void testGetChannelPreferenceHistory() {
        Contact individual = [SELECT Id FROM Contact LIMIT 1];
        List<PreferenceService.IndividualPreferenceWrapper> individualPreferenceWrappers = generateIndividualPreferenceWrappers(individual.Id);

        Test.startTest();
        List<PreferenceHistoryService> history = PreferenceHistoryService.getchannelPreferenceHistory(individualPreferenceWrappers);
        Test.stopTest();

        Map<Id, Channel_Preference__c> mapChannelPreferenceById = new Map<Id, Channel_Preference__c>();
        for (PreferenceService.IndividualPreferenceWrapper ipw : individualPreferenceWrappers) {
            mapChannelPreferenceById.put(ipw.channelPreference.Id, ipw.channelPreference);
        }

        Set<String> historyFields = new Set<String> {'created', 'Active__c', 'NotifyBySMS__c', 'DeliveryByEmail__c', 'Fax__c', 'Phone__c', 'DeliveryMail__c', 'Location_Reference__c'};
        Integer historyCount = [SELECT count() FROM Channel_Preference__History WHERE ParentId IN: mapChannelPreferenceById.keySet() AND Field IN :historyFields];
        System.assertEquals(historyCount, history.size(), 'History count does not match');
    }

    @isTest static void testPopulateFieldValue_WithCreatedValue() {
        Map<String, String> mapFieldLabelByDeveloperName = new Map<String, String> {
            'created' => 'Created'
        };

        Test.startTest();
        String fieldValue = PreferenceHistoryService.populateFieldValue('created', mapFieldLabelByDeveloperName);
        Test.stopTest();

        System.assertEquals('Created', fieldValue, 'Field value was not populated correctly');
    }

    @isTest static void testCompareTo() {
        List<PreferenceHistoryService> histories = new List<PreferenceHistoryService>();
        for (Integer i = 0; i < 10; i++) {
            Integer randomNumber = (Math.random() * 10).intValue();
            PreferenceHistoryService phs = new PreferenceHistoryService();
            phs.dt = Datetime.now().addHours(randomNumber);
            histories.add(phs);
        }
        Test.startTest();
        histories.sort();
        Test.stopTest();

        for (Integer i = 0; i < histories.size() - 1; i++) {
            System.assert(histories[i].dt >= histories[i + 1].dt, 'History was not sorted correctly');
        }
    }

    private static List<PreferenceService.IndividualPreferenceWrapper> generateIndividualPreferenceWrappers(Id contactId) {
        Map<Id, Channel_Preference_Setting__c> mapChannelPreferenceSettingById = PreferenceService.getChannelPreferenceSettingMap('Farm');
        Map<Id, Channel_Preference__c> mapChannelPreferenceById = PreferenceService.getChannelPreferenceMap(contactId, true, 'Farm');

        if (mapChannelPreferenceById.keySet().size() == 0) {
            return null;
        }

        List<PreferenceService.IndividualPreferenceWrapper> individualPreferenceWrappers = new List<PreferenceService.IndividualPreferenceWrapper>();

        for (Channel_Preference__c cp : mapChannelPreferenceById.values()) {
            individualPreferenceWrappers.add(new PreferenceService.IndividualPreferenceWrapper(cp, mapChannelPreferenceSettingById.get(cp.Channel_Preference_Setting__c)));
        }

        return individualPreferenceWrappers;
    }

}