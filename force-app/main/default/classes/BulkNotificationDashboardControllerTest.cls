@isTest
private class BulkNotificationDashboardControllerTest {
    private static Id individualFarmRecordTypeId = Schema.SObjectType.Individual_Relationship__c.getRecordTypeInfosByName().get('Individual-Farm').getRecordTypeId();

    @testSetup static void setup() {

        Contact individual = new Contact(LastName = 'Smith', FirstName = 'Tim', MobilePhone = '+61406133552', Phone = '+61406133552');
        Contact individual2 = new Contact(LastName = 'Jones', FirstName = 'John', Phone = '+61406123456');
        insert individual;
        insert individual2;

        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        List<Account> farms = TestClassDataUtil.createAUFarms(3, false);
        farms[0].Name ='farm1';
        farms[1].Name ='farm2';
        farms[2].Name ='farm3';
        insert farms;

        Individual_Relationship__c ir = new Individual_Relationship__c(
            RecordTypeId = individualFarmRecordTypeId,
            Individual__c = individual.Id,
            Farm__c = farms[0].Id,
            Active__c = true,
            Role__c = '',
            Party__c = party.Id
        );
        insert ir;
        Individual_Relationship__c ir2 = new Individual_Relationship__c(
            RecordTypeId = individualFarmRecordTypeId,
            Individual__c = individual2.Id,
            Farm__c = farms[1].Id,
            Active__c = true,
            Role__c = '',
            Party__c = party.Id
        );
        insert ir2;

        List<Group> queues = new List<Group>();
        queues.add(new Group(Type = 'Queue', Name = 'Outbound_Specialist'));
        insert queues;
    }

    @isTest static void testMyDashboard() {
        // set an id, you can't create a report in a test so just set any Id
        Account farm1 = [SELECT Id FROM Account WHERE Name = 'farm1'];
        BulkNotificationDashboardController.casesCreatedReportId = farm1.Id;
        System.assertEquals(farm1.Id, BulkNotificationDashboardController.casesCreatedReportId);

        BulkNotificationDashboardController.tasksCreatedReportId = farm1.Id;
        System.assertEquals(farm1.Id, BulkNotificationDashboardController.tasksCreatedReportId);

        List<Case> cases = new List<Case>();
        Id caseRt = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND Name = 'Communications AU'].Id;
        cases.add(new Case(Description = 'ID:test:ID', AccountId = farm1.Id, RecordTypeId = caseRt));
        insert cases;

        List<Task> tasks = new List<Task>();
        Id taskRt = [Select Id from RecordType Where SobjectType = 'Task' And Name = 'System Generated Task'].Id;
        tasks.add(new Task(Description = 'a task', SMS_Status_Reason__c = 'PENDING', SMS_Message_ID__c = '1', WhatId = cases[0].Id, RecordTypeId = taskRt));
        insert tasks;

        Test.setCurrentPage(Page.BulkNotificationDashboard);

        BulkNotificationDashboardController bndc = new BulkNotificationDashboardController();
    }

}