@isTest private class DocumentImageControllerTest {

    @isTest static void testDocument(){
        String DEVELOPER_NAME = 'my_document';

        List<Folder> folders = [SELECT Id FROM Folder WHERE Name = 'Buttons, Logos, Images'];
        if(!folders.isEmpty()){
            Document document = new Document();
            document.Body = Blob.valueOf('Some Text');
            document.ContentType = 'image/jpeg';
            document.DeveloperName = DEVELOPER_NAME;
            document.IsPublic = true;
            document.Name = 'My Document';
            document.FolderId = folders[0].id;
            insert document;

            DocumentImageController cont = new DocumentImageController();
            cont.documentDeveloperName = DEVELOPER_NAME;

            String url = cont.imageUrl;
            System.assert(url.contains(String.valueOf(document.Id)));
            System.assert(url.contains(DocumentImageController.IMAGE_URL));
        }
    }

}