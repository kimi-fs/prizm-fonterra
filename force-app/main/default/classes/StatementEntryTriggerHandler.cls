public class StatementEntryTriggerHandler {

    public static void deleteStatementsWithNoStatementEntries(Map<Id, Statement_Entry__c> oldMap){

    	Set<Id> statementIdSet = new Set<Id>();
    	for(Statement_Entry__c se : oldMap.values()){
    		statementIdSet.add(se.Statement__c);
    	}
		
		List<Statement__c> statements = new List<Statement__c>([SELECT Id, (SELECT Id FROM Statement_Entries__r) FROM Statement__c WHERE Id IN: statementIdSet]);

		List<Statement__c> statementsToDelete = new List<Statement__c>();
		for(Statement__c s : statements){
			if(s.Statement_Entries__r == null || s.Statement_Entries__r.size() == 0){
				statementsToDelete.add(s);
			}
		}

		delete statementsToDelete;
	}
}