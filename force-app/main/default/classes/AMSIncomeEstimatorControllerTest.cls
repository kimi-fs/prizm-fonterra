@isTest
private class AMSIncomeEstimatorControllerTest {
    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup static void testSetup() {
        SetupAMSIncomeEstimatorCustomSettings.createIncomeEstimatorCustomSettings();

        TestClassDataUtil.integrationUserProfile();

        TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        Contact individual = [SELECT Id FROM Contact WHERE Email = : USER_EMAIL];

        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        List<Reference_Region__c> referenceRegions = TestClassDataUtil.createAuReferenceRegions();
        insert referenceRegions;
        List<Account> farms = TestClassDataUtil.createAuFarms(referenceRegions);
        insert farms;

        TestClassDataUtil.createPrimaryAccessRelationships(individual.Id, party, farms);

        List<Reference_Period__c> referencePeriods = TestClassDataUtil.createReferencePeriods(5, 'Milking Season AU', true);
        List<Farm_Season__c> farmSeasons = TestClassDataUtil.createFarmSeasons(farms, referencePeriods, true);
        List<Payment_Split__c> paymentSplits = TestClassDataUtil.insertFarmPartiesAgreementsAndPaymentSplits(farms[0].Id);

        Agreement__c standardAgreement = new Agreement__c(Id = paymentSplits[0].Agreement__c, RecordTypeId = GlobalUtility.agreementStandardRecordTypeId, Specialty_Milk_Price__c = 'West Fresh');
        update standardAgreement;

        List<Collection_Summary__c> collectionSummaries = TestClassDataUtil.createSeasonOfMonthlyCollectionSummary(farms[0], true);

        Agreement__c contractAgreement = TestClassDataUtil.createAgreement(farms[0].Id, false);
        contractAgreement.RecordTypeId = GlobalUtility.agreementContractSupplyAURecordTypeId;
        insert contractAgreement;

        Season__c contractSeason = TestClassDataUtil.createContractAUSeason(contractAgreement.Id, true);
    }
     @isTest static void testController() {
        System.runAs(getCommunityUser()) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();

            System.assert(controller.entityRelationships.size() > 0, 'User should have access to some farms');
            System.assertEquals(SetupAMSIncomeEstimatorCustomSettings.adminData[0].Company_Milk_Price__c, controller.companyMilkPrice, 'Incorrect Company Milk Price');
            System.assertNotEquals(null, controller.selectedERelID, 'Farm not selected');
            System.assertNotEquals(null, controller.selectedFarmSeason, 'Farm Season not selected');
            System.assertEquals(false, controller.noFarmsWithAccess, 'noFarmsWithAccess incorrectly set');

            Test.stopTest();
        }
    }

    @isTest static void testController_UserWithNoFarms() {
        Test.startTest();
        List<Individual_Relationship__c> irs = [SELECT Id, Active__c FROM Individual_Relationship__c];
        for (Individual_Relationship__c ir : irs) {
            ir.Active__c = false;
        }
        update irs;
        System.runAs(getCommunityUser()) {
            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();

            System.assert(controller.entityRelationships.isEmpty(), 'User should not have access to any farms');
            System.assertEquals(null, controller.selectedERelID, 'Farm should not have been selected');
            System.assertEquals(null, controller.selectedFarmSeason, 'Farm Season should not have been selected');
            System.assertEquals(true, controller.noFarmsWithAccess, 'noFarmsWithAccess incorrectly set');
        }
        Test.stopTest();
    }

    @isTest static void testPopulateFarmSelectOptionsList() {
        User communityUser = getCommunityUser();
        System.runAs(communityUser) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();
            List<SelectOption> farmSelectOptions = controller.populateFarmSelectOptionsList();

            System.assert(farmSelectOptions.size() > 0, 'User should have access to some farms');
            System.assertEquals(RelationshipAccessService.accessToEntityRelationships(communityUser.ContactId, RelationshipAccessService.Access.ACCESS_STATEMENTS).size(), farmSelectOptions.size(), 'Incorrect number of farm select options');

            Test.stopTest();
        }
    }

    @isTest static void testGetPartyName() {
        System.runAs(getCommunityUser()) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();
            Entity_Relationship__c eRelToSelect = getERelToSelect(controller);
            controller.selectedERelId = eRelToSelect.Id;

            String partyName = controller.getPartyName();

            System.assertEquals(eRelToSelect.Party__r.Name, partyName, 'partyName incorrectly retrieved');

            Test.stopTest();
        }
    }

    @isTest static void testGenerateTableFromCollectionSummaryRecords() {
        System.runAs(getCommunityUser()) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();
            Entity_Relationship__c eRelToSelect = getERelToSelect(controller);
            controller.selectedERelId = eRelToSelect.Id;

            controller.generateTableFromCollectionSummaryRecords();

            System.assertNotEquals(null, controller.table, 'Table not generated');

            Test.stopTest();
        }
    }

    @isTest static void testGenerateTableFromInputsOnPage_CurrentSeason() {
        System.runAs(getCommunityUser()) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();
            Entity_Relationship__c eRelToSelect = getERelToSelect(controller);
            controller.selectedERelId = eRelToSelect.Id;

            controller.generateTableFromCollectionSummaryRecords();
            for (AMSIncomeEstimatorDataService.MonthData existingMonth : controller.table.monthData) {
                existingMonth.production_litres += 1;
                existingMonth.production_fat += 1;
                existingMonth.production_protein += 1;
                existingMonth.specialtyMilk_butterfatKgMS += 1;
                existingMonth.specialtyMilk_proteinKgMS += 1;
                existingMonth.production_firstStops += 1;
                existingMonth.production_secondStops += 1;
                existingMonth.production_qualityPoints += 1;
            }

            controller.generateTableFromInputsOnPage();

            System.assertNotEquals(null, controller.table, 'Table not generated');

            Test.stopTest();
        }
    }

    @isTest static void testGenerateTableFromInputsOnPage_ScenarioPlanner() {
        System.runAs(getCommunityUser()) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();
            controller.currentSeasonTab = false;
            Entity_Relationship__c eRelToSelect = getERelToSelect(controller);
            controller.selectedERelId = eRelToSelect.Id;

            controller.generateTableFromCollectionSummaryRecords();
            for (AMSIncomeEstimatorDataService.MonthData existingMonth : controller.table.monthData) {
                existingMonth.production_litres += 1;
                existingMonth.production_fat += 1;
                existingMonth.production_protein += 1;
                existingMonth.specialtyMilk_butterfatKgMS += 1;
                existingMonth.specialtyMilk_proteinKgMS += 1;
                existingMonth.production_firstStops += 1;
                existingMonth.production_secondStops += 1;
                existingMonth.production_qualityPoints += 1;
            }

            controller.generateTableFromInputsOnPage();

            System.assertNotEquals(null, controller.table, 'Table not generated');

            Test.stopTest();
        }
    }

    @isTest static void testGetFarmSeasonsAndGenerateTableFromCollectionSummaryRecords() {
        System.runAs(getCommunityUser()) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();
            Entity_Relationship__c eRelToSelect = getERelToSelect(controller);
            controller.selectedERelId = eRelToSelect.Id;

            controller.getFarmSeasonsAndGenerateTableFromCollectionSummaryRecords();

            System.assert(controller.farmSeasonSelectOptions.size() > 0, 'farmSeasonSelectOptions not generated');
            System.assertNotEquals(null, controller.table, 'Table not generated');

            Test.stopTest();
        }
    }

    @isTest static void testSetSelectedFarmSeasonAndGenerateTableFromCollectionSummaryRecords() {
        System.runAs(getCommunityUser()) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();
            Entity_Relationship__c eRelToSelect = getERelToSelect(controller);
            controller.selectedERelId = eRelToSelect.Id;
            controller.selectedFarmSeasonId = getFarmSeasonToSelect(controller);
            controller.getFarmSeasonsAndGenerateTableFromCollectionSummaryRecords();

            controller.setSelectedFarmSeasonAndGenerateTableFromCollectionSummaryRecords();

            System.assertNotEquals(null, controller.selectedFarmSeason, 'selectedFarmSeason not populated');
            System.assertNotEquals(null, controller.table, 'Table not generated');
            System.assertEquals(12, controller.table.monthData.size(), 'Wrong number of months');

            Test.stopTest();
        }
    }

    @isTest static void testSetSelectedFarmSeasonAndGenerateTableFromCollectionSummaryRecords_NoCollectionSummaryRecords() {
        delete [SELECT Id FROM Collection_Summary__c];

        System.runAs(getCommunityUser()) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();
            Entity_Relationship__c eRelToSelect = getERelToSelect(controller);
            controller.selectedERelId = eRelToSelect.Id;
            controller.selectedFarmSeasonId = getFarmSeasonToSelect(controller);
            controller.getFarmSeasonsAndGenerateTableFromCollectionSummaryRecords();

            controller.setSelectedFarmSeasonAndGenerateTableFromCollectionSummaryRecords();

            System.assertNotEquals(null, controller.selectedFarmSeason, 'selectedFarmSeason not populated');
            System.assertNotEquals(null, controller.table, 'Table not generated');
            System.assertEquals(12, controller.table.monthData.size(), 'Wrong number of months');

            Test.stopTest();
        }
    }

    @isTest static void testGeneratePDFFileName() {
        System.runAs(getCommunityUser()) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();
            Entity_Relationship__c eRelToSelect = getERelToSelect(controller);
            controller.selectedERelId = eRelToSelect.Id;
            controller.generateTableFromCollectionSummaryRecords();

            String fileName = controller.generatePDFFileName();

            System.assert(fileName.contains(eRelToSelect.Farm__r.Name.replaceAll('[^0-9a-zA-Z]', '_')), 'File name does not contain farm name.');

            Test.stopTest();
        }
    }

    @isTest static void testSaveToPDF() {
        System.runAs(getCommunityUser()) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();
            Entity_Relationship__c eRelToSelect = getERelToSelect(controller);
            controller.selectedERelId = eRelToSelect.Id;
            controller.generateTableFromCollectionSummaryRecords();

            controller.saveToPDF();

            System.assertEquals('PDF', controller.renderingService);

            Test.stopTest();
        }
    }

    @isTest static void testGetSelectedCompanyMilkPrice_CurrentSeasonTab() {
        System.runAs(getCommunityUser()) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();
            Entity_Relationship__c eRelToSelect = getERelToSelect(controller);
            controller.selectedERelId = eRelToSelect.Id;
            controller.generateTableFromCollectionSummaryRecords();

            System.assertEquals('$' + String.valueOf(controller.companyMilkPrice), controller.getSelectedCompanyMilkPriceValue(), 'companyMilkPrice not correct for current season tab.');

            Test.stopTest();
        }
    }

    @isTest static void testGetSelectedCompanyMilkPrice_ScenarioPlannerTab() {
        System.runAs(getCommunityUser()) {
            Test.startTest();

            AMSIncomeEstimatorController controller = new AMSIncomeEstimatorController();
            controller.currentSeasonTab = false;

            Entity_Relationship__c eRelToSelect = getERelToSelect(controller);
            controller.selectedERelId = eRelToSelect.Id;

            SelectOption selectedMilkPrice = controller.companyMilkPriceSelectOptions[1];
            controller.selectedMilkPriceStep = selectedMilkPrice.getValue();

            System.assertEquals(selectedMilkPrice.getLabel(), controller.getSelectedCompanyMilkPriceValue(), 'companyMilkPrice not correct for scenario planner tab.');

            Test.stopTest();
        }
    }

    private static Entity_Relationship__c getERelToSelect(AMSIncomeEstimatorController controller) {
        Entity_Relationship__c eRelToSelect;
        for (Id i : controller.entityRelationships.keyset()) {
            eRelToSelect = controller.entityRelationships.get(i);
            break;
        }

        return eRelToSelect;
    }

    private static String getFarmSeasonToSelect(AMSIncomeEstimatorController controller) {
        String fsToSelect;
        if (controller.farmSeasonSelectOptions != null && !controller.farmSeasonSelectOptions.isEmpty()) {
            fsToSelect = controller.farmSeasonSelectOptions[0].getValue();
        }

        return fsToSelect;
    }

    private static User getCommunityUser() {
        User theUser = [SELECT Id, ContactId FROM User WHERE Email = : USER_EMAIL ];

        return theUser;
    }
}