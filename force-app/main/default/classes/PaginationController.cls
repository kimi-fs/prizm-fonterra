/**
 * Description: Creates pagination links for a page
 *              Uses URL parameter and a full page refresh
 *              Only handles the displaying of pagination and links to the page, its up to the controller to determine what to show on the page
 *              Contains some static functions to help the controller figure out which records to show
 * @author: Amelia (Trineo)
 * @date: March 2018
 */
public with sharing class PaginationController {
    public Integer totalNumberOfRecords {get;set;}
    public Integer recordsShownPerPage {get;set;}
    public Integer numberOfPagesToShow {get;set;}

    private Integer currentPage {get;set;}

    // wrapper for the pagination links
    public class PaginationLink{
        public String label {get;set;}
        public Integer pageNumber {get;set;}
        public PageReference link {get; private set;}

        public PaginationLink(String label, Integer pageNumber){
            this.label = label;
            // if its the current page dont add the number so we dont create this one as a link
            if(pageNumber != getCurrentPageNumber()){
                this.pageNumber = pageNumber;
            }
            this.link = createPageLink();
        }

        public Boolean getShowAsLink(){
            return this.pageNumber != null;
        }

        public PageReference createPageLink(){
            PageReference pr = new PageReference(ApexPages.currentPage().getUrl());
            pr.getParameters().put(getPageNumberParameterName(), String.valueOf(this.pageNumber));
            return pr;
        }
    }

    public PaginationController() {
        this.currentPage = getCurrentPageNumber();
    }

    public List<PaginationLink> getPaginationLinks() {
        Integer pageNumber = this.currentPage;
        Integer numberOfPages = getNumberOfPages(this.totalNumberOfRecords, this.recordsShownPerPage);

        List<PaginationLink> paginationLinks = new List<PaginationLink>();

        //create the prev link if we're not on the first page
        if(pageNumber != 1){
            paginationLinks.add(new PaginationLink('Prev', pageNumber - 1));
        }

        // always create the first page
        paginationLinks.add(new PaginationLink('1', 1));

        //create if the current page is not within the limit from 1
        if(pageNumber > this.numberOfPagesToShow+1){
            paginationLinks.add(new PaginationLink('...', null));
        }

        for(Integer i=2; i<numberOfPages; i++){
            //create if within limit the current page
            if(i > pageNumber-this.numberOfPagesToShow && i < pageNumber+this.numberOfPagesToShow){
                paginationLinks.add(new PaginationLink(String.valueOf(i), i));
            }
        }

        //create if the current page is not within the limit from the total number of pages
        if(pageNumber < (numberOfPages-this.numberOfPagesToShow)){
            paginationLinks.add(new PaginationLink('...', null));
        }

        //create the last page if there is more the one
        if(numberOfPages > 1){
            paginationLinks.add(new PaginationLink(String.valueOf(numberOfPages), numberOfPages));
        }

        // create the next link if we're not on the last page
        if(pageNumber != numberOfPages){
            paginationLinks.add(new PaginationLink('Next', pageNumber + 1));
        }

        return paginationLinks;
    }

    private static Integer getNumberOfPages(Integer numberOfRecords, Integer recordsPerPage) {
        Integer numberOfPages = (numberOfRecords / recordsPerPage);
        if (Math.mod(numberOfRecords, recordsPerPage) != 0) {
            numberOfPages++;
        }
        numberOfPages = numberOfPages < 1 ? 1 : numberOfPages;
        return numberOfPages;
    }

    public static String getPageNumberParameterName() {
        return AMSCommunityUtil.getPageNumberParameterName();
    }

    /**
    * Static helper functions to call from the page controller to help get the correct records
    */
    // Takes the value of the url parameter and converts to an integer or defaults to 1
    public static Integer getCurrentPageNumber() {
        Integer pageNumber = 1;
        try {
            pageNumber = Integer.valueOf(ApexPages.currentPage().getParameters().get(getPageNumberParameterName()));
        }
        catch (Exception e) {
            pageNumber = 1;
        }
        if (pageNumber == null || pageNumber < 1) {
            pageNumber = 1;
        }
        return pageNumber;
    }

    // Call this function to get the offset for a SOQL query
    public static Integer getOffset(Integer recordsPerPage){
        return (getCurrentPageNumber() - 1) * recordsPerPage;
    }

    // Call this function to get a subset of a list of records to show on the page
    public static List<sObject> getRecordsToDisplayOnPage(Integer recordsPerPage, List<sObject> allRecords){
        List<sObject> recordsForPage = new List<sObject>();
        Integer offset = getOffset(recordsPerPage);
        Integer upperLimit = offset + recordsPerPage;
        for(Integer i=offset; i<upperLimit; i++){
            if(i >= allRecords.size()){
                // we've hit the limit of the farms we have
                break;
            }
            recordsForPage.add(allRecords[i]);
        }
        return recordsForPage;
    }

}