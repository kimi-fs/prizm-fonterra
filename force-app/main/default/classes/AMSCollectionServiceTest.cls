/**
 * AMSCollectionServiceTest
 *
 * Test the AMSFormsController
 *
 * Author: Nick (Trineo)
 * Date: August 2017
 */
@isTest
private class AMSCollectionServiceTest {
    @TestSetup
    static void integrationUserAndSeasonData() {
        //Create a integration user
        TestClassDataUtil.individualDefaultAccountAu();
        TestClassDataUtil.integrationUserProfile();
        TestClassDataUtil.createIntegrationUser();
        Date today = System.today();
        Reference_Period__c currentReferencePeriod = TestClassDataUtil.createReferencePeriod('Milking Season AU', 'current', false);
        currentReferencePeriod.Status__c = 'Current';
        insert currentReferencePeriod;
        Reference_Period__c priorReferencePeriod = TestClassDataUtil.createReferencePeriod('Milking Season AU', 'prior', false);
        priorReferencePeriod.Status__c = 'Past';
        insert priorReferencePeriod;
    }

    @isTest
    static void checkFormCreationForMonth() {
        //Create community user and associated farms
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
        List<Account> farms;
        Farm_Season__c currentFarmSeason, priorFarmSeason;
        Date today = System.today();

        //Get farms created during user set up
        farms = [
                SELECT Id
                FROM Account
                WHERE Id IN (
                        SELECT Farm__c
                        FROM Individual_Relationship__c
                        WHERE Individual__c = :communityUser.ContactId
                        AND Active__c = true
                )
        ];
        //reference periods already exist in system, get them
        Reference_Period__c currentReferencePeriod;
        Reference_Period__c priorReferencePeriod;
        for (Reference_Period__c referencePeriod : [
                SELECT Id,
                        Start_Date__c,
                        End_Date__c,
                        Reference_Period__c
                FROM Reference_Period__c
                WHERE Start_Date__c <= :today
                ORDER BY Start_Date__c DESC
        ]) {
            if (currentReferencePeriod == null && referencePeriod.Start_Date__c < today && referencePeriod.End_Date__c > today) {
                currentReferencePeriod = referencePeriod;
            } else if (currentReferencePeriod != null && priorReferencePeriod == null) {
                priorReferencePeriod = referencePeriod;
            }
        }
        //create farm seasons for associated farms
        currentFarmSeason = TestClassDataUtil.createFarmSeasons(new List<Account>{
                farms[0]
        }, new List<Reference_Period__c>{
                currentReferencePeriod
        }, false)[0];
        currentFarmSeason.Peak_Cows__c = 10;
        currentFarmSeason.Dairy_Hectares__c = 10;
        insert currentFarmSeason;
        priorFarmSeason = TestClassDataUtil.createFarmSeasons(new List<Account>{
                farms[0]
        }, new List<Reference_Period__c>{
                priorReferencePeriod
        }, false)[0];
        priorFarmSeason.Peak_Cows__c = 10;
        priorFarmSeason.Dairy_Hectares__c = 10;
        insert priorFarmSeason;
        User integrationUser = [
                SELECT Id
                FROM User
                WHERE IsActive = true AND Profile.Name = 'Integration User'
                LIMIT 1
        ];
        //Create 2 collections for today and 2 for same date last year
        Collection__c todayCollection1 = TestClassDataUtil.createSingleCollection(farms[0], false);
        todayCollection1.Pick_Up_Date__c = today;
        todayCollection1.Volume_Ltrs__c = 100;
        todayCollection1.Fat_Percent__c = 10;
        todayCollection1.Prot_Percent__c = 10;
        todayCollection1.Fat_Kgs__c = 10;
        todayCollection1.Prot_Kgs__c = 10;
        //todayCollection1.Total_KgMS__c is formula field = Prot_Kgs__c + Fat_Kgs__c = 20
        //todayCollection1.Protein_to_Fat_Ratio__c is formula field  = Prot_Kgs__c/Fat_Kgs__c = 1;
        Collection__c todayCollection2 = todayCollection1.clone();
        Collection__c lastYearCollection1 = TestClassDataUtil.createSingleCollection(farms[0], false);
        lastYearCollection1.Pick_Up_Date__c = today.addYears(-1);
        lastYearCollection1.Volume_Ltrs__c = 50;
        lastYearCollection1.Fat_Percent__c = 5;
        lastYearCollection1.Prot_Percent__c = 5;
        lastYearCollection1.Fat_Kgs__c = 5;
        lastYearCollection1.Prot_Kgs__c = 5;
        //Collections are inserted as integration user as they would be in production
        System.runAs(integrationUser) {
            insert new List<Collection__c>{
                    todayCollection1, todayCollection2, lastYearCollection1
            };
        }
        //Create Milk Quality data associated with Collection 1
        Milk_Quality__c todayMilkQualityTest1 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest1.Name = 'Temperature';
        todayMilkQualityTest1.Result_Interpretation__c = null;
        todayMilkQualityTest1.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest2 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest2.Name = 'Cell Count';
        todayMilkQualityTest2.Result_Interpretation__c = null;
        todayMilkQualityTest2.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest3 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest3.Name = 'Custom Test 1';
        todayMilkQualityTest3.Result_Interpretation__c = null;
        todayMilkQualityTest3.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest4 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest4.Name = 'Custom Test 2';
        todayMilkQualityTest4.Result_Interpretation__c = null;
        todayMilkQualityTest4.Test_Result__c = '5';
        //Create Milk Quality data associated with last year's collection
        Milk_Quality__c todayMilkQualityTest5 = TestClassDataUtil.createMilkQuality(lastYearCollection1, false);
        todayMilkQualityTest5.Name = 'Temperature';
        todayMilkQualityTest5.Result_Interpretation__c = null;
        todayMilkQualityTest5.Test_Result__c = '3';
        Milk_Quality__c todayMilkQualityTest6 = TestClassDataUtil.createMilkQuality(lastYearCollection1, false);
        todayMilkQualityTest6.Name = 'Cell Count';
        todayMilkQualityTest6.Result_Interpretation__c = null;
        todayMilkQualityTest6.Test_Result__c = '3';
        //Milk quality is inserted as integration user
        System.runAs(integrationUser) {
            insert new List<Milk_Quality__c>{
                    todayMilkQualityTest1,
                    todayMilkQualityTest2,
                    todayMilkQualityTest3,
                    todayMilkQualityTest4,
                    todayMilkQualityTest5,
                    todayMilkQualityTest6
            };
        }

        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = new AMSCollectionService.CollectionPeriodForm(
                today,
                today,
                today.addYears(-1),
                today.addYears(-1),
                priorFarmSeason,
                currentFarmSeason,
                farms[0].Id,
                'Month');
        //verify default assignments occurred correctly
        System.assert(collectionPeriodForm.priorFarmSeason == priorFarmSeason, 'Prior farm season not assigned');
        System.assert(collectionPeriodForm.selectedFarmSeason == currentFarmSeason, 'Current farm season not assigned');
        System.assert(collectionPeriodForm.degreeofIteration == 'Month', 'Degree of iteration incorrect');
        //verify sample calculations
        System.assert(collectionPeriodForm.collectionPeriodWrapperList.size() == 1, 'Collection Period Wrapper List expected 1 received ' + collectionPeriodForm.collectionPeriodWrapperList.size());
        System.assert(collectionPeriodForm.warningCount == 0, 'Warning count is incorrect received' + collectionPeriodForm.warningCount);
        System.assert(collectionPeriodForm.totalCollectionWrapper.collectionPeriod.kgMSTotal == 40, 'KgMSTotal is incorrect. Expected 40 received ' + collectionPeriodForm.totalCollectionWrapper.collectionPeriod.kgMSTotal);
        System.assert(collectionPeriodForm.totalCollectionWrapper.collectionPeriod.volumeLitresTotal == 200, 'Volume Litres sum is inaccurate. Expected 200, received ' + collectionPeriodForm.totalCollectionWrapper.collectionPeriod.volumeLitresTotal);
        System.assert(collectionPeriodForm.averageCollectionWrapper.collectionPeriod.kgMSTotal == 40, 'Average calculation of kgMS is not correct. Expected 40, received ' + collectionPeriodForm.averageCollectionWrapper.collectionPeriod.kgMSTotal);
    }

    @isTest
    static void checkFormCreationForSeason() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
        Date today = System.today();
        //Get farms created during user set up
        List<Account> farms = [
                SELECT Id
                FROM Account
                WHERE Id IN (
                        SELECT Farm__c
                        FROM Individual_Relationship__c
                        WHERE Individual__c = :communityUser.ContactId
                        AND Active__c = true
                )
        ];
        //reference periods already exist in system, get them
        Reference_Period__c currentReferencePeriod;
        Reference_Period__c priorReferencePeriod;
        for (Reference_Period__c referencePeriod : [
                SELECT Id,
                        Start_Date__c,
                        End_Date__c,
                        Reference_Period__c
                FROM Reference_Period__c
                WHERE Start_Date__c <= :today
                ORDER BY Start_Date__c DESC
        ]) {
            if (currentReferencePeriod == null && referencePeriod.Start_Date__c < today && referencePeriod.End_Date__c > today) {
                currentReferencePeriod = referencePeriod;
            } else if (currentReferencePeriod != null && priorReferencePeriod == null) {
                priorReferencePeriod = referencePeriod;
            }
        }
        //create farm seasons for associated farms
        Farm_Season__c currentFarmSeason = TestClassDataUtil.createFarmSeasons(new List<Account>{
                farms[0]
        }, new List<Reference_Period__c>{
                currentReferencePeriod
        }, false)[0];
        currentFarmSeason.Peak_Cows__c = 10;
        currentFarmSeason.Dairy_Hectares__c = 10;
        insert currentFarmSeason;
        Farm_Season__c priorFarmSeason = TestClassDataUtil.createFarmSeasons(new List<Account>{
                farms[0]
        }, new List<Reference_Period__c>{
                priorReferencePeriod
        }, false)[0];
        priorFarmSeason.Peak_Cows__c = 10;
        priorFarmSeason.Dairy_Hectares__c = 10;
        insert priorFarmSeason;
        User integrationUser = [
                SELECT Id
                FROM User
                WHERE IsActive = true AND Profile.Name = 'Integration User'
                LIMIT 1
        ];
        //Create 2 collections for today and 2 for same date last year
        Collection__c todayCollection1 = TestClassDataUtil.createSingleCollection(farms[0], false);
        todayCollection1.Pick_Up_Date__c = today;
        todayCollection1.Volume_Ltrs__c = 100;
        todayCollection1.Fat_Percent__c = 10;
        todayCollection1.Prot_Percent__c = 10;
        todayCollection1.Fat_Kgs__c = 10;
        todayCollection1.Prot_Kgs__c = 10;
        //todayCollection1.Total_KgMS__c is formula field = Prot_Kgs__c + Fat_Kgs__c = 20
        //todayCollection1.Protein_to_Fat_Ratio__c is formula field  = Prot_Kgs__c/Fat_Kgs__c = 1;
        Collection__c todayCollection2 = todayCollection1.clone();
        Collection__c lastYearCollection1 = TestClassDataUtil.createSingleCollection(farms[0], false);
        lastYearCollection1.Pick_Up_Date__c = today.addYears(-1);
        lastYearCollection1.Volume_Ltrs__c = 50;
        lastYearCollection1.Fat_Percent__c = 5;
        lastYearCollection1.Prot_Percent__c = 5;
        lastYearCollection1.Fat_Kgs__c = 5;
        lastYearCollection1.Prot_Kgs__c = 5;

        System.runAs(integrationUser) {
            insert new List<Collection__c>{
                    todayCollection1, todayCollection2, lastYearCollection1
            };
        }
        //Create Milk Quality data associated with Collection 1
        Milk_Quality__c todayMilkQualityTest1 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest1.Name = 'Temperature';
        todayMilkQualityTest1.Result_Interpretation__c = null;
        todayMilkQualityTest1.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest2 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest2.Name = 'Cell Count';
        todayMilkQualityTest2.Result_Interpretation__c = null;
        todayMilkQualityTest2.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest3 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest3.Name = 'Custom Test 1';
        todayMilkQualityTest3.Result_Interpretation__c = null;
        todayMilkQualityTest3.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest4 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest4.Name = 'Custom Test 2';
        todayMilkQualityTest4.Result_Interpretation__c = null;
        todayMilkQualityTest4.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest5 = TestClassDataUtil.createMilkQuality(lastYearCollection1, false);
        todayMilkQualityTest5.Name = 'Temperature';
        todayMilkQualityTest5.Result_Interpretation__c = null;
        todayMilkQualityTest5.Test_Result__c = '3';
        Milk_Quality__c todayMilkQualityTest6 = TestClassDataUtil.createMilkQuality(lastYearCollection1, false);
        todayMilkQualityTest6.Name = 'Cell Count';
        todayMilkQualityTest6.Result_Interpretation__c = null;
        todayMilkQualityTest6.Test_Result__c = '3';
        System.runAs(integrationUser) {
            insert new List<Milk_Quality__c>{
                    todayMilkQualityTest1,
                    todayMilkQualityTest2,
                    todayMilkQualityTest3,
                    todayMilkQualityTest4,
                    todayMilkQualityTest5,
                    todayMilkQualityTest6
            };
        }
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = new AMSCollectionService.CollectionPeriodForm(
                today,
                today,
                today.addYears(-1),
                today.addYears(-1),
                priorFarmSeason,
                currentFarmSeason,
                farms[0].Id,
                'Season');
        //verify default assignments occurred correctly
        System.assert(collectionPeriodForm.priorFarmSeason == priorFarmSeason, 'Prior farm season not assigned');
        System.assert(collectionPeriodForm.selectedFarmSeason == currentFarmSeason, 'Current farm season not assigned');
        System.assert(collectionPeriodForm.degreeofIteration == 'Season', 'Degree of iteration incorrect');
        //verify calculations
        System.assert(collectionPeriodForm.collectionPeriodWrapperList.size() == 1, 'Collection Period Wrapper List expected 1 received ' + collectionPeriodForm.collectionPeriodWrapperList.size());
        System.assert(collectionPeriodForm.warningCount == 0, 'Warning count is incorrect received' + collectionPeriodForm.warningCount);
        System.assert(collectionPeriodForm.totalCollectionWrapper.collectionPeriod.kgMSTotal == 40, 'KgMSTotal is incorrect. Expected 40 received ' + collectionPeriodForm.totalCollectionWrapper.collectionPeriod.kgMSTotal);
        System.assert(collectionPeriodForm.totalCollectionWrapper.collectionPeriod.volumeLitresTotal == 200, 'Volume Litres sum is inaccurate. Expected 200, received ' + collectionPeriodForm.totalCollectionWrapper.collectionPeriod.volumeLitresTotal);
        System.assert(collectionPeriodForm.averageCollectionWrapper.collectionPeriod.kgMSTotal == 40, 'Average calculation of kgMS is not correct. Expected 40, received ' + collectionPeriodForm.averageCollectionWrapper.collectionPeriod.kgMSTotal);
    }

    @isTest
    static void checkFormCreationForDay() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
        Date today = System.today();
        //Get farms created during user set up
        List<Account> farms = [
                SELECT Id
                FROM Account
                WHERE Id IN (
                        SELECT Farm__c
                        FROM Individual_Relationship__c
                        WHERE Individual__c = :communityUser.ContactId
                        AND Active__c = true
                )
        ];
        //reference periods already exist in system, get them
        Reference_Period__c currentReferencePeriod;
        Reference_Period__c priorReferencePeriod;
        for (Reference_Period__c referencePeriod : [
                SELECT Id,
                        Start_Date__c,
                        End_Date__c,
                        Reference_Period__c
                FROM Reference_Period__c
                WHERE Start_Date__c <= :today
                ORDER BY Start_Date__c DESC
        ]) {
            if (currentReferencePeriod == null && referencePeriod.Start_Date__c < today && referencePeriod.End_Date__c > today) {
                currentReferencePeriod = referencePeriod;
            } else if (currentReferencePeriod != null && priorReferencePeriod == null) {
                priorReferencePeriod = referencePeriod;
            }
        }
        //create farm seasons for associated farms
        Farm_Season__c currentFarmSeason = TestClassDataUtil.createFarmSeasons(new List<Account>{
                farms[0]
        }, new List<Reference_Period__c>{
                currentReferencePeriod
        }, false)[0];
        currentFarmSeason.Peak_Cows__c = 10;
        currentFarmSeason.Dairy_Hectares__c = 10;
        insert currentFarmSeason;
        Farm_Season__c priorFarmSeason = TestClassDataUtil.createFarmSeasons(new List<Account>{
                farms[0]
        }, new List<Reference_Period__c>{
                priorReferencePeriod
        }, false)[0];
        priorFarmSeason.Peak_Cows__c = 10;
        priorFarmSeason.Dairy_Hectares__c = 10;
        insert priorFarmSeason;
        User integrationUser = [
                SELECT Id
                FROM User
                WHERE IsActive = true AND Profile.Name = 'Integration User'
                LIMIT 1
        ];
        //Create 2 collections for today and 2 for same date last year
        Collection__c todayCollection1 = TestClassDataUtil.createSingleCollection(farms[0], false);
        todayCollection1.Pick_Up_Date__c = today;
        todayCollection1.Volume_Ltrs__c = 100;
        todayCollection1.Fat_Percent__c = 10;
        todayCollection1.Prot_Percent__c = 10;
        todayCollection1.Fat_Kgs__c = 10;
        todayCollection1.Prot_Kgs__c = 10;
        //todayCollection1.Total_KgMS__c is formula field = Prot_Kgs__c + Fat_Kgs__c = 20
        //todayCollection1.Protein_to_Fat_Ratio__c is formula field  = Prot_Kgs__c/Fat_Kgs__c = 1;
        Collection__c todayCollection2 = todayCollection1.clone();
        Collection__c lastYearCollection1 = TestClassDataUtil.createSingleCollection(farms[0], false);
        lastYearCollection1.Pick_Up_Date__c = today.addYears(-1);
        lastYearCollection1.Volume_Ltrs__c = 50;
        lastYearCollection1.Fat_Percent__c = 5;
        lastYearCollection1.Prot_Percent__c = 5;
        lastYearCollection1.Fat_Kgs__c = 5;
        lastYearCollection1.Prot_Kgs__c = 5;

        System.runAs(integrationUser) {
            insert new List<Collection__c>{
                    todayCollection1, todayCollection2, lastYearCollection1
            };
        }
        //Create Milk Quality data associated with Collection 1
        Milk_Quality__c todayMilkQualityTest1 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest1.Name = 'Temperature';
        todayMilkQualityTest1.Result_Interpretation__c = null;
        todayMilkQualityTest1.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest2 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest2.Name = 'Cell Count';
        todayMilkQualityTest2.Result_Interpretation__c = null;
        todayMilkQualityTest2.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest3 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest3.Name = 'Custom Test 1';
        todayMilkQualityTest3.Result_Interpretation__c = null;
        todayMilkQualityTest3.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest4 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest4.Name = 'Custom Test 2';
        todayMilkQualityTest4.Result_Interpretation__c = null;
        todayMilkQualityTest4.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest5 = TestClassDataUtil.createMilkQuality(lastYearCollection1, false);
        todayMilkQualityTest5.Name = 'Temperature';
        todayMilkQualityTest5.Result_Interpretation__c = null;
        todayMilkQualityTest5.Test_Result__c = '3';
        Milk_Quality__c todayMilkQualityTest6 = TestClassDataUtil.createMilkQuality(lastYearCollection1, false);
        todayMilkQualityTest6.Name = 'Cell Count';
        todayMilkQualityTest6.Result_Interpretation__c = null;
        todayMilkQualityTest6.Test_Result__c = '3';
        System.runAs(integrationUser) {
            insert new List<Milk_Quality__c>{
                    todayMilkQualityTest1,
                    todayMilkQualityTest2,
                    todayMilkQualityTest3,
                    todayMilkQualityTest4,
                    todayMilkQualityTest5,
                    todayMilkQualityTest6
            };
        }
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = new AMSCollectionService.CollectionPeriodForm(
                today,
                today,
                today.addYears(-1),
                today.addYears(-1),
                priorFarmSeason,
                currentFarmSeason,
                farms[0].Id,
                'Day');
        //verify default assignments occurred correctly
        System.assert(collectionPeriodForm.priorFarmSeason == priorFarmSeason, 'Prior farm season not assigned');
        System.assert(collectionPeriodForm.selectedFarmSeason == currentFarmSeason, 'Current farm season not assigned');
        System.assert(collectionPeriodForm.degreeofIteration == 'Day', 'Degree of iteration incorrect');
        //verify calculations
        System.assert(collectionPeriodForm.collectionPeriodWrapperList.size() == 1, 'Collection Period Wrapper List expected 1 received ' + collectionPeriodForm.collectionPeriodWrapperList.size());
        System.assert(collectionPeriodForm.warningCount == 0, 'Warning count is incorrect received' + collectionPeriodForm.warningCount);
        System.assert(collectionPeriodForm.totalCollectionWrapper.collectionPeriod.kgMSTotal == 40, 'KgMSTotal is incorrect. Expected 40 received ' + collectionPeriodForm.totalCollectionWrapper.collectionPeriod.kgMSTotal);
        System.assert(collectionPeriodForm.totalCollectionWrapper.collectionPeriod.volumeLitresTotal == 200, 'Volume Litres sum is inaccurate. Expected 200, received ' + collectionPeriodForm.totalCollectionWrapper.collectionPeriod.volumeLitresTotal);
        System.assert(collectionPeriodForm.averageCollectionWrapper.collectionPeriod.kgMSTotal == 40, 'Average calculation of kgMS is not correct. Expected 40, received ' + collectionPeriodForm.averageCollectionWrapper.collectionPeriod.kgMSTotal);
    }

    @isTest
    static void checkFormCreationForMonthWithAlerts() {
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
        Date today = System.today();
        //Get farms created during user set up
        List<Account> farms = [
                SELECT Id
                FROM Account
                WHERE Id IN (
                        SELECT Farm__c
                        FROM Individual_Relationship__c
                        WHERE Individual__c = :communityUser.ContactId
                        AND Active__c = true
                )
        ];
        //reference periods already exist in system, get them
        Reference_Period__c currentReferencePeriod;
        Reference_Period__c priorReferencePeriod;
        for (Reference_Period__c referencePeriod : [
                SELECT Id,
                        Start_Date__c,
                        End_Date__c,
                        Reference_Period__c
                FROM Reference_Period__c
                WHERE Start_Date__c <= :today
                ORDER BY Start_Date__c DESC
        ]) {
            if (currentReferencePeriod == null && referencePeriod.Start_Date__c < today && referencePeriod.End_Date__c > today) {
                currentReferencePeriod = referencePeriod;
            } else if (currentReferencePeriod != null && priorReferencePeriod == null) {
                priorReferencePeriod = referencePeriod;
            }
        }
        //create farm seasons for associated farms
        Farm_Season__c currentFarmSeason = TestClassDataUtil.createFarmSeasons(new List<Account>{
                farms[0]
        }, new List<Reference_Period__c>{
                currentReferencePeriod
        }, false)[0];
        currentFarmSeason.Peak_Cows__c = 10;
        currentFarmSeason.Dairy_Hectares__c = 10;
        insert currentFarmSeason;
        Farm_Season__c priorFarmSeason = TestClassDataUtil.createFarmSeasons(new List<Account>{
                farms[0]
        }, new List<Reference_Period__c>{
                priorReferencePeriod
        }, false)[0];
        priorFarmSeason.Peak_Cows__c = 10;
        priorFarmSeason.Dairy_Hectares__c = 10;
        insert priorFarmSeason;
        User integrationUser = [
                SELECT Id
                FROM User
                WHERE IsActive = true AND Profile.Name = 'Integration User'
                LIMIT 1
        ];
        //Create 2 collections for today and 2 for same date last year
        Collection__c todayCollection1 = TestClassDataUtil.createSingleCollection(farms[0], false);
        todayCollection1.Pick_Up_Date__c = today;
        todayCollection1.Volume_Ltrs__c = 100;
        todayCollection1.Fat_Percent__c = 10;
        todayCollection1.Prot_Percent__c = 10;
        todayCollection1.Fat_Kgs__c = 10;
        todayCollection1.Prot_Kgs__c = 10;
        //todayCollection1.Total_KgMS__c is formula field = Prot_Kgs__c + Fat_Kgs__c = 20
        //todayCollection1.Protein_to_Fat_Ratio__c is formula field  = Prot_Kgs__c/Fat_Kgs__c = 1;
        Collection__c todayCollection2 = todayCollection1.clone();
        Collection__c lastYearCollection1 = TestClassDataUtil.createSingleCollection(farms[0], false);
        lastYearCollection1.Pick_Up_Date__c = today.addYears(-1);
        lastYearCollection1.Volume_Ltrs__c = 50;
        lastYearCollection1.Fat_Percent__c = 5;
        lastYearCollection1.Prot_Percent__c = 5;
        lastYearCollection1.Fat_Kgs__c = 5;
        lastYearCollection1.Prot_Kgs__c = 5;

        System.runAs(integrationUser) {
            insert new List<Collection__c>{
                    todayCollection1, todayCollection2, lastYearCollection1
            };
        }
        //Create Milk Quality data associated with Collection 1
        Milk_Quality__c todayMilkQualityTest1 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest1.Name = 'Temperature';
        todayMilkQualityTest1.Result_Interpretation__c = 'Alert';
        todayMilkQualityTest1.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest2 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest2.Name = 'Cell Count';
        todayMilkQualityTest2.Result_Interpretation__c = 'Alert';
        todayMilkQualityTest2.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest3 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest3.Name = 'Custom Test 1';
        todayMilkQualityTest3.Result_Interpretation__c = null;
        todayMilkQualityTest3.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest4 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest4.Name = 'Custom Test 2';
        todayMilkQualityTest4.Result_Interpretation__c = null;
        todayMilkQualityTest4.Test_Result__c = '5';
        Milk_Quality__c todayMilkQualityTest5 = TestClassDataUtil.createMilkQuality(lastYearCollection1, false);
        todayMilkQualityTest5.Name = 'Temperature';
        todayMilkQualityTest5.Result_Interpretation__c = null;
        todayMilkQualityTest5.Test_Result__c = '3';
        Milk_Quality__c todayMilkQualityTest6 = TestClassDataUtil.createMilkQuality(lastYearCollection1, false);
        todayMilkQualityTest6.Name = 'Cell Count';
        todayMilkQualityTest6.Result_Interpretation__c = null;
        todayMilkQualityTest6.Test_Result__c = '3';
        
        // Added hidden on AMS Milk results. Results should not be added to the warning count
        Milk_Quality__c todayMilkQualityTest7 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest7.Name = 'Cell Count';
        todayMilkQualityTest7.Result_Interpretation__c = 'Alert';
        todayMilkQualityTest7.Test_Result__c = '670';
        todayMilkQualityTest7.Hidden_on_AMS_Community__c = true;
        Milk_Quality__c todayMilkQualityTest8 = TestClassDataUtil.createMilkQuality(todayCollection1, false);
        todayMilkQualityTest8.Name = 'Temperature';
        todayMilkQualityTest8.Result_Interpretation__c = 'Alert';
        todayMilkQualityTest8.Test_Result__c = '670';
        todayMilkQualityTest8.Hidden_on_AMS_Community__c = true;
        
        System.runAs(integrationUser) {
            insert new List<Milk_Quality__c>{
                    todayMilkQualityTest1,
                    todayMilkQualityTest2,
                    todayMilkQualityTest3,
                    todayMilkQualityTest4,
                    todayMilkQualityTest5,
                    todayMilkQualityTest6,
                    todayMilkQualityTest7,
                    todayMilkQualityTest8
            };
        }
        AMSCollectionService.CollectionPeriodForm collectionPeriodForm = new AMSCollectionService.CollectionPeriodForm(
                today,
                today,
                today.addYears(-1),
                today.addYears(-1),
                priorFarmSeason,
                currentFarmSeason,
                farms[0].Id,
                'Day');
        //verify default assignments occurred correctly
        //verify calculations
        System.assert(collectionPeriodForm.collectionPeriodWrapperList.size() == 1, 'Collection Period Wrapper List expected 1 received ' + collectionPeriodForm.collectionPeriodWrapperList.size());
        System.assert(collectionPeriodForm.warningCount == 2, 'Warning count is incorrect. Expected 2, received ' + collectionPeriodForm.warningCount);
        //System.assert(collectionPeriodForm.collectionPeriodWrapperList[0].collectionPeriod.temperatureAlert, 'Temperature alert not activated');
        System.assert(collectionPeriodForm.collectionPeriodWrapperList[0].collectionPeriod.bMCCAlert, 'BMCC alert not activated');
    }

    @isTest static void testUpdateFarmSeason() {
        Integer PEAK_COWS = 100;
        Integer DAIRY_HECTARES = 200;
        User communityUser = TestClassDataUtil.createFarmsAndIndividualsAu();
        List<Account> farms = [SELECT Id FROM Account WHERE Id IN (SELECT Farm__c FROM Individual_Relationship__c WHERE Individual__c = :communityUser.ContactId AND Active__c = true)];
        List<Farm_Season__c> farmSeason = TestClassDataUtil.createFarmSeasonAU(farms);

        System.runAs(communityUser) {
            farmSeason[0].Peak_Cows__c = PEAK_COWS;
            farmSeason[0].Dairy_Hectares__c = DAIRY_HECTARES;

            AMSFarmSeasonService.updateFarmSeason(farmSeason[0]);

        }
        Farm_Season__c updadatedFarmSeason = [SELECT Id, Peak_Cows__c, Dairy_Hectares__c FROM Farm_Season__c WHERE Id = :farmSeason[0].Id];
        System.assertEquals(PEAK_COWS, updadatedFarmSeason.Peak_Cows__c, 'Farm season not updated');
        System.assertEquals(DAIRY_HECTARES, updadatedFarmSeason.Dairy_Hectares__c, 'Farm season not updated');
    }
}