/*------------------------------------------------------------
Author:			Sean Soriano
Company:		Davanti Consulting
Description:	Scheduler Class for BatchSetActiveAgreement Batch Class
History
18/12/2015		Sean Soriano	Created
------------------------------------------------------------*/
global class ScheduleBatchSetActiveAgreement implements Schedulable, Database.Batchable<SObject> {

    global static void schedule() {
        String expression = ScheduleService.getCRONExpression('ScheduleBatchSetActiveAgreement');
        String jobName = ScheduleService.getCRONJobName('ScheduleBatchSetActiveAgreement');
        if (String.isNotBlank(expression)) {
            final ScheduleBatchSetActiveAgreement job = new ScheduleBatchSetActiveAgreement();
            System.schedule(jobName, expression, job);
        }
    }

	global void execute(SchedulableContext sc) {
		ScheduleBatchSetActiveAgreement bsa = new ScheduleBatchSetActiveAgreement();
		database.executebatch(bsa);
    }

    String query = 'SELECT Id, Start_Date__c, End_Date__c, Active__c, Agreement_Status__c, RecordTypeId From Agreement__c WHERE ((Active__c = false AND Start_Date__c <= TODAY AND (End_Date__c = null OR End_Date__c >= TODAY)) OR (Active__c = true AND End_Date__c <= TODAY))';

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Date dtToday = System.today();
        List<String> agreementStatusToNotActivate = new List<String> {
            AgreementService.AGREEMENT_STATUS_CANCELLED, AgreementService.AGREEMENT_STATUS_PENDING
        };

        List<Agreement__c> lstAgreementsForUpdate = new List<Agreement__c>();

        for (sObject so : scope) {
            Agreement__c agr = (Agreement__c)so;

            // Check dates for Active Agreements
            if (dtToday >= agr.Start_Date__c && (dtToday <= agr.End_Date__c || agr.End_Date__c == null) && agr.Active__c == false) {
                // Agreement_Status__c is only being used by AU.
                // Agreements that are not signed by the Farmer(i.e. have a status of “Pending” or “Cancelled”, should not be activated.
                if (agreementStatusToNotActivate.contains(agr.Agreement_Status__c) && agr.RecordTypeId == GlobalUtility.agreementStandardRecordTypeId) {
                    continue;
                }

                agr.Active__c = true;
                lstAgreementsForUpdate.add(agr);

                // Check dates for Inactive Agreements
            } else if ((dtToday < agr.Start_Date__c || dtToday > agr.End_Date__c) && agr.Active__c == true) {
                agr.Active__c = false;
                lstAgreementsForUpdate.add(agr);
            }
        }

        if (!lstAgreementsForUpdate.isEmpty()) {
            system.debug('##Number of Agreements for Update: ' + lstAgreementsForUpdate.size());

            Database.SaveResult[] srList = Database.update(lstAgreementsForUpdate, false);

            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('##Successfully updated Agreement. Agreement ID: ' + sr.getId());
                } else {
                    // Operation failed, so get all errors
                    for (Database.Error err : sr.getErrors()) {
                        System.debug('##The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('##Agreement fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }

    global void finish(Database.BatchableContext BC) { }
}