/* 
* Class Name   - Prizm_VoidQueueTransactionController
*
* Description  - Action Class for Prizm_VoidQueuedTransaction component
*
* Developer(s) - Financial Spectra
*/


public class Prizm_VoidQueueTransactionController {
    
    @AuraEnabled
    public static void voidQueuedTransaction(Id pIndexRateId){
        
           List<fsServ__Transaction_Processing_Queue__c> voidTransactionsList = new List<fsServ__Transaction_Processing_Queue__c>();
           List<Id>  lendingContractsUpdationList = new List<Id>();
        
           Id newQueuedTransactionsRecordId = Schema.SObjectType.fsServ__Transaction_Processing_Queue__c.getRecordTypeInfosByName().get('New Transaction').getRecordTypeId();
        
           fsCore__Transaction_Setup__c rateChangeTransaction = 
             [Select Id
             , Name
             , fsCore__Transaction_Code__c
             , fsCore__Transaction_Category__c
             from fsCore__Transaction_Setup__c 
             where  fsCore__Transaction_Code__c = 'RATE_CHANGED'];
         
      
           List<fsServ__Transaction_Processing_Queue__c>  queueTransaction =
               [Select  Id
                , fsServ__Transaction_Name_Formula__c
                , fsServ__Transaction_Category__c 
                , fsServ__Processing_Status__c
                , fsServ__Lending_Contract_Number__c 
                from fsServ__Transaction_Processing_Queue__c
                where RecordTypeId =: newQueuedTransactionsRecordId and fsServ__Transaction_Name__c =: rateChangeTransaction.id and fsServ__Related_Record_ID__c =: pIndexRateId];
        
        
        List<fsServ__Lending_Contract__c> contractRateIndexClearingList= new List<fsServ__Lending_Contract__c>();
           if(queueTransaction.size()>0)
           {
               for(fsServ__Transaction_Processing_Queue__c queuedTransaction : queueTransaction)
               {
                  queuedTransaction.fsServ__Processing_Status__c = 'Void';
                  voidTransactionsList.add(queuedTransaction);
                  lendingContractsUpdationList.add(queuedTransaction.fsServ__Lending_Contract_Number__c);
               }
           }
        
        System.debug(loggingLevel.ERROR, 'contractRateIndexClearingList : '+ contractRateIndexClearingList);
        
        Id inactiveRateIndexRecTypeId = Schema.SObjectType.fsCore__Index_Rate_Setup__c.getRecordTypeInfosByName().get('Inactive').getRecordTypeId();


        fsCore__Index_Rate_Setup__c rateIndex = [Select Id, Name, Is_Already_Picked__c, Is_Approved__c, fsCore__Is_Active__c from fsCore__Index_Rate_Setup__c where Id =: pIndexRateId];
        if(rateIndex!= null)
        {
            rateIndex.Is_Already_Picked__c = false;
            rateIndex.Is_Approved__c = false;
            rateIndex.fsCore__Is_Active__c = false;
            rateIndex.RecordTypeId = inactiveRateIndexRecTypeId;
        }
        
        List<fsServ__Lending_Contract__c> contractsToBeUpdated = [Select Id, Name ,Index_Rate_Setup__c from fsServ__Lending_Contract__c where Id IN: lendingContractsUpdationList];
        
        
        if(contractsToBeUpdated.size()>0){
            for(fsServ__Lending_Contract__c con : contractsToBeUpdated)
            {
                con.Index_Rate_Setup__c = null;
                contractRateIndexClearingList.add(con);
            }
         }
        
        
        if(voidTransactionsList.size()>0 && contractRateIndexClearingList.size()>0 && rateIndex!=null)
        {
            update voidTransactionsList;
            update contractRateIndexClearingList;
            update rateIndex;
        }

    }
    
}