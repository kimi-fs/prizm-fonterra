/**
 * Controller for AMSPreferences
 *
 * Allows user to set their notification preferences
 *
 * Author: John (Trineo)
 * Date: October 2017
 */
public without sharing class AMSPreferencesController {

    public Contact individual { get; set; }

    private List<Channel_Preference__c> channelPreferences;
    public List<PartyWrapper> partyWrappers { get; set; }
    public MyPersonalPreferences myPersonalPreferences { get; set; }

    public String selectedTab {
        public get {
            if (this.selectedTab == null) {
                this.selectedTab = myPreferenceTab;
            }

            return this.selectedTab;
        }
        public set {
            if (this.selectedTab != value) {
                saved = false; //reset 'save' state on tab change
            }

            this.selectedTab = value;
        }
    }

    public static final String myPreferenceTab { public get { return 'my-preferences'; } private set; }
    public static final String myPartyPreferenceTab { public get { return 'party-preferences'; } private set; }

    public Boolean saved { get; set; }

    public AMSPreferencesController() {
        this.individual = AMSContactService.getContact();
        init();
    }

    private void init() {
        this.channelPreferences = getChannelPreferences(this.individual.Id);
        this.myPersonalPreferences = new MyPersonalPreferences(this.channelPreferences, this.individual.Id);
        initializePartyWrappers();
    }

    private void initializePartyWrappers() {
        partyWrappers = new List<PartyWrapper>();
        Map<Id, Entity_Relationship__c> erelsForIndividual = RelationshipAccessService.accessToEntityRelationships(this.individual.Id, RelationshipAccessService.Access.ACCESS_PRIMARY);
        List<Id> partyIds = new List<Id>();

        for (Entity_Relationship__c erelForIndividual : erelsForIndividual.values()) {
            partyIds.add(erelForIndividual.Party__c);
        }

        List<Account> parties = [Select Id, Name, AMS_Statement_Delivery_By_Post__c From Account Where Id In :partyIds];

        for (Account party : parties) {
            this.partyWrappers.add(new PartyWrapper(party));
        }
    }

    public class PreferenceSelectionWrapper {
        public Channel_Preference__c preference { get; set; }
    }

    public void savePreferences() {
        saved = true;

        upsert(MyPersonalPreferences.AUDailyMilkResult.preference);
        upsert(MyPersonalPreferences.AUMilkQualityAlerts.preference);
        upsert(MyPersonalPreferences.AUStatementNotifications.preference);
        upsert(MyPersonalPreferences.AUMarketingCommunications.preference);
    }

    public void savePartyPreferences() {
        saved = true;

        List<Account> partyPreferencesToSave = new List<Account>();

        for (PartyWrapper partyWrapper : partyWrappers) {
            partyPreferencesToSave.add(new Account(
                Id = partyWrapper.party.Id,
                AMS_Statement_Delivery_By_Post__c = partyWrapper.party.AMS_Statement_Delivery_By_Post__c
            ));
        }

        update (partyPreferencesToSave);
    }

    public void cancel() {
        saved = false;

        init();
    }

    public class PartyWrapper {
        public Account party { get; set; }

        public PartyWrapper(Account party) {
            this.party = party;
        }
    }

    public class MyPersonalPreferences {
        public PreferenceSelectionWrapper AUDailyMilkResult { get; set; }
        public PreferenceSelectionWrapper AUMilkQualityAlerts { get; set; }
        public PreferenceSelectionWrapper AUStatementNotifications { get; set; }
        public PreferenceSelectionWrapper AUMarketingCommunications { get; set; }

        public MyPersonalPreferences(List<Channel_Preference__c> channelPreferences, Id contactId) {
            for (Channel_Preference__c cp : channelPreferences) {
                Boolean textOn = cp.NotifyBySMS__c;
                Boolean emailOn = cp.DeliveryByEmail__c;

                if (cp.Channel_Preference_Setting__r.CME_Message_Type_Id__c.equals(AMSChannelPreferencesUtil.AU_DAILY_MILK_RESULT)) {
                    AUDailyMilkResult = new PreferenceSelectionWrapper();
                    AUDailyMilkResult.preference = cp;
                } else if (cp.Channel_Preference_Setting__r.CME_Message_Type_Id__c.equals(AMSChannelPreferencesUtil.AU_MILK_QUALITY_ALERT)) {
                    AUMilkQualityAlerts = new PreferenceSelectionWrapper();
                    AUMilkQualityAlerts.preference = cp;
                } else if (cp.Channel_Preference_Setting__r.CME_Message_Type_Id__c.equals(AMSChannelPreferencesUtil.AU_STATEMENT_NOTIFICATIONS)) {
                    AUStatementNotifications = new PreferenceSelectionWrapper();
                    AUStatementNotifications.preference = cp;
                } else if (cp.Channel_Preference_Setting__r.Name.equals(AMSChannelPreferencesUtil.AU_MARKETING_COMMUNICATIONS_NAME)) {
                    AUMarketingCommunications = new PreferenceSelectionWrapper();
                    AUMarketingCommunications.preference = cp;
                }
            }

            if (AUDailyMilkResult == null) {
                AUDailyMilkResult = new PreferenceSelectionWrapper();
                AUDailyMilkResult.preference = new Channel_Preference__c();
                AUDailyMilkResult.preference.NotifyBySMS__c = true;
                AUDailyMilkResult.preference.DeliveryByEmail__c = true;
                AUDailyMilkResult.preference.Contact_ID__c = contactId;
                AUDailyMilkResult.preference.Channel_Preference_Setting__c = AMSChannelPreferencesUtil.DAILY_MILK_RESULT_SETTING.Id;
            }
            if (AUMilkQualityAlerts == null) {
                AUMilkQualityAlerts = new PreferenceSelectionWrapper();
                AUMilkQualityAlerts.preference = new Channel_Preference__c();
                AUMilkQualityAlerts.preference.NotifyBySMS__c = true;
                AUMilkQualityAlerts.preference.DeliveryByEmail__c = true;
                AUMilkQualityAlerts.preference.Contact_ID__c = contactId;
                AUMilkQualityAlerts.preference.Channel_Preference_Setting__c = AMSChannelPreferencesUtil.MILK_QUALITY_ALERT_SETTING.Id;
            }
            if (AUStatementNotifications == null) {
                AUStatementNotifications = new PreferenceSelectionWrapper();
                AUStatementNotifications.preference = new Channel_Preference__c();
                AUStatementNotifications.preference.NotifyBySMS__c = true;
                AUStatementNotifications.preference.DeliveryByEmail__c = true;
                AUStatementNotifications.preference.Contact_ID__c = contactId;
                AUStatementNotifications.preference.Channel_Preference_Setting__c = AMSChannelPreferencesUtil.STATEMENT_NOTIFICATION_SETTING.Id;
            }
            if (AUMarketingCommunications == null) {
                AUMarketingCommunications = new PreferenceSelectionWrapper();
                AUMarketingCommunications.preference = new Channel_Preference__c();
                AUMarketingCommunications.preference.NotifyBySMS__c = true;
                AUMarketingCommunications.preference.DeliveryByEmail__c = true;
                AUMarketingCommunications.preference.Contact_ID__c = contactId;
                AUMarketingCommunications.preference.Channel_Preference_Setting__c = AMSChannelPreferencesUtil.MARKETING_COMMUNICATIONS_SETTING.Id;
            }
        }
    }

    private List<Channel_Preference__c> getChannelPreferences(Id contactId) {
        List<Channel_Preference__c> channelPreferences = [SELECT
                                                            Id,
                                                            DeliveryByEmail__c,
                                                            NotifyBySMS__c,
                                                            Channel_Preference_Setting__c,
                                                            Channel_Preference_Setting__r.Name,
                                                            Channel_Preference_Setting__r.DeliveryByEmail__c,
                                                            Channel_Preference_Setting__r.NotifyBySMS__c,
                                                            Channel_Preference_Setting__r.CME_Message_Type_Id__c
                                                            FROM
                                                                Channel_Preference__c
                                                            WHERE
                                                                Contact_ID__c = :contactId
                                                        ];
        return channelpreferences;
    }
}