/**
 * AMSNewsController.cls
 * Description: Displays a list of a AMS News knowledge articles on a page
 * @author: Amelia (Trineo)
 * @date: June 2017
 */
public with sharing class AMSNewsController extends AMSKnowledgeList {

    // Because this is used on the dashboard page (which also has pagination) we only care about which page we're on if its the news page otherwise default to the latest records
    // Addendum- also used for filtering out dairy code articles
    private Boolean isNewsDetailPage = ApexPages.currentPage().getUrl().containsIgnoreCase(Page.AMSNews.getURL());

    //used for switching between the guest and community template
    public Boolean isLoggedIn {
        get {
            return AMSUserService.checkUserLoggedIn();
        }
    }

    public static Integer ARTICLES_PER_PAGE = 10;
    public static final Integer PAGINATION_NUMBER_OF_LINKED_PAGES = 4;

    public static Map<String, String> messages { get; set; }
    static {
        messages = ExceptionService.getPageMessages('AMSNews');
    }

    // cached values that only last for one transaction
    public transient Integer currentPageNumber {get; private set;}

    public AMSNewsController() {
        PageReference currentPage = ApexPages.currentPage();
        this.currentArticleCategoryName = currentPage.getParameters().get(getArticleCategoryParameterName());

        this.currentPageNumber = PaginationController.getCurrentPageNumber();
        this.dataCategoryWrappers = getArticleCategories(NEWS_DATA_CATEGORY);
    }

    private List<Knowledge__kav> getNewsArticles() {
        Integer offset = isNewsDetailPage ? PaginationController.getOffset(ARTICLES_PER_PAGE) : 0;

        if (String.isBlank(currentArticleCategoryName)) {
            return AMSKnowledgeService.newsArticlesPaginated(offset, ARTICLES_PER_PAGE);
        } else {
            return AMSKnowledgeService.newsArticlesPaginated(offset, ARTICLES_PER_PAGE, currentArticleCategoryName);
        }
    }

    public List<NewsArticleWrapper> getWrappedArticles() {
        List<NewsArticleWrapper> wrappedArticles = new List<NewsArticleWrapper>();
        for (Knowledge__kav news : getNewsArticles()) {
            if (isNewsDetailPage || !AMSKnowledgeService.isDairyCodeArticle(news)) {
                wrappedArticles.add(new NewsArticleWrapper(news));
            }
        }
        return wrappedArticles;
    }

    public Integer getNumberOfArticles(){
        if (String.isBlank(currentArticleCategoryName)) {
            return AMSKnowledgeService.totalNewsArticles();
        } else{
            return AMSKnowledgeService.totalNewsArticles(currentArticleCategoryName);
        }
    }

    public static Integer getArticlesPerPage(){
        return ARTICLES_PER_PAGE;
    }

    public static String getArticleParameterName() {
        return AMSCommunityUtil.getArticleParameterName();
    }

    public static String getPageNumberParameterName() {
        return AMSCommunityUtil.getPageNumberParameterName();
    }

    public static String getArticleCategoryParameterName() {
        return AMSCommunityUtil.getArticleCategoryParameterName();
    }

}