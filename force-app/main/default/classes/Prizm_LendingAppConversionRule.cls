global class Prizm_LendingAppConversionRule implements fsCore.CustomRuleEvaluator {
    
    private fsCore__Rule_Setup__c mRuleToBeEvaluated;
	private Map<Id, fsCore.RuleResultObject> mSourceRecToResultMap;
	public static string ERROR_MSG = 'The Current User does not have Permission to convert Lending Application to Lending Contract.';
    public static string SUCCESS_MSG = 'The Current User have Permission to convert Lending Application to Lending Contract';

	global Prizm_LendingAppConversionRule() {
		mSourceRecToResultMap = new Map<Id, fsCore.RuleResultObject>();
	}

	global void setRule(fsCore__Rule_Setup__c pRule) {
		mRuleToBeEvaluated = pRule;
	}

	global void evaluate(List<SObject> pSourceRecordList) {

		for (fsCore__Lending_Application__c app : (List<fsCore__Lending_Application__c>) pSourceRecordList) {
			fsCore.RuleResultObject ruleResult = new fsCore.RuleResultObject();
			ruleResult.setApplicable(true);
            
            Boolean accessBool = FeatureManagement.checkPermission('Lending_Application_Conversion_Permission');
            system.debug(loggingLevel.ERROR, accessBool);

			if (accessBool) {
				ruleResult.setResult(accessBool);
				ruleResult.setResultMessage(SUCCESS_MSG);
			} else {
				ruleResult.setResult(accessBool);
				ruleResult.setResultMessage(ERROR_MSG);
			}

			mSourceRecToResultMap.put(app.Id, ruleResult);
		}
	}

	global Map<Id, fsCore.RuleResultObject> getResults() {
		return mSourceRecToResultMap;
	}

	global fsCore.RuleResultObject getResult(Id pSourceRecordId) {
		return mSourceRecToResultMap.get(pSourceRecordId);
	}

}