/**
 * Description: Test for AMSFPasswordResetController
 * @author: Amelia (Trineo)
 * @date: August 2017
 */
@isTest
private class AMSPasswordResetControllerTest {
    private static final String USER_EMAIL = 'amsUser@thisTest.com';
    private static final String PASSWORD = 'Pa$$w0rd!!';

    private static final Map<String, Community_Page_Messages__c> PAGE_MESSAGES = new Map<String, Community_Page_Messages__c>();
    static {
        String PAGE_NAME = 'AMSPasswordReset';
        PAGE_MESSAGES.put('password-not-matching', new Community_Page_Messages__c(Name = PAGE_NAME+'1', Page__c = PAGE_NAME, Reason__c = 'password-not-matching', Message__c = 'password-not-matching'));
        PAGE_MESSAGES.put('unknown-error', new Community_Page_Messages__c(Name = PAGE_NAME+'2', Page__c = PAGE_NAME, Reason__c = 'unknown-error', Message__c = 'unknown-error'));
        PAGE_MESSAGES.put('invalid-password', new Community_Page_Messages__c(Name = PAGE_NAME+'3', Page__c = PAGE_NAME, Reason__c = 'invalid-password', Message__c = 'invalid-password'));
    }

    @testSetup static void testSetup(){
        upsert PAGE_MESSAGES.values();
        TestClassDataUtil.individualDefaultAccountAU();
    }

    @isTest static void testResetPassword_ValidInputs_SetupComplete(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);
        Contact communityContact = new Contact(Id = communityUser.ContactId, Type__c = 'Personal');
        communityContact.FarmSourceONE_Setup_Complete__c = true;
        update communityContact;

        System.runAs(communityUser){
            AMSPasswordResetController controller = new AMSPasswordResetController();

            Test.startTest();
            controller.newPassword = PASSWORD;
            controller.confirmPassword = PASSWORD;
            PageReference pr = controller.resetPassword();
            Test.stopTest();

            System.assert(TestClassDataUtil.checkPageReferenceMatches(Page.AMSDashboard, pr), 'Page did not redirect');
        }
    }

    @isTest static void testResetPassword_ValidInputs_SetupNotComplete(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSPasswordResetController controller = new AMSPasswordResetController();

            Test.startTest();
            controller.newPassword = PASSWORD;
            controller.confirmPassword = PASSWORD;
            PageReference pr = controller.resetPassword();
            Test.stopTest();

            System.assert(TestClassDataUtil.checkPageReferenceMatches(Page.AMSFarmDetails, pr), 'Page did not redirect');
        }
    }

    @isTest static void testResetPassword_MismatchingPassword(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSPasswordResetController controller = new AMSPasswordResetController();

            Test.startTest();
            controller.newPassword = PASSWORD;
            controller.confirmPassword = 'I do not match';
            PageReference pr = controller.resetPassword();
            Test.stopTest();

            System.assertEquals(PAGE_MESSAGES.get('password-not-matching').Message__c, controller.message, 'Wrong error message');
        }
    }

    @isTest static void testResetPassword_MissingPassword(){
        User communityUser = TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        System.runAs(communityUser){
            AMSPasswordResetController controller = new AMSPasswordResetController();

            Test.startTest();
            PageReference pr = controller.resetPassword();
            Test.stopTest();

            System.assert(String.isNotBlank(controller.message), 'No error message');
        }
    }
}