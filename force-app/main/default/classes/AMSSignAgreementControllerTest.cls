/**
 * Testing Signing of agreements
 *
 * There are three parts, the initialisation, the polling and the submission of the agreement
 *
 * @author Ed (Trineo)
 * @date   May 2020
 */
@isTest
private class AMSSignAgreementControllerTest {
    private static String USER_EMAIL = 'amsUser@thisTest.com';

    @testSetup
    private static void testSetup() {
        TestClassDataUtil.individualDefaultAccountAU();
        TestClassDataUtil.createAMSCommunityUserAndIndividual(USER_EMAIL);

        // One Party
        Account party = TestClassDataUtil.createSingleAccountPartyAU();

        // 3 Farms
        List<Account> farms = TestClassDataUtil.createAUFarms(3, false);
        for (Account farm : farms) {
            farm.Name = 'AgreementTest' + farm.Name;
        }
        insert farms;

        // Connect the party with the 3 farms by ERs
        List<Entity_Relationship__c> entityRelationships = TestClassDataUtil.createPartyFarmEntityRelationships(new Map<Account, List<Account>>{party => farms}, true);

        // Connect the Farmer with the Party - which will create derived IRs between the individual and the farms.
        List<Individual_Relationship__c> individualRelationships = TestClassDataUtil.createIndividualPartyRelationships(getCommunityUser().ContactId, new List<Account>{party}, true);

        AMS_Agreement_Settings__c settings = new AMS_Agreement_Settings__c(
            Show_Agreement_Section__c = true,
            Conga_Solution__c = '22222222',
            Conga_Template_Id__c = '333333333'
        );
        insert settings;
    }

    @isTest
    private static void testInit() {
        List<Account> farms = [SELECT Id, Name FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId AND Name LIKE 'AgreementTest%'];
        List<Account> parties = [SELECT Id FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountPartyRecordTypeId];

        System.assertEquals(3, farms.size(), 'Should be 3 farms: ' + farms);
        System.assertEquals(1, parties.size(), 'Should be 1 party: ' + parties);

        PageReference pageRef = Page.AMSSignAgreement;

        pageRef.getParameters().put('FarmId', farms[0].Id);
        pageRef.getParameters().put('PartyId', parties[0].Id);

        Test.setCurrentPage(pageRef);

        System.runAs(getCommunityUser()) {
            AMSSignAgreementController controller = new AMSSignAgreementController();

            System.assertEquals(0, controller.fiveSecondCounts);
            System.assertEquals(true, controller.showLoadingSpinner);
            System.assertEquals(farms[0].Id, controller.farmId);
            System.assertEquals(parties[0].Id, controller.partyId);
            System.assertNotEquals(null, controller.farm);
            System.assertNotEquals(true, controller.hasMultipleOwners);

            PageReference reference = controller.setupAgreement();

            System.assertEquals(null, reference);

            // Also test if it is already executed.
            controller.agreement.Agreement_Status__c = AgreementService.AGREEMENT_STATUS_EXECUTED;

            reference = controller.setupAgreement();

            System.assertEquals(Page.AMSPricing.getUrl(), reference.getUrl());
            System.assertEquals(AgreementService.AGREEMENT_TYPE_STANDARD, controller.agreement.Agreement_Type__c);
        }
    }

    @isTest
    private static void testPolling() {
        List<Account> farms = [SELECT Id, Name FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId AND Name LIKE 'AgreementTest%'];
        List<Account> parties = [SELECT Id FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountPartyRecordTypeId];

        System.assertEquals(3, farms.size(), 'Should be 3 farms: ' + farms);
        System.assertEquals(1, parties.size(), 'Should be 1 party: ' + parties);

        PageReference pageRef = Page.AMSSignAgreement;

        pageRef.getParameters().put('FarmId', farms[0].Id);
        pageRef.getParameters().put('PartyId', parties[0].Id);

        Test.setCurrentPage(pageRef);

        System.runAs(getCommunityUser()) {
            AMSSignAgreementController controller = new AMSSignAgreementController();

            System.assertEquals(0, controller.fiveSecondCounts);
            System.assertEquals(true, controller.showLoadingSpinner);
            System.assertEquals(farms[0].Id, controller.farmId);
            System.assertEquals(parties[0].Id, controller.partyId);
            System.assertNotEquals(null, controller.farm);
            System.assertNotEquals(true, controller.hasMultipleOwners);

            PageReference reference = controller.setupAgreement();

            System.assertEquals(null, reference);
            System.assertEquals(null, controller.agreementPDFContents);
            System.assertEquals(true, controller.showLoadingSpinner);

            controller.checkFileGenerated();

            System.assertEquals(null, controller.agreementPDFContents);
            System.assertEquals(true, controller.showLoadingSpinner);

            controller.agreement.Unsigned_Document_ID__c = UserInfo.getUserId(); // a bogus Id
            AgreementService.updateAgreement(controller.agreement);

            controller.checkFileGenerated();

            // Is now generated
            System.assertEquals('testing PDF', controller.agreementPDFContents);
            System.assertEquals(false, controller.showLoadingSpinner);
        }
    }

    @isTest
    private static void testSubmission() {
        List<Account> farms = [SELECT Id, Name FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountFarmRecordTypeId AND Name LIKE 'AgreementTest%'];
        List<Account> parties = [SELECT Id FROM Account WHERE RecordTypeId = :GlobalUtility.auAccountPartyRecordTypeId];

        System.assertEquals(3, farms.size(), 'Should be 3 farms: ' + farms);
        System.assertEquals(1, parties.size(), 'Should be 1 party: ' + parties);
        
        for (Account farm : farms) {
            farm.Agreement_Type__c = 'Standard';
        }
        update farms;
        
        // Only create one agreement, leave the other farms so that the controller can do it
        List<Agreement__c> agreements = new List<Agreement__c>();
        agreements.add(AgreementService.createPendingOnlineStandardAgreement(
            farms[0].Id,
            parties[0].Id,
            Date.today().addMonths(1),
            Date.today().addMonths(6))
        );
        insert agreements;

        PageReference pageRef = Page.AMSSignAgreement;

        pageRef.getParameters().put('FarmId', farms[0].Id);
        pageRef.getParameters().put('PartyId', parties[0].Id);

        Test.setCurrentPage(pageRef);

        System.runAs(getCommunityUser()) {
            AMSSignAgreementController controller = new AMSSignAgreementController();

            System.assertEquals(0, controller.fiveSecondCounts);
            System.assertEquals(true, controller.showLoadingSpinner);
            System.assertEquals(farms[0].Id, controller.farmId);
            System.assertEquals(parties[0].Id, controller.partyId);
            System.assertNotEquals(null, controller.farm);
            System.assertNotEquals(true, controller.hasMultipleOwners);

            PageReference reference = controller.setupAgreement();

            System.assertEquals(null, reference);

            controller.agreement.Unsigned_Document_ID__c = UserInfo.getUserId(); // a bogus Id
            AgreementService.updateAgreement(controller.agreement);

            controller.checkFileGenerated();

            // Is now generated
            System.assertEquals('testing PDF', controller.agreementPDFContents);
            System.assertEquals(false, controller.showLoadingSpinner);

            PageReference submitReference = controller.signAndSubmit();

            System.assertNotEquals(null, submitReference);

            System.assertNotEquals(null, controller.agreement.Signed_Timestamp__c);
            System.assertNotEquals(null, controller.agreement.Execution_Date__c);
            System.assertNotEquals(null, controller.agreement.User_Signed__c);
            System.assertEquals(AgreementService.AGREEMENT_STATUS_SUBMITTED, controller.agreement.Agreement_Status__c);
            System.assertEquals(AgreementService.SIGNING_METHOD_ONLINE, controller.agreement.Signing_Method__c);
        }
    }

    private static User getCommunityUser() {
        User theUser = [
                           SELECT Id, ContactId
                           FROM User
                           WHERE Email = :USER_EMAIL
                       ];

        return theUser;
    }
}