/**
 * ScheduleBatchSendEmail.cls
 * Description: Sends emails based on a list of tasks
 * @author: Dan (Trineo)
 * @date: October 2017
 */
global class ScheduleBatchSendEmail implements Database.Batchable<Task>, Database.AllowsCallouts {

    public String errorMessage;
    private List<Task> emailTasksToComplete;

    global ScheduleBatchSendEmail(List<Task> emailTasksToComplete) {
        this.emailTasksToComplete = emailTasksToComplete;
    }

    global List<Task> start(Database.BatchableContext batchableContext) {
        return [Select Id, WhoId, WhatId, Subject, Description From Task Where Id In :emailTasksToComplete];
    }

    global void execute(Database.BatchableContext batchableContext, List<Task> tasks) {
        System.assertEquals(tasks.size(), 1);

        List<Id> taskContactIds = new List<Id>();

        for (Task task : tasks) {
            taskContactIds.add(task.WhoId);
        }

        Map<Id, Contact> taskContactMap = new Map<Id, Contact>([Select Id, MobilePhone, Email From Contact Where Id In :taskContactIds]);

        // Step 0: Create a master list to hold the emails we'll send
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

        // Use Organization Wide Address
        OrgWideEmailAddress auEmail = GlobalUtility.ORG_WIDE_EMAIL_ADDRESS_FARM_SOURCE_AU;

        for (Task task : tasks) {
            Contact taskContact = taskContactMap.get(task.WhoId);

            // Step 1: Create a new Email
            Messaging.SingleEmailMessage mail =
            new Messaging.SingleEmailMessage();

            // Step 2: Set list of people who should get the email
            List<String> sendTo = new List<String>();
            sendTo.add(taskContact.Email);
            mail.setToAddresses(sendTo);

            // Step 3: Set who the email is sent from
            mail.setOrgWideEmailAddressId(auEmail.Id);

            // Step 4. Set email contents - you can use variables!
            mail.setSubject(task.Subject);
            String body = task.Description;
            mail.setPlainTextBody(body);

            // Step 5. Add your email to the master list
            mails.add(mail);

            // Update the task to completed
            task.Status = 'Completed';
        }

        try {
            // Step 6: Send all emails in the master list
            Messaging.sendEmail(mails);
        } catch (System.EmailException emailException) {
            if (!emailException.getMessage().contains('NO_MASS_MAIL_PERMISSION')) {
                throw emailException;
            }
        }

        update (tasks);
    }

    global void finish(Database.BatchableContext batchableContext) {

    }
}