/*------------------------------------------------------------
Author:         Clayde Bael
Company:        Trineo
Description:    Test class for MergeContactsController

History
09/01/2018      Clayde Bael         Created
-------------------------------------------------------------*/

@isTest
private class MergeContactsControllerTest {

    @testSetup
    private static void setup() {
        // Integration User Profile
        TestClassDataUtil.integrationUserProfile();
        // Custom Settings
        Contact_Merge_Settings__c settings = new Contact_Merge_Settings__c(
            Disabled_Fields__c = 'Individual_Dead__c',
            Field_Prior_to_FS_Details__c = 'Phone',
            Hidden_Fields__c = 'Merged_MS_Individual_IDs__c',
            Step_Headers__c = 'Search\nSelect the records to merge\nSelect the values to retain\nSelect the active Addresses\nReview the Individual Relationships'
                    + '\nSelect the Channel Preferences\nSelect the Entity Channel Preferences');
        insert settings;

        // Create default account
        Account acct = new Account(Name = 'Default Account AU');
        insert acct;
        insert new Field_Team_Tools_Settings__c(Name = 'IndividualDefaultAccountAU', Value__c = acct.Id);

        List<Contact> individuals = new List<Contact>();
        Contact ind = TestClassDataUtil.createIndividualAU('Test', 'Merge 1', false);
        individuals.add(ind);

        ind = TestClassDataUtil.createIndividualAU('Test', 'Merge 2', false);
        individuals.add(ind);
        Database.SaveResult[] results = Database.insert(individuals);

        Schema.RecordTypeInfo rt = Schema.SObjectType.Address__c.getRecordTypeInfosByName().get('Postal');
        Integer i = 1;
        List<Id> contactIds = new List<Id>();
        List<Address__c> addresses = new List<Address__c>();
        for ( Database.SaveResult result : results) {
            Address__c addr = new Address__c(
                RecordTypeId = rt.getRecordTypeId(),
                Individual__c = result.getId(),
                Street_Number__c = String.valueOf(i),
                Street_1__c = 'StreetAddress' + i,
                Active__c = true
            );
            addresses.add(addr);
            contactIds.add(result.getId());
            i++;
        }
        insert addresses;

        // Create Farm
        Account farm = TestClassDataUtil.createSingleAccountFarm(true);
        // Create Party
        Account party = TestClassDataUtil.createSingleAccountPartyAU();
        // Create Party/Farm ERel
        Entity_Relationship__c eRel = TestClassDataUtil.createPartyFarmEntityRelationship(party, farm, true);
        // Create Party IRel
        List<Individual_Relationship__c> iRels = new List<Individual_Relationship__c>();
        for (Id contactId : contactIds) {
            iRels.add(TestClassDataUtil.createIndividualPartyRelationship(contactId, party.Id, false));
        }
        insert iRels;


        // Channel Preferences
        List<Channel_Preference_Setting__c> channelPreferenceSettings = new List<Channel_Preference_Setting__c>();
        List<Channel_Preference_Setting__c> auChannelPreferenceSettings = TestClassDataUtil.createAUChannelPreferenceSettings();
        channelPreferenceSettings.addAll(auChannelPreferenceSettings);

        List<Channel_Preference__c> channelPreferences = new List<Channel_Preference__c>();
        for (Id contactId : contactIds) {
            for (Channel_Preference_Setting__c cps : channelPreferenceSettings) {
                channelPreferences.add(new Channel_Preference__c(
                    Channel_Preference_Setting__c = cps.Id,
                    Contact_ID__c = contactId,
                    NotifyBySMS__c = true
                ));
            }
        }
        insert channelPreferences;

    }

    private static testMethod void testPopulateAndMergeDefault() {

        Account defaultAccount = [SELECT Id FROM Account WHERE Name = 'Default Account AU'];
        List<Contact> contacts = [SELECT Id FROM Contact WHERE LastName IN ('Merge 1', 'Merge 2') ORDER BY LastName];
        System.assertEquals(2, contacts.size());

        Test.setMock(HttpCalloutMock.class, new UIAPIHttpMock());
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        System.currentPageReference().getParameters().put('acctId', defaultAccount.Id);

        MergeContactsController controller = new MergeContactsController();
        controller.parentAccounts[0].Merge_Filter__c = contacts[0].Id;
        controller.parentAccounts[1].Merge_Filter__c = contacts[1].Id;

        // Populate the Field details (Step 1)
        controller.executeNext();
        System.assert(ApexPages.getMessages().isEmpty());
        System.assertEquals('Test Merge 1', controller.names.get(contacts[0].Id));
        System.assertEquals('Test Merge 2', controller.names.get(contacts[1].Id));

        System.assert(controller.mergeWrappers != null);
        // System.assertEquals(22, controller.mergeWrappers.size());

        System.assertEquals('Master Record', controller.mergeWrappers[0].Label);

        MergeContactsController.ContactMergeWrapper nameField = controller.mergeWrappers[2];
        System.assertEquals('Name', nameField.fieldName);
        System.assertEquals('', nameField.fieldType);
        System.assertEquals('Name', nameField.label);
        System.assertEquals(2, nameField.values.size());
        System.assertEquals('Test  Merge 1', nameField.values[0]);
        System.assertEquals('Test  Merge 2', nameField.values[1]);
        System.assertEquals('INDIVIDUAL_1', nameField.mergeSelected);
        System.assert(nameField.isUpdateable);
        System.assert(nameField.components != null);
        System.assertEquals(4, nameField.components.size());
        System.assert(nameField.hasDiff);
        System.assert(nameField.highlight);

        // Address Selection (Step 2)
        List<Address__c> addresses = [SELECT Id FROM Address__c WHERE Individual__c IN (:contacts[0].Id, :contacts[1].Id)];
        System.assertEquals(2, addresses.size());

        PageReference nextPage =  controller.executeNext();
        System.assert(ApexPages.getMessages().isEmpty());
        System.assertEquals(1, controller.addressWrappers.size());
        System.assertEquals('Postal', controller.addressWrappers[0].addrType);
        MergeContactsController.ContactAddressWrapper addWrapper = controller.addressWrappers[0];
        System.assertEquals(2, addWrapper.addrDetails.size());
        for (Integer i = 0; i < addWrapper.addrDetails.size(); i++) {
            System.assertEquals(addresses[i].Id, addWrapper.addrDetails[i].addressId);
            System.assert(addWrapper.addrDetails[i].isSelected);
        }
        // Individual Relationships (Step 3)
        controller.executeNext();
        System.assertEquals(4, controller.individualRelationshipWrappers.size());
        // Channel Preferences (Step 4)
        controller.executeNext();
        System.assertEquals(1, controller.channelPreferencesWrappers.size());
        // Farm Channel Preferences (Step 5)
        controller.executeNext();
        System.assertEquals(3, controller.defaultFarmPreferencesWrappers.size());
        // Merge (Step 6)
        nextPage =  controller.executeNext();
        Test.stopTest();

        // Merge should have no error
        System.assert(ApexPages.getMessages().isEmpty());

        List<Contact> contactsAfter = [SELECT Id, Name, (SELECT Id FROM Addresses__r),
                                            (SELECT Id FROM Individual_Relationships__r)
                                       FROM Contact
                                       WHERE LastName IN ('Merge 1', 'Merge 2')
                                       ORDER BY LastName];
        System.assertEquals(1, contactsAfter.size());
        System.assertEquals('Test Merge 1', contactsAfter[0].Name);
        System.assertEquals(contacts[0].Id, contactsAfter[0].Id);
        System.assertEquals(2, contactsAfter[0].Addresses__r.size());
        System.assertEquals(2, contactsAfter[0].Individual_Relationships__r.size());
        System.assertEquals('/' + contacts[0].Id, nextPage.getURL());

    }

    private static testMethod void testMergeToIndividual2() {

        Account defaultAccount = [SELECT Id FROM Account WHERE Name = 'Default Account AU'];
        List<Contact> contacts = [SELECT Id FROM Contact WHERE LastName IN ('Merge 1', 'Merge 2') ORDER BY LastName];
        System.assertEquals(2, contacts.size());

        Test.setMock(HttpCalloutMock.class, new UIAPIHttpMock());
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        System.currentPageReference().getParameters().put('acctId', defaultAccount.Id);

        MergeContactsController controller = new MergeContactsController();
        controller.parentAccounts[0].Merge_Filter__c = contacts[1].Id;
        controller.parentAccounts[1].Merge_Filter__c = contacts[0].Id;

        // Populate the Field details (Step 1)
        controller.executeNext();

        // Set Master record to individual 2
        MergeContactsController.ContactMergeWrapper masterSelector = controller.mergeWrappers[0];
        masterSelector.mergeSelected = 'INDIVIDUAL_2';
        controller.mergeWrappers.set(0, masterSelector);

        // Address Selection (Step 2)
        controller.executeNext();
        // IRel Review (Step 3)
        controller.executeNext();
        // Channel Preferences (Step 4)
        controller.executeNext();
        // Farm Channel Preferences (Step 5)
        controller.executeNext();
        // Perform Contact Merge (Step 6)
        PageReference nextPage = controller.executeNext();
        Test.stopTest();

        // Merge should have no error
        System.assert(ApexPages.getMessages().isEmpty());

        List<Contact> contactsAfter = [SELECT Id, Name, (SELECT Id FROM Addresses__r),
                                            (SELECT Id FROM Individual_Relationships__r)
                                       FROM Contact
                                       WHERE LastName IN ('Merge 1', 'Merge 2')
                                       ORDER BY LastName];

        System.assertEquals(1, contactsAfter.size());
        System.assertEquals(contacts[0].Id, contactsAfter[0].Id);
        System.assertEquals(2, contactsAfter[0].Addresses__r.size());
        System.assertEquals('/' + contacts[0].Id, nextPage.getURL());

    }


    private class UIAPIHttpMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setBody(MergeContactsControllerTest.setupMockLayout());
            response.setStatusCode(200);
            response.setStatus('OK');
            return response;
        }
    }

    static String setupMockLayout() {

            List<LayoutItem> items = new List<LayoutItem>();

            // Name
            List<LayoutComponent> components = new List<LayoutComponent>
                {createLayoutComponent('Salutation', 'Field'),
                 createLayoutComponent('FirstName', 'Field'),
                 createLayoutComponent('MiddleName', 'Field'),
                 createLayoutComponent('LastName', 'Field')};
            items.add(createLayoutItem('Name', true, components));
            // Phone
            items.add(createLayoutItem('Main Phone', true, new List<LayoutComponent> {createLayoutComponent('Phone', 'Field')}));
            items.add(createLayoutItem('Mobile Phone', true, new List<LayoutComponent> {createLayoutComponent('MobilePhone', 'Field')}));
            // Birthdate
            items.add(createLayoutItem('Birthdate', true, new List<LayoutComponent> {createLayoutComponent('Birthdate', 'Field')}));
            // Type
            items.add(createLayoutItem('Type', true, new List<LayoutComponent> {createLayoutComponent('Type__c', 'Field')}));
            // Email
            items.add(createLayoutItem('Email', true, new List<LayoutComponent> {createLayoutComponent('Email', 'Field')}));
            // Farm Source One Id
            items.add(createLayoutItem('Farm Source ONE Id', false, new List<LayoutComponent> {createLayoutComponent('MS_Individual_ID__c', 'Field')}));
            // Individual_Dead__c
            items.add(createLayoutItem('Deceased', true, new List<LayoutComponent> {createLayoutComponent('Individual_Dead__c', 'Field')}));

            LayoutRow row = new LayoutRow();
            row.layoutItems = items;

            Section sect = new Section();
            sect.layoutRows = new List<LayoutRow> {row};

            Layout newLayout = new Layout();
            newLayout.sections = new List<Section> {sect};

            ResponseLayouts respLayouts = new ResponseLayouts();
            respLayouts.Contact = new Map<String, Map<String, Map<String, Layout>>> {
                '01290000000iBl5AAE' =>
                    new Map<String, Map<String, Layout>>{'Full' =>
                        new  Map<String, Layout> {'View' => newLayout}
                    }
                };

            APIResponse resp = new APIResponse();
            resp.layouts = respLayouts;

            return JSON.serialize(resp);
        }

        private static LayoutItem createLayoutItem(String label, Boolean editable,
                List<LayoutComponent> components) {

            LayoutItem layoutItem = new LayoutItem();
            layoutItem.label = label;
            layoutItem.editableForUpdate = editable;
            layoutItem.layoutComponents = components;
            return layoutItem;
        }

        private static LayoutComponent createLayoutComponent(String apiName, String componentType) {
            LayoutComponent component = new LayoutComponent();
            component.apiName = apiName;
            component.componentType = componentType;
            return component;
        }

    private class APIResponse {
        ResponseLayouts layouts;
    }
    private class ResponseLayouts {
        Map<String, Map<String, Map<String, Layout>>> Contact;
    }
    private class Layout {
        List<Section> sections;
    }
    private class Section {
        List<LayoutRow> layoutRows;
    }
    private class LayoutRow {
        List<LayoutItem> layoutItems;
    }
    private class LayoutItem {
        String label;
        Boolean editableForUpdate;
        List<LayoutComponent> layoutComponents;
    }
    private class LayoutComponent {
        String apiName, componentType;
    }


}