import { LightningElement, track} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import partiesByFarm from '@salesforce/apex/FormsController.getPartiesByFarm';
import createCase from '@salesforce/apex/FormsController.createBankingInformationCase';
import * as util from 'c/formUtils';

export default class LwcMilkCoolingEquipmentPI extends NavigationMixin(LightningElement) {

    handleFarmSearch = util.handleFarmSearch;
    handlePartySearch = util.handlePartySearch;

    @track partyOptions = [];
    @track disableBtn = false;
    FarmId = null;
    PartyId = null;

    GST_Account__c = false;
    Effective_Date__c = null;
    Payment_Instruction_Type__c = '';
    Bank__c = '';
    Branch__c = '';
    Branch_Number__c = '';
    Account_Number__c = '';

    get options() {
        return [
            { label: 'Change to existing bank information', value: 'Change to existing bank information' },
            { label: 'New payment instructions', value: 'New payment instructions' }
        ];
    }

    handlePartySearch = (event) => {
        this.partyOptions = [];
        partiesByFarm({farmId: event.detail})
            .then(results => {
                results.forEach(element => {
                    this.partyOptions = [...this.partyOptions ,{value: element.value , label: element.label}]; 
                });
            })
            .catch(error => {
                console.log(error);
            });
    }

    handlePartyChange(event) {
        this.PartyId = event.detail.value;
    }

    handleSave(event) {
        const isFormValid = [...this.template.querySelectorAll('lightning-input,lightning-radio-group,c-lookup,lightning-combobox')]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);

        if (isFormValid) {
            event.target.disabled = true;

            var jsonData = JSON.stringify(this);

            createCase({formData: jsonData, farmId: this.FarmId, partyId: this.PartyId})
                .then(result => util.showToast(this, result))
                .catch(error => util.showErrorToast(this, error));
        }
    }

    handleChange(event) {
        util.handleChange(this, event);
    }

    handleLookupChange(event) {
        util.handleLookupChange(this, event);
    }

    cancel() {
        util.cancel(this);
    }

    toJSON() {
        return {
            GST_Account__c: this.GST_Account__c,
            Effective_Date__c: this.Effective_Date__c,
            Payment_Instruction_Type__c: this.Payment_Instruction_Type__c,
            Bank__c: this.Bank__c,
            Branch__c: this.Branch__c,
            Branch_Number__c: this.Branch_Number__c,
            Account_Number__c: this.Account_Number__c
        };
    }
}