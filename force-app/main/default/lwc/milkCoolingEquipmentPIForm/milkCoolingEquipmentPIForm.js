import { LightningElement, track} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import partiesByFarm from '@salesforce/apex/FormsController.getPartiesByFarm';
import createCase from '@salesforce/apex/FormsController.createMilkCoolingPurchaseIncentiveCase';
import * as util from 'c/formUtils';

export default class MilkCoolingEquipmentPIForm extends NavigationMixin(LightningElement) {

    handleFarmSearch = util.handleFarmSearch;
    handlePartySearch = util.handlePartySearch;

    FarmId = null;
    PartyId = null;

    @track partyOptions = [];

    Action_Code__c = '';
    Cooling_Equipment_Complies_AS1187_S__c = '';
    Installed_Cooling_Equipment_Price__c = '';
    Capacity_Increased_To_50__c = false;
    Incentive_Percentage_Rate__c = '20';
    Significantly_Improves_Cooling_Capacity__c = false;
    Total_Incentive_Payment__c = '';
    Vat_Graduated_2500_Liters__c = false;
    Term_Incentive_No_Months__c = '36';
    Number_Vats_Not_Exceed_Two__c = false;
    Monthly_Payment__c = '';
    Vat_Has_75mm_Outlet__c = false;
    Commencement_Date__c = null;
    Completion_Date__c = null;

    saveButtonDisabled = false;

    get actionCodeOptions() {
        return [
            { label: 'A (Add)', value: 'A' },
            { label: 'D (Delete)', value: 'D' },
            { label: 'C (Change)', value: 'C' }
        ];
    }

    handlePartySearch = (event) => {
        this.partyOptions = [];
        partiesByFarm({farmId: event.detail})
            .then(results => {
                results.forEach(element => {
                    this.partyOptions = [...this.partyOptions ,{value: element.value , label: element.label}]; 
                });
                
            })
            .catch(error => {
                console.log(error);
            });
    }

    handlePartyChange(event) {
        this.PartyId = event.detail.value;
    }

    handleSave(event) {
        const isFormValid = [...this.template.querySelectorAll('lightning-input,lightning-radio-group,c-lookup')]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);

        if (isFormValid) {
            this.saveButtonDisabled = true;

            var jsonData = JSON.stringify(this);

            createCase({formData: jsonData, farmId: this.FarmId, partyId: this.PartyId})
                .then(result => util.showToast(this, result))
                .catch(error => { util.showErrorToast(this, error); this.saveButtonDisabled = false; });
        }
    }

    handleChange(event) {
        util.handleChange(this, event);
    }

    handleLookupChange(event) {
        util.handleLookupChange(this, event);
    }

    cancel() {
        util.cancel(this);
    }

    toJSON() {
        return {
            Action_Code__c: this.Action_Code__c,
            Cooling_Equipment_Complies_AS1187_S__c: this.Cooling_Equipment_Complies_AS1187_S__c,
            Installed_Cooling_Equipment_Price__c: this.Installed_Cooling_Equipment_Price__c,
            Capacity_Increased_To_50__c: this.Capacity_Increased_To_50__c,
            Incentive_Percentage_Rate__c: this.Incentive_Percentage_Rate__c,
            Significantly_Improves_Cooling_Capacity__c: this.Significantly_Improves_Cooling_Capacity__c,
            Total_Incentive_Payment__c: this.Total_Incentive_Payment__c,
            Vat_Graduated_2500_Liters__c: this.Vat_Graduated_2500_Liters__c,
            Term_Incentive_No_Months__c: this.Term_Incentive_No_Months__c,
            Number_Vats_Not_Exceed_Two__c: this.Number_Vats_Not_Exceed_Two__c,
            Monthly_Payment__c: this.Monthly_Payment__c,
            Vat_Has_75mm_Outlet__c: this.Vat_Has_75mm_Outlet__c,
            Commencement_Date__c: this.Commencement_Date__c,
            Completion_Date__c: this.Completion_Date__c
        };
    }
}