import { LightningElement, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import partiesByFarm from '@salesforce/apex/FormsController.getPartiesByFarm';
import createCase from '@salesforce/apex/FormsController.createABNUpdateCase';
import * as util from 'c/formUtils';

export default class ABNUpdateForm extends NavigationMixin(LightningElement) {

    handleFarmSearch = util.handleFarmSearch;

    @track partyOptions = [];
    @track disableBtn = false;
    FarmId = null;
    PartyId = null;

    Australian_Business_Number__c = '';
    GST_Registered__c = 'N';

    get options() {
        return [
            { label: 'Yes', value: 'Y' },
            { label: 'No', value: 'N' }
        ];
    }

    handlePartySearch = (event) => {
        this.partyOptions = [];
        partiesByFarm({farmId: event.detail})
            .then(results => {
                results.forEach(element => {
                    this.partyOptions = [...this.partyOptions ,{value: element.value , label: element.label}]; 
                });
                
            })
            .catch(error => {
                console.log(error);
            });
    }

    handlePartyChange(event) {
        this.PartyId = event.detail.value;
    }

    handleSave(event) {
        const isFormValid = [...this.template.querySelectorAll('lightning-input,c-lookup,lightning-combobox')]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);

        if (isFormValid) {
            this.disableBtn = true;
            var jsonData = JSON.stringify(this);

            createCase({formData: jsonData, farmId: this.FarmId, partyId: this.PartyId})
                .then(result => util.showToast(this, result))
                .catch(error => {  
                    util.showErrorToast(this, error);       
                })
                .finally(() => {
                    this.disableBtn = false;
                });
        }
    }

    handleChange(event) {
        util.handleChange(this, event);
    }

    handleLookupChange(event) {
        util.handleLookupChange(this, event);
    }

    cancel() {
        util.cancel(this);
    }

    toJSON() {
        return {
            Australian_Business_Number__c: this.Australian_Business_Number__c,
            GST_Registered__c: this.GST_Registered__c
        };
    }
}