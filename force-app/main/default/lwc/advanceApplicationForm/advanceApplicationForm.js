import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import updateAdvanceApplication from '@salesforce/apex/FormsController.updateAdvanceApplication';
import getAdvanceSettings from '@salesforce/apex/FormsController.getAdvanceSettings';
import getAdvanceApplicationDetail from '@salesforce/apex/FormsController.getAdvanceApplicationDetail';
import getPaymentSplitForFarmAndParty from '@salesforce/apex/FormsController.getPaymentSplitForFarmAndParty';
import isUserMilkSupplyOfficer from '@salesforce/apex/FormsController.isUserMilkSupplyOfficer';
import isApplicationDraft from '@salesforce/apex/FormsController.isApplicationDraft';
import * as util from 'c/formUtils';

export default class AdvanceApplicationForm extends NavigationMixin(LightningElement) {

    @api recordId;

    handleFarmSearch = util.handleFarmSearch;
    handlePartySearch = util.handlePartySearch;

    Farm = {};
    Party = {};

    @track advanceApplicationDetail = { };

    get typeOptions() {
        return [
            { label: 'New RABO Advance', value: 'New RABO Advance' },
            { label: 'Restructure Existing Advance', value: 'Restructure Existing Advance' }
        ];
    }

    get raboTypeOptions() {
        return [
            { label: 'Standard Rabo Advance (Tue & Fri Payment)', value: 'Standard Rabo Advance (Tue & Fri Payment)' },
            { label: 'Reschedule of existing advance (RABO Exception Process)', value: 'Reschedule of existing advance (RABO Exception Process)' }
        ];
    }

    get tradingArrangementOptions() {
        return [
            { label: 'Individual or Partnership', value: 'Individual or Partnership' },
            { label: 'Company', value: 'Company' },
            { label: 'Trust', value: 'Trust' },
            { label: 'Other', value: 'Other' }
        ];
    }

    get reasonForInterestFreeOptions() {
        return [
            { label: 'Water Purchase (North)', value: 'Water Purchase (North)' },
            { label: 'One-off (attach separate approval)', value: 'One-off (attach separate approval)' },
            { label: 'EMSA', value: 'EMSA' },
            { label: 'Regional Interest Free Offer', value: 'Regional Interest Free Offer' }
        ];
    }

    get overLimitReasonOptions() {
        return [
            { label: 'Autumn Calving', value: 'Autumn Calving' },
            { label: 'Existing EMSA', value: 'Existing EMSA' },
            { label: 'Existing Advance', value: 'Existing Advance' },
            { label: 'Other', value: 'Other' }
        ];
    }

    get raboBankApprovalOptions() {
        return [
            { label: 'Approved', value: 'Approved' },
            { label: 'Rejected', value: 'Rejected' }
        ];
    }

    advanceSettings = null;
    isDraft = null;
    raboLendingLevel = 0.0;
    fonterraExposureLevel = 0.0;
    isSupplyServicesOfficer = null;

    get isNewRaboAdvance() {
        return this.advanceApplicationDetail.Type__c === this.typeOptions[0].value;
    }

    get isRestructureExistingAdvance() {
        return this.advanceApplicationDetail.Type__c === this.typeOptions[1].value;
    }

    get isOver150000() {
        return this.advanceApplicationDetail.Proposed_new_RABO_Advance__c > 150000.0;
    }

    get isAllowedRABOFields() {
        return this.isSupplyServicesOfficer === true && this.isOver150000 === true;
    }

    isOverLimit = false;
    isOverHardLimit = false;
    isShareFarmer = false;
    isOverLimitReasonOther = false;

    renderedCallback() {
        if(this.isSupplyServicesOfficer === null) {
            isUserMilkSupplyOfficer()
            .then(result => {
                this.isSupplyServicesOfficer = result;
            });
        }
        
        if (this.isDraft == null) {
            isApplicationDraft({applicationId: this.recordId})
            .then(result => {
                this.isDraft = result;
                console.log(result);
                console.log(this.isDraft);
            });
        }

        if (this.advanceSettings === null) {
            getAdvanceSettings()
                .then(result => {
                    this.advanceSettings = result;
                    this.raboLendingLevel = result.Lending_Level__c;
                    this.fonterraExposureLevel = result.Fonterra_Exposure_Level__c;
                })
                .catch(error => console.log(error));
        }

        if (!this.advanceApplicationDetail.Id) {
            getAdvanceApplicationDetail({advanceApplicationId: this.recordId})
                .then(result => {
                    this.advanceApplicationDetail = result;
                    console.log('this.advanceApplicationDetail.Trading_Arrangement__c--->'+this.advanceApplicationDetail.Trading_Arrangement__c)
                    if (!this.advanceApplicationDetail.Additional_Litres__c) this.advanceApplicationDetail.Additional_Litres__c = 0;
                    if (!this.advanceApplicationDetail.Additional_KGs__c) this.advanceApplicationDetail.Additional_KGs__c = 0;
                    if (!this.advanceApplicationDetail.Number_of_Cows__c) this.advanceApplicationDetail.Number_of_Cows__c = 0;
                    if (!this.advanceApplicationDetail.Livestock_Value_per_unit__c) this.advanceApplicationDetail.Livestock_Value_per_unit__c = 0.00;
                    if (!this.advanceApplicationDetail.Livestock_Market_Value__c) this.advanceApplicationDetail.Livestock_Market_Value__c = 0.00;
                    if (!this.advanceApplicationDetail.Livestock_Adjustment_to_Balance_Sheet__c) this.advanceApplicationDetail.Livestock_Adjustment_to_Balance_Sheet__c = 0.00;
                    if (!this.advanceApplicationDetail.Land_Size__c) this.advanceApplicationDetail.Land_Size__c = 0;
                    if (!this.advanceApplicationDetail.Land_Value_per_unit__c) this.advanceApplicationDetail.Land_Value_per_unit__c = 0.00;
                    if (!this.advanceApplicationDetail.Land_Market_Value__c) this.advanceApplicationDetail.Land_Market_Value__c = 0.00;
                    if (!this.advanceApplicationDetail.Land_Adjustment_to_Balance_Sheet__c) this.advanceApplicationDetail.Land_Adjustment_to_Balance_Sheet__c = 0.00;
                    if (!this.advanceApplicationDetail.Adjustment_to_Equity_position__c) this.advanceApplicationDetail.Adjustment_to_Equity_position__c = 0.00;
                    if (!this.advanceApplicationDetail.Adjusted_equity_position__c) this.advanceApplicationDetail.Adjusted_equity_position__c = 0.00;

                    this.Farm = result.Case__r.Account;
                    this.Party = result.Case__r.Party__r;
                })
                .then(result => {
                    getPaymentSplitForFarmAndParty({farmId: this.Farm.Id, partyId: this.Party.Id})
                        .then(result => {
                            if (result !== null) {
                                this.advanceApplicationDetail.Last_12_Months_Litres__c = result.Agreement__r.Farm__r.Total_Volume_12_Months__c;
                                this.advanceApplicationDetail.Last_12_Months_Solids_KGs__c = result.Agreement__r.Farm__r.Total_Solids_12_Months__c;
                                this.advanceApplicationDetail.Percentage_of_Milk__c = result.Percentage__c / 100.0;
                                this.isShareFarmer = result.Payee_Type__c == 'share farmer';
                            }

                            this.recalculate();
                        })
                        .catch(error => console.log(error));
                })
                .catch(error => console.log(error));
        }
    }

    handleSave(event) {
        let isFormValid = [...this.template.querySelectorAll('lightning-input,lightning-radio-group,c-lookup')]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);

        if (this.advanceApplicationDetail.Current_Account__c > 0) {
            util.showErrorToastMessage(this, 'As the Supplier has a current account, a new advance cannot be issued. Do not proceed.');
            isFormValid = false;
        }

        if (this.isOverHardLimit) {
            util.showErrorToastMessage(this, 'The loan amount requested is above the maximum limit.');
            isFormValid = false;
        }

        if (isFormValid) {
            event.target.disabled = true;

            var jsonData = JSON.stringify(this);

            updateAdvanceApplication({formData: jsonData})
                .then(result => { this.backToCase() })
                .catch(error => { util.showErrorToast(this, error); event.target.disabled = false; });
        }
    }

    handleChange(event) {
        util.handleChange(this.advanceApplicationDetail, event);
    }

    handleLookupChange(event) {
        util.handleLookupChange(this, event);
    }

    handleChangeAndRecalculate(event) {
        this.handleChange(event);
        this.recalculate();
    }

    recalculate() {
        let advanceApplicationDetail = this.advanceApplicationDetail;
        let newAmount = this.advanceApplicationDetail.Type__c === this.typeOptions[1].value ? 0.0 : this.advanceApplicationDetail.Proposed_new_RABO_Advance__c;
        let indicativeLevel = (advanceApplicationDetail.Last_12_Months_Litres__c + advanceApplicationDetail.Additional_Litres__c) * this.raboLendingLevel * advanceApplicationDetail.Percentage_of_Milk__c;
        const maximumIndicativeLevel = this.isShareFarmer ? 35000.0 : 100000.0;

        advanceApplicationDetail.Indicative_RABO_Lending_Level__c = parseFloat((indicativeLevel > maximumIndicativeLevel ? maximumIndicativeLevel : indicativeLevel).toFixed(2), 10);
        advanceApplicationDetail.Indicative_Fonterra_Exposure_Level__c = parseFloat(((advanceApplicationDetail.Last_12_Months_Solids_KGs__c + advanceApplicationDetail.Additional_KGs__c) * this.fonterraExposureLevel * advanceApplicationDetail.Percentage_of_Milk__c).toFixed(2), 10);
        advanceApplicationDetail.Total_Loan_Exposure__c = parseFloat((advanceApplicationDetail.Current_RABO_Advances__c + newAmount).toFixed(2), 10);
        advanceApplicationDetail.Total_Loan_Rate_Cents_Per_Litre__c = parseFloat((advanceApplicationDetail.Total_Loan_Exposure__c / ((advanceApplicationDetail.Last_12_Months_Litres__c + advanceApplicationDetail.Additional_Litres__c) * advanceApplicationDetail.Percentage_of_Milk__c)).toFixed(2), 10);
        advanceApplicationDetail.Dollars_Per_KG_Loan_Rate__c = parseFloat((advanceApplicationDetail.Total_Loan_Exposure__c / ((advanceApplicationDetail.Last_12_Months_Solids_KGs__c + advanceApplicationDetail.Additional_KGs__c) * advanceApplicationDetail.Percentage_of_Milk__c)).toFixed(2), 10);

        this.isOverHardLimit = newAmount > 2500000.0;
        this.isOverLimit = newAmount > 100000.0 || advanceApplicationDetail.Total_Loan_Exposure__c > advanceApplicationDetail.Indicative_RABO_Lending_Level__c || advanceApplicationDetail.Dollars_Per_KG_Loan_Rate__c > 0.60;
        this.isOverLimitReasonOther = advanceApplicationDetail.Over_Limit_Reason__c == 'Other';
        this.isAboveIndicativeLendingLevel = newAmount > advanceApplicationDetail.Indicative_RABO_Lending_Level__c;
        this.isAboveTotalExposureLendingLevel = advanceApplicationDetail.Total_Loan_Exposure__c > advanceApplicationDetail.Indicative_RABO_Lending_Level__c;
    }

    handleEquityChangeAndRecalculate(event) {
        this.handleChange(event);
        this.recalculateEquity();
    }

    recalculateEquity() {
        let advanceApplicationDetail = this.advanceApplicationDetail;

        advanceApplicationDetail.Livestock_Adjustment_to_Balance_Sheet__c = (advanceApplicationDetail.Number_of_Cows__c * advanceApplicationDetail.Livestock_Market_Value__c) - (advanceApplicationDetail.Number_of_Cows__c * advanceApplicationDetail.Livestock_Value_per_unit__c)
        advanceApplicationDetail.Land_Adjustment_to_Balance_Sheet__c = (advanceApplicationDetail.Land_Size__c * advanceApplicationDetail.Land_Market_Value__c) - (advanceApplicationDetail.Land_Size__c * advanceApplicationDetail.Land_Value_per_unit__c)
        advanceApplicationDetail.Adjustment_to_Equity_position__c = advanceApplicationDetail.Livestock_Adjustment_to_Balance_Sheet__c + advanceApplicationDetail.Land_Adjustment_to_Balance_Sheet__c;
        advanceApplicationDetail.Adjusted_equity_position__c = advanceApplicationDetail.Equity_Position_Equity__c + advanceApplicationDetail.Adjustment_to_Equity_position__c;
    }

    backToCase() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.advanceApplicationDetail.Case__c,
                actionName: 'view',
            }
        });
    }

    toJSON() {
        return {
            Id: this.advanceApplicationDetail.Id,
            Type__c: this.advanceApplicationDetail.Type__c,
            RABO_Advance_Type__c: this.advanceApplicationDetail.RABO_Advance_Type__c,
            Reason_for_Interest_Free__c: this.advanceApplicationDetail.Reason_for_Interest_Free__c,
            Loan_Number_being_Restructured__c: this.advanceApplicationDetail.Loan_Number_being_Restructured__c,
            Current_Account__c: this.advanceApplicationDetail.Current_Account__c,
            Current_RABO_Advances__c: this.advanceApplicationDetail.Current_RABO_Advances__c,
            Percentage_of_Milk__c: this.advanceApplicationDetail.Percentage_of_Milk__c,
            Last_12_Months_Litres__c: this.advanceApplicationDetail.Last_12_Months_Litres__c,
            Last_12_Months_Solids_KGs__c: this.advanceApplicationDetail.Last_12_Months_Solids_KGs__c,
            Additional_Litres__c: this.advanceApplicationDetail.Additional_Litres__c,
            Additional_KGs__c: this.advanceApplicationDetail.Additional_KGs__c,
            Proposed_new_RABO_Advance__c: this.advanceApplicationDetail.Proposed_new_RABO_Advance__c,
            Proposed_RABO_Restructured_Advance_Amt__c: this.advanceApplicationDetail.Proposed_RABO_Restructured_Advance_Amt__c,
            Over_Limit_Reason__c: this.advanceApplicationDetail.Over_Limit_Reason__c,
            Over_Limit_Reason_Description__c: this.advanceApplicationDetail.Over_Limit_Reason_Description__c,
            AM_Risk_Evaluation__c: this.advanceApplicationDetail.AM_Risk_Evaluation__c,
            Cost_of_Production_kg_MS__c: this.advanceApplicationDetail.Cost_of_Production_kg_MS__c,
            Equity_Position_perc_Equity__c: this.advanceApplicationDetail.Equity_Position_perc_Equity__c,
            Equity_Position_Equity__c: this.advanceApplicationDetail.Equity_Position_Equity__c,
            Number_of_Cows__c: this.advanceApplicationDetail.Number_of_Cows__c,
            Indicative_RABO_Lending_Level__c: this.advanceApplicationDetail.Indicative_RABO_Lending_Level__c,
            Indicative_Fonterra_Exposure_Level__c: this.advanceApplicationDetail.Indicative_Fonterra_Exposure_Level__c,
            Total_Loan_Exposure__c: this.advanceApplicationDetail.Total_Loan_Exposure__c,
            Total_Loan_Rate_Cents_Per_Litre__c: this.advanceApplicationDetail.Total_Loan_Rate_Cents_Per_Litre__c,
            Dollars_Per_KG_Loan_Rate__c: this.advanceApplicationDetail.Dollars_Per_KG_Loan_Rate__c,
            Advance_Application_has_been_verified__c: this.advanceApplicationDetail.Advance_Application_has_been_verified__c,
            Other_Attachments_have_been_verified__c: this.advanceApplicationDetail.Other_Attachments_have_been_verified__c,
            All_pages_have_been_initialled__c: this.advanceApplicationDetail.All_pages_have_been_initialled__c,
            Attachment_pages_have_been_initialled__c: this.advanceApplicationDetail.Attachment_pages_have_been_initialled__c,
            Livestock_Value_per_unit__c: this.advanceApplicationDetail.Livestock_Value_per_unit__c,
            Livestock_Market_Value__c: this.advanceApplicationDetail.Livestock_Market_Value__c,
            Livestock_Adjustment_to_Balance_Sheet__c: this.advanceApplicationDetail.Livestock_Adjustment_to_Balance_Sheet__c,
            Land_Size__c: this.advanceApplicationDetail.Land_Size__c,
            Land_Value_per_unit__c: this.advanceApplicationDetail.Land_Value_per_unit__c,
            Land_Market_Value__c: this.advanceApplicationDetail.Land_Market_Value__c,
            Land_Adjustment_to_Balance_Sheet__c: this.advanceApplicationDetail.Land_Adjustment_to_Balance_Sheet__c,
            Adjustment_to_Equity_position__c: this.advanceApplicationDetail.Adjustment_to_Equity_position__c,
            Adjusted_equity_position__c: this.advanceApplicationDetail.Adjusted_equity_position__c,
            Explanation_of_equity_adjustment__c: this.advanceApplicationDetail.Explanation_of_equity_adjustment__c,
            RABO_Bank_Approval__c: this.advanceApplicationDetail.RABO_Bank_Approval__c,
            RABO_Bank_Approval_File_Attached__c: this.advanceApplicationDetail.RABO_Bank_Approval_File_Attached__c,
            Interest_Free__c: this.advanceApplicationDetail.Interest_Free__c,
            Alternate_Payment_Arrangements__c: this.advanceApplicationDetail.Alternate_Payment_Arrangements__c,
            Alternate_Payment_Arrangements_Note__c: this.advanceApplicationDetail.Alternate_Payment_Arrangements_Note__c,
            Trading_Arrangement__c: this.advanceApplicationDetail.Trading_Arrangement__c
        }
    }
}