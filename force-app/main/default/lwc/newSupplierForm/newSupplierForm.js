import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import createCase from '@salesforce/apex/FormsController.createNewSupplierCase';
import * as util from 'c/formUtils';

export default class NewSupplierForm extends NavigationMixin(LightningElement) {

    handleIndividualSearch = util.handleIndividualSearch;
    handleUserSearch = util.handleUserSearch;

    IndividualId = null;

    Comments = '';
    CommencementDate = null;

    SupplierName = '';
    SupplierNumber = '';
    SupplierRegion = '';
    TradingName = '';

    Action_Code__c = 'A';
    Street_Number__c = '';
    Street_1__c = '';
    Street_2__c = '';
    Suburb__c = '';
    State__c = '';
    City__c = '';
    Postcode__c = '';
    Phone_Number__c = '';
    Fax__c = '';
    Mobile_Number__c = '';
    Email__c = '';
    UDV_Member__c = 'N';
    Farm_Access_Approved_by_Transport__c = 'N';
    Dairy_License_Number__c = '';
    FSP_Audit_Status_Verified__c = 'N';
    Last_Audit_Date__c = null;
    Area_Manager__c = null;

    //banking information

    GST_Account__c = false;
    Effective_Date__c = null;
    Payment_Instruction_Type__c = 'New payment instructions';
    Bank__c = '';
    Branch__c = '';
    Branch_Number__c = '';
    Account_Number__c = '';

    //abn update

    Australian_Business_Number__c = '';
    GST_Registered__c = 'N';

    saveButtonDisabled = false;

    get options() {
        return [
            { label: 'Yes', value: 'Y' },
            { label: 'No', value: 'N' }
        ];
    }

    get regionOptions() {
        return [
            { label: 'North', value: 'North' },
            { label: 'East', value: 'East' },
            { label: 'South', value: 'South' },
            { label: 'West', value: 'West' }
        ];
    }

    value = 'N'; // to bind the default value to the radio button

    get paymentInstructionOptions() {
        return [
            { label: 'New payment instructions', value: 'New payment instructions' }
        ];
    }

    get actionCodeOptions() {
        return [
            { label: 'A (Add)', value: 'A' }
        ];
    }

    get stateOptions() {
        return [
            { label: 'NSW', value: 'NSW' },
            { label: 'TAS', value: 'TAS' },
            { label: 'NT', value: 'NT' },
            { label: 'SA', value: 'SA' },
            { label: 'VIC', value: 'VIC' },
            { label: 'QLD', value: 'QLD' },
            { label: 'WA', value: 'WA' }
        ];
    }

    handleSave(event) {
        const isFormValid = [...this.template.querySelectorAll('lightning-input,lightning-combobox,lightning-radio-group,c-lookup')]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);

        if (isFormValid) {
            this.saveButtonDisabled = true;

            let supplierInformationJSON = JSON.stringify(this.getSupplierInformationJSON());
            let bankingInformationJSON = JSON.stringify(this.getBankingInformationJSON());
            let ABNUpdateJSON = JSON.stringify(this.getABNUpdateJSON());

            createCase({
                    supplierInformationFormData: supplierInformationJSON,
                    bankingInformationFormData: bankingInformationJSON,
                    abnFormData: ABNUpdateJSON,
                    individualId: this.IndividualId,
                    description: this.Comments,
                    incidentDate: this.CommencementDate,
                    supplierName: this.SupplierName,
                    tradingName: this.TradingName,
                    supplierRegion: this.SupplierRegion,
                    supplierNumber: this.SupplierNumber
                })
                .then(result => util.showToast(this, result))
                .catch(error => { util.showErrorToast(this, error); this.saveButtonDisabled = false; });
        }
    }

    handleChange(event) {
        util.handleChange(this, event);
    }

    handleLookupChange(event) {
        util.handleLookupChange(this, event);
    }

    cancel() {
        util.cancel(this);
    }

    getSupplierInformationJSON() {
        return {
            Action_Code__c: this.Action_Code__c,
            Street_Number__c: this.Street_Number__c,
            Street_1__c: this.Street_1__c,
            Street_2__c: this.Street_2__c,
            Suburb__c: this.Suburb__c,
            State__c: this.State__c,
            City__c: this.City__c,
            Postcode__c: this.Postcode__c,
            Phone_Number__c: this.Phone_Number__c,
            Fax__c: this.Fax__c,
            Mobile_Number__c: this.Mobile_Number__c,
            Email__c: this.Email__c,
            UDV_Member__c: this.UDV_Member__c,
            Farm_Access_Approved_by_Transport__c: this.Farm_Access_Approved_by_Transport__c,
            Dairy_License_Number__c: this.Dairy_License_Number__c,
            FSP_Audit_Status_Verified__c: this.FSP_Audit_Status_Verified__c,
            Last_Audit_Date__c: this.Last_Audit_Date__c,
            Area_Manager__c: this.Area_Manager__c
        };
    }

    getBankingInformationJSON() {
        return {
            GST_Account__c: this.GST_Account__c,
            Effective_Date__c: this.Effective_Date__c,
            Payment_Instruction_Type__c: this.Payment_Instruction_Type__c,
            Bank__c: this.Bank__c,
            Branch__c: this.Branch__c,
            Branch_Number__c: this.Branch_Number__c,
            Account_Number__c: this.Account_Number__c
        };
    }

    getABNUpdateJSON() {
        return {
          Australian_Business_Number__c: this.Australian_Business_Number__c,
          GST_Registered__c: this.GST_Registered__c
        };
    }
}