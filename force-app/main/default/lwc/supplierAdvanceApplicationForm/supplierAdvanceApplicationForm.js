import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import partiesByFarm from '@salesforce/apex/FormsController.getPartiesByFarm';
import createAdvanceApplication from '@salesforce/apex/FormsController.createAdvanceApplication';
import updateAdvanceApplication from '@salesforce/apex/FormsController.updateAdvanceApplication';
import getPaymentSplitForFarmAndParty from '@salesforce/apex/FormsController.getPaymentSplitForFarmAndParty';
import getAdvanceApplicationDetail from '@salesforce/apex/FormsController.getAdvanceApplicationDetail';
import * as util from 'c/formUtils';

export default class SupplierAdvanceApplicationForm extends NavigationMixin(LightningElement) {

    handleFarmSearch = util.handleFarmSearch;
    handlePartySearch = util.handlePartySearch;

    FarmId = null;
    PartyId = null;
    Farm = {};
    Party = {};

    advanceApplicationDetail = {};
    @api recordId;

    @track partyOptions = [];

    paymentSplitExistsForFarmAndParty = false;
    saveButtonDisabled = false;

    get paymentSplitInvalid() {
        return this.FarmId !== null && this.PartyId !== null && !this.paymentSplitExistsForFarmAndParty;
    }

    get tradingArrangementOptions() {
        return [
            { label: 'Individual or Partnership', value: 'Individual or Partnership' },
            { label: 'Company', value: 'Company' },
            { label: 'Trust', value: 'Trust' },
            { label: 'Other', value: 'Other' }
        ];
    }

    renderedCallback() {
        if (this.recordId && !this.advanceApplicationDetail.Id) {
            getAdvanceApplicationDetail({advanceApplicationId: this.recordId})
                .then(result => {
                    this.advanceApplicationDetail = result;
                    this.getPartiesByFarmId(result.Case__r.Account.Id);
                    this.Farm = result.Case__r.Account;
                    console.log('result.Case__r.Party__r--->',result.Case__r.Party__r);
                    this.Party = result.Case__r.Party__r;
                    this.PartyId = result.Case__r.Party__r.Id;
                });
        }
    }

    getPartiesByFarmId = (farmId) => {
        this.partyOptions = [];
        partiesByFarm({farmId: farmId})
            .then(results => {
                results.forEach(element => {
                    this.partyOptions = [...this.partyOptions ,{value: element.value , label: element.label}]; 
                });
                
            })
            .catch(error => {
                console.log(error);
            });
    }

    handlePartySearch = (event) => {
        this.partyOptions = [];
        partiesByFarm({farmId: event.detail})
            .then(results => {
                results.forEach(element => {
                    this.partyOptions = [...this.partyOptions ,{value: element.value , label: element.label}]; 
                });
                
            })
            .catch(error => {
                console.log(error);
            });
    }

    handlePartyChange(event) {
        this.PartyId = event.detail.value;
        this.handlePartyLookupChangeAndCheckSplits(event);
    }
    
    handleSave(event) {
        const isFormValid = [...this.template.querySelectorAll('lightning-input,lightning-textarea,c-lookup,lightning-combobox')]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);

        if (isFormValid && this.paymentSplitExistsForFarmAndParty) {
            this.saveButtonDisabled = true;

            var jsonData = JSON.stringify(this);

            if (this.recordId) {
                updateAdvanceApplication({formData: jsonData})
                    .then(result => { util.showToast(this, result, null, 'Your advance application has been updated'); this.close(); })
                    .catch(error => { util.showErrorToast(this, error); this.saveButtonDisabled = false; });
            } else {
                createAdvanceApplication({formData: jsonData, farmId: this.FarmId, partyId: this.PartyId})
                    .then(result => { util.showToast(this, result); this.close(); })
                    .catch(error => { util.showErrorToast(this, error); this.saveButtonDisabled = false; });

            }
        }
    }

    handleChange(event) {
        util.handleChange(this.advanceApplicationDetail, event);
    }

    handleLookupChange(event) {
        util.handleLookupChange(this, event);
    }

    handleLookupChangeAndCheckSplits(event) {
        this.handleLookupChange(event);
        if (this.PartyId !== null && this.FarmId !== null) {
            getPaymentSplitForFarmAndParty({farmId: this.FarmId, partyId: this.PartyId})
                .then(result => {
                    this.paymentSplitExistsForFarmAndParty = result !== null;
                });
        }
    }

    handlePartyLookupChangeAndCheckSplits(event) {
        if (this.PartyId !== null && this.FarmId !== null) {
            getPaymentSplitForFarmAndParty({farmId: this.FarmId, partyId: this.PartyId})
                .then(result => {
                    this.paymentSplitExistsForFarmAndParty = result !== null;
                });
        }
    }

    close() {
        if (this.recordId) {
            this.backToCase();
        } else {
            util.close(this);
        }
    }

    backToCase() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.advanceApplicationDetail.Case__c,
                actionName: 'view',
            }
        });
    }

    toJSON() {
        return {
            Id: this.advanceApplicationDetail.Id,
            Finance_Required__c: this.advanceApplicationDetail.Finance_Required__c,
            Finance_Purpose__c: this.advanceApplicationDetail.Finance_Purpose__c,
            July__c: this.advanceApplicationDetail.July__c,
            November__c: this.advanceApplicationDetail.November__c,
            March__c: this.advanceApplicationDetail.March__c,
            August__c: this.advanceApplicationDetail.August__c,
            December__c: this.advanceApplicationDetail.December__c,
            April__c: this.advanceApplicationDetail.April__c,
            September__c: this.advanceApplicationDetail.September__c,
            January__c: this.advanceApplicationDetail.January__c,
            May__c: this.advanceApplicationDetail.May__c,
            October__c: this.advanceApplicationDetail.October__c,
            February__c: this.advanceApplicationDetail.February__c,
            June__c: this.advanceApplicationDetail.June__c,
            Comments__c: this.advanceApplicationDetail.Comments__c,
            Paid_by_Direct_Credit_to_Another_Account__c: this.advanceApplicationDetail.Paid_by_Direct_Credit_to_Another_Account__c,
            Account_Name__c: this.advanceApplicationDetail.Account_Name__c,
            Bank__c: this.advanceApplicationDetail.Bank__c,
            Branch_Number__c: this.advanceApplicationDetail.Branch_Number__c,
            Branch__c: this.advanceApplicationDetail.Branch__c,
            Account_Number__c: this.advanceApplicationDetail.Account_Number__c,
            Applicant_1_Date_of_Birth__c: this.advanceApplicationDetail.Applicant_1_Date_of_Birth__c,
            Applicant_2_Date_of_Birth__c: this.advanceApplicationDetail.Applicant_2_Date_of_Birth__c,
            Applicant_1_Driver_s_License_Number__c: this.advanceApplicationDetail.Applicant_1_Driver_s_License_Number__c,
            Applicant_2_Driver_s_License_Number__c: this.advanceApplicationDetail.Applicant_2_Driver_s_License_Number__c,
            Trading_Arrangement__c: this.advanceApplicationDetail.Trading_Arrangement__c
        };
    }
}