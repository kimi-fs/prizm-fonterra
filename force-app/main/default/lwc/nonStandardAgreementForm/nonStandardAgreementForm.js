import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import upsertNonStandardAgreement from '@salesforce/apex/FormsController.upsertNonStandardAgreement';
import getNonStandardAgreement from '@salesforce/apex/FormsController.getNonStandardAgreement';
import getCurrentOpportunity from '@salesforce/apex/FormsController.getCurrentOpportunity';
import getCurrentFarm from '@salesforce/apex/FormsController.getCurrentFarm';
import * as util from 'c/formUtils';

export default class NonStandardAgreementForm extends NavigationMixin(LightningElement) {
    //in case anyone is wondering why this class is a hot mess, it's because the data model is a hot mess.

    Farm = {};
    Party = {};
    Opportunity = {};
    isEntity = false;
    isOpportunity = false;
    isGlobalContext = true;
    oppId;

    handleIndividualSearch = util.handleIndividualSearch;
    handleFarmSearch = util.handleFarmSearch;
    handlePartySearch = util.handlePartySearch;
    handleOpportunitySearch = util.handleOpportunitySearch;

    @api recordId;

    @track nonStandardAgreement = { Risk_Factor__c: 0.95 };

    AgreementTerm = 0;
    KeyContact = {};
    Individual2 = {};
    Individual3 = {};
    Individual4 = {};

    saveButtonDisabled = false;
    mimMonthlyVolumeComboDisabled = true;
    opportunityOrFarmPartySelected = false;

    get agreementTypeOptions() {
        return [
            { label: 'Standard', value: 'Standard' },
            { label: 'West Fresh', value: 'West Fresh' },
            { label: 'Vic Fresh', value: 'Vic Fresh' },
            { label: 'Tasmania', value: 'Tasmania' },
            { label: 'Organic', value: 'Organic' }
        ];
    }

    get increaseMonthlyVloumeOptions() {
        return [
            { label: 'No', value: 'No' },
            { label: 'Yes', value: 'Yes' }
        ];
    }

    get agreementTypePopulated() {
        return this.nonStandardAgreement.Agreement_Type__c != null;
    }

    get standardAgreement() {
        return this.nonStandardAgreement.Agreement_Type__c === this.agreementTypeOptions[0].value;
    }

    get westFreshAgreement() {
        return this.nonStandardAgreement.Agreement_Type__c === this.agreementTypeOptions[1].value;
    }

    get vicFreshAgreement() {
        return this.nonStandardAgreement.Agreement_Type__c === this.agreementTypeOptions[2].value;
    }

    get tasmaniaAgreement() {
        return this.nonStandardAgreement.Agreement_Type__c === this.agreementTypeOptions[3].value;
    }

    get organicAgreement() {
        return this.nonStandardAgreement.Agreement_Type__c === this.agreementTypeOptions[4].value;
    }

    get cashflowSelectable() {
        return this.standardAgreement;
    }

    get competitorPriceComparisonSelectable() {
        return this.standardAgreement;
    }

    get combinedProductionSelectable() {
        return !this.organicAgreement;
    }

    get mandatoryConfidentialityClause() {
        return this.tasmaniaAgreement || this.nonStandardAgreement.Competitor_Price_Comparison__c;
    }

    get minimumPriceTableOptions() {
        return [
            { label: '7/5', value: 'A' },
            { label: '8/4', value: 'B' }
        ];
    }

    get processorOptions() {
        return [
            { label: 'Australian Consolidated Milk (ACM)', value: 'Australian Consolidated Milk (ACM)' },
            { label: 'Australian Dairy Farmers Corporation (ADFC)', value: 'Australian Dairy Farmers Corporation (ADFC)' },
            { label: 'ALBA Cheese Manufacturing', value: 'ALBA Cheese Manufacturing' },
            { label: 'Bega Cheese', value: 'Bega Cheese' },
            { label: 'Bulla Diary Foods', value: 'Bulla Diary Foods' },
            { label: 'Burra Foods (Australia) Pty LTD', value: 'Burra Foods (Australia) Pty LTD' },
            { label: 'Mondelez International', value: 'Mondelez International' },
            { label: 'Coles Supermarkets Pty Ltd', value: 'Coles Supermarkets Pty Ltd' },
            { label: 'Dairy Farmers Milk Co-operative (DFMC)', value: 'Dairy Farmers Milk Co-operative (DFMC)' },
            { label: 'Ferraro Dairy Foods', value: 'Ferraro Dairy Foods' },
            { label: 'Freedom Foods Group Limited', value: 'Freedom Foods Group Limited' },
            { label: 'Kyvalley Dairy Group', value: 'Kyvalley Dairy Group' },
            { label: 'Lactalis Australia', value: 'Lactalis Australia' },
            { label: 'Lion Pty Ltd', value: 'Lion Pty Ltd' },
            { label: 'Mountain Milk Co-operative', value: 'Mountain Milk Co-operative' },
            { label: 'Riverina Fresh Pty Ltd', value: 'Riverina Fresh Pty Ltd' },
            { label: 'Saputo Dairy Australia', value: 'Saputo Dairy Australia' }
        ];
    }

    get maximumInterestFreePeriodOptions() {
        return [
            { label: '3 months', value: '3 months' },
            { label: '6 months', value: '6 months' },
            { label: '9 months', value: '9 months' },
            { label: '12 months', value: '12 months' }
        ];
    }

    get seasonTypeOptions() {
        return [
            { label: 'Current Season', value: 'Current Season' },
            { label: 'Upcoming season', value: 'Upcoming season' }
        ];
    }

    get existingAdvances() {
        return this.nonStandardAgreement.Existing_Advances__c === 'Yes';
    }

    set existingAdvances(value) {
        if (value) {
            this.nonStandardAgreement.Existing_Advances__c = 'Yes';
        } else {
            this.nonStandardAgreement.Existing_Advances__c = 'No';
        }
    }

    get cashWillBePaidPrior() {
        return this.nonStandardAgreement.Will_Cash_Be_Paid_Prior_to_the_Supplier__c === 'Yes';
    }

    set cashWillBePaidPrior(value) {
        if (value) {
            this.nonStandardAgreement.Will_Cash_Be_Paid_Prior_to_the_Supplier__c = 'Yes';
        } else {
            this.nonStandardAgreement.Will_Cash_Be_Paid_Prior_to_the_Supplier__c = 'No';
        }
    }

    renderedCallback() {
        
        if(!this.Opportunity.Name) {
            getCurrentOpportunity({oppId: this.recordId})
                .then(result => {
                    if(result.length === 1) {
                        this.Opportunity = result[0];
                        this.isOpportunity = true;
                        this.isGlobalContext = false;
                        this.oppId = this.recordId;
                    }
                });
        }

        if(!this.Farm.Name) {
            getCurrentFarm({farmId: this.recordId})
                .then(result => {
                    if(result.length === 1) {
                        this.Farm = result[0];
                        this.isEntity = true;
                        this.isGlobalContext = false;
                    }
                });
        }

        if (!this.nonStandardAgreement.Id) {
            getNonStandardAgreement({opportunityId: this.recordId})
                .then(result => {
                    if (result) {
                        this.nonStandardAgreement = result;
                        if (result.Key_Contact_Name__r) this.KeyContact = result.Key_Contact_Name__r;
                        if (result.Individual_Name_2__r) this.Individual2 = result.Individual_Name_2__r;
                        if (result.Individual_Name_3__r) this.Individual3 = result.Individual_Name_3__r;
                        if (result.Individual_Name_4__r) this.Individual4 = result.Individual_Name_4__r;
                        if (result.Opportunity__r) this.Opportunity = result.Opportunity__r;
                        if (result.Party__r) this.Party = result.Party__r;
                        if (result.Farm__r) this.Farm = result.Farm__r;
                    }
                });
        }
    }

    resetSpecialConditions() {
        this.nonStandardAgreement.Cashflow__c = false;
        this.nonStandardAgreement.Competitor_Price_Comparison__c = false;
        this.nonStandardAgreement.Interest_Free_Advance__c = false;
        this.nonStandardAgreement.Combined_Production__c = false;
        this.nonStandardAgreement.Additional_Special_Condition__c = false;
    }

    handleSave(event) {
        let isFormValid = [...this.template.querySelectorAll('lightning-input,lightning-combobox,lightning-radio-group,c-lookup')]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);

        if (isFormValid) {
            this.opportunityOrFarmPartySelected =
                (this.nonStandardAgreement.Opportunity__c || (this.nonStandardAgreement.Farm__c && this.nonStandardAgreement.Party__c)) &&
                !(this.nonStandardAgreement.Opportunity__c && (this.nonStandardAgreement.Farm__c || this.nonStandardAgreement.Party__c));

            isFormValid = this.opportunityOrFarmPartySelected;

            if (!this.opportunityOrFarmPartySelected) {
                util.showErrorToastMessage(this, 'Please select either an Opportunity OR a Farm AND Party');
            }
        }

        if (isFormValid) {
            this.saveButtonDisabled = true;

            var jsonData = JSON.stringify(this);

            upsertNonStandardAgreement({formData: jsonData, opportunityId: this.recordId})
                .then(result => util.showToast(this, result, 'Agreement Application'))
                .catch(error => { util.showErrorToast(this, error); this.saveButtonDisabled = false; });
        }
    }

    handleAgreementTypeChange(event) {
        this.resetSpecialConditions();
        this.handleChangeAndCheckConfidentialityClause(event);
    }

    handleChangeAndCheckConfidentialityClause(event) {
        this.handleChange(event);

        if (this.tasmaniaAgreement || this.nonStandardAgreement.Competitor_Price_Comparison__c) {
            this.nonStandardAgreement.Confidentiality__c = true;
        }
    }

    handleAgreementDateChange(event) {
        this.handleChange(event);

        if (this.nonStandardAgreement.Agreement_Start_Date__c && this.nonStandardAgreement.Agreement_End_Date__c) {
            let startDate = new Date(this.nonStandardAgreement.Agreement_Start_Date__c);
            let endDate = new Date(this.nonStandardAgreement.Agreement_End_Date__c);

            this.AgreementTerm = ((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24 * 365)).toFixed(2);
        }
        this.mimMonthlyVolumeComboDisabled = this.AgreementTerm >= 2?false:true;
    }

    _crFields = ['CR_Jul__c','CR_Aug__c', 'CR_Sep__c', 'CR_Oct__c', 'CR_Nov__c', 'CR_Dec__c', 'CR_Jan__c', 'CR_Feb__c', 'CR_Mar__c', 'CR_Apr__c', 'CR_May__c', 'CR_Jun__c'];

    handleTotalExpectedIncomeChange(event) {
        this.handleChange(event);

        this.nonStandardAgreement.Income_available_for_distribution__c = (this.nonStandardAgreement.Total_expected_income__c).toFixed(2);

        let monthlyIncome = ((this.nonStandardAgreement.Income_available_for_distribution__c * this.nonStandardAgreement.Risk_Factor__c) / 12.0).toFixed(2);
        this._crFields.forEach(prop => {
            if (prop.startsWith('CR_')) {
                if (prop !== event.target.dataset.id && prop !== 'CR_Jun__c') {
                    this.nonStandardAgreement[prop] = monthlyIncome;
                } else if (prop == 'CR_Jun__c') {
                    this.nonStandardAgreement[prop] = (this.nonStandardAgreement.Income_available_for_distribution__c -(monthlyIncome * 11)).toFixed(2);
                }
            }
        });
    }

    handleMonthlyExpectedIncomeChange(event) {
        this.handleChange(event);

        let totalMonthlyIncome = 0.0;

        this._crFields.forEach(prop => {
            if (prop.startsWith('CR_') && prop !== 'CR_Jul__c') {
                totalMonthlyIncome += parseFloat(this.nonStandardAgreement[prop], 10);
            }
        });

        this.nonStandardAgreement.CR_Jul__c = (this.nonStandardAgreement.Total_expected_income__c - totalMonthlyIncome).toFixed(2);
    }

    handleChange(event) {
        util.handleChange(this.nonStandardAgreement, event);
    }

    handleLookupChange(event) {
        util.handleLookupChange(this.nonStandardAgreement, event);
    }

    cancel() {
        util.close(this);
    }

    changeCheckboxesToMultiPicklistForSpecialConditions() {
        let specialConditions = '';

        if (this.nonStandardAgreement.Cashflow__c) {
            specialConditions += 'Cash Flow;';
        }
        if (this.nonStandardAgreement.Competitor_Price_Comparison__c) {
            specialConditions += 'Competitor Price Comparison;';
        }
        if (this.nonStandardAgreement.Interest_Free_Advance__c) {
            specialConditions += 'Interest Free Advance;';
        }
        if (this.nonStandardAgreement.Combined_Production__c) {
            specialConditions += 'Combined Production;';
        }
        if (this.nonStandardAgreement.Additional_Special_Condition__c) {
            specialConditions += 'Additional Special Condition;';
        }

        return specialConditions;
    }

    toJSON() {
        if(this.oppId == null) {
            this.oppId = this.nonStandardAgreement.Opportunity__c;
        }

        return {
            Id: this.nonStandardAgreement.Id,
            Key_Contact_Name__c: this.nonStandardAgreement.Key_Contact_Name__c,
            Individual_Name_2__c: this.nonStandardAgreement.Individual_Name_2__c,
            Individual_Name_3__c: this.nonStandardAgreement.Individual_Name_3__c,
            Individual_Name_4__c: this.nonStandardAgreement.Individual_Name_4__c,
            Agreement_Type__c: this.nonStandardAgreement.Agreement_Type__c,
            Agreement_Start_Date__c: this.nonStandardAgreement.Agreement_Start_Date__c,
            Agreement_End_Date__c: this.nonStandardAgreement.Agreement_End_Date__c,
            Milk_Collection_Start_Date__c: this.nonStandardAgreement.Milk_Collection_Start_Date__c,
            Minimum_Price_Table__c: this.nonStandardAgreement.Minimum_Price_Table__c,
            WEST_Monthly_fat_Kg__c: this.nonStandardAgreement.WEST_Monthly_fat_Kg__c,
            WEST_Monthly_Protein_Kg__c: this.nonStandardAgreement.WEST_Monthly_Protein_Kg__c,
            WEST_February_Fat_kg__c: this.nonStandardAgreement.WEST_February_Fat_kg__c,
            WEST_February_Protein_kg__c: this.nonStandardAgreement.WEST_February_Protein_kg__c,
            VIC_Monthly_fat_Kg__c: this.nonStandardAgreement.VIC_Monthly_fat_Kg__c,
            VIC_Monthly_Protein_Kg__c: this.nonStandardAgreement.VIC_Monthly_Protein_Kg__c,
            Special_Conditions__c: this.changeCheckboxesToMultiPicklistForSpecialConditions(),
            Will_Cash_Be_Paid_Prior_to_the_Supplier__c: this.nonStandardAgreement.Will_Cash_Be_Paid_Prior_to_the_Supplier__c ? 'Yes' : 'No',
            Existing_Advances__c: this.nonStandardAgreement.Existing_Advances__c ? 'Yes' : 'No',
            Applicable_Season_and_Type_in_the_Year__c: this.nonStandardAgreement.Applicable_Season_and_Type_in_the_Year__c,
            Total_expected_income__c: this.nonStandardAgreement.Total_expected_income__c,
            Risk_Factor__c: this.nonStandardAgreement.Risk_Factor__c,
            Income_available_for_distribution__c: this.nonStandardAgreement.Income_available_for_distribution__c,
            CR_Aug__c: this.nonStandardAgreement.CR_Aug__c,
            CR_Sep__c: this.nonStandardAgreement.CR_Sep__c,
            CR_Oct__c: this.nonStandardAgreement.CR_Oct__c,
            CR_Nov__c: this.nonStandardAgreement.CR_Nov__c,
            CR_Dec__c: this.nonStandardAgreement.CR_Dec__c,
            CR_Jan__c: this.nonStandardAgreement.CR_Jan__c,
            CR_Feb__c: this.nonStandardAgreement.CR_Feb__c,
            CR_Mar__c: this.nonStandardAgreement.CR_Mar__c,
            CR_Apr__c: this.nonStandardAgreement.CR_Apr__c,
            CR_May__c: this.nonStandardAgreement.CR_May__c,
            CR_Jun__c: this.nonStandardAgreement.CR_Jun__c,
            CR_Jul__c: this.nonStandardAgreement.CR_Jul__c,
            CEP_Aug__c: this.nonStandardAgreement.CEP_Aug__c,
            CEP_Sep__c: this.nonStandardAgreement.CEP_Sep__c,
            CEP_Oct__c: this.nonStandardAgreement.CEP_Oct__c,
            CEP_Nov__c: this.nonStandardAgreement.CEP_Nov__c,
            CEP_Dec__c: this.nonStandardAgreement.CEP_Dec__c,
            CEP_Jan__c: this.nonStandardAgreement.CEP_Jan__c,
            CEP_Feb__c: this.nonStandardAgreement.CEP_Feb__c,
            CEP_Mar__c: this.nonStandardAgreement.CEP_Mar__c,
            CEP_Apr__c: this.nonStandardAgreement.CEP_Apr__c,
            CEP_May__c: this.nonStandardAgreement.CEP_May__c,
            CEP_Jun__c: this.nonStandardAgreement.CEP_Jun__c,
            CEP_Jul__c: this.nonStandardAgreement.CEP_Jul__c,
            Forecast_Cost__c: this.nonStandardAgreement.Forecast_Cost__c,
            Forecast_Benefit__c: this.nonStandardAgreement.Forecast_Benefit__c,
            Supporting_Notes__c: this.nonStandardAgreement.Supporting_Notes__c,
            Advance_Amount__c: this.nonStandardAgreement.Advance_Amount__c,
            Maximum_interest_Free_Period__c: this.nonStandardAgreement.Maximum_interest_Free_Period__c,
            Supporting_Notes_Interest__c: this.nonStandardAgreement.Supporting_Notes_Interest__c,
            Processor__c: this.nonStandardAgreement.Processor__c,
            Farm_Number_to_Combine_Production_with__c: this.nonStandardAgreement.Farm_Number_to_Combine_Production_with__c,
            Require_a_Confidentiality_Clause__c: this.nonStandardAgreement.Confidentiality__c ? 'Yes' : 'No',
            Please_Add_Additional_Special_Condition__c: this.nonStandardAgreement.Please_Add_Additional_Special_Condition__c,
            Increase_in_Monthly_Minimum_Volume__c: this.nonStandardAgreement.Increase_in_Monthly_Minimum_Volume__c,
            Opportunity__c: this.oppId,
            Farm__c : this.nonStandardAgreement.Farm__c,
            Party__c : this.nonStandardAgreement.Party__c
        };
    }
}