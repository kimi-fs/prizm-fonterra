import { LightningElement,track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import partiesByFarm from '@salesforce/apex/FormsController.getPartiesByFarm';
import createCase from '@salesforce/apex/FormsController.createSupplierInformationDetailCase';
import * as util from 'c/formUtils';

export default class SupplierInformationDetailForm extends NavigationMixin(LightningElement) {

    handleFarmSearch = util.handleFarmSearch;
    handlePartySearch = util.handlePartySearch;
    handleIndividualSearch = util.handleIndividualSearch;
    handleUserSearch = util.handleUserSearch;

    FarmId = null;
    PartyId = null;
    IndividualId = null;

    @track partyOptions = [];

    Comments = '';
    CommencementDate = null;

    Action_Code__c = '';
    Street_Number__c = '';
    Street_1__c = '';
    Street_2__c = '';
    Suburb__c = '';
    State__c = '';
    City__c = '';
    Postcode__c = '';
    Phone_Number__c = '';
    Fax__c = '';
    Mobile_Number__c = '';
    Email__c = '';
    UDV_Member__c = 'N';
    Farm_Access_Approved_by_Transport__c = 'N';
    Dairy_License_Number__c = '';
    FSP_Audit_Status_Verified__c = 'N';
    Last_Audit_Date__c = null;
    Area_Manager__c = null;

    saveButtonDisabled = false;

    get options() {
        return [
            { label: 'Yes', value: 'Y' },
            { label: 'No', value: 'N' }
        ];
    }

    value = 'N'; // to bind the default value to the radio button

    get actionCodeOptions() {
        return [
            { label: 'A (Add)', value: 'A' },
            { label: 'D (Delete)', value: 'D' },
            { label: 'C (Change)', value: 'C' }
        ];
    }

    get stateOptions() {
        return [
            { label: 'NSW', value: 'NSW' },
            { label: 'TAS', value: 'TAS' },
            { label: 'NT', value: 'NT' },
            { label: 'SA', value: 'SA' },
            { label: 'VIC', value: 'VIC' },
            { label: 'QLD', value: 'QLD' },
            { label: 'WA', value: 'WA' }
        ];
    }

    handlePartySearch = (event) => {
        this.partyOptions = [];
        partiesByFarm({farmId: event.detail})
            .then(results => {
                results.forEach(element => {
                    this.partyOptions = [...this.partyOptions ,{value: element.value , label: element.label}]; 
                });
                
            })
            .catch(error => {
                console.log(error);
            });
    }

    handlePartyChange(event) {
        this.PartyId = event.detail.value;
    }

    handleSave(event) {
        const isFormValid = [...this.template.querySelectorAll('lightning-input,lightning-combobox,lightning-radio-group,c-lookup,lightning-combobox')]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);

        if (isFormValid) {
            this.saveButtonDisabled = true;

            var jsonData = JSON.stringify(this);

            createCase({formData: jsonData, farmId: this.FarmId, partyId: this.PartyId, individualId: this.IndividualId, description: this.Comments, incidentDate: this.CommencementDate})
                .then(result => util.showToast(this, result))
                .catch(error => { util.showErrorToast(this, error); this.saveButtonDisabled = false });
        }
    }

    handleChange(event) {
        util.handleChange(this, event);
    }

    handleLookupChange(event) {
        util.handleLookupChange(this, event);
    }

    cancel() {
        util.cancel(this);
    }

    toJSON() {
        return {
            Action_Code__c: this.Action_Code__c,
            Street_Number__c: this.Street_Number__c,
            Street_1__c: this.Street_1__c,
            Street_2__c: this.Street_2__c,
            Suburb__c: this.Suburb__c,
            State__c: this.State__c,
            City__c: this.City__c,
            Postcode__c: this.Postcode__c,
            Phone_Number__c: this.Phone_Number__c,
            Fax__c: this.Fax__c,
            Mobile_Number__c: this.Mobile_Number__c,
            Email__c: this.Email__c,
            UDV_Member__c: this.UDV_Member__c,
            Farm_Access_Approved_by_Transport__c: this.Farm_Access_Approved_by_Transport__c,
            Dairy_License_Number__c: this.Dairy_License_Number__c,
            FSP_Audit_Status_Verified__c: this.FSP_Audit_Status_Verified__c,
            Last_Audit_Date__c: this.Last_Audit_Date__c,
            Area_Manager__c: this.Area_Manager__c
        };
    }
}