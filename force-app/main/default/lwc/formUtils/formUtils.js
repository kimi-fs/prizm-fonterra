import farmSearch from '@salesforce/apex/FormsController.searchFarms';
import partySearch from '@salesforce/apex/FormsController.searchParties';
import individualSearch from '@salesforce/apex/FormsController.searchIndividuals';
import userSearch from '@salesforce/apex/FormsController.searchUsers';
import opportunitySearch from '@salesforce/apex/FormsController.searchOpportunities';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const handleOpportunitySearch = (event) => {
    const target = event.target;
    opportunitySearch(event.detail)
        .then(results => {
            target.setSearchResults(results);
        })
        .catch(error => {
            console.log(error);
        });
}

const handleUserSearch = (event) => {
    const target = event.target;
    userSearch(event.detail)
        .then(results => {
            target.setSearchResults(results);
        })
        .catch(error => {
            console.log(error);
        });
}

const handleIndividualSearch = (event) => {
    const target = event.target;
    individualSearch(event.detail)
        .then(results => {
            target.setSearchResults(results);
        })
        .catch(error => {
            console.log(error);
        });
}

const handleFarmSearch = (event) => {
    const target = event.target;
    farmSearch(event.detail)
        .then(results => {
            target.setSearchResults(results);
        })
        .catch(error => {
            console.log(error);
        });
}

const handlePartySearch = (event) => {
    const target = event.target;
    partySearch(event.detail)
        .then(results => {
            target.setSearchResults(results);
        })
        .catch(error => {
            console.log(error);
        });
}

const handleChange = (controller, event) => {
    var field = event.target.dataset.id;

    if (event.target.type === 'checkbox') {
        controller[field] = event.target.checked;
    } else if (event.target.type === 'radio' && (event.target.value === 'true' || event.target.value === 'false')) {
        controller[field] = event.target.value === 'true';
    } else if (event.target.type === 'number') {
        controller[field] = parseFloat(event.target.value, 10);
    } else {
        controller[field] = event.target.value;
    }
}

const handleLookupChange = (controller, event) => {
    controller[event.target.controllerPropertyName] = event.detail && event.detail.length > 0 ? event.detail[0] : null;
}

const showToast = (controller, result, label, message) => {
    const event = new ShowToastEvent({
        'title': 'Success!',
        'message': message ? message : 'A new {0} has been created.',
        'variant': 'success',
        'messageData': [
            {
                url: '/' + result,
                label: label ? label : 'Case'
            }
        ]
    });

    // Dispatches the dialog close event.
    controller.dispatchEvent(new CustomEvent('close'));
    // Dispatches the toast event.
    controller.dispatchEvent(event);
}

const showErrorToast = (controller, error) => {
    var errorMessage = '';

    if (error.body.message) {
        errorMessage = error.body.message;
    } else if (error.body.pageErrors && error.body.pageErrors.length > 0) {
        errorMessage = error.body.pageErrors[0].message;
    } else if (error.body.fieldErrors) {
        for (var fieldName in error.body.fieldErrors) {
            errorMessage = error.body.fieldErrors[fieldName][0].message;
            break;
        } 
    }

    showErrorToastMessage(controller, errorMessage);
}

const showErrorToastMessage = (controller, errorMessage) => {
    const event = new ShowToastEvent({
        "title": "Error!",
        "message": errorMessage,
        "variant": "error"
    });

    controller.dispatchEvent(event);
}

const cancel = (controller) => {
    controller.dispatchEvent(new CustomEvent('cancel'));
}

const close = (controller) => {
    controller.dispatchEvent(new CustomEvent('close'));
}

export { handleOpportunitySearch, handleUserSearch, handleIndividualSearch, handleFarmSearch, handlePartySearch, handleChange, handleLookupChange, showToast, showErrorToast, showErrorToastMessage, cancel, close }