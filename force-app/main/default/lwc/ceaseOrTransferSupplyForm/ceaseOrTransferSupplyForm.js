import { LightningElement, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import partiesByFarm from '@salesforce/apex/FormsController.getPartiesByFarm';
import createCase from '@salesforce/apex/FormsController.createCeaseOrTransferSupplyCase';
import * as util from 'c/formUtils';

export default class CeaseOrTransferSupplyForm extends NavigationMixin(LightningElement) {

    handleFarmSearch = util.handleFarmSearch;
    handleIndividualSearch = util.handleIndividualSearch;

    @track partyOptions = [];
    @track disableBtn = false;
    FarmId = null;
    PartyId = null;
    IndividualId = null;

    Ceasing_Supply__c = false;
    Moving_to__c = null;
    Transferring_Supply__c = false;
    Transferring_to__c = null;
    Locations__c = null;
    Production_last_season_litres__c = null;
    Last_Pickup_Date__c = null;
    Current_Exclusive_Milk_Supply_Agreement__c = 'No';
    Owner_Number__c = null;

    get options() {
        return [
            { label: 'Yes', value: 'Yes' },
            { label: 'No', value: 'No' }
        ];
    }

    handlePartySearch = (event) => {
        this.partyOptions = [];
        partiesByFarm({farmId: event.detail})
            .then(results => {
                results.forEach(element => {
                    this.partyOptions = [...this.partyOptions ,{value: element.value , label: element.label}]; 
                });
             })
            .catch(error => {
                console.log(error);
            });
    }

    handlePartyChange(event) {
        this.PartyId = event.detail.value;
    }

    handleSave(event) {
        const isFormValid = [...this.template.querySelectorAll('lightning-input,c-lookup,lightning-combobox')]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);
        if (isFormValid) {
            this.disableBtn = true;
            var jsonData = JSON.stringify(this);
            createCase({formData: jsonData, farmId: this.FarmId, partyId: this.PartyId, individualId: this.IndividualId, ownerNumber: this.Owner_Number__c})
                .then(result => util.showToast(this, result))
                .catch(error => { 
                    util.showErrorToast(this, error); 
                })
                .finally(() => {
                    this.disableBtn = false;
                });;
        }
    }

    handleChange(event) {
        util.handleChange(this, event);
    }

    handleLookupChange(event) {
        util.handleLookupChange(this, event);
    }

    cancel() {
        util.cancel(this);
    }

    toJSON() {
        return {
            Ceasing_Supply__c: this.Ceasing_Supply__c,
            Moving_to__c: this.Ceasing_Supply__c?this.Moving_to__c:null,
            Transferring_Supply__c: this.Transferring_Supply__c,
            Transferring_to__c: this.Transferring_Supply__c?this.Transferring_to__c:null,
            Locations__c: this.Locations__c,
            Production_last_season_litres__c: this.Production_last_season_litres__c,
            Last_Pickup_Date__c: this.Last_Pickup_Date__c,
            Current_Exclusive_Milk_Supply_Agreement__c: this.Current_Exclusive_Milk_Supply_Agreement__c
        };
    }
}