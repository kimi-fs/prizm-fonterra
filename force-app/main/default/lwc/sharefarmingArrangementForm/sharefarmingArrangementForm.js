import { LightningElement, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import createCase from '@salesforce/apex/FormsController.createSharefarmingArrangementCase';
import activeErels from '@salesforce/apex/FormsController.getActiveERELs';
import * as util from 'c/formUtils';

export default class SharefarmingAgreementForm extends NavigationMixin(LightningElement) {

    handleFarmSearch = util.handleFarmSearch;
    handlePartySearch = util.handlePartySearch;

    FarmId = null;

    Party__c = null;
    Party_2__c = null;
    Party_3__c = null;
    Party_4__c = null;

    Party_Text_2__c = '';
    Party_Text_3__c = '';
    Party_Text_4__c = '';

    Payment_Party_Split__c = null;
    Payment_Party_Split_2__c = null;
    Payment_Party_Split_3__c = null;
    Payment_Party_Split_4__c = null;

    Payment_Party_KgMS_2__c = null;
    Payment_Party_KgMS_3__c = null;
    Payment_Party_KgMS_4__c = null;

    New_Supplier_2__c = false;
    New_Supplier_3__c = false;
    New_Supplier_4__c = false;

    Effective_Date__c = null;
    @track Terminate_Agreement__c = false;
    @track Use_KgMS__c = false;
    @track Time_Shift__c = '';

    saveButtonDisabled = false;

    get options() {
        return [
            { label: 'Please pay all amounts due to us in respect of milk supplied to Fonterra Milk Australia (FA, FBA, BSC) in the following proportions with effect from the following date', value: 'false' }, //radio-group uses string values, boo.
            { label: 'Please terminate our previous Sharefarming Arrangement made in respect to milk supplied to Fonterra Milk Australia with effect from the following date', value: 'true' }
        ];
    }

    get timeOptions() {
        return [
            { label: 'AM', value: 'AM' },
            { label: 'PM', value: 'PM' }
        ];
    }

    @track ActiveErels = [];
    @track isKgMSDisabled = true;

    handleToggleSolids(event) {
        this.isKgMSDisabled = !this.isKgMSDisabled;
        this.Use_KgMS__c = !this.Use_KgMS__c;
    }

    handleSave(event) {
        const isFormValid = [...this.template.querySelectorAll('lightning-input,lightning-radio-group,c-lookup')]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);

        if (isFormValid) {
            this.saveButtonDisabled = true;

            var jsonData = JSON.stringify(this);

            createCase({formData: jsonData, farmId: this.FarmId})
                .then(result => util.showToast(this, result))
                .catch(error => { util.showErrorToast(this, error); this.saveButtonDisabled = false; });
        }
    }

    handleChange(event) {
        util.handleChange(this, event);
    }

    handleLookupChange(event) {
        util.handleLookupChange(this, event);
    }

    handleNewSupplierToggle(event) {
        let erel = this.ActiveErels.find((result) => result.Id === event.target.dataset.id);
        erel.NewSupplier = !erel.NewSupplier;
        this[erel.NewSupplierVariable] = erel.NewSupplier;
    }

    handleLookupChangeAndRetrieveErels(event) {
        this.handleLookupChange(event);
        activeErels({farmId : this.FarmId})
            .then(result => {
                const cont = this;
                let count = 1;
                this.ActiveErels = result.map(function(erel, index) {
                    count = index + 1;
                    var newErel = {
                        Id: erel.Id,
                        Party__c: erel.Party__c,
                        Party__r: {
                            Name: erel.Party__r.Name,
                            Party_ID__c: erel.Party__r.Party_ID__c
                        },
                        Farm__r: {
                            Name: erel.Farm__r.Name
                        },
                        Role__c: erel.Role__c,
                        IsOwner: erel.Role__c === 'Owner',
                        NewSupplier: false,
                        PartyVariable: 'Party' + (count > 1 ? '_' + count : '') + '__c',
                        PartyTextVariable: 'Party_Text' + (count > 1 ? '_' + count : '') + '__c',
                        PaymentPartySplitVariable: 'Payment_Party_Split' + (count > 1 ? '_' + count : '') + '__c',
                        PaymentPartyKgMSVariable: 'Payment_Party_KgMS' + (count > 1 ? '_' + count : '') + '__c',
                        NewSupplierVariable: 'New_Supplier' + (count > 1 ? '_' + count : '') + '__c'
                    };

                    cont['Party' + (count > 1 ? '_' + count : '') + '__c'] = erel.Party__c;
                    return newErel;
                });

                while (count < 4) {
                    this.ActiveErels.push({
                        Id: '' + count++,
                        Party__c: '',
                        Party__r: { },
                        Farm__r: { },
                        IsOwner: false,
                        NewSupplier: false,
                        PartyVariable: 'Party' + (count > 1 ? '_' + count : '') + '__c',
                        PartyTextVariable: 'Party_Text' + (count > 1 ? '_' + count : '') + '__c',
                        PaymentPartySplitVariable: 'Payment_Party_Split' + (count > 1 ? '_' + count : '') + '__c',
                        PaymentPartyKgMSVariable: 'Payment_Party_KgMS' + (count > 1 ? '_' + count : '') + '__c',
                        NewSupplierVariable: 'New_Supplier' + (count > 1 ? '_' + count : '') + '__c'
                    });
                }
            })
            .catch(error => console.log(error)); //fixme
    }

    cancel() {
        util.cancel(this);
    }

    toJSON() {
        let json = {
            Party__c: this.Party__c,
            Effective_Date__c: this.Effective_Date__c,
            Time_Shift__c : this.Time_Shift__c,
            Terminate_Agreement__c: this.Terminate_Agreement__c
        };

        if (!this.Terminate_Agreement__c) {
            json.Party_2__c = this.New_Supplier_2__c ? null : this.Party_2__c;
            json.Party_3__c = this.New_Supplier_3__c ? null : this.Party_3__c;
            json.Party_4__c = this.New_Supplier_4__c ? null : this.Party_4__c;
            json.Party_Text_2__c = this.New_Supplier_2__c ? this.Party_Text_2__c : this.null;
            json.Party_Text_3__c = this.New_Supplier_3__c ? this.Party_Text_3__c : this.null;
            json.Party_Text_4__c = this.New_Supplier_4__c ? this.Party_Text_4__c : this.null;
            json.New_Supplier_2__c = this.New_Supplier_2__c;
            json.New_Supplier_3__c = this.New_Supplier_3__c;
            json.New_Supplier_4__c = this.New_Supplier_4__c;
            json.Use_KgMS__c = this.Use_KgMS__c;

            if (this.Use_KgMS__c) {
                json.Payment_Party_KgMS_2__c = this.Payment_Party_KgMS_2__c;
                json.Payment_Party_KgMS_3__c = this.Payment_Party_KgMS_3__c;
                json.Payment_Party_KgMS_4__c = this.Payment_Party_KgMS_4__c;
            } else {
                json.Payment_Party_Split__c = this.Payment_Party_Split__c;
                json.Payment_Party_Split_2__c = this.Payment_Party_Split_2__c;
                json.Payment_Party_Split_3__c = this.Payment_Party_Split_3__c;
                json.Payment_Party_Split_4__c = this.Payment_Party_Split_4__c;
            }
        }

        return json;
    }
}