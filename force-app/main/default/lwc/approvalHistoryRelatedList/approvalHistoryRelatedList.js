import { LightningElement , api} from 'lwc';
import getProcessInstance from '@salesforce/apex/ApprovalHistoryController.retriveProcessInstance';

const columns = [
    { label: 'Date', fieldName: 'Date', type: "date", typeAttributes:{
        weekday: "long",
        month: "2-digit",
        day: "2-digit",
        year: "numeric",
        hour: "2-digit",
        minute: "2-digit"} },
    { label: 'Assignee', fieldName: 'Assignee', type: 'text' },
    { label: 'Role', fieldName: 'Role', type: 'text' },
    { label: 'Status', fieldName: 'Status', type: 'text' },
    { label: 'Comments', fieldName: 'Comments', type: 'text' },
];

export default class ApprovalHistoryRelatedList extends LightningElement {

    data = [];
    columns = columns;
    @api recordId;

    async connectedCallback() {
        const data = await this.retriveProcessInstance();
    }

    retriveProcessInstance()
    {
        getProcessInstance({recordId: this.recordId})
            .then(result => {
                if(result){
                    let currentData = [];
                    result.forEach((row) => {
                        row.StepsAndWorkitems.forEach((stepWorkItem) =>{
                            let rowData = {};
                            rowData.Status = stepWorkItem.StepStatus=='Started'?'Approved':stepWorkItem.StepStatus;
                            rowData.Assignee = stepWorkItem.Actor.Name;
                            rowData.Role = stepWorkItem.Actor.UserRole!=null?stepWorkItem.Actor.UserRole.Name:'';
                            rowData.Comments = stepWorkItem.Comments;
                            rowData.Date = stepWorkItem.CreatedDate;
                            currentData.push(rowData); 
                        });
                    });
                    this.data = currentData;
                } 
            })
            .catch(error => {
                console.error('error--->',error);
                this.error = error;
            });
    }
}