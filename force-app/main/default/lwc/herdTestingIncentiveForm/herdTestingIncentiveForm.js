import { LightningElement, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import partiesByFarm from '@salesforce/apex/FormsController.getPartiesByFarm';
import createCase from '@salesforce/apex/FormsController.createHerdTestingIncentiveCase';
import activeErels from '@salesforce/apex/FormsController.getActiveERELs';
import * as util from 'c/formUtils';

export default class HerdTestingIncentiveForm extends NavigationMixin(LightningElement) {

    handleFarmSearch = util.handleFarmSearch;
    handlePartySearch = util.handlePartySearch;

    FarmId = null;
    PartyId = null;

    @track ActiveErels = [];
    @track partyOptions = [];
    
    Detailed_Service_Statement__c = false;
    If_Other_please_describe__c = null;
    Other__c = false;
    Other_evidence_description__c = null;
    Other_evidence_of_cow_numbers__c = false;
    Peak_number_of_cows_milked_on_this_farm__c = null;
    Photograph_of_Equipment__c = false;
    Somatic_Cell_Count_Equipment_Details__c = null;
    Stock_Register__c = false;

    Payment_Party__c = null;
    Payment_Party_2__c = null;
    Payment_Party_3__c = null;
    Payment_Party_4__c = null;

    Payment_Party_Split__c = null;
    Payment_Party_Split_2__c = null;
    Payment_Party_Split_3__c = null;
    Payment_Party_Split_4__c = null;

    saveButtonDisabled = false;

    handlePartySearch = (event) => {
        this.partyOptions = [];
        partiesByFarm({farmId: event.detail})
            .then(results => {
                results.forEach(element => {
                    this.partyOptions = [...this.partyOptions ,{value: element.value , label: element.label}]; 
                });
                
            })
            .catch(error => {
                console.log(error);
            });
    }

    handlePartyChange(event) {
        this.PartyId = event.detail.value;
    }

    handleSave(event) {
        const isFormValid = [...this.template.querySelectorAll('lightning-input,lightning-radio-group,c-lookup,lightning-combobox')]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);

        if (isFormValid) {
            this.saveButtonDisabled = true;

            const jsonData = JSON.stringify(this);

            createCase({formData: jsonData, farmId: this.FarmId, partyId: this.PartyId})
                .then(result => util.showToast(this, result))
                .catch(error => { util.showErrorToast(this, error); this.saveButtonDisabled = false; });
        }
    }

    handleChange(event) {
        util.handleChange(this, event);
    }

    handleLookupChange(event) {
        util.handleLookupChange(this, event);
    }

    handleLookupChangeAndFindERELs(event) {
        this.resetPartyLookupsAndSplits();
        util.handleLookupChange(this, event);
        activeErels({farmId : this.FarmId})
            .then(result => {
                const cont = this;
                this.ActiveErels = result.map(function(erel, index) {
                    let count = index + 1;
                    var newErel = {
                        Id: erel.Id,
                        Party__c: erel.Party__c,
                        Party__r: {
                            Name: erel.Party__r.Name,
                            Party_ID__c: erel.Party__r.Party_ID__c
                        },
                        Farm__r: {
                            Name: erel.Farm__r.Name
                        },
                        Role__c: erel.Role__c,
                        Active__c: erel.Active__c,
                        PaymentPartySplitVariable: 'Payment_Party_Split' + (count > 1 ? '_' + count : '') + '__c'
                    };

                    cont['Payment_Party' + (count > 1 ? '_' + count : '') + '__c'] = erel.Party__c;
                    return newErel;
                });
             })
            .catch(error => console.log(error)); //fixme
    }

    resetPartyLookupsAndSplits() {
        this.Payment_Party__c = null;
        this.Payment_Party_2__c = null;
        this.Payment_Party_3__c = null;
        this.Payment_Party_4__c = null;
        this.Payment_Party_Split__c = null;
        this.Payment_Party_Split_2__c = null;
        this.Payment_Party_Split_3__c = null;
        this.Payment_Party_Split_4__c = null;
    }

    cancel() {
        util.cancel(this);
    }

    toJSON() {
        return {
            Detailed_Service_Statement__c: this.Detailed_Service_Statement__c,
            If_Other_please_describe__c: this.If_Other_please_describe__c,
            Other__c: this.Other__c,
            Other_evidence_description__c: this.Other_evidence_description__c,
            Other_evidence_of_cow_numbers__c: this.Other_evidence_of_cow_numbers__c,
            Peak_number_of_cows_milked_on_this_farm__c: this.Peak_number_of_cows_milked_on_this_farm__c,
            Photograph_of_Equipment__c: this.Photograph_of_Equipment__c,
            Somatic_Cell_Count_Equipment_Details__c: this.Somatic_Cell_Count_Equipment_Details__c,
            Stock_Register__c: this.Stock_Register__c,
            Payment_Party__c: this.Payment_Party__c,
            Payment_Party_2__c: this.Payment_Party_2__c,
            Payment_Party_3__c: this.Payment_Party_3__c,
            Payment_Party_4__c: this.Payment_Party_4__c,
            Payment_Party_Split__c: this.Payment_Party_Split__c,
            Payment_Party_Split_2__c: this.Payment_Party_Split_2__c,
            Payment_Party_Split_3__c: this.Payment_Party_Split_3__c,
            Payment_Party_Split_4__c: this.Payment_Party_Split_4__c
        };
    }
}