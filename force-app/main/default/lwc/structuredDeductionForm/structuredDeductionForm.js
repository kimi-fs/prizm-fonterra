import { LightningElement, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

import partiesByFarm from '@salesforce/apex/FormsController.getPartiesByFarm';
import createCase from '@salesforce/apex/FormsController.createStructuredDeductionCase';
import * as util from 'c/formUtils';

export default class StructuredDeductionForm extends NavigationMixin(LightningElement) {

    handleFarmSearch = util.handleFarmSearch;
    handlePartySearch = util.handlePartySearch;

    FarmId = null;
    PartyId = null;

    @track RegularDeduction = false;
    @track partyOptions = [];
    
    Address__c = null;
    Postcode__c = null;
    Deduction_Payable_To__c = null;
    Change_to_Existing_Deduction__c = false;

    Bank__c = null;
    Branch_Number__c = null;
    Branch__c = null;
    Account_Number__c = null;

    July__c = null;
    November__c = null;
    March__c = null;
    August__c = null;
    December__c = null;
    April__c = null;
    September__c = null;
    January__c = null;
    May__c = null;
    October__c = null;
    February__c = null;
    June__c = null;

    Start_Date__c = null;
    End_Date__c = null;

    Deduction_Type__c = null;
    Division__c = null;
    Deduction_Description__c = null;

    saveButtonDisabled = false;

    get changeToExistingDeductionOptions() {
        return [
            { label: 'New deduction', value: 'false' }, //radio-group uses string values, boo.
            { label: 'Change to existing deduction', value: 'true' }
        ];
    }

    get deductionTypeOptions() {
        return [
            { label: 'N', value: 'N' },
            { label: 'C', value: 'C' }
        ];
    }

    handlePartySearch = (event) => {
        this.partyOptions = [];
        partiesByFarm({farmId: event.detail})
            .then(results => {
                results.forEach(element => {
                    this.partyOptions = [...this.partyOptions ,{value: element.value , label: element.label}]; 
                });
                
            })
            .catch(error => {
                console.log(error);
            });
    }

    handlePartyChange(event) {
        this.PartyId = event.detail.value;
    }

    handleSave(event) {
        const isFormValid = [...this.template.querySelectorAll('lightning-input,lightning-textarea,c-lookup,lightning-combobox')]
            .reduce((validSoFar, inputField) => {
                return validSoFar && inputField.reportValidity();
            }, true);

        if (isFormValid) {
            this.saveButtonDisabled = true;

            var jsonData = JSON.stringify(this);

            createCase({formData: jsonData, farmId: this.FarmId, partyId: this.PartyId})
                .then(result => util.showToast(this, result))
                .catch(error => { util.showErrorToast(this, error); this.saveButtonDisabled = false; });
        }
    }

    handleChange(event) {
        util.handleChange(this, event);
    }

    handleLookupChange(event) {
        util.handleLookupChange(this, event);
    }

    cancel() {
        util.cancel(this);
    }

    toJSON() {
        return {
            Address__c: this.Address__c,
            Postcode__c: this.Postcode__c,
            Deduction_Payable_To__c: this.Deduction_Payable_To__c,
            Change_to_Existing_Deduction__c: this.Change_to_Existing_Deduction__c,
            Bank__c: this.Bank__c,
            Branch_Number__c: this.Branch_Number__c,
            Branch__c: this.Branch__c,
            Account_Number__c: this.Account_Number__c,
            July__c: this.July__c,
            November__c: this.November__c,
            March__c: this.March__c,
            August__c: this.August__c,
            December__c: this.December__c,
            April__c: this.April__c,
            September__c: this.September__c,
            January__c: this.January__c,
            May__c: this.May__c,
            October__c: this.October__c,
            February__c: this.February__c,
            June__c: this.June__c,
            Deduction_Type__c: this.Deduction_Type__c,
            Division__c: this.Division__c,
            Deduction_Description__c: this.Deduction_Description__c,
            Start_Date__c: this.Start_Date__c,
            End_Date__c: this.End_Date__c
        };
    }
}