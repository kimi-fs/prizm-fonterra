trigger MilkQualityTrigger on Milk_Quality__c (after insert, after update) {
    new MilkQualityTriggerHandler().run();
}