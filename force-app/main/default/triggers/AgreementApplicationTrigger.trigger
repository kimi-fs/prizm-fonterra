trigger AgreementApplicationTrigger on Non_Standard_Agreement__c (after update) {
    new AgreementApplicationTriggerHandler().run();
}