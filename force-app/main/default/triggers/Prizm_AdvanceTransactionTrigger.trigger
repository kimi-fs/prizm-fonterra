trigger Prizm_AdvanceTransactionTrigger on Advance_Transaction__c(
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete
) {
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
            Prizm_AdvanceTransactionTriggerWrapper.getInstance().beforeInsertWrapper(Trigger.New);
        }
        if (Trigger.isUpdate) {
            Prizm_AdvanceTransactionTriggerWrapper.getInstance()
                .beforeUpdateWrapper(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
        }
        if (Trigger.isDelete) {
            Prizm_AdvanceTransactionTriggerWrapper.getInstance().beforeDeleteWrapper(Trigger.old, Trigger.oldMap);
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            Prizm_AdvanceTransactionTriggerWrapper.getInstance().afterInsertWrapper(Trigger.new, Trigger.newMap);
        }
        if (Trigger.isUpdate) {
            Prizm_AdvanceTransactionTriggerWrapper.getInstance()
                .afterUpdateWrapper(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
        }
        if (Trigger.isDelete) {
            Prizm_AdvanceTransactionTriggerWrapper.getInstance().afterDeleteWrapper(Trigger.old, Trigger.oldMap);
        }
        if (Trigger.isUndelete) {
            Prizm_AdvanceTransactionTriggerWrapper.getInstance().afterUndeleteWrapper(Trigger.new, Trigger.newMap);
        }
    }
}