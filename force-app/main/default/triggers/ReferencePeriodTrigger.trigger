/*------------------------------------------------------------
Author:         Sean Soriano
Company:        Davanti Consulting
Description:    Trigger for Reference Period
Test Class:     BatchSetReferencePeriodStatus_Test
History
25/01/2016    Sean Soriano    Created
------------------------------------------------------------*/
Trigger ReferencePeriodTrigger on Reference_Period__c (before insert,before update, after insert, after update) {

    //BEFORE TRIGGER
    if(Trigger.isBefore && Trigger.isInsert) {
        ReferencePeriodTriggerHandler.setReferencePeriodStatus(Trigger.new);
    }

    if(Trigger.isBefore && Trigger.isUpdate) {
        ReferencePeriodTriggerHandler.setReferencePeriodStatus(Trigger.new);
    }

    //AFTER TRIGGERS
    if(Trigger.isAfter && Trigger.isInsert) {

    }

    if(Trigger.isAfter && Trigger.isUpdate) {

    }

    

}