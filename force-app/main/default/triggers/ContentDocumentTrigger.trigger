trigger ContentDocumentTrigger on ContentDocument (before update, before delete, after insert) {
    new ContentDocumentTriggerHandler().run();
}