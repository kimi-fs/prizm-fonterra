trigger IndividualEmailResult on et4ae5__IndividualEmailResult__c (before insert, after insert, before update, after update) {
    if (Trigger.isAfter && Trigger.isInsert) {
        IndividualEmailResultTriggerHandler.afterInsert(Trigger.new);
    }
}