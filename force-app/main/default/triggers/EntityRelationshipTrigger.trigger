/**
 * Description: Trigger for Entity_Relationship__c
 * @author: Salman Zafar (Davanti Consulting)
 * @date: June 2015
 * @test: EntityRelationshipTrigger_Test
 */
trigger EntityRelationshipTrigger on Entity_Relationship__c (before insert, after insert, before update, after update, after delete) {
    new EntityRelationshipTriggerHandler().run();
}