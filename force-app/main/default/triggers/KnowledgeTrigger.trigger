trigger KnowledgeTrigger on Knowledge__kav (before insert, after insert) {
    new KnowledgeTriggerHandler().run();
}