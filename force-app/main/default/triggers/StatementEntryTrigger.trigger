trigger StatementEntryTrigger on Statement_Entry__c (before insert,before update, after insert, after update, after delete) {

    //BEFORE TRIGGER
    if(Trigger.isBefore && Trigger.isInsert) {

    }

    if(Trigger.isBefore && Trigger.isUpdate) {
    }

    //AFTER TRIGGERS
    if(Trigger.isAfter && Trigger.isInsert) {

    }

    if(Trigger.isAfter && Trigger.isUpdate) {

    }

    if(Trigger.isAfter && Trigger.isDelete) {
    	StatementEntryTriggerHandler.deleteStatementsWithNoStatementEntries(trigger.oldMap);
    }

}