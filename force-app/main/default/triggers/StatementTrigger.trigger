/**
 * Description:
 * Handles notifications for sending SMS/email alerts u
 *
 * @author: John Au (Trineo)
 * @date: August 2018
 * @test: StatementTriggerTest
 */
trigger StatementTrigger on Statement__c (after insert) {
    if (Trigger.isAfter && Trigger.isInsert) {
        StatementTriggerHandler.sendStatementNotifications(Trigger.new);
    }
}