/*------------------------------------------------------------
Author:			Salman Zafar
Company:		Davanti Consulting
Description:	1. When the Agreement is Created
  a. if ShareMilker/ContractMilker1 is populated then Create a Sharemilker/Contract Milker record linking to the relevant contact
  b. if ShareMilker/ContractMilker 2 is populated then Create a Sharemilker/Contract Milker record linking to the contact
  c. if Party is populated then create Sharemilker/Contract Milker record linking to the relevant account
2. When Agreement is Updated
  a. check related shareMilker/Contract milker records are correct. Delete any incorrect records and create new as needed.
Test Class:		AgreementTrigger_Test
History
23/03/2015		Salman Zafar	Created
24/04/2015		Salman Zafar	Logic Added in createSharemilkerContractMilker method
------------------------------------------------------------*/
trigger AgreementTrigger on Agreement__c (before insert, before update, before delete, after insert, after update) {
    if (Trigger.isBefore) {
        if(Trigger.isInsert) {
            AgreementTriggerHandler.checkExistingAgreement(Trigger.new, true);
        }
        if (Trigger.isUpdate) {
            AgreementTriggerHandler.setAgreementId(Trigger.new, Trigger.oldMap);
            AgreementTriggerHandler.validateEditAgreementAccess(Trigger.newMap);
            AgreementTriggerHandler.checkExistingAgreement(Trigger.new, false);
        }
        if (Trigger.isDelete) {
            AgreementTriggerHandler.validateDeleteAgreementAccess(Trigger.oldMap);
        }
    }
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            AgreementTriggerHandler.createCaseForManualSignedAgreements(Trigger.new, Trigger.oldMap);
        }
    }
}