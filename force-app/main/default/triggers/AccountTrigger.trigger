/**
 * Description: Handler class for AccountTrigger
 * @author: Salman Zafar (Davanti Consulting)
 * @date: May 2015
 * @test: AccountTrigger_Test
 */
trigger AccountTrigger on Account(before insert, before update, after insert, after update) {
    new AccountTriggerHandler().run();
}