/*------------------------------------------------------------
Author:         Sean Soriano
Company:        Davanti Consulting
Description:

Test Class:     AddressTrigger_Test
History
18/12/2015      Sean Soriano        Created
4/7/2018        Clayde Bael         FSCRM-8947
------------------------------------------------------------*/
trigger AddressTrigger on Address__c (before insert, before update, after insert, after update, after delete) {
    new AddressTriggerHandler().run();
}