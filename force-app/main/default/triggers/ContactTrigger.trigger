/**
* Description: Handler class for ContactTrigger
* @author: Salman Zafar (Davanti Consulting)
* @date: 14 May 2015
* @test: ContactTrigger_Test
*/
trigger ContactTrigger on Contact (before insert, after insert, before update, after update, before delete, after delete) {
    new ContactTriggerHandler().run();
}