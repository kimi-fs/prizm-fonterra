/**
 * New ContentDocumentLink has been created.
 *
 * @author Ed (Trineo)
 */
trigger ContentDocumentLinkTrigger on ContentDocumentLink (before delete, before insert, after insert, after delete) {
    new ContentDocumentLinkTriggerHandler().run();
}