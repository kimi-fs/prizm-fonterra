trigger ChannelPreferenceTrigger on Channel_Preference__c (before insert, before update) {
    try {
        if (Trigger.isBefore) {
            if (trigger.isInsert){
                ChannelPreferenceTriggerHandler.setUnsubscribeKey(trigger.new);
            }       
        }
    } catch (Exception e) {
        System.debug(GlobalUtility.Stringify.jsonifyException(e));
    }
}