/**
* Description: IndividualRelationship Trigger
* @author: Salman Zafar (Davanti Consulting)
* @date: May 2015
* @test: IndividualRelationshipTrigger_Test
*/
trigger IndividualRelationshipTrigger on Individual_Relationship__c (before insert, before update, after insert, after update) {
   new IndividualRelationshipTriggerHandler().run();
}