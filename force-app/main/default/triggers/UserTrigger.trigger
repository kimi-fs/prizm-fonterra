/*
    Start : Fonterra Project
    Author : Paul Maddox
    Created Date : 2/07/2015
    Modified Date : 2/07/2015
    Purpose : Add user to chatter group when 'Add to default Chatter Groups' flag is true.

    29/06/2016      Damon Shaw      Removed if statement from isInsert that adds users to mapInactiveUserId2UserProfileName,
                                    added IsNonChatterProfile__c trigger old check to isUpdate when adding users to mapUserId2UserProfileName and mapInactiveUserId2UserProfileName.

    29/03/2017      Damon Shaw      Added UserTriggerHandler.AddDefaultECLinks() to give new users the default Link__kav records
                                    in the MilkyWay community

    29/03/2017      Damon Shaw      Added Manager lookup to Trigger.isBefore when Manager_GUID__c changes

    13/03/208       John Au         Cleanup of all Fonterra Live/Chatter/MilkyWay stuff
*/

trigger UserTrigger on User (after insert, after update, before insert, before update) {

    Map<Id, Profile> profilesById = new Map<Id, Profile>([SELECT Id, Name, UserLicense.Name FROM Profile]);

    if (Trigger.isAfter) {

        //Add user to chatter group when 'Add to default Chatter Groups' flag is true.

        Set<Id> setPulserUserIdSelected = new set<Id>();
        Set<Id> setPulserUserIdUnSelected = new set<Id>();
        Set<Id> setCongaUserIdSelected = new Set<Id>();
        Set<Id> setCongaUserIdUnSelected = new Set<Id>();

        for (User usr : Trigger.New) {
            if (Trigger.isInsert) {
                if (usr.Pulsar_User__c) setPulserUserIdSelected.add(usr.Id);

                if (usr.Conga_Composer_User__c) {
                    setCongaUserIdSelected.add(usr.Id);
                }
            }
            if (Trigger.isUpdate) {
                if (usr.Pulsar_User__c && usr.Pulsar_User__c != Trigger.oldMap.get(usr.Id).Pulsar_User__c) setPulserUserIdSelected.add(usr.Id);
                else if (!usr.Pulsar_User__c && usr.Pulsar_User__c != Trigger.oldMap.get(usr.Id).Pulsar_User__c) {
                    setPulserUserIdUnSelected.add(usr.Id);
                }

                if (usr.Conga_Composer_User__c && usr.Conga_Composer_User__c != Trigger.oldMap.get(usr.Id).Conga_Composer_User__c) {
                    setCongaUserIdSelected.add(usr.Id);
                } else if (!usr.Conga_Composer_User__c && usr.Conga_Composer_User__c != Trigger.oldMap.get(usr.Id).Conga_Composer_User__c) {
                    setCongaUserIdUnSelected.add(usr.Id);
                }
            }
        }

        List<UserPackageLicense> listUserPackageLicense2Insert  = new list<UserPackageLicense>();

        if (!setPulserUserIdSelected.isEmpty()) {
            LIST<PackageLicense> listPackageLicense = [select Id, NamespacePrefix from PackageLicense where NamespacePrefix = : GlobalUtility.strPulsarNameSpace];

            if (!listPackageLicense.isEmpty()) {

                //PSDS - 25/01/2015 - Prevent Duplicate Errors when creating pulsar users
                for (User us : [SELECT Id FROM User
                                       WHERE Id NOT IN (select UserID from UserPackageLicense where PackageLicense.NameSpacePrefix = : GlobalUtility.strPulsarNameSpace)
                                       AND Id in: setPulserUserIdSelected]) {
                    listUserPackageLicense2Insert.add(new UserPackageLicense(UserId = us.Id, PackageLicenseId = listPackageLicense[0].Id));
                }
            }

        }

        if (!setCongaUserIdSelected.isEmpty()) {
            LIST<PackageLicense> listCongaPackageLicense = [select Id, NamespacePrefix from PackageLicense where NamespacePrefix = : GlobalUtility.strCongaNameSpace];
            if (!listCongaPackageLicense.isEmpty()) {
                for (User us2 : [SELECT Id FROM User
                                        WHERE Id NOT IN (select UserID from UserPackageLicense where PackageLicense.NameSpacePrefix = : GlobalUtility.strCongaNameSpace)
                                        AND Id in: setCongaUserIdSelected]) {
                    listUserPackageLicense2Insert.add(new UserPackageLicense(UserId = us2.Id, PackageLicenseId = listCongaPackageLicense[0].Id));
                }
            }
        }

        if (!listUserPackageLicense2Insert.isEmpty()) {
            insert listUserPackageLicense2Insert;
        }

        if (!setPulserUserIdUnSelected.isEmpty()) {
            list<UserPackageLicense> listUserPackageLicense2Delete  = [select Id from UserPackageLicense where PackageLicense.NameSpacePrefix = : GlobalUtility.strPulsarNameSpace
                           and UserId IN:setPulserUserIdUnSelected];
            delete listUserPackageLicense2Delete;
        }


        if (!setCongaUserIdUnSelected.isEmpty()) {
            List<UserPackageLicense> listUserCongaPackageLicense2Delete = [SELECT Id FROM UserPackageLicense WHERE PackageLicense.NamespacePrefix = : GlobalUtility.strCongaNameSpace AND UserId IN :setCongaUserIdUnSelected];

            delete listUserCongaPackageLicense2Delete;
        }
    }

    /*
    * for AMS community users create an encrypted id to use in the email verification process
    */
    if (Trigger.isInsert  && Trigger.isBefore) {
        List<Profile> amsCommunityProfile = [SELECT Id FROM Profile WHERE Name = 'AMS Community User' LIMIT 1];
        if (!amsCommunityProfile.isEmpty()) {
            Id amsCommunityProfileId = amsCommunityProfile[0].Id;
            List<Contact> userContacts = new List<Contact>();
            for (User u : Trigger.new) {
                if (u.ProfileId == amsCommunityProfileId) {
                    Contact newContact = new Contact(Id = u.ContactId);
                    String h = AMSCommunityUtil.encryptString(u.username);
                    newContact.Encrypted_Username__c = h;
                    newContact.Preferred_Name__c = u.Preferred_Name__c;
                    userContacts.add(newContact);
                }
            }
            update userContacts;
        }
    }

}