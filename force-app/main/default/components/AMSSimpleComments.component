<apex:component controller="AMSSimpleCommentsController" allowDML="true">
    <apex:attribute name="parentId" required="true" type="Id" assignTo="{!parentIdParam}" description="Parent record ID for feed items" />

    <apex:actionRegion >
        <apex:outputPanel layout="block" styleClass="c-comments" id="comments-section">
            <h1 class="c-comments__title">Comment</h1>

            <div class="c-comments__body">
                <apex:repeat value="{!feedItemWrappers}" var="feedItemWrapper">
                    <div class="c-comments__body-comment">
                        <h2 class="author">
                            <a href="">{!feedItemWrapper.feedItem.CreatedBy.Name}</a>
                        </h2>

                        <apex:outputPanel layout="block" styleClass="actions" rendered="{!feedItemWrapper.editAllowed}">
                            <a href="#" class="action action-edit js-edit-comment" onclick="return false;">Edit</a>
                            <apex:commandLink action="{!feedItemWrapper.save}" reRender="comments-section" oncomplete="window.comments();">
                                <button class="action action-save js-button-comment">Save</button>
                            </apex:commandLink>
                        </apex:outputPanel>
                        <pre class="message"><apex:outputText value="{!feedItemWrapper.feedItem.body}" escape="false" /></pre>
                        <apex:inputTextarea styleClass="comment-editor edit-message js-comment-editor" html-placeholder="Leave your comments here" value="{!feedItemWrapper.feedItem.body}" />

                        <p class="date">
                            Posted on {!feedItemWrapper.formattedCreatedDate}
                        </p>
                    </div>
                </apex:repeat>

                <div class="c-comments__body-comment-create">
                    <apex:inputTextarea styleClass="comment-editor js-new-comment-editor" html-placeholder="Leave your comments here" value="{!newFeedItemWrapper.feedItem.body}" />
                    <apex:commandButton styleClass="save-button js-save-new-comment" value="Send" action="{!insertNewFeedItem}" reRender="comments-section" oncomplete="window.comments();" />
                </div>
            </div>
        </apex:outputPanel>
    </apex:actionRegion>

    <script>
        window.comments = function() {
            var editCommentButtons = document.querySelectorAll('.js-edit-comment');

            for (var i = 0; i < editCommentButtons.length; i++) {
                editCommentButtons[i].addEventListener('click', function() {
                    this.parentNode.classList.add('edit-mode');
                });
            }

            var commentEditors = document.querySelectorAll('.js-comment-editor');

            for (var i = 0; i < commentEditors.length; i++) {
                commentEditors[i].addEventListener('keyup', function() {
                    var button = this.parentNode.querySelector('.js-button-comment');

                    button.disabled = false;

                    if (this.value.trim() === '') { button.disabled = true; }
                });
            }

            var newCommentEditor = document.querySelector('.js-new-comment-editor');
            var saveNewCommentButton = document.querySelector('.js-save-new-comment');

            saveNewCommentButton.disabled = true;

            newCommentEditor.addEventListener('keyup', function() {
                saveNewCommentButton.disabled = false;

                if (this.value.trim() === '') { saveNewCommentButton.disabled = true; }
            });
        }

        window.comments();
    </script>

</apex:component>