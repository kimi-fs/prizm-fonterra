<apex:component controller="AMSSimpleAttachmentsController" allowDML="true">
    <apex:attribute name="parentId" required="true" type="Id" assignTo="{!parentIdParam}" description="Parent record ID for feed items" />
    <apex:attribute name="parentPageName" required="true" type="String" assignTo="{!parentPageNameParam}" description="Parent page name for redirect after actions" />
    <apex:attribute name="parentPageParamName" required="true" type="String" assignTo="{!parentPageParamNameParam}" description="Parent page param name for ID param" />

    <h2 class="title">Supporting Documentation</h2>

        <apex:outputPanel id="list-section">
            <apex:actionRegion >
                <apex:repeat value="{!attachmentWrappers}" var="wrapper">
                    <div class="c-list-card">
                        <div class="grid-box grid-box--v-center grid-box--no-gutter grid-box--single-row">
                            <div class="col col--sm-21">
                                <h3 class="c-list-card__heading">
                                    <a href="{!URLFOR($Action.Attachment.Download, wrapper.attachment.Id)}" data-escapewebapp="true">{!wrapper.attachment.Name}</a>
                                </h3>
                            </div>
                            <div class="col col--sm-3 l-strategic-goal__docs-action">
                                <apex:commandLink action="{!wrapper.del}" styleClass="delete" value="Delete" rendered="{!wrapper.deleteAllowed}" reRender="list-section" />
                            </div>
                        </div>
                        <div class="c-list-card__meta">
                            <span class="c-list-card__meta-item">
                                Uploaded date: {!wrapper.formattedCreatedDate}
                            </span>
                            <span class="c-list-card__meta-item">
                                Uploaded by: {!wrapper.attachment.CreatedBy.Name}
                            </span>
                        </div>
                    </div>
                </apex:repeat>
            </apex:actionRegion>
        </apex:outputPanel>

    <div class="buttons">
        <apex:actionRegion >
            <div class="grid-box grid-box--v-center grid-box--no-gutter grid-box--single-row">
                <div class="col col--sm-16">
                    <div class="file-input">
                        <apex:inputFile styleClass="js-file-input" value="{!newAttachmentWrapper.attachment.body}" fileName="{!newAttachmentWrapper.attachment.name}" />
                    </div>
                </div>
                <div class="col col--sm-8">
                    <apex:commandButton styleClass="upload-button js-upload-button" value="Upload Document" action="{!insertNewAttachment}" />
                </div>
            </div>
        </apex:actionRegion>
    </div>

    <script>
        var fileInput = document.querySelector('.js-file-input');
        var uploadFileButton = document.querySelector('.js-upload-button');

        uploadFileButton.disabled = true;

        fileInput.addEventListener('change', function() {
            if (fileInput.value !== '') { uploadFileButton.disabled = false; }
        });
    </script>

</apex:component>