({
    initialize : function(component, event, helper) {
        helper.hideSpinner(component, event);
        helper.showMessage(component, event, "info", $A.get("$Label.c.Approval_Request"));
        
      /* var action = component.get("c.isRequestAlreadySubmitted");
       action.setParams({
            "pIndexRateId" : component.get("v.recordId")
        });
       action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS") {
            var result = response.getReturnValue();
             console.log(result.size());
             component.set('v.alreadySubmittedRequest', result);
              }
             else if(state === "ERROR"){
                var errors = response.getError();
                console.error(errors);
                component.set("v.showOK", false);
           }
        });
        $A.enqueueAction(action);*/
    },
    
    requestApproval : function(component, event, helper) {
        console.log('1.1');
        helper.showSpinner(component, event);
        console.log('1.2');
        var action = component.get("c.submitForApproval");
        action.setParams({
            "pIndexRateId" : component.get("v.recordId")
        });
        console.log('1.3');
        action.setCallback(this, function(response){
            console.log('1.4');
            var state = response.getState();
            helper.hideSpinner(component, event);
            console.log('1');
            if(state == "SUCCESS") {
              console.log('2');
              helper.showSuccess(component, event, helper);
              //helper.raiseToast(component,event,returnObj);
              helper.refreshAndCloseWindow(component, event);
              }
             else if(state === "ERROR"){
                var errors = response.getError();
                console.error(errors);
            
                    component.set("v.showOK", false);
                    helper.showMessage(component, event, "error", returnObj.message);
            }
        });
        $A.enqueueAction(action);
    },
    
   
    raiseToast : function(component, event, result) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": "Success",
            "title": "SUCCESS",
            "message": result.message
        });
        toastEvent.fire();
    },
    
    showSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success',
            message: 'Approval Request Submitted',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
    },
    
    requestSubmittedToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error',
            message:'Approval request already submitted',
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },
     
    refreshAndCloseWindow : function(component, event) {
         console.log('4');
         $A.get("e.force:refreshView").fire();
         $A.get("e.force:closeQuickAction").fire();
    },
    showMessage : function(component, event, messageType, messageText) {
        if (messageType == "error"){
            component.set("v.iconName", "utility:ban");
            component.set("v.iconVariant", "error");
            
            var textMessage = component.find("textMessage");
            $A.util.addClass(textMessage, "slds-text-color_error");
        } else if (messageType == "warning"){
            component.set("v.iconName", "utility:notification");
            component.set("v.iconVariant", "warning");
        } 
        component.set("v.messageText", messageText);
        component.set("v.showMessage", true);
    },
    showSpinner : function(component, event) {
        var spinner = component.find("spinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner : function(component, event) {
        var spinner = component.find("spinner");
        $A.util.addClass(spinner, "slds-hide");
    },
})