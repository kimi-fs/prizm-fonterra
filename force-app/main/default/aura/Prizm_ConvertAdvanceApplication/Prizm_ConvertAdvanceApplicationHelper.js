({
    initialize : function(component, event, helper) {
        helper.hideSpinner(component, event);
        helper.showMessage(component, event, "info", $A.get("$Label.c.Convert_Advance_Application"));
    },
    convertAdvanceApplication : function(component, event, helper) {
        helper.showSpinner(component, event);
        var action = component.get("c.convertAdvanceApptoLendingApp");
        action.setParams({
            "pAdvanceApplicationId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            helper.hideSpinner(component, event);
            if(state === "SUCCESS") {
                var returnObj = JSON.parse(response.getReturnValue());
                console.log(returnObj);
                if(returnObj.isSuccess == true) {
                    helper.raiseToast(component,event,returnObj);
                    helper.refreshAndCloseWindow(component, event);
                } else {
                    component.set("v.showOK", false);
                    helper.showMessage(component, event, "error", returnObj.message);
                }
            } else if(state === "ERROR"){
                var errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);
    },
    raiseToast : function(component, event, result) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": "Success",
            "title": "SUCCESS",
            "message": result.message
        });
        toastEvent.fire();
    },
    refreshAndCloseWindow : function(component, event) {
        $A.get("e.force:refreshView").fire();
        $A.get("e.force:closeQuickAction").fire();
    },
    showMessage : function(component, event, messageType, messageText) {
        if (messageType == "error"){
            component.set("v.iconName", "utility:ban");
            component.set("v.iconVariant", "error");
            
            var textMessage = component.find("textMessage");
            $A.util.addClass(textMessage, "slds-text-color_error");
        } else if (messageType == "warning"){
            component.set("v.iconName", "utility:notification");
            component.set("v.iconVariant", "warning");
        } 
        component.set("v.messageText", messageText);
        component.set("v.showMessage", true);
    },
    showSpinner : function(component, event) {
        var spinner = component.find("spinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner : function(component, event) {
        var spinner = component.find("spinner");
        $A.util.addClass(spinner, "slds-hide");
    },
})