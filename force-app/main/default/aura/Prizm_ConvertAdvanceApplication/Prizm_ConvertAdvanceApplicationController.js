({
	doInit : function(component, event, helper) {
        helper.initialize(component, event, helper);
	},
    doCancel : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
	},
    doConvert : function(component, event, helper) {
        helper.convertAdvanceApplication(component, event, helper);
	}
})