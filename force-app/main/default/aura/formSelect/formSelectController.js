({
    selectForm: function(component, event, helper) {
        const formValue = component.find('formSelect').get('v.value');
        component.set('v.formName', formValue);
    },
    closeQA: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    cancel: function(component, event, helper) {
        component.set('v.formName', null);
    }
})