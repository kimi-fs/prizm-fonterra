({
	doInit : function(component, event, helper) {
        helper.initialize(component, event, helper);
	},
    doCancel : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
	},
    doConvert : function(component, event, helper) {
        if(component.get("v.isApproved")==true)
        {
        helper.applyRateChange(component, event, helper);
        }
        else
        {
            helper.showError(component, event, helper);
        }
	}
})