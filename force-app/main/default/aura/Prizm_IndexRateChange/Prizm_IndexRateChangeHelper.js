({
    initialize : function(component, event, helper) {
        helper.hideSpinner(component, event);
        helper.showMessage(component, event, "info", $A.get("$Label.c.Apply_Rate_Change"));
        
       var action = component.get("c.isApprovalDone");
       action.setParams({
            "pIndexRateId" : component.get("v.recordId")
        });
       action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS") {
            var result = response.getReturnValue();
                console.log(result);
             component.set('v.isApproved', result);
              }
             else if(state === "ERROR"){
                var errors = response.getError();
                console.error(errors);
                component.set("v.showOK", false);
           }
        });
        $A.enqueueAction(action);
  
    },
    
    applyRateChange : function(component, event, helper) {
        helper.showSpinner(component, event);
        var action = component.get("c.executeRateChangeJob");
        console.log('1');
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state'+state);
            helper.hideSpinner(component, event);
            if(state == "SUCCESS") {
                    console.log('in success');
                    console.log('2');
                    helper.showSuccess(component, event, helper);
                   // helper.raiseToast(component,event,returnObj);
                    helper.refreshAndCloseWindow(component, event);
               } else if(state === "ERROR"){
                var errors = response.getError();
                console.error(errors);
                component.set("v.showOK", false);
                helper.showMessage(component, event, "error", returnObj.message);
            }
        });
        $A.enqueueAction(action);
    },
    
    showSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success',
            message: 'Index Rate Applied',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
    },
    
    showError : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error',
            message:'Index Rate is not approved',
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },
    
    refreshAndCloseWindow : function(component, event) {
        console.log('4');
        $A.get("e.force:refreshView").fire();
        $A.get("e.force:closeQuickAction").fire();
    },
    showMessage : function(component, event, messageType, messageText) {
        if (messageType == "error"){
            component.set("v.iconName", "utility:ban");
            component.set("v.iconVariant", "error");
            
            var textMessage = component.find("textMessage");
            $A.util.addClass(textMessage, "slds-text-color_error");
        } else if (messageType == "warning"){
            component.set("v.iconName", "utility:notification");
            component.set("v.iconVariant", "warning");
        } 
        component.set("v.messageText", messageText);
        component.set("v.showMessage", true);
    },
    showSpinner : function(component, event) {
        var spinner = component.find("spinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    hideSpinner : function(component, event) {
        var spinner = component.find("spinner");
        $A.util.addClass(spinner, "slds-hide");
    },
})